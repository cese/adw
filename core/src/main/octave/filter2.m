%sudo apt-get install liboctave-dev
%pkg install -forge control
%pkg install -forge signal
%pkg load signal

%Start from nothing!
clear;

x = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];
len = 4;
stride = len/2;

% Buffer it and window
win = hamming(len); %chose window type based on your application
x = buffer(x, len, stride); % 50% overlap between frames in this instance
x = x(:, 2:end-1); % optional step to remove zero padded frames
x = (  x' * diag(win)  )'; % efficiently window each frame using matrix algebra

x;


