package fasten

import pt.inescn.etl.stream.Load._
import scala.collection.mutable.ArraySeq

object Test2_writeRowToStringCSV {
  /**---------------------imperative programming way---------------------*/
  def rowToString(row:Row): String = {
    val colNames:List[String] = row.colNames
    var colSize =  unpack[ArraySeq[String]](row(colNames(0))).right.get.size

    var i = 0
    var str:String = ""
    //set header
    /*var strCount = 0
    while (strCount < row.colNames.size) {
      if (strCount == row.colNames.size-1) str += row.colNames(strCount) + "\n"
      else str += row.colNames(strCount) + ","
      strCount += 1
    }*/
    str += colNames.mkString(",") + "\n"
    //set body
    while (i < colSize) {
      var j = 0
      while (j < row.size) {
        if (j == row.size-1)
          str += unpack[ArraySeq[String]](row(colNames(j))).right.get(i) + "\n"
        else
          str += unpack[ArraySeq[String]](row(colNames(j))).right.get(i) + ","
        j += 1
      }
      i += 1
    }
    str += "\n"
    str
  }
  /**---------------------/imperative programming way---------------------*/
  /**---------------------functional programming way---------------------*/
  /**
    * This function writes the Row contents in a String (CSV format)
    * @param row row to be converted to string
    * @return string with comma-separated fields
    */
  def rowToStrFP(row:Row): String = {
    val colNames:List[String] = row.colNames
    val str = colNames.mkString(",") + "\n"
    val fstCol = unpack[ArraySeq[String]](row(colNames(0))).right.get

    def rowIterator (i:Int, acc:String):String = {
      (0 until row.size).foldLeft(acc){case (acc,j) =>
        if (j == row.size-1)
          acc + unpack[ArraySeq[String]](row(colNames(j))).right.get(i) + "\n"
        else
          acc + unpack[ArraySeq[String]](row(colNames(j))).right.get(i) + ","
      }
    }

    (0 until fstCol.size).foldLeft(str){(acc, j) =>
      rowIterator(j, acc)
    }
  }
  /**---------------------/functional programming way---------------------*/

  val testRow = Test0_frame.testingRow.testRow

  /**---------------------imperative programming way---------------------*/
  /*---------------------use function rowToString---------------------*/
  val conToString:String = rowToString(testRow)
  //println("imperative programming (use function rowToString): \n" + conToString + "\n /imperative programming (use function rowToString)")
  /**---------------------/imperative programming way---------------------*/

  /**---------------------functional programming way---------------------*/
  /*---------------------use function rowToStringFP---------------------*/
  val conToStringFP:String = rowToStrFP(testRow)
  //println("functional programming (use function rowToStringFP): \n" + conToStringFP + "\n /functional programming (use function rowToStringFP)")
  /**---------------------/functional programming way---------------------*/

  /*val wdow = Test0_frame.wdow
  val testStrWdw = wdow.iterable.toIterator.foreach(rowToStrFP)
  println("teste: " + wdow.iterable.toIterator.foreach(rowToStrFP))
  wdow.map { r: Row =>
    println(rowToStrFP(r))
  }
  wdow.map {r: Row =>
    println(unpack[ArraySeq[String]](r("x")).right.get(1))
  }*/

  /*val read33:Iterator[String] = wdow
    .iterable
    .toIterator
    .map{roo:Row =>
      println("entrou1")
      val testString:String = rowToStrFP(roo)
    testString
    }
  println(read33)
  read33.foreach(println)*/


  def main(args: Array[String]): Unit = {
    println("imperative programming (use function rowToString): \n" + conToString + "\n /imperative programming (use function rowToString)")
    println("functional programming (use function rowToStringFP): \n" + conToStringFP + "\n /functional programming (use function rowToStringFP)")
  }
}
