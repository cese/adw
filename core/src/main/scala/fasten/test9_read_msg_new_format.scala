package fasten

/*
Rui told the messages structure cannot differ much from:
{"Time(s)":0,"X":0.356335,"Y":1.946053,"Z":2.391698,"timestamp":1589360333017}
where the fields:
- X, Y, Z represent each of the rows in the file
- Time(s) was added to represent the time in which the message is generated
*/

import io.circe.parser._
import io.circe.Error

import produtech.ReceiveApp2.makeFrame
import pt.inescn.app.RabbitConfigs.{ReceiveAppConfig, ReceiveAppConfigHelp}
import pt.inescn.etl.stream.Load._
import pt.inescn.etl.stream.RabbitMQ.RabbitConnection
import pt.inescn.utils.ADWError

import scala.collection.mutable.ListBuffer

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global

import java.text.SimpleDateFormat
import java.util.Calendar
/**
  * root/runMain fasten.test9_read_msg_new_format -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_NewMachine -publishQueue outbox/tests
  */

object test9_read_msg_new_format {

  private val format = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")

  def now: String = {
    val now = Calendar.getInstance
    format.format(now.getTime)
  }

  def processData(conf: ReceiveAppConfig, publishQueue: String) = {
    val constate = makeFrame(conf)
    constate match {
      case Left(e) => e
      case Right(cons) =>

        val newFrame: Either[ADWError, Frame] = publish(cons.frame, cons.conn, conf, publishQueue)

        if (conf.numMessages <= 0) {
          newFrame.fold(e=>e, f => f.iterable.foreach(_ => ()))
        }
        else {
          newFrame.fold(e => e,  _.iterable.take(conf.numMessages).foreach( _ => ()))
        }

        cons.stop
    }
  }

  def getFilenames(args: Array[String])= {
    val init: Seq[String] = Seq()
    val remainder: Seq[String] = Seq()
    val indFile = args.indexOf("-filename")
    if (indFile>=0) {
      //check if there is a flag after the filename flag
      val indNextFlag = args.indexWhere(_ contains "-",indFile+1)
      val ubound = if (indNextFlag > indFile) indNextFlag else args.length
      ((indFile+1 until ubound).foldLeft(init) { (acc, cur) =>
        acc :+ args(cur)
      },
        (ubound until args.length).foldLeft(remainder) {(acc,cur) =>
          acc :+ args(cur)
        }
      )
    } else (init,remainder)//(init,-1)

  }

  def publish(f:Frame,
              conn: RabbitConnection,conf: ReceiveAppConfig,
              queueName:String) = {

    def publishMessage(row: Row): Row = {
      val times = "Time(s)"
      val timestamp = "timestamp"
      val x = "X"
      val y = "Y"
      val z = "Z"

      // val v_times = unpack[Double](row(times))
      val v_timestamp = unpack[String](row(timestamp))
      println(s"**************************v_timestamp:${v_timestamp.toString}**************************")
      val v_x = unpack[String](row(x))
      val v_y = unpack[String](row(y))
      val v_z = unpack[String](row(z))

      val e = for {
        //varTimes <- v_times
        varTimestamp <- v_timestamp
        varX <- v_x
        varY <- v_y
        varZ <- v_z

        //data = List(now.toString, conn.tearID , varTimes, varTimestamp, varX, varY, varZ)
        data = List(now.toString, conn.tearID , varTimestamp, varX, varY, varZ)
      } yield data

      e.fold(
        // print error
        err => if (conf.verbose > 0) println(err),
        { msg =>
          if (msg.nonEmpty) {
            val header = "Send_Timestamp,TearID,Time(s),X_acc,Y_acc,Z_acc"
            val temp = (header concat "\n" concat msg.mkString(",")).getBytes()

            conn.channel.basicPublish("", queueName, null, temp)
            if (conf.verbose > 0) {
              println(s"ALERT:\n $e")
            }
          }
        }
      )
      row
    }

    Right(f.map(publishMessage(_)))

  }

  def decodeTest() = {

    /*tests*/

    val msg_numeric = "{\"Time(s)\":0,\"X\":0.356335,\"Y\":1.946053,\"Z\":2.391698,\"timestamp\":1589360333017}"
    val msg_string = """{"Time(s)":"0","X":"0.356335","Y":"1.946053","Z":"2.391698","timestamp":"1589360333017"}"""

    // test decode to numeric (message currently received in this format)
    val test_decode_numeric = decode[Map[String, Double]](msg_numeric)

    // test decode to string (message not currently received in this format thus yields error)
    val test_decode_string = decode[Map[String, String]](msg_string)

    // test decode with ADIRA format
    val test_decode_adira_numeric = decode[Map[String, Vector[String]]](msg_numeric)
    val test_decode_adira_string = decode[Map[String, Vector[String]]](msg_string)

    println(test_decode_numeric)
    println(test_decode_string)
    println(test_decode_adira_numeric)
    println(test_decode_adira_string)

    /*tests*/

  }

  def main(args: Array[String]): Unit = {

    //decodeTest()

    // not using futures
    // (...)

    // using futures for multiple filenames passed
    val (filenames,remainder) = getFilenames(args)
    if (filenames.isEmpty) {
      println("Errors found:\nNo config filepath provided.\nTerminating.")
      return
    }

    val publishQueue = if (args.indexOf("-publishQueue") >= 0) {
      args(args.indexOf("-publishQueue")+1)
    } else {
      println("Errors found:\nNo publish queue provided.\nTerminating.")
      return
    }

    val list = filenames.map{ f =>
      val default = ReceiveAppConfig(f)
      /*
      not assuming -filename is the last flag:
      one cannot assume the relative position of the flag, as such one needs to compose the array to parse in:
      1- args up to -filename flag
      2- appended of the current filename, f
      3- appended of the remainder of the args
      */
      val argsWithFN = args.take(args.indexOf("-filename")+1) :+ f
      val parseArgs = argsWithFN ++ remainder
      // Parse command line parameters
      val recAppConf = default.parse(parseArgs)
      // execute according to command line parameters
      if (recAppConf.error.nonEmpty) {
        val errStr = recAppConf.error.indices.foldLeft(""){(acc,cur) =>
          "\t" + acc.concat(recAppConf.error(cur))
        }
        Left("Errors found:\n" + errStr + "\nTerminating." + "\n Help:\n" + ReceiveAppConfigHelp)
      }
      else {
        val f: Future[Unit] = Future {
          processData(recAppConf, publishQueue)
        }
        Right(f)
      }
    }

    val (errs, futures) = list.partition{
      case Left(_) => true
      case Right(_) => false    //if this case is not exhaustive it yields an exception in the right case
    }

    if (errs.nonEmpty) {
      errs.foreach {case  Left(e) => println(e)}
    }
    else {
      val fut: Seq[Future[Unit]] = futures.map {case Right(n) => n}
      val futs = Future.sequence(fut)

      Await.ready(futs, Duration.Inf)
    }

  }
}
