package fasten

import scala.collection.immutable

object Test1_readCSV {

  /*val str =
    """a,b,c
      |1,2,3
      |4,5,6""".stripMargin*/

  /*esta forma de definir a string acrescenta uns caracteres no final da linha! usar a forma seguinte:*/

  val str = "a,b,c\n1,2,3\n4,5,6"

  val lines = str.split("\n")

  //val header_errado = lines(0)
  /*errado -> considera como um único elemento, que é uma string*/

  //val header_errado2 = lines(0).map(_.toString)
  /* errado -> considera como vários elementos
  (tantos quantos o número de caracteres);
   cada caracter (incluíndo virgulas) é uma string*/

  //val linesColumns:Array[Array[String]] = lines.map(l => l.split(","))
  /*incompleto -> para cada linha é feito um novo split,
   (neste caso por vírgula, isto é, as colunas), sendo que após
  split se obtêm os elementos de cada coluna;
  incompleto porque não elimina os espaços em branco entre os caracteres*/

  val linesColumns:Array[Array[String]] = lines.map(l => l.split(",").map(_.trim))
  /*igual ao anterior, mas para cada elemento é feito um map que faz o trim à sequência
  ou seja, retorna uma cópia da string mas elimina os espaços em branco antes e após a string*/

  val header:Array[String] = linesColumns(0)
  /*de forma equivalente:
  val headerTest:Array[String] = linesColumns.head
  */
  val colIndices:Range = linesColumns.head.indices//.map(_.toString)
  /*usar os índices de coluna em vez dos valores*/

  //val map = linesColumns.drop(1).zip(header)
  /*errado -> está a fazer o merge das colunas (Array completo da coluna) com
  os elementos do header
  -> pretende-semerge elemento a elemento da coluna com o elemento respectivo do header
  */

  //val map = linesColumns.drop(1).map(l=>l.zip(header))
  /*ordem trocada*/
  val map = linesColumns.drop(1).map(l=>header.zip(l))
  /*
  correcto -> para cada elemento de l (da coluna, que neste caso é um array de string)
  faz o zip (merge) do elemento do header e o correspondente elemento da coluna (array de string)
   */
  val mapIndices = linesColumns.drop(1).map(l=>colIndices.zip(l))

  //Forma para ler ficheiro do Luis Neto (split by "|")
  //val linesColumns:Array[Array[String]] = lines.map(l => l.split("|").map(_.trim))      //does not work as expected
  /*split uses regular expression and in regex | is a metacharacter representing the OR operator.
    You need to escape that character using \ (written in String as "\\" since \ is also a metacharacter
    in String literals and require another \ to escape it).
   */
  //val linesColumns:Array[Array[String]] = lines.map(l => l.split("[|]").map(_.trim))

  //in order not to be exclusive, ie, either split by ",", or split by "|", this operation should work:
  val rgx = "[,|]"
  val linesColumnsLN:Array[Array[String]] = lines.map(l => l.split(rgx).map(_.trim))
  //based on: https://stackoverflow.com/questions/13525024/how-to-split-a-mathematical-expression-on-operators-as-delimiters-while-keeping

  val headerLN:Array[String] = linesColumnsLN(0)
  val rowLines:Array[Array[(String, String)]] = linesColumns.drop(1).map(l=>header.zipAll(l,"",""))

  /**------------------código do toCSVStringIterator------------------ */
  val csvLines = lines.map( s => s.split(",").map(_.trim))
  val indices = csvLines.head.indices.map(_.toString)
  val rowlLines: Array[immutable.IndexedSeq[(String, String)]] = csvLines.map(l => indices.zip(l))
  /** ------------------/código do toCSVStringIterator------------------ */

}