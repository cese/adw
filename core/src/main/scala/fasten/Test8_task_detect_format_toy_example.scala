package fasten

import pt.inescn.etl.stream.Load._

/**
  * sbt "root/runMain fasten.Test8_task_detect_format_toy_example"
  */
object Test8_task_detect_format_toy_example {

  def detectValuesFormatBlock(f:Frame): Frame = {

    val filteredFrame = f.filterWith { r =>
      // get column names
      val cols = r.colNames

      // instead of getting the value individually, as in the case one knows the 2 column ids:
      // val colTS: Either[String, String] = r(conn.varTs)
      // get the values for all columns by mapping
      val row_values = cols.map(rv => r(rv))

      // instead of performing the operation individually, as in the case one knows the 2 column ids:
      // eval = valTS.toString.length > 0 && valTemp.toString.length > 0
      // use fold to run through all vals and accumulate the value of the operation bu folding through the collection
      // true as the null initial param for the logic operation &&

      val eval = row_values.forall(curr => curr.toString.length > 15)

      // If uncommented does not acknowledge the type returned inside eval
      //eval.fold(_ => false, s => s)
      //for debugging purposes:
      println("****************evalEND*************************")
      eval
    }
    println("****************detectValuesEND*************************")
    filteredFrame
  }

  /**
    * replicate [[fasten.Test8_task_detect_format_toy_example#detectValuesFormat_oneColumnFilter(pt.inescn.etl.stream.Load.Frame)]]
    * for multiple conditions on filtering
    * @param f
    * @return
    */
  def detectValuesFormat_multipleColumnFilter(f:Frame): Frame = {

    val filteredFrame = f.filterWith { r =>
      val xv: Either[String, String] = r("x")
      val yv: Either[String, String] = r("y")

      val t = for {
        xx <- xv
        yy <- yv

        eval = xx.toDouble > 100.0 && yy.toDouble < 215.0
      } yield eval
      println("****************evalEND*************************")

      t.fold(_ => false, s => s)

    }
    println("****************detectValuesEND*************************")
    filteredFrame
  }

  /**
    * replicate [[fasten.Test8_task_detect_format_toy_example#detectValuesFormat_oneColumnFilter_replicateExampleLoadStreamSpec(pt.inescn.etl.stream.Load.Frame)]]
    * using different function notation - to be used for multiple conditions for filtering
    * @param f frame to filter values from
    * @return filtered frame
    */
  def detectValuesFormat_oneColumnFilter(f:Frame): Frame = {

    val filteredFrame = f.filterWith { r =>
      val xv: Either[String, String] = r("x")

      val t = for {
        xx <- xv
        eval = xx.toDouble > 100.0
      } yield eval

      t.fold(_ => false, s => s)

    }
    filteredFrame
  }

  def test_map(f:Frame): Frame = {

    val mapped_frame = f.map({ r:Row =>
      val nr = for {
        xx <- unpack[String](r("x"))
        yy <- unpack[String](r("y"))
        zz <- unpack[String](r("z"))
        xd = xx.toDouble
        yd = yy.toDouble
        //zd = zz.toDouble
        x_y = xd + yd
        nrow = Row(Map[String, Val[_]]("x" -> xd, "y" -> yd, "z" -> x_y))
      } yield nrow
      nr.fold(err => r + ("x+y" -> Val[String](err)), (nrow: Row) => r ++ nrow)
    },
      "x" -> 0.0, "y" -> 0.0, "z" -> 0.0
    )
    mapped_frame
  }

  /**
    * replicate example in [[pt/inescn/etl/stream/LoadStreamSpec.scala:1167]] to filter values
    * from an input frame, based on condition
    * @param f frame to filter values from
    * @return filtered frame
    */
  def detectValuesFormat_oneColumnFilter_replicateExampleLoadStreamSpec(f:Frame): Frame = {

    val filteredFrame = f.filterWith { r =>
      val xv: Either[String, String] = r("x")

      xv.fold(_ => false, s => s.toDouble > 100.0)

    }
    filteredFrame
  }


  def printFrame(f:Frame) = {
    println("***********************************frame***********************************")
    f.iterable.toIterator.foreach(println)
  }

  def main(args: Array[String]): Unit = {
    val ix: Iterator[String] = Iterable("11", "12", "13", "14", "15", "16", "17", "18", "19", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120").toIterator   // ix: Iterator[String] = <iterator>
    val iy: Iterator[String] = Iterable("21", "22", "23", "24", "25", "26", "27", "28", "29", "210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220").toIterator   // iy: Iterator[String] = <iterator>
    val iz: Iterator[String] = Iterable("31", "32", "33", "34", "35", "36", "37", "38", "39", "310", "311", "312", "313", "314", "315", "316", "317", "318", "319", "320").toIterator   // iz: Iterator[String] = <iterator>
    val colx1:(String, Iterator[String]) = ("x", ix)                                                                                                                                    // colx1: (String, Iterator[String]) = (x,<iterator>)
    val coly1:(String, Iterator[String]) = ("y", iy)                                                                                                                                    // coly1: (String, Iterator[String]) = (y,<iterator>)
    val colz1:(String, Iterator[String]) = ("z", iz)                                                                                                                                    // colz1: (String, Iterator[String]) = (z,<iterator>)

    val testFrame:Frame = Frame(colx1, coly1, colz1)

    // prints unfiltered frame
    //printFrame(testFrame)

    // filters frame rows for which:
    // values in column x are such that: val(x) > 100
    //val filteredFrame = detectValuesFormat_oneColumnFilter_replicateExampleLoadStreamSpec(testFrame)

    // same as previous; different function design
    //val filteredFrame = detectValuesFormat_oneColumnFilter(testFrame)

    // filters frame rows for which:
    // values in column x are such that: val(x) > 100
    // values in column y are such that: val(y) < 215
    //val filteredFrame = detectValuesFormat_multipleColumnFilter(testFrame)

    // test for block vars structure
    // filters frame rows for which:
    // for any column, if the filed is empty
    val filteredFrame = detectValuesFormatBlock(testFrame)

//    val filteredFrame = test_map(testFrame)

    // prints filtered frame
    printFrame(filteredFrame)

  }
}
