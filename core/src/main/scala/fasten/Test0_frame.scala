package fasten

import pt.inescn.etl.stream.Load._
import pt.inescn.search.stream.Tasks.{ColumnName, ColumnNames}
import pt.inescn.utils.ADWError

import scala.collection.mutable.ArraySeq

/**
  root/runMain fasten.Test0_frame
  */

object Test0_frame {

  //test code of frames and rows from http://cese.gitlab.io/adw/docs/frames.html

  /**row*/
  object testingRow {
    val r0: Val[String] = "100"

    val r1: Val[Double] = 200.0
    val r2: Val[_] = "300"

    val row1 = Row("r0" -> r0, "r1" -> r1, "r2" -> r2)
    val row3 = row1((e: String) => e.toDouble, "r0", "r2")
    val row4 = row1((e: String) => e.toDouble, "r0" -> "r0d", "r2" -> "r2d")
    //println("1:" + row1.colTypes + "\n" +
    //        "3:" + row3.colTypes + "\n" +
    //        "4:" + row4.colTypes)

    //println("1:" + row1 + "\n" + "3:" + row3 + "\n" + "4:" + row4)

    val nr0: Val[_] = "0"
    val row5 = row4 - "r0" - "r1" - "r2"
    val row6 = row5 ++ row1
    //println("add rows 5++1 \n" + "1:" + row1 + "\n" + "5:" + row5 + "\n" + "6:" + row6)


    val testRow = Row(Map("x" -> Val(ArraySeq("1", "1", "1", "1", "1")), "y" -> Val(ArraySeq("11", "11", "11", "11", "11")), "z" -> Val(ArraySeq("21", "21", "21", "21", "21"))))

    //simple retrieve ops:
    /*
    println("elementos da coluna x :" + unpack[ArraySeq[String]](testRow("x")).right.get)
    println("elementos da coluna x (optional) :" + unpack[ArraySeq[Int]](testRow("x")))
    println("primeiro elemento do conjunto de elementos da coluna x :" + unpack[ArraySeq[String]](testRow("x")).right.get(1))
    println("comprimento da row :" + testRow.size)
    println("nome das colunas da row :" + testRow.colNames)
    println("col size = " + unpack[ArraySeq[String]](testRow("x")).right.get.size)
    */

    val colNames:List[String] = testRow.colNames
    val cols:ArraySeq[String] = unpack[ArraySeq[String]](testRow(colNames(1))).right.get

    //println("elementos da coluna " + colNames(1) + ": " + cols.foldLeft("")((acc,res)=> acc + res + ","))

    var colSize =  unpack[ArraySeq[String]](testRow(colNames(0))).right.get.size
  }
  /** /row*/

  /**frame*/
  object testingFrame {

    val ix: Iterator[String] = Iterable("11", "12", "13", "14", "15", "16", "17", "18", "19", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120").toIterator   // ix: Iterator[String] = <iterator>
    val iy: Iterator[String] = Iterable("21", "22", "23", "24", "25", "26", "27", "28", "29", "210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220").toIterator   // iy: Iterator[String] = <iterator>
    val iz: Iterator[String] = Iterable("31", "32", "33", "34", "35", "36", "37", "38", "39", "310", "311", "312", "313", "314", "315", "316", "317", "318", "319", "320").toIterator   // iz: Iterator[String] = <iterator>
    val colx1:(String, Iterator[String]) = ("x", ix)                                                                                                                                    // colx1: (String, Iterator[String]) = (x,<iterator>)
    val coly1:(String, Iterator[String]) = ("y", iy)                                                                                                                                    // coly1: (String, Iterator[String]) = (y,<iterator>)
    val colz1:(String, Iterator[String]) = ("z", iz)                                                                                                                                    // colz1: (String, Iterator[String]) = (z,<iterator>)

    val testFrame:Frame = Frame(colx1, coly1, colz1)
    //println("***********************************frame***********************************")
    //testFrame.iterable.toIterator.foreach(println)
    //println(testFrame.toString)
    //println("testFrame to String:\n" + testFrame.toString + "\n/testFrame to String.")

    val ixw: Iterator[String] = Iterable("11", "12", "13", "14", "15", "16", "17", "18", "19", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120").toIterator    // ix: Iterator[String] = <iterator>
    val iyw: Iterator[String] = Iterable("21", "22", "23", "24", "25", "26", "27", "28", "29", "210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220").toIterator    // iy: Iterator[String] = <iterator>
    val izw: Iterator[String] = Iterable("31", "32", "33", "34", "35", "36", "37", "38", "39", "310", "311", "312", "313", "314", "315", "316", "317", "318", "319", "320").toIterator    // iz: Iterator[String] = <iterator>
    val colx1w:(String, Iterator[String]) = ("x", ixw)                                                                                                                                    // colx1: (String, Iterator[String]) = (x,<iterator>)
    val coly1w:(String, Iterator[String]) = ("y", iyw)                                                                                                                                    // coly1: (String, Iterator[String]) = (y,<iterator>)
    val colz1w:(String, Iterator[String]) = ("z", izw)                                                                                                                                    // colz1: (String, Iterator[String]) = (z,<iterator>)

    val windowedFrame:Frame = Frame(colx1w, coly1w, colz1w).window(5,5)
    //println("***********************************windowed frame***********************************")
    //windowedFrame.iterable.toIterator.foreach(println)
    //println(windowedFrame)
    //println(cols1.window(5,5))


    val ix2:List[String] = List("11", "12", "13", "14", "15", "16", "17", "18", "19", "110", "111", "112", "113", "114", "115", "116", "117", "118", "119", "120")
    val iy2:List[String] = List("21", "22", "23", "24", "25", "26", "27", "28", "29", "210", "211", "212", "213", "214", "215", "216", "217", "218", "219", "220")
    val iz2:List[String] = List("31", "32", "33", "34", "35", "36", "37", "38", "39", "310", "311", "312", "313", "314", "315", "316", "317", "318", "319", "320")
    val colx12:(String, List[String]) = ("x", ix2)
    val coly12:(String, List[String]) = ("y", iy2)
    val colz12:(String, List[String]) = ("z", iz2)

    val testFrame2:Frame = Frame(colx12, coly12, colz12)
    //val windowedFrame2:Frame = testFrame2.window(5,5)           //if we define this frame from an operation on the other frame (frame2) when we consume one we can no longer consume the other
    val windowedFrame2:Frame = Frame(colx12, coly12, colz12).window(5,5)

    /*
    println("***********************************frame2***********************************")
    testFrame2.iterable.toIterator.foreach(println)
    println("***********************************windowed frame2***********************************")
    windowedFrame2.iterable.toIterator.foreach(println)
    println("***********************************windowed frame2 properties***********************************" +
      "field n: " + windowedFrame2.colSize +"\n" +
      "field names (toString): " + windowedFrame.colIDs.toString +"\n" +
      "field names (mkString): " + windowedFrame.colIDs.mkString(",") +"\n" +
      "field types (toString): " + windowedFrame2.colTypes.toString +"\n" +
      "field types (mkString): " + windowedFrame2.colTypes.mkString(","))
    */

    val wdow = Frame(colx1, coly1, colz1).window(5,5)
    //println(wdow)
    //wdow.iterable.toIterator.foreach(println)
    //println("window:\n" + wdow + "\n/window.")
    //println(testFrame.window(5,5))

    /*println("dif window def:\n")
    testFrame.window(5,5).iterable.toIterator.foreach(println)
    println("\n/dif window def.")*/

    /*println("iterator print")
    testFrame.window(5,5).iterable.toIterator.foreach(println)
    println("regular print")
    wdow.foreach(println)*/

    //testFrame.window(5,5).iterable.toIterator.foreach(println)
    val read3 = testFrame.window(5,5)
          .iterable
          .toIterator
    // Each window is converted to a Map of arrays columns
          .map { r =>
            r
          }
    //read3.foreach(println)

  }
  /** /frame*/


  /** templateTask - task simulation: perform a given operation in a frame,
    * and add the result of the operation to a newly created column
    * @param f the frame to which the operation is to be performed
    * @param col the base name of the new column/s to be created (see func name for details)
    * @param cols the columns required to perform the operation
    * @return Either[ADWError, Frame] the newly created frame with the added columns with the results, or error
    * */
  def templateTask(f:Frame, col:ColumnName, cols: ColumnNames) : Either[ADWError, Frame] = {

    /**
      * funcName - creates the name of the new column as:
      * name of the column where operation is performed + operation performed
      * @param str
      * @return [[String]]
      * */
    def funcName(str:String): String = str + col.col

    /**
      *
      * */
    val newRow = cols.cols.map(c=> funcName(c) -> Val(ArraySeq(0.0)))   //row typing info

    /**
      * func - the function to perform in the task
      * (in this case it just generates a random number btw 100 and 200)
      * */
    def func(n:Array[String]) = {
      //def func(n:Array[Int]) = {
      val start = 100
      val end = 200
      val rnd = new scala.util.Random
      val ret = start + rnd.nextInt((end-start)+1)
      ret
      ArraySeq(ret)   //após adicionar esta linha continua a dar erro cannot be cast to scala.collection.mutable.ArraySeq
    }


    def op(data:Row) : Row = {
      val temp = data.filter(cols.cols: _*).map { case (k,v) =>
        val nk = funcName(k)
        val tmp = unpack[ArraySeq[String]](v)
        val nv = tmp match {
        case Right(a) =>
          val arr = a.toArray
          val nv = func(arr)
          Val(ArraySeq(nv))
        case _ =>
          ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk -> nv
      }
      data ++ temp
    }

    /**
      * the returning result of the task, ie
      * to the received frame apply op
      * */
    Right(f.map(op, newRow: _*))
  }

  def main (args: Array[String] ): Unit = {


    /*val testFrame:Frame = testingFrame.testFrame2
    val testRow:Row = testingRow.testRow

    val extendCol:ColumnName = ColumnName("testExtend")
    val opColumns: ColumnNames = ColumnNames(Seq("x", "y"))
    val templateFrame = templateTask(testFrame.window(5,5), extendCol, opColumns)

    templateFrame match {
      case Right(f) =>
        f.iterable.toIterator.foreach(println)
      case _ => println("erro")
    }*/
    val testVal = testingRow.testRow("x")
    val d: Either[String, ArraySeq[String]] = unpack[ArraySeq[String]](testVal)
    //println(unpack[ArraySeq[String]](testingRow.testRow("x")))
    //println(unpack[ArraySeq[String]](testingRow.testRow("x")).right.get)

    d.foreach{vd =>
      val colY = unpack[ArraySeq[String]](testingRow.testRow("y"))
      val colZ = unpack[ArraySeq[String]](testingRow.testRow("z"))
      println(vd)
    }

    d.right.get.foreach{vd =>
      val colY = unpack[ArraySeq[String]](testingRow.testRow("y"))
      val colZ = unpack[ArraySeq[String]](testingRow.testRow("z"))
      println(vd)
    }

  }
}