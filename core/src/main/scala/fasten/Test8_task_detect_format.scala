package fasten

import pt.inescn.etl.stream.Load._
import pt.inescn.etl.stream.RabbitMQ.RabbitConnection
import pt.inescn.app.RabbitConfigs.{ReceiveAppConfig, ReceiveAppConfigHelp}
import pt.inescn.utils.ADWError
import produtech.ReceiveApp2.makeFrame

import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration

import produtech.DemoDetectIndVars.taskRangeCheck
import scala.concurrent.ExecutionContext.Implicits.global

import java.text.SimpleDateFormat
import java.util.Calendar



/**
Independent variables call:
  root/runMain fasten.Test8_task_detect_format -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -publishQueue outbox/ba6a6169-8fe1-4b23-8e4b-bfc65da81549/alarms
  root/runMain fasten.Test8_task_detect_format -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -publishQueue outbox/ba6a6169-8fe1-4b23-8e4b-bfc65da81549/alarms
  */

object Test8_task_detect_format {

  def taskDectectFormat(f:Frame,
                        conn: RabbitConnection,conf: ReceiveAppConfig,
                        queueName:String
                       ) : Either[ADWError, Frame] = {

    /*def checkValues(row: Row): Either[ADWError, Row] = {
      val v = conn.varID
      val timestamp = conn.varTs
      val v_varVal = unpack[Double](row(v))
      val v_timestamp = unpack[String](row(timestamp))

//      val torf_var = v_varVal match {
//        case Left(value) => false
//        case Right(value) => true
//      }
//
//      val torf_timestamp = v_timestamp match {
//        case Left(value) => false
//        case Right(value) => true
//      }
//
//      val torf = torf_var && torf_timestamp

      val e = for {
        varTimestamp <- Try(v_timestamp)
        varVal <- Try(v_varVal)

        msg = List(varTimestamp, varVal.toString)

      } yield msg

      msg match {
        case Success(v) =>
          Success(v)
        case Failure(e) =>
      }

    }*/

    def detectValuesFormatBlock(f:Frame
                               ): Either[ADWError, Frame] = {

      val filteredFrame = f.filterWith { r =>
        // get column names
        val cols = r.colNames

        // instead of getting the value individually, as in the case one knows the 2 column ids:
        // val colTS: Either[String, String] = r(conn.varTs)
        // get the values for all columns by mapping
        val row_values = cols.map(rv => r(rv))

        // instead of performing the operation individually, as in the case one knows the 2 column ids:
        // eval = valTS.toString.length > 0 && valTemp.toString.length > 0
        // use fold to run through all vals and accumulate the value of the operation bu folding through the collection
        // true as the null initial param for the logic operation &&

        val eval = row_values.forall(curr => curr.toString.length > 0)

        // If uncommented does not acknowledge the type returned inside eval
        //eval.fold(_ => false, s => s)
        //for debugging purposes:
        println("****************evalEND*************************")
        eval
      }
      println("****************detectValuesEND*************************")
//      Right(filteredFrame)
      Right(f)
    }

    def detectValuesFormatInd(f:Frame
                             ): Either[ADWError, Frame] = {

      println("****************detectValuesFormatInd*************************")

      val filteredFrame = f.filterWith { r =>
        val colTS: Either[String, String] = r(conn.varTs)
        val colTemp: Either[String, String] = r(conn.varID)

        val t = for {
          valTS <- colTS
          valTemp <- colTemp

          eval = valTS.toString.length > 0 && valTemp.toString.length > 0

        } yield eval
        println("****************evalEND*************************")

        t.fold(_ => false, s => s)
      }
      println("****************detectValuesEND*************************")
      Right(filteredFrame)
    }

    // detectValuesFormatInd(f)
    detectValuesFormatBlock(f)

  }

  def printFrame(f:Frame) = {
    println("***********************************frame***********************************")
    f.iterable.toIterator.foreach(println)
  }

  private val format = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")

  def now: String = {
    val now = Calendar.getInstance
    format.format(now.getTime)
  }

  def processData(conf: ReceiveAppConfig, publishQueue: String) = {
    val constate = makeFrame(conf)

    constate match {
      case Left(e) => e
      case Right(cons) =>

        import produtech.DetectFilterFormat.formatFilter

        val newFrame: Either[ADWError, Frame] = for {
//          filterFrame <- taskDectectFormat(cons.frame, cons.conn, conf, publishQueue)
//          nFrame <- publish(filterFrame, cons.conn, conf, publishQueue)
          filterFrame <- formatFilter(cons.frame)
          nFrame <- publish(filterFrame, cons.conn, conf, publishQueue)
        } yield nFrame

//        val newFrame = taskDectectFormat(cons.frame, cons.conn, conf, publishQueue)

        //note that in the task (taskRangeCheck) the flag verbose is checked already;
        //as such this is performed only due to lazy frame processing
        if (conf.numMessages <= 0) {
          newFrame.fold(e => e,  f => f.iterable.foreach( _ => ()))
        }
        else {
          newFrame.fold(e => e,  _.iterable.take(conf.numMessages).foreach( _ => ()))
        }

        cons.stop
    }
  }

  def getFilenames(args: Array[String])= {
    val init: Seq[String] = Seq()
    val remainder: Seq[String] = Seq()
    val indFile = args.indexOf("-filename")
    if (indFile>=0) {
      //check if there is a flag after the filename flag
      val indNextFlag = args.indexWhere(_ contains "-",indFile+1)
      val ubound = if (indNextFlag > indFile) indNextFlag else args.length
      ((indFile+1 until ubound).foldLeft(init) { (acc, cur) =>
        acc :+ args(cur)
      },
        (ubound until args.length).foldLeft(remainder) {(acc,cur) =>
          acc :+ args(cur)
        }
      )
    } else (init,remainder)//(init,-1)

  }

  def publish(f:Frame,
              conn: RabbitConnection,conf: ReceiveAppConfig,
              queueName:String) = {
    println("*****************************************publishtask*****************************************")

    def publishMessage(row: Row): Row = {
      println(row)
      println("**********+++++++++++++")
      val v = conn.varID
      val timestamp = conn.varTs
      val v_varVal = unpack[Double](row(v))
      val v_timestamp = unpack[String](row(timestamp))

      val e = for {
        varTimestamp <- v_timestamp
        varVal <- v_varVal

        data = List(now.toString, conn.tearID , v,
          "p.lbound + - + p.ubound",
          varTimestamp,
          varVal.toString)
      } yield data

      e.fold(
        // print error
        err => if (conf.verbose > 0) println(err),
        { msg =>
          if (msg.nonEmpty) {
            val header = "Send_Timestamp,TearID,VarID,Range,Reading_Timestamp, Observation_value"
            val temp = (header concat "\n" concat msg.mkString(",")).getBytes()
            //msg.mkString(",").getBytes()
            conn.channel.basicPublish("", queueName, null, temp)
            if (conf.verbose > 0) {
              println(s"ALERT:\n $e")
            }
          }
        }
      )
      row
    }

    Right(f.map(publishMessage(_)))

  }

  def main(args: Array[String]): Unit = {

    val (filenames,remainder) = getFilenames(args)
    if (filenames.isEmpty) {
      println("Errors found:\nNo config filepath provided.\nTerminating.")
      return
    }

    val publishQueue = if (args.indexOf("-publishQueue") >= 0) {
      args(args.indexOf("-publishQueue")+1)
    } else {
      println("Errors found:\nNo publish queue provided.\nTerminating.")
      return
    }

    val list = filenames.map{ f =>
      val default = ReceiveAppConfig(f)
      /*
      not assuming -filename is the last flag:
      one cannot assume the relative position of the flag, as such one needs to compose the array to parse in:
      1- args up to -filename flag
      2- appended of the current filename, f
      3- appended of the remainder of the args
      */
      val argsWithFN = args.take(args.indexOf("-filename")+1) :+ f
      val parseArgs = argsWithFN ++ remainder
      // Parse command line parameters
      val recAppConf = default.parse(parseArgs)
      // execute according to command line parameters
      if (recAppConf.error.nonEmpty) {
        val errStr = recAppConf.error.indices.foldLeft(""){(acc,cur) =>
          "\t" + acc.concat(recAppConf.error(cur))
        }
        Left("Errors found:\n" + errStr + "\nTerminating." + "\n Help:\n" + ReceiveAppConfigHelp)
      }
      else {
        val f: Future[Unit] = Future {
          processData(recAppConf, publishQueue)
        }
        Right(f)
      }
    }

    val (errs, futures) = list.partition{
      case Left(_) => true
      case Right(_) => false    //if this case is not exhaustive it yields an exception in the right case
    }

    if (errs.nonEmpty) {
      errs.foreach {case  Left(e) => println(e)}
    }
    else {
      val fut: Seq[Future[Unit]] = futures.map {case Right(n) => n}
      val futs = Future.sequence(fut)

      Await.ready(futs, Duration.Inf)
    }

  }


}
