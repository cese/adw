package fasten





import produtech.ReceiveApp2
import pt.inescn.etl.stream.Load.{Frame, _}
import pt.inescn.app.RabbitConfigs
import pt.inescn.search.stream.Tasks._
import pt.inescn.utils.ADWError

import scala.collection.mutable.ArraySeq
import scala.math.Numeric

/**
  * root/runMain fasten.Test3_tasks
  */

object Test3_tasks {
  /*send configs*/
  def sendMessages(numMessages:Int): Unit = {
    val sendArray:Array[String] = Array("-verbose", "1",
      "-bufferSize", "2",
      "-machineID", "m1",
      "-sensorID", "s1",
      "-numMessages", numMessages.toString,
      "-dir", "../data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv")
    produtech.SendApp2.main(sendArray)
  }

  /*receive configs*/
  val receiveArrayCallback:Array[String] = Array("-verbose", "1",
    "-iter", "CallBack",
    "-numMessages", "6")

  // Start with default and update flag values
  val filename = "src/main/scala/produtech/ReceiveConfig2_Tests"
  val default = RabbitConfigs.ReceiveAppConfig(filename)
  // Parse command line parameters
  val conf:RabbitConfigs.ReceiveAppConfig = default.parse(receiveArrayCallback)
  //conf
  /* /receive configs*/

  /*
  Esta função não funciona como pretendido;
  como a frame é processada como lazy, ao fechar a conecção nesta função, quando for chamada a frame
  e for necessário processar já não tem a conecção para descarregar os dados
  */


  /*def getFrame(): Either[ADWError, Frame]= {
    //based on ReceiveApp.processData
    val connect: Either[ADWError,
      (Frame, pt.inescn.etl.stream.RabbitMQ.RabbitConnection)]
    = ReceiveApp.makeFrame(conf)

    val testFrame: Either[ADWError, Frame] = connect match {
      case Left(e) => Left(e)
      case Right(f) => {
        Right(f._1)
      }
    }
        /** não é possível fazer o stop da ligação logo no primeiro match? */

    connect match {
      case Right(f) => {pt.inescn.etl.stream.RabbitMQ.stop(conf, f._2)}
    }
    testFrame
  }
  */

  def toDouble(f:Frame, colID: String):Frame = {
    f((x:String) => x.toDouble, colID )
  }

  def toDouble(f:Frame, colID: String*):Frame = {
    //colID.foreach{ c => f((x:String) => x.toDouble, c ) }
    //colID.foreach{ f((x:String)=>x.toDouble, _) }

    //f((x:String)=> x.toDouble, colID)


    /*colID.foldLeft(f){(facc, _ ) =>
      f((x:String) => x.toDouble, _)}*/

    (0 until colID.size).foldLeft(f){(facc, j)=>
      facc((x:String)=> x.toDouble, colID(j))
    }
  }

  def task6_mode(f:Frame,  col: ColumnName, colsAux:String*): Either[ADWError, Frame] = {
    val cols = ColumnNames(colsAux)
    def funcName(str: String):String = str + col.col

    val newRow = cols.cols.map(c => funcName(c) -> Val(0.0))

    def mode[T](set: Iterable[T]):Iterable[T] = {
      val grouped = set.groupBy(i => i).map(kv => (kv._1, kv._2.size))    //get a mapping of unique values (keyValues) and respective counting (values).
      val modeValue = grouped.maxBy(_._2)._2                              //get the element with the maximum value (._2 -> map value)
      val modes = grouped.filter(kv => kv._2 == modeValue).map(_._1)      //filter the initial map by the modeValue and return the map keys (ie, the mode values)
      modes
    }

    def op (data:Row): Row = {

      val temp = data.filter(cols.cols: _*).map {case (k,v)=>
        val nk = funcName(k)  //k + data.colNames(c)
        val temp = unpack[ArraySeq[Double]](v)
        val nv = temp match {
        case Right(a) =>
          val arr = a.toArray
          val nv = mode(arr).head
          Val(nv)
          //Val(ArraySeq(nv))
        case _ =>
          ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk->nv
        //val temp = unpack[ArraySeq[Double]](v)
      }
      data ++ temp
    }
    Right(f.map(op, newRow: _*))
  }

  def task7_lfccStep(f:Frame, col:ColumnName, cols:ColumnNames,
                     Fs: Double, NmbFltrs: Int, MaxFrq: Double, MinFrq: Double, NmbCpCoe: Int):
                      Either[ADWError, Frame] = {
    def funcName(str: String): String = str + col.col

    val newRow = cols.cols.map(c => funcName(c) -> Val(ArraySeq(0.0)))

    def op (data:Row): Row = {

      val temp = data.filter(cols.cols: _*).map {case (k,v)=>
        val nk = funcName(k)  //k + data.colNames(c)
        val temp = unpack[ArraySeq[Double]](v)
        val nv = temp match {
          case Right(a) =>
            val arr = a.toArray
            val nv = pt.inescn.features.LFCC(Fs, NmbFltrs, MaxFrq, MinFrq, NmbCpCoe, arr.length).LFCCcomp(arr)
            Val(ArraySeq(nv: _*))
          case _ =>
            ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk->nv
        //val temp = unpack[ArraySeq[Double]](v)
      }
      data ++ temp
    }
    Right(f.map(op, newRow: _*))
  }

  def task8_concatenateStep(f:Frame,  col: ColumnName, cols:ColumnNames): Either[ADWError, Frame] = {
    val newRow = Seq(col.col -> Val(ArraySeq(0.0))) //row typing info

    def op (data:Row): Row = {
      val initVec: ArraySeq[Double] = ArraySeq()

      val finVec = cols.cols.foldLeft(initVec) { (acc, j) =>
        val d = data(j)
        val tmp = unpack[ArraySeq[Double]](d)
        tmp match {
          case Right(v) => acc ++ v
          //case _ => ErrVal(s"Expected a numerical Vector but got: $d")  //type mismatch error
        }
      }

      val tmp = Row(col.col -> Val(finVec))
      data ++ tmp
    }

    Right(f.map(op, newRow: _*))
  }

  /*def task10_median(f:Frame, col:ColumnName, cols:ColumnNames,
                    NmbOfColunms: Int, Stride: Int):
                    Either[ADWError, Frame] = {
    def funcName(str: String):String = str + col.col

    val newRow = cols.cols.map(c => funcName(c) -> Val(0.0))

    def median(set: ArraySeq[Double]):Double = {
      val (low,up) = set.sortWith(_<_).splitAt(set.size/2)
      if (set.size%2 == 0) (low.last + up.head)/2 else up.head
    }

    def op (data:Row): Row = {

      val temp = data.filter(cols.cols: _*).map {case (k,v)=>
        val nk = funcName(k)
        //val temp = unpack[ArraySeq[Double]](v)
        val temp = unpack[ArraySeq[ArraySeq[Double]]](v)
        val nv = temp match {
          case Right(a) =>
            //var smthLfcc = ArraySeq.fill(NmbOfColunms)(0.0)
            //var tmp1 = ArraySeq.fill(Stride)(0.0)
            val initCol:ArraySeq[Double] = ArraySeq()
            val initStr:ArraySeq[Double] = ArraySeq()

            def strideIter = {
              (0 to Stride).foldLeft(initStr){(accStr, j) =>
                accStr ++ a(j)(k)

                /*
                C:\Users\cese\IdeaProjects\adw\core\src\main\scala\fasten\Test3_tasks.scala:198:31: Double does not take parameters
                [error]                 accStr ++ a(j)(k)
                [error]                               ^
                */

              }
            }

            val smthLfcc = (0 to NmbOfColunms).foldLeft(initCol){(accCol,k)=>
              accCol ++ Seq(median(strideIter))
              }

            val nv = Val(smthLfcc)
            nv
          //Val(ArraySeq(nv))
          case _ =>
            ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk->nv
        //val temp = unpack[ArraySeq[Double]](v)
      }
      data ++ temp
    }

    Right(f.map(op, newRow: _*))
  }*/

  def printFrame(f:Frame)= {
    /*
        Comandos com erro:
        f.foreach(println)
        f.iterable.foreach(println)
        f.iterable.toIterator.foreach(println)
        */
    f.iterable.take(conf.numMessages).foreach(println)
  }

  def main(args: Array[String]): Unit = {
    //val sendMessagesN: Int = 10
    //sendMessages(sendMessagesN)

    val testFrame = ReceiveApp2.makeFrame(conf)
    println("frame created")
    testFrame match {
      case Left(e) => println("erro \n" + e.msg)
      case Right(con) =>
        /**task1_renameColumns*/

        //con.frame.iterable.foreach(println)

        //val renamedFrame1: Frame = task1_renameColumns(f)
        val renamedFrame: Frame = con.frame.renameCols("Time(s)" -> "t","damage" -> "isanomaly")  //works but changes column order
        //printFrame(renamedFrame)

        /**task2_convertLabelValue*/
        val expr:String = "norm"
        val colID: String = "isanomaly"
        val convFrame1: Frame =
          //convFrame1((x:String) => x.toBoolean, "isanomaly")
          renamedFrame((x:String) => if (x.toLowerCase == expr) false else true, colID)
        //println("convFrame1.colTypes: " + convFrame1.colTypes)

        /**task3_boolToDouble*/
        val convFrame2:Frame =  convFrame1((x:Boolean)=> if (x) 1.0 else 0.0, colID )
        //println("convFrame2.colTypes: " + convFrame2.colTypes)

        /**task4_toDouble*/
        val convFrame3: Frame = toDouble(convFrame2 ,"t", "X", "Y", "Z")
        //println("convFrame3.colTypes: " + convFrame3.colTypes)

        /**task5_window*/
        val ssize:Int = 1
        val step:Int = 1
        val windowedFrame: Frame = convFrame3.window(ssize, step)
        //windowedFrame.iterable.toIterator.take(conf.numMessages).foreach(println)

        /***task6_mode*/
        val cols = "isanomaly"
        val extend = "mode"
        val modedFrame:Either[ADWError, Frame] = task6_mode(windowedFrame, ColumnName(extend), cols)
        modedFrame match {
          case Right(f) =>
            println("********************************************************modedFrame********************************************************")
            printFrame(f)
        }
/*
        /***task7_lfccStep*/
        val colsLFCC: ColumnNames = ColumnNames(Seq("X", "Y", "Z"))
        val extendLFCC = "lfcc"
        val Fs = 5100
        val NmbFltrs = 5
        val MaxFrq = 2550.0
        val MinFrq = 10.0
        val NmbCpCoe = 5

        val lfccFrame = modedFrame match {
          case Right(f) =>
            val lfccFrame: Either[ADWError, Frame] = task7_lfccStep(f, ColumnName(extendLFCC), colsLFCC,
              Fs, NmbFltrs, MaxFrq, MinFrq, NmbCpCoe)
            /*lfccFrame match {
              case Right(f2) =>
                f2.iterable.toIterator.take(conf.numMessages).foreach(println)
            }*/
            lfccFrame
        }

        /***task8_concatenateStep*/
        val extendConc = ColumnName("lfcc")
        val colsConc = ColumnNames(Seq("Xlfcc","Ylfcc","Zlfcc"))

        val concFrame: Either[ADWError, Frame] = lfccFrame match {
          case Right(f) =>
            val concFrame = task8_concatenateStep(f, extendConc, colsConc)
            concFrame match {
              case Right (f2) =>
                f2.iterable.toIterator.take(conf.numMessages).foreach(println)
            }
            concFrame
        }

        /**task9_window*/
        val FrmSz2:Int = 128
        val FrmStp2:Int = 1
        val windowedFrame2:Frame = concFrame match {
          case Right(f) => f.window(FrmSz2, FrmStp2)
        }

        /**task10_median*/

        /**task11_mode*/
        /**task12_anomalyDetectionTrainStep*/
        /**task11_anomDetectionEvalStep*/
        */

        pt.inescn.etl.stream.RabbitMQ.stop(conf, con.conn)
    }
  }
}