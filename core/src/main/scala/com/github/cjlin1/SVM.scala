/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package com.github.cjlin1

import java.io
import java.io.{InputStream, PrintStream}

import com.github.cjlin1.utils.System
import java.nio.file.Paths
import java.util.UUID

import better.files.File
import pt.inescn.eval.BinaryClassificationMetrics
import pt.inescn.models.Base.AlgorithmState.{Clean, Clean_or_ModelRecorded, ModelRecorded, ModelRecorded_or_Predicted, Predicted}
import pt.inescn.models.Base.AlgorithmType.{BinaryClassification, OneClass, Regression}
import pt.inescn.etl.Base.ScalerState
import pt.inescn.etl.Base.ScalerState.{RangeRecorded, RangeUsed, Unscaled}
import pt.inescn.models.Base._

// Hiding does not work
//import utils.{System =>_ }
import java.lang.{System => JSystem }

/**
  * Wrapper for libSVM's  commands. Allows us to script the command lines for use within a Java or Scala system.
  * The data files must be in lightSVM's sparse format. We generate and pass the command line directly. So all
  * libSVM checks are made. The output from the standard and error outputs are redirected to temporary files.
  * Check those to get any additional information on possible errors or warnings. The files are created and stored
  * in the system's temporary directory using unique names.
  *
  * Created by hmf on 12-07-2017.
  *
  * We are interested in the one-class classification used in anomaly detection. The corresponding command line is:
  * -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
  *
  * Some references on one-class SVM classification:
  *
  * B. Schölkopf, A. Smola, R. Williamson, and Parameters. L. Bartlett. New support vector algorithms. Neural Computation, 12,
  * 2000, 1207-1245.
  * B. Schölkopf, J. Platt, J. Shawe-Taylor, A. J. Smola, and R. C. Williamson. Estimating the support of a
  * high-dimensional distribution. Neural Computation, 13, 2001, 1443-1471.
  *
  * (Plane)
  * Schölkopf, Bernhard; Williamson, Robert C; Smola, Alex J; Shawe-Taylor, John; Platt, John C; Support vector method
  * for novelty detection, Advances in neural information processing systems, 582-588, 2000
  * (Sphere)
  * Tax, David MJ, Duin, Robert PW; Support vector data description, Machine learning vol 54, issue 1, pages 45-66,
  * Kluwer Academic Publishers-Plenum Publishers, 2004
  *
  * // TODO: automate update of latest source from Github
  *
  * https://stackoverflow.com/questions/3228427/redirect-system-out-println
  *
  * @see https://github.com/cjlin1/libsvm
  * @see http://elki.dbs.ifi.lmu.de/browser/elki/addons/libsvm/src/main/java/de/lmu/ifi/dbs/elki/algorithm/outlier/svm/LibSVMOneClassOutlierDetection.java
  * @see https://github.com/yuemingl/SymJava/blob/master/src/symjava/examples/SVM.java
  * @see https://github.com/tfahub/fluent-libsvm
  * @see https://github.com/rvlasveld/oc_svm
  * @see http://rvlasveld.github.io/blog/2013/07/12/introduction-to-one-class-support-vector-machines/
  * @see http://activisiongamescience.github.io/2015/12/23/Unsupervised-Anomaly-Detection-SOD-vs-One-class-SVM/
  * @see https://github.com/haifengl/smile/blob/master/core/src/main/java/smile/classification/SVM.java
  *
  * "One-class classification" thesis written by door David Martinus Johannes TAX
  * @see http://prlab.tudelft.nl/content/one-class-classification-1)
  *
  * Data-sets
  * @see https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/
  * @see http://homepage.tudelft.nl/n9d04/occ/index.html
  * @see https://pdfs.semanticscholar.org/7889/1cbf6f84848e79af4a2802e5b3f0dd6fa301.pdf
  *
  * General
  * Christopher J. C. Burges. A Tutorial on Support Vector Machines for Pattern Recognition. Data Mining and Knowledge Discovery 2:121-167, 1998.
  * John Platt. Sequential Minimal Optimization: A Fast Algorithm for Training Support Vector Machines.
  * Rong-En Fan, Pai-Hsuen, and Chih-Jen Lin. Working Set Selection Using Second Order Information for Training Support Vector Machines. JMLR, 6:1889-1918, 2005.
  * Antoine Bordes, Seyda Ertekin, Jason Weston and Leon Bottou. Fast Kernel Classifiers with Online and Active Learning, Journal of Machine Learning Research, 6:1579-1619, 2005.
  * Tobias Glasmachers and Christian Igel. Second Order SMO Improves SVM Online and Active Learning.
  * Chih-Chung Chang and Chih-Jen Lin. LIBSVM: a Library for Support Vector Machines.
  * https://en.wikipedia.org/wiki/Platt_scaling
  * http://leon.bottou.org/projects/lasvm
  */
object SVM {

  /**
    * LibSVM makes provides command lines for scaling, learning and predicting.
    * This is done via a `run` command with this signature.
    */
  type SVMRun = Array[String] => Unit

  // https://blog.codecentric.de/en/2016/02/phantom-types-scala/
  // http://typelevel.org/blog/2016/09/19/variance-phantom.html
  /*
    * Avoid concurrency problems. Either block as we do here
    * or create a new object manually at the call site.
    *
  object Scale {
    def apply(): Scale[ScalerState.Unscaled] = this.synchronized( new Scale() )
  }*/

  /**
    * Wrapper for libSVM's `svm_scale`. It allows you to scale both the features (independent variables)
    * and the output (dependent variable). When we scale a first file, the scaling range is recorded. We
    * assume this is the training data. In a second phase you can rescale the testing data. This step automatically
    * reloads the previously saved range data to obtain the scaling range bounds/limits.
    *
    * @param trainDataFile - training data
    * @param testDataFile - test data
    * @param xLower - feature's lower scaling range bound
    * @param xUpper - feature's upper scaling range bound
    * @param yLower - output's lower scaling range bound
    * @param yUpper - output's upper scaling range bound
    * @param outDir - directory were the rage information is stored. If this is empty, the systems temporary
    *               directory is used
    * @param rangeDataFile - this is automatically set to a file that goes to `outDir` and whose name is a unique
    *                      name with no extension (starts with "SVMScaleRange").
    * @tparam S - holds a state so tha we must first scale the taring data and only then can we rescale the test data
    *           using the range information.
    * @see [[SVM.Train!]]
    */
  case class Scale[S <: ScalerState]
  (trainDataFile: String = "train", testDataFile: String = "test",
   xLower: Double = Double.NaN, xUpper: Double = Double.NaN,
   yLower: Double = Double.NaN, yUpper: Double = Double.NaN,
   outDir: String = "", rangeDataFile: String = "") {

    // TODO: LibSVM bug (see https://github.com/cjlin1/libsvm/issues/100)
    // https://stackoverflow.com/questions/5236056/force-point-as-decimal-separator-in-java
    import java.util.Locale
    Locale.setDefault(new Locale("en", "US"))

    private val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    private val tmpOut = if (outDir == "") tmp else File(outDir)


    /**
      * When the (training) data is scaled, the range information can be stored. This range information
      * can later be read and used to scale the testing data. For cross-validation purposes, this range
      * is a temporary artifact. This function generates a unique file name to store this possibly
      * temporary information.
      *
      * @param tmp - directory wherein the file is created.
      * @return
      */
    def temporaryDataFilename(tmp: File): String = {
      val uuid = this.synchronized(UUID.randomUUID.toString)
      Paths.get(tmp.toString, "SVMScaleRange_" + uuid).toString
    }

    /**
      * Automatically generates the scaled data file name based
      * on an input data file name. We also add an "id". This
      * id is required so that the test suites, that are executed
      * in parallel, do not overwrite each others files.
      *
      * @param trainFile
      * @param id
      * @return
      */
    def scaledDataFile(trainFile: String, id: String): String = {
      val ttmp = File(trainFile)
      val idExt = if (id.trim != "") "." + id.trim else ""
      (tmpOut / (ttmp.path.getFileName + ".scaled" + idExt)).toString
    }

    private def limitSet(param: String, v: Double): List[String] = if (v.isNaN) List() else List(param, v.toString)
    private def limit2Set(param: String, v1: Double, v2: Double): List[String] = if (v1.isNaN || v2.isNaN) List() else List(param, v1.toString, v2.toString)
    private def limitArgs: List[String] = limitSet("-l", xLower) ++ limitSet("-u", xUpper) ++ limit2Set("-y", yLower, yUpper)
    private def scaleTrainArgs(rangeData: String, trainData: File): List[String] = limitArgs ++ List("-s", rangeData) ++ List(trainData.toString)
    private def scaleTestArgs(trainData: File): List[String] = limitArgs ++ List("-r", rangeDataFile) ++ List(trainData.toString)
    private def trainOut(trainData: String, testData: String) = trainData
    private def testOut(trainData: String, testData: String) = testData

    def xLowerTo[T >: S <: Unscaled](newXLower: Double): Scale[Unscaled] = Scale(trainDataFile, testDataFile, newXLower, xUpper, yLower, yUpper, outDir, rangeDataFile)
    def xUpperTo[T >: S <: Unscaled](newXUpper: Double): Scale[Unscaled] = Scale(trainDataFile, testDataFile, xLower, newXUpper, yLower, yUpper, outDir, rangeDataFile)
    def yLowerTo[T >: S <: Unscaled](newYLower: Double): Scale[Unscaled] = Scale(trainDataFile, testDataFile, xLower, xUpper, newYLower, yUpper, outDir, rangeDataFile)
    def yUpperTo[T >: S <: Unscaled](newYUpper: Double): Scale[Unscaled] = Scale(trainDataFile, testDataFile, xLower, xUpper, yLower, newYUpper, outDir, rangeDataFile)
    def outDirTo[T >: S <: Unscaled](newOut: File): Scale[Unscaled] = Scale(trainDataFile, testDataFile, xLower, xUpper, yLower, yUpper, newOut.toString, rangeDataFile)

    // For printing/testing
    def trainArgs: List[String] = limitArgs ++ List("-s", rangeDataFile) ++ List(trainDataFile)
    def testArgs: List[String] = limitArgs ++ List("-r", rangeDataFile) ++ List(trainDataFile)


    // https://stackoverflow.com/questions/10015182/in-a-multithreaded-java-program-does-each-thread-have-its-own-copy-of-system-ou
    // https://stackoverflow.com/questions/25178969/redirect-system-out-from-every-thread
    // https://www.javaadvent.com/2014/12/thread-local-storage-in-java.html
    // http://tutorials.jenkov.com/java-concurrency/threadlocal.html
    private def run[T <: ScalerState](commandLine: List[String], trainData: String, testData: String, selectOut: (String, String) => String, newRangeFile: String, id: String): Either[String, Scale[T]] = {
      // Bypass stdio and stderr
      val file: String = scaledDataFile(selectOut(trainData, testData), id)
      val scaledData: PrintStream = new java.io.PrintStream(file)
      val errorsTmp: io.File = File.newTemporaryFile(prefix = "SVMError", parent = Some(tmpOut)).toJava
      val errors: PrintStream = new java.io.PrintStream(errorsTmp)
      /* TODO: remove
      val std = JSystem.out
      val err = JSystem.err
      */
      val fileIn = tmpOut / "empty.in"
      fileIn.createIfNotExists()
      val in: InputStream = fileIn.newInputStream

      val sys = new System(in, scaledData, errors)
      try {
        /* TODO: remove
        JSystem.setOut(scaledData)
        JSystem.setErr(errors)
        */
        val scale_data = new svm_scale(sys)
        scale_data.run(commandLine.toArray)
        //JSystem.setSecurityManager(old)
        errors.close()
        val msg = File(errorsTmp.toPath).contentAsString
        msg match {
          case "" => Right(Scale(trainData, testData, xLower, xUpper, yLower, yUpper, outDir, newRangeFile))
          case _ => Left("WARNING:" + msg)
        }
      } catch {
        case e: Throwable => Left("ERROR:" + e.getMessage)
      } finally {
        //JSystem.setSecurityManager(old)
        scaledData.close()
        errors.close()
        /* TODO: remove
        JSystem.setOut(std)
        JSystem.setErr(err)
        */
        sys.close()
      }
    }

    def scale[T >: S <: Unscaled](trainFile: File, id: String = ""): Either[String, Scale[RangeRecorded]] = {
      val range = temporaryDataFilename(tmpOut) // new range file
      val commandLine = scaleTrainArgs(range, trainFile)
      run(commandLine, trainFile.toString, testDataFile, trainOut, range, id)
    }

    def reScale[T >: S <: RangeRecorded](testFile: File, id: String = ""): Either[String, Scale[RangeUsed]] = {
      val commandLine = scaleTestArgs(testFile)
      run(commandLine, trainDataFile, testFile.toString, testOut, rangeDataFile, id)
    }

    def scaledData[T >: S <: RangeRecorded](id: String = "") : File = File( scaledDataFile(trainDataFile, id) )
    def reScaledData[T >: S <: RangeUsed](id: String = "") : File = File( scaledDataFile(testDataFile, id) )
  }


  sealed trait Params {
    /** generate a command argument for a given parameter */
    def convert[T](command: String, value : T)(implicit check : IsDefault[T]) : List[String] =
      if (check(value)) List() else List(command, value.toString)
    /** command line parameters */
    def args : List[String]
  }

  /* Default values for parameters  */
  val defaultD: Double = Double.NaN
  val defaultI: Int = Integer.MIN_VALUE
  val defaultB = false
  val defaultType: SVM.SVMAlgorithm[BinaryClassification] = DefaultAlgorithm()
  val defaultKernel: SVM.Kernel = DefaultKernel()

  /**
    * Check if a parameter value is a default. In such cases the parameter does need to
    * be passed onto the algorithm functions.
    *
    * @see https://users.scala-lang.org/t/enforcing-type-equality-bound-on-implicit-type-parameter/1524
    * @see https://users.scala-lang.org/t/how-can-we-override-shadow-predef-conforms/1142/2
    * @see   // https://issues.scala-lang.org/browse/SI-2811
    *
    * @param test - test function used to check the value
    * @tparam A - type of value to be checked
    */
  final case class IsDefault[-A](test: A => Boolean) extends AnyVal {
    def apply(v:A): Boolean = test(v)
  }

  implicit val isDefaultDouble: IsDefault[Double] = IsDefault(_.isNaN)
  implicit val isDefaultInt: IsDefault[Int] = IsDefault(_ == defaultI)
  implicit val isDefaultBool: IsDefault[Boolean] = IsDefault(_ == defaultB)
  implicit val isDefaultKernel: IsDefault[SVM.Kernel] = IsDefault(_.isInstanceOf[DefaultKernel])
  implicit val isDefaultAlg: IsDefault[SVMAlgorithm[_]] = IsDefault(_.isInstanceOf[DefaultAlgorithm])

  type Weights = List[(Int, Double)]

  /**
    * All extending classes are used tio identify and parameterize a specific libSVM problem encoding.
    * LibSMV provides 3 types of encodings: classification, regression and one-class classification
    * Its command argument parameters are:
    * -s svm_type : set type of SVM (default 0)
    *   0 -- C-SVC		(multi-class classification)
    *   1 -- nu-SVC		(multi-class classification)
    *   2 -- one-class SVM
    *   3 -- epsilon-SVR	(regression)
    *   4 -- nu-SVR		(regression)
    *   -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
    *   -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
    *   -p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
    *   -wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)
    */
  sealed trait SVMAlgorithm[S <: AlgorithmType] extends Params {
    val type_ : List[String]
    def addAlgorithm[T <: AlgorithmType](value : SVMAlgorithm[T])(implicit check : IsDefault[SVMAlgorithm[T]]) : List[String] =
      if (check(value)) List() else value.args
    def addKernel(value : Kernel)(implicit check : IsDefault[Kernel]) : List[String] =
      if (check(value)) List() else value.args
  }

  /**
    * Contains a set of parameters that are global to all of the SVM algorithm. This includes the
    * optimization algorithm used to identify the support vectors.
    *
    * @param cacheSize -m cachesize : set cache memory size in MB (default 100)
    * @param epsilon -e epsilon : set tolerance of termination criterion (default 0.001)
    * @param shrinking -h shrinking : whether to use the shrinking heuristics, 0 or 1 (default 1)
    * @param probabilityEstimates -b probability_estimates : whether to train a SVC or SVR model for probability estimates, 0 or 1 (default 0)
    * @param v -v n: n-fold cross validation mode
    * @param q -q : quiet mode (no outputs)
    */
  case class TrainParameters[T <: AlgorithmType](
                                                  cacheSize: Int = defaultI, epsilon: Double = defaultD,
                                                  shrinking: Boolean = defaultB, probabilityEstimates: Boolean = defaultB,
                                                  v: Int = defaultI, q: Boolean = defaultB,
                                                  algorithm: SVMAlgorithm[T] = DefaultAlgorithm(),
                                                  kernel: Kernel=DefaultKernel()) extends SVMAlgorithm[T] {

    val type_ = List()
    val cache_ : List[String] = convert( "-m", cacheSize)
    val epsilon_ : List[String] = convert("-e", epsilon)
    val shrinking_ : List[String] = convert("-h", shrinking)
    val probabilityEstimates_ : List[String] =  convert("-b", probabilityEstimates)
    val v_ : List[String] = convert("-v", v)
    val q_ : List[String] = convert("-q", q)
    val a_ : List[String] = addAlgorithm(algorithm)(isDefaultAlg)
    val k_ : List[String] = addKernel(kernel)

    def args: List[String] = a_ ++ k_ ++ cache_ ++ epsilon_ ++ shrinking_ ++ probabilityEstimates_ ++ v_ ++ q_
  }

  /**
    * Represents the default parameter for the Algorithm. When this is used, no command line
    * parameters are generated for the algorithm.
    */
  case class DefaultAlgorithm() extends SVMAlgorithm[BinaryClassification] {
    override val type_ : List[String] = List()
    override def args: List[String] = List()
  }

  /**
    * Select the C-SVC classification algorithm and its parameters.
    *
    * @param c : -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
    * @param weights: -wi weight : set the parameter C of class i to weight*C, for C-SVC (default 1)
    */
  case class CSVC(c: Double = defaultD, weights : Weights = List()) extends SVMAlgorithm[BinaryClassification] {
    override val type_ = List("-s", "0")
    val c_ : List[String] = convert( "-c", c)
    val weights_ : List[String] = weights.flatMap(p => List("-w"+p._1.toString, p._2.toString))
    def args: List[String] = type_ ++ c_ ++ weights_
  }

  /**
    * Select the nu-SVC classification algorithm and its parameters.
    *
    * @param nu : -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
    */
  case class NuSVC(nu: Double = defaultD) extends SVMAlgorithm[BinaryClassification] {
    override val type_ = List("-s", "1")
    val nu_ : List[String] = convert("-n", nu)
    override def args: List[String] = type_ ++ nu_
  }

  /**
    * Select the one-class classification algorithm and its parameters.
    *
    * @param nu : -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
    */
  case class oneClass(nu: Double = defaultD) extends SVMAlgorithm[OneClass] {
    override val type_ = List("-s", "2")
    val nu_ : List[String] = convert("-n", nu)
    override def args: List[String] = type_ ++ nu_
  }

  /**
    * Select the epsilon-SVR regression algorithm and its parameters.
    *
    * @param c : -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
    * @param epsilon : -p epsilon : set the epsilon in loss function of epsilon-SVR (default 0.1)
    */
  case class epsilonSVR(c: Double = defaultD, epsilon: Double = defaultD) extends SVMAlgorithm[Regression] {
    override val type_ = List("-s", "3")
    val c_ : List[String] = convert( "-c", c)
    val epsilon_ : List[String] = convert( "-p", epsilon)
    override def args: List[String] = type_ ++ c_ ++ epsilon_
  }

  /**
    * Select the nu-SVR regression algorithm and its parameters.
    *
    * @param c : -c cost : set the parameter C of C-SVC, epsilon-SVR, and nu-SVR (default 1)
    * @param nu : -n nu : set the parameter nu of nu-SVC, one-class SVM, and nu-SVR (default 0.5)
    */
  case class NuSVR(c: Double = defaultD, nu: Double = defaultD) extends SVMAlgorithm[Regression] {
    override val type_ = List("-s", "4")
    val c_ : List[String] = convert( "-c", c)
    val nu_ : List[String] = convert("-n", nu)
    override def args: List[String] = type_ ++ c_ ++ nu_
  }

  /**
    * Represents the libSVM's available kernels.
    * As of 17/7/2017 libSVM has:
    * LINEAR:	u'*v
    * POLY:	(gamma*u'*v + coef0)^degree
    * RBF:	exp(-gamma*|u-v|^2)
    * SIGMOID:	tanh(gamma*u'*v + coef0)
    * PRECOMPUTED: kernel values in training_set_file
    *
    * The command line information:
    * -t kernel_type : set type of kernel function (default 2)
    *   0 -- linear: u'*v
    *   1 -- polynomial: (gamma*u'*v + coef0)^degree
    *   2 -- radial basis function: exp(-gamma*|u-v|^2)
    *   3 -- sigmoid: tanh(gamma*u'*v + coef0)
    *   4 -- precomputed kernel (kernel values in training_set_file)
    */
  sealed trait Kernel {
    /** "-t" command line parameter */
    val type_ : List[String]
    /** generate a command argument for a given parameter */
    def convert[T](command: String, value : T)(implicit check : IsDefault[T]) : List[String] =
      if (check(value)) List() else List(command, value.toString)
    /** command line parameters */
    def args : List[String]
  }

  case class DefaultKernel() extends Kernel {
    override val type_ : List[String] = List()
    override def args: List[String] = List()
  }

  /**
    * use a linear kernel
    */
  case class Linear() extends Kernel {
    override val type_ : List[String] = List("-t", "0")
    override def args: List[String] = type_
  }
  /**
    * Use a polynomial
    * @param degree set degree in kernel function (default 3, -d)
    * @param gamma set gamma in kernel function (default 1/num_features, -g)
    * @param coef0 set coef0 in kernel function (default 0, -r)
    */
  case class Poly(degree : Int = defaultI, gamma : Double = defaultD, coef0 : Double = defaultD) extends Kernel {
    override val type_ : List[String] = List("-t", "1")
    val degree_ : List[String] = convert("-d", degree)
    val gamma_ : List[String] = convert("-g", gamma)
    val coef0_ : List[String] = convert("-r", coef0)
    override def args: List[String] = type_ ++ degree_ ++ gamma_ ++ coef0_
  }
  /**
    * Use a Radial Basis Function
    * @param gamma set gamma in kernel function (default 1/num_features, -g)
    */
  case class RBF(gamma : Double = defaultD) extends Kernel {
    override val type_ : List[String] = List("-t", "2")
    val gamma_ : List[String] = convert("-d", gamma)
    override def args: List[String] = type_ ++ gamma_
  }
  /**
    * Use a Sigmoid function
    * @param gamma set gamma in kernel function (default 1/num_features, -g)
    * @param coef0 set coef0 in kernel function (default 0, -r)
    */
  case class Sigmoid(gamma : Double = defaultD, coef0 : Double = defaultD) extends Kernel {
    override val type_ : List[String] = List("-t", "3")
    val gamma_ : List[String] = convert("-d", gamma)
    val coef0_ : List[String] = convert("-d", coef0)
    override def args: List[String] = type_ ++ gamma_ ++ coef0_
  }

  /**
    * Use precomputed kernel
    */
  case class Precomputed() extends Kernel {
    override val type_ : List[String] = List("-t", "4")
    override def args: List[String] = type_
  }

  /**
    * libSVM's wrapper has the following parameters for the `svm_predict` command.
    *   -b probability_estimates: whether to predict probability estimates, 0 or 1 (default 0); one-class SVM not supported yet
    *   -q : quiet mode (no outputs)
    *
    * @param probabilityEstimates - indicate if we want to return the probability of a class
    * @param q - quite or verbose output
    */
  case class PredictParameters(probabilityEstimates: Boolean = defaultB, q: Boolean = defaultB ) extends Params {

    val type_ = List()
    val probabilityEstimates_ : List[String] =  convert("-b", probabilityEstimates)
    val q_ : List[String] = convert("-q", q)

    def args: List[String] = probabilityEstimates_ ++ q_
  }

  /*
  object Train {
    def apply(): Train[Clean, Unknown] = new Train()
  }*/

  /**
    * This is a libSVM wrapper for training. It generates the sommand line arguments that allow one to set
    * general parameters (such as cache size), the algorithm used (classification or regression) and the kernel
    * (such as linear or polynomial). For more information, see the related classes.
    *
    * @param trainDataFile - training data file in lightSVM's spare format
    * @param modelDataFile - trained model will be stored in this file. If not name is given then a model file that
    *                  has the same name as the training data but the extension ".mode", is placed in the `outDir`
    * @param outDir - output directory. By default it is the system's temporary directory.
    * @tparam S - One can only predict when a model has been trained
    * @see [[SVM.SVMAlgorithm]], [[SVM.Kernel]], [[SVM.TrainParameters]], [[SVM.Scale!]]
    */
  case class Train[S <: AlgorithmState, U <: AlgorithmType]
  (trainDataFile: String = "train",
   modelDataFile: String = "model",
   testDataFile: String = "train",
   outDir: String = "",
   out: String = "") {


    private val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    private val tmpOut = if (outDir == "") tmp else File(outDir)
    private def snake(id:String) = {
      val tid = id.trim
      if (tid == "") tid else "_" + id
    } // snake case versus camel case

    //private def temporaryFilename(dir: File, in: File): String = Paths.get(dir.toString, in.toString +  "_" + UUID.randomUUID.toString).toString
    def modelFile(id:String, in: File) : File = Paths.get(tmpOut.toString, in.path.getFileName + snake(id) +  ".model")
    def predictFile(id:String, in: File) : File = Paths.get(tmpOut.toString, in.path.getFileName +  snake(id) + ".predict")
    def callPredict(sys: System, commandLine: List[String]): Unit = new svm_predict(sys).run(commandLine.toArray)
    def callTrain(sys: System, commandLine: List[String]): Unit = new svm_train(sys).run(commandLine.toArray)
    def updateInformation[T <: AlgorithmState, V <: AlgorithmType]
    (train: String, model: String, test: String, outdir: String, out : String): Train[T, V] = Train[T, V](train, model, test, outdir, out)

    def run[T<: AlgorithmState, V <: AlgorithmType]
    ( command : (System, List[String]) => Unit,
      commandLine: List[String],
      train: String,
      model: String,
      test: String): Either[String, Train[T,V]] = {

      val stdTmp = File.newTemporaryFile(prefix = "SVMStd", parent = Some(tmpOut)).toJava
      val errorsTmp = File.newTemporaryFile(prefix = "SVMError", parent = Some(tmpOut)).toJava
      val errors = new java.io.PrintStream(errorsTmp)
      val stds = new java.io.PrintStream(stdTmp)
      /*TODO: remove
      val std = JSystem.out
      val err = JSystem.err
      */
      val fileIn = tmp / "empty.in"
      fileIn.createIfNotExists()
      val in: InputStream = fileIn.newInputStream
      val sys = new System(in, stds, errors)
      try {
        /* TODO: remove
        JSystem.setOut(stds)
        JSystem.setErr(errors)
        */
        command(sys, commandLine)
        errors.close()
        val msg = File(errorsTmp.toPath).contentAsString
        msg match {
          case "" => Right(updateInformation(train, model, test, outDir, stdTmp.toString))
          case _ => Left("WARNING:" + msg)
        }
      } catch {
        case e: Throwable => Left("ERROR:" + e.getMessage)
      } finally {
        stds.close()
        errors.close()
        /* TODO: remove
        JSystem.setOut(std)
        JSystem.setErr(err)*/
        sys.close()
      }
    }

    def train[T >: S <: Clean, V <: AlgorithmType](params: TrainParameters[V], trainFile: File, modelFile: File): Either[String, Train[ModelRecorded,V]] = {
      val commandLine = params.args :+ trainFile.toString :+ modelFile.toString
      val result: Either[String, Train[ModelRecorded,V]] = run(callTrain, commandLine, trainFile.toString, modelFile.toString, testDataFile )
      result
    }

    def train[T >: S <: Clean, V <: AlgorithmType](params: TrainParameters[V], trainFile: File): Either[String, Train[ModelRecorded, V]] = {
      val model : File = modelFile("",trainFile)
      train[T,V](params, trainFile, model)
    }

    def getModel[T >: S <: ModelRecorded]: String = modelDataFile

    private def predict[T >: S <: AlgorithmState, V <: AlgorithmType](params: PredictParameters, modelFileName: String, testFile: File, predictFile: File): Either[String, Train[Predicted,V]] = {
      val commandLine = params.args ++ List(testFile.toString, modelFileName, predictFile.toString)
      //println(commandLine)
      run(callPredict, commandLine, trainDataFile, modelDataFile, testFile.toString)
    }

    def predict[T >: S <: Clean_or_ModelRecorded, V <: AlgorithmType](params: PredictParameters, modelFileName: String, testFile: File): Either[String, Train[Predicted,U]] = {
      val prediction : File = predictFile("",testFile)
      predict[T,U](params, modelFileName, testFile, prediction)
    }

    def predict[T >: S <: ModelRecorded, V <: AlgorithmType](params: PredictParameters, testFile: File): Either[String, Train[Predicted,U]] = {
      val prediction : File = predictFile("",testFile)
      predict[T,U](params, modelDataFile, testFile, prediction)
    }

    def getBoolMetrics[T >: S <: Predicted, V >: U <: BinaryClassification] : BinaryClassificationMetrics = {
      val prediction : File = predictFile("",File(testDataFile))
      val predictions: Seq[Boolean] = prediction.lineIterator.toSeq.map(p => if (p.toDouble > 0.0 ) true else false)
      val label = File(testDataFile)
      val labels = label.lineIterator.toSeq.map {
        p => val label = p.split(" ")(0).trim
          if (label.toDouble > 0.0 ) true else false }
      BinaryClassificationMetrics(labels, predictions)
    }

    /*
      Utility functions used for testing
     */

    /**
      * Get the last line from the output file generated by libSVM.
      * @param outFileName - output file name
      * @param separator - string separator of output data
      * @return last line split into trimmed tokens
      */
    def getLastLine(outFileName : String, separator : String = "="): Array[String] = {
      val data = File(outFileName)
      val lastLine = data.lines.last
      lastLine.split(separator).map( _.trim )
    }

    /**
      * When doing classification, get simple classification metric. The metric used
      * is in part 1 (example accuracy) and part 2 has the actual value. Part 2 is in
      * percent but is converted into a fraction.
      */
    def getClassificationAccuracy[T >: S <: ModelRecorded_or_Predicted, V >: U <: AlgorithmType] : (String, Double) = {
      val parts = getLastLine(out)
      val part1 = parts.head
      val part2 = parts(1).trim
      val n = part2.indexOf("%")
      val part2Num = part2.dropRight(part2.length - n)
      (part1, part2Num.toDouble / 100.0)
    }

    /**
      * When doing classification, get cross validation classification metric.
      * The metric used is in part 1 (example accuracy) and part 2 has the
      * actual value. Part 2 is in percent but is converted into a fraction.
      */
    def getCrossValidationAccuracy[T >: S <: ModelRecorded_or_Predicted, V >: U <: AlgorithmType] : (String, Double) = {
      val parts = getLastLine(out)
      val part1 = parts.head
      val part2 = parts(1).trim.dropRight(1)
      (part1, part2.toDouble / 100.0)
    }

  }

  // TODO: read data
  // TODO: scale columns
  // TODO: convert to libSVM format
  // TODO: select kernel
  // TODO: select parameters
  // TODO: select configuration via CV
}
