package pt.inescn.eval.stream

import pt.inescn.eval._

/**
  * Utility class to calculate the binary classification metrics.
  * This class is specifically designed to work on stream data.
  * take a look at [[pt.inescn.etl.stream.Load.Val]],
  * [[pt.inescn.etl.stream.Load.Row]] and
  * [[pt.inescn.etl.stream.Load.Frame]].
  *
  * @see https://en.wikipedia.org/wiki/Precision_and_recall
  * @see https://www.kaggle.com/wiki/Metrics
  */
case class BinaryClassMetrics(fp: Long,            // false positives
                              fn: Long,            // false negatives
                              tp: Long,            // true positives
                              tn: Long,            // true negatives
                              recall: Recall[Double],
                              precision: Precision[Double],
                              accuracy: Accuracy[Double],
                              falsePositRate:FalsePositRate[Double],
                              mcc: MCC[Double],
                              fscore_1: FScore[Double],
                              fscore_2: FScore[Double],
                              fscore_05: FScore[Double],
                              gmeasure: GMeasure[Double]) {


  /**
    * Sensitivity or true positive rate (TPR)
    * eqv. with hit rate, recall
    * TPR = TP / Parameters = TP / ( TP + FN )
    */
  private def get_recall(fp: Long, fn: Long, tp: Long,  tn: Long): Recall[Double] =  Recall((tp * 1.0)/ (tp + fn))

  /**
    * Precision or positive predictive value (PPV)
    * PPV = TP / ( TP + FP )
    */
  private def get_precision(fp: Long, fn: Long, tp: Long,  tn: Long): Precision[Double] = Precision((tp*1.0) / ( tp + fp ))

  /**
    * Accuracy (ACC)
    * ACC = ( TP + TN ) / ( TP + FP + FN + TN )
    */
  def get_accuracy(fp: Long, fn: Long, tp: Long,  tn: Long): Accuracy[Double] = Accuracy( (tp + tn + 0.0) / ( tp + fp + fn + tn ) )


  //False positive rate
  def get_falsePositRate(fp: Long, fn: Long, tp: Long,  tn: Long): FalsePositRate[Double] = FalsePositRate( fp*1.0/(fp+tn+0.0) )



  /**
    * Matthews correlation coefficient (MCC)
    *                  TP × TN − FP × FN
    * -----------------------------------------------------
    * Sqrt( ( TP + FP )( TP + FN ) ( TN + FP ) ( TN + FN ) )
    *
    *
    * Wikipedia states on the page for the metric that "If any of the four
    * sums in the denominator is zero, the denominator can be arbitrarily
    * set to one; this results in a Matthews correlation coefficient of
    * zero, which can be shown to be the correct limiting value."
    *
    * @see https://github.com/mlr-org/mlr/issues/1736
    * @see https://github.com/scikit-learn/scikit-learn/issues/1937
    *
    */
  def get_mcc(fp: Long, fn: Long, tp: Long,  tn: Long): MCC[Double] = {
    val num = (tp * tn) - (fp * fn)
    // NOTE: if we place the 1.0 multiplier at the end, then the sum is done in integer type
    // This exceed the limits and generates a negative value. To solve this either place the
    // multiplier in the first place or inside one of the additions
    val testDenom = Math.sqrt( 1.0 * ( tp + fp ) * ( tp + fn ) * ( tn + fp ) * ( tn + fn ))
    val denom = if (testDenom == 0.0) 1 else testDenom
    MCC(num / denom)
  }

  /**
    * Harmonic mean
    *
    * The F score can be interpreted as a weighted average of the precision and recall, where a score reaches its best
    * value at 1 and worst score at 0. The relative contribution of precision and recall to the F beta are beta and
    * (1-beta). For F1 the relative scores score are equal. F2 weighs recall higher than precision (by placing more
    * emphasis on false negatives). F0.5 measure, weighs recall lower than precision (by attenuating the influence of
    * false negatives).
    *
    * @see [[G]]
    */
  def Fbeta(betaC : FBeta[Double])(fp: Long, fn: Long, tp: Long,  tn: Long) : FScore[Double] = {
    val beta = betaC.value
    val beta2 = beta*beta
    val num = (1.0 + beta2) * tp
    val denom = ((1.0 + beta2)* tp) + (beta2*fn) + fp
    FScore(betaC, num / denom)
  }

  /**
    * Same weight for both recall and precision
    */
  def F1(fp: Long, fn: Long, tp: Long,  tn: Long): FScore[Double] = Fbeta(FBeta(1.0))(fp,fn,tp,tn)

  /**
    * Weighs recall higher than precision
    */
  def F2(fp: Long, fn: Long, tp: Long,  tn: Long): FScore[Double] = Fbeta(FBeta(2.0))(fp,fn,tp,tn)

  /**
    * Weighs recall lower than precision
    */
  def F0_5(fp: Long, fn: Long, tp: Long,  tn: Long): FScore[Double] = Fbeta(FBeta(0.5))(fp,fn,tp,tn)

  /**
    * Geometric mean
    * @see [[Fbeta]]
    */
  def G(fp: Long, fn: Long, tp: Long,  tn: Long): GMeasure[Double] = GMeasure( Math.sqrt(precision.value * recall.value) )


  def update(label: Boolean, predict: Boolean) : BinaryClassMetrics = {
    val ntp = if (label && predict) tp + 1 else tp
    val ntn = if (!label && !predict) tn + 1 else tn
    val nfp = if (!label && predict) fp + 1 else fp
    val nfn = if (label && !predict) fn + 1 else fn

    val nrecall = get_recall(nfp,nfn,ntp,ntn)
    val nprecision = get_precision(nfp,nfn,ntp,ntn)
    val naccuracy = get_accuracy(nfp,nfn,ntp,ntn)
    val nfalsePositRate = get_falsePositRate(nfp,nfn,ntp,ntn)
    val nmcc = get_mcc(nfp,nfn,ntp,ntn)
    val nfscore_1 = F1(nfp,nfn,ntp,ntn)
    val nfscore_2 = F2(nfp,nfn,ntp,ntn)
    val nfscore_05 = F0_5(nfp,nfn,ntp,ntn)
    val ngmeasure = G(nfp,nfn,ntp,ntn)


    BinaryClassMetrics( nfp,nfn,ntp,ntn,
      nrecall,nprecision,naccuracy,nfalsePositRate,
      nmcc,
      nfscore_1,nfscore_2,nfscore_05,
      ngmeasure )
  }

}

/**
  * Constructors for the BinaryClassMetrics
  */
object BinaryClassMetrics {
  def apply(): BinaryClassMetrics = BinaryClassMetrics(0,0,0,0,
    Recall(0.0), Precision(0.0), Accuracy(0.0), FalsePositRate(0.0), MCC(0.0),
    FScore(FBeta(1),0.0),FScore(FBeta(2),0.0),FScore(FBeta(0.5),0.0),
    GMeasure(0.0))
}

