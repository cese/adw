package pt.inescn.search.stream

import java.time.{Duration, LocalDateTime}

import pt.inescn.etl.Load.format
import pt.inescn.etl.stream.Load._
import pt.inescn.search.stream.Pipes.Func
import pt.inescn.search.stream.Tasks._
import pt.inescn.utils.ADWError

// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._

object UtilTasks {

  /*
    General predefined tasks for Streams (in and out of type Frame)

    IMPORTANT NOTE: Circe is used to generate the JSON encoder and decoder.
    However Circe uses Shapeless which cannot deal with companion objects.
    So all DSL companion objects need to be named differently. We add an
    `f` to make this change.
   */


  /**
    * No operation. Place holder used for debugging.
    */
  object Nop {
    def apply[I,O](): Func[I,O] = T( (i:I) => Left(ADWError(s"NOP($i)")) )
  }

  def NOP[I,O]: Task[I,O] = Nop().t


  /**
    * Loads a data source in the very simple [[DataFrame]] format.
    * Creates a [[pt.inescn.etl.stream.Load.Frame]]. Used for
    * testing and debugging.
    *
    * @param data data in simple string table format
    */
  def load$(data: DataFrame, any: Any): Either[ADWError, Frame] = {
    val d = DataFrame(data.f)
    val f = Frame(d.f: _*)
    Right(f)
  }

  /**
    * [[pt.inescn.search.stream.Pipes.Func]] constructors for the tuples loader.
    */
  def load(data: Seq[(String, Seq[String])]): Func[Any,Frame] = T(load$ _, DataFrame(data))


  /**
    * Converts one or more string columns to double
    * @param cols column names
    */
  def toDouble$(cols: ColumnNames, i: Frame) : Either[ADWError, Frame] = {
    Right(i((x:String) => x.toDouble, cols.cols:_*))
  }

  /**
    * [[pt.inescn.search.stream.Pipes.Func]] constructors for the double converter.
    */
  def toDouble(cols: String*): Func[Frame,Frame] = T(toDouble$ _, ColumnNames(cols))


  /**
    * Converts one or more string columns to long
    *
    * @param cols column names
    * @param i input stream
    * @return either an error or a stream with the converted column
    */
  def toLong$(cols:ColumnNames, i: Frame): Either[ADWError, Frame] = {
    Right(i((x:String) => x.toLong, cols.cols:_*))
  }

  /**
    *
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the long converter.
    */
  def toLong(cols:String*): Func[Frame,Frame] = T(toLong$ _, ColumnNames(cols))

  /**
    * Converts one or more string columns to boolean
    *
    * @param cols column names
    * @param i input stream
    * @return either an error or a stream with the converted column
    */
  def toBool$(cols: ColumnNames, i: Frame): Either[ADWError, Frame]= {
    Right(i((x:String) => x.toBoolean, cols.cols:_*))
  }

  /**
    * Converts one or more string columns to boolean
    *
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the boolean converter.
    */
  def toBool(cols:String*): Func[Frame,Frame] =
    T(toBool$ _, ColumnNames(cols))

  /**
    * Converts one or more boolean columns to double
    *
    * @param cols column names
    * @param i input stream
    * @return either an error or a stream with the converted column
    */
  def boolToDouble$(cols:ColumnNames, i:Frame) : Either[ADWError, Frame]= {
    Right(i((x: Boolean) => if (x) 1.0 else 0.0, cols.cols:_*))
  }

  /**
    * Converts one or more boolean columns to double
    *
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the boolean to double converter.
    */
  def boolToDouble(cols:String*): Func[Frame,Frame] =
    T(boolToDouble$ _, ColumnNames(cols))


  /**
    * Multiplies one or more double columns by a constant
    *
    * @param v constant to multiply
    * @param cols column names
    * @param i input stream
    * @return either an error or a stream with the new column
    */
  def xDouble$(v: DoubleP, cols: ColumnNames, i: Frame): Either[ADWError, Frame]= {
    Right(i((x: Double) => v.v * x, cols.cols:_*))
  }

  /**
    *
    * @param v constant to multiply
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the double multiplication.
    */
  def xDouble(v: Double, cols:String*): Func[Frame,Frame] =
    T(xDouble$ _, DoubleP(v), ColumnNames(cols) )


  /**
    * Converts one or more double columns to long
    *
    * @param cols column names
    * @param i input stream
    * @return either an error or a new frame with converted columns
    */
  def doubleToLong$(cols:ColumnNames, i:Frame) : Either[ADWError, Frame]= {
    Right(i((x: Double) => x.toLong, cols.cols:_*))
  }

  /**
    * Converts one or more double columns to long
    *
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the double to long converter.
    */
  def doubleToLong(cols:String*): Func[Frame,Frame] =
    T(doubleToLong$ _, ColumnNames(cols))

  /**
    * Converts one or more long columns to duration
    * Assumes time in nanoseconds
    *
    * @param cols column names
    * @param i input stream
    * @return either an error or a new frame with converted columns
    */
  def longToDuration$(cols:ColumnNames, i:Frame): Either[ADWError, Frame]= {
    Right(i((x: Long) => Duration.ofNanos(x), cols.cols:_*))
  }

  /**
    * Converts one or more long columns to duration
    * Assumes time in nanoseconds
    *
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the long to Duration converter.
    */
  def longToDuration(cols:String*): Func[Frame,Frame] = {
    T(longToDuration$ _, ColumnNames(cols))
  }

  /**
    * Converts one or more string columns to date and time
    *
    * @param cols column names
    * @param i input stream
    * @return Either and error or a new Frame with the converted columns
    */
  def toDateTime$(cols:ColumnNames, i: Frame) : Either[ADWError, Frame]= {
    Right(i((x:String) => LocalDateTime.parse( x, format), cols.cols:_*))
  }

  /**
    *
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the data and time converter.
    */
  def toDateTime(cols:String*): Func[Frame,Frame] = {
    T(toDateTime$ _, ColumnNames(cols))
  }

  /**
    * Changes one or more columns names
    *
    * @param cols column names (old -> new)
    */
  def renameColumns$(cols:ColumnRenames, i: Frame): Either[ADWError,Frame] = {
    Right(i.renameCols(cols.cols:_*))
  }

  /**
    *
    * @param cols column names (old -> new)
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the
    *        columns renaming function.
    */
  def renameColumns(cols: (String,String)*): Func[Frame,Frame] =
    T(renameColumns$ _ , ColumnRenames(cols))

  /**
    * Adds a single counter column to the table/stream
    *
    * @param col column name of the counter
    * @param i stream input
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the counter column appender.
    */
  def addCounterColumn$(col:ColumnName, i: Frame): Either[ADWError, Frame] = {
    // Create a counter
    def from(c: Long): Stream[Long] = c #:: from(c + 1)
    val count = Frame(col.col -> from(1L).toIterator)
    Right(i || count)
  }

  /**
    * Adds a single counter column to the table/stream
    *
    * @param col column name of the counter
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the counter column appender.
    */
  def addCounterColumn(col: String): Func[Frame,Frame] = T(addCounterColumn$ _, ColumnName(col) )

  /**
    * Adds a date time to o one or more string columns
    * Assume columns are durations
    *
    * @param cols column names
    */
  def addDateTimeToDuration$(dt:DateTime, cols:ColumnNames, i: Frame): Either[ADWError, Frame] =
    Right(i((x:Duration) => dt.dt.plus(x), cols.cols:_* ))


  /**
    * Adds a date time to o one or more string columns
    * Assume columns are durations
    *
    * @param dt constant time to add
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the date time adder.
    */
  def addDateTimeToDuration(dt: LocalDateTime, cols: String*): Func[Frame,Frame] =
    T(addDateTimeToDuration$ _, DateTime(dt), ColumnNames(cols))


  /**
    * Adds a fixed duration to one or more string columns
    * Assume columns are durations
    *
    * @param cols column names
    */
  def addDurationToDuration$(dt:DurationTime, cols:ColumnNames, i: Frame): Either[ADWError, Frame] ={
    Right(i((x:Duration) => dt.dt.plus(x), cols.cols:_* ))
  }

  /**
    * Adds a fixed duration to one or more string columns
    * Assume columns are durations
    *
    * @param dt constant time to add
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructor that has the date time adder.
    */
  def addDurationToDuration(dt: Duration, cols: String*): Func[Frame,Frame] =
    T(addDurationToDuration$ _, DurationTime(dt), ColumnNames(cols))


  private def add(acc: Map[String, Double], e:Double, k:String): Map[String, Double] = {
    val old = acc(k)
    acc.updated(k, old + e)
  }

  /**
    * Sums the numeric values and creates a new column with the result
    *
    * @param left name of left operand column
    * @param right name of right operand column
    * @param out name of result column
    */
  def addNumeric$(left:ColumnName, right:ColumnName, out:ColumnName,i: Frame): Either[ADWError, Frame] = {

    // row typing info
    // column name and type (store vector)
    val colInfo = out.col -> Val(0.0)

    def op(i: Row): Row = {
      val l = unpack[Double](i(left.col))
      val r = unpack[Double](i(right.col))
      val sum = (l,r) match {
        case (Right(vl),Right(vr)) => Val(vl+vr)
        case (Left(el),Left(er)) => ErrVal(s"Expected numeric values but got: left - $el, right - $er")
        case (Left(el),_) => ErrVal(s"Expected numeric value on left operand but got: $el")
        case (_,Left(er)) => ErrVal(s"Expected numeric value on right operand but got: $er")
      }
      // extend row with new data
      i ++ Row(out.col -> sum)
    }

    Right(i.map(op,colInfo))
  }

  /**
    * Sums the numeric values and creates a new column with the result
    *
    * @param left name of left operand column
    * @param right name of right operand column
    * @param out name of result column
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructor that adds 2 columns
    *        and creates a new one
    */
  def addNumeric(left:String, right:String, out:String): Func[Frame,Frame] =
    T( addNumeric$ _, ColumnName(left), ColumnName(right), ColumnName(out) )


  /**
    * Sums two duration values and creates a new column with the result
    *
    * @param left name of left operand column
    * @param right name of right operand column
    * @param out name of result column
    */
  def addDuration$(left:ColumnName, right:ColumnName, out:ColumnName,i: Frame): Either[ADWError, Frame] = {

    // row typing info
    // column name and type (store vector)
    val colInfo = out.col -> Val(Duration.ofNanos(0))

    def op(i: Row): Row = {
      val l = unpack[Duration](i(left.col))
      val r = unpack[Duration](i(right.col))
      val sum = (l,r) match {
        case (Right(vl),Right(vr)) => Val(vl.plus(vr))
        case (Left(le),Left(re)) => ErrVal(s"Expected duration values but got: left - $le, right - $re")
        case (Left(le),_) => ErrVal(s"Expected duration value on left operand but got: $le")
        case (_,Left(re)) => ErrVal(s"Expected duration value on right operand but got: $re")
      }
      // extend row with new data
      i ++ Row(out.col -> sum)
    }

    Right(i.map(op,colInfo))
  }

  /**
    * Sums two duration values and creates a new column with the result
    *
    * @param left name of left operand column
    * @param right name of right operand column
    * @param out name of result column
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for [[addDuration]]
    */
  def addDuration(left:String, right:String, out:String): Func[Frame,Frame] =
    T( addDuration$ _, ColumnName(left), ColumnName(right), ColumnName(out) )


  /**
    * Sums all numeric values of one or more columns and convert
    * to a map from column names to double sum total. Note that this
    * is a cumulative sum - we keep adding as each row comes in, and
    * produce a stream of current sums.
    *
    * @param cols column names
    */
  def sum$(cols:ColumnNames, i: Frame): Either[ADWError, Val[Map[String, Double]]] = {
    val accs: Map[String, Double] = cols.cols.map(idx => (idx,0.0)).toMap
    val sums: Map[String, Double] = i.iterable.foldLeft(accs){ (acc, row) =>
      accs.keys.foldLeft(acc){ (acc,k) =>
        val column = unpack[Double](row(k))
        column match {
          case Left(_) => acc  // ??
          case Right(e) => add(acc,e,k)
        }
      }
    }
    Right(Val(sums))
  }

  /**
    * Sums all numeric values of one or more columns and convert
    * to a map from column names to double sum total. Note that this
    * is a cumulative sum - we keep adding as each row comes in, and
    * produce a stream of current sums.
    *
    * [[pt.inescn.search.stream.Pipes.Func]] constructors for the columns summing.
    *
    * @param cols column names
    * @return pipe function that sums stream columns
    */
  def sum(cols: String*): Func[Frame,Val[Map[String,Double]]] = T( sum$ _, ColumnNames(cols) )


  /**
    * Converts a table stream to a stream of sliding windows with a specific
    * length and stride. The same columns are maintained without change.
    *
    * @param win indicates the length and stride of the window
    * @param i stream input
    * @return either an error or a Frame of windows
    */
  def window$(win:SlidingWindow, i: Frame): Either[ADWError, Frame] = {
    Right(i.window(win.size, win.stride))
  }

  /**
    * Converts a table stream to a stream of sliding windows with a specific
    * length and stride. The same columns are maintained without change.
    * Windows may overlap (stride less than the window size).
    *
    * @param size size of the window
    * @param stride amount window shifts
    * @param i input stream
    * @return either an error or a Frame of windows
    */
  def window1(size:Int, stride:Int, i: Frame): Either[ADWError, Frame] = {
    Right(i.window(size, stride))
  }

  /**
    * [[pt.inescn.search.stream.Pipes.Func]] constructors for the sliding window transformer.
    * Converts a table stream to a stream of sliding windows with a specific
    * length and stride. The same columns are maintained without change.
    * Windows may overlap (stride less than the window size).
    *
    * @param size size of the window
    * @param stride amount window shifts
    * @return pipe function that generates windows
    */
  def window(size:Int, stride:Int): Func[Frame,Frame] = T( window$ _,  SlidingWindow(size, stride))

  /**
    * Creates a new steam that only has the columns `cols`.
    *
    * @param cols names of columns to keep
    * @param i input stream
    * @return either an error or a Frame with the selected columns
    */
  def project$(cols:ColumnNames, i: Frame): Either[ADWError, Frame] = {
    Right(i.project(cols.cols:_*))
  }

  /**
    * Creates a new steam that only has the columns `cols`.
    *
    * @param cols names of columns to keep
    * @return either an error or a Frame with the selected columns
    */
  def project(cols:ColumnNames): Func[Frame,Frame] = T( project$ _,  cols:ColumnNames)


  /**
    * Converts the damage column to boolean:
    *   0 - no failure
    *   1 -  anomaly present
    *
    * @param col column name
    */
  def convertLabelValue$(col:ColumnName, i: Frame): Either[ADWError, Frame] ={
    Right(i((x:String) => if (x.toLowerCase == "norm") false else true, col.col))
  }

  /**
    * [[pt.inescn.search.stream.Pipes.Func]] constructors for the label to Boolean converter.
    * Converts the damage column to boolean:
    *   0 - no failure
    *   1 -  anomaly present
    *
    * @param col column name
    */
  def convertLabelValue(col:String): Func[Frame,Frame] = T(convertLabelValue$ _, ColumnName(col))

  /**
    *
    * Collect some of the results (executes now)
    * When processing each experiment we are only interested in the last element.
    * Standard techniques are slow. In addition to this they require that you know
    * the length of the stream (assuming it is finite) in order to get the last
    * element.
    * The following experiments were done:
    * Gets the last element : reduce 192 sec
    * Gets the last element : reduceLeft 194 sec
    * Gets the last element : reduceLeftOption 190 sec
    * Gets all the elements ; drop 2999 and take 1 107 sec
    * Gets all the elements : take 3000 and access the last element (92 sec)
    *
    * @param cols names of columns to project
    * @param i input frame
    * @return either the last [[Row]] if exists else an error
    */
  def getLastRow$(cols: ColumnNames, i:Frame): Either[ADWError, Row] = {
    //Left(ADWError(s"$name: f(i:I) not implemented"))
    // Execute the pending steps on each lazy stream
    val read1: Option[Row] =
    i.project(cols.cols:_*).
      iterable.
      lastOption
      read1 match {
      case Some(e) =>
        Right(e)
      case None =>
        Left(ADWError("col: empty list, no result."))
    }
  }

  /**
    * [[pt.inescn.search.stream.Pipes.Func]] constructors for the task that
    * returns the last row of the Frame.
    *
    * @param cols names of columns to project
    * @param i input frame
    * @return either the last [[Row]] if exists else an error
    */
  def getLastRow(cols: String*): Func[Frame, Row] = T(getLastRow$ _, ColumnNames(cols))

  /**
    * This function creates a new column named "isanomaly"
    * with constant value anomVal passed as parameter.
    * It is to be used for testing purposes while using simulated data
    * that does not contain this information - it is required for the learning pipe.
    * */
  def constFrame$(anomVal: String, i: Frame): Either[ADWError, Frame] = {
    import pt.inescn.samplers.stream.Enumeration
    val isanom = Enumeration.continually(anomVal).toIterator
    val constFrame = Frame("isanomaly" -> isanom)
    val frameWithConstants = i || constFrame
    Right(frameWithConstants)
  }
}
