package pt.inescn.search.stream
import java.util

import pt.inescn.detector.stream.CantelliFCCAnomalyDetector
import pt.inescn.etl.stream.Load._
import pt.inescn.eval._
import pt.inescn.eval.stream.BinaryClassMetrics
import pt.inescn.search.stream.Pipes.Func
import pt.inescn.search.stream.Tasks._
import pt.inescn.utils.ADWError
import pt.inescn.features.{EMDFeat, LFCC}
import pt.inescn.plot.{JPlot, JPlotAlarmInterface}

import scala.collection.mutable

// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._


import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArraySeq
import scala.language.existentials

/**
  * Tasks represent functions that are executed within a pipe. It would
  * be better if any client could define and use their tasks independently
  * of the pipe system. However because we have to encode these as JSON
  * all tasks must be defined in this module. Later we may opt for binary
  * object serialization or some form of meta-compilation and avoid this
  * restriction.
  *
  */
object TasksFCC {





  /**
    * Calculates the FFT of a sliding window stored in a Double's Vector
    *
    * @param Fs       - sampling frequency (in Hz)
    * @param NmbCpCoe - Number of cepstral coefficients
    * @param MaxFrq   - Maximum frequency of the filter bank(in Hz)
    * @param MinFrq   - Minimum frequency  of the filter bank (in Hz)
    * @param NmbFltrs - Number of filters of the filter bank( equal to NmbCpCoe)
    * @param cols     compute FFT on these column names
    */
  def lfccStep$(Fs: Double, NmbFltrs: Int, MaxFrq: Double, MinFrq: Double, NmbCpCoe: Int,
               cols: ColumnNames, col: ColumnName,
               i: Frame):
  Either[ADWError, Frame] = {

    //cols.cols - wrappedarray com os nomes dos sinais
    //col.col   - string a concatenar
    //colInfo   - array buffer
    def lfccName(n: String) = n + col.col // new column names

    val newRow = cols.cols.map(c => lfccName(c) -> Val(ArraySeq(0.0))) //row typing info

    def op(data: Row): Row = {
      val tmp = data.filter(cols.cols: _*).map { case (k, v) =>
        // rename the column
        val nk = lfccName(k)
        val tmp = unpack[ArraySeq[Double]](v)
        val nv = tmp match {
          case Right(a) =>
            // Get the LFCC
            val arr = a.toArray
            // TODO: use arraySeq to avoid memory allocation?
            val LFCCclss = LFCC(Fs, NmbFltrs, MaxFrq, MinFrq, NmbCpCoe, arr.length)
            val nv = LFCCclss.LFCCcomp(arr)
            Val(ArraySeq(nv:_*))
          case _ =>
            ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk -> nv
      }
      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, newRow: _*)) // map only replaces
  }


  /**
    * Calculates the FFT of a sliding window stored in a Double's Vector
    *
    * @param Fs       - sampling frequency (in Hz)
    * @param NmbCpCoe - Number of cepstral coefficients
    * @param MaxFrq   - Maximum frequency of the filter bank(in Hz)
    * @param MinFrq   - Minimum frequency  of the filter bank (in Hz)
    * @param NmbFltrs - Number of filters of the filter bank( equal to NmbCpCoe)
    * @param cols     compute FFT on these column names
    * @return
    */
  def lfccStep(extend: String, Fs: Double, NmbFltrs: Int, MaxFrq: Double, MinFrq: Double, NmbCpCoe: Int, cols: String*):
  Func[Frame,Frame] =
    T(lfccStep$ _,
      Fs, NmbFltrs, MaxFrq, MinFrq, NmbCpCoe, ColumnNames(cols), ColumnName(extend))

  /**
    * Computes EMD features
    *
    * @param Fs       - sampling frequency (in Hz)
    * @param NmbCpCoe - Number of cepstral coefficients
    * @param MaxFrq   - Maximum frequency of the filter bank(in Hz)
    * @param MinFrq   - Minimum frequency  of the filter bank (in Hz)
    * @param NmbFltrs - Number of filters of the filter bank( equal to NmbCpCoe)
    * @param cols     compute FFT on these column names
    */
  def emdFeatStep$(cols: ColumnNames, col: ColumnName, i: Frame):
  Either[ADWError, Frame] = {

    //cols.cols - wrappedarray com os nomes dos sinais
    //col.col   - string a concatenar
    //colInfo   - array buffer
    def emdName(n: String) = n + col.col // new column names

    val newRow = cols.cols.map(c => emdName(c) -> Val(ArraySeq(0.0))) //row typing info

    def op(data: Row): Row = {
      val tmp = data.filter(cols.cols: _*).map { case (k, v) =>
        // rename the column
        val nk = emdName(k)
        val tmp = unpack[ArraySeq[Double]](v)
        val nv = tmp match {
          case Right(a) =>
            // Get the LFCC
            val arr = a.toArray
            val EMDFeatclss=EMDFeat(20,1000,1)
            val nv = EMDFeatclss.comp( arr , "EMDEng",500,10)
            Val(ArraySeq(nv:_*))
          case _ =>
            ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk -> nv
      }
      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, newRow: _*)) // map only replaces
  }

  /**
    * Calculates the FFT of a sliding window stored in a Double's Vector
    *
    * @param Fs       - sampling frequency (in Hz)
    * @param NmbCpCoe - Number of cepstral coefficients
    * @param MaxFrq   - Maximum frequency of the filter bank(in Hz)
    * @param MinFrq   - Minimum frequency  of the filter bank (in Hz)
    * @param NmbFltrs - Number of filters of the filter bank( equal to NmbCpCoe)
    * @param cols     compute FFT on these column names
    * @return
    */
  def emdFeatStep(extend: String, cols: String*):
  Func[Frame,Frame] =
    T(emdFeatStep$ _, ColumnNames(cols), ColumnName(extend))









  /**
    * Apply the median filter for each colunm
    *
    * @param NmbOfColunms - Number of columns
    * @param Stride       - step of the window
    * @param cols         - Columns that will be processed
    * @param col          - string to combine with the colunms names
    */
  def medianStep$(NmbOfColunms: Int, Stride: Int, cols: ColumnNames, col: ColumnName, i: Frame):
  Either[ADWError, Frame] = {

    def medianName(n: String): String = n + col.col // new column names

    val newRow = cols.cols.map(c => medianName(c) -> Val(ArraySeq(0.0))) //row typing info

    // Vector processing functions
    def median(Values: ArraySeq[Double]): Double = {
      val sortedArray = Values.sorted

      if (sortedArray.length % 2 == 1) {
        sortedArray(sortedArray.length / 2)
      }
      else {
        val (up, down) = sortedArray.splitAt(sortedArray.length / 2)
        (up.last + down.head) / 2
      }
    }


    def op(data: Row): Row = {

      val tmp = data.filter(cols.cols: _*).map { case (k, v) =>
        // rename the column
        val nk = medianName(k)
        val tmp = unpack[ArraySeq[ArraySeq[Double]]](v)
        val nv = tmp match {
          case Right(vv) =>

            var smthLfcc = ArraySeq.fill(NmbOfColunms)(0.0)
            var tmp1 = ArraySeq.fill(Stride)(0.0)

            var k = 0
            while (k < NmbOfColunms) {
              var j = 0
              while (j < Stride) {
                tmp1 = tmp1.updated(j, vv(j)(k))
                j = j + 1
              }
              smthLfcc = smthLfcc.updated(k, median(tmp1))
              k = k + 1
            }
            val nv = Val(smthLfcc)
            nv
          case _ =>
            ErrVal(s"Expected a vector of numerical vectors but got: $v")
        }
        nk -> nv
      }
      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, newRow: _*)) // map only replaces
  }

  def medianStep(extend: String, NmbOfFeatures: Int, Stride: Int, cols: String*): Func[Frame,Frame] =
    T(medianStep$ _, NmbOfFeatures, Stride, ColumnNames(cols), ColumnName(extend))


  /**
    * Calculates the mode of a columns
    *
    * @param cols compute mode on these column names
    */
  def modeStep$(cols: ColumnNames, col: ColumnName, i: Frame):
  Either[ADWError, Frame] = {

    def modeName(n: String): String = n + col.col // new columns

    val newRow = cols.cols.map(c => modeName(c) -> Val(0.0)) //row typing info

    def mode(n: Array[Double]) = {

      val CounterMap: scala.collection.mutable.Map[Double, Int] = scala.collection.mutable.Map(n(0) -> 1)
      var i = 1
      while (i < n.length) {
        if (CounterMap.contains(n(i)))
          CounterMap(n(i)) = CounterMap(n(i)) + 1
        else
          CounterMap += (n(i) -> 1)
        i = i + 1
      }

      val Keys = CounterMap.keys.toArray
      val Values = CounterMap.values.toArray

      Keys(Values.indexOf(Values.max))

    }


    def op(data: Row): Row = {
      val tmp = data.filter(cols.cols: _*).map { case (k, v) =>
        // rename the column
        val nk = modeName(k)
        val tmp = unpack[ArraySeq[Double]](v)
        val nv = tmp match {
          case Right(a) =>
            val arr = a.toArray
            // TODO: use ArraySeq to avoid memory allocation?
            val nv = mode(arr)
            Val(nv)
          case _ =>
            ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk -> nv
      }
      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, newRow: _*)) // map only replaces
  }

  def modeStep(extend: String, cols: String*): Func[Frame,Frame] =
    T(modeStep$ _, ColumnNames(cols), ColumnName(extend))


  /**
    * Concatenate several columns
    *
    * @param cols name of columns to concatenate
    */
  def concatenateStep$(cols: ColumnNames, col: ColumnName, i: Frame):
  Either[ADWError, Frame] = {

    val newRow = Seq(col.col -> Val(ArraySeq(0.0))) //row typing info

    def op(data: Row): Row = {

      var finalVec: ArraySeq[Double] = ArraySeq()

      val collist = cols.cols.toArray
      var i = 0
      while (i < collist.length) {
        val d = data(collist(i))
        val tmp = unpack[ArraySeq[Double]](d)
        tmp match {
          case Right(v) =>
            finalVec = finalVec ++ v
          case _ =>
            ErrVal(s"Expected a numerical Vector but got: $d")
        }
        i = i + 1
      }

      val tmp = Row(col.col -> Val(finalVec))

      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, newRow: _*)) // map only replaces
  }

  def concatenateStep(extend: String, cols: String*): Func[Frame,Frame] =
    T(nameFunc(concatenateStep$ _), ColumnNames(cols), ColumnName(extend))


  /**
    * Calculates the FFT of a sliding window stored in [[scala.Vector]]
    *
    * @param FtrsNmbr   - Number of features
    * @param AnomThres1 - Threshold 1
    * @param AnomThres2 - Threshold 2
    */
  def anomalyDetectionTrainStep$(FtrsNmbr: Int, AnomThres1: Double, AnomThres2: Double, NTrainFaseExmpl: Int, cols: ColumnNames, col: ColumnName, i: Frame):
  Either[ADWError, Frame] = {


    val AnomalyDetectorclss = CantelliFCCAnomalyDetector(FtrsNmbr, AnomThres1, AnomThres2)

    // row typing info
    // column name and type (store vector)
    val modelKey = "model"
    val newRow = List(col.col -> Val(0.0), modelKey -> Val(AnomalyDetectorclss))

    def op(data: Row): Row = {
      val field = data(cols.cols.head)
      val temp = unpack[ArraySeq[Double]](field)

      val (nv, model) = temp match {
        case Right(a) =>
          val arr = a.toArray
          val prediction = AnomalyDetectorclss.predict(arr)
          val model = if (prediction == 0.0 || AnomalyDetectorclss.ItObs.N < NTrainFaseExmpl) {
            AnomalyDetectorclss.train(arr)
          } else {
            AnomalyDetectorclss.currentModel()
          }

          (Val(prediction.toDouble), Val(model))
        //Val(prediction.toDouble)
        case _ =>
          val tmpErr = ErrVal(s"Expected a numerical vector but got: $temp")
          (tmpErr, tmpErr)
      }
      val tmp = Row(col.col -> nv, modelKey -> model)
      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, newRow: _*)) // map only replaces
  }

  def anomalyDetectionTrainStep(extend: String, FtrsNmbr: Int, AnomThres1: Double, AnomThres2: Double,
                                NTrainFaseExmpl: Int, cols: String*): Func[Frame,Frame] =
    T(anomalyDetectionTrainStep$ _,
      FtrsNmbr, AnomThres1, AnomThres2, NTrainFaseExmpl, ColumnNames(cols), ColumnName(extend))

  /**
    * Calculates the FFT of a sliding window stored in [[scala.Vector]]
    *
    * @param FtrsNmbr   - Number of features
    * @param AnomThres1 - Threshold 1
    * @param AnomThres2 - Threshold 2
    */
  def anomalyDetectionTrainStep2$(FtrsNmbr: Int, AnomThres1: Double, AnomThres2: Double,
                                  NTrainFaseExmpl: Int, cols: ColumnNames, col: ColumnName, i: Frame):
  Either[ADWError, Frame] = {


    val AnomalyDetectorclss = CantelliFCCAnomalyDetector(FtrsNmbr, AnomThres1, AnomThres2)

    // row typing info
    // column name and type (store vector)
    val modelKey = "model"
    val newRow = List(col.col -> Val(0.0), modelKey -> Val(AnomalyDetectorclss))

    def op(data: Row): Row = {
      val field = data(cols.cols.head)
      val temp = unpack[ArraySeq[Double]](field)

      val (nv, model) = temp match {
        case Right(a) =>
          val arr = a.toArray
          val prediction = AnomalyDetectorclss.predict(arr)
          val model = if (prediction == 0.0 || AnomalyDetectorclss.ItObs.N < NTrainFaseExmpl) {
            AnomalyDetectorclss.train(arr)
          } else {
            AnomalyDetectorclss.currentModel()
          }

          (Val(prediction.toDouble), Val(model))
        //Val(prediction.toDouble)
        case _ =>
          val tmpErr = ErrVal(s"Expected a numerical vector but got: $temp")
          (tmpErr, tmpErr)
      }
      val tmp = Row(col.col -> nv, modelKey -> model)
      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, newRow: _*)) // map only replaces
  }

  def anomalyDetectionTrainStep2(extend: String, FtrsNmbr: Int, AnomThres1: Double, AnomThres2: Double,
                                 NTrainFaseExmpl: Int, cols: String*): Func[Frame,Frame] =
    T(anomalyDetectionTrainStep2$ _,
      FtrsNmbr, AnomThres1, AnomThres2, NTrainFaseExmpl, ColumnNames(cols), ColumnName(extend))








  /**
    * Calculates the FFT of a sliding window stored in a Doubles vector.
    *
    * TODO: cols not used. Remove
    */
  def anomDetectionEvalStep$(tvColName: ColumnName, pvColName: ColumnName, cols: ColumnNames, i: Frame):
  Either[ADWError, Frame] = {

    val colInfo = Seq("acc" -> Val(0.0), "far" -> Val(0.0))

    var perfmMeasures = BinaryClassMetrics (0, 0, 0, 0,
      Recall(0.0), Precision (0.0), Accuracy(0.0), FalsePositRate(0.0), MCC(0.0),
      FScore(FBeta(1), 0.0), FScore(FBeta(2), 0.0), FScore(FBeta(0.5), 0.0),
      GMeasure(0.0))


    def op(data: Row): Row = {
      val tmpTrueval = data(tvColName.col)
      val tmpPredval = data(pvColName.col)

      val trueval = unpack[Double](tmpTrueval)
      val predval = unpack[Double](tmpPredval)

      val tmp = (trueval, predval) match {
        case (Right(tv), Right(pv)) =>

          perfmMeasures = perfmMeasures.update(if (tv == 1) {
            true
          } else {
            false
          }, if (pv == 1) {
            true
          } else {
            false
          })

          //println(s" $tv  $pv  $accCounter  $farCounter   $exmplCounter ")
          Row("acc" -> perfmMeasures.accuracy.value, //
            "far" -> perfmMeasures.falsePositRate.value) //
        case _ =>
          val v = ErrVal(s"Expected a numerical vector but got: trueval = $trueval and predval = $predval")
          Row("acc" -> v)
      }
      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, colInfo: _*)) // map only replaces
  }

  def anomDetectionEvalStep(truevals: String, predvals: String, cols: String*): Func[Frame,Frame] =
    T(anomDetectionEvalStep$ _,
      ColumnName(truevals), ColumnName(predvals), ColumnNames(cols))

  /**
    * Print colunms
    *
    * @param cols     name of colunm for concatenation
    * @param bufferSz size of the plotting buffer
    */
  def plotStreamStep$(bufferSz: Int, cols: ColumnNames, ii: Frame): Either[ADWError, Frame] = {


    val fig = new JPlot("")
    val yybuffer: Array[ListBuffer[Double]] = new Array[ListBuffer[Double]](cols.cols.length)
    val yyarray: Array[Array[Double]] = new Array[Array[Double]](cols.cols.length)
    val xbuffer: ListBuffer[Double] = ListBuffer()
    var counter: Double = 0
    var counter1 = 0
    fig.figure()

    var i = 0
    while (i < yybuffer.length) {
      yybuffer(i) = ListBuffer[Double]()
      i += 1
    }


    def op(data: Row): Row = {

      val collist = cols.cols.toArray

      var i = 0
      while (i < collist.length) {
        //print(counter)
        val v = unpack[Double](data(collist(i)))
        //print(v)
        v match {
          case Right(v) =>
            if (counter < bufferSz) {
              xbuffer += counter
              yybuffer(i) = yybuffer(i) :+ v
            }
            else {
              yybuffer(i) = yybuffer(i) :+ v
              yybuffer(i).remove(0)
            }
          case _ =>
            ErrVal(s"Expected a numerical value but got: $v")
        }
        i += 1
      }

      counter += 1
      counter1 += 1
      if (counter1 == 30) {

        var j = 0
        while (j < yyarray.length) {
          yyarray(j) = yybuffer(j).toArray
          j += 1
        }

        fig.wiggle(xbuffer.toArray, yyarray)
        Thread.sleep(100)
        counter1 = 0
      }
      data // Data bypass
    }

    Right(ii.map(op)) // map only replaces
  }

  def plotStreamStep(bufferSz: Int, cols: String*): Func[Frame,Frame] =
    T(plotStreamStep$ _, bufferSz, ColumnNames(cols))




  /**
    * Print colunms
    *
    * @param cols     name of colunm for concatenation
    * @param bufferSz size of the plotting buffer
    */
  def plotAlarmInterfaceStep$(bufferSz: Int, cols: ColumnNames, ii: Frame): Either[ADWError, Frame] = {


    // Create some sample data
    var ay1: Array[Double] = new Array[Double](1)
    var ay2: Array[Double] = new Array[Double](1)
    var ay3: Array[Double] = new Array[Double](1)
    val y1: util.List[Double] = new util.LinkedList[Double]
    val y2: util.List[Double] = new util.LinkedList[Double]
    val y3: util.List[Double] = new util.LinkedList[Double]

    val bufferSz: Int = 10000

    val x: Array[Double] = new Array[Double](bufferSz)
    val yybuffer: Array[ListBuffer[Double]] = new Array[ListBuffer[Double]](4)
    val yyarray: Array[Array[Double]] = new Array[Array[Double]](4)
    val xbuffer: ListBuffer[Double] = ListBuffer()
    val figw: JPlotAlarmInterface = new JPlotAlarmInterface("")
    val yy: Array[Array[Double]] = new Array[Array[Double]](3)
    figw.ylim(-10, 10);  figw.holdon()


    var i = 0
    while (i < yybuffer.length) {
      yybuffer(i) = ListBuffer[Double]()
      i += 1
    }

    var a = 10
    var av = 0.0
    var pt = -10.0
    var blink = 0
    var counter =0
    var counter1=0

    //==================
    def op(data: Row): Row = {

      val collist = cols.cols.toArray

      var i = 0
      while (i < collist.length) {
        //print(counter)
        val v = unpack[Double](data(collist(i)))
        //print(v)
        v match {
          case Right(v) =>
            if (counter < bufferSz) {
              xbuffer += counter
              yybuffer(i) = yybuffer(i) :+ v
            }
            else {
              yybuffer(i) = yybuffer(i) :+ v
              yybuffer(i).remove(0)
            }
          case _ =>
            ErrVal(s"Expected a numerical value but got: $v")
        }
        i += 1
      }

      av=yybuffer(3).toArray.last

      //print(av)

      counter += 1

      counter1 += 1
      if (counter1 == 100) {

        //print(yyarray.length)

        var j = 0
        while (j < yyarray.length-1) {
          yy(j) = yybuffer(j).toArray
          j += 1
        }

        if (av == 1) {
          if (pt == -10) pt = bufferSz
          else pt -= 100

          if (pt == 0) {
            pt = -10
            av = 0
          }
        }



        val xp = Array(pt, pt)
        figw.wiggle(xbuffer.toArray, yy, xp, bufferSz)

        // Alarm lamp
        if (av == 0)
          figw.setState(1)
        else if ( blink == 0) {
          figw.setState(3)
          blink = 1
        }
        else {
          figw.setState(0)
          blink = 0
        }

        val CF = figw.meanCrestFactor(yy)
        val ClF = figw.meanClearanceFactor(yy)
        val IF= figw.meanImpulseFactor(yy)
        val SF= figw.meanShapeFactor(yy)


        figw.updateGaugesValue(CF, ClF, IF, SF)
        figw.updateDisplayValues(CF, ClF, IF, SF)

        Thread.sleep(400)
        counter1 = 0
      }
      data // Data bypass
    }

    Right(ii.map(op)) // map only replaces    Right(ii.map(op)) // map only replaces
  }

  def plotAlarmInterfaceStep(bufferSz: Int, cols: String*): Func[Frame,Frame] =
    T(plotAlarmInterfaceStep$ _, bufferSz, ColumnNames(cols))









  /**
    * PrintVec plots the values of colunms of vectors
    *
    * @param cols     name of colunm for concatenation
    * @param bufferSz size of the plotting buffer
    * @param Sz       size of the vectors
    */
  def plotVecStreamStep$(bufferSz: Int, Sz: Int, cols: ColumnNames, ii: Frame): Either[ADWError, Frame] = {


    val fig = new JPlot("")
    val yybuffer: Array[ListBuffer[Double]] = new Array[ListBuffer[Double]](Sz)
    val yyarray: Array[Array[Double]] = new Array[Array[Double]](Sz)
    val xbuffer: ListBuffer[Double] = ListBuffer()
    var counter: Double = 0
    var counter1 = 0
    fig.figure()

    var i = 0
    while (i < yybuffer.length) {
      yybuffer(i) = ListBuffer[Double]()
      i += 1
    }


    def op(data: Row): Row = {

      val v = unpack[ArraySeq[Double]](data(cols.cols(0)))

      v match {
        case Right(v) =>

          var i = 0
          while (i < v.length) {
            if (counter < bufferSz) {
              xbuffer += counter
              yybuffer(i) = yybuffer(i) :+ v(i)
            }
            else {
              yybuffer(i) = yybuffer(i) :+ v(i)
              yybuffer(i).remove(0)
            }
            i += 1
          }

        case _ =>
          ErrVal(s"Expected a numerical value but got: $v")
      }

      counter += 1

      counter1 += 1
      if (counter1 == 5) {

        var j = 0
        while (j < yyarray.length) {
          yyarray(j) = yybuffer(j).toArray
          j += 1
        }

        fig.wiggle(xbuffer.toArray, yyarray)
        Thread.sleep(100)
        counter1 = 0
      }
      data // Data bypass
    }

    Right(ii.map(op)) // map only replaces
  }

  def plotVecStreamStep(bufferSz: Int, Sz: Int, cols: String*): Func[Frame,Frame] =
    T(plotVecStreamStep$ _, bufferSz, Sz, ColumnNames(cols))


  /**
    * PrintVec plots the values of colunms of vectors
    *
    * @param cols     name of colunm for concatenation
    * @param bufferSz size of the plotting buffer
    * @param Sz       size of the vectors
    */
  def addWhiteGaussianNoiseStep$(SNR:Double,Seed:Int, col: ColumnName, cols: ColumnNames, i: Frame): Either[ADWError, Frame] = {

    def modeName(n: String): String = n + col.col // new columns

    val newRow = cols.cols.map(c => modeName(c) -> Val(0.0)) //row typing info
    val rnd= new scala.util.Random(Seed)

    def addWhiteGaussianNoise(noiseLeavel: Double, smpFrame: Array[Double]):Array[Double] = {

      val frmEnergy = smpFrame.map(Math.pow(_,2)).sum
      val r = Seq.fill(smpFrame.length)(rnd.nextGaussian())
      val rEnergy= r.map(Math.pow(_,2)).sum
      val alpha = Math.sqrt(frmEnergy/(Math.pow(10,SNR/10)*rEnergy))
      val outFrame= (smpFrame,r).zipped.map( _ + _*alpha )

      outFrame
    }


    def op(data: Row): Row = {
      val tmp = data.filter(cols.cols: _*).map { case (k, v) =>
        // rename the column
        val nk = modeName(k)
        val tmp = unpack[ArraySeq[Double]](v)
        val nv = tmp match {
          case Right(a) =>
            val arr = a.toArray
            val nv = addWhiteGaussianNoise(SNR,arr)
            Val(ArraySeq(nv:_*))
          case _ =>
            ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk -> nv
      }
      // extend row with new data
      data ++ tmp
    }

    Right(i.map(op, newRow: _*)) // map only replaces
  }

  def addWhiteGaussianNoiseStep(extend: String, SNR: Double,Seed:Int,cols: String*): Func[Frame,Frame] =
    T(addWhiteGaussianNoiseStep$ _, SNR, Seed, ColumnName(extend) ,ColumnNames(cols))



  /**
    * Converts one or more string columns to double
    * @param cols column names
    */
  def valMapStep$(vMap:Map[String,Double] , cols: ColumnNames , i: Frame) : Either[ADWError, Frame] = {


    Right(i( (x:String) => vMap(x) , cols.cols:_*))

  }

  def valMapStep( vMap:Map[String,Double] , cols: String*): Func[Frame,Frame] = {
    T(valMapStep$ _, vMap,  ColumnNames(cols)  )
  }












  /**
    * aggregationFuncStep  joint the results of several experiments according to an aggregation function
    *
    * @param cols     name of colunm for concatenation
    * @param bufferSz size of the plotting buffer
    * @param Sz       size of the vectors
    */
  def aggregToTableStep$[A, B](LinePar: Seq[A], ColPar: Seq[B], cols: ColumnNames, ii: Frame): Either[ADWError, Frame] = {

    // Table inicialization
    val nl = LinePar.length
    val nc = ColPar.length
    val table = new Array[Array[Double]](nl)
    var i = 0
    while (i < nl) {
      table(i) = new Array[Double](nc)
      i += 1
    }


    def op(data: Row): Row = {

      /* var collist = cols.cols.toArray

      var i=0
      while( i< cols.cols.length){
        var v = unpack[Double](data(cols.cols(i)))

        v match {
          case Right(v) =>


            while( i < v.length ) {
              if (counter < bufferSz) {
                xbuffer += counter
                yybuffer(i) = yybuffer(i) :+ v(i)
              }
              else {
                yybuffer(i) = yybuffer(i) :+ v(i)
                yybuffer(i).remove(0)
              }

          case _ =>
            ErrVal(s"Expected a numerical value but got: $v")
        }



        i+=1
      }

      var v = unpack[Double](data(cols.cols(0)))*/

      data // Data bypass
    }

    Right(ii.map(op)) // map only replaces
  }

  def aggregToTableStep[A, B](LinePar: Seq[A], ColPar: Seq[B], cols: String*): Func[Frame,Frame] =
    T(aggregToTableStep$ _, LinePar, ColPar, ColumnNames(cols))


  /**
    * aggregationFuncStep  joint the results of several experiments according to an aggregation function
    *
    * @param cols     name of colunm for concatenation
    * @param bufferSz size of the plotting buffer
    * @param Sz       size of the vectors
    */
  def aggregMaxStep$(cols: ColumnNames, ii: Frame): Either[ADWError, Frame] = {

    // Table inicialization
    /*val table= new Array[Array[Double]](nl)
    var i=0
    while(i < nl) {
      table(i) = new Array[Double](nc)
      i+=1
    }*/


    def op(data: Row): Row = {
      /*
      var collist = cols.cols.toArray

      var i=0
      while( i< cols.cols.length){
        var v = unpack[Double](data(cols.cols(i)))

        v match {
          case Right(v) =>


            while( i < v.length ) {
              if (counter < bufferSz) {
                xbuffer += counter
                yybuffer(i) = yybuffer(i) :+ v(i)
              }
              else {
                yybuffer(i) = yybuffer(i) :+ v(i)
                yybuffer(i).remove(0)
              }

              case _ =>
                ErrVal(s"Expected a numerical value but got: $v")
            }

            i+=1
        }

        var v = unpack[Double](data(cols.cols(0)))*/

      data // Data bypass
    }
    Right(ii.map(op)) // map only replaces
  }

  def aggregMaxStep(cols: String*): Func[Frame,Frame] = T(aggregMaxStep$ _, ColumnNames(cols))


}

