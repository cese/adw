package pt.inescn.search.stream

import better.files.File
import com.github.tototoshi.csv.CSVFormat
import org.hipparchus.transform.DftNormalization
import pt.inescn.dsp.Filter._
import pt.inescn.dsp.Fourier.FFT
import pt.inescn.etl.stream.DataFiles
import pt.inescn.etl.stream.Load._
import pt.inescn.models.stream.MeanWSD
import pt.inescn.search.stream.Pipes.{Agg, Executing, Func}
import pt.inescn.search.stream.Tasks._
import pt.inescn.utils.ADWError
import pt.inescn.app.Utils

// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._

object TestTasks {
  /**
    * Concatenates a string to one or more string columns.
    * Used for debugging and testing.
    *
    * @param a string to be concatenated
    * @param cols column names
    * @param i input stream
    * @return either a stream or a converted frame
    */
  def concat$(a:StringP, cols:ColumnNames, i: Frame): Either[ADWError, Frame] = {
    Right( i( (x:String) => x + a.v,  cols.cols:_*) )
  }

  /**
    *
    * @param a string to be concatenated
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the string concatenation transformer.
    */
  def concat(a:String, cols: String*): Func[Frame,Frame] = {
    val colsp = ColumnNames(cols)
    val ap = StringP(a)
    T(concat$ _, ap, colsp)
  }


  val msg = "Column('X') exception"

  /**
    * Concatenates a string to one or more string columns. This version
    * raises an exception. Used for debugging and testing.
    *
    * @param a string to be concatenated
    * @param cols column names
    * @param i input stream
    * @return either a stream or a converted frame
    */
  def concatThrow$(a:StringP, cols:ColumnNames, i: Frame): Either[ADWError, Frame] = {
    val hasX = cols.cols.contains("x")
    if (hasX)
      throw new RuntimeException(msg)
    else
      Right( i( (x:String) => x + a.v,  cols.cols:_*) )
  }

  /**
    * Concatenates a string to one or more string columns. This version
    * raises an exception. Used for debugging and testing.
    *
    * @param a string to be concatenated
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the string concatenation with exception.
    */
  def concatThrow(a:String, cols: String*): Func[Frame,Frame] = {
    val colsp = ColumnNames(cols)
    val ap = StringP(a)
    T(concatThrow$ _, ap, colsp)
  }


  /**
    * [[pt.inescn.search.stream.Pipes.Func]] constructors for the max column value aggregator.
    */
  object maxSum {

    type I = Val[Map[String, Double]]                 // Output of the pipes to aggregate: metric per column
    type Acc = Either[ADWError,(Executing,Val[_])]    // Either an error or maximum of a metric

    /**
      * This is an accumulator that receives results from several tasks. If
      * we get a sum result [[ Val[Map[String, Double]] ]],
      * then we extract the sum of a column and store it if it is the maximum.
      * If it is not the maximum, we keep the old value. If we get an error
      * as input then we store that error. We always store the last (latest)
      * error. Any input other than the expected results in an error.
      *
      * Note that this  is an updated maximum - we keep adding as each row comes
      * in, and produce a stream of current maximums.
      *
      * @param col column name
      */
    def maxSum(col: String, acc:Acc, i: Either[ADWError,I], trk: Executing): Acc = {
      (acc,i) match {
        case (Left(_), Right(vm)) =>
          // record latest as best
          val m = unpack[Map[String,Double]](vm)
          m.fold(err =>
            Right((trk,ErrVal(err))),
            { map: Map[String,Double] =>
              val v = map(col)
              Right((trk,Val(v)))
            })
        case (Right((_,os)), Right(vm)) =>
          // All is ok, select best score
          val m = unpack[Map[String,Double]](vm)
          val oldScore = unpack[Double](os)
          (oldScore, m) match  {
            case (Right(o), Right(map)) =>
              val v = map(col)
              if (v > o) Right((trk,Val(v))) else acc
            case (_, _) => acc
          }
        case (Right(_), Left(_)) =>
          // Ignore current failure, keep best so far
          acc
        case (Left(_), Left(e)) =>
          // Record latest failure (we could accumulate)
          Left(e)
      }
    }

    def apply(col: String): Agg[I,Acc] = {

      // TODO: how to automate this?
      val tsk = new Aggregator[I,Acc] {
        //override type Params = String
        override def name: String = "maxSum"

        override def params: Params = Map("col" -> col)

        override def zero: Acc = Left(ADWError("zero"))

        override def collect(acc: Acc, in: Either[ADWError,I], ta:Long, trk: Executing): Acc = {
          val tmp = maxSum(col, acc, in, trk)
          //println("tmp")
          //println(tmp)
          tmp
        }
      }

      Agg(tsk)
    }
  }

  /* Inegi */

  /**
    * Loads a data source from CSV files with the INEGI file name format.
    * Creates a [[pt.inescn.etl.stream.Load.Frame]]. Used for experimenting
    * with INEGI data.
    *
    * @param format CSV formatting such as separator used
    * @param fileNames list of file names to load and concatenate into a table
    * @param i input stream (not used)
    * @return either an error or a stream of the data
    */
  def loadINEGICSVs$(format: CSVFormat, fileNames: FileNames, i: Any): Right[ADWError, Frame] = {
    // Get features from file name
    val features1 = DataFiles.fileNamesfeatures(fileNames.fs.map(File(_)).toIterator, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1)
    // Load the file as a stream and extend existing features
    // with the ones parsed from the file names. We assume the
    // data fields are correct (no checks)
    // Exceptions will be handled by the pipe
    Right(Frame.extendCSVs(format, features2))
  }

  /**
    * Loads a data source from CSV files with the INEGI file name format.
    * Creates a [[pt.inescn.etl.stream.Load.Frame]]. Used for experimenting
    * with INEGI data.
    *
    * @param format CSV formatting such as separator used
    * @param fileNames list of file names to load and concatenate into a table
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the INEGI CSV loader.
    */
  def loadINEGICSVs(format: CSVFormat, fileNames: List[String]): Func[Any,Frame] = {
    T(loadINEGICSVs$ _, format, FileNames(fileNames))
  }

  /**
    * Loads a data source from CSV files with the INEGI file name format.
    * Creates a [[pt.inescn.etl.stream.Load.Frame]]. Used for experimenting
    * with INEGI data. Also allows us to add a constant Row of features
    * for each file.
    *
    * NOTE: Extra features cannot be passed as a
    * [[pt.inescn.etl.stream.Load.Row]]. Rows have
    * [[pt.inescn.etl.stream.Load.Val[_]]]. Vals are not sealed, so no implicit
    * Json will be found. This makes Task implicit Json conversion also fail.
    *
    * @param format CSV formatting such as separator used
    * @param fileName list of file names to load and concatenate into a table
    * @param extraFeatures set of features to add to rows (one per file)
    * @param i input stream (not used)
    * @return either an error or a stream of the data
    */
  def loadExtraINEGICSVs$(format: CSVFormat, fileName:FileNames, extraFeatures: List[(String, String)], i: Any): Right[ADWError, Frame] = {
    // Get features from file name
    val features1 = DataFiles.fileNamesfeatures(fileName.fs.map(File(_)).toIterator, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1)
    // Add a set of additional features (besides those from the file name)
    val rows = extraFeatures.map(e => Row((e._1, Val(e._2)) ))
    val features3 = features2.zip(rows.toIterator).map{ case ((df,r1),r2) => (df,r1++r2) }
    // Load the file as a stream and extend existing features with the
    // ones parsed from the file names and extras. We assume the data
    // fields are correct (no checks) Exceptions will be handled by the
    // pipe
    Right(Frame.extendCSVs(format, features3))
  }

  /**
    * Loads a data source from CSV files with the INEGI file name format.
    * Creates a [[pt.inescn.etl.stream.Load.Frame]]. Used for experimenting
    * with INEGI data. Also allows us to add a constant Row of features
    * for each file.
    *
    * @param format CSV formatting such as separator used
    * @param fileNames list of file names to load and concatenate into a table
    * @param extraFeatures set of features to add to rows (one per file)
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the INEGI CSV loader.
    */
  def loadExtraINEGICSVs(format: CSVFormat, fileNames: List[String], extraFeatures: List[(String, String)]): Func[Frame,Frame] = {
    T(loadExtraINEGICSVs$ _, format, FileNames(fileNames), extraFeatures)
  }

  /**
    * Fits a simple mean and standard deviation model. In other words it
    * calculates the mean and SD from a finite size sequence of samples.
    *
    * @param maxSamples number of samples (rows) of stream used for model
    *                   generation
    * @param cols column names
    */
  def fitWSD$(maxSamples: Int, cols: ColumnNames, i: Frame): Either[ADWError, Frame] = {
    Right( MeanWSD.fit(i, cols.cols.toList, maxSamples) )
  }

  /**
    * Fits a simple mean and standard deviation model. In other words it
    * calculates the mean and SD from a finite size sequence of samples.
    *
    * @param maxSamples number of samples (rows) of stream used for model
    *                   generation
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for a simple stats window model.
    */
  def fitWSD(maxSamples: Int, cols: String*): Func[Frame,Frame] =
    T( fitWSD$ _, maxSamples, ColumnNames(cols) )

  /**
    * Predict the binary class using simple mean and SD model. It generates
    * one prediction per sliding window. It generates a prediction per row.
    * If at least one row has a true (failure) prediction, then the window is
    * marked as failure.
    *
    * @param threshHold threshold used to test if a sample is a failure or not
    *          relative to the standard deviation (larger values, lower
    *          detection rates, mor false negatives)
    * @param cols column names
    */
  def predictWSD$(threshHold: ThreshHold, cols: ColumnNames, i: Frame): Either[ADWError, Frame] = {
    Right( MeanWSD.predict(i, cols.cols.toList, threshHold.t) )
  }

  /**
    * Predict the binary class using simple mean and SD model. It generates
    * one prediction per sliding window. It generates a prediction per row.
    * If at least one row has a true (failure) prediction, then the window is
    * marked as failure.
    *
    * @param threshHold threshold used to test if a sample is a failure or not
    *          relative to the standard deviation (larger values, lower
    *          detection rates, mor false negatives)
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for a simple stats predictor.
    */
  def predictWSD(threshHold: Double, cols: String*): Func[Frame,Frame] =
    T( predictWSD$ _, ThreshHold(threshHold), ColumnNames(cols) )

  /**
    * Evaluate prediction made using simple mean and SD model. The
    * prediction is compared to the sliding window label (if any anomaly
    * exists, the the prediction should also be of an anomaly). The
    * standard binary classification metrics are calculated and added to
    * the frame as a new column.
    *
    * @param dropSize number of samples to ignore before starting evaluation.
    *          Those are teh learn data-set. Any samples after that
    *          are the test data-set.
    * @param cols column names
    * @param i input stream
    * @return
    */
  def evaluateWSD$(dropSize: DropSize, cols: ColumnNames, i: Frame): Either[ADWError, Frame] = {
    val fl = i.drop(dropSize.l)
    Right( MeanWSD.evaluate(fl, cols.cols.toList) )
  }

  /**
    * Evaluate prediction made using simple mean and SD model. The
    * prediction is compared to the sliding window label (if any anomaly
    * exists, the the prediction should also be of an anomaly). The
    * standard binary classification metrics are calculated and added to
    * the frame as a new column.
    *
    * @param dropSize number of samples to ignore before starting evaluation.
    *          Those are teh learn data-set. Any samples after that
    *          are the test data-set.
    * @param cols column names
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors a simple predictor evaluator (binary classification).
    */
  def evaluateWSD(dropSize: Int, cols: String*): Func[Frame,Frame] =
    T( evaluateWSD$ _, DropSize(dropSize), ColumnNames(cols) )

  /* INEGI sFCC 1 */

  /**
    * Parameters for the [[fftStep]] task.
    * Removed fftNormalization - type of normalization to use with FFT
    * Cannot serialize to JSON
    *
    * @param fs            - sampling frequency used on the signal `sig`
    * @param pad           - if true extend the signal otherwise truncate it to have a size of power of 2
    */
  case class fftP(fs: Double, pad : Boolean) extends Param
  //case class fftP(fs: Double, pad : Boolean, fftNormalization: DftNormalization) extends Param

  /**
    * Calculates the FFT of a sliding window stored in `Val[Vector]`
    *
    * @param p FFT parameters [[fftP]]
    * @param cols compute FFT on these column names
    * @param col create new columns with the same original appended with this name
    * @param i input stream
    * @return either an error or a new frame with the new columns
    */
  def fftStep$(p: fftP, cols: ColumnNames, col: ColumnName, i: Frame): Either[ADWError, Frame] = {

    def fftName(n:String):String = n + col

    // row typing info
    // column name and type (store vector)
    val colInfo = cols.cols.map( c => fftName(c) -> Val(Vector(0.0)) )

    def op(i: Row): Row = {
      val tmp = i.filter(cols.cols:_*).map { case (k,v) =>
        // rename the column
        val nk = fftName(k)
        val vec = unpack[Vector[Double]](v)
        val nv = vec match {
          case Right(a) =>
            // Get FFT
            val arr = a.toArray
            val (_,_,nv) = FFT(p.fs, arr.length, arr, p.pad, DftNormalization.STANDARD)
            // Use only the magnitude
            val abs_nv = nv.map(_.abs())
            Val(abs_nv.toVector)
          case _ =>
            ErrVal(s"Expected a numerical vector but got: $v")
        }
        nk -> nv
      }
      // extend row with new data
      i ++ tmp
    }

    Right(i.map(op,colInfo:_*)) // map only replaces
  }

  /**
    * Calculates the FFT of a sliding window stored in `Val[Vector]`
    *
    * @param extend create new columns with the same original appended with this name
    * @param fs sampling frequency
    * @param pad either pad or don't pad last window
    * @param cols column names were to calculate FFT windows
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the FFT task [[fftStep]].
    */
  def fftStep(extend:String, fs: Double, pad : Boolean, cols: String*): Func[Frame,Frame] =
    T( fftStep$ _, fftP(fs, pad), ColumnNames(cols), ColumnName(extend) )


  /**
    * Parameters to generate FCC like filter bank. Applies a filter bank to
    * a signal. Assumes the signal is an FFT (magnitude only).
    *
    * @param name name of the filter to use. See dsp.Filter for more filters
    * @param windowLen the length of the signal window
    * @param sampleRate sampling rate of the
    * @param numFilters number of filters that bank should have
    * @param minFreq start bank at this frequency
    * @param maxFreq stop bank at this frequency
    */
  case class filterP(name: String, windowLen:Int, sampleRate:Double, numFilters:Int, minFreq:Double, maxFreq:Double) extends Param


  /**
    * Generates a bank of filters and applies the to the streams columns
    *
    * @param p filter bank set-up
    * @param cols columns names were filter banks are applied
    * @param i input stream
    * @return either an error or a new frame with converted columns
    */
  def filterBankStep$(p: filterP, cols: ColumnNames, i: Frame): Either[ADWError, Frame] = {

    /* See dsp.Filter for more filters */
    val filter: Vector[(Int, Int, Vector[Double])] = p match {
      case filterP("Hann", windowLen, sampleRate, numFilters, minFreq, maxFreq) =>
        prepareFilter(windowLen, sampleRate, numFilters, minFreq, maxFreq, initHann)
      case filterP("Triangle", windowLen, sampleRate, numFilters, minFreq, maxFreq) =>
        prepareFilter(windowLen, sampleRate, numFilters, minFreq, maxFreq, initTriangular(windowLen))
      case filterP("Bartlett", windowLen, sampleRate, numFilters, minFreq, maxFreq) =>
        prepareFilter(windowLen, sampleRate, numFilters, minFreq, maxFreq, initBartlett)
      case _ => throw new RuntimeException(s"filterBankStep: unknown filter $p")
    }

    // row typing info, all stays the same
    // map not given type parameter

    def op(i: Row): Row = {
      val tmp = i.filter(cols.cols:_*).map { case (k,v) =>
        val vv = unpack[Vector[Double]](v)
        // rename the column
        val nv = vv match {
          case Right(a) =>
            // apply filter
            val arr = a.toArray
            println(s"Filter bank size ${filter.size}")
            val filtered = applyFilterBank(filter)(arr)
            Val(filtered)
          case Left(e) =>
            ErrVal(s"Expected a numerical vector but got: $e")
        }
        k -> nv
      }
      // extend row with new data (replace because same column name)
      i ++ tmp
    }

    Right(i.map(op)) // map only replaces
  }

  /**
    * Generates a bank of filters and applies the to the streams columns
    *
    * @param p filter bank set-up
    * @param cols columns names were filter banks are applied
    * @return [[pt.inescn.search.stream.Pipes.Func]] constructors for the filtering task [[filterBankStep]].
    */
  def filterBankStep(fp: filterP, cols: String*): Func[Frame,Frame] =
    T( filterBankStep$ _, fp, ColumnNames(cols) )


}
