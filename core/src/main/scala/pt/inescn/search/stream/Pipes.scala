package pt.inescn.search.stream

import pt.inescn.search.stream.Tasks.Task
import pt.inescn.search.stream.UtilTasks.Nop

import scala.collection.AbstractIterator
import scala.language.existentials
//import scala.language.higherKinds

import pt.inescn.utils.ADWError
import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.UtilTasks.NOP
import pt.inescn.samplers.stream.Ops._
import pt.inescn.utils.Utils.{printStackTrace, time}

import scala.annotation.tailrec
import scala.concurrent.blocking
import scala.concurrent.duration.Duration

import monix.execution.Scheduler


object Pipes {

  /**
    * When compiling or executing the pipes, we record the order in which
    * the tasks are compiled or executed. The data that is recorded includes:
    * the name of the task, the name and parameters of the task and a
    * chronologically ordered list of the
    * [[pt.inescn.search.stream.Tasks.Task]]s.
    */
  trait Tracking {
    /**
      * Chronologically ordered tasks
      */
    val history: List[Task[_,_]]

    def +(tsk: Task[_,_]): Tracking

    def drop: (Task[_,_], Tracking)

    def dropLast: (Task[_,_], List[Task[_,_]]) = {
      // last task to do
      val last = history.last
      // remove the last entry
      val nhistory = history.drop(1)
      (last, nhistory)
    }

    def ++(trk: Tracking): Tracking
  }

  /**
    * When compiling the pipes, we record the order in which the tasks are
    * compiled (expected order of executed). The data that is recorded
    * includes: the name of the task, the name and parameters of the task
    * and a chronologically ordered list of the
    * [[pt.inescn.search.stream.Tasks.Task]]s.
    *
    * @param ids     set of task names used. No repetitions kept.
    * @param traces  set of task names and parameters used. No repetitions kept.
    * @param history chronologically ordered tasks (correct order)
    */
  case class Compiling(history: List[Task[_,_]]) extends Tracking {
    def +(tsk: Task[_,_]): Compiling = Compiling(tsk :: history)

    def drop: (Task[_,_], Compiling) = {
      val (last, nhistory) = dropLast
      (last, Compiling(nhistory))
    }

    def ++(trk: Tracking): Compiling = {
      val newHistory = history ++ trk.history
      Compiling(newHistory)
    }
  }

  /**
    * When executing the pipes, we record the order in which the tasks are
    * executed (reverse order). The data that is recorded includes: the name
    * of the task, the name and parameters of the task and a chronologically
    * ordered list of the [[pt.inescn.search.stream.Tasks.Task]]s.
    *
    * @param ids     set of task names used. No repetitions kept.
    * @param traces  set of task names and parameters used. No repetitions kept.
    * @param history chronologically ordered tasks (correct order)
    */
  case class Executing(history: List[Task[_,_]]) extends Tracking {
    def +(tsk: Task[_,_]): Executing = Executing(tsk :: history)

    def drop: (Task[_,_], Executing) = {
      val (last, nhistory) = dropLast
      (last, Executing(nhistory))
    }

    def ++(trk: Tracking): Executing = {
      val newHistory = history ++ trk.history
      Executing(newHistory)
    }
  }

  /**
    * Compile-time tracking constructors.
    */
  object Compiling {
    def apply(): Compiling = new Compiling(List[Task[_,_]]())
  }

  /**
    * Run-time tracking constructors.
    */
  object Executing {
    def apply(): Executing = new Executing(List[Task[_,_]]())
    def apply(tsk: Task[_,_]): Executing = new Executing(List(tsk))
  }


  import OpCheck._

  /**
    * AST representing various operations and how they are chained. This AST
    * is then compiled into one or more partial functions. Each partial
    * function represents a linearized (sequential pipe) that make up the
    * original pipe description. Successful compilation generates all linear
    * pips contained in the AST pipe description.
    *
    * ISSUE: scalac does ot handle types correctly. I cannot use lists or
    * sequences with the type Op[_,Nothing,_]. The nothing causes problems.
    * Tests with Null and Any seem to work.
    *
    * @see https://users.scala-lang.org/t/type-failure-with-list-of-container-typed-with-nothing-dotty-works/3084
    *
    */
  sealed trait Op[I,O]

  /**
    * Does nothing. Used as a temporary placeholder.
    *
    * @param s message for debug/tracking purposes
    */
  final case class NoOp[I,O](s: String) extends Op[I,O]

  /**
    * Basic operation that execute a task (function). A task produces output.
    * When generating outputs the task is given the opportunity to check and
    * alter its parameters. This allows tasks to set-up their own parameters
    * that can then be (re)used later.
    *
    * @see [[pt.inescn.search.stream.Tasks.Task]]
    * @param t task that generates output or aggregates results
    * @tparam I type parameter of the task input
    * @tparam O type parameter of the task output
    */
  final case class Func[I,O](t: Transformer[I,O]) extends Op[I,O]

  /**
    * Basic operation that execute a task (function).
    * This task aggregates results of previous tasks.
    *
    *
    * @see [[pt.inescn.search.stream.Tasks.Task]]
    * @param t task that generates output or aggregates results
    * @tparam I type parameter of the task input
    * @tparam O type parameter of the task output
    */
  final case class Agg[I,O](t: Aggregator[I,O]) extends Op[I,O]

  /**
    * Enumerates a list of operations to be executed. Allows the user to
    * express the existence of several separate tasks that may be chained
    * with other tasks in different ways. They can be chained in pairs
    * with [[SeqOp]] using the same order of the operations or with
    * [[CrossOp]] to generate all possible combinations.
    *
    * @param f list of operations to chain
    */
  final case class All[I,O](f: Iterable[Op[I,O]]) extends Op[I,O]

  /**
    * Chains two lists of operations by composing each pair of operations in
    * the lists into a single operation in the order they appear. If the left
    * or right hand side of operands have do not have the same length, they
    * can be extended by the compiler (according to some rules).
    *
    * @param f1 list of operations to chain in left hand side
    * @param f2 list of operations to chain in right hand side
    */
  protected[stream] final case class SeqOp[I,T,O](f1: Iterable[Op[I,T]], f2: Iterable[Op[T,O]]) extends Op[I,O]

  /**
    * Chains two lists of operations by composing each pair of operations in
    * the lists into a single operation. All possible pairs are generated
    * (cross product). No requirements should be imposed on the operands
    * list length (save for memory and CPU concerns).
    *
    * @param f1 list of operations to chain in left hand side
    * @param f2 list of operations to chain in right hand side
    */
  protected[stream] final case class CrossOp[I,T,O](f1: Iterable[Op[I,T]], f2: Iterable[Op[T,O]]) extends Op[I,O]

  /**
    * Does a runtime check on the current runtime tracking, input and last
    * executed task. If the check returns true then the checked operation
    * is executed and its results returned. If it is false, then a
    * run-time error is returned.
    *
    * @param iff runtime check if checked operation should be executed
    * @param f   checked operation
    */
  final case class Iff[I,O](iff: Fiff, f: Op[I,O]) extends Op[I,O]

  /**
    * Does a compile-time check on the current compile tracking. If the
    * check returns true then the checked operation is compiled and
    * the corresponding linear pipe is returned otherwise a compile
    * time error is returned.
    *
    * @param when runtime check if checked operation should be executed
    * @param f    checked operation
    */
  final case class When[I,O](when: Fwhen, f: Op[I,O]) extends Op[I,O]

  /**
    * All related operations in `f` are execute and the results collected.
    * Each element of the collected data is passed on to a task that can
    * aggregate, filter or select a single output.
    *
    * All related operations are executed in sequence and in a single thread
    * if `parallelism <= 0` otherwise  concurrent (multiple thread) execution
    * is performed and no ordering is maintained.
    *
    * @param f operations from which to collect outputs
    * @param p set-up of the aggregate executor (parallel, sequential)
    * @tparam I Input type to executing pipe(s)
    * @tparam T Output type to executing pipe(s). Also the
    *           input type to aggregating task
    * @tparam O Output type to the aggregating task.
    */
  final case class Aggregate[I,T,O](f: Op[I,T], p: Par[T,O]) extends Op[I,O]

  /**
    * All related operations in `f` are repeated `n` over and the results
    * collected. Each element of the collected data is passed on to a task
    * that can aggregate, filter or select a single output.
    *
    * All related operations are executed in sequence and in a single thread
    * if `parallelism <= 0` otherwise  concurrent (multiple thread) execution
    * is performed and no ordering is maintained.
    *
    * @param f operations repeated and from which to collect outputs
    * @param p set-up of the aggregate executor (parallel, sequential)
    * @tparam I Input type to executing pipe(s)
    * @tparam T Output type to executing pipe(s). Also the
    *           input type to aggregating task
    * @tparam O Output type to the aggregating task.
    */
  final case class Repeat[I,T,O](n: Int, f:Op[I,T], p:Par[T,O]) extends Op[I,O]


  /**
    * A [[All]] constructor to ease DSL use.
    */
  object All {
    def apply[I,O](ops: Op[I,O]*): All[I,O] = new All(ops.toIterable)
  }

  /**
    * A [[Iff]] constructor to ease DSL use.
    */
  object Iff {
    def apply[I,O](iff: Fiff, ops: Op[I,O]*): Iff[I,O] = {
      val lOps = ops.toList
      if (lOps.size <= 0)
        new Iff(iff, NoOp("All(empty)"))
      else if (lOps.size == 1)
        new Iff(iff, lOps.head)
      else
        new Iff(iff, All(ops.toIterable))
    }
  }

  /**
    * A [[When]] constructor to ease DSL use.
    */
  object When {
    def apply[I,O,A](when: Fwhen, ops: Op[I,O]*): When[I,O] = {
      val lOps = ops.toList
      if (lOps.size <= 0)
        new When(when, NoOp("All(empty)"))
      else if (lOps.size == 1)
        new When(when, lOps.head)
      else
        new When(when, All(ops.toIterable))
    }
  }


  /**
    * This class holds configuration information on th execution of the task.
    *
    * @param t aggregation task
    * @param parallelism maximum number of threads to use
    * @param duration time to wait for processing the partial functions
    * @param verbose verbosity of output (for debug)
    * @param s Monix scheduler
    * @tparam I Input type to task
    * @tparam O Output type to the task.
    * @tparam A Aggregation of task
    */
  case class Par[I,O](t: Aggregator[I,O], parallelism: Int, duration: Duration, verbose:Int, s: Scheduler)

  /**
    * Helper builders of an aggregation task. An aggregation task is used to aggregate
    * the result of one or pipes. By default if only a [[Task]] or [[Func]] are used
    * to initialize [[Par]], then linear (non-concurrent) execution is set-up. If the
    * `parallelism` value (maximum number of threads to use) is greater than one, then
    * multi.-threading is used (no ordering is maintained).
    */
  object Par {

    val waitFor: Duration = Duration.Inf // 5000 millis
    val verbose: Int = 0  // > 0 to print output
    val cores: Int = Runtime.getRuntime.availableProcessors // default nº CPU
    val scheduler: Scheduler = monix.execution.Scheduler.Implicits.global  // could be used implicitly

    /**
      * Configure a task to execute aggregation of pipes in a single thread.
      *
      * @param t aggregation task
      * @tparam I Input type to task
      * @tparam T Output type to the task.
      * @tparam O Aggregation of task
      * @return a [[Par]] with `parallelism` set to 0
      */
    def apply[I,O](t: Aggregator[I,O]): Par[I,O] =
      new Par(t, cores, waitFor, verbose, scheduler)

    /**
      * Configure a task to execute aggregation of pipes in a single thread.
      *
      * @param func aggregation task
      * @tparam I Input type to task
      * @tparam T Output type to the task.
      * @tparam O Aggregation of task
      * @return a [[Par]] with `parallelism` set to 0
      */
    def apply[I,O](func: Agg[I,O]): Par[I,O] =
      new Par(func.t, cores, waitFor, verbose, scheduler)

    /**
      * Configure a task to execute aggregation of pipes concurrently.
      * Must explicitly set `parallelism` to a value greater than 0.
      *
      * @param func [[Func]] tha holds the aggregation [[Task]]
      * @param parallelism maximum number of threads to use
      * @param duration time to wait for processing the partial functions
      * @param verbose verbosity of output (for debug)
      * @param s Monix scheduler
      * @tparam I Input type to task
      * @tparam T Output type to the task.
      * @tparam O Aggregation of task
      * @return a [[Par]] with `parallelism` set to greater than 0
      */
    def apply[I,O](func: Agg[I,O],
                       parallelism: Int = cores,
                       duration: Duration = waitFor,
                       verbose:Int = verbose)
                      (implicit s: Scheduler): Par[I,O] =
      new Par(func.t,parallelism,duration,verbose,s)
  }


  /**
    * Companion object of aggregation [[Op]]. If a [[Task]] is provided but
    * no [[Par]] is provided, then the execution is set-up to be sequential.
    */
  object Aggregate {

    def apply[I,T,O](f: Op[I,T], t: Agg[T,O]): Aggregate[I,T,O] = {
      //val p = Par(t)
      val p = Par(t, parallelism = 0)(Par.scheduler)
      new Aggregate(f, p)
    }

    def apply[I,T,O](f: Op[I,T], p: Par[T,O]): Aggregate[I,T,O] = {
      new Aggregate(f, p)
    }
  }

  /**
    * Companion object of repetition [[Op]]. If a [[Task]] is provided but
    * no [[Par]] is provided, then the execution is set-up to be sequential.
    */
  object Repeat {

    def apply[I,T,O](n: Int, f: Op[I,T], t: Agg[T,O]): Repeat[I,T,O] = {
      val p = Par(t, parallelism = 0)(Par.scheduler)
      new Repeat(n, f, p)
    }

    def apply[I,T,O](n: Int, f: Op[I,T], p: Par[T,O]): Repeat[I,T,O] = {
      new Repeat(n, f, p)
    }
  }

  /* DSL */

  /**
    * Domain Specific Language (DSL) used to construct the pipes. It allows
    * a user to chain the execution of two (simple or complex) operations
    * [[Op]]. Depending on the chaining operator we may have a pipes
    * descriptor that represents one or more sequential (independent) pipes.
    * Each of these linear pipes is generated during compilation and reified
    * as a single partial function.
    *
    * The operators are right associative.
    *
    * @param o right hand side operand
    */
  implicit class OpOps[I,T,O](o: Op[T,O]) {

    /**
      * Executes two operations in sequence. Depending on the operand one
      * or more pipes may be generated. Any [[All]] generates one sequence
      * pipe per [[All]]'s operation. The [[SeqOp]] and [[CrossOp]] operands
      * will pair operations in the same order as listed. [[Func]] and
      * [[Aggregate]] represent a single operation and will only be
      * duplicated if combined with the [[All]], [[SeqOp]] or [[CrossOp]]
      * operands.
      *
      * Finally conditional compilation [[When]] and runtime conditional
      * execution [[Iff]] will generate one sequence per operation that
      * is references be it a single [[Func]], multiple operations [[All]]
      * or combinations of [[SeqOp]] and [[CrossOp]].
      *
      * @param that right hand side operand
      * @return sequential execution of two operations
      */
    def ->:(that: Op[I,T]): Op[I,O] = (that, o) match {
      case (Func(_), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Func(_), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Func(_), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (Func(_), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Func(_), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Func(_), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Func(_), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Func(_), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (Func(_), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (Agg(_), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Agg(_), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Agg(_), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (Agg(_), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Agg(_), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Agg(_), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Agg(_), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Agg(_), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (Agg(_), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (Iff(_, _), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Iff(_, _), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Iff(_, _), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (Iff(_, _), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Iff(_, _), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Iff(_, _), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Iff(_, _), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Iff(_, _), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (Iff(_, _), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (When(_, _), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (When(_, _), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (When(_, _), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (When(_, _), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (When(_, _), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (When(_, _), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (When(_, _), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (When(_, _), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (When(_, _), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (All(f1s), Func(_)) => SeqOp(f1s, Iterable(o))
      case (All(f1s), Agg(_)) => SeqOp(f1s, Iterable(o))
      case (All(f1s), All(f2s)) => SeqOp(f1s, f2s)
      case (All(f1s), SeqOp(_, _)) => SeqOp(f1s, Iterable(o))
      case (All(f1s), CrossOp(_, _)) => SeqOp(f1s, Iterable(o))
      case (All(f1s), Iff(_, _)) => SeqOp(f1s, Iterable(o))
      case (All(f1s), When(_, _)) => SeqOp(f1s, Iterable(o))
      case (All(f1s), Aggregate(_,_)) => SeqOp(f1s, Iterable(o))
      case (All(f1s), Repeat(_,_,_)) => SeqOp(f1s, Iterable(o))

      case (SeqOp(_, _), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (SeqOp(_, _), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (CrossOp(_, _), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (CrossOp(_, _), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (Aggregate(_,_), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (Aggregate(_,_), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (Repeat(_,_,_), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (Repeat(_,_,_), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (NoOp(s), _) => NoOp(s"-> applied to $that is a NoOp($s)")
      case (_, NoOp(s)) => NoOp(s"-> applied to $that is a NoOp($s)")
    }

    /**
      * Executes two operations in sequence. Generates all possible
      * combinations of two multiple operation operands. Multiple operation
      * operands include [[All]], [[CrossOp]] or any conditional [[Iff]] or
      * [[When]] that reference a multiple operation operand. No other
      * operands are possible.
      *
      * @param that right hand side operand
      * @return sequential execution of two operations
      */
    def *:(that: Op[I,T]): Op[I,O] = (that, o) match {
      case (Func(_), Func(_)) => CrossOp(Iterable(that), Iterable(o))
      case (Func(_), Agg(_)) => CrossOp(Iterable(that), Iterable(o))
      case (Func(_), All(f2s)) => CrossOp(Iterable(that), f2s)
      case (Func(_), SeqOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Func(_), CrossOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Func(_), Iff(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Func(_), When(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Func(_), Aggregate(_,_)) => CrossOp(Iterable(that), Iterable(o))
      case (Func(_), Repeat(_, _,_)) => CrossOp(Iterable(that), Iterable(o))

      case (Agg(_), Func(_)) => CrossOp(Iterable(that), Iterable(o))
      case (Agg(_), Agg(_)) => CrossOp(Iterable(that), Iterable(o))
      case (Agg(_), All(f2s)) => CrossOp(Iterable(that), f2s)
      case (Agg(_), SeqOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Agg(_), CrossOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Agg(_), Iff(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Agg(_), When(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Agg(_), Aggregate(_,_)) => CrossOp(Iterable(that), Iterable(o))
      case (Agg(_), Repeat(_,_,_)) => CrossOp(Iterable(that), Iterable(o))

      case (Iff(_, _), Func(_)) => CrossOp(Iterable(that), Iterable(o))
      case (Iff(_, _), Agg(_)) => CrossOp(Iterable(that), Iterable(o))
      case (Iff(_, _), All(f2s)) => CrossOp(Iterable(that), f2s)
      case (Iff(_, _), SeqOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Iff(_, _), CrossOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Iff(_, _), Iff(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Iff(_, _), When(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (Iff(_, _), Aggregate(_,_)) => CrossOp(Iterable(that), Iterable(o))
      case (Iff(_, _), Repeat(_,_,_)) => CrossOp(Iterable(that), Iterable(o))

      case (When(_, _), Func(_)) => CrossOp(Iterable(that), Iterable(o))
      case (When(_, _), Agg(_)) => CrossOp(Iterable(that), Iterable(o))
      case (When(_, _), All(f2s)) => CrossOp(Iterable(that), f2s)
      case (When(_, _), SeqOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (When(_, _), CrossOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (When(_, _), Iff(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (When(_, _), When(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (When(_, _), Aggregate(_,_)) => CrossOp(Iterable(that), Iterable(o))
      case (When(_, _), Repeat(_,_,_)) => CrossOp(Iterable(that), Iterable(o))

      case (All(f1s), Func(_)) => CrossOp(f1s, Iterable(o))
      case (All(f1s), Agg(_)) => CrossOp(f1s, Iterable(o))
      case (All(f1s), All(f2s)) => CrossOp(f1s, f2s)
      case (All(f1s), SeqOp(_, _)) => CrossOp(f1s, Iterable(o))
      case (All(f1s), CrossOp(_, _)) => CrossOp(f1s, Iterable(o))
      case (All(f1s), Iff(_, _)) => CrossOp(f1s, Iterable(o))
      case (All(f1s), When(_, _)) => CrossOp(f1s, Iterable(o))
      case (All(f1s), Aggregate(_,_)) => CrossOp(f1s, Iterable(o))
      case (All(f1s), Repeat(_,_,_)) => CrossOp(f1s, Iterable(o))

      case (SeqOp(_, _), Func(_)) => CrossOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), Agg(_)) => CrossOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), All(f2s)) => CrossOp(Iterable(that), f2s)
      case (SeqOp(_, _), SeqOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), CrossOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), Iff(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), When(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), Aggregate(_,_)) => CrossOp(Iterable(that), Iterable(o))
      case (SeqOp(_, _), Repeat(_,_,_)) => CrossOp(Iterable(that), Iterable(o))

      case (CrossOp(_, _), SeqOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Func(_)) => CrossOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Agg(_)) => CrossOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), All(f2s)) => CrossOp(Iterable(that), f2s)
      case (CrossOp(_, _), CrossOp(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Iff(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), When(_, _)) => CrossOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Aggregate(_,_)) => CrossOp(Iterable(that), Iterable(o))
      case (CrossOp(_, _), Repeat(_,_,_)) => CrossOp(Iterable(that), Iterable(o))

      case (Aggregate(_,_), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (Aggregate(_,_), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (Aggregate(_,_), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (Repeat(_,_,_), Func(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), Agg(_)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), All(f2s)) => SeqOp(Iterable(that), f2s)
      case (Repeat(_,_,_), SeqOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), CrossOp(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), Iff(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), When(_, _)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), Aggregate(_,_)) => SeqOp(Iterable(that), Iterable(o))
      case (Repeat(_,_,_), Repeat(_,_,_)) => SeqOp(Iterable(that), Iterable(o))

      case (NoOp(s), _) => NoOp(s"-> applied to $that is a NoOp($s)")
      case (_, NoOp(s)) => NoOp(s"-> applied to $that is a NoOp($s)")
    }

    /**
      * Allows the linear execution of all sequential pipes represented by
      * the left hand side operand. All the outputs are collected and passed
      * on to this right hand side. This allows for aggregation, selection
      * and filtering operations. For example selecting the best output of
      * a set of linearized pipes.
      *
      * @param that right hand side operand
      * @return sequential execution of two operations
      */
    def |:(that: Op[I,T]): Op[I,O] = (that, o) match {
      case (op:Op[I,T], f:Agg[T,O]) =>
        val seq = Par(f, parallelism = 0)(Par.scheduler)
        val tmp = Aggregate(op, seq)
        tmp
      case _ => NoOp(s"|: can only be applied to a Func(_) construct, but here was used with $that *: $o")
    }

  }


  implicit class AggOps[I,T,O](o: Par[T,O]) {
    /**
      * Allows the concurrent execution of all sequential pipes represented by
      * the left hand side operand. All the outputs are collected and passed
      * on to this right hand side **in any order**. This allows for aggregation,
      * selection and filtering operations. For example selecting the best output of
      * a set of linearized pipes.
      *
      * Note: concurrent execution is performed using a multi-thread library
      * To set the concurrent execution related parameters see [[Aggregate]]
      *
      * @param that right hand side operand
      * @return concurrent execution of two operations
      */
    def ||:(that: Op[I,T]): Op[I,O] = (that, o) match {
      case (op:Op[I,T], p:Par[T,O]) =>
        val tmp = Aggregate(op,p)
        tmp
      case _ => NoOp(s"||: can only be applied to a Func(_) construct, but here was used with $that *: $o")
    }

  }


    /* Partial evaluation */

  type PartialFunc[I,O] = (Executing, I) => (Either[ADWError,O], Executing)

  /**
    * This class holds the compilation result. The compilation result consist
    * of a list of partial functions that takes a parameter and input and
    * return an output. It also holds a compilation track indicating all of
    * the compiles [[Func]] sub-expressions. This allows us to know what tasks
    * have been compiled and in what order thy will execute.
    *
    * @param fs list of compiled partial functions
    */
  sealed case class Exec[I,O](fs: PartialFunc[I,O], track:Compiling)

  /**
    * Creates a partial function that composes the two partials `f1` and
    * `f2`. `f1` is executed before `f2`. Compilation tracking is updated.
    *
    * @param f1 compiled partial function
    * @param trk1 current compilation tracking after `f1` was compiled
    * @param f2 compiled partial function
    * @param trk2 current compilation tracking after `f2` was compiled
    * @return composed partial function and the updated compilation tracking
    */
  private def compose[I1,I2,O](f1: PartialFunc[I1,I2], trk1: Compiling, f2: PartialFunc[I2,O], trk2:Compiling):
  (PartialFunc[I1,O], Compiling) = {
    val newTrk = trk1 ++ trk2
    val partial = (trk: Executing, i:I1) => {
      try {
        // execute first task
        val (r1, track1) = f1(trk,i)
        r1.fold(
          { e => (Left(e), track1) },
          { o1 =>
            // execute next task
            val (r2, track2) = f2(track1, o1)
            r2.fold(
              { e => (Left(e), track2) },
              { o2 => (Right(o2), track2) }
            )
        })
      } catch {
        case e:Throwable =>
          (Left(ADWError(s"compose:\n"+printStackTrace(e))), trk )// fail silently
      }
    }
    (partial,newTrk)
  }

  private def zipIter[I1,I2,O](e1: Iterator[Either[ADWError, Exec[I1,I2]]], e2:Iterator[Either[ADWError, Exec[I2,O]]]):
  Iterable[(Either[ADWError, Exec[I1, I2]], Either[ADWError, Exec[I2, O]])] = {
    val iterable = new Iterable[(Either[ADWError, Exec[I1, I2]], Either[ADWError, Exec[I2, O]])] {

      override def iterator: Iterator[(Either[ADWError, Exec[I1, I2]], Either[ADWError, Exec[I2, O]])] = {
        val iter = new AbstractIterator[(Either[ADWError, Exec[I1, I2]], Either[ADWError, Exec[I2, O]])] {

          override def hasNext: Boolean = e1.hasNext || e2.hasNext

          override def next(): (Either[ADWError, Exec[I1, I2]], Either[ADWError, Exec[I2, O]]) = {
            if (e1.hasNext && e2.hasNext){
              (e1.next(), e2.next())
            } else if (e1.hasNext) {
              (e1.next(), Left(ADWError(s"zipIter: no right pair available")))
            } else {
              (Left(ADWError(s"zipIter: no left pair available")), e2.next())
            }
          } // next
        }
        iter
      }
    }
    iterable
  }

  /**
    * Generates a set of partial functions by composing a pair of partials,
    * one from each list in the already compiled `e1` and `e2` results. It
    * assumes that the number of partial functions in both lists are the
    * same. Partials in `e1` are executed before `e2`.
    *
    * @param e1 list of compiled partial functions
    * @param e2 list of compiled partial functions
    * @return
    */
  private def compose[I1,I2,O](e1: Iterator[Either[ADWError, Exec[I1,I2]]], e2:Iterator[Either[ADWError, Exec[I2,O]]]):
  Iterable[Either[ADWError,Exec[I1,O]]] = {
    zipIter(e1,e2).map{ case (i1,i2) =>
      (i1,i2) match {
        case (Left(e), _) => Left(e)
        case (_,Left(e)) => Left(e)
        case (Right(Exec(op1,trk1)),Right(Exec(op2,trk2))) =>
          val tmp: (PartialFunc[I1, O], Compiling) = compose(op1, trk1, op2, trk2)
          Right(Exec(tmp._1, tmp._2))
      }
    }.toIterable
  }


  /**
    * Generates a set of partial functions that represent the composition
    * of all possible combinations (cross product) of the partial functions
    * in `e1` and `e2`. Partials in `e1` are executed before `e2`.
    *
    * @param e1 list of compiled partial functions
    * @param e2 list of compiled partial functions
    * @return cross product composition of the partial functions in `e1`
    *         and `e2`
    */
  private def cross[I1,I2,O](e1: Iterable[Either[ADWError, Exec[I1,I2]]], e2:Iterable[Either[ADWError, Exec[I2,O]]]):
  Iterable[Either[ADWError,Exec[I1,O]]] = {
    val ops = e1.flatMap { i1 =>
      val tmp = i1 match {
        case Left(e) =>
          Iterable(Left(e))
        case Right(Exec(op1,trk1)) =>
          val rr = e2.map {
            case Left(e) =>
              Left(e)
            case Right(Exec(op2, trk2)) =>
              val tmp = compose(op1, trk1, op2, trk2)
              Right(Exec(tmp._1, tmp._2))
          }
          rr
      }
      tmp
    }
    ops
  }

  /**
    * Creates a standard (simple) partial function that executes a
    * transformation task `t`. It first allows the task to check and
    * alter its parameters based on the input `i`. If a valid
    * re-parameterized task is returned it is executed. The current
    * execution trace is updated with the task `t` and both the tasks
    * output and its trace are returned.
    *
    * Exceptions are automatically handled by this function (returned as
    * errors).
    *
    * @param t task to be executed
    * @tparam I type of output from previous task, input for this task
    * @tparam O type of output of this task
    * @return tasks output and updated runtime trace
    */
  def partial[I,O](t:Transformer[I,O]): PartialFunc[I,O] = (trk: Executing, i:I) => {
    try {
      val tmp: Either[ADWError, (Executing, O)] = for {
        // Pre-processing: generate the parameters if need be
        new_t <- t.pre(i)
        // Execute the task
        o <- new_t.f(i)
        // runtime trace: update trace of the execution
        ntrk = trk + new_t
      } yield (ntrk, o)
      tmp.fold( e => (Left(e), trk), { case (track,o) => (Right(o), track) } )
    } catch {
      case e: Throwable =>
        (Left(ADWError(s"${t.trace}:\n" + printStackTrace(e))), trk) // fail silently
    }
  }

  /**
    * Creates a standard (simple) partial function that executes an aggregation
    * task `t`. This is **not** the intended use of such a function. However we
    * add this for completeness. Here we execute the `collect` by: a) providing
    * a zero value for the accumulator, b) record the execution time of the
    * aggregated task as 0 and c) use the pipe's current racking as the
    * aggregation tracking.
    *
    * Exceptions are automatically handled by this function (returned as
    * errors).
    *
    * @param t task to be executed
    * @tparam I type of output from previous task, input for this task
    * @tparam O type of output of this task
    * @return tasks output and updated runtime trace
    */
  def partial[I,O](t:Aggregator[I,O]): PartialFunc[I,O] = (trk: Executing, i:I) => {
    try {
      // Execute the task
      val o = t.collect(t.zero, Right(i), 0, trk)
      // runtime trace: update trace of the execution
      val ntrk = trk + t
      val tmp: Either[ADWError, (Executing, O)] = Right((ntrk, o))
      tmp.fold( e => (Left(e), trk), { case (track,o) => (Right(o), track) } )
    } catch {
      case e: Throwable =>
        (Left(ADWError(s"${t.trace}:\n" + printStackTrace(e))), trk) // fail silently
    }
  }

  /**
    * Creates a partial function that checks in runtime via `iff`, if the
    * enclosed task `t` (encoded as the partial function `f`) should or
    * should not be executed. If is should not be executed, an error is
    * returned as the output. Such errors should either be ignored or
    * analysed for debug purposes. If it should be executed, then the
    * partial `f` is executed and its results returned.
    *
    * Exceptions are automatically handled by this function (returned as
    * errors).
    *
    * @param iff runtime check if task `t` should or should not be executed
    * @param f implementation of the task that is already compiled
    * @param t task that is tested for execution
    * @return
    *
    */
  def partial[I,O](iff: (Executing,I,Task[_,_]) => Boolean, f:PartialFunc[I,O], t:Task[_,_]):
  PartialFunc[I,O] =
    (trk: Executing, i:I) => {
    try {
      // Should we execute?
      if (iff(trk,i,t)) {
        // yes, execute the task
        f(trk,i)
      } else
        (Left(ADWError(s"iff(Exec($trk)) == false")), trk)
    } catch {
      case e:Throwable =>
        val traces = trk.history.map{t => s"${t.name}(${t.params.values.toString})"}
        (Left(ADWError(s"$traces:\n"+printStackTrace(e))), trk) // fail silently
    }
  }

  /**
    * Aggregates the trace of executions when aggregating results of a
    * list of partial functions. If the executed function produces an
    * error then that error is returned. If it produces a result with
    * a valid trace, we add the aggregating task `t` to the trace.
    *
    * @param out output to be aggregated
    * @param t task that will be added to the trace if the output
    *          was valid (no error)
    * @return an error or the execution output with the updated trace
    */
  def addTracking[I,O](out: (Either[ADWError,I],Executing), t:Task[I,O]):
  (Either[ADWError,I], Executing) =
    out._1 match {
      case Left(_) => out
      case Right(v) =>
        val n_exec = out._2 + t
        (Right(v), n_exec)
    }

  /**
    * This function executes each function in `npairs` providing the input
    * `i` and then passes the result onto the the `t` task in order to
    * aggregate the results. the aggregating task `t` must be able to
    * deal with error output. Currently execution tracking is only
    * accumulated on successful executions.
    *
    * @param npairs functions to execute and whose outputs will be aggregated
    * @param t aggregating task
    * @param trk current execution tracking
    * @param i inout to consume
    * @param acc aggregation of results by `t`
    * @return
    */
  @tailrec
  private def accumulate[I,T,O](npairs: Iterator[Either[ADWError, Exec[I,T]]], t:Aggregator[T,O],
                                  trk: Executing,
                                  i:I,
                                  acc:O, verbose:Int, id:Int): O = {
    if (!npairs.hasNext)
      acc
    else {
      val f = npairs.next()
      f match {
        case Left(ADWError(e)) =>
          printlnVerbose(verbose, s"Experiment $id no executed. ERROR = $e")
          acc
        case Right(Exec(h,_)) =>
          printlnVerbose(verbose, s"Setting-up experiment: $h = $id")
          val (rr: (Either[ADWError, T], Executing), t1) = time( h(trk,i) )
          printlnVerbose(verbose, s"Set-up of experiment $id (${rr._2.history}) took ${t1 / 1.0e9} seconds")

          val (read1, exec) = rr
          val nacc = read1 match {
            case Right(o) =>
              t.collect(acc,Right(o),t1,exec)
            case Left(err) =>
              t.collect(acc,Left(err),t1,exec)
          }
          printlnVerbose(verbose,s"Evaluating task $id that took (CPU = ${t1/1.0e9} seconds)")
          accumulate(npairs,t,trk,i,nacc,verbose,id+1)
      }
    }
  }

  /**
    * Creates a partial function that aggregates results from a list of
    * partial functions `npairs` using the the task `t` to accumulate those
    * results. The aggregating task `t` must deal with errors returned by
    * executed partial functions.
    *
    * Exceptions are automatically handled by this function (returned as
    * errors).
    *
    * @param npairs functions whose results will be aggregated
    * @param t task that will aggregate the results
    * @return a partial function that take an input used to execute a list
    *         of partial functions and returns a single aggregates result
    *         based on those results
    */
  def partials[I,T,O](npairs: Iterator[Either[ADWError, Exec[I,T]]], t:Aggregator[T,O], verbose:Int): PartialFunc[I,O] = {
    (trk: Executing, i: I) => {
      try {
        ( Right(accumulate(npairs, t, trk, i, t.zero, verbose,id = 0)), Executing(t) )
      } catch {
        case e: Throwable =>
          ( Left(ADWError(s"${t.trace}:\n" + printStackTrace(e))), trk) // fail silently
      }
    }
  }



  // Using monix

  // Don't mix with our Task
  import monix.eval.{Task => MTask}
  import monix.reactive.Observable
  import monix.execution.CancelableFuture
  import scala.concurrent.Await
  import scala.concurrent.duration._
  import monix.execution.Scheduler

  def printlnVerbose[A](verbose:Int, a:A): Unit = if (verbose > 0) blocking( println(a) )

  /**
    * Here we generate a Monix Task that allows us to wrap and execute a
    * partial function concurrently. We use our task to process the partial
    * function's output (kicks off long running process) and then that
    * output with the trace of the execution and time of execution.
    *
    * @param t Task that aggregates the results
    * @param trk current execution tracking used for logging the pipe execution
    * @param verbose verbosity of output (for debug)
    * @param count ID assigned to the task
    * @param f long running partial function to execute
    * @param i the input of the partial function
    * @tparam I input to partial functions
    * @tparam T task input type (output of partial functions)
    * @tparam O task output type
    * @tparam A task aggregation type (resulting pipe output)
    * @return the Monix Task that returns the Task aggregation, the
    *         pipe execution trace, and the time in nanoseconds used
    *         to execute the pipe and then to execute the task on
    *         its output.
    */
  def execMAggregation[I,T]( trk: Executing, verbose:Int = 0)
                             ( count:Int, f: Either[ADWError, Exec[I,T]], i:I):
  MTask[(Either[ADWError,T], Executing, Int, Long)]
  = {
    // create a Monix task, it is executed concurrently
    MTask {

      f match {
        case Left(e) =>
          printlnVerbose(verbose, s"Experiment $count no executed. ERROR = $e")
          (Left(e), trk, count, 0)
        case Right(Exec(func,_)) =>
          printlnVerbose(verbose, s"Setting-up experiment $count: $f")

          // execute the pipe (may be long running)
          val (result: (Either[ADWError, T],Executing), t1) = time( func(trk, i) )
          printlnVerbose(verbose, s"Set-up of experiment $count took ${t1 / 1.0e9} seconds")

          val (read1, exec) = result
          // Return the output, the tracking information and the processing time
          // processing time are the pipe execution and task processing times
          (read1, exec, count, t1)
      }
    }

  }

  /**
    * Takes a stream of partial functions and executes each one concurrently.
    * The Monix scheduler and execution models determine how these functions
    * are executed. A aggregation task `t` kicks off the partial function
    * execution if it is lazy and then aggregates these results into a single
    * output.
    *
    * The partial functions are executed as Monix tasks. No ordering
    * is maintained.
    *
    * @see https://monix.io/docs/3x/tutorials/parallelism.html
    *
    * @param npairs stream of long running partial functions to execute
    * @param t Task that aggregates the results
    * @param trk current execution tracking used for logging the pipe execution
    * @param i the input of the partial function
    * @param parallelism maximum number of threads to use
    * @param duration time to wait for processing the partial functions
    * @param verbose verbosity of output (for debug)
    * @param s Monix scheduler
    * @tparam I input to partial functions
    * @tparam T task input type (output of partial functions)
    * @tparam O task aggregation type (resulting pipe output)
    * @return aggregated result of type `O`
    */
  def execMTask[I,T,O](npairs: Iterator[Either[ADWError, Exec[I,T]]], t:Aggregator[T,O],
                         trk: Executing,
                         i:I,
                         parallelism: Int, duration: Duration, verbose:Int)
                        (implicit s: Scheduler): O = {

    // Give each pipe an experiment ID
    val experiments = Stream.from(1) zip npairs.toIterable
    // Prepare the Monix execute loop
    val source: Observable[(Int, Either[ADWError, Exec[I,T]])] = Observable.fromIterable(experiments)
    // Prepare the aggregation task `t` for pipe with tracking history `trk`
    val agg: (Int, Either[ADWError, Exec[I,T]], I) => MTask[(Either[ADWError,T], Executing, Int, Long)] =
      execMAggregation[I,T](trk, verbose)

    // Prepare execution of the tasks, no ordering kept
    val processed: Observable[(Either[ADWError,T], Executing, Int, Long)] =
      source.mapParallelUnordered(parallelism = parallelism){
        case (id,f) =>
          agg(id,f,i)
      }

    // execute tasks and collect the results
    val t1 = processed.foldLeftL(t.zero){ (acc,r) =>
      r match {
        case (Left(e),count,_,_) =>
          printlnVerbose(verbose, s"ID: $count $e")
          acc
        case (o@Right(_),exec,count,ta) =>
          printlnVerbose(verbose,s"ID: $count Evaluating task that took (CPU = ${ta/1.0e9} seconds)")
          t.collect(acc,o,ta,exec)
      }
    }

    // Keep executing the tasks until it is finished or time-out
    val r1: CancelableFuture[O] = t1.runToFuture
    Await.result(r1, duration)
  }

  /**
    * Creates a partial function that aggregates results from a list of
    * partial functions `npairs` using the the task `t` to accumulate those
    * results. The aggregating task `t` must deal with errors returned by
    * executed partial functions.
    *
    * Exceptions are automatically handled by this function (returned as
    * errors).
    *
    * @param npairs functions whose results will be aggregated
    * @param t task that will aggregate the results
    * @return a partial function that take an input used to execute a list
    *         of partial functions and returns a single aggregates result
    *         based on those results
    * @param parallelism number of concurrent threads. Suggest using the
    *                    number of CPUs detected by the JVM.
    * @param duration how long we will wait for the execution of all the tasks
    * @param verbose indicates if we print out logging data during execution
    * @param s Monix scheduler that manages the thread pool
    * @tparam I input to partial functions
    * @tparam T task input type (output of partial functions)
    * @tparam O task aggregation type (resulting pipe output)
    * @return
    */
  def partialsPar[I,T,O](npairs: Iterator[Either[ADWError, Exec[I,T]]], t:Aggregator[T,O],
                                   parallelism: Int, duration: Duration, verbose:Int)
                                  (implicit s: Scheduler): PartialFunc[I,O] = {
    (trk: Executing, i: I) => {
      try {
        val tmp: O = execMTask(npairs, t, trk, i, parallelism, duration, verbose)(s)
        ( Right(tmp), Executing(t) )
      } catch {
        case e: Throwable =>
          ( Left(ADWError(s"${t.trace}:\n" + printStackTrace(e))), trk) // fail silently
      }
    }
  }

  // TODO: provide a linearization pre-compilation for stream operations. For example
  // we may set-up operations on columns separately but will have to compose those so
  // that need only scan and transform the data once. (x*2) -> (y*1) -> (x+y) ==>
  // (x*2, y*1) -> (x + y)

  /**
    * Compiles a pipes description into a list of partial functions. Each
    * partial function represents a linearized (sequential) sequence of
    * task executions. Each sequential pipe can be executed independently.
    *
    * @param o pipes descriptor
    * @param track tracking of compilation. Used for debug
    * @return a list of partial function if compilation succeeded otherwise
    *         an error.
    */
  def compile[I,T,O](o:Op[I,O], track: Compiling = Compiling()):Iterable[Either[ADWError,Exec[I,O]]] = o match {

      case NoOp(_) =>
        val partFunc = partial(Nop[I,O]().t)
        // Compile time trace
        val new_track = track + NOP[I,O]
        // return the partial and the compile-time trace
        val exec = Exec(partFunc, new_track)
        Iterable(Right(exec))

      case Func(t:Transformer[I,O]) =>
        val partFunc = partial(t)
        // Compile time trace
        val new_track = track + t
        // return the partial and the compile-time trace
        val exec = Exec(partFunc, new_track)
        Iterable(Right(exec))

      case Agg(t:Aggregator[I,O]) =>
        val partFunc = partial(t)
        // Compile time trace
        val new_track = track + t
        // return the partial and the compile-time trace
        val exec = Exec(partFunc, new_track)
        Iterable(Right(exec))

      case Iff(iff,f) =>
        val fc: Iterable[Either[ADWError, Exec[I, O]]] = compile(f, track)
        fc.map {
          case Left(e) =>
            Left(e)
          case Right(Exec(func, trk)) =>
            val last = trk.history.last
            val tmp: PartialFunc[I, O] = partial(iff.apply, func, last)
            Right(Exec(tmp, trk))
        }

      case When(when,f) =>
        val fc: Iterable[Either[ADWError, Exec[I, O]]] = compile(f, track)
        fc.filter{ f =>
          f match {
            case Left(_) =>
              true
            case Right(Exec(_,trk)) =>
              when(trk)
          }
        }

      case All(ops) =>
        // compile the sub-expressions
        val subs: Iterable[Either[ADWError, Exec[I, O]]] = ops.flatMap(o => compile(o, track))
        subs

      case SeqOp(f1, f2) =>
        // compile sub-expressions
        val fc1 = f1.flatMap(op => compile(op,track) )
        val fc2 = f2.flatMap( op => compile(op,track) )
        // compose pairs into single partial
        compose(fc1.iterator,fc2.iterator)

      case CrossOp(f1, f2) =>
        // compile sub-expressions
        val fc1 = f1.flatMap(op => compile(op,track) )
        val fc2 = f2.flatMap(op => compile(op,track) )
        // generate all combinations
        cross(fc1, fc2)

      // Note: compiler complains of erasure in abstract types I and T, but
      // if we remove these compilation will fail
      case Aggregate(f:Op[I,T], p:Par[T,O]) =>
        val fc: Iterable[Either[ADWError, Exec[I,T]]] = compile(f, track)
        val fs: PartialFunc[I,O] = if (p.parallelism > 1)
          partialsPar(fc.iterator, p.t, p.parallelism, p.duration, p.verbose)(p.s)
        else
          partials(fc.iterator,p.t, p.verbose)
        // We use ++ to make sure the history is chronologically correct
        val tt = Compiling() + p.t
        Iterable(Right(Exec(fs, tt)))

      case Repeat(n: Int, f:Op[I,T], p:Par[T,O]) =>
        // generate the partial functions
        val fc = compile(f, track)
        // iterate over these partial functions `n` times
        val fr: Iterable[Either[ADWError, Exec[I, T]]] = repeatFor(n)(fc)
        // Sae as Aggregate - execute as per Par's configuration
        val fs: PartialFunc[I,O] = if (p.parallelism > 1)
          partialsPar(fr.iterator, p.t, p.parallelism, p.duration, p.verbose)(p.s)
        else
          partials(fr.iterator,p.t, p.verbose)
        // We use ++ to make sure the history is chronologically correct
        val tt = Compiling() + p.t
        Iterable(Right(Exec(fs, tt)))

  }

  /**
    * Executes a compiled pipe. Each linear (sequential pipe) may return
    * either an error or a result. The result contains the
    * [[pt.inescn.etl.stream.Load.Val]] output and the execution trace
    * [[Executing]]. The execution trace can be used to debug what tasks
    * were actually executed and or use that to generate a sequential pipe.
    * This version of execution strips away all errors.
    *
    * @param e compiled set of pipes
    * @param i input for the execution of all linear pipes
    * @return the results of executing the pipes, no errors included
    */
  def exec[I,O](e:Iterable[Either[ADWError,Exec[I,O]]])(i:I): Iterable[(Executing, O)] = {
    val acc = Executing()
    // Convert error to None so they can be removed
    val tmp: Iterable[(Either[ADWError, O], Executing)] = e.flatMap { f =>
      f match {
        case Left(_) => None
        case Right(Exec(op,_)) =>
          val tmp: (Either[ADWError, O], Executing) = op(acc,i)
          Some(tmp)
      }
    }
    tmp.collect{ case (Right(o),exec) => (exec,o) }
  }

  /**
    * Executes a compiled pipe. Each linear (sequential pipe) may return
    * either an error or a result. The result contains the
    * [[pt.inescn.etl.stream.Load.Val]] output and the execution trace
    * [[Executing]]. The execution trace can be used to debug what tasks
    * were actually executed and or use that to generate a sequential pipe.
    * This version of execution does not strip away the errors. These
    * errors may include the runtime check if a task should or should not
    * be executed.
    *
    * @param e compiled set of pipes
    * @param i input for the execution of all linear pipes
    * @return the results of executing the pipes, including errors
    */
  def execDebug[I,O](e:Iterable[Either[ADWError,Exec[I,O]]])(i:I): Iterable[(Either[ADWError,O],Executing)] = {
    val acc: Executing = Executing()
    val tmp: Iterable[(Either[ADWError, O], Executing)] = e.map {
      case Left(err) =>
        (Left(err), acc)
      case Right(Exec(op, _)) =>
        val tmp: (Either[ADWError, O], Executing) = op(acc, i)
        tmp
    }
    tmp
  }

  /* TODO:
  /**
    * This function takes in a list of [[pt.inescn.search.stream.Tasks.Task]]s
    * that represents a single sequential (linear) pipe that were executed
    * with success and produces a sequential pipe. The order is in reverse
    * chronological order. The resulting pipe can then be compiled and
    * executed.
    *
    * @param p list of tasks in reverse chronological order
    * @return a single linear pipe ready for compilation
    */
  def deploy(p:List[Task[_,_]]): Op[_,_] = {
    @tailrec
    def linearize(p:List[Task[_,_]],acc:Op[_,_]) : Op[_,_] = p match {
      case t1::t2::tail =>
        val t = SeqOp(List(Func(t2)),List(Func(t1)))
        val n_acc = acc match {
          case NoOp(_) => t
          case _ => SeqOp(List(t),List(acc))
        }
        linearize(tail,n_acc)
      case t::Nil =>
        SeqOp(List(Func(t)),List(acc))
      case Nil =>
        acc
    }
    linearize(p, NoOp(""))
  }
*/

}
