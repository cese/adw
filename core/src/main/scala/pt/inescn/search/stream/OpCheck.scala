package pt.inescn.search.stream

import pt.inescn.search.stream.Pipes.{Compiling, Executing}
import pt.inescn.search.stream.Tasks.{ColumnNames, Task}

/**
  * Contains the (examples of) functions that perform run-time and
  * compile-time checking within the pipes.
  *
  */
object OpCheck {


  /**
    * Represents either run-time or compile-time checks.
    */
  sealed trait OpCheck

  /**
    * Run-time check.
    */
  trait Fiff extends OpCheck {
    /**
      * Check. Should be placed before the expressions that will be executed.
      * Prior executed tasks are found in the trace in chronological order.
      *
      * @param e current execution trace
      * @param f input value to task
      * @return true if task `t` should execute, otherwise false
      */
    def apply[I,O](e:Executing, f:I, t: Task[_,_]): Boolean
  }

  /**
    * Compile-time check.
    */
  trait Fwhen extends OpCheck {
    /**
      * Check. Note that this check should wrap the
      * [[pt.inescn.search.stream.Pipes.Op]] that is to be checked because
      * compilation is done bottom up, so the trace only shows the trace
      * of those sub-expressions.
      *
      * @param c current compilation trace
      * @return true if task `t` should execute, otherwise false
      */
    def apply(c:Compiling): Boolean
  }

  /**
    * Check that we do not repeat the same function (name) applied
    * to the same columns (ColumnNames) irrespective of the parameters.
    */
  case object NoRepeatsW extends Fwhen {
    override def apply(trk:Compiling): Boolean = {
      // full chronological tracking
      val tmp: List[Task[_,_]] = trk.history
      // Get name and column ids
      val tmp1 = tmp.map{ t =>
        val name = t.name
        val params = t.params.filter{ case (_, ColumnNames(_)) => true ; case _ => false }
        name -> params
      }
      // Get rid of repeats
      val tmp2 = tmp1.toSet
      // Use if no repeats
      tmp1.size == tmp2.size
    }
  }

  /**
    * Check that we do not repeat the same function (name) applied
    * to the same columns (ColumnNames) irrespective of the parameters.
    */
  case object NoRepeats extends Fiff {
    override def apply[I,O](trk:Executing, i:I, tsk:Task[_,_]): Boolean = {
      // full chronological tracking
      val tmp: List[Task[_,_]] = trk.history
      // Get name and column ids
      val tmp1 = tmp.map(t => t.name -> t.params.filter{ case (_,ColumnNames(_)) => true ; case _ => false } )
      // Get rid of repeats
      val tmp2 = tmp1.toSet
      // Get the current task with column names
      val params = tsk.params.filter{ case (_, ColumnNames(_)) => true ; case _ => false }
      // Have we already execute such a call?
      val done = tmp2.contains((tsk.name,params))
      // If not, execute it
      !done
    }
  }


}
