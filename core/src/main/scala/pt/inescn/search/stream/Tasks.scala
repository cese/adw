package pt.inescn.search.stream

import java.time.{Duration, LocalDateTime}

import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.search.stream.Pipes._
import pt.inescn.utils.ADWError

import scala.collection.AbstractIterator

/**
  * Tasks represent functions that are executed within a pipe. It would
  * be better if any client could define and use their tasks independently
  * of the pipe system. However because we have to encode these as JSON
  * all tasks must be defined in this module. Later we may opt for binary
  * object serialization or some form of meta-compilation and avoid this
  * restriction.
  *
  * TODO: Automate the creation of tasks !!! Include JSON encoding. PRIORITY
  */
object Tasks {

  // TODO: remove common base of parameters
  /**
    * Use to describe the parameters of the task. This facilitates type
    * checking, run-time debugging and automated parameter search.
    */
  trait Param
  case object NotUsed extends Param
  case object PreCalc extends Param

  /**
    * Data in table frame format. Consists of a sequence of rows made up of
    * pairs of column name and column value. All elements are strings.
    * Provides a simple data source for testing and debugging.
    *
    * @param f data in table format
    */
  case class DataFrame(f:Seq[(String, Seq[String])]) extends Param
  case class FileNames(fs:Seq[String]) extends Param
  case class ColumnName(col:String) extends Param
  case class ColumnNames(cols:Seq[String]) extends Param
  case class DurationTime(dt:Duration) extends Param
  case class DateTime(dt:LocalDateTime) extends Param
  case class ColumnRenames(cols:Seq[(String,String)]) extends Param
  case class SlidingWindow(size:Int, stride:Int) extends Param
  case class ThreshHold(t:Double) extends Param
  case class DropSize(l:Int) extends Param

  case class StringP(v:String) extends Param
  case class DoubleP(v:Double) extends Param

  type Params = Seq[Param]

  /**
    * Name of the task compiled or executed
    * @param v name of the task
    */
  case class ID(v:String) extends AnyVal

  /**
    * Name and parameters of the task compiled or executed
    * @param v name and parameters of the task
    */
  case class Trace(v:String) extends AnyVal

  /**
    * Core functional block of the pipes. It has two basic operation modes.
    *
    * - The first is standard execution: the method `pre(i)` is called during
    * execution with the input. We can use this to set-up execution. For
    * example changing parameters based on the input. Then the method `f(i)`
    * is executed using the task updated by `pre(i)`. The output is then
    * passe onto the next task.
    *
    * - The second is aggregation: the method `collect(acc,i)` is called and
    * fed the output of a set of sequential pipes. This allows one to filter,
    * select or aggregate results. Note that in tis case `pre(i)` is *not*
    * called.
    *
    * Note that any exceptions thrown by the tasks are caught and transformed
    * into errors. When this happens pipe execution terminates early.
    */
  trait Task[I,O] {
    type Params = Map[String,Any]

    def name:String
    def params:Params
    def id:ID = ID(s"$name")
    def trace:Trace = Trace(s"$name($params)")

    override def toString: String = s"$name($params)"

    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case t:Task[I,O] =>
          (t.name == this.name) &&
            (t.params == this.params)
        case _ => false
      }
    }

  }

  trait Transformer[I,O] extends Task[I,O] {

    /**
      * Set-up execution of `f(i)`. For example update the parameters.
      *
      * @param i Input data
      * @return task that will process the data
      */
    def pre(i:I): Either[ADWError,Transformer[I,O]] = Right(this)

    /**
      * Process the data and produce the results. If an error
      * occurred we can signal this
      *
      * @param i Input data
      * @return output passed onto the next task
      */
    def f(i:I): Either[ADWError,O]
  }

  trait Aggregator[I,A] extends Task[I,A] {
    /**
      * Initial value of the accumulator
      */
    def zero: A

    /**
      * Given and accumulator `acc` and an input `i`, update the accumulator.
      * When provided with various pipe results, its used is akin to the
      * fold function. It may be used to filter, select or aggregate results.
      *
      * @param acc initial aggregation value set as an [[pt.inescn.utils.ADWError]]
      * @param i Input data
      * @param ta time to execute pipe call
      * @return output passed onto the next task
      */
    def collect(acc: A, i: Either[ADWError,I], ta:Long, track: Executing): A
  }

  def getTask[I,O](op:Op[I,O],id:Int = 0): Either[String,Task[I,O]] = op match {
    case Func(t) => Right(t)
    case All(ts) => if (ts.isEmpty) Left("All: Empty task list") else getTask(ts.head, id)
    case Iff(_, f) => getTask(f)
    case When(_, f) => getTask(f)
    case _ => Left(s"Op: $op has no single specific task")
  }

  /*
   * Automating the creation of the tasks. This allows:
   * 1. The user to provide a function that will be converted to a Task
   * 2. Will add the boilerplate automatically (name, pre default, Either return
   * 3. Allow direct use and implicit conversion in the Pipe DSL
   */

  type Out[O] = Either[ADWError,O]

  def toTask0[I,O,P1](func:I => Out[O], nm:TaskInfo): Transformer[I,O] = new Transformer[I,O] {
    //override type Params = Unit

    override val name: String = nm.funcName
    override val params: Params = Map()
    def f(i:I):Out[O] = func(i)
 }

  def toAgg0[I,O,P1](func:I => Out[O], nm:TaskInfo): Transformer[I,O] = new Transformer[I,O] {
    //override type Params = Unit

    override val name: String = nm.funcName
    override val params: Params = Map()
    def f(i:I):Out[O] = func(i)
  }

  def toTask1[I,O,P1](func:(P1,I) => Out[O], p:P1, nm:TaskInfo): Transformer[I,O] = new Transformer[I,O] {
    //override type Params = List[P1]

    override val name: String = nm.funcName
    override val params: Params = Map( nm.params.head -> p )
    def f(i:I):Out[O] = func(p,i)
  }

  def toTask2[I,O,P1,P2](func:(P1,P2,I) => Out[O], p1:P1, p2:P2, nm:TaskInfo): Transformer[I,O] = new Transformer[I,O] {
    //override type Params = (P1, P2)

    override val name: String = nm.funcName
    override val params: Params = Map(nm.params.head -> p1,
      nm.params(1) -> p2)
    def f(i:I):Out[O] = func(p1,p2,i)
  }

  def toTask3[I,O,P1,P2,P3](func:(P1,P2,P3,I) => Out[O], p1:P1, p2:P2, p3:P3, nm:TaskInfo): Transformer[I,O] = new Transformer[I,O] {
    //override type Params = (P1, P2, P3)

    override val name: String = nm.funcName
    override val params: Params = Map(nm.params.head -> p1,
      nm.params(1) -> p2,
      nm.params(2) -> p3)
    def f(i:I):Out[O] = func(p1,p2,p3,i)
  }

  def toTask4[I,O,P1,P2,P3,P4](func:(P1,P2,P3,P4,I) => Out[O], p1:P1, p2:P2, p3:P3, p4:P4, nm:TaskInfo): Transformer[I,O] =
    new Transformer[I,O] {
      //override type Params = (P1, P2, P3, P4)

      override val name: String = nm.funcName
      override val params: Params = Map(nm.params.head -> p1,
        nm.params(1) -> p2,
        nm.params(2) -> p3,
        nm.params(3) -> p4)
      def f(i:I):Out[O] = func(p1,p2,p3,p4,i)
    }

  def toTask5[I,O,P1,P2,P3,P4,P5](func:(P1,P2,P3,P4,P5,I) => Out[O], p1:P1, p2:P2, p3:P3, p4:P4, p5:P5,
                                  nm:TaskInfo): Transformer[I,O] =
    new Transformer[I,O] {
      //override type Params = (P1, P2, P3, P4, P5)

      override val name: String = nm.funcName
      override val params: Params = Map(nm.params.head -> p1,
        nm.params(1) -> p2,
        nm.params(2) -> p3,
        nm.params(3) -> p4,
        nm.params(4) -> p5)
      def f(i:I):Out[O] = func(p1,p2,p3,p4,p5,i)
    }

  def toTask6[I,O,P1,P2,P3,P4,P5,P6](func:(P1,P2,P3,P4,P5,P6,I) => Out[O], p1:P1, p2:P2, p3:P3, p4:P4, p5:P5, p6:P6,
                                  nm:TaskInfo): Transformer[I,O] =
    new Transformer[I,O] {
      //override type Params = (P1, P2, P3, P4, P5, P6)

      override val name: String = nm.funcName
      override val params: Params = Map(nm.params.head -> p1,
        nm.params(1) -> p2,
        nm.params(2) -> p3,
        nm.params(3) -> p4,
        nm.params(4) -> p5,
        nm.params(5) -> p6)
      def f(i:I):Out[O] = func(p1,p2,p3,p4,p5,p6,i)
    }

  def toTask7[I,O,P1,P2,P3,P4,P5,P6,P7]
  (func:(P1,P2,P3,P4,P5,P6,P7,I) => Out[O], p1:P1, p2:P2, p3:P3, p4:P4, p5:P5, p6:P6, p7:P7,
   nm:TaskInfo): Transformer[I,O] =
    new Transformer[I,O] {
      //override type Params = (P1, P2, P3, P4, P5, P6, P7)

      override val name: String = nm.funcName
      override val params: Params = Map(nm.params.head -> p1,
        nm.params(1) -> p2,
        nm.params(2) -> p3,
        nm.params(3) -> p4,
        nm.params(4) -> p5,
        nm.params(5) -> p6,
        nm.params(6) -> p7)
      def f(i:I):Out[O] = func(p1,p2,p3,p4,p5,p6,p7,i)
    }

  def toTask8[I,O,P1,P2,P3,P4,P5,P6,P7,P8]
  (func:(P1,P2,P3,P4,P5,P6,P7,P8,I) => Out[O], p1:P1, p2:P2, p3:P3, p4:P4, p5:P5, p6:P6, p7:P7, p8:P8,
   nm:TaskInfo): Transformer[I,O] =
    new Transformer[I,O] {
      //override type Params = (P1, P2, P3, P4, P5, P6, P7, P8)

      override val name: String = nm.funcName
      override val params: Params = Map(nm.params.head -> p1,
        nm.params(1) -> p2,
        nm.params(2) -> p3,
        nm.params(3) -> p4,
        nm.params(4) -> p5,
        nm.params(5) -> p6,
        nm.params(6) -> p7,
        nm.params(7) -> p8)
      def f(i:I):Out[O] = func(p1,p2,p3,p4,p5,p6,p7,p8,i)
    }

  def toTask9[I,O,P1,P2,P3,P4,P5,P6,P7,P8,P9]
  (func:(P1,P2,P3,P4,P5,P6,P7,P8,P9,I) => Out[O], p1:P1, p2:P2, p3:P3, p4:P4, p5:P5, p6:P6, p7:P7, p8:P8, p9:P9,
   nm:TaskInfo): Transformer[I,O] =
    new Transformer[I,O] {
      //override type Params = (P1, P2, P3, P4, P5, P6, P7, P8, P9)

      override val name: String = nm.funcName
      override val params: Params = Map(nm.params.head -> p1,
        nm.params(1) -> p2,
        nm.params(2) -> p3,
        nm.params(3) -> p4,
        nm.params(4) -> p5,
        nm.params(5) -> p6,
        nm.params(6) -> p7,
        nm.params(7) -> p8,
        nm.params(8) -> p9)
      def f(i:I):Out[O] = func(p1,p2,p3,p4,p5,p6,p7,p8,p9,i)
    }

  /*
   * Explicitly call `T`to convert to a Func(Task)
   */


  def T[I,O](named_func: (TaskInfo, I => Out[O])): Func[I,O] = {
    val name = named_func._1
    val f = named_func._2
    Func( toTask0(f, name) )
  }

  def T[I,O,P1](named_func: (TaskInfo, (P1,I) => Out[O]), p1: P1): Func[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask1(f, p1, name) )
  }

  def T1[I,O,P1](named_func: (TaskInfo, (P1,I) => Out[O]), p1: P1): Func[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask1(f, p1, name) )
  }

  def T[I,O,P1,P2](named_func: (TaskInfo, (P1,P2,I) => Out[O]), p1:P1, p2:P2): Func[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask2(f, p1, p2, name) )
  }

  def T[I,O,P1,P2,P3](named_func: (TaskInfo, (P1,P2,P3,I) => Out[O]), p1:P1, p2:P2, p3:P3): Func[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask3(f, p1, p2, p3, name) )
  }

  def T[I,O,P1,P2,P3,P4](named_func:(TaskInfo, (P1,P2,P3,P4,I) => Out[O]), p1:P1,p2:P2,p3:P3,p4:P4): Func[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask4(f, p1, p2, p3, p4, name) )
  }

  def T[I,O,P1,P2,P3,P4,P5](named_func: (TaskInfo, (P1,P2,P3,P4,P5,I) => Out[O]),p1:P1,p2:P2,p3:P3,p4:P4,p5:P5): Func[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask5(f, p1, p2, p3, p4, p5, name) )
  }
  
  def T[I,O,P1,P2,P3,P4,P5,P6](named_func:(TaskInfo, (P1,P2,P3,P4,P5,P6,I) => Out[O]),p1:P1,p2:P2,p3:P3,p4:P4,p5:P5,p6:P6): Func[I,O] =
  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask6(f, p1, p2, p3, p4, p5, p6, name) )
  }

  def T[I,O,P1,P2,P3,P4,P5,P6,P7]
  (named_func: (TaskInfo, (P1,P2,P3,P4,P5,P6,P7,I) => Out[O]),p1:P1,p2:P2,p3:P3,p4:P4,p5:P5,p6:P6,p7:P7): Func[I,O] =
  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask7(f, p1, p2, p3, p4, p5, p6, p7, name) )
  }

  def T[I,O,P1,P2,P3,P4,P5,P6,P7,P8]
  (named_func: (TaskInfo, (P1,P2,P3,P4,P5,P6,P7,P8,I) => Out[O]),p1:P1,p2:P2,p3:P3,p4:P4,p5:P5,p6:P6,p7:P7,p8:P8): Func[I,O] =
  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask8(f, p1, p2, p3, p4, p5, p6, p7, p8, name) )
  }

  def T[I,O,P1,P2,P3,P4,P5,P6,P7,P8,P9]
  (named_func: (TaskInfo, (P1,P2,P3,P4,P5,P6,P7,P8,P9,I) => Out[O]),p1:P1,p2:P2,p3:P3,p4:P4,p5:P5,p6:P6,p7:P7,p8:P8,p9:P9): Func[I,O] =
  {
    val name = named_func._1
    val f = named_func._2
    Func( toTask9(f, p1, p2, p3, p4, p5, p6, p7, p8, p9, name) )
  }

  /*
   * Explicitly cal `x.T`to convert to a All(Iterable(Func(Task)))
   * Parametrized tasks.
   */

  def lazyMap1[I,O,P1](f:(P1,I) => Out[O], i: Iterable[P1], info: TaskInfo): Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = Func( toTask1(f,iter1.next(),info) )
        } // iterator
        converter
      }
    }
    convertable
  }

  def lazyMap2[I,O,P1,P2](f:(P1,P2,I) => Out[O], i: Iterable[(P1,P2)], info: TaskInfo): Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = {
            val (p1,p2) = iter1.next()
            Func( toTask2(f,p1,p2,info) )
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  def lazyMap3[I,O,P1,P2,P3](f:(P1,P2,P3,I) => Out[O], i: Iterable[(P1,P2,P3)], info: TaskInfo): Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = {
            val (p1,p2,p3) = iter1.next()
            Func( toTask3(f,p1,p2,p3,info) )
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  def lazyMap4[I,O,P1,P2,P3,P4](f:(P1,P2,P3,P4,I) => Out[O], i: Iterable[(P1,P2,P3,P4)], info: TaskInfo):
  Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = {
            val (p1,p2,p3,p4) = iter1.next()
            Func( toTask4(f,p1,p2,p3,p4,info) )
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  def lazyMap5[I,O,P1,P2,P3,P4,P5](f:(P1,P2,P3,P4,P5,I) => Out[O], i: Iterable[(P1,P2,P3,P4,P5)], info: TaskInfo):
  Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = {
            val (p1,p2,p3,p4,p5) = iter1.next()
            Func( toTask5(f,p1,p2,p3,p4,p5,info) )
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  def lazyMap6[I,O,P1,P2,P3,P4,P5,P6](f:(P1,P2,P3,P4,P5,P6,I) => Out[O], i: Iterable[(P1,P2,P3,P4,P5,P6)],
                                      info: TaskInfo):
  Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = {
            val (p1,p2,p3,p4,p5,p6) = iter1.next()
            Func( toTask6(f,p1,p2,p3,p4,p5,p6,info) )
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  def lazyMap7[I,O,P1,P2,P3,P4,P5,P6,P7](f:(P1,P2,P3,P4,P5,P6,P7,I) => Out[O], i: Iterable[(P1,P2,P3,P4,P5,P6,P7)],
                                         info: TaskInfo):
  Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = {
            val (p1,p2,p3,p4,p5,p6,p7) = iter1.next()
            Func( toTask7(f,p1,p2,p3,p4,p5,p6,p7,info) )
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  def lazyMap8[I,O,P1,P2,P3,P4,P5,P6,P7,P8](f:(P1,P2,P3,P4,P5,P6,P7,P8,I) => Out[O],
                                            i: Iterable[(P1,P2,P3,P4,P5,P6,P7,P8)],
                                            info: TaskInfo):
  Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = {
            val (p1,p2,p3,p4,p5,p6,p7,p8) = iter1.next()
            Func( toTask8(f,p1,p2,p3,p4,p5,p6,p7,p8,info) )
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  def lazyMap9[I,O,P1,P2,P3,P4,P5,P6,P7,P8,P9](f:(P1,P2,P3,P4,P5,P6,P7,P8,P9,I) => Out[O],
                                               i: Iterable[(P1,P2,P3,P4,P5,P6,P7,P8,P9)],
                                               info: TaskInfo):
  Iterable[Func[I,O]] = {
    val convertable: Iterable[Func[I,O]] = new Iterable[Func[I,O]] {

      override def iterator: AbstractIterator[Func[I,O]] = {

        val converter: AbstractIterator[Func[I,O]] = new AbstractIterator[Func[I,O]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): Func[I,O] = {
            val (p1,p2,p3,p4,p5,p6,p7,p8,p9) = iter1.next()
            Func( toTask9(f,p1,p2,p3,p4,p5,p6,p7,p8,p9,info) )
          }
        } // iterator
        converter
      }
    }
    convertable
  }


  // Use implicits in order to avoid duplicate function definitions
  type D = DummyImplicit

  def T[I,O,P1](named_func: (TaskInfo, (P1,I) => Out[O]), p:Iterable[P1])(implicit d: D): All[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap1(f,p,name)

    All( tmp )
  }

  def T[I,O,P1,P2](named_func: (TaskInfo, (P1,P2,I) => Out[O]), p:Iterable[(P1,P2)])
                  (implicit d1: D, d2: D): All[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap2(f,p,name)

    All( tmp )
  }

  def T[I,O,P1,P2,P3](named_func: (TaskInfo, (P1,P2,P3,I) => Out[O]), p:Iterable[(P1,P2,P3)])
                     (implicit d1: D, d2: D, d3: D): All[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap3(f,p,name)

    All( tmp )
  }

  def T[I,O,P1,P2,P3,P4](named_func: (TaskInfo, (P1,P2,P3,P4,I) => Out[O]), p: Iterable[(P1,P2,P3,P4)])
                        (implicit d1: D, d2: D, d3: D, d4:D):
  All[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap4(f,p,name)

    All( tmp )
  }

  def T[I,O,P1,P2,P3,P4,P5](named_func: (TaskInfo, (P1,P2,P3,P4,P5,I) => Out[O]), p:Iterable[(P1,P2,P3,P4,P5)])
                           (implicit d1: D, d2: D, d3: D, d4:D, d5:D):
  All[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap5(f,p,name)

    All( tmp )
  }

  def T[I,O,P1,P2,P3,P4,P5,P6](named_func: (TaskInfo, (P1,P2,P3,P4,P5,P6,I) => Out[O]),
                               p:Iterable[(P1,P2,P3,P4,P5,P6)])
                              (implicit d1: D, d2: D, d3: D, d4:D, d5:D, d6:D): All[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap6(f,p,name)

    All( tmp )
  }

  def T[I,O,P1,P2,P3,P4,P5,P6,P7](named_func: (TaskInfo, (P1,P2,P3,P4,P5,P6,P7,I) => Out[O]),
                                  p:Iterable[(P1,P2,P3,P4,P5,P6,P7)])
                                 (implicit d1: D, d2: D, d3: D, d4:D, d5:D, d6:D, d7:D): All[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap7(f,p,name)

    All( tmp )
  }

  def T[I,O,P1,P2,P3,P4,P5,P6,P7,P8](named_func: (TaskInfo, (P1,P2,P3,P4,P5,P6,P7,P8,I) => Out[O]),
                                     p:Iterable[(P1,P2,P3,P4,P5,P6,P7,P8)])
                                    (implicit d1: D, d2: D, d3: D, d4:D, d5:D, d6:D, d7:D, d8: D): All[I,O] =  {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap8(f,p,name)

    All( tmp )
  }

  def T[I,O,P1,P2,P3,P4,P5,P6,P7,P8,P9](named_func: (TaskInfo, (P1,P2,P3,P4,P5,P6,P7,P8,P9,I) => Out[O]),
                                        p:Iterable[(P1,P2,P3,P4,P5,P6,P7,P8,P9)])
                                       (implicit d1: D, d2: D, d3: D, d4:D, d5:D, d6:D, d7:D, d8: D, d9:D): All[I,O] = {
    val name = named_func._1
    val f = named_func._2
    val tmp: Iterable[Func[I,O]] = lazyMap9(f,p,name)

    All( tmp )
  }


}
