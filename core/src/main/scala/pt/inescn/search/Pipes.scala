package pt.inescn.search

import pt.inescn.utils.ADWError
import pt.inescn.utils.Utils.printStackTrace


object Pipes {

  /**
    * This class represents a means to track
    * the execution of a single pipe.
    */
  trait Tracking {
    /**
      * Adds another track to this one thereby concatenating both
      * traces into a single one. The `that` track usually has a
      * single record record of the last task.
      *
      * @param that other track
      * @return
      */
    def +(that: Tracking): Tracking

    /**
      * Removes the last added trace to this track.
      * @return
      */
    def drop: Tracking
  }

  /**
    * This track holds the traces generated during the execution of a pipe's
    * operations. Its members allow one top record the operations with their
    * parameters and inputs.
    *
    * @param all           a comma separated string that holds descriptions
    *                      of the operations' execution
    * @param lstIDs        list in execution order of the operations
    *                      that were executed
    * @param setIDs        set of the operations that were executed
    *                      (in no particular order)
    * @param setNames      set of the operations names that were executed
    *                      (in no particular order, see [[Func]])
    * @param lstDefaultIDs list in execution order of the operations
    *                      that were executed. Here we record te default
    *                      parameter used and not the one that was
    *                      returned by the operations' pre-processing step
    *
    * TODO: do we need names?
    * TODO: remove the all member, use lstIDs in the tests
    * TODO: issue with the same function being called several times: when
    * we drop an id from lstIDs can we also drop it from the sets? Only if
    * we keep count and know no more calls were made.
    */
  sealed case class Runtime(all:String,
                            lstIDs:List[String],
                            setIDs:Set[String],
                            setNames:Set[String],
                            lstDefaultIDs:List[String] )
    extends Tracking  {

    def addStrings(x: String, y: String): String = if (x == "") y
    else if (y == "")
      x
    else
      x + "," + y

    /**
      * Extend tracking with the trace in `that`. Specifically
      * used for run-time tracking.
      *
      * @param that Trace to extend his object with
      * @return trace that includes information in `that`
      */
    def +(that: Runtime): Runtime = Runtime(addStrings(all,that.all),
      lstIDs ++ that.lstIDs,
      setIDs ++ that.setIDs,
      setNames ++ that.setNames,
      lstDefaultIDs ++ that.lstDefaultIDs)

    /**
      * Drops the lats trace from the track. Note that the tracking also
      * uses sets. This means that we may inadvertently remove traces
      * that that represent repeated tasks (operations).
      *
      * @return the track with the last trace removed.
      */
    override def drop: Runtime = {
      val remove = lstIDs.head
      val ids = if (lstIDs.nonEmpty) lstIDs.drop(1) else lstIDs
      val defIds = if (lstDefaultIDs.nonEmpty) lstDefaultIDs.drop(1) else lstDefaultIDs
      val names = if (setNames.nonEmpty) setNames.drop(1) else setNames
      val idss = if (setIDs.nonEmpty) setIDs - remove  else setIDs
      val newTrack = Runtime(all, ids, idss, names, defIds)
      newTrack
    }

    /**
      * Extend tracking with the trace in `that`.
      * Allowed only for run-time tracking.
      *
      * @param that Trace to extend his object with
      * @return trace that includes information in `that`
      */
    override def +(that: Tracking): Tracking = that match {
      case c@Runtime(_,_,_,_,_) => this + c
      case _ => throw new RuntimeException(s"Required a $this but got a $that")
    }
  }


  /**
    * Companion object used tio facilitate the instantiation of the
    * run-time tracks.
    */
  object Runtime {
    def apply() : Runtime = Runtime("",List(),Set(),Set(),List())
    def apply[P,I,O](fun:Func[P, I, O], p:P): Runtime = fun.idd(p)
    def apply(r:Runtime) : Runtime = Runtime(all = r.all,
      lstIDs = r.lstIDs,
      setIDs = r.setIDs,
      setNames = r.setNames,
      lstDefaultIDs = r.lstDefaultIDs)
  }

  // TODO: see if we need the lstParams member
  sealed case class Compile(lstIDs:List[String]=List(),
                            lstParams:List[String]=List(),
                            setIDs:Set[String]=Set()) extends Tracking {

    /**
      * Extend tracking with the trace in `that`. Specifically
      * used for compile-time tracking.
      *
      * @param that Trace to extend his object with
      * @return trace that includes information in `that`
      */
    def +(that: Compile): Compile =
      Compile(lstIDs ++ that.lstIDs,
        lstParams ++ that.lstParams,
        setIDs ++ that.setIDs)

    /**
      * Drops the lats trace from the track. Note that the tracking also
      * uses sets. This means that we may inadvertently remove traces
      * that that represent repeated tasks (operations).
      *
      * @return the track with the last trace removed.
      */
    override def drop: Compile = {
      val remove = lstIDs.head
      val ids = if (lstIDs.nonEmpty) lstIDs.drop(1) else lstIDs
      val params = if (lstParams.nonEmpty) lstParams.drop(1) else lstParams
      val idss = if (setIDs.nonEmpty) setIDs - remove  else setIDs
      //val newTrack = copy(lstIDs = ids)
      val newTrack = Compile(ids, params, idss)
      newTrack
    }

    /**
      * Extend tracking with the trace in `that`.
      * Allowed only for compile-time tracking.
      *
      * @param that Trace to extend his object with
      * @return trace that includes information in `that`
      */
    override def +(that: Tracking): Tracking = that match {
      case c@Compile(_,_,_) => this + c
      case _ => throw new RuntimeException(s"Required a $this but got a $that")
    }
  }

  /**
    * Companion object used tio facilitate the instantiation of the
    * compile-time tracks.
    */
  object Compile {
    def apply(id:String, param:String): Compile = new Compile(List(id), List(param), Set(id))
  }


  /**
    * Base for describing how operations can be combined to
    * generate a set of functions that will execute these
    * operations.
    * See [[compile]] fo more information.
    *
    * @tparam I Function input type
    * @tparam IO Function intermediate input-out (used only
    *            during composition, otherwise [[scala.Nothing]]
    * @tparam O Function output type
    */
  sealed trait Op[I, IO, O]

  /**
    * This class represents a non-operation. In other
    * words no function is composed and executed. We
    * can use this as a place holder or a means to deal
    * with errors.
    *
    * @param s Additional information
    * @tparam I Function input type
    * @tparam IO Function intermediate input-out (used only
    *            during composition, otherwise [[scala.Nothing]]
    * @tparam O Function output type
    */
  private [Pipes] case class NoOp[I, IO, O](s: String) extends Op[I, IO, O]

  /**
    * This class represents the composition of pairs of functions
    * in the order they are listed. So if the list `f1` has
    * the list of operations with functions [f11,f12,f13] and
    * `f2` has the list of operations with functions [f21,f22,f23],
    * then the the result of the compilation is a list of composed
    * functions [f11(f21(_)),f12(f22(_)),f13(f23(_))].
    * See [[compile]] for more information.
    *
    * @param f1 list of functions to be executed first
    * @param f2 list of functions to be executed second
    * @tparam I Function input type
    * @tparam IO Function intermediate input-out (used only
    *            during composition, otherwise [[scala.Nothing]]
    * @tparam O Function output type
    */
  private [Pipes] case class SeqOp[I, IO, O](f1: List[Op[I, _, IO]], f2: List[Op[IO, _, O]]) extends Op[I, IO, O]

  /**
    * This class represents the composition of pairs of functions
    * that can be generated by the cross product of two lists
    * of functions. So if the list `f1` has the list of operations
    * with functions [f11,f12,f13] and `f2` has the list of
    * operations with functions [f21,f22,f23], then the the result
    * of the compilation is a list of composed
    * functions [
    *  f11(f21(_)),f11(f21(_)),f11(f21(_)),
    *  f12(f22(_)),f12(f22(_)),f12(f22(_)),
    *  f13(f23(_)),f13(f23(_)),f13(f23(_))
    *  ].
    * See [[compile]] for more information.
    *
    * @param f1 list of functions to be executed first
    * @param f2 list of functions to be executed second
    * @tparam I Function input type
    * @tparam IO Function intermediate input-out (used only
    *            during composition, otherwise [[scala.Nothing]]
    * @tparam O Function output type
    */
  private [Pipes] case class CrossOp[I, IO, O](f1: List[Op[I, _, IO]], f2: List[Op[IO, _, O]]) extends Op[I, IO, O]

  /**
    * This class represents a function that is compiled
    * into a pipe via partial evaluation. The function
    * can be configured with a parameter `p`. Note that this
    * parameter can be changed during runtime when the
    * input is available via `pre`. In this way it is possible
    * to use default parameters that are changed later.
    * The function `f` is composed nto the pipe via partial
    * evaluation and executed in the correct order. During
    * execution its input, output an parameters are recorded
    * via a trace so that one can verify how the pipe was
    * executed. It is also used to replicate the pipe for
    * a new execution. The function [[id]], [[idr]], [[idi]]
    * and [[idd]] are used to generate the trace.
    * See [[compile]] for more information.
    *
    * @param name name used to identify the operation
    * @param p parameter used to configure the operation
    * @param pre function used to check and possibly change
    *            the parameter `p`. It can do this during
    *            runtime when it has access to the input.
    * @param f the function that is executed when composed
    *          into a pipe. It takes in a parameter and an input.
    *          It either generates an output or en error
    * @tparam P Function parameter type
    * @tparam I Function input type
    * @tparam O Function output type
    */
  case class Func[P, I, O](name: String, p: P, pre:(P,I) => P, f: (P,I) => Either[ADWError,O]) extends Op[I, Nothing, O]{
    /**
      * Returns a string with he format [operation name]([parameter])
      * This is the parameter returned by `pre`.
      *
      * @param np function parameter
      * @return
      */
    def id(np:P): String = {
      s"$name($np)"
    }

    /**
      * Returns a compile-time trace using [[id]]
      * See [[Tracking]]
      *
      * @param np function parameter
      * @return
      */
    def idr(np:P): Compile = {
      val idp = id(np)
      Compile(List(idp),List(p.toString),Set(idp))
    }

    /**
      * Returns a run-time trace using [[id]]. It also
      * records the input obtained during execution.
      * See [[Tracking]]
      *
      * @param np function parameter
      * @param i function input
      * @return
      */
    def idi(np:P,i:I): Runtime = {
      val call = s"$name($np,$i)"
      val idp = id(np)
      Runtime(call, List(idp), Set(idp), Set(name), List(id(p)))
    }

    /**
      * Returns a run-time trace using [[id]]. It does
      * not record the input obtained during execution.
      * This is used to recreate the pipe during the
      * deployment phase.
      * See [[Tracking]]
      *
      * @param np function parameter
      * @return
      */
    def idd(np:P): Runtime = {
      val call = s"$name($np,?)"
      val idp = id(np)
      Runtime(call, List(idp), Set(idp), Set(name), List(id(p)))
    }
  }

  /**
    * This class represents a list of operations. This
    * allows us to apply the same input to a set of
    * operations (for example to set-up experiments). This
    * also allows us to replicate an operation with all
    * these functions (for example apply a common
    * pre-process function to a set if experiments) using
    * the [[pt.inescn.search.Pipes.SeqOp]] operator.
    * See [[compile]] for more information.
    *
    * @param f list iof operations to be executed.
    * @tparam I Function input type
    * @tparam IO Function intermediate input-out (used only
    *            during composition, otherwise [[scala.Nothing]]
    * @tparam O Function output type
    */
  case class All[I, IO, O](f: List[Op[I, _, O]]) extends Op[I, IO, O]

  /**
    * This class represents a run-time check if the operation
    * can be executed. If not an error is generated.
    * See [[compile]] for more information.
    * See [[When]]
    *
    * @param iff checks during run-time if `f` can be executed or
    *            not. If not, an error is generated. Note that
    *            we have access to the trace of previously executed
    *            operations and possibly the current operation being
    *            execute (Option[Runtime]). A current trace only exists
    *            when `f` is a [[Func]]
    * @param f the operation that is executed when composed
    *          into a pipe. It takes in an input and generates
    *          an output or en error
    * @tparam I Function input type
    * @tparam IO Function intermediate input-out (used only
    *            during composition, otherwise [[scala.Nothing]]
    * @tparam O Function output type
    */
  case class Iff[I, IO, O](iff: (Runtime, I, Option[Runtime]) => Boolean, f: Op[I, _, O]) extends Op[I, IO, O]

  /**
    * This class represents a compile-time check if the operation
    * can be executed. If not a compile-time error is generated
    * and the rest of the pipe is not composed.
    * See [[compile]] for more information.
    * See [[Iff]]
    *
    * @param iff checks during compile-time if `f` can be composed or
    *            not. If not, an error is generated. Note that
    *            we have access to the trace of previously executed
    *            operations and possibly the current operation being
    *            execute (Option[Compile]). A current trace only exists
    *            when `f` is a [[Func]]
    * @param f the operation that is to be composed
    *          into a pipe. It takes in an input and generates
    *          an output or en error
    * @tparam I Function input type
    * @tparam IO Function intermediate input-out (used only
    *            during composition, otherwise [[scala.Nothing]]
    * @tparam O Function output type
    */
  case class When[I, IO, O](iff: (Compile, Option[Compile]) => Boolean, f: Op[I, _, O]) extends Op[I, IO, O]

  /**
    * A set of compiled pipes consists of a list of functions that
    * takes in an empty [[Runtime]] trace, an input and returns
    * either an error or a tuple consisting of an updated
    * [[Runtime]] trace and an output. An error may be returned if
    * the conditional execution of one of the pipe's inner composed
    * functions is not executed.
    * The [[Runtime]] trace contains a record (as strings) of all of
    * the composed functions that were executed, the order in which
    * they were executed and the intermediate outputs. This trace
    * can be used to reconstruct a single pipe's composed function.
    * Besides the conditional execution and trace, these functions
    * also return an error if an exception is thrown (i.e all
    * pipe composed functions are executed instead of the whole set
    * of pipes failing)
    * See [[compile]] fo more information.
    *
    * @param fs set of pipes compiled into a list of composed functions
    * @param track Runtime execution trace
    * @tparam I Type of input of the composed pipe function
    * @tparam O Type of output of the composed pipe function
    */
  sealed case class Exec[I,O](fs: Iterable[(Runtime, I) => Either[ADWError,(Runtime, O)]], track:List[Tracking])

  /**
    * A set of compiled pipes consists of a list of functions that
    * takes in an empty [[Runtime]] trace, an input and returns
    * either an error or a tuple consisting of an updated
    * [[Runtime]] trace and an output. An error may be returned if
    * the conditional execution of one of the pipe's inner composed
    * functions is not executed.
    * Here we ony store a single function.
    * The [[Runtime]] trace contains a record (as strings) of all of
    * the composed functions that were executed, the order in which
    * they were executed and the intermediate outputs. This trace
    * can be used to reconstruct a single pipe's composed function.
    * Besides the conditional execution and trace, these functions
    * also return an error if an exception is thrown (i.e all
    * pipe composed functions are executed instead of the whole set
    * of pipes failing)
    * See [[compile]] fo more information.
    *
    * @param f set of pipes compiled into a single composed functions
    * @param track Runtime execution trace
    * @tparam I Type of input of the composed pipe function
    * @tparam O Type of output of the composed pipe function
    */
  sealed case class Exec1[I,O](f: (Runtime, I) => Either[ADWError,(Runtime, O)], track:Tracking)

  /* Smart constructors */

  /**
    * Smart constructor for [[Func]].
    *
    * @param name Name of the function `f` that is used in the
    *             [[Tracking]] traces. See [[Compile]] and [[Runtime]]
    * @param p parameter to pass to the function `f`
    * @param pre Allow us to return a new set of parameters based in
    *            the current set parameter ad the input. This allows
    *            us to set parameters when none are (can be)
    *            provided.
    * @param f The function that will be executed after the parameter
    *          has been (re)set by the `pre` call
    * @tparam P type of the function parameter
    * @tparam I type of the function input
    * @tparam O type of the function output
    * @return The [[Func]] construct
    */
  def fp[P, I, O](name: String, p: P, pre: (P,I) => P, f: (P,I) => Either[ADWError,O]) = Func(name, p, pre, f)

  /**
    * Smart constructor for [[Func]]. No parameter (re) setting is
    * possible before the function `f` is called.
    *
    * @param name Name of the function `f` that is used in the
    *             [[Tracking]] traces. See [[Compile]] and [[Runtime]]
    * @param p parameter to pass to the function `f`
    * @param f The function that will be executed after the parameter
    *          has been (re)set by the `pre` call
    * @tparam P type of the function parameter
    * @tparam I type of the function input
    * @tparam O type of the function output
    * @return The [[Func]] construct
    */
  def f[P, I, O](name: String, p: P, f: (P,I) => O) = Func(name, p, (p:P, _:I) => p, (p:P, i:I) => Right(f(p,i)))

  /**
    * Smart constructor for [[All]].
    *
    * @param fs The [[Op]] to add to the pipe
    * @tparam I  [[Op]] input type
    * @tparam IO [[Op]] output type that serves as intermediate input
    *            (used only during composition, otherwise [[scala.Nothing]])
    * @tparam O  [[Op]] output type
    * @return
    */
  def all[I, IO, O](fs: Op[I, IO, O]*) = All(fs.toList)

  /**
    * Smart constructor of [[Iff]].
    * This construct is used during run-time to to check
    * with `iff` if the [[Op]] `fs` should or should not be
    * executed. The whole pipe execution terminates as soon
    * as one of its [[Op]]s fails the test.
    * If the [[Op]] is a function, we have access to
    * the current trace otherwise it is None.
    *
    * @param ifff Allows checking the [[Runtime]] trace, current
    *             input and the current [[Op]] trace to see if the
    *             current [[Op]] should or should not be
    *             included.
    * @param fs The [[Op]] to add to the pipe
    * @tparam I [[Op]] input type
    * @tparam O [[Op]] output type
    * @return the `iff` construct used for conditional
    *         run-time execution
    */
  def iff[I, O](ifff: (Runtime, I, Option[Runtime]) => Boolean, fs: Op[I, _, O]*): Op[I, Nothing, O] = {
    if (fs.isEmpty)
      // If we don't use typing here,
      // the compiler overflows its stack
      NoOp[I,Nothing,O]("Iff(NoOp)")
    else if (fs.size == 1)
      Iff(ifff,fs.toList.head)
    else {
      Iff(ifff,All(fs.toList))
    }
  }

  /**
    * Smart constructor of [[When]].
    * This construct is used during compilation to to check
    * with `iff` if the [[Op]] `fs` should or should not be
    * added to the compiled pipe. The whole pipe is removed if
    * at least one of its [[Op]]s fails the test.
    * If the [[Op]] is a function, we have access to
    * the current trace otherwise it is None.
    *
    * @param ifff Allows checking the [[Compile]] trace and
    *             the current [[Op]] trace to see if the
    *             current [[Op]] should or should not be
    *             included
    * @param fs The [[Op]] to add to the pepe
    * @tparam I [[Op]] input type
    * @tparam O [[Op]] output type
    * @return the `when` construct used for conditional
    *         compilation
    */
  def when[I, O](ifff: (Compile, Option[Compile]) => Boolean, fs: Op[I, _, O]*): Op[I, Nothing, O] = {
    if (fs.isEmpty)
    // If we don't use typing here,
    // the compiler overflows its stack
      NoOp[I,Nothing,O]("Iff(NoOp)")
    else if (fs.size == 1)
      When(ifff,fs.toList.head)
    else {
      When(ifff,All(fs.toList))
    }
  }

  /* Function composition */

  /**
    * This is a DSL for combining the functions that can be
    * used to check if a function in a pipe should or should
    * not be executed. And operation.
    * See [[When]] and [[Iff]]
    *
    * For usage example, please take a look at the tests in
    * [[pt.inescn.search.PipesSpec]] and [[pt.inescn.search.TasksSpec]]
    *
    * @param iff1 the function used for run-time or compile-time checks
    * @tparam A usually a Tracking type
    * @tparam P [[Op]] parameter type
    * @tparam I [[Op]] input type
    */
  implicit class IffOps[A, P, I](iff1: (A, I, P) => Boolean) {
    def &(iff2: (A, I, P) => Boolean): (A, I, P) => Boolean = {
      (a:A, i:I, f:P) => {
        iff1(a, i, f) && iff2(a, i, f)
      }
    }

    def |(iff2: (A, I, P) => Boolean): (A, I, P) => Boolean = {
      (a:A, i:I, f:P) => {
        iff1(a, i, f) | iff2(a, i, f)
      }
    }

    def ^(iff2: (A, I, P) => Boolean): (A, I, P) => Boolean = {
      (a:A, i:I, f:P) => {
        iff1(a, i, f) ^ iff2(a, i, f)
      }
    }
  }

  /**
    * This is the DSL for constructing the pipe-lines. It takes two
    * [[Op]]s and returns a single composed [[Op]]. There are only 2
    * types of combinations. A simple sequence indicated by the [[->:]]
    * operator that describes a standard function composition via the
    * [[SeqOp]] construct. And a [[*:]] operator that performs the cross
    * product of two lists of [[Op]]s via the [[CrossOp]] construct.
    *
    * The [[Op]]s may be a single function [[Func]] or a list of functions
    * in the [[All]] construct. When the [[->:]] operator is applied
    * to two sets of [[All]], all [[Op]]s are combined in pairs. This
    * means, the number of functions must be the same. When the [[*:]]
    * operator is used, all combinations (cross-product) are generated.
    *
    * Note that we can control whether an [[Op]] is executed or compiled
    * by using the [[Iff]] and [[When]] constructs respectively. To use
    * these constructs we can use the smart constructor functions [[fp]],
    * [[f]], [[all]], [[iff]] and [[when]]. It is important to note that
    * the [[Iff]] allows us to use both the updated parameter and the input
    * for the check. If the check is true execution is aborted and an error is
    * returned. The [[When]] construct has only access to the default
    * parameter value. The default parameter is the parameter set during the
    * construction of the [[Func]]. The updated parameter is the default
    * parameter that has been changed by [[Func.pre]] call that is executed
    * just before [[Func.f]].
    *
    * Note that both operators are right associative.
    *
    * For usage example, please take a look at the tests in
    * [[pt.inescn.search.PipesSpec]] and [[pt.inescn.search.TasksSpec]]
    *
    * @param o The [[Op]] on the right to which to apply the operator
    * @tparam I Input of the left hand [[Op]]
    * @tparam O1 Output of the left hand [[Op]],
    *            Input of the right hand [[Op]]
    * @tparam O2 Output of the left hand [[Op]]
    */
  implicit class OpOps[I, O1, O2](o: Op[O1, _, O2]) {

    /**
      * Creates a sequential composition of two [[Op]]s. Represents
      * the combination of the [[Op]]s that are paired in order during
      * compilation.
      * Right associative, precedence 0
      *
      * @param that the [[Op]] on the left to which to apply the operator
      * @return [[pt.inescn.search.Pipes.SeqOp]]
      */
    def ->:(that: Op[I, _, O1]): Op[I, O1, O2] = (that, o) match {
      case (Func(_,_,_,_), Func(_,_,_,_)) => SeqOp(List(that), List(o))
      case (Func(_,_,_,_), All(f2s)) => SeqOp(List(that), f2s)
      case (Func(_,_,_,_), SeqOp(_,_)) => SeqOp(List(that), List(o))
      case (Func(_,_,_,_), CrossOp(_,_)) => SeqOp(List(that), List(o))
      case (Func(_,_,_,_), Iff(_, _)) => SeqOp(List(that), List(o))
      case (Func(_,_,_,_), When(_, _)) => SeqOp(List(that), List(o))
      case (Iff(_, _), Func(_,_,_,_)) => SeqOp(List(that), List(o))
      case (Iff(_, _), All(f2s)) => SeqOp(List(that), f2s)
      case (Iff(_, _), SeqOp(_, _)) => SeqOp(List(that), List(o))
      case (Iff(_, _), CrossOp(_, _)) => SeqOp(List(that), List(o))
      case (Iff(_, _), Iff(_, _)) => SeqOp(List(that), List(o))
      case (Iff(_, _), When(_, _)) => SeqOp(List(that), List(o))
      case (When(_, _), Func(_,_,_,_)) => SeqOp(List(that), List(o))
      case (When(_, _), All(f2s)) => SeqOp(List(that), f2s)
      case (When(_, _), SeqOp(_, _)) => SeqOp(List(that), List(o))
      case (When(_, _), CrossOp(_, _)) => SeqOp(List(that), List(o))
      case (When(_, _), Iff(_, _)) => SeqOp(List(that), List(o))
      case (When(_, _), When(_, _)) => SeqOp(List(that), List(o))
      case (All(f1s), Func(_,_,_,_)) => SeqOp(f1s, List(o))
      case (All(f1s), All(f2s)) => SeqOp(f1s, f2s)
      case (All(f1s), SeqOp(_, _)) => SeqOp(f1s, List(o))
      case (All(f1s), CrossOp(_, _)) => SeqOp(f1s, List(o))
      case (All(f1s), Iff(_, _)) => SeqOp(f1s, List(o))
      case (All(f1s), When(_, _)) => SeqOp(f1s, List(o))
      case (SeqOp(_, _), Func(_,_,_,_)) => SeqOp(List(that), List(o))
      case (SeqOp(_, _), All(f2s)) => SeqOp(List(that), f2s)
      case (SeqOp(_, _), SeqOp(_, _)) => SeqOp(List(that), List(o))
      case (SeqOp(_, _), CrossOp(_, _)) => SeqOp(List(that), List(o))
      case (SeqOp(_, _), Iff(_, _)) => SeqOp(List(that), List(o))
      case (SeqOp(_, _), When(_, _)) => SeqOp(List(that), List(o))
      case (CrossOp(_, _), SeqOp(_, _)) => SeqOp(List(that), List(o))
      case (CrossOp(_, _), Func(_,_,_,_)) => SeqOp(List(that), List(o))
      case (CrossOp(_, _), All(f2s)) => SeqOp(List(that), f2s)
      case (CrossOp(_, _), CrossOp(_, _)) => SeqOp(List(that), List(o))
      case (CrossOp(_, _), Iff(_, _)) => SeqOp(List(that), List(o))
      case (CrossOp(_, _), When(_, _)) => SeqOp(List(that), List(o))
      case (NoOp(s), _) => NoOp(s"~> applied to $that is a NoOp($s)")
      case (_, NoOp(s)) => NoOp(s"~> applied to $that is a NoOp($s)")
    }

    /**
      * Creates a sequential composition of two [[Op]]s. Represents
      * the all possible combination (cross product) of the [[Op]]s that
      * are paired during compilation.
      * Right associative, precedence 1
      *
      * @param that the [[Op]] on the left to which to apply the operator
      * @return [[SeqOp]]
      */
    def *:(that: Op[I, _, O1]): Op[I, O1, O2] = (that, o) match {
      case (All(f1s), All(f2s)) => CrossOp(f1s, f2s)
      case (All(f1s), CrossOp(_,_)) => CrossOp(f1s, List(o))
      case (All(f1s), Iff(_, All(_))) => CrossOp(f1s, List(o))
      case (Iff(_, All(_)), All(f2s)) => CrossOp(List(that), f2s)
      //case _ => that ->: o
      case _ => NoOp(s"~*: can only be applied to a limited number of constructs, but here was used with $that -*: $o")
    }
  }


  /**
    * When we compile a complex [[Op]] consisting of a pair inner [[Op]]
    * via partial evaluation, we get back a pair of lists of [[Exec1]].
    * This function is called only when we combine two [[Op]] with the
    * ->: operator (i.e: sequence). It checks the conditions required
    * to extend one or the other lists. If the two lists have the same size,
    * then nothing is done. If either one of the lists has one element,
    * then the 1 element list is "extended" to have as many elements as
    * the other list (thr same element is repeated). If both lists
    * have a different number of elements, then an error is returned.
    *
    * @param f1 the compilation result of the left [[Op]]
    * @param f2 the compilation result of the right [[Op]]
    * @tparam I type of the left [[Op]] input
    * @tparam O type of the left [[Op]] output (input of the
    *           right [[Op]])
    * @tparam O2 type of the right [[Op]] output
    * @return two lists of [[Exec1]] with the same length otherwise an error
    */
  def extend[I,O,O2](f1: List[Exec1[I,O]], f2: List[Exec1[O,O2]]):
  Either[ADWError, (List[Exec1[I,O]], List[Exec1[O,O2]])] = {
    if ((f1.size == 1) && (f2.size > 1)) {
      // 1 : m make it an m : m
      val e = f1.head
      val nf1 = List.fill(f2.size)(e)
      Right((nf1, f2))
    } else if ((f2.size == 1) && (f1.size > 1)) {
      // m : 1 make it an m : m
      val e = f2.head
      val nf2 = List.fill(f1.size)(e)
      Right((f1, nf2))
    } else if (f2.size != f1.size) {
      // m : n make and m <> m, so error
      Left(ADWError(s"Sequence must have equal length functions if greater than 1. We have ${f1.size} <> ${f2.size}"))
    } else {
      Right((f1, f2))
    }
  }

  /**
    * When we compile a complex [[Op]] consisting of a pair inner [[Op]]
    * via partial evaluation, we get back a pair of lists of [[Exec1]].
    * This function checks if any error occurred and if so passes it back.
    * Otherwise it operates on each list by possibly extending them so
    * tha both lists are of the same size. This then allows us to compose
    * pairs of functions into a single one during partial evaluation at
    * a later stage. See [[[extend]]] above
    * for conditions under which the lists are extended.
    *
    * @param f1 the compilation result of the left [[Op]]
    * @param f2 the compilation result of the right [[Op]]
    * @tparam I type of the left [[Op]] input
    * @tparam O type of the left [[Op]] output (input of the
    *           right [[Op]])
    * @tparam O2 type of the right [[Op]] output
    * @return two lists of [[Exec1]] with the same length otherwise an error
    */
  def extend[I,O,O2](f1: Either[ADWError,List[Exec1[I,O]]], f2: Either[ADWError,List[Exec1[O,O2]]])
  : Either[ADWError, (List[Exec1[I,O]], List[Exec1[O,O2]])] = {
    (f1,f2) match {
      case (Left(ADWError(l)),Left(ADWError(r))) => Left(ADWError(l+","+r))
      case (Left(l),_) => Left(l)
      case (_,Left(r)) => Left(r)
      case (Right(l),Right(r)) =>
        extend(l,r)
    }
  }

  /**
    * When we compile an [[Op]] via partial evaluation we can get back
    * one or more of these partial functions (for example composing
    * all combinations of two sets of functions). In certain cases
    * however we know that each compilation results in a single
    * function composition. Here we simply re-code this single
    * functon using the [[Exec1]] case class.
    *
    * @see https://www.youtube.com/watch?v=knqlWboqf_U
    * @see http://slides.com/pchiusano/unison-scala-world-2017#/
    *
    * @param exec Result of a recursive descent compilation of an [[Op]]
    * @tparam I type of the input
    * @tparam O type of the output
    * @return List of single function [[Exec1]] partial evaluations
    */
  def flatten[I,O](exec:Either[ADWError, List[Exec[I,O]]]):Either[ADWError, List[Exec1[I,O]]] = exec match {
    case Left(e) => Left(e)
    case Right(l) =>
      val funcs: List[(Runtime,I)=>Either[ADWError,(Runtime,O)]] = l.flatMap( _.fs )
      val trks: List[Tracking] = l.flatMap( _.track)
      val execs = funcs.zip(trks).map { case (f,t) => Exec1(f,t) }
      Right(execs)
  }


  /**
    * This is called to check if a [[Func]] should be added to a pipe or
    * not. This is done when we want too deploy a specific pipe. It receives
    * a run-time tracking (execution of a previous pipe) and uses that to see
    * if the current function/pipe should be generated or ignored. If the
    * tracking is a compile-time trace, the function/pipe is always included
    * (initial compilation of pipes).
    *
    * @param track Compilation or execution trace. Only the execution trace
    *              is used in the check.
    * @param fun [[Func]] to de deployed in pipe if it is the last entry
    *           in the execution trace.
    * @param p parameter of the [[Func]]
    * @tparam P type of the [[Func]] parameter
    * @tparam I type of the [[Func]] input
    * @tparam O type of the [[Func]] output
    * @return returns a tuple were the first element is true if the function
    *         must be used in a pipe and the updated tracking. Compile time
    *         traces always include the pipe.
    */
  def deploy[P, I, O](track: Tracking, fun:Func[P, I, O], p:P) : (Boolean, Tracking) = {
    track match {
      case trk@Compile(_,_,_) =>
        val id = fun.id(p)
        (true, Compile(id,p.toString))
      case trk@Runtime(_,_,_,_,_) =>
        val id = fun.id(p)
        if (trk.lstDefaultIDs.nonEmpty){
          // IDs with the updated parameters
          val deployIDX = if (trk.lstIDs.nonEmpty) trk.lstIDs.head else "?"
          // IDs with the default parameters
          val deployID = if (trk.lstDefaultIDs.nonEmpty) trk.lstDefaultIDs.head else "?"
          // A trace with the random parameter
          val same = Runtime(fun,p)
          // Update trace to contain the updated parameter
          val samex = same.copy(lstIDs = List(deployIDX))
          (id == deployID, samex)
        }
        else
          (false, track)
    }
  }


  /**
    * This function is called during compilation of the pipes to check if a
    * complex operator should be compiled and included in the pipe or not.
    * If the operation is not included, the whole pipe is removed (chain of
    * passing result to the next function is broken).
    * Note that this is used during compilation only. If we encounter a
    * run-time tracker, we assume the compilation should proceed because
    * the run-time check used for deployment is performed elsewhere (see
    * [[deploy]]).
    * Example use: mak sure we do not call the same function irrespective
    * of the order in which they are called.
    *
    * @param track tracking of a runtime or compile time trace
    * @param iff takes in the compile time tracking of a previous branch and
    *            the track of this current function. It should return true to
    *            include this function in te pipe otherwise false.
    * @tparam P Type of the [[Func]] parameter
    * @tparam I Type of the [[Func]] input
    * @tparam O Type of the [[Func]] ouput
    * @return type with first element as the output of `iff` (compile time) and
    *         the the updated (compile-time) tracking. At run-time, allways
    *         returns true.
    */
  def compilePartial[P, I, O](track: Tracking, iff: (Compile, Option[Compile]) => Boolean): Boolean = {
    track match {
      case trk@Compile(_,_,_) =>
        val r = iff(trk,None)
        r
      case Runtime(_,_,_,_,_) => true
    }
  }

  /**
    * This function is called during compilation of the pipes to check if the
    * function should be compiled and included in the pipe or not. If the
    * function is not included, the whole pipe is removed (chain of passing
    * result to the next function is broken). Here we have access to the
    * [[Func]] and the function's parameter. Note that currently the
    * parameter is not used by the checking function `iff` because it is
    * als used in the [[When]] for standard compile operations [[Op]], where
    * the parameter is unknown.
    * Note that this is used during compilation only. If we encounter a
    * run-time tracker, we assume the compilation should proceed because
    * the run-time check used for deployment is performed elsewhere (see
    * [[deploy]]).
    * Example use: remove pipes with unwanted function calls.
    *
    * @param track tracking of a runtime or compile time trace
    * @param fun function that will either be compiled or not into a final pipe
    *            according to `iff`'s output
    * @param p parameter that was used by the function
    * @param iff takes in the compile time tracking of a previous branch and
    *            the track of this current function. It should return true to
    *            include this function in te pipe otherwise false.
    * @tparam P Type of the [[Func]] parameter
    * @tparam I Type of the [[Func]] input
    * @tparam O Type of the [[Func]] ouput
    * @return type with first element as the output of `iff` (compile time) and
    *         the the updated (compile-time) tracking. At run-time, allways
    *         returns true.
    */
  def compilePartial[P, I, O](track: Tracking, fun:Func[P, I, O], p:P, iff: (Compile, Option[Compile]) => Boolean):
  (Boolean, Tracking) = {
    track match {
      case trk@Compile(_,_,_) =>
        val id: Compile = fun.idr(p)
        val r = iff(trk,Some(id))
        (r,track + id)
      case Runtime(_,_,_,_,_) => (true, track)
    }
  }

  /**
    * This is the main transforming function for the pipes (and tasks). A
    * pipe represents a single sequence (thread) of operations that are
    * executed in order. The output of an operation is passed on to the
    * next one until the final output is generated. One can describe a set
    * of pipes using the various [[Op]] constructs buy combining them
    * several ways using a very simple algebra.
    *
    * Compilation has two main modes of functioning:
    *
    * - The compile mode that transforms the pipes description into a function
    * that can be executed. When the resulting pipes function is executed,
    * all operations that are executed are tracked and the execution trace
    * recorded. This can be used to set-up experimentation and evaluation.
    *
    * - The deploy mode takes a trace of a previous pipes execution's track
    * (description) and uses that to generate a single pipe. This can be used
    * to activate the best performing pipe.
    *
    * The compiler is either in the compile [[Compile]] or deploy [[Runtime]]
    * mode depending on the [[Tracking]] type. When in compile mode, the
    * compiler uses partial evaluation to create a function that:
    * - Takes a set of parameters and the input and pre-process these
    * to generate a new set of parameters
    * - Calls the operation [[Func]] passing to it the parameter and input
    * - If the return of the [[Func]] is an error, it returns this error
    * - If the return of the [[Func]] is valid output, it adds this
    * execution to the trace and returns the output.
    * - If the [[Func]] raises an exception then an error is returned
    * containing the stack trace.
    *
    * During deploy the same actions are performed, but only a single
    * pipe that is passed on via the [[Runtime]] [[Tracking]] is generated.
    * Note that if the pipe description is no found (invalid), then an
    * error is returned.
    *
    * The creation of the [[Func]] can be controlled via conditional
    * compilation using the [[When]] construct. The checking function
    * takes in [[Compile]] trace of the previous pipe trace and the
    * current operaton's trace and returns either a true to include
    * this pipe or not. If not the pipe is simply not generated.
    *
    * The same check of the [[When]] can be done during run-time using
    * the [[Iff]] construct. In this case the checking function not only
    * has access to the [[Runtime]] [[Tracking]] of the previous
    * operations and the current operation but also the input. If
    * the check returns a true, execution proceeds as normal. If it is
    * false an error is generated and returned.
    *
    * A list of operations may be expressed via the [[All]] construct.
    * A [[Func]] or a list of [[Func]] in an [[All]] may be combined with
    * two algebraic operations represented by the following constructs:
    *
    * - [[SeqOp]] combines two operation in order, the output of the first
    * is passed onto the second. If we combines two lists of [[All]]
    * operations `All(f1, f2, f3)` and `All(f4, f5, f6)` the composed
    * functions Exec( f4(f1(_)), f5(f2(_)), f6(f3(_)) ) are generated. If
    * we combine a single `Func(f0)` with an `All(f1,f2)` then the composed
    * functions Exec( f0(f1(_)), f0(f2(_)), f0(f3(_)) ) are generated. Any
    * other number of combinations will result in an error.
    *
    * - [[CrossOp]] combines two operation in order, the output of the first
    * is passed onto the second. If we combines two lists of [[All]]
    * operations `All(f1, f2)` and `All(f4, f5)` the composed functions
    * consisting of all combinations Exec( f4(f1(_)), f5(f1(_)), f4(f2(_)),
    * f5(f2(_)) ) are generated. If we combine a single `Func(f0)` with an
    * `All(f1,f2)` then the composed functions Exec( f0(f1(_)), f0(f2(_))) are
    * generated. Any other number of combinations are possible.
    *
    * The resulting function of the compiler may either be called directly
    * of via the function [[exec()]] and [[execDebug()]]. The latter simply
    * returns all output, including the errors. The former filters out
    * all errors.
    *
    * For usage examples see the [[pt.inescn.search.PipesSpec]] and
    * [[pt.inescn.search.TasksSpec]] tests.
    *
    * TODO: Do we need a way to strip the test and evaluation phase of the pipe?
    *
    * @see http://slides.com/pchiusano/unison-scala-world-2017#/
    *      https://www.youtube.com/watch?v=knqlWboqf_U
    * @param o
    * @param track
    * @tparam I
    * @tparam O1
    * @tparam O2
    * @return
    */
  def compile[I,O1,O2](o:Op[I,O1,O2],track: Tracking = Compile()):Either[ADWError,Exec[I,O2]] = o match {
    case e:NoOp[_,_,_] =>
      /*def func(acc0:A,i:I) = {
        (acc0, e.s)
      }
      Exec( List(func _) )*/
      Left(ADWError(e.s))
    case fun@Func(n,p,pre,f) =>
      val (dep, ntrack) = deploy(track,fun,p)
      if (dep) {
        def func(acc0:Runtime, i:I): Either[ADWError, (Runtime, O2)] = {
          try {
            // Check that the parameters are ok otherwise use defaults
            val np = pre(p,i)
            // Execute the call
            val rr: Either[ADWError, O2] = f(np,i)
            // If everything is ok, record and carry on else stop
            rr.flatMap{ o =>
              val acc1 = fun.idi(np,i)
              val ret: (Runtime, O2) = (acc0+acc1, o)
              Right(ret)
            }
          } catch {
            case e:Throwable => Left(ADWError(s"o:\n"+printStackTrace(e))) // fail silently
          }
        }
        Right(Exec( List(func _), List(ntrack) ))
      }
      else {
        Right(Exec( List(), List() ))
      }
    case When(iff,fun@Func(_,p,pre,f)) =>
      val (dep,nTrack) = compilePartial(track,fun,p,iff)
      if (dep) {
        def func(acc0:Runtime,i:I): Either[ADWError, (Runtime, O2)] = {
          try {
            // Check that the parameters are ok otherwise use defaults
            val np = pre(p,i)
            // Execute the call
            // Execute the call iff is true
            val rr: Either[ADWError, O2] = f(np,i)
            // If everything is ok, record and carry on else stop
            rr.flatMap{ o =>
              val acc1 = fun.idi(np,i)
              val ret: (Runtime, O2) = (acc0+acc1, o)
              Right(ret)
            }
          } catch {
            case e:Throwable => Left(ADWError(s"o:\n"+printStackTrace(e))) // fail silently
          }
        }
        Right(Exec( List(func _), List(nTrack) ))
      } else {
        Right(Exec( List(), List() ))
      }
    case Iff(iff,fun@Func(n,p,pre,f)) =>
      val (dep, nTrack) = deploy(track,fun,p)
      if (dep) {
        def func(acc0:Runtime, i:I) = {
          try {
            // Check that the parameters are ok otherwise use defaults
            val np = pre(p,i)
            // Execute the call
            val acc1 = fun.idi(np,i)
            // Execute the call iff is true
            if (iff(acc0,i,Some(acc1))) {
              val rr: Either[ADWError, O2] = f(np,i)
              // If everything is ok, record and carry on else stop
              rr.flatMap{ o =>
                val acc1 = fun.idi(np,i)
                val ret: (Runtime, O2) = (acc0+acc1, o)
                Right(ret)
              }
            }
            else
              Left(ADWError(s"$o failed"))
          } catch {
            case e:Throwable => Left(ADWError(s"o:\n"+printStackTrace(e))) // fail silently
          }
        }
        Right(Exec( List(func _), List(nTrack) ))
      } else {
        Right(Exec( List(), List() ))
      }
    case When(iff,f) =>
      val tmp0 = compile(f,track)
      val tmp1: Either[ADWError, Exec[I,O2]] = tmp0.fold(
        fa => Left(ADWError(s"$o - ${fa.msg}")),
        { fb: Exec[I,O2] =>
          val pairs = fb.track.zip(fb.fs)
          val npairs = pairs.filter{ case (trk,_) => compilePartial(trk,iff) }
          val (trcks, fs) = npairs.unzip
          Right(Exec( fs, trcks ))
        }
      )
      tmp1
    case Iff(iff,f) =>
      val tmp0 = compile(f,track)
      val tmp1: Either[ADWError, Exec[I,O2]] = tmp0.fold(
        fa => Left(ADWError(s"$o - ${fa.msg}")),
        { fb: Exec[I,O2] =>
          val tmp: Iterable[(Runtime, I) => Either[ADWError, (Runtime, O2)]] = fb.fs.map { fi =>
            def func(acc0:Runtime, i:I) = {
              if (iff(acc0,i,None)) {
                val ret: Either[ADWError, (Runtime, O2)] = fi(acc0,i)
                ret
              }
              else
                Left(ADWError(s"$o failed"))
            }
            func _
          }
          Right(Exec( tmp, fb.track ))
        }
      )
      tmp1
    case All(fs) =>
      val lc: Seq[Either[ADWError, Exec[I,O2]]] = fs.map( compile(_, track) )
      val lcr: Either[ADWError, List[Exec[I,O2]]] = ADWError.leftOrRight(lc)
      val flcr: Either[ADWError, List[Exec1[I,O2]]] = flatten(lcr)
      flcr match {
        case Left(l) => Left(l)
        case Right(lcrx) =>
          val tmp: List[(Runtime,I) => Either[ADWError, (Runtime, O2)]] = lcrx.map { fi =>
            def func(acc0:Runtime, i:I) = {
              fi.f(acc0,i)
            }
            func _
          }
          val trks = lcrx.map( _.track )
          Right(Exec( tmp, trks ))
      }
    case SeqOp(l1,l2) =>
      val l1c: Seq[Either[ADWError, Exec[I,O1]]] = l1.map( compile(_, track) )
      // Remove call from l1
      // TODO: possible BUG - compile may not have found the call to remove
      val track1 = track match { case Runtime(_,_,_,_,_) => track.drop ; case _ => track }
      val l2c: Seq[Either[ADWError, Exec[O1,O2]]] = l2.map( compile(_, track1) )
      val l1cr: Either[ADWError, List[Exec[I,O1]]] = ADWError.leftOrRight(l1c)
      val l2cr: Either[ADWError, List[Exec[O1,O2]]] = ADWError.leftOrRight(l2c)
      val fl1cr: Either[ADWError, List[Exec1[I,O1]]] = flatten(l1cr)
      val fl2cr: Either[ADWError, List[Exec1[O1,O2]]] = flatten(l2cr)
      val extc: Either[ADWError, (List[Exec1[I,O1]], List[Exec1[O1,O2]])] = extend(fl1cr, fl2cr)
      val tmp : Either[ADWError, Exec[I,O2]] = extc match {
        case Left(l) => Left(l)
        case Right((l1crx,l2crx)) =>
          val tmp1: Iterable[(Runtime,I) => Either[ADWError,(Runtime,O2)]] = l1crx.zip(l2crx).map { case (f1, f2) =>
            def func(acc0: Runtime, i: I) = {
              f1.f(acc0, i) match {
                case Left(e) => Left(e)
                case Right((acc1, o1)) => f2.f(acc1, o1)
              }
            }
            func _
          }
          val trks: List[Tracking] = l1crx.zip(l2crx).map { case (t1, t2) => t1.track + t2.track }
          val tmp: Exec[I, O2] = Exec(tmp1,trks)
          Right(tmp)
      }
      tmp
    case CrossOp(l1,l2) =>
      val l1c: Seq[Either[ADWError, Exec[I,O1]]] = l1.map( compile(_, track) )
      // Remove call from l1
      // TODO: possible BUG - compile may not have found the call to remove
      val track1 = track match { case Runtime(_,_,_,_,_) => track.drop ; case _ => track }
      val l2c: Seq[Either[ADWError, Exec[O1,O2]]] = l2.map( compile(_, track1) )
      val l1cr: Either[ADWError, List[Exec[I,O1]]] = ADWError.leftOrRight(l1c)
      val l2cr: Either[ADWError, List[Exec[O1,O2]]] = ADWError.leftOrRight(l2c)
      val fl1cr: Either[ADWError, List[Exec1[I,O1]]] = flatten(l1cr)
      val fl2cr: Either[ADWError, List[Exec1[O1,O2]]] = flatten(l2cr)
      val tmpx: Either[ADWError, Exec[I,O2]] = (fl1cr,fl2cr) match {
        case (Left(ADWError(l)),Left(ADWError(r))) => Left(ADWError(l+","+r))
        case (Left(l),_) => Left(l)
        case (_,Left(r)) => Left(r)
        case (Right(l1crx),Right(l2crx)) =>
          val tmp1: Iterable[(Runtime,I) => Either[ADWError,(Runtime,O2)]] = l1crx.flatMap { f1 =>
            l2crx.map { f2 =>
              def func(acc0: Runtime, i: I): Either[ADWError, (Runtime, O2)] = {
                f1.f(acc0, i) match {
                  case Left(e) => Left(e)
                  case Right((acc1, o1)) => f2.f(acc1, o1)
                }
              }
              func _
            }
          }
          val trks: List[Tracking] = l1crx.flatMap { f1 =>
            l2crx.map { f2 => f1.track + f2.track }
          }
          val tmp: Exec[I, O2] = Exec(tmp1,trks)
          Right(tmp)
      }
      tmpx
  }

  /**
    * Execute the compiled pipes once for each input. As
    * each pipe is execute, its execution is tracked into
    * a [[Runtime]] trace. If any pipes execution fails, it
    * is silently removed. To retain all traces see
    * [[execDebug]].
    *
    * @param i List of inputs to apply to the pipes.
    * @param f List of compiled pipes t execute
    * @tparam I Pipe's input type
    * @tparam O Pipes output type
    * @return Returns the traces and the output
    */
  def exec[I,O](i: Seq[I], f:Exec[I,O]): Seq[(Runtime, O)] = {
    val acc = Runtime()
    i.flatMap { i =>
      //f.fs.flatMap( f => f(acc,i) )
      // Convert error to None so they can be removed
      f.fs.flatMap { f => f(acc,i).fold( _ => None, e => Some(e))
      }
    }
  }

  /**
    * Execute the compiled pipes once for each input. As
    * each pipe is execute, its execution is tracked into
    * a [[Runtime]] trace. If any pipes execution fails, it
    * is recored in the trace that is returned. To silently
    * removed all failed traces see [[exec]].
    *
    * @param i List of inputs to apply to the pipes.
    * @param f List of compiled pipes t execute
    * @tparam I Pipe's input type
    * @tparam O Pipes output type
    * @return Returns the traces and the output
    */
  def execDebug[I,O](i: Seq[I], f:Exec[I,O]): Seq[Either[ADWError, (Runtime, O)]] = {
    val acc = Runtime()
    i.flatMap { i => f.fs.map(f => f(acc,i) ) }
  }


}
