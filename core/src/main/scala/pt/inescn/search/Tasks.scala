package pt.inescn.search

import pt.inescn.etl.Transform._
import pt.inescn.search.Pipes.Func
import pt.inescn.utils.ADWError

import scala.annotation.tailrec

object Tasks {


  /**
    * Represents the parameters of task. Each parameter
    * is used to generate a [[Pipes.Op]]. A parameter
    * is optional and a default value may be set to
    * [[scala.None]].
    *
    * @tparam P type of task parameter
    */
  type Params[P] = List[Option[P]]

  /**
    * A task is a container for the [[Pipes.Op]] that automates the
    * creation of a [[Pipes.All]] consisting of a list of [[Pipes.Op]],
    * one for each parameter in [[Params]]. These parameters are optional
    * [[scala.Option]]. When a [[scala.None]] is used, it represents either
    * a default option or no option.
    *
    * The task's `defaultOf` function is called prior the execution of
    * the operation. It receives optional parameter and input. This allows
    * the task to check and set the parameters appropriately. It can use
    * the parameter as is, check if it is a default and set the default
    * parameter value if need be (based in the inout, for example) or,
    * if the parameter is not a default none, use it as is.
    *
    * Each parameter is combined with the base function `exec` to create
    * a [[Pipes.Op]]. The list of operations is returned as a [[Pipes.All]].
    * One can use the `toFunc` call to combine the `defaultOf` and `exec`
    * functions into a pipe operation. This means that these tasks can then
    * be combined into pipes via the [[Pipes]] DSL.
    *
    * @tparam P type of task parameter
    * @tparam I type of task input
    * @tparam O type of task output
    */
  sealed trait PipeTask[P,I,O] {
    def name : String
    val params : Params[P]

    /**
      * This function is called before the main `exec` is called.
      * It allows the task to set default parameter values based
      * on the user defined parameter and the input. Default
      * parameters are signaled with a [[scala.None]]
      *
      * @param p the parameter to check and possibly overwrite
      * @param i the input that will be passed to `exec`
      * @return the new parameter
      */
    def defaultOf(p:Option[P],i:I): Option[P]

    /**
      * This is the main function that will actually be executed within
      * the compiled pipe. It receives the parameter (after being
      * pre-processed) and the input and generates the output.
      *
      * @param p the parameter to check and possibly overwrite
      * @param i the input that will be passed to `exec`
      * @return the output that is either final are passed on to
      *         another task
      */
    def exec(p:Option[P],i:I): Either[ADWError,O]

    // TODO: def toJson(p:Option[P]): String // and inverse

    /**
      * Builds the [[Pipes.All]] construct base on the pre-processing
      * function `defaultOf` and the main function `exec`. One
      * [[Pipes.Func]] is generated for each parameter `params`.
      *
      * @return a list of [[Pipes.Op]] for ech parameter
      */
    def toFunc: Pipes.All[I, Nothing, O] = {
      val tmp: List[Func[Option[P], I, O]] = params.map { pi =>
        Pipes.fp(name, pi, defaultOf, exec)
      }
      Pipes.All(tmp)
    }
  }

/*
  https://github.com/zavtech/morpheus-core
  https://github.com/martincooper/java-datatable
  http://quandl4j.org/
    https://github.com/jtablesaw/tablesaw
  https://arrow.apache.org/

  // scala.collection.mutable.WrappedArray
  // scala.collection.mutable.ArrayOps
  case class Columns(cs:IndexedSeq[Array[Double]]) {
    //def apply(cols:Int*): IndexedSeq[Array[Double]]
    //def apply(cols:Int*)(rows:Int*): IndexedSeq[Array[Double]]
  }

  object Columns {

  }
*/

  /* Tasks input and out types aliases */

  type Vec = Array[Double]
  type Vec2 = IndexedSeq[Array[Double]] // Column major

  /* Tasks paramete aliases */

  type Param0 = Unit
  type Param2 = (Double,Double)
  type Param3 = (Double,Double,Double)

  type Parami1 = Int
  type Parami2 = (Int,Int)


  /**
    * Converts a list of options `P` into an option of a list
    * of `P`s.
    *
    * @param l list of options `A`s
    * @tparam A type of the option list
    * @return option of a list of `A`s
    */
  def flatten[A](l:List[Option[A]]) : Option[List[A]] = {
    @tailrec
    def loop(ls:List[Option[A]],acc:Option[List[A]]):Option[List[A]] = (acc,ls) match {
      case (None,_) => None
      case (_,Nil) =>  acc
      case (Some(acl), Some(h)::t) => loop(t, Some(h::acl))
      case (Some(_), None::_) =>  None
    }
    loop(l,Some(List()))
  }

  /**
    * This task applies another task `inner` to to each [[Vec]] that is
    * in a matrix [[Vec2]]. The `inner` task is applied to the [[Vec]]
    * columns indexed by `idxs`. If no column indexes are indicated, then
    * the function is applied to all columns.
    *
    * The `inner` tasks may have several parameters. Each of these
    * parameters is used for all calls of the `inner` on each of the
    * columns.
    *
    * As with any task the `defaultOf` is called prior to the execution
    * of `exec` in order to (re)set the appropriate parameters. It delegates
    * to the `inner`'s `defaultOf` function to set those parameters. Note
    * that this is repeated for each input-parameter pair for all of the
    * columns.
    *
    * @param inner the task that is applied to each column of [[Vec2]]
    * @param idxs the indexes of the columns where `inner` will be applied to
    * @tparam X the type the `inner`'s parameters
    */
  case class ApplyColumns1[X](inner: PipeTask[X,Vec,Vec],
                              idxs : List[Parami1]) extends PipeTask[List[X],Vec2,Vec2] {

    val tmp: List[Option[List[X]]] = inner.params.map{ p:Option[X] =>p.flatMap(e => Some(List(e))) }
    override val params: Params[List[X]] = tmp
    // TODO: how to add column number parameter?
    override val name: String = s"ApplyColumns1.${inner.name}"

    /**
      * This function sets the `inner`'s parameters. It replicates
      * this parameters for each of the column indexes. If the column
      * indexes are not set, all [[Vec2]] indexes are used.
      *
      * @param p the parameter to check and possibly overwrite
      * @param i the input that will be passed to `exec`
      * @return the new parameter
      */
    override def defaultOf(p: Option[List[X]], i: Vec2): Option[List[X]] = p match {
      case None =>
        // If column indexes not indicated, apply to all of them
        val l: List[Parami1] = if (idxs.isEmpty) i.indices.toList else idxs
        val nparam: List[Option[X]] = l.map(idx => inner.defaultOf(None,i(idx)))
        // Default on None if any error occurred
        val rparams: Option[List[X]] = flatten(nparam)
        rparams
      case Some(innerParam) =>
        // If column indexes not indicated, apply to all of them
        val l: List[Parami1] = if (idxs.isEmpty) i.indices.toList else idxs
        // Assume that we provide only a single default parameter for all columns
        val param = if (innerParam.isEmpty || innerParam.size > 1) None else Some(innerParam.head)
        // Make sure this default is correct and applies to the corresponding column
        val nparam: List[Option[X]] = l.map(idx => inner.defaultOf(param,i(idx)))
        // Default on None if any error occurred
        val rparams: Option[List[X]] = flatten(nparam)
        rparams
    }

    /**
      * This function applies the `inner` `exec` to all of the
      * indexed columns. Each column is updated with the `inner`
      * output.
      *
      * @param p the parameter to check and possibly overwrite
      * @param i the input that will be passed to `exec`
      * @return the output that is either final are passed on to
      *         another task
      */
    override def exec(p: Option[List[X]], i: Vec2): Either[ADWError, Vec2] = {
      // If column indexes not indicated, apply to all of them
      val l: List[Parami1] = if (idxs.isEmpty) i.indices.toList else idxs
      // Generate the pair (col index, parameter to use on this column)
      val colParam: List[(Parami1, X)] = l.zip(p.getOrElse(Nil))
      // Apply the inner task to each column using the corresponding parameter
      // We do not fold here so that we can terminate early
      def loop(acc:Vec2, colParams: List[(Parami1,X)]):Either[ADWError,Vec2] = colParams match {
        case Nil => Right(acc)
        case p::t =>
          val (colIdx,colP) = p
          val col: Vec = i(colIdx)
          val ncol: Either[ADWError, Vec] = inner.exec(Some(colP),col)
          val nacc = ncol.flatMap{ c => Right(acc.updated(colIdx,c)) }
          nacc match {
            case Left(e) => Left(e)
            case Right(e) => loop(e,t)
          }
      }
      loop(i, colParam)
    }
  }

  object ApplyColumns1 {
    def apply[X](inner: PipeTask[X,Vec,Vec], cols:Parami1*):ApplyColumns1[X] = {
      new ApplyColumns1(inner, cols.toList)
    }
  }

  /**
    * Tas used to normalize a column [[Vec]]. If the parameters are not
    * set, they will be set using the input.
    *
    * @param params normalization parameters (mean and standard deviations)
    * @param name name of the task
    */
  case class Normalize(override val params: Params[Param2] = List(None),
                       override val name: String = "Normalize") extends PipeTask[Param2,Vec,Vec] {

    /**
      * If no mean and standard deviation is made available, calculate these.
      * If we do calculate these, return them so tha they can be used
      * elsewhere.
      *
      * @param p mean and standard deviation used for normalization
      * @param i data vector to be normalized
      * @return the mean and standard deviation if they were calculated
      */
    override def defaultOf(p: Option[Param2],i:Vec): Option[Param2] = p match {
      case None =>
        val meanv = mean(i)
        val sdv = standardDeviation(i)
        Some((meanv,sdv))
      case o@Some(_) => o
    }

    /**
      * Normalizes the input using each pair of mean and standard deviation
      * values to scale the data.
      *
      * @param p mean and standard deviation used for normalization
      * @param i data vector to be normalized
      * @return the normalized data
      */
    override def exec(p:Option[Param2], i:Vec): Either[ADWError,Vec] = {
      p match {
        case None => Left(ADWError("Parameter missing."))
        case Some((meanv, sdv)) =>
          val o = normalize(meanv, sdv, i)
          Right(o)
      }
    }
  }

  /**
    * Task that standardizes the [[Vec]] column. The upper and lower bounds
    * should be set otherwise the default range {-1.0 ... 1.0} will be used.
    *
    * @param params
    * @param name name of the task
    */
  case class Scale(override val params: Params[Param2] = List(None),
                   override val name: String = "Scale") extends PipeTask[Param2,Vec,Vec] {

    /**
      * If no lower and upper range is made available, use lower
      * set to -1.0 and upper set to 1.0. If we do calculate these, return
      * them so tha they can be used elsewhere.
      *
      * @param p mean and standard deviation used for normalization
      * @param i data vector to be normalized
      * @return the mean and standard deviation if they were calculated and
      *         the normalized data
      */
    override def defaultOf(p: Option[(Double, Double)], i: Vec): Option[(Double, Double)] = p match {
      case None =>
        val lower = -1.0
        val upper = 1.0
        Some((lower,upper))
      case o@Some(_) => o
    }

    /**
      * Scale the input using each pair of lower and upper bounds to scale
      * the data.
      *
      * @param p mean and standard deviation used for normalization
      * @param i data vector to be normalized
      * @return the normalized data
      */
    override def exec(p:Option[Param2], i:Vec): Either[ADWError,Vec] = {
      p match {
        case None => Left(ADWError("Parameter missing."))
        case Some((lower,upper)) =>
          val o = scale(lower, upper, i)
          Right(o)
      }
    }
  }

  object Scale {
    def apply(limits:Param2*): Scale = {
      val np = limits.map( Some(_) ).toList
      new Scale(np)
    }
  }

  /**
    * Task that converts the [[Vec]] to have unit length. No
    * parameters should (need to) be set.
    *
    * @param params Not used
    * @param name name of the task
    */
  case class UnitLen(override val params: Params[Param0] = List(None),
                     override val name: String = "Unit") extends PipeTask[Param0,Vec,Vec] {

    /**
      * No parameters are used. So don't change it.
      *
      * @param p not used
      * @param i data vector to be normalized
      * @return same parameter
      */
    override def defaultOf(p: Option[Param0], i: Vec): Option[Param0] = p

    /**
      * Scale the input to a unit vector [[pt.inescn.etl.Transform.unit]].
      * No parameters are used.
      *
      * @param p not used
      * @param i data vector to be normalized
      * @return the unit vector of the data
      */
    override def exec(p:Option[Param0], i:Vec): Either[ADWError,Vec] = {
      val o = unit(i)
      Right(o)
    }
  }

  /**
    * Task used to convert the values in [[Vec]] to their Softmax values.
    * No parameters should (need to) be set.
    *
    * @param params Not used
    * @param name name of the task
    */
  case class Softmax(override val params: Params[Param0] = List(None),
                     override val name: String = "Softmax") extends PipeTask[Param0,Vec,Vec] {

    /**
      * No parameters are used. So don't change it.
      *
      * @param p not used
      * @param i data vector to be normalized
      * @return same parameter
      */
    override def defaultOf(p: Option[Param0], i: Vec): Option[Param0] = p

    /**
      * Scale the input using a softmax function [[pt.inescn.etl.Transform.softmax]].
      * No parameters are used.
      *
      * @param p not used
      * @param i data vector to be normalized
      * @return the unit vector of the data
      */
    override def exec(p:Option[Param0], i:Vec): Either[ADWError,Vec] = {
      val o = softmax(i)
      Right(o)
    }
  }


  /**
    * Task used to convert the [[Vec]] to the lambda-Softmax values.
    * It uses the mean, standard deviation and lambda values. The
    * mean and standard deviation should be set automatically based
    * on the input. See the companion object that allows one to set
    * the lambda and calculates the other parameters.
    *
    * @param params usually the lambda, but may include the mean
    *               and standard deviation
    * @param name name of the task
    */
  case class SoftLambdaMax(override val params: Params[Param3] = List(None),
                           override val name: String = "SoftLambdaMax") extends PipeTask[Param3,Vec,Vec] {

    /**
      * Soft Lambda Scaling uses the mean, standard deviation and lambda values.
      * [[[pt.inescn.etl.Transform.softLambdaMax]]].
      * If these values are not provided we calculate them. If we do calculate these,
      * return them so tha they can be used elsewhere.
      *
      * @param p mean, standard deviation and lambda used for scaling
      * @param i data vector to be normalized
      * @return the mean, standard deviation and lambda if they were
      *         calculated
      */
    override def defaultOf(p: Option[(Double, Double, Double)], i: Vec): Option[(Double, Double, Double)] = p match {
      case None =>
        val meanv = mean(i)
        val sdv = standardDeviation(i)
        val lambda = 2.0
        Some((meanv,sdv,lambda))
      case Some((meanv,sdv,lambda)) =>
        val (nmean, nsdv) = if (meanv.isNaN || sdv.isNaN) (mean(i), standardDeviation(i)) else (meanv,sdv)
        Some((nmean,nsdv,lambda))
    }

    /**
      * Scale the input using mean, standard deviation and lambda values.
      * [[[pt.inescn.etl.Transform.softLambdaMax]]].
      *
      * @param p mean, standard deviation and lambda used for scaling
      * @param i data vector to be normalized
      * @return the scaled data
      */
    override def exec(p:Option[Param3], i:Vec): Either[ADWError,Vec] = {
      p match {
        case None => Left(ADWError("Parameter missing."))
        case Some((meanv,sdv,lambda)) =>
          val o = softLambdaMax(meanv, sdv, i, lambda)
          Right(o)
      }
    }
  }

  object SoftLambdaMax {
    def apply(lambdas: Double*): SoftLambdaMax = {
      val params = lambdas.map(e => Some((Double.NaN, Double.NaN, e)))
      SoftLambdaMax(params.toList)
    }
  }

  /**
    * This task converts the [[Vec]] values to the logit value. The
    * parameters consisting of the upper asymptote, the slope of the curve
    * at zero and the y crossing at x=0.
    *
    * @param params the logit upper asymptote, slope at zero and the y crossing
    * @param name   name of the task
    */
  case class Logit(override val params: Params[Param3] = List(None),
                   override val name: String = "Logit") extends PipeTask[Param3,Vec,Vec] {

    /**
      * We can provide parameters to set the upper asymptote (symmetrical at x=0)
      * value, the slope of the curve at zero and the y crossing at x=0. If none
      * of these values are provided we set the default (asymptote between -1 and
      * 1 crossing at y = 0). If we do set these, return them so that they can be
      * used elsewhere.
      *
      * @param p mean and standard deviation used for normalization
      * @param i data vector to be normalized
      * @return the upper asymptote (symmetrical at x=0) value, the slope of the
      * curve at zero and the y crossing at x=0 used to scale the data
      */
    override def defaultOf(p: Option[(Double, Double, Double)], i: Vec): Option[Param3] = p match {
      case None =>
        val l = 1.0
        val k = 1.0
        val zero = 0.0
        Some((l,k,zero))
      case o@Some(_) => o
    }

    /**
      * Scale the input using the logit function [[pt.inescn.etl.Transform.logit]].
      *
      * @param p mean and standard deviation used for normalization
      * @param i data vector to be normalized
      * @return scaled data
      */
    override def exec(p:Option[Param3], i:Vec): Either[ADWError,Vec] = {
      p match {
        case None => Left(ADWError("Parameter missing."))
        case Some((l,k,zero)) =>
          val o = logit(i, l, k, zero)
          Right(o)
      }
    }
  }


  /**
    * This task detrends a [[Vec2]] using simple linear regression. The
    * parameters indicating the x and y columns respectively should be
    * set. If not the task assumes that the y is in the first (0) column,
    * and the x is in the second (1) column.
    *
    * @param params the x and y columns
    * @param name name of the task
    */
  case class RegressionDetrend(override val params: Params[Parami2] = List(None),
                               override val name: String = "RegressionDetrend") extends PipeTask[Parami2,Vec2,Vec2] {

    /**
      * Parameters indicate the index of the x column and y column
      * respectively. If no parameters are given we assume that the
      * y column is at column 0 and the x column is column 1.
      *
      * @param p not used
      * @param i data vector to be detrended
      * @return same as input
      */
    override def defaultOf(p: Option[Parami2], i: Vec2): Option[Parami2] = p match {
      case None =>
        val xi = 1
        val yi = 0
        Some((xi,yi))
      case o@Some(_) => o
    }

    /**
      * Detrend the input using regression with
      * [[[pt.inescn.etl.Transform.regressionDetrend]]].
      * Parameters indicate the index of the x column and y column
      * respectively.
      *
      * @param p not used
      * @param i data vector to be detrended
      * @return detrended data
      */
    override def exec(p:Option[Parami2], i:Vec2): Either[ADWError,Vec2] = {
      p match {
        case None => Left(ADWError("Parameter missing."))
        case Some((xi,yi)) =>
          val o = regressionDetrend(i, xi, yi)
          Right(o)
      }
    }
  }

  object RegressionDetrend {
    def apply(x:Int, y:Int): RegressionDetrend = {
      val np = List(Some( (x, y) ) )
      new RegressionDetrend(np)
    }
  }


  /**
    * This task detrends a [[Vec2]] using simple difference. The
    * parameters indicating the x and y columns respectively should be
    * set. If not the task assumes that the y is in the first (0) column,
    * and the x is in the second (1) column.
    *
    * @param params the x and y columns
    * @param name name of the task
    */
  case class DifferenceDetrend(override val params: Params[Parami2] = List(None),
                               override val name: String = "DifferenceDetrend") extends PipeTask[Parami2,Vec2,Vec2] {

    /**
      * Parameters indicate the index of the x column and y column
      * respectively. If no parameters are given we assume that the
      * y column is at column 0 and the x column is column 1.
      *
      * @param p not used
      * @param i data vector to be detrended
      * @return same as input
      */
    override def defaultOf(p: Option[Parami2], i: Vec2): Option[Parami2] = p match {
      case None =>
        val xi = 1
        val yi = 0
        Some((xi,yi))
      case o@Some(_) => o
    }

    /**
      * Detrend the input using difference with [[pt.inescn.etl.Transform.differenceDetrend]].
      *
      * Parameters indicate the index of the x column and y column
      * respectively.
      *
      * @param p not used
      * @param i data vector to be detrended
      * @return detrended data
      */
    override def exec(p:Option[Parami2], i:Vec2): Either[ADWError,Vec2] = {
      p match {
        case None => Left(ADWError("Parameter missing."))
        case Some((xi,yi)) =>
          val o = differenceDetrend(i, xi, yi)
          Right(o)
      }
    }
  }

  object DifferenceDetrend {
    def apply(x:Int, y:Int): DifferenceDetrend = {
      val np = List(Some( (x, y) ) )
      new DifferenceDetrend(np)
    }
  }


}
