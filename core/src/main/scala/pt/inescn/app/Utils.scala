package pt.inescn.app

import better.files.File
import io.circe.Error
import io.circe.Error
import io.circe.parser.decode
import io.circe.generic.auto._
import io.circe.syntax._
import pt.inescn.etl.stream.Load.{Row, Val}
import pt.inescn.utils.ADWError
import pt.inescn.etl.stream.RabbitMQ

import scala.collection.immutable
import scala.collection.mutable.ListBuffer

/**
  * Utility routines used to read and parse the INEGI data. This includes
  * data in files and data read from message brokers.
  *
  * The data file names have a specific format that encode additional
  * information such as experiment ID and other features. We parse these so
  * that they can be later added to the data used in the analysis and
  * prediction experiments and simulations. The generic CSV data loading
  * routines use these file name parsers to augment the data used to
  * generate the [[pt.inescn.etl.stream.Load.Frame]].
  *
  * Each message read from a message broker has a specific format encoding
  * one or more records. The message must first be decoded into a user
  * defined type and then converted into a [[Row]]. In this case each message
  * consists of 'n' records. Each record consists of a map from a key (the
  * column or field name) to a vector of values (sensor readings). Each
  * combination of a key and one value is then used to construct the [[Row]].
  * All records are returned as an iterator of [[Row]]s.
  *
  */
object Utils {

  /**
    * Parses the file name looking for features that are
    * separated by an underscore. Each feature consists
    * of a unit/name concatenated with a value. For example
    * the the rotation speed of 1000 RPMs is written as
    * `_rpm1000_`. Note that not all features have a value
    * or unit. So they are processed in a fixed order. The
    * following is the naming convention used:
    *
    * Ball-bearing data
    * The file will be used to encode the experiment set-up and label.
    * The file name has the following structure:
    *
    * - **rolX** indicates the ID of the ball-bearing used. It identifies
    *            the physical component.
    * - **rpmXXXX** indicates the rotation speed of bearings (in this case
    *             the number o revolutions per minute)
    * - **hpXX** indicates the load applied to the bearings (in this case
    *            measured in horse power)
    * - **AAAAA** is the code labelling the data with the type of failure.
    *            The current codes are:
    *            - _norm_: No anomaly (failure)
    *            - _ir_: damage in the inner race
    *            - _or_: damage in the outer race
    *            - _b_: damage in the ball-bearing
    *            - **micronXXXX** : is the size of the damage (in this case
    *                               measured in microns)
    *            - **expXXX** : is the identifier of the experiment
    *
    * @param f The file token used to get its name
    * @return an error if an error occurs during parsing otherwise two lists
    *         of tuples where the first is the feature key "bearing", "speed",
    *         "load", "damage", "damage_size", "exp" and the second elements
    *         are respectively the values and the units.
    */
  def parseFileName(f:File): Either[ADWError, (Map[String, String], Map[String, String])] = {
    // bearing ID, rotation speed", load, damage_size, exp
    val feature_names = List("bearing", "speed", "load", "damage", "damage_size", "exp")
    val len = feature_names.length
    val name_ext = f.name
    val ext = f.extension()
    ext match {
      case Some(e) =>
        // remove the extension from the file name
        val name =  name_ext.replace(e, "")
        // Split the name into features
        val features = name.split("_").map(_.toLowerCase())
        // Get the feature units by removing the numeric (integer) values from the end of the feature
        val units = features.map(s => s.replaceFirst("[0-9]*$",""))
        // Get the value by stripping the units from the features
        val values_tmp = features.zip(units).map{ case (s,u) => s.replace(u,"") }
        // Some features are nominal, put them back were they should be
        val values = values_tmp.zip(units).map{ case(value,unit) => if (value == "") unit else value }
        // Map from expected feature to value
        val named_features  = feature_names.zip(values).toMap
        // Map from expected feature to unit
        val named_units  = feature_names.zip(units).toMap
        if (units.length == len)
          Right((named_features, named_units))
        else
          Left(ADWError(s"Unexpected length of features ($name_ext). Expected $len but got ${units.length}"))
      case None =>
        Left(ADWError(s"No extension found ($name_ext)."))
    }
  }


  /**
    * Another possible format.
    *
    * @see [[parseFileName()]]
    *
    * @param f The file token used to get its name
    * @return an error if an error occurs during parsing otherwise two lists
    *         of tuples where the first is the feature key "bearing", "speed",
    *         "load", "damage", "exp" and the second elements are respectively
    *         the values and the units.
    */
  def parseFileName2(f:File): Either[ADWError, (Map[String, String], Map[String, String])] = {
    // bearing ID, rotation speed", load, damage_size, exp
    val feature_names = List("bearing", "speed", "damage", "exp")
    val len = feature_names.length
    val name_ext = f.name

    println(name_ext)


    val ext = f.extension()
    ext match {
      case Some(e) =>
        // remove the extension from the file name
        val name =  name_ext.replace(e, "")
        // Split the name into features
        val features = name.split("_").map(_.toLowerCase())
        // Get the feature units by removing the numeric (integer) values from the end of the feature
        val units = features.map(s => s.replaceFirst("[0-9]*$",""))
        // Get the value by stripping the units from the features
        val values_tmp = features.zip(units).map{ case (s,u) => s.replace(u,"") }
        // Some features are nominal, put them back were they should be
        val values = values_tmp.zip(units).map{ case(value,unit) => if (value == "") unit else value }
        // Map from expected feature to value
        val named_features  = feature_names.zip(values).toMap
        // Map from expected feature to unit
        val named_units  = feature_names.zip(units).toMap
        if (units.length == len)
          Right((named_features, named_units))
        else
          Left(ADWError(s"Unexpected length of features ($name_ext). Expected $len but got ${units.length}"))
      case None =>
        Left(ADWError(s"No extension found ($name_ext)."))
    }
  }

  /**
    * This function receives a JSON string and attempts to decode it into a
    * `Map[String, Vector[String] ] ]`. The map's keys are the names of the
    * [[Row]] columns. The `Vector[String] ]` represents several records of
    * that specific column. Each map in effects represents several [[Row]]
    * (as many as the number of elements in the vector).
    *
    * @param str JSON string
    * @return returns either a Map with the decode data or an Error that may
    *         have occurred during decoding.
    */
  def JSONtoMap(str: String): Either[Error, Map[String, Vector[String]]] = {
    decode[Map[String, Vector[String]]](str) match {
      case Left(e) =>
        Left(e)
      case Right(s) =>
        Right(s)
    }
  }

  /**
    * This function takes in a [[String]] that has a message obtained from a
    * message broker. The string represents a sequence of data records in
    * JSON records. The string is decoded and converted into an iterator of
    * [[Row]] that holds the data records.
    *
    * @param str JSON string with the message that contains one or more data
    *            records.
    * @return iterator of [[Row]] representing the data records
    */
  def toJSONIterator(str: String): Iterator[Row] = {

    JSONtoMap(str) match {
      case Left(e) =>
        throw new Exception(s"Error in message decode: $e")

      case Right(map) =>
        val Msg: Map[String, Vector[String]] = Right(map).right.get
        val iter: Iterator[Row] = RabbitMQ.splitMap(Msg)
        iter
    }
  }


  def toCSVStringIterator(str: String): Iterator[Row] = {
    val lines = str.split("\n")
    val csvLines = lines.map( s => s.split(",").map(_.trim))
    val indices = csvLines.head.indices.map(_.toString)   //TODO: possible bug - use of indices instead of head values
    val rowlLines: Array[immutable.IndexedSeq[(String, String)]] = csvLines.map(l => indices.zip(l))
    val rows = rowlLines.map( e => Row(e:_*) )
    rows.toIterator
  }

  /*added functions*/
  /**
    * These functions take in a [[String]] that has a message obtained from a
    * message broker. The string represents a sequence of data records in
    * CSV string records. The string is decoded and converted into an iterator of
    * [[Row]] that holds the data records.
    */

  /**
    * The function toStringIterator is used when consuming the messages from the block
    * queue, where the CSV string has an header (identifying the variables)
    *
    * @param str CSV string with the message that contains one or more data
    *            records.
    * @return iterator of [[Row]] representing the data records
    */
  def toStringIterator(str: String): Iterator[Row] = {
    val lines = str.split("\n")

    //in order not to be exclusive, ie, either split by ",", or split by "|":
    val rgx = "[,|]"
    val linesColumns:Array[Array[String]] = lines.map(l => l.split(rgx).map(_.trim))
    val header:Array[String] = linesColumns(0)

    //when using zip, if the collections have different lengths it discard the elements of the longer list that can't be matched
    //when using zipAll, rather than discarding (thus creating a smaller collection) it takes the parameter that will replace those elements
    val rowLines:Array[Array[(String, String)]] = linesColumns.drop(1).map(l=>header.zipAll(l,"",""))
    val rows:Array[Row] = rowLines.map( e => Row(e:_*) )
    rows.toIterator
  }

  /**
    * The function toStringIterator_noHeader is used when consuming the messages
    * related to the independent variables, ie it receives two values corresponding
    * to a single observation (no header).
    * By convention, the two values respect the ordering: timestamp, observation_value
    *
    * @param timeStampID identifies the value the timestamp of the observation
    * @param varID identifies the value the independent variable takes
    * @param str CSV string with the message that contains one or more data
    *            records.
    * @return iterator of [[Row]] representing the data records
    * */
  //def toStringIterator_noHeader(varID: String)(str: String): Iterator[Row] = {
  def toStringIterator_noHeader(timeStampID:String, varID: String)(str: String): Iterator[Row] = {
    val wStr:String = timeStampID + "," + varID + "\n" + str
    val lines = wStr.split("\n")
    val rgx = "[,|]"
    val linesColumns:Array[Array[String]] = lines.map(l => l.split(rgx).map(_.trim))
    val header:Array[String] = linesColumns(0)

    //when using zip, if the collections have different lengths it discard the elements of the longer list that can't be matched
    //when using zipAll, rather than discarding (thus creating a smaller collection) it takes the parameter that will replace those elements
    val rowLines:Array[Array[(String, String)]] = linesColumns.drop(1).map(l=>header.zipAll(l,"",""))
    val rows:Array[Row] = rowLines.map( e => Row(e:_*) )
    rows.toIterator
  }

  /**
    * the following are functions to parse messages from the broker in JSON format, adapted from
    * similar function used for adira4.0:
    * @see [[pt.inescn.app.Utils.toJSONIterator()]]
    * @see [[pt.inescn.app.Utils.JSONtoMap()]]
    * @see [[pt.inescn.etl.stream.RabbitMQ.splitMap()]]
    *
    * check each for the details.
    * Here the adaptation is that:
    * Map types for message decoding are changed from
    * `Map[String, Vector[String]]` to `Map[String, Double]`
    * because for the case of adira messages are received in blocks, whereas here it is processed
    * line by line (thus no vector of strings received)
    */

  /**
    * function adapted from:
    *
    * @see [[pt.inescn.etl.stream.RabbitMQ.splitMap()]]
    *      check it for details.
    *
    * Note: Unlike the adira case, here we do not need a buffer (there are not multiple messages).
    * Adapted accordingly.
    *
    * @param Msg
    * @return
    */
  def splitMap_single(Msg: Map[String, String]): Iterator[Row] = {
    //val buf: ListBuffer[Row] = ListBuffer()
    //buf += Row(Msg.map { case (e1, e2) => (e1, Val(e2)) })

    //unlike adira, as it receives a single message at a time - there is no need to use ListBuffer
    val buf: List[Row] = List(Row(Msg.map { case (e1, e2) => (e1, Val(e2)) }))

    buf.toIterator
  }

  /**
    * function adapted from:
    * @see [[pt.inescn.app.Utils.JSONtoMap()]]
    * check it for details.
    *
    * @param str
    * @return
    */
  def JSONtoMap_single(str: String): Either[Error, Map[String, String]] = {
    decode[Map[String, String]](str) match {
      case Left(e) =>
        Left(e)
      case Right(s) =>
        Right (s)
    }
  }

  /**
    * function adapted from:
    * @see [[pt.inescn.app.Utils.toJSONIterator()]]
    * check it for details.
    *
    * @param str
    * @return
    */
  def toJSONIterator_single(str:String): Iterator[Row] = {
    JSONtoMap_single(str) match {
      case Left(e) =>
        throw new Exception(s"Error in message decode: $e")

      case Right(map) =>
        val Msg: Map[String, String] = Right(map).right.get
        val iter: Iterator[Row] = splitMap_single(Msg)
        iter
    }
  }

  /*added functions*/

}
