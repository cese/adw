package pt.inescn.app

import better.files.File
import scala.io.Source

/**
  * Utility functions used to handle configurations. These can be loaded from
  * a file or passed in via command line parameters.
  *
  */
object Configs {

  /*
   *  File loading
   */

  /**
    * Loads a pair key and value from a text file.
    * The formats can be (combinations of):
    *   key = value  // comment
    *   key : value  #  comment
    *   key value       comment
    *
    * @param filepath full path to the file that has the configuration data
    * @return a map with the key value pairs with string types
    */
  def loadFrom(filepath: String): Map[String, String] = {
    Source.fromFile(filepath)
      .getLines
      .filter( s => !s.trim.startsWith("//"))
      .map { e =>
        val stringSplit: Array[String] = e.split(" ")
        (stringSplit(0).trim, stringSplit(2).trim)
    }.toMap
  }

  /**
    * Checks if a file can be loaded (exists). The full
    * file path must be given.
    *
    * @param filepath full path to file
    * @return true if the file exists otherwise false
    */
  def canLoad(filepath: String): Boolean = File(filepath).exists

  /**
    * Takes a list of file paths and checks which of this exist. It then
    * returns first available file if it exists. Note that this list
    * should be ordered in **decreasing** priority. The first (highest)
    * priority file is used.
    *
    * @param filepaths list of full paths to the files
    * @return `None` of no file is available else `Some` of the first file
    *        found
    */
  def bootstrapFrom(filepaths: String*): Option[Map[String, String]] = {
    val fs = filepaths.filter(canLoad).headOption
    fs.map( loadFrom )
  }

  // priority order
  val defaults = List(".", "./config", "./configs")

  /**
    * Takes a file name (not full path) ad goes through a list of directories
    * were this file will be searched. The first file that exists will be used
    * to load the data.
    *
    * @param filename only the file name (not full path) that will be loaded
    * @return None of the file is not found in any of the pre-defined
    *         directories else Some map with the configuration key-value
    *         pairs.
    */
  def bootstrap(filename : String): Option[Map[String, String]] = {
    val filepaths = defaults.map(_ + "/" + filename)
    bootstrapFrom(filepaths:_*)
  }

  /*
   *  Command line parameters
   */


}
