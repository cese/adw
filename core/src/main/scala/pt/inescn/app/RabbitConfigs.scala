package pt.inescn.app

import java.time.Duration
import scala.collection.mutable.ListBuffer
import io.circe.generic.auto._
import io.circe.syntax._

import pt.inescn.etl.stream.RabbitMQ


/**
  *
  * * sbt "root/ runMain inegi.Configs"
  */
object RabbitConfigs {

  sealed trait CmdParams {
    val showHelp: Boolean = false
    val verbose: Int      = 0
    val helpK             = "-help"
    val verboseK          = "-verbose"

    val error:   ListBuffer[String] = new ListBuffer[String]()
    val example: String
    val helpMsg: Map[String, String]

    def loadDefaultValues(flag: String, value: String): CmdParams

    def parse(args: Array[String]): CmdParams
    def addError(err: String) : CmdParams

    def toJason: String

    def printHelp(): Unit = {
      val flags = helpMsg.keys
      val maxLen = flags.map(_.length).max
      val flagEqLen: Iterable[(String, String)] = flags.map{ s =>
        val diff = maxLen - s.length
        val add = " " * diff
        val fixedLen = s + add
        (s, fixedLen)
      }
      flagEqLen.foreach{ case (k, pk) => println(pk + ":" + helpMsg(k)) }
      println(s"\nExample: $example")
    }


  }

  /**
    * This function is used for the specific purpose of returning an help message in case it is requested in the cmd.
    * */
  def ReceiveAppConfigHelp = {
    "verbose        :Verbosity level to print debug and test information. By default no output.\n" +
    "host           :Message broke's address. IP or host name.\n" +
    "port           :Message broke's port number. Port number used for data reception.\n" +
    "queueName      :Message broke's queue name. Queue to which data is sent.\n" +
    "userName       :Message broke's access control credential. User name.\n" +
    "password       :Message broke's access control credential. Password.\n" +
    "iter           :Data reception is done synchronously (poll) or asynchronously (callback).\n" +
    "waitMultiplier :When polling we use exponential back-off wait-times. This is the wait-time multiplier.\n" +
    "numMessages    :Number of messages. If defined consume and process only this amount of records. Else process all the data.\n" +
    "bufferMsgSize  :Number of alert messages that are buffered before being send to the message broker\n" +
    "fileName       :The path of the configuration file.\n" +
    "varType        :The type of variables; either block variables (block) or independent variables (ind)\n" +
    "publishQueue   :The queue id where the publication is to be done (note: parameter to be used in the alert app only)\n" +
    "Example        :root/runMain inegi.ReceiveApp -verbose 1 -iter CallBack -waitMultiplier 2 -numMessages 5"
  }

  case class ReceiveAppConfig(connect:                          RabbitMQ.Config,
                              filename:                         String = "",
                              override val verbose:              Int = 0,
                              override val error: ListBuffer[String] = new ListBuffer[String](),
                              waitMultiplier:                    Int = 1,
                              iter:                           String = "poll",
                              numMessages:                       Int = -1,
                              bufferMsgSize:                     Int = 10,
                              override val showHelp:         Boolean = false,
                              varType:                        String = "",
                              publishQueue:                   String = "",
                              publishType:                    String = "outRange")
    extends CmdParams {

    val CALLBACK = "callback"
    val POLL = "poll"
    val MANUAL = "manual"

    val iterK           = "-iter"
    val waitMultiplierK = "-waitMultiplier"
    val numMessagesK    = "-numMessages"
    val bufferMsgSizeK  = "-bufferMsgSize"
    val hostK           = "-host"
    val passwordK       = "-password"
    val portK           = "-port"
    val queueNameK      = "-queueName"
    val userNameK       = "-userName"

    val fileNameK       = "-filename"
    val varTypeK        = "-vartype"
    val publishQueueK   = "-publishQueue"
    val publishTypeK    = "-publish"

    val example = "root/runMain inegi.ReceiveApp -verbose 1 -iter CallBack -waitMultiplier 2 -numMessages 5"

    val helpMsg: Map[String, String] = Map(
      verboseK        -> "Verbosity level to print debug and test information. By default no output.",
      hostK           -> "Message broke's address. IP or host name.",
      portK           -> "Message broke's port number. Port number used for data reception.",
      queueNameK      -> "Message broke's queue name. Queue to which data is sent.",
      userNameK       -> "Message broke's access control credential. User name.",
      passwordK       -> "Message broke's access control credential. Password.",
      iterK           -> "Data reception is done synchronously (poll) or asynchronously (callback).",
      waitMultiplierK -> "When polling we use exponential back-off wait-times. This is the wait-time multiplier.",
      numMessagesK    -> "Number of messages. If defined consume and process only this amount of records. Else process all the data.",
      bufferMsgSizeK  -> "Number of alert messages that are buffered before being send to the message broker",
      fileNameK       -> "The path of the configuration file.",
      varTypeK        -> "The type of variables; either block variables (block) or independent variables (ind)",
      publishQueueK   -> "The queue where the detection is to be published",
      publishTypeK    -> "The publication type; if 'all' then all messages are published, if 'outRange' only detected alerts are published (this is the default value)"
    )

    def addError(err: String): ReceiveAppConfig = copy(error = error += err)

    /**
      * Checks the parameters entered in the App and returns a
      * [[ReceiveAppConfig]] instance with these parameters.
      *
      */
    def loadDefaultValues(flag: String, value: String): ReceiveAppConfig = {

      flag match {

        case `iterK` =>
          val temp = value
          val newConf = copy(iter = temp)
          newConf

        case `waitMultiplierK` =>
          val temp = value.toInt
          val newConf = copy(waitMultiplier = temp)
          newConf

        case `numMessagesK` =>
          val temp = value.toInt
          val newConf = copy(numMessages = temp)
          newConf

        case `bufferMsgSizeK` =>
          val temp = value.toInt
          val newConf = copy(bufferMsgSize = temp)
          newConf

        case `verboseK` =>
          val temp = value.toInt
          val newConf = copy(verbose = temp)
          newConf

        case `hostK` =>
          val temp = connect.setHost(value)
          val newConf = copy(connect = temp)
          newConf

        case `passwordK` =>
          val temp = connect.setPassword(value)
          val newConf = copy(connect = temp)
          newConf

        case `portK` =>
          val temp = connect.setPort(value.toInt)
          val newConf = copy(connect = temp)
          newConf

        case `queueNameK`=>
          val temp = connect.setQueueName(value)
          val newConf = copy(connect = temp)
          newConf

        case  `userNameK`=>
          val temp = connect.setUserName(value)
          val newConf = copy(connect = temp)
          newConf

        case `helpK` =>
          val newConf = copy(showHelp = true)
          newConf

        case `fileNameK` =>
          val temp = value
          val newConf = copy(filename = temp)
          newConf

        case `varTypeK` =>
          val temp = value
          val newConf = copy(varType = temp)
          newConf

        case `publishQueueK` =>
          val temp = value
          val newConf = copy(publishQueue = temp)
          newConf

        case `publishTypeK` =>
          val temp = value
          val newConf = copy(publishType = temp)
          newConf

        case _ =>
          addError(s"flag $flag not found")
      }
    }

    override def toJason: String = this.asJson.toString()

    def parseArgs(args: Array[String], acum: ReceiveAppConfig): ReceiveAppConfig = {

      // Split command line arguments into flag name and value
      val argPairs: Iterator[Array[String]] = args.sliding(2, 2).map( _.map(_.trim) )

      val temp3 = argPairs.foldLeft(acum) { case (acc, a) =>
        if (a.length == 1 && (a(0) == acc.helpK) ){
          val flag = a(0)
          val value = "?"
          acc.loadDefaultValues(flag, value)
        } else if (a.length == 2) {
          val flag = a(0)
          val value = a(1).toLowerCase()
          acc.loadDefaultValues(flag, value)
        }
        else {
          val temp = acc.addError("Args must have length fag name and value" )
          //should delete the print?
          if (acc.verbose > 0) println(s"error : ${acc.error}")
          temp
        }
      }
      temp3
    }

    def parse(args: Array[String]): ReceiveAppConfig = parseArgs(args, this)
  }

  case class SendAppConfig(connect:                    RabbitMQ.Config,
                           bufferSize:                        Int = 10,
                           throttleWait:                     Long = 0,
                           override val verbose:              Int = 0,
                           dir:                            String = "",
                           shuffle:                       Boolean = false,
                           machineID:                      String = "machineID",
                           sensorID:                       String = "sensorID",
                           timeStampStart:                 String = "",
                           timeSample:                     String = "PT0.5S",
                           filter:                         String = "",
                           numMessages:                    String = "",
                           override val error: ListBuffer[String] = new ListBuffer[String](),
                           override val showHelp:         Boolean = false)
    extends CmdParams {

    val numMessagesK    = "-numMessages"
    val filterK         = "-filter"
    val hostK           = "-host"
    val passwordK       = "-password"
    val portK           = "-port"
    val queueNameK      = "-queueName"
    val userNameK       = "-userName"
    val dirK            = "-dir"
    val shuffleK        = "-shuffle"
    val machineIDK      = "-machineID"
    val sensorIDK       = "-sensorID"
    val timeStampStartK = "-timeStampStart"
    val timeSampleK     = "-timeSample"
    val bufferSizeK     = "-bufferSize"
    val throttleK       = "-throttle"

    val example = "root/runMain inegi.SendApp -verbose 1 -bufferSize 2 -machineID m1 -sensorID s1 -numMessages 100 -dir data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv"

    val helpMsg: Map[String, String] = Map(
      verboseK        -> "Verbosity level to print debug and test information. By default no output.",
      hostK           -> "Message broke's address. IP or host name.",
      portK           -> "Message broke's port number. Port number used for data reception.",
      queueNameK      -> "Message broke's queue name. Queue to which data is sent.",
      userNameK       -> "Message broke's access control credential. User name.",
      passwordK       -> "Message broke's access control credential. Password.",
      dirK            -> "File path. Filename or directory with files that contain the data to send.",
      filterK         -> "Globbing. Match string used to load a set of data files.",
      shuffleK        -> "Shuffle. Used to create a random sequence of data samples.",
      numMessagesK    -> "Number of messages. If defined use this to define the number of messages to send. Else send all the data. If 'forever' repeatedly send all data.",
      bufferSizeK     -> "Buffer Size. Number of records to pack into JSON format.",
      throttleK       -> "Number of milliseconds to wait between per message (throttles rate of transmission).",
      machineIDK      -> "Machine ID. Use this as the machine ID to send in messages.",
      sensorIDK       -> "Sensor ID. Use this as the sensor ID in the final frame with the messages.",
      timeStampStartK -> "Time-stamp. If defined use this as the staring time-stamp, otherwise use the current date and hour.",
      timeSampleK     -> "Duration. If defined use as the difference between the timestamps otherwise we default to 0.5 second. See Java.time.Duration.parse API."
    )

    val FOREVER = "forever"

    def addError(err: String): SendAppConfig = copy(error = error += err)

    /**
      * This function updates the default configuration with a command line's
      * flag and value. It checks the arguments flag name and converts its value
      * accordingly. If the flag name is not recognized an error is generated.
      *
      * entered in the App and returns a SendAppConfig instance with these parameters.
      *
      * @param flag flag that identifies the argument whose value is to be updated
      * @param value value to assign to the argument
      * @return updated [[SendAppConfig]]
      */
    def loadDefaultValues(flag: String, value: String): SendAppConfig = {

      flag match {
        case `numMessagesK` =>
          val temp = value
          val newConf = copy(numMessages = temp)
          newConf

        case `filterK` =>
          val temp = value
          val newConf = copy(filter = temp)
          newConf

        case `verboseK` =>
          val temp = value.toInt
          val newConf = copy(verbose = temp)
          newConf

        case `hostK` =>
          val temp = connect.setHost(value)
          val newConf = copy(connect = temp)
          newConf

        case `passwordK` =>
          val temp = connect.setPassword(value)
          val newConf = copy(connect = temp)
          newConf

        case `portK` =>
          val temp = connect.setPort(value.toInt)
          val newConf = copy(connect = temp)
          newConf

        case `queueNameK` =>
          val temp = connect.setQueueName(value)
          val newConf = copy(connect = temp)
          newConf

        case `userNameK` =>
          val temp = connect.setUserName(value)
          val newConf = copy(connect = temp)
          newConf

        case `dirK` =>
          val temp = value
          val newConf = copy(dir = temp)
          newConf

        case `shuffleK` =>
          val temp = value.toBoolean
          val newConf = copy(shuffle = temp)
          newConf

        case `machineIDK` =>
          val temp = value
          val newConf = copy(machineID = temp)
          newConf

        case `sensorIDK` =>
          val temp = value
          val newConf = copy(sensorID = temp)
          newConf

        case `timeStampStartK` =>
          val temp = value
          val newConf = copy(timeStampStart = temp)
          newConf

        case `timeSampleK` =>
          val temp = Duration.parse(value)
          val newConf = copy(timeSample = temp.toString)
          newConf

        case `bufferSizeK` =>
          val temp = value.toInt
          val newConf = copy(bufferSize = temp)
          newConf

        case `throttleK` =>
          val temp = value.toLong
          val newConf = copy(throttleWait = temp)
          newConf

        case `helpK` =>
          val newConf = copy(showHelp = true)
          newConf

        case _ =>
          addError(s"flag $flag not found" )
      }
    }

    override def toJason: String = this.asJson.toString()

    def parseArgs(args: Array[String], acum: SendAppConfig): SendAppConfig = {

      // Split command line arguments into flag name and value
      val argPairs: Iterator[Array[String]] = args.sliding(2, 2).map( _.map(_.trim) )

      val temp3 = argPairs.foldLeft(acum) { case (acc, a) =>
        if (a.length == 1 && (a(0) == acc.helpK) ){
          val flag = a(0)
          val value = "?"
          acc.loadDefaultValues(flag, value)
        } else if (a.length == 2) {
          val flag = a(0)
          val value = a(1).toLowerCase()
          acc.loadDefaultValues(flag, value)
        }
        else {
          val temp = acc.addError("Args must have length fag name and value" )
          if (acc.verbose > 0) println(s"error : ${acc.error}")
          temp
        }
      }
      temp3
    }

    def parse(args: Array[String]): SendAppConfig = parseArgs(args, this)

    // Interfacing with Java (copy does not work)
    def setNumMessages(n:String): SendAppConfig = copy(numMessages = n)
  }



  object ReceiveAppConfig{
    def apply(filename: String): ReceiveAppConfig = new ReceiveAppConfig(RabbitMQ.from(filename))
    def boot(filename: String): ReceiveAppConfig = new ReceiveAppConfig(RabbitMQ.boot(filename))
  }

  object SendAppConfig{
    def apply(filename: String): SendAppConfig = new SendAppConfig(RabbitMQ.from(filename))
    def boot(filename: String): SendAppConfig = new SendAppConfig(RabbitMQ.boot(filename))
  }

  def main(args: Array[String]): Unit = {
    val filename = "core/src/main/scala/inegi/SendConfig"
    val rabbit = RabbitMQ.from(filename)
    println(rabbit)

    val rabbitJson = rabbit.asJson.toString()
    println(rabbitJson)

    import io.circe.generic.auto._
    import io.circe.parser.decode

    val newRabbit = decode[RabbitMQ.Config](rabbitJson)
    println(newRabbit)

    val send = SendAppConfig(filename)
    println(send)

    val sendJson = send.asJson.toString()
    println(sendJson)

    val newSend = decode[SendAppConfig](sendJson)
    println(newSend)

    val receive = ReceiveAppConfig(filename)
    println(receive)

    val receiveJson = receive.asJson.toString()
    println(receiveJson)

    val newReceive = decode[ReceiveAppConfig](receiveJson)
    println(newReceive)

    val test =
      """
        |{
        |  "connect" : {
        |    "params" : {
        |      "queue" : "qqqqqqqqqqqq",
        |      "host" : "111.222.333.444",
        |      "port" : "5672",
        |      "user" : "uuuuuuuuuu",
        |      "password" : "pppppppppppp"
        |    }
        |  },
        |  "verbose" : 0,
        |  "error" : [
        |  ],
        |  "waitMultiplier" : 1,
        |  "iter" : "IteratorSyncronous",
        |  "numMessages" : -1,
        |  "showHelp" : false
        |}
        |      """.stripMargin

    //val testReceive = decode[ReceiveAppConfig](test)
    val testReceive = decode[SendAppConfig](test)
    println(testReceive)


  }
}
