/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.models

import better.files.File
import pt.inescn.models.Base.AlgorithmType.Classification
import pt.inescn.search.ParamSearch.Param
import pt.inescn.utils.ADWError

/**
  * Created by hmf on 06-06-2017.
  */
object Base {

  //////////////////////////////////////////////////////

  /**
    * Indicates what type of training algorithm is being used. In this
    * way we can match the appropriate metrics to the algorithm during
    * evaluation.
    */
  sealed trait AlgorithmType
  object AlgorithmType {
    // We use an enclosing object to facilitate IDE code completion
    sealed trait Unknown extends AlgorithmType
    sealed trait Classification extends AlgorithmType
    sealed trait BinaryClassification extends Classification
    sealed trait MultiClassification extends Classification
    sealed trait OneClass extends Classification
    sealed trait Regression extends AlgorithmType
  }

  // https://stackoverflow.com/questions/40298181/how-to-reduce-the-unwanted-type-parameter-in-a-generic-method
  trait ML {
    type Algorithm <: AlgorithmType
    type Label
    type Value
    type Prediction
    type Arguments
  }

  /**
    * Indicates what state the training algorithm is in. This allows us to
    * invoke a fitting function only after training has been done.
    */
  sealed trait AlgorithmState
  object AlgorithmState {
    // We use an enclosing object to facilitate IDE code completion
    sealed trait Clean_or_ModelRecorded extends AlgorithmState
    sealed trait ModelRecorded_or_Predicted extends AlgorithmState
    /** No model is available, so no training was done. */
    sealed trait Clean extends Clean_or_ModelRecorded
    /** Trained model is available, so no prediction can be done. */
    sealed trait ModelRecorded extends Clean_or_ModelRecorded with ModelRecorded_or_Predicted
    /** Trained model is available and prediction was already done (prediction can be repeated) */
    sealed trait Predicted extends ModelRecorded_or_Predicted
  }

  trait Prediction[Label] {
    type Labels

    def toArray: Array[Label]
    def toBoolean(trueVal:Label): Seq[Boolean]
  }

  trait Data[Label, Value] {
    // type Label // type of element used for label
    // type Value // type of element kept in the (row, column)
    type Labels
    type Row
    type Rows
    type Index = Array[Int]

    /**
      * Size of the data set.
      * @return - returns a tuple were the first element are the number of rows and the
      *         second the number of columns (including the label)
      */
    def size: (Int, Int)

    /**
      * Get the data row label and features at a given index.
      * @param i - index to the data row
      * @return - tuple with the dependent variable and the features
      */
    def apply(i : Int) : Row

    /**
      * Applies the function `f` to a column `col` using the fold left
      * function. The function takes in a the index of the row, an
      * accumulator value that is initially se to `zero` and the value
      * of the (row, 'col').
      *
      * @param col - column on which to apply the function
      * @param zero - initial accumulator value
      * @param f - function to apply to the column's row values
      * @tparam T - type of accumulator (allows us to compose functions)
      * @return T that has been accumulated by `f`
      */
    def apply[T](col: Int, zero: T, f: (Int, T, Value) => T): T

    /**
      * Applies the function `f` to a column `col` using the fold left
      * function. The function takes in an  accumulator value that is
      * initially se to `zero` and the value of the (row, 'col').
      *
      * @param col - column on which to apply the function
      * @param zero - initial accumulator value
      * @param f - function to apply to the column's row values
      * @tparam T - type of accumulator (allows us to compose functions)
      * @return T that has been accumulated by `f`
      */
    def apply[T](col: Int, zero: T, f: (T, Value) => T): T = {
      apply[T](col, zero, (_: Int, b: T, c: Value) => f(b, c))
    }

    /**
      * Applies the function `f` to a column `col` using the fold left
      * function. The function takes in the value of the (row, 'col')
      * and maps that to another value. Such a mapping can be used
      * for example to scale data per column.
      *
      * NOTE: does in-place changes to data
      *
      * @param col - column on which to apply the function
      * @param f - function to apply to the column's row values
      * @return T that has been accumulated by `f`
      */
    def apply(col: Int, f: Value => Value): Unit

    /**
      * Get data vector at column `i`. Data may be copied.
      * @param i column number
      * @return
      */
    def getColumn(i: Int): Array[Label]
    def getBooleanColumn(i: Int): Array[Boolean]

    /**
      * Return a subset of the data selected by `func`
      *
      * @param func function used to filter the data
      * @return
      */
    def select(func: Row => Boolean): Data[Label, Value]

    /**
      * Return two subsets of the data selected by `func`.
      * Data selected by `func` are placed on the tuple's
      * first element. Data not selected by `func` are placed
      * on the tuple's second element.
      *
      * @param func function used to filter the data
      * @return
      */
    def split(func: Row => Boolean): (Data[Label, Value], Data[Label, Value])

    /**
      * Replace *in place* an element by `substitute` if `sel` os true
      *
      * @param sel predicate that selects elem,ents to be replaced
      * @param substitute replaced the elements (in place)
      */
    def replace(sel: Row => Boolean, substitute: Row => Row) : Unit

    /**
      * Get a sub matrix of the data. The selected `rows` and `cols` (columns) may be
      * copied. The data is also reordered.
      *
      * @param rows - row index
      * @param cols - column index
      * @return
      */
    def getMatrix(rows: Seq[Int], cols: Seq[Int]): Array[Array[Double]]

    /**
      * Returns the `rows`
      *
      * @param rows list of row indexes
      * @return
      */
    def getRows(rows: Seq[Int]): Array[Row]

  }

  trait DataIn[Label,Value] extends Data[Label,Value] {

    /** Number of labels or classes in the data */
    def numLabels[V <: Classification] : Int
    /** Number or records (rows or examples) for a given class or label */
    def classSize[V <: Classification](labelId : Label) : Int
    /**
      * Splits the data into a given number of folds. Used to split the data
      * for cross-validation. Split must be stratified. Note that the stream
      * is lazy, so the data will not be allocated in full for all data-sets.
      * Make sure you do not assign the stream to a val because this will
      * require memoization, which in turn generates and keeps all data in
      * memory.
      *
      * @param nrFold number of cross validation folds
      * @return lazy stream that will produce the train and test data sets
      */
    def splitStratifiedFolds(nrFold : Int) : Either[ADWError, Stream[(DataIn[Label,Value], DataIn[Label,Value])]]
    /**
      * Generates the training and test data set for a one-class
      * classification problem. It is assumed that only two classes exists:
      *   1 - for good signals
      *   0 - for bad (anomalous) signals.
      * Note that the training data set contains *only* good signal. The test
      * data set contains both good and bad (anomalous) signals. Note that
      * stratified sapling is still used.
      *
      * @param nrFold number of cross validation folds
      * @return lazy stream that will produce the train and test data sets
      */
    def splitOneClassFolds(nrFold : Int) : Either[ADWError, Stream[(DataIn[Label,Value], DataIn[Label,Value])]]

    /**
      * This generates cross-fold validations sets for regression. It
      * does not check for classes (assumes only one class exists).
      *
      * @param nrFold number of cross validation folds
      * @return lazy stream that will produce the train and test data sets
      */
    def splitFolds(nrFold: Int): Either[ADWError, Stream[(DataIn[Label,Value], DataIn[Label,Value])]]
    /** Split the data into two sets. The split s indicated by a percentage.
      * The percentage is described by a value range of (0..1). Split must be
      * stratified. Note that the stream is lazy, so the data will not be
      * allocated in full for all data-sets. Make sure you do not assign the
      * stream to a val because this will require memoization, which in turn
      * generates and keeps all data in memory.
      *
      * @param percent value of 0 .. 1 of the training (larger) data set.
      *                Test data will be the rest of the data set.
      * @return lazy stream that will produce the train and test data sets
      */
    def splitStratifiedPercent(percent: Double) : Either[ADWError, (DataIn[Label,Value], DataIn[Label,Value])]

    /**
      * Save data to a file. File will be overwritten.
      * @param f File indicating path and name
      */
    def saveTo(f: File) : Unit
  }

  /**
    * Base class that represent all algorithms. Note that the Mode `Model` is
    * an abstract class. This means that the type is opaque to all users of the
    * any extending Algorithms. This means that the model is a dependent type:
    * one cannot use a model generated by one algorithm (event of the the same
    * type). NOTE: this means that the algorithm is responsible for saving and
    * loading the model.
    *
    * @see https://typelevel.org/blog/2015/07/23/type-projection.html
    */
  trait Algorithm[S <: ML] {
    type Model
    type Args = S#Arguments
    type Parameters <: Param[Args]


    val params: Parameters

    def fit(data: DataIn[S#Label,S#Value], params: Args): Either[ADWError,Model]
    def predict(data: DataIn[S#Label,S#Value], m: Model): Either[ADWError,Prediction[S#Prediction]]
  }


}


//////////////////////////////////////////////////////
/*
  /*
  We use value classes to tag the types of the Op[_,_] class. We make these
  case classes to facilitate their use.

  @see https://workday.github.io/scala/2015/02/05/scala-typesafe-wrappersclass
  @see http://docs.scala-lang.org/overviews/core/value-classes.html
 */

  case class ModelT[+T](val value: T) extends AnyVal
  case class DataT[+T](val value: T) extends AnyVal
  case class QueryT[+T](val value: T) extends AnyVal
  case class LabelT[+T](val value: T) extends AnyVal
  case class ResultT[+T](val value: T) extends AnyVal
  case class MetricT[+T](val value: T) extends AnyVal

  /**
    * This class represents the connection between to operations. The class takes a set of functions
    * as defined by their signature using the value classes and combines them into a new operation.
    * The DSL is defined by a set of implicit classes take two [[Op]]s and convert them to a singel
    * [[Op]]
    *
    * NOTE: to use more specific types we must indicate parameters as covariant, however then +A cannot be used in a function
    * ERROR: Covariant A appears in contravariant position i tpe A => B of method run
    * SOLUTION: use polymorphic typed value classes to hold the correct invariant type.
    *
    * @tparam A
    * @tparam B
    */
  abstract class Op[-A, +B] {
    def run: A => B
  }


  implicit class ML_DSL_1[A,B](op1: Op[DataT[A], ModelT[B]]) {

    def bind[C, D](op2: Op[(QueryT[C], ModelT[B]), (ResultT[D], ModelT[B])])
    : Op[(DataT[A], QueryT[C]), (ResultT[D], ModelT[B])] = new Op[(DataT[A], QueryT[C]), (ResultT[D], ModelT[B])] {
      override def run: ((DataT[A], QueryT[C])) => (ResultT[D], ModelT[B]) =
        (i : (DataT[A], QueryT[C])) => {
          val r1 = op1.run(i._1)
          val r2 = op2.run((i._2,r1))
          r2
        }
    }

    /**
      * Left associative, high precedence
      */
    def *>[C,D](op2: Op[(QueryT[C], ModelT[B]), (ResultT[D], ModelT[B])])
    : Op[(DataT[A], QueryT[C]), (ResultT[D], ModelT[B])] = bind(op2)


    /*
      * Right associative, low precedence
      *
    def |>:(a: A) = map(a)*/
  }

  implicit class ML_DSL_2[A,B,C,D](op1: Op[(DataT[A], QueryT[C]), (ResultT[D], ModelT[B])]) {
    def bind[E, F, G](op2: Op[(LabelT[E], ResultT[D]), MetricT[G]])
    : Op[(DataT[A], QueryT[C], LabelT[E]), MetricT[G]] = new Op[(DataT[A], QueryT[C], LabelT[E]), MetricT[G]] {
      override def run: ((DataT[A], QueryT[C], LabelT[E])) => MetricT[G] =
        (i : (DataT[A], QueryT[C], LabelT[E])) => {
          val dd = i._1
          val qq = i._2
          val ll = i._3
          val r1 = op1.run((dd, qq))
          val r2 = op2.run((ll,r1._1))
          r2
        }
    }

    /**
      * Left associative, high precedence
      */
    def *> [E, F, G](op2: Op[(LabelT[E], ResultT[D]), MetricT[G]])
    : Op[(DataT[A], QueryT[C], LabelT[E]), MetricT[G]] = bind(op2)
  }

  /*
    We use Phantom types to tag Model state. This can be used to compose the model functions correctly.
    For example, we cannot request a predictions before performing a learning action.
   */

  trait ModelState

  trait Clean extends ModelState

  trait Learning extends ModelState

  trait Learned extends ModelState

  trait Predicting extends ModelState

  trait OnLine extends Learning with Predicting


  // TODO: Use it as a base class for all Models.
  trait Model {

    type T = this.type // This model
    type D          // The expected input learning data
    type Q          // The expected input testing data
    type R          // Output of the prediction

    def do_fit(d:D) : T
    def do_prediction(q:Q) : (R,T)


    def fit : Op[DataT[D], ModelT[T]] = new Op[DataT[D], ModelT[T]] {
      override def run: (DataT[D]) => ModelT[T] = (dd : DataT[D]) => {
        val tmp = do_fit(dd.value)
        val model = ModelT( tmp )
        model
      }
    }

    def predict : Op[(QueryT[Q], ModelT[T]), (ResultT[R], ModelT[T])] = new Op[(QueryT[Q], ModelT[T]), (ResultT[R], ModelT[T])] {
      override def run: ((QueryT[Q], ModelT[T])) => (ResultT[R], ModelT[T]) = (i: (QueryT[Q], ModelT[T])) => {
        val model_x: T = i._2.value
        val query_x = i._1.value
        val (r1,r2) = model_x.do_prediction(query_x)
        val rs = ResultT(r1)
        val model = ModelT(r2)
        (rs, model)
      }
    }
  }

}
*/