package pt.inescn.models.svm

import scala.collection.mutable

object Kernels {

  def dot(v1: Array[Float], v2: Array[Float]): Float = {
    var i = 0
    val n = v1.length
    var d = 0.0f
    while (i < n){
      d += v1(i) * v2(i)
      i += 1
    }
    d
  }

  // TODO: add generic bound to Numeric
  def multiplyAdd(c: Float, v1: Array[Float], v2: Array[Float]): Unit = {
    if (c != 0){
      var i = 0
      val n = v1.length
      while (i < n){
        v2(i) += c * v1(i)
        i += 1
      }
    }
  }

  // TODO: add cache for dot product?

  // TODO: https://data-flair.training/blogs/svm-kernel-functions/

  // TODO: Make the kernels functions?
  // TODO: make these implicit here and pass as implicit to Kernel cache constructor?
  trait Kernel[T] {
    // K_ij
    def apply(v1: T, v2:T): Float
  }

  // u'*v
  case object LinearF extends Kernel[Array[Float]] {
    override def apply(v1: Array[Float], v2: Array[Float]): Float = dot(v1, v2)
  }

  // polynomial: (gamma*u'*v + coef0)^degree\n"
  case class PolynomialF(gamma:Float, coef0:Float, degree:Float) extends Kernel[Array[Float]] {
    override def apply(v1: Array[Float], v2: Array[Float]): Float = {
      val base = (gamma * dot(v1, v2)) + coef0
      Math.pow(base, degree).toFloat
    }
  }

  // exp(-|u-v|^2 / two_sigma_squared)
  case class RBFF(two_sigma_squared: Float) extends Kernel[Array[Float]] {
    override def apply(v1: Array[Float], v2: Array[Float]): Float = {
      var s = dot(v1, v2)
      s *= -2
      s += dot(v1, v1) + dot(v2,v2)
      Math.exp(-s / two_sigma_squared).toFloat
    }
  }

  // sigmoid: tanh(gamma*u'*v + coef0)
  case class SigmoidF(gamma: Float, coef0:Float) extends Kernel[Array[Float]] {
    override def apply(v1: Array[Float], v2: Array[Float]): Float = {
      val base = (gamma * dot(v1, v2)) + coef0
      Math.tanh(base).toFloat
    }
  }

  trait KernelCache[T] {
    val K: Kernel[T]
    // K_ii
    def apply(v1: T): Float
    // K_ij
    def apply(v1: T, v2: T): Float
  }

  case class SupportVector[T](alpha: Float, g: Float, A: Float, B:Float, x: T, y: Float)

  class SupportVectors[T] {
    val svs = mutable.DoubleLinkedList[SupportVector[T]]()


  }

  class HashMapKernelCache[T](val data: Array[T], val K: Kernel[T]) {

    // Ensure we have a hashCode and equal
    case class KernelCache1(v1: Int)
    // A tuple is a case class and has hashCode and equal,
    // but this way we can track the memory in 'jvisualvm'
    case class KernelCache2(v1: Int, v2: Int)

    implicit object KernelCache1O extends Ordering[KernelCache1] {
      def compare(a:KernelCache1, b:KernelCache1): Int = a.v1 - b.v1
    }

    implicit object KernelCache2O extends Ordering[KernelCache2] {
      def compare(a:KernelCache2, b:KernelCache2): Int = {
        val c = a.v1 - b.v1
        if (c != 0)
          c
        else
          a.v2 - b.v2
      }
    }


    // The mutable.HashMap consume much memory (about 3x what is required)
    // The mapKij has N^2 support vectors which is about the value in
    // jvisualvm. Should be n(n-1)/2 - don't know why it is about 2x as
    // large. Of this total we have:
    // a little more than 1/3 for KernelCache2 (about
    // 1/3 for scala.collection.mutable.DefaultEntry
    // 1/3 for java.lang.Float
    // So in total it consume about 3x2 = 6 times the required memory
    //val mapKii = new mutable.HashMap[KernelCache1, Float]()
    //val mapKij = new mutable.HashMap[KernelCache2, Float]()
    // Worse that mutable.HashMap. It uses an addition set of scala.Some
    // So we now have 4 sets of very large memory blocks showing up in
    // jvisualvm
    //val mapKii = new mutable.OpenHashMap[KernelCache1, Float]()
    //val mapKij = new mutable.OpenHashMap[KernelCache2, Float]()
    // Same as mutable.OpenHashMap
    val mapKii = new mutable.TreeMap[KernelCache1, Float]()
    val mapKij = new mutable.TreeMap[KernelCache2, Float]()

    // K_ii
    def apply(v1: Int): Float = {
      // Avoid needless construction of key
      val key = KernelCache1(v1)
      if (!mapKii.contains(key)) {
        // Avoid needless inner product calculation
        val k = K(data(v1), data(v1))
        mapKii.update(key, k)
        k
      } else
        // memoization
        mapKii(key)
    }

    // K_ij
    // This does not have a memory leak because Scala tuples are case classes (hash and equals generated)
    //def apply(v1: Int, v2: Int): Float = mapKij.getOrElseUpdate((v1, v2), K(data(v1), data(v2)))
    def apply(v1: Int, v2: Int): Float = {
      // The Q matrix is semi-definite positive so of the N^2 kernels we
      // need only keep N(N-1)^2/2. Avoid needless construction of key.
      val key = if (v1 <= v2) KernelCache2(v1, v2) else KernelCache2(v2, v1)
      if (!mapKij.contains(key)){
        // Avoid needless inner product calculation
        val k = K(data(v1), data(v2))
        mapKij.update(key, k)
        k
      }
      else
        // memoization
        mapKij(key)
    }

    def remove(v1: Int): Unit = mapKii.remove(KernelCache1(v1))
    def remove(v1: Int, v2: Int): Unit = mapKij.remove(KernelCache2(v1,v2))
    def purgeLeastUsed(): Unit = ???
  }

  /*
  class KernelCache[T](val data: Array[T], val K: Kernel[T]) {
    val mapKii = new mutable.HashMap[Int, Float]()
    val mapKij = new mutable.HashMap[(Int, Int), Float]()

    // K_ii
    // This has a memory leak - Int has no hash value
    //def apply(v1: Int): Float = mapKii.getOrElseUpdate(v1, K(data(v1), data(v1)))
    //def apply(v1: Int): Float = if (!mapKii.contains(v1)) mapKii.getOrElseUpdate(v1, K(data(v1), data(v1))) else mapKii(v1)
    def apply(v1: Int): Float =
      if (!mapKii.contains(v1)) {
        val k = K(data(v1), data(v1))
        mapKii.update(v1, k)
        k
      } else
        mapKii(v1)
    // K_ij
    // This does not have a memory leak because Scala tuples are case classes (hash and equals generated)
    //def apply(v1: Int, v2: Int): Float = mapKij.getOrElseUpdate((v1, v2), K(data(v1), data(v2)))
    /*
    def apply(v1: Int, v2: Int): Float =
      if (v1 <= v2 )
        mapKij.getOrElseUpdate((v1, v2), K(data(v1), data(v2)))
      else
        mapKij.getOrElseUpdate((v2, v1), K(data(v2), data(v1)))
    */
    def apply(v1: Int, v2: Int): Float = {
      val key = if (v1 <= v2) (v1, v2) else (v2, v1)
      if (!mapKij.contains(key)){
        val k = K(data(v1), data(v2))
        mapKij.update(key, k)
        k
      }
      else
        mapKij(key)
    }

    def remove(v1: Int): Unit = mapKii.remove(v1)
    def remove(v1: Int, v2: Int): Unit = mapKij.remove((v1,v2))
    def purgeLeastUsed(): Unit = ???
  }

   */

}
