package pt.inescn.models.svm

import better.files.File

/**
  * root/runMain pt.inescn.models.svm.Load
  * Full stack trace:
  *   testOnly pt.inescn.model.LASVMSpec -- -oF
  */
object Load {

  /**
    * Reads the LightSVM file format. One should set `is_libsvm_format`
    * format to true - assumes the sparse indexed row is split by a `:`
    * character.
    *
    * TODO: add is_binary: Boolean and read binary data.
    *
    * @param file file that contains the data set
    * @param numColumns number of expected columns in the data set
    * @param is_libsvm_format indicates the row index and data are separated by a `:`
    *
    * @return returns tow arrays - the target label and the data set as a dense matrix.
    *         Also returns the number of elements in the matrix
    */
  def readLightSVM(file: File, numColumns: Int, is_libsvm_format: Boolean = true, offset: Int = 1): (Array[Int], Array[Array[Float]], Int) = {
    //readLightSVMData(is_sparse_data, is_libsvm_format, is_binary, id)
    readLightSVMData(file, numColumns, is_libsvm_format, offset, true)
  }

  /**
    * Reads the LASVM file format. One should set `is_libsvm_format`
    * format to true - assumes the sparse indexed row is split by a `:`
    * character.
    * The LASVM data set can be split into a data header and a data set
    * indexer. The data set is read in as a normal LightSVM data file.
    * The file name with data set is found in the data header file.
    * The data header file indicates the data set file name and if the
    * data set file is in binary or text format.
    *
    * The header may also contain the indexes of a subset of the data set
    * to load. This allows us to prepare several experiments that can be
    * replicated by others without the need to duplicate data.
    *
    * The header can also contain the relabelling of data. This allows one
    * to use the binary classification SVM algorithm by using the
    * one-versus-all classification strategy.
    *
    * Note that relabelling is only possible when reindexing is done. In
    * this case if we want to relabel all existing data then the indexes
    * of the subset of data will in fact have indexed all of the data.
    *
    * TODO: add is_binary: Boolean and read binary data.
    *
    * @param file file that contains the data set
    * @param numColumns number of expected columns in the data set
    * @param is_libsvm_format indicates the row index and data are separated
    *                         by a `:`
    *
    * @return returns tow arrays - the target label and the data set as a
    *         dense matrix. Also returns the number of elements in the
    *         matrix.
    */
  def readLASVM(file: File, numColumns: Int, is_libsvm_format: Boolean = true, offset: Int = 1, checkRelabel: Boolean = true): (Array[Int], Array[Array[Float]], Int) = {
    readLASVMData(file, numColumns, is_libsvm_format, offset, checkRelabel)
  }


  /**
    * The `line` has a set of pair values separated by a `:` character.
    * Here we check that this data has the `keyName`. If so it returns the
    * value otherwise No value is returned.
    *
    * @param line string with key-value pair
    * @param keyName name of the key that we expect
    * @return A value if it exists otherwise no value is returned.
    */
  private def keyValuePair(line: Iterator[String], keyName: String): Option[String] = {
    if (line.hasNext) {
      val pair = line.next().split(":").map(_.trim)
      if ((pair.length == 2) && (pair.head == keyName))
        Some(pair(1))
      else
        None
    } else
      None
  }

  /**
    * Reads the LASSVM experimental data.
    *
    * Example header:
    *  file_name: banana.all.txt
    *  binary_file: 0
    *  supply_indices: 1
    *  supply_new_labels: 0
    *
    * @param file
    * @param numColumns
    * @param is_libsvm_format
    * @return
    */
  def readLASVMData(file: File, numColumns: Int, is_libsvm_format: Boolean, offset: Int, checkRelabel: Boolean): (Array[Int], Array[Array[Float]], Int) = {
    val header = file.lineIterator
    val headerData = for {
      fileName <- keyValuePair(header, "file_name")
      binaryFile <- keyValuePair(header, "binary_file")
      supplyIndices <- keyValuePair(header, "supply_indices")
      supplyNewLabels <- keyValuePair(header, "supply_new_labels")
    } yield (fileName, binaryFile, supplyIndices, supplyNewLabels)
    headerData match {
      case None =>
        readLightSVMData(file, numColumns, is_libsvm_format, offset, checkRelabel)

      case Some((fileName, binaryFile, supplyIndices, supplyNewLabels)) =>
        // Maybe in the next version
        if (binaryFile != "0") throw new RuntimeException("Binary file types not supported.\nConvert data to text format first.")

        // Read the data from a separate file
        val allDataFile = File(file.path.getParent) / fileName
        val (tempTarget, tempData, tempN) = readLightSVMData(allDataFile, numColumns, is_libsvm_format, offset, checkRelabel)

        // Do we need to obtain subset of the data?
        val (target, data, n) = if (supplyIndices != "0") {
          if (supplyNewLabels != "0"){
            // read indexed subset of data index and new label
            val indices = header.map(_.split("\\s+").map(_.toInt)).toList
            // collect indexed data only using data index
            val data = indices.map(i => tempData(i(0) - offset)).toArray
            // Get the new labels
            val target = indices.map{i =>
              val y = i(1)
              if ((y != +1) && (y != -1)) throw new RuntimeException(s"Target has unexpected relabelling value: $y")
              y
            }.toArray
            val n = data.length
            (target, data, n)
          } else {
            // read indexed subset of data
            val indices = header.map(_.toInt).toList
            // collect indexed data only
            val data = indices.map(i => tempData(i-offset)).toArray
            // get existing labels
            val target = indices.map(i => tempTarget(i-offset)).toArray
            val n = data.length
            (target, data, n)
          }
        } else
          (tempTarget, tempData, tempN)

        (target, data, n)
    }
  }

  //private def readLightSVMData(is_sparse_data: Boolean, is_libsvm_format: Boolean, is_binary: Boolean, id: Iterator[String]): Int = {
  private def readLightSVMData(file: File, numColumns:Int, is_libsvm_format: Boolean, offset: Int, checkRelabel: Boolean): (Array[Int], Array[Array[Float]], Int) = {

    val id = file.lineIterator

    val target0 = List[Int]()
    val data0 = List[Array[Float]]()
    val nlines0 = 0
    val tmp = id.foldLeft((target0,data0,nlines0)) { case ((target,data,nlines), s) =>
      val nnlines = nlines + 1
      val lineTmp = if (is_libsvm_format) s.split("\\s+|[:]") else s.split("\\s+")
      val line = if (is_libsvm_format) {
        val h = lineTmp.head
        val yl = lineTmp.drop(1)
        yl :+ h
      } else
        lineTmp

      // Row
      //println(line.mkString(","))
      val v = line.map(_.toFloat)
      // Label
      val y = v.last.toInt
      val yy = if (checkRelabel) {
        if ((y == 0) || (y == -1) ) -1
        else if (y == 1) +1
        else throw new RuntimeException(s"Target has unexpected value: $y")
      } else
        y
      val ntarget = yy :: target

      // remove label
      val n = v.length - 1
      val datat = v
        .slice(0,n)             // remove label (last element)
        .sliding(2,2)           // (index, value)

      val vec = Array.fill(numColumns)(0.0f)
      datat.foreach{ sparse =>
        val i = sparse(0).toInt
        if ( i > numColumns)
          throw new RuntimeException(s"Row with incorrect number of columns ($i < $numColumns): ${line.mkString(",")}")
        vec(i-offset) = sparse(1)
      }

      (ntarget, vec::data, nnlines)
    }
    (tmp._1.toArray.reverse, tmp._2.toArray.reverse, tmp._3)
  }

  /*
     TODO: allow relabelling of data: one-versus-all
     TODO: allow selection of data for multi-class classification: one-versus-one

   */


  /*
      Scaling
   */

  private def scaleValue(lower: Float, upper: Float)(min: Float, max: Float, value: Float) : Float = {
    /* skip single-valued attribute */
    if(min == max)
      value
    else {

      if(value == min)
        lower
      else if(value == max)
        upper
      else
        lower + (((upper-lower) * (value-min)) / (max-min))
    }

  }

  def scale(lower: Float, upper: Float, maxs: Array[Float], mins: Array[Float])(d: Int, data: Array[Array[Float]]): Unit = {
    var k = 0
    val f = scaleValue(lower, upper) _
    while (k < data.length){
      var i = 0
      while (i < d) {
        data(k)(i) = f( mins(i), maxs(i), data(k)(i))
        i += 1
      }
      k += 1
    }
  }

  def minMax(d: Int, data: Array[Array[Float]]): (Array[Float], Array[Float]) = {
    val maxs = Array.fill(d)(Float.MinValue)
    val mins = Array.fill(d)(Float.MaxValue)
    var k = 0
    while (k < data.length){
      var i = 0
      while (i < d) {
        maxs(i) = Math.max( maxs(i), data(k)(i))
        mins(i) = Math.min( mins(i), data(k)(i))
        i += 1
      }
      k += 1
    }
    (mins, maxs)
  }


  def main(args: Array[String]): Unit = {
    import better.files.Dsl._
    val trainData = cwd / ".." / "data/libsvm/svmguide1"
    //val testData = cwd / ".." / "data/libsvm/svmguide1.t"
    //val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    println(trainData)

    //read(trainData, is_sparse_data = false, is_libsvm_format = true, is_binary = false)
    val d = 4
    val (target, data, n) = readLightSVM(trainData, d, is_libsvm_format = true)
    println(s"n = $n")
    println(s"target = ${target.mkString(",")}")
    //println(s"data= ${data.mkString(",")}")
    assert(n == 3089)
    assert(target(0) == 1)
    assert(target(2000-1) == 1)
    assert(target(2001-1) == -1)
    assert(target(3089-1) == -1)
    //println(s"data(0) = ${data(0).mkString(",")}")
    assert(data(0) sameElements Array(2.617300e+01f, 5.886700e+01f, -1.894697e-01f, 1.251225e+02f))

    val (mins, maxs) = minMax(d, data)
    println(s"mins = ${mins.mkString(",")}")
    println(s"maxs = ${maxs.mkString(",")}")
    assert(mins sameElements Array( 0.0f, -4.555206f, -0.7524385f, 8.157474f))
    assert(maxs sameElements Array( 297.05f, 581.0731f, 0.7170606f, 180.0f))

    val scaleFunc = scale(-1f, +1f, mins, maxs) _
    scaleFunc(d, data)
    val (smins, smaxs) = minMax(d, data)
    println(s"smins = ${smins.mkString(",")}")
    println(s"smaxs = ${smaxs.mkString(",")}")
    assert(smins sameElements Array( -1f, -1f, -1f, -1f))
    assert(smaxs sameElements Array( +1f, +1f, +1f, +1f))


  }

}
