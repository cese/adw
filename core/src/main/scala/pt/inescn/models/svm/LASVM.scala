package pt.inescn.models.svm

import scala.util.Random
import pt.inescn.models.svm.Kernels._

import scala.collection.mutable



/**
  *
  * https://towardsdatascience.com/a-practical-guide-to-interpreting-and-visualising-support-vector-machines-97d2a5b0564e
  */
object LASVM {


  def pickRandomSample(nTotalSamples: Int): Int = Random.nextInt(nTotalSamples)

  // TODO: get faster method fisher-yates?)
  // https://algs4.cs.princeton.edu/11model/Knuth.java.html
  def shuffle(n: Int): Array[Int] = Random.shuffle((0 until n).toList).toArray

  trait Algo[T] {

    // DEBUG
    var numOverruns:Int = 0

    def learn(): Unit
    def fit(): Unit
    def numSupportVector(): (Int, Int, Int)
    def predictRaw(dk: T): Float
    def predict(dk: T): Int
    def predictions(dk: Array[T]): Array[Int]
    def learnErrorRate(): (Int, Int, Float)
    def errorRate(yIn: Array[Float], dataIn: Array[T]): (Int, Int, Float)
    def errorIdxs(preds: Array[Int]): IndexedSeq[Int]
    def learnErrorIdxs(): IndexedSeq[Int]
  }

  /**
    * Stochastic online SVM algorithm for 2 class classification. Alg1 follows
    * the pseudo code of the article. When using the simple select pair based
    * on the maximum violating pair, we find that sometime the reprocess
    * carries on for many steps because the same pair is always selected. The
    * problem is that the step "lambda" is very small (order of 1e-7) the the
    * alpha(i) do not change much. The process eventually ends when one of
    * the alpha(i) reach 0 or +/- 1.
    *
    * We also find the when using a linear kernel the offset "b" does not
    * seem to fall on center of a maximal margin. Alg1 uses the simplest
    * on-line selection method: Random. Other methods include: gradient,
    * active and auto-active.
    *
    *
    * Features:
    * Active example selection (early stopping)
    * Shrinking
    * Kernel caching strategy
    *
    * Experiments:
    * . Modulate calls to reprocess (heuristics)
    * . Adaptive sampling for randomized search (AUTOACTIVE in [1])
    *
    * 1. Training Invariant Support Vector Machines using Selective Sampling
    *    Gaëlle Loosli, Stéphane Canu, Léon Bottou
    * 2. Support Vector Machine Solvers
    *    Léon Bottou, Chih-Jen Lin
    * 3. Making Large-Scale SVM Learning Practical
    *    Thorsten Joachims
    *
    * TODO: Support Vector Machine Solvers
    * Off-line SMO
    * 1. Add 2nd order working set selection (section 7.2.3)
    * 2. Add shrinking on unbound for alpha = C (section 7.3)
    * 3. Add support vector cache (section 7.4)
    * 4. Add check for eta = 0, select min alph_i's at extremes
    *
    * TODO: LASVM improvements
    * LASVM
    * 1. Stop criterion on max number of iterations (batch)
    * 2. Stop when error does not change (below threshold)
    * 3. Stop when delta alpha_i is too small
    * 4. Add other selection modes to LASVM?
    *
    * [1] Christopher J. C. Burges. A Tutorial on Support Vector Machines for Pattern Recognition. Data Mining and Knowledge Discovery 2:121-167, 1998.
    * [2] John Platt. Sequential Minimal Optimization: A Fast Algorithm for Training Support Vector Machines.
    * [3] Rong-En Fan, Pai-Hsuen, and Chih-Jen Lin. Working Set Selection Using Second Order Information for Training Support Vector Machines. JMLR, 6:1889-1918, 2005.
    * [4] Antoine Bordes, Seyda Ertekin, Jason Weston and Leon Bottou. Fast Kernel Classifiers with Online and Active Learning, Journal of Machine Learning Research, 6:1579-1619, 2005.
    * [5] Tobias Glasmachers and Christian Igel. Second Order SMO Improves SVM Online and Active Learning.
    * [6] Chih-Chung Chang and Chih-Jen Lin. LIBSVM: a Library for Support Vector Machines.
    *
    *
    * [1] B. Scholkopf, J. C. Platt, J. Shawe-Taylor, A. J. Smola, and R. C. Williamson. Estimating the support of a high-dimensional distribution. Neural Computation, 13(7):1443{1471, 2001.
    * [2] Yunqiang Chen, Xiang Zhou, and Thomas S. Huang, One-Class SVM For Learning in Image Retrieval, Proceedings of ICIP, 2001
    * [3] R. Vert, J.-P. Vert, Consistency and Convergence Rates of One-Class SVMs and Related Algorithms, JMLR 7, 817-854, 2006.
    * [4] The Minimum Volume Covering Ellipsoid Estimation in Kernel-Defined Feature Spaces by A. N. Dolia, T. D. Bie, C. J. Harris, J. Shawe-Taylor, and D.M. Titterington,
    * [5] Support Vector Method For Novelty Detection by Schölkopf et al.
    * [6] Support Vector Data Description by Tax and Duin (SVDD)
    * [7] One Class Classification for Anomaly Detection: Support Vector Data Description Revisited, Eric J. PauwelsOnkar Ambekar
    * [8] Simple Incremental One-Class Support Vector Classification, Kai LabuschFabian TimmThomas Martinetz
    * [9] One-Class SVM with Privileged Information and itsApplication to Malware Detection, Evgeny Burnaev and Dmitry Smolyakov
    * [10] Subspace Support Vector Data Description, Fahad Sohrab, Jenni Raitoharju, Moncef Gabbouj and Alexandros Iosifidis
    * [11] Testing Stationarity with Surrogates - A One-ClassSVM ApproachJun Xiao, Pierre Borgnat, Patrick Flandrin, Cédric Richard
    * [12] Fast Incremental SVDD Learning Algorithm with the Gaussian Kernel, Hansi Jiang, Haoyu Wang
    * [13] Calibration of One-Class SVM for MV set estimation, Albert Thomas∗†, Vincent Feuillard∗and Alexandre Gramfor
    * [14] Support vector data descriptions andk-means clustering: one class?Nico G ̈ornitz1, Luiz Alberto Lima, Klaus-Robert M ̈uller1, Marius Kloft, and Shinichi Nakajima
    * [15] Fast Incremental Learning for One-class Support Vector Classifier using Sample Margin Information, Pyo Jae Kim, Hyung Jin Chang, Jin Young Choi
    * [16] Incremental One-Class Learning with Bounded Computational Complexity, Rowland R. Sollito, Robert B. Fisher
    * [17] Support Vector Method for Novelty Detection, Bernhard Scholkopf, Robert Williamson, Alex Smola§, John Shawe-Taylort, John Platt*
    * [18] Estimating the Support of a High-Dimensional Distribution, Bernhard Sch ̈olkopf, John C. Platt, John Shawe-Taylory, Alex J. Smolax, Robert C. Williamsonx
    *
    * A New Multi-class SVM Algorithm Based on One-Class SVM, Xiao-Yuan YangJia LiuMin-Qing ZhangKe Niu
    *
    * TODO: 18, 12, 15, 8, 9, 10, 14 (primal SGD for SVDD)
    *
    * http://haifengl.github.io/smile/
    * https://github.com/haifengl/smile
    *   https://github.com/haifengl/smile/blob/1d55b75f77c649d1de595d338e95e3eb1396b74a/core/src/main/java/smile/classification/SVM.java
    *
    * https://leon.bottou.org/projects/lasvm
    *   https://leon.bottou.org/_media/projects/lasvm-source-1.1.tar.gz
    *
    * TODO: on-line version cannot get data and use N
    * TODO: cache the search for g-maximum/g-minimum so we do not repeat argMax and argMin?
    * TODO: cache A, B without saving C*y(i)
    * TODO: cache support vectors << N using faster than Map (circular queue ?)
    *
    */
  class Algo1[T](val K: HashMapKernelCache[T],
                 val data: Array[T],
                 val y: Array[Float], // TODO: make Int ??
                 val C: Float,
                 val pickSample: Int => Int,
                 val epsilon: Float = 0.001f,
                 //val cacheSize: Int,
                 val initSampleSize: Int,
                 val onlineEpochs: Int = 1,
                 val maxFinishIter:Int = Int.MaxValue,
                 val debug: Boolean = false) extends Algo[T] {

    val EMPTY: Int = -1
    val FLT_EPSILON = 1e-6f

    val N: Int = data.length
    // TODO: use ArrayBuffer or Maps (slow)
    // TODO: figure out how to indirectly index samples
    val alpha: Array[Float] = Array.fill(N)(0.0f)
    // gradients
    val g: Array[Float]= Array.fill(N)(0.0f)
    // Support vectors (index to samples)
    val S: Array[Int] = Array.fill(N)(EMPTY)
    val SSize: Int = 0

    // TODO: add to parameter ?
    //var TAU = 1E-12
    var TAU = 1E-7F

    var b: Float = 0.0f
    var delta: Float = 0.0f


    def dprintln[A](s:A):Unit = if (debug) println(s)

    def numSupportVector(): (Int, Int, Int) = {
      var i = 0
      var non_bound_support = 0
      var bound_support = 0
      var incorrect_support = 0
      while (i < N) {
        //if (Math.abs(alpha(i)) > 0.0) println(s"alpha($i) = ${alpha(i)} : A(i) = ${A(i)}, B(i) = ${B(i)}")
        //if (Math.abs(alpha(i)) > 0.0) println(s"alpha($i) = ${alpha(i) == A(i)}, B(i) = ${alpha(i) == B(i)}")
        if (alpha(i) != 0) {
          if (alpha(i) == A(i) || alpha(i) == B(i)){
            bound_support += 1
          }
          else if (alpha(i) > A(i) && alpha(i) < B(i)) {
            non_bound_support += 1
          }
          else {
            incorrect_support += 1
          }
        }
        i += 1
      }
      (non_bound_support, bound_support, incorrect_support)
    }

    def dprintDiagnostics(iters: Int): Unit = {
      println(s"Number iteration = $iters")
      var s = 0.0F
      var i = 0
      while (i < N) {
        if (S(i) != EMPTY) s += alpha(i)*y(i)
        i += 1
      }
      println(s"SUM( alpha(i)*y(i) ) = $s")
      var t = 0.0F
      i = 0
      while (i < N) {
        var j = 0
        if (S(i) != EMPTY){
          while (j < N) {
            //if (debug) println(s"K($i, $j) = ${K(i, j)}")
            if (S(i) != EMPTY) t += alpha(i) * alpha(j) * K(i, j)
            j += 1
          }
        }
        i += 1
      }
      println(s"SUM( alpha(i)*alpha(j)*K(i,j) ) = $t")
      println(s"Objective function = ${s - t / 2.0}")
      // Lagrange subject to:  Sum (alpha_i) = 0
      s = 0.0F
      i = 0
      while (i < N) {
        if (S(i) != EMPTY) s += alpha(i)
        i += 1
      }
      println(s"SUM(alpha(i)) = $s == 0")
      // Lagrange subject to:  all Ai <= alpha_i <= Bi
      // Lagrange subject to:  alpha_i < Ai
      i = 0
      while (i < N) {
        if ((S(i) != EMPTY) && (alpha(i) < A(i)))
          println(s"alpha($i) = ${alpha(i)} < Ai  ${y(i)}")
        i += 1
      }
      // Lagrange subject to:  alpha_i > Bi
      i = 0
      while (i < N) {
        if ((S(i) != EMPTY) && (alpha(i) > B(i)))
          println(s"alpha($i) = ${alpha(i)} > Bi")
        i += 1
      }
      i = 0
      val (non_bound_support, bound_support, incorrect_support) = numSupportVector()
      print(s"non_bound = $non_bound_support  ")
      print(s"bound_support = $bound_support  ")
      print(s"incorrect_support = $incorrect_support  ")
      println(s"sparsity = ${(bound_support + non_bound_support)*1.0 / N} %")
      println(s"C = $C")
      println(s"kernel = ${K.K}")
      println(s"b = $b")
    }

    // TODO: pre-calculate and cache ?
    def A(i:Int): Float = Math.min(0, C*y(i))
    def B(i:Int): Float = Math.max(0, C*y(i))

    // TODO: automatically update g-max/g-min instead of scanning
    def argMin(): Int = {
      var i = 0
      var acc = Float.MaxValue
      var k = EMPTY
      while (i < N){
        val s = S(i)
        if ( s != EMPTY && alpha(s) > A(s)) {
          // s in S
          if (g(s) < acc) {
            acc = g(s)
            k = s
          }
        }
        i += 1
      }
      k
    }

    // TODO: automatically update g-max/g-min instead of scanning
    def argMax(): Int = {
      var i = 0
      var acc = Float.MinValue
      var k = EMPTY
      while (i < N){
        val s = S(i)
        if ( s != EMPTY && alpha(s) < B(s)) {
          // s in S
          if (g(s) > acc) {
            acc = g(s)
            k = s
          }
        }
        i += 1
      }
      k
    }

    // TODO: make parameter
    // TODO: we have additional versions of selectPair to test
    // TODO: SMILE https://github.com/haifengl/smile/blob/1d55b75f77c649d1de595d338e95e3eb1396b74a/core/src/main/java/smile/classification/SVM.java#L690
    //  [573] maximizes the curvature (gradient) See of this was optimal in LASVM experiments
    def selectPair(k: Int): (Int, Int) = {
      if (k != EMPTY){
        if (y(k) == 1){
          val min = argMin()
          dprintln(s"selectPair min ($k, $min)")
          (k, min )
        } else if (y(k) == -1) {
          val max = argMax()
          dprintln(s"selectPair max ($max, $k)")
          (max, k )
        } else
          throw new RuntimeException(s"Unexpected y($k) = ${y(k)} value")
      } else {
        val min = argMin()
        val max = argMax()
        dprintln(s"selectPair min/max ($max, $min)")
        (max, min)
      }
    }

    // TODO: is this an and or and or?
    // Note that argMax and/or argMin may not find candidates so no violation can occur
    def tau_ViolatingPair(i:Int, j:Int): Boolean = {
      // Check not in the pseudo-code
      if (i == EMPTY || j == EMPTY)
        false
      else {
        //println(s"--------------------- g($i) - g($j) = ${g(i) - g(j)}")
        (alpha(i) < B(i)) &&
        (alpha(j) > A(j)) &&
        (g(i) - g(j) > TAU)
      }
    }

    // g_k
    def gradient(k:Int): Float = {
      var i = 0
      var sum = 0.0f
      while (i < N){
        val s = S(i)
        // TODO: remove
        if (s != EMPTY){
          if (s != i) throw new RuntimeException(s"gradient: s($s) != i($i)")
          sum += alpha(s) * K(k, s)
        }
        i += 1
      }
      y(k) - sum
    }

    // TODO: update argMax and argMin here
    def updateGradients(lambda: Float, i:Int, j:Int): Unit = {
      var k = 0
      while (k < N){
        val s = S(k)
        if (s != EMPTY){
          // TODO: remove
          if (s != k) throw new RuntimeException(s"updateGradients: s($s) != i($k)")
          g(s) = g(s) - (lambda * (K(i,s) - K(j,s)) )
        }
        k += 1
      }
    }

    // See: https://github.com/haifengl/smile/blob/1d55b75f77c649d1de595d338e95e3eb1396b74a/core/src/main/java/smile/classification/SVM.java#L690
    def step(i: Int, j: Int) : Unit = {
      val eta = K(i) + K(j) - (2 * K(i, j))
      if (eta == 0)
        // We have a duplicated sample
        return
      if (eta < 0.0) {
        // should not happen
        throw new RuntimeException(s"step: eta = $eta is < 0 between i=$i and j=$j")
      }
      val ostep = Math.min( B(i) - alpha(i), alpha(j) - A(j))
      // Not in the pseudo-code but in the original code
      val lambda = Math.min( (g(i) - g(j)) / eta,  ostep)
      dprintln(s"step with lambda = $lambda: alpha($i) = ${alpha(i)}, alpha($j) = ${alpha(j)}, ostep = $ostep")
      if (lambda <= 0) throw new RuntimeException(s"lambda = $lambda")
      alpha(i) = alpha(i) + lambda
      alpha(j) = alpha(j) - lambda
      dprintln(s"step after lambda= $lambda: alpha($i) = ${alpha(i)}, alpha($j) = ${alpha(j)}")
      //System.out.flush()

      updateGradients(lambda, i, j)
    }

    def process(k:Int): Unit = {
      // 1. Bail out if k in S
      if (S(k) != EMPTY) {
        dprintln(s"process bail-out @ $k in S")
        return
      }

      // 2. Add new support vector (default bound)
      alpha(k) = 0
      g(k) = gradient(k) // TODO = 1, always constant?
      S(k) = k
      dprintln(s"Adding support vector $k")

      // 3. Select possible violating pair
      // TODO: make selection a parameter
      val (max, min) = selectPair(k)
      if (max == min) throw new RuntimeException(s"max($max) == min($min)")

      // 4. Bail out if not tau-violating pair
      if (!tau_ViolatingPair(max, min)) {
        dprintln(s"process bail-out @ ($max, $min) not tau-violating pair")
        return
      }

      /*
      // This check is not in the pseudo code
      // Is in original LASVM and SMILE
      if (g(min) < g(max)) {
        if ((y(k) > 0 && g(k) < g(min)) || (y(k) < 0 && g(k) > g(max))) {
          return
        }
      }
       */

      // 5. Update support vectors
      step(max, min)
    }

    def purgeCache(r: Int): Int = {
      var n = 1
      K.remove(r)
      var k = 0
      while (k < N){
        val s = S(k)
        if (s != EMPTY) {
          K.remove(r, s)
          n += 1
        }
        k += 1
      }
      n
    }

    def shrink(): (Int, Int) = {
      val i = argMax()
      val j = argMin()
      // Checks for empty not in pseudo-code
      val gi = if (i != EMPTY) g(i) else Float.MaxValue
      val gj = if (j != EMPTY) g(j) else Float.MinValue
      var k = 0
      var n = 0
      while (k < N){
        val s = S(k)
        if (s != EMPTY) n += 1
        //if (s != EMPTY && Math.abs(alpha(s)) <= epsilon ){
        // TODO: make search alpha close to 0?
        if (s != EMPTY && alpha(s) == 0 ){
          if (y(s) == -1 && g(s) >= gi) S(s) = EMPTY // remove
          else if (y(s) == +1 && g(s) <= gj) S(s) = EMPTY // remove
          val del = if (S(s) == EMPTY) purgeCache(s) else 0
          // TODO:remove
          if (S(s) == EMPTY) dprintln(s"shrink: removed $del support vector of sample $s")
        }
        k += 1
      }
      dprintln(s"****************************** shrink: number of SV's = $n")
      (i,j)
    }

    def reProcess(): (Float, Int, Int) = {
      // 1. Select possible violating pair
      // TODO: make selection a parameter
      val (i, j) = selectPair(EMPTY)
      // Unbound may mean i == j but this means that it is not a violating pair, so bails out
      //if (i == j) throw new RuntimeException(s"i($i) == j($j) : ${alpha(i)} < ${B(i)} ${alpha(j)} > ${A(i)}")

      // 4. Bail out if not tau-violating pair
      if (!tau_ViolatingPair(i, j)) {
        // Not in pseudo-code: return 0 to terminate finish
        // Note that even if non-violating, we may end up repeatedly selecting
        // the same pair. We therefore end-up iterating until maxIter
        dprintln(s"reProcess bail-out @ ($i, $j) not tau-violating pair (i=$i, j=$j, delta=$delta)")
        //return Float.MaxValue
        return (0.0f, i, j)
      }

      // 3. Update support vectors
      // TODO: some times the violating pair has a very small step and
      //  they keep on getting selected until maxIters. Shrink does nothing
      step(i, j)

      // 4. Shrink - remove blatant non support vectors
      val (maxf, minf) = shrink()

      // 5. update offset (bias) and gradient of most violating pair
      // TODO: original uses i, j
      if (maxf != EMPTY && minf != EMPTY){
        b = (g(maxf) + g(minf)) / 2
        delta = g(maxf) - g(minf)
        (delta, maxf, minf)
      } else {
        // TODO: Should this be 0?
        delta = 0.0f
        (delta, maxf, minf)
      }
    }


    // TODO: we need a true online version - scan only the new sample
    def online(numIters: Int): Unit = {
      var iters = 0
      // RANDOM selection (simplest)
      // TODO: get faster method fisher-yates?)
      // https://algs4.cs.princeton.edu/11model/Knuth.java.html
      val samples = Random.shuffle((0 until N).toList).toArray
      dprintln(s"Shuffled ${samples.length} samples.")
      while (iters < onlineEpochs){
        var i = 0
        while (i < N){
          val k = samples(i) // pickSample(N)
          dprintln(s"online @ $iters - picked sample ------------------------------------ $k")
          process(k)
          reProcess()
          i += 1
        }
        iters += 1
      }
    }

    def finish(): Unit = {
      numOverruns = 0
      var iters = 0
      var delta = Float.MaxValue
      while (delta > TAU && iters < maxFinishIter){
        dprintln(s"finish @ $iters")
        val (deltat, max, min) = reProcess()
        delta = deltat
        //dprintDiagnostics(iters)
        dprintln(s"finish: delta = $delta <= $TAU")
        iters += 1
      }
      //if (iters == maxFinishIter) println(s"delta = $delta @ $iters")
      numOverruns += iters
      if (iters == maxFinishIter) println(s"Stopped: delta <= TAU = $delta <= $TAU ; iters < maxFinishIter = $iters < $maxFinishIter ")
    }

    def initialGradients(): Unit = {
      var i = 0
      while (i < N){
        val s = S(i)
        if (s != EMPTY){
          // TODO: remove
          if (s != i) throw new RuntimeException(s"initialGradients: s($s) != i($i)")
          g(s) = gradient(s)
          dprintln(s"initialGradients g($s) = ${g(s)}")
        }
        i += 1
      }
    }

    // Because all alphas = 0 this implies from equation (9) that gk = yk
    // TODO: substitute gradient(i) with y(i) ? Maybe we can initialize alpha randomly?
    def seedSamples(nSamples: Int): Unit = {
      // TODO: get faster method fisher-yates?)
      // https://algs4.cs.princeton.edu/11model/Knuth.java.html
      val samples = Random.shuffle((0 until N).toList)
      samples.take(initSampleSize).foreach{
        i =>
          S(i) = i
          dprintln(s"seeSample: S($i) = ${S(i)}")
          //g(i) = gradient(i)
          //println(s"init: g($i) = ${g(i)}")
      }
    }

    def learn(): Unit = {

      // Initialization
      seedSamples(initSampleSize)
      initialGradients()
      if (debug) dprintDiagnostics(0)

      // Online iterations
      online(onlineEpochs)
      if (debug) dprintDiagnostics(0)

      // Finishing
      finish() //TODO: BUG - always selecting the same max/min Pair and bailing out

      //S.foreach( si => if (si != EMPTY) println(s"alpha(${si}) = ${alpha(si)}"))
    }

    def fit(): Unit = learn()

    def predictRaw(dk: T): Float = {
      var s = 0.0f
      var i = 0
      while ( i < N) {
        // TODO: use alpha(i) != 0 ? (for al alpha seems ok, for alpha(i) > 0 does not work)
        // S(i) == i
        if (S(i) != EMPTY ) {
          //s += alpha(i) * y(i) * K.K(data(i), dk)
          s += alpha(i) * K.K(data(i), dk)
        }
        i += 1
      }
      //s -= b
      s += b
      s
    }

    def predict(dk: T): Int = if (predictRaw( dk ) > 0) 1 else 0

    def predictions(dk: Array[T]): Array[Int] = {
      dk.map(predict)
    }

    def errorRate(yIn: Array[Float], dataIn: Array[T]): (Int, Int, Float) = {
      var n_total = 0
      var n_error = 0
      var i = 0
      while ( i < dataIn.length ) {
        val yo = yIn(i) > 0
        val y_hat = predict(dataIn(i)) > 0
        //if (debug) println(s"y = $yo ? y_hat = $y_hat")
        if ( y_hat != yo)
          n_error +=1
        n_total += 1
        i += 1
      }
      val error = (n_error / n_total.toDouble).toFloat
      dprintln(s"n_total = $n_total; n_error = $n_error ; error = $error")
      (n_total, n_error, error)
    }


    def learnErrorRate(): (Int, Int, Float) = errorRate(y, data)

    def updatePrimeW(data: Array[Array[Float]], w: Array[Float]): Unit = {
      var i = 0
      while ( i < N) {
        // TODO: use alpha(i) != 0 ? (for al alpha seems ok, for alpha(i) > 0 does not work)
        // S(i) == i
        val s = S(i)
        if (s != EMPTY ) {
          if (s != i) throw new RuntimeException(s"updatePrimeW: s($s) != i($i)")
          //println(s"alpha($s) = ${alpha(s)}")
          //println(s"data($s) = ${data(s).mkString(",")}")
          multiplyAdd(alpha(s), data(s), w)
          //println(s"w = ${w.mkString(",")}")
        }
        i += 1
      }
    }

    def updateLinearW(data: Array[Array[Float]]): Option[Array[Float]] = {
      val w = Array.fill(data(0).length)(0.0f)
      if (K.K.isInstanceOf[LinearF.type]){
        updatePrimeW(data, w)
        //println(s"Final w = ${w.mkString(",")}")
        Some(w)
      } else {
        None
      }
    }

    def predictLinearW(w: Array[Float], x: Array[Float]): Float = dot(w, x) + b

    def linearErrorRate(w: Array[Float])(data: Array[Array[Float]]): Float = {
      var n_total = 0
      var n_error = 0
      var i = 0
      while ( i < N ) {
        val yo = y(i) > 0
        val y_pred = predictLinearW(w, data(i))
        val y_hat = y_pred > 0
        //if (debug) println(s"y = $yo ? y_hat = $y_hat")
        //if (debug) println(s"y = $yo ? y_pred = $y_pred")
        if ( y_hat != yo)
          n_error +=1
        n_total += 1
        i += 1
      }
      dprintln(s"n_total = $n_total n_error = $n_error ")
      (n_error / n_total.toDouble).toFloat
    }

    def errorIdxs(preds: Array[Int]): IndexedSeq[Int] = {
      val failed = preds.indices.map{i =>
        val yo = y(i) > 0
        val y_hat = preds(i) > 0
        if ( y_hat != yo)
          -i
        else
          i
      }.filter( _ < 0)
      failed
    }

    def learnErrorIdxs(): IndexedSeq[Int] = errorIdxs(predictions(data))

  }

/*
  class Algo1O[T](val K: HashMapKernelCache[T],
                 val data: Array[T],
                 val y: Array[Float], // TODO: make Int ??
                 val C: Float,
                 val pickSample: Int => Int,
                 val epsilon: Float = 0.001f,
                 //val cacheSize: Int,
                 val initSampleSize: Int,
                 val onlineEpochs: Int = 1,
                 val maxFinishIter:Int = Int.MaxValue,
                 val debug: Boolean = false) extends Algo[T] {

    val EMPTY: Int = -1
    val FLT_EPSILON = 1e-6f

    // https://docs.scala-lang.org/overviews/collections/concrete-mutable-collection-classes.html
    // Support vectors
    val svs = mutable.DoubleLinkedList[SupportVector[T]]()

    // TODO: add to parameter ?
    //var TAU = 1E-12
    var TAU = 1E-7F

    var b: Float = 0.0f
    var delta: Float = 0.0f


    def dprintln[A](s:A):Unit = if (debug) println(s)

    def numSupportVector(): (Int, Int, Int) = {
      var i = 0
      var non_bound_support = 0
      var bound_support = 0
      var incorrect_support = 0
      while (i < N) {
        //if (Math.abs(alpha(i)) > 0.0) println(s"alpha($i) = ${alpha(i)} : A(i) = ${A(i)}, B(i) = ${B(i)}")
        //if (Math.abs(alpha(i)) > 0.0) println(s"alpha($i) = ${alpha(i) == A(i)}, B(i) = ${alpha(i) == B(i)}")
        if (alpha(i) != 0) {
          if (alpha(i) == A(i) || alpha(i) == B(i)){
            bound_support += 1
          }
          else if (alpha(i) > A(i) && alpha(i) < B(i)) {
            non_bound_support += 1
          }
          else {
            incorrect_support += 1
          }
        }
        i += 1
      }
      (non_bound_support, bound_support, incorrect_support)
    }

    def dprintDiagnostics(iters: Int): Unit = {
      println(s"Number iteration = $iters")
      var s = 0.0F
      var i = 0
      while (i < N) {
        if (S(i) != EMPTY) s += alpha(i)*y(i)
        i += 1
      }
      println(s"SUM( alpha(i)*y(i) ) = $s")
      var t = 0.0F
      i = 0
      while (i < N) {
        var j = 0
        if (S(i) != EMPTY){
          while (j < N) {
            //if (debug) println(s"K($i, $j) = ${K(i, j)}")
            if (S(i) != EMPTY) t += alpha(i) * alpha(j) * K(i, j)
            j += 1
          }
        }
        i += 1
      }
      println(s"SUM( alpha(i)*alpha(j)*K(i,j) ) = $t")
      println(s"Objective function = ${s - t / 2.0}")
      // Lagrange subject to:  Sum (alpha_i) = 0
      s = 0.0F
      i = 0
      while (i < N) {
        if (S(i) != EMPTY) s += alpha(i)
        i += 1
      }
      println(s"SUM(alpha(i)) = $s == 0")
      // Lagrange subject to:  all Ai <= alpha_i <= Bi
      // Lagrange subject to:  alpha_i < Ai
      i = 0
      while (i < N) {
        if ((S(i) != EMPTY) && (alpha(i) < A(i)))
          println(s"alpha($i) = ${alpha(i)} < Ai  ${y(i)}")
        i += 1
      }
      // Lagrange subject to:  alpha_i > Bi
      i = 0
      while (i < N) {
        if ((S(i) != EMPTY) && (alpha(i) > B(i)))
          println(s"alpha($i) = ${alpha(i)} > Bi")
        i += 1
      }
      i = 0
      val (non_bound_support, bound_support, incorrect_support) = numSupportVector()
      print(s"non_bound = $non_bound_support  ")
      print(s"bound_support = $bound_support  ")
      print(s"incorrect_support = $incorrect_support  ")
      println(s"sparsity = ${(bound_support + non_bound_support)*1.0 / N} %")
      println(s"C = $C")
      println(s"kernel = ${K.K}")
      println(s"b = $b")
    }

    // TODO: pre-calculate and cache ?
    def A(i:Int): Float = Math.min(0, C*y(i))
    def B(i:Int): Float = Math.max(0, C*y(i))

    // TODO: automatically update g-max/g-min instead of scanning
    // TODO: avoid use of Option?
    def argMin(): Option[SupportVector[T]] = {
      var acc = Float.MaxValue
      val i = svs.iterator
      // Must be fast avoid use of Option
      var k: SupportVector[T] = null
      while (i.nonEmpty){
        val s = i.next()
        if ( s.alpha > s.A) {
          // s in S
          if (s.g < acc) {
            acc = s.g
            k = s
          }
        }
      }
      Option(k)
    }


    // TODO: automatically update g-max/g-min instead of scanning
    // TODO: avoid use of Option?
    def argMax(): Option[SupportVector[T]] = {
      var acc = Float.MinValue
      val i = svs.iterator
      // Must be fast avoid use of Option
      var k: SupportVector[T] = null
      while (i.nonEmpty){
        val s = i.next()
        if ( s.alpha < s.B ) {
          // s in S
          if (s.g > acc) {
            acc = s.g
            k = s
          }
        }
      }
      Option(k)
    }

    // TODO: make parameter
    // TODO: we have additional versions of selectPair to test
    // TODO: SMILE https://github.com/haifengl/smile/blob/1d55b75f77c649d1de595d338e95e3eb1396b74a/core/src/main/java/smile/classification/SVM.java#L690
    //  [573] maximizes the curvature (gradient) See of this was optimal in LASVM experiments
    def selectPair(k: Int): (Int, Int) = {
      if (k != EMPTY){
        if (y(k) == 1){
          val min = argMin()
          dprintln(s"selectPair min ($k, $min)")
          (k, min )
        } else if (y(k) == -1) {
          val max = argMax()
          dprintln(s"selectPair max ($max, $k)")
          (max, k )
        } else
          throw new RuntimeException(s"Unexpected y($k) = ${y(k)} value")
      } else {
        val min = argMin()
        val max = argMax()
        dprintln(s"selectPair min/max ($max, $min)")
        (max, min)
      }
    }

    // TODO: is this an and or and or?
    // Note that argMax and/or argMin may not find candidates so no violation can occur
    def tau_ViolatingPair(i:Int, j:Int): Boolean = {
      // Check not in the pseudo-code
      if (i == EMPTY || j == EMPTY)
        false
      else {
        //println(s"--------------------- g($i) - g($j) = ${g(i) - g(j)}")
        (alpha(i) < B(i)) &&
          (alpha(j) > A(j)) &&
          (g(i) - g(j) > TAU)
      }
    }

    // g_k
    def gradient(k:Int): Float = {
      var i = 0
      var sum = 0.0f
      while (i < N){
        val s = S(i)
        // TODO: remove
        if (s != EMPTY){
          if (s != i) throw new RuntimeException(s"gradient: s($s) != i($i)")
          sum += alpha(s) * K(k, s)
        }
        i += 1
      }
      y(k) - sum
    }

    // TODO: update argMax and argMin here
    def updateGradients(lambda: Float, i:Int, j:Int): Unit = {
      var k = 0
      while (k < N){
        val s = S(k)
        if (s != EMPTY){
          // TODO: remove
          if (s != k) throw new RuntimeException(s"updateGradients: s($s) != i($k)")
          g(s) = g(s) - (lambda * (K(i,s) - K(j,s)) )
        }
        k += 1
      }
    }

    // See: https://github.com/haifengl/smile/blob/1d55b75f77c649d1de595d338e95e3eb1396b74a/core/src/main/java/smile/classification/SVM.java#L690
    def step(i: Int, j: Int) : Unit = {
      val eta = K(i) + K(j) - (2 * K(i, j))
      if (eta == 0)
      // We have a duplicated sample
        return
      if (eta < 0.0) {
        // should not happen
        throw new RuntimeException(s"step: eta = $eta is < 0 between i=$i and j=$j")
      }
      val ostep = Math.min( B(i) - alpha(i), alpha(j) - A(j))
      // Not in the pseudo-code but in the original code
      val lambda = Math.min( (g(i) - g(j)) / eta,  ostep)
      dprintln(s"step with lambda = $lambda: alpha($i) = ${alpha(i)}, alpha($j) = ${alpha(j)}, ostep = $ostep")
      if (lambda <= 0) throw new RuntimeException(s"lambda = $lambda")
      alpha(i) = alpha(i) + lambda
      alpha(j) = alpha(j) - lambda
      dprintln(s"step after lambda= $lambda: alpha($i) = ${alpha(i)}, alpha($j) = ${alpha(j)}")
      //System.out.flush()

      updateGradients(lambda, i, j)
    }

    def process(k:Int): Unit = {
      // 1. Bail out if k in S
      if (S(k) != EMPTY) {
        dprintln(s"process bail-out @ $k in S")
        return
      }

      // 2. Add new support vector (default bound)
      alpha(k) = 0
      g(k) = gradient(k) // TODO = 1, always constant?
      S(k) = k
      dprintln(s"Adding support vector $k")

      // 3. Select possible violating pair
      // TODO: make selection a parameter
      val (max, min) = selectPair(k)
      if (max == min) throw new RuntimeException(s"max($max) == min($min)")

      // 4. Bail out if not tau-violating pair
      if (!tau_ViolatingPair(max, min)) {
        dprintln(s"process bail-out @ ($max, $min) not tau-violating pair")
        return
      }

      /*
      // This check is not in the pseudo code
      // Is in original LASVM and SMILE
      if (g(min) < g(max)) {
        if ((y(k) > 0 && g(k) < g(min)) || (y(k) < 0 && g(k) > g(max))) {
          return
        }
      }
       */

      // 5. Update support vectors
      step(max, min)
    }

    def purgeCache(r: Int): Int = {
      var n = 1
      K.remove(r)
      var k = 0
      while (k < N){
        val s = S(k)
        if (s != EMPTY) {
          K.remove(r, s)
          n += 1
        }
        k += 1
      }
      n
    }

    def shrink(): (Int, Int) = {
      val i = argMax()
      val j = argMin()
      // Checks for empty not in pseudo-code
      val gi = if (i != EMPTY) g(i) else Float.MaxValue
      val gj = if (j != EMPTY) g(j) else Float.MinValue
      var k = 0
      var n = 0
      while (k < N){
        val s = S(k)
        if (s != EMPTY) n += 1
        //if (s != EMPTY && Math.abs(alpha(s)) <= epsilon ){
        // TODO: make search alpha close to 0?
        if (s != EMPTY && alpha(s) == 0 ){
          if (y(s) == -1 && g(s) >= gi) S(s) = EMPTY // remove
          else if (y(s) == +1 && g(s) <= gj) S(s) = EMPTY // remove
          val del = if (S(s) == EMPTY) purgeCache(s) else 0
          // TODO:remove
          if (S(s) == EMPTY) dprintln(s"shrink: removed $del support vector of sample $s")
        }
        k += 1
      }
      dprintln(s"****************************** shrink: number of SV's = $n")
      (i,j)
    }

    def reProcess(): (Float, Int, Int) = {
      // 1. Select possible violating pair
      // TODO: make selection a parameter
      val (i, j) = selectPair(EMPTY)
      // Unbound may mean i == j but this means that it is not a violating pair, so bails out
      //if (i == j) throw new RuntimeException(s"i($i) == j($j) : ${alpha(i)} < ${B(i)} ${alpha(j)} > ${A(i)}")

      // 4. Bail out if not tau-violating pair
      if (!tau_ViolatingPair(i, j)) {
        // Not in pseudo-code: return 0 to terminate finish
        // Note that even if non-violating, we may end up repeatedly selecting
        // the same pair. We therefore end-up iterating until maxIter
        dprintln(s"reProcess bail-out @ ($i, $j) not tau-violating pair (i=$i, j=$j, delta=$delta)")
        //return Float.MaxValue
        return (0.0f, i, j)
      }

      // 3. Update support vectors
      // TODO: some times the violating pair has a very small step and
      //  they keep on getting selected until maxIters. Shrink does nothing
      step(i, j)

      // 4. Shrink - remove blatant non support vectors
      val (maxf, minf) = shrink()

      // 5. update offset (bias) and gradient of most violating pair
      // TODO: original uses i, j
      if (maxf != EMPTY && minf != EMPTY){
        b = (g(maxf) + g(minf)) / 2
        delta = g(maxf) - g(minf)
        (delta, maxf, minf)
      } else {
        // TODO: Should this be 0?
        delta = 0.0f
        (delta, maxf, minf)
      }
    }


    // TODO: we need a true online version - scan only the new sample
    def online(numIters: Int): Unit = {
      var iters = 0
      // RANDOM selection (simplest)
      // TODO: get faster method fisher-yates?)
      // https://algs4.cs.princeton.edu/11model/Knuth.java.html
      val samples = Random.shuffle((0 until N).toList).toArray
      dprintln(s"Shuffled ${samples.length} samples.")
      while (iters < onlineEpochs){
        var i = 0
        while (i < N){
          val k = samples(i) // pickSample(N)
          dprintln(s"online @ $iters - picked sample ------------------------------------ $k")
          process(k)
          reProcess()
          i += 1
        }
        iters += 1
      }
    }

    def finish(): Unit = {
      numOverruns = 0
      var iters = 0
      var delta = Float.MaxValue
      while (delta > TAU && iters < maxFinishIter){
        dprintln(s"finish @ $iters")
        val (deltat, max, min) = reProcess()
        delta = deltat
        //dprintDiagnostics(iters)
        dprintln(s"finish: delta = $delta <= $TAU")
        iters += 1
      }
      //if (iters == maxFinishIter) println(s"delta = $delta @ $iters")
      numOverruns += iters
      if (iters == maxFinishIter) println(s"Stopped: delta <= TAU = $delta <= $TAU ; iters < maxFinishIter = $iters < $maxFinishIter ")
    }

    def initialGradients(): Unit = {
      var i = 0
      while (i < N){
        val s = S(i)
        if (s != EMPTY){
          // TODO: remove
          if (s != i) throw new RuntimeException(s"initialGradients: s($s) != i($i)")
          g(s) = gradient(s)
          dprintln(s"initialGradients g($s) = ${g(s)}")
        }
        i += 1
      }
    }

    // Because all alphas = 0 this implies from equation (9) that gk = yk
    // TODO: substitute gradient(i) with y(i) ? Maybe we can initialize alpha randomly?
    def seedSamples(nSamples: Int): Unit = {
      // TODO: get faster method fisher-yates?)
      // https://algs4.cs.princeton.edu/11model/Knuth.java.html
      val samples = Random.shuffle((0 until N).toList)
      samples.take(initSampleSize).foreach{
        i =>
          S(i) = i
          dprintln(s"seeSample: S($i) = ${S(i)}")
        //g(i) = gradient(i)
        //println(s"init: g($i) = ${g(i)}")
      }
    }

    def learn(): Unit = {

      // Initialization
      seedSamples(initSampleSize)
      initialGradients()
      if (debug) dprintDiagnostics(0)

      // Online iterations
      online(onlineEpochs)
      if (debug) dprintDiagnostics(0)

      // Finishing
      finish() //TODO: BUG - always selecting the same max/min Pair and bailing out

      //S.foreach( si => if (si != EMPTY) println(s"alpha(${si}) = ${alpha(si)}"))
    }

    def fit(): Unit = learn()

    def predictRaw(dk: T): Float = {
      var s = 0.0f
      var i = 0
      while ( i < N) {
        // TODO: use alpha(i) != 0 ? (for al alpha seems ok, for alpha(i) > 0 does not work)
        // S(i) == i
        if (S(i) != EMPTY ) {
          //s += alpha(i) * y(i) * K.K(data(i), dk)
          s += alpha(i) * K.K(data(i), dk)
        }
        i += 1
      }
      //s -= b
      s += b
      s
    }

    def predict(dk: T): Int = if (predictRaw( dk ) > 0) 1 else 0

    def predictions(dk: Array[T]): Array[Int] = {
      dk.map(predict)
    }

    def errorRate(yIn: Array[Float], dataIn: Array[T]): (Int, Int, Float) = {
      var n_total = 0
      var n_error = 0
      var i = 0
      while ( i < dataIn.length ) {
        val yo = yIn(i) > 0
        val y_hat = predict(dataIn(i)) > 0
        //if (debug) println(s"y = $yo ? y_hat = $y_hat")
        if ( y_hat != yo)
          n_error +=1
        n_total += 1
        i += 1
      }
      val error = (n_error / n_total.toDouble).toFloat
      dprintln(s"n_total = $n_total; n_error = $n_error ; error = $error")
      (n_total, n_error, error)
    }


    def learnErrorRate(): (Int, Int, Float) = errorRate(y, data)

    def updatePrimeW(data: Array[Array[Float]], w: Array[Float]): Unit = {
      var i = 0
      while ( i < N) {
        // TODO: use alpha(i) != 0 ? (for al alpha seems ok, for alpha(i) > 0 does not work)
        // S(i) == i
        val s = S(i)
        if (s != EMPTY ) {
          if (s != i) throw new RuntimeException(s"updatePrimeW: s($s) != i($i)")
          //println(s"alpha($s) = ${alpha(s)}")
          //println(s"data($s) = ${data(s).mkString(",")}")
          multiplyAdd(alpha(s), data(s), w)
          //println(s"w = ${w.mkString(",")}")
        }
        i += 1
      }
    }

    def updateLinearW(data: Array[Array[Float]]): Option[Array[Float]] = {
      val w = Array.fill(data(0).length)(0.0f)
      if (K.K.isInstanceOf[LinearF.type]){
        updatePrimeW(data, w)
        //println(s"Final w = ${w.mkString(",")}")
        Some(w)
      } else {
        None
      }
    }

    def predictLinearW(w: Array[Float], x: Array[Float]): Float = dot(w, x) + b

    def linearErrorRate(w: Array[Float])(data: Array[Array[Float]]): Float = {
      var n_total = 0
      var n_error = 0
      var i = 0
      while ( i < N ) {
        val yo = y(i) > 0
        val y_pred = predictLinearW(w, data(i))
        val y_hat = y_pred > 0
        //if (debug) println(s"y = $yo ? y_hat = $y_hat")
        //if (debug) println(s"y = $yo ? y_pred = $y_pred")
        if ( y_hat != yo)
          n_error +=1
        n_total += 1
        i += 1
      }
      dprintln(s"n_total = $n_total n_error = $n_error ")
      (n_error / n_total.toDouble).toFloat
    }

    def errorIdxs(preds: Array[Int]): IndexedSeq[Int] = {
      val failed = preds.indices.map{i =>
        val yo = y(i) > 0
        val y_hat = preds(i) > 0
        if ( y_hat != yo)
          -i
        else
          i
      }.filter( _ < 0)
      failed
    }

    def learnErrorIdxs(): IndexedSeq[Int] = errorIdxs(predictions(data))

  }
*/


  /**
    * Added changes to article's pseudo code with changes found in SMILE.
    * These changes have no effect on average outcome. They also have no
    * effect on running the simple two blob quasi-linear separable problem.
    * However the finish steps still extends past the maxFinishIter = 1000,
    * which does not seem to occur in the SMILE version.
    *
    * Changes:
    *
    * 1. process(k: Int): Unit makes a check on the derivatives in order
    *    to ignore further processing of a pair. No effect.
    *
    * 2. step(i: Int, j: Int): Unit add as numerical check ion lambda (reset
    *    fractions to epsilon). No effect.
    *
    * 3. selectPair(k: Int): (Int, Int) selects a pair based on the curvature
    * if either a max or min support vector is not found. Has no effect.
    * This section seems to be executed only on the online's reprocess step.
    * It is not called in the finish step. In addition to this when we have
    * and max search for a min, all alpha = 0 and g = 1. So no no new min is
    * found. When we have a min an search a max, we have alpha <> 0 but still
    * no new max is found.
    *
    * Long running finish step still occurs.
    *
    * @param K
    * @param data
    * @param y
    * @param C
    * @param pickSample
    * @param epsilon
    * @param initSampleSize
    * @param onlineEpochs
    * @param maxFinishIter
    * @param debug
    * @tparam T
    */
  class Algo2[T](override val K: HashMapKernelCache[T],
                 override val data: Array[T],
                 override val y: Array[Float],
                 override val C: Float,
                 override val pickSample: Int => Int,
                 override val epsilon: Float = 0.001f,
                 //override val cacheSize: Int,
                 override val initSampleSize: Int,
                 override val onlineEpochs: Int = 1,
                 override val maxFinishIter:Int = Int.MaxValue,
                 override val debug: Boolean = false) extends
    Algo1(K, data, y, C, pickSample, epsilon, initSampleSize, onlineEpochs, maxFinishIter,debug) {

    // Added SMILE search for max and min support vectors as hown in
    // https://github.com/haifengl/smile/blob/1d55b75f77c649d1de595d338e95e3eb1396b74a/core/src/main/java/smile/classification/SVM.java#L580
    override  def selectPair(k: Int): (Int, Int) = {
      if (k != EMPTY){
        if (y(k) == 1){
          val min = argMin()
          dprintln(s"selectPair min ($k, $min)")
          (k, min )
        } else if (y(k) == -1) {
          val max = argMax()
          dprintln(s"selectPair max ($max, $k)")
          (max, k )
        } else
          throw new RuntimeException(s"Unexpected y($k) = ${y(k)} value")
      } else {

        // Not like the article ut used in the original code and SMILE
        // In the original code it is the 'gs1' function
        // In SMILE it starts at https://github.com/haifengl/smile/blob/1d55b75f77c649d1de595d338e95e3eb1396b74a/core/src/main/java/smile/classification/SVM.java#L580
        val min = argMin()
        val max = argMax()

        // Issue, in SMILE if we do get an EMPTY, we still process the next steps
        // However the original code does not process the steps
        val gmax = if (max == EMPTY) Float.MinValue else g(max)
        val gmin = if (min == EMPTY) Float.MaxValue else g(min)

        if (max != EMPTY && min != EMPTY){
          (max, min)
        }
        // SMILE code checks gmax > -gmin
        else if (max != EMPTY ){
          //S.foreach( si => if (si != EMPTY) println(s"alpha($si) = ${alpha(si)} g($si) = ${g(si)}"))
          // v2 = max
          // Have inf, get sup
          var i = 0
          var best = 0.0f // Float.MinValue ?
          var new_min = EMPTY
          while (i < N){
            val s = S(i)
            if ( s != EMPTY && s != max) {
              // s in S
              var curve = K(s) + K(max) - (2 * K(s, max)) // eta
              if (curve <= 0) throw new RuntimeException(s"Sup: curve s($s) = $curve")
              if (curve == 0.0) curve = TAU
              //println(s"$gmax - ${g(s)}")
              val z = gmax - g(s)
              val mu = z / curve
              //if (mu <= 0) throw new RuntimeException("selectPair: Min mu <= 0")
              //println(s"Min(max=$max) z = $z curve = $curve mu = $mu alpha($s) = ${alpha(s)}  > ${B(s)} < ${A(s)}")
              // Violating pair - sup only
              //if (tau_ViolatingPair(max, s)) {
              //if (alpha(s) > A(s)) {
              if ((mu > 0.0 && alpha(s) > B(s)) ||      // max > B(s)
                  (mu < 0.0 && alpha(s) < A(s))){       // min < A(s)
                val gain = z * mu
                if (gain > best) {
                  best = mu
                  new_min = s
                  println(s"Best = $best")
                }
              }
            }
            i += 1
          }

          if (best != 0.0 )  throw new RuntimeException(s"selectPair Min: g($max): $gmax > -g($min) : $gmin)")
          (max, new_min)

        } else
          // SMILE seems to check gmax <= -gmin
        if (min != EMPTY) {
          //S.foreach( si => if (si != EMPTY) println(s"alpha($si) = ${alpha(si)} g($si) = ${g(si)}"))
          // v1 = min
          // Have inf, get sup
          var i = 0
          var new_max = EMPTY
          var best = 0.0f // Float.MaxValue ?
          while (i < N){
            val s = S(i)
            if ( s != EMPTY && s != min) {
              // s in S
              var curve = K(s) + K(min) - (2 * K(s, min)) // eta
              if (curve <= 0) throw new RuntimeException(s"Inf: curve s($s) = $curve")
              if (curve == 0.0) curve = TAU
              val z = g(s) - gmin
              val mu = z / curve
              //if (mu <= 0) throw new RuntimeException("selectPair: Max mu <= 0")
              //println(s"Min($min) @$s == $i curve = $curve ; ${g(s) - gmin} ; mu = $mu")
              // Violating pair - inf only
              //println(s"Max(min=$min) z = $z curve = $curve  mu = $mu alpha($s) = ${alpha(s)}  > ${B(s)} < ${A(s)}")
              //if (tau_ViolatingPair(s, min)) {
              //if (alpha(s) < B(s)) {
              if ((mu > 0.0 && alpha(s) < B(s)) ||      // max < B(s)
                  (mu < 0.0 && alpha(s) > A(s))) {      // min > A(s)
                val gain = z * mu
                if (gain > best) {
                  best = mu
                  new_max = s
                  println(s"Best = $best")
                }
              }
            }
            i += 1
          }
          if (best != 0.0 ) throw new RuntimeException(s"selectPair Max: g($max): $gmax <= -g($min) : $gmin)")

          (new_max, min)
        } else {
          (max, min)
        }
      }
    }


    // Added derivaive checks as in original and SMILE code
    override def process(k: Int): Unit = {
      // 1. Bail out if k in S
      if (S(k) != EMPTY) {
        dprintln(s"process bail-out @ $k in S")
        return
      }

      // 2. Add new support vector (default bound)
      alpha(k) = 0
      g(k) = gradient(k)
      S(k) = k
      dprintln(s"Adding support vector $k")

      // 3. Select possible violating pair
      // TODO: make selection a parameter
      val (max, min) = selectPair(k)
      if (max == min) throw new RuntimeException(s"max($max) == min($min)")

      // 4. Bail out if not tau-violating pair
      if (!tau_ViolatingPair(max, min)) {
        dprintln(s"process bail-out @ ($max, $min) not tau-violating pair")
        return
      }

      // Change to original - never seems to be activated
      // This check is not in the pseudo code
      // Is in original LASVM and SMILE
      if (g(min) < g(max)) {
        if ((y(k) > 0 && g(k) < g(min)) || (y(k) < 0 && g(k) > g(max))) {
          throw new RuntimeException("Process: change to original 1")
          //return
        }
      }

      // 5. Update support vectors
      step(max, min)
    }

    // Added lambda check as in original code
    // See: https://github.com/haifengl/smile/blob/1d55b75f77c649d1de595d338e95e3eb1396b74a/core/src/main/java/smile/classification/SVM.java#L690
    override def step(i: Int, j: Int): Unit = {
      val eta = K(i) + K(j) - (2 * K(i, j))
      if (eta == 0)
        // We have a duplicated sample
        return
      if (eta < 0.0) {
        // should not happen
        throw new RuntimeException(s"step: eta = $eta is < 0 between i=$i and j=$j")
      }
      val ostep = Math.min(B(i) - alpha(i), alpha(j) - A(j))
      // Not in the pseudo-code but in the original code
      //val lambda = Math.min( (g(i) - g(j)) / eta,  ostep)
      val curvature = (g(i) - g(j)) / eta
      // Change to original - this i activated many times, however their seems to be no change
      val lambda = if (eta >= FLT_EPSILON) Math.min(curvature, ostep) else ostep
      dprintln(s"step with lambda = $lambda: ostep = $ostep, curvature = $curvature")
      dprintln(s"step with lambda = $lambda: alpha($i) = ${alpha(i)}, alpha($j) = ${alpha(j)}, ostep = $ostep")
      if (lambda <= 0) throw new RuntimeException(s"lambda = $lambda")
      alpha(i) = alpha(i) + lambda
      alpha(j) = alpha(j) - lambda
      dprintln(s"step after lambda= $lambda: alpha($i) = ${alpha(i)}, alpha($j) = ${alpha(j)}")
      //System.out.flush()

      updateGradients(lambda, i, j)
    }

  }

}


