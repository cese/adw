package pt.inescn.models.svm


import java.lang.{System => JSystem}
import better.files.File

import scala.collection.mutable.{ArrayBuffer => Vec}
import scala.util.Random

/**
  * Note: when we scale during training, the scale limits and extremes are
  * recorded. The scaling is automatically repeated in the test data-set.
  *
  * We assume that the number of features in the training and testing data
  * sets are the same. This may not be strictly true. Always use the "-d"
  * flag to set the # features.
  *
  * No shrinking is implemented, so the solution may not be optimally sparse.
  * Note that here we do not keep an active set of alpha. Only the value of
  * alpha is used to check if it is bound, unbound or inactive (after clipping).
  * So no explicit shrinking is possible.
  *
  * Test data sets:
  * . https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html
  *   . a1a, a1a.t
  *     They say we have #123 features but train has only 199 and test has only 121 features.
  *     root/runMain pt.inescn.models.svm.SMO -s -c 1 -p 0.0003 -libsvm -d 123 -scale -1,1 -f "../data/libsvm/a1a"
  *     non_bound = 1184  bound_support = 404  sparsity = 0.5034267912772585
  *     error_rate = 0.008722741 (accuracy = 0.9912773)
  *     root/runMain pt.inescn.models.svm.SMO -s -c 1 -p 0.0003 -libsvm -d 123 -a -f "../data/libsvm/a1a.t"
  *     Error rate = 0.23898436 (accuracy = 0.76101565)
  *
  * . LibSVM Guide
  *   https://www.csie.ntu.edu.tw/~cjlin/papers/guide/guide.pdf
  *
  *   . svmguide1: RBF kernel, libSVM gamma = 2 = SMO 1/(2sigma²) = 0.5
  *   . C = 2
  *     root/runMain pt.inescn.models.svm.SMO -i 200 -s -c 2 -p 0.5 -libsvm -d 4 -scale -1,1 -f "../data/libsvm/svmguide1"
  *     . non_bound = 27  bound_support = 286  sparsity = 0.18517319520880543
  *     . error_rate = 0.037876334 (accuracy = 0.9621237)
  *     . 96.8892% with libSVM
  *     Checking
  *     root/runMain pt.inescn.models.svm.SMO -s -libsvm  -d 4 -a -f "../data/libsvm/svmguide1"
  *     . Error rate = 0.037876334 (accuracy = 0.9621237)
  *     root/runMain pt.inescn.models.svm.SMO -s -libsvm -d 4 -a -f "../data/libsvm/svmguide1.t"
  *     . Error rate = 0.0345 (accuracy = 0.9655)
  *     . 96.875% with libSVM
  *
  *   . svmguide3: RBF kernel, libSVM gamma = 0.125 = SMO 1/(2sigma²) = 8
  *   . C = 128
  *     root/runMain pt.inescn.models.svm.SMO -i 2000 -s -c 128 -p 8 -libsvm -d 22 -scale -1,1 -f "../data/libsvm/svmguide3"
  *     . non_bound = 60  bound_support = 56  sparsity = 0.09010458567980692
  *     . error_rate = 0.1842317 (accuracy = 0.8157683)
  *     . 84.8753% libSVM
  *     . non_bound = 101  bound_support = 106  sparsity = 0.17055510860820594
  *     . error_rate = 0.13032985 (accuracy = 0.86967015) of -i 5000
  *     Checking
  *     root/runMain pt.inescn.models.svm.SMO -s -libsvm  -d 4 -a -f "../data/libsvm/svmguide3"
  *     . Error rate = 0.1842317 (accuracy = 0.8157683)
  *     . Ok
  *     root/runMain pt.inescn.models.svm.SMO -s -libsvm -d 4 -a -f "../data/libsvm/svmguide3.t"
  *     . Error rate = 0.2682927 (accuracy = 0.73170733)
  *     . 87.8049% libSVM
  *     . Error rate = 0.31707317 (accuracy = 0.68292683) for -i 5000 (over-fitting)
  *
  * Issue 1: only handles binary classes
  * Issue 2: we need a maximum number of iterations in case problem is not solvable
  *          we can stop if the number of support vectors does not increase
  *          we can stop if after max number of iterations the error does not change
  *          we can stop if after max number when the number of bound an unbound
  *          support vectors does not change, even if yh number of changed vectors
  *          is > 0
  *          we can stop if the \delta \alpha_i << \varepsilon
  * Issue 3: Support Vector Machines, Léon Bottou and Chih-Jen Li. We need to add:
  *          - shrinking
  *          - second order working set selection
  *          - maxinal violating pair working set selection (already done)
  * Issue : svmguide1 : the number of alpha = 0 is near 0. As we progress bound and non-bound vectors increases !?
  *
  * Examples:
  * root/runMain pt.inescn.models.svm.SMO -s -libsvm -d 4 -f "../data/libsvm/svmguide1"
  * root/runMain pt.inescn.models.svm.SMO -s -libsvm -d 4 -scale 0,1 -f "../data/libsvm/svmguide1"
  * root/runMain pt.inescn.models.svm.SMO -s -libsvm -d 4 -scale -1,1 -f "../data/libsvm/svmguide1"
  * root/runMain pt.inescn.models.svm.SMO -s -c 1 -p 0.0003 -libsvm -d 4 -scale -1,1 -f "../data/libsvm/svmguide1"
  *
  */
object SMO {
  val endl = "\n"
  val debug = true
  var debugx = false

  var N: Int  = 0                        // number of samples
  var d: Int  = -1                       // number of features
  var C: Float = 0.05F                   // hyper-parameter - slack
  var tolerance: Float = 0.001F
  var eps: Float = 0.001F
  var two_sigma_squared: Float = 2F

  var alph: Vec[Float] = Vec()     // Lagrange multipliers
  var b: Float = 0                               // threshold
  var w: Vec[Float] = Vec()        // weight vector: only for linear kernel

  var error_cache: Vec[Float] = Vec()
  class sparse_binary_vector {
    val id: Vec[Int] = Vec()
  }

  class sparse_vector {
    val id: Vec[Int] = Vec()
    val value: Vec[Float] = Vec()
  }

  type dense_vector = Vec[Float]

  var is_sparse_data: Boolean = false
  var is_binary: Boolean = false

  // use only one of these
  val sparse_binary_points: Vec[sparse_binary_vector] = Vec()
  val sparse_points: Vec[sparse_vector] = Vec()
  var dense_points: Vec[dense_vector] = Vec()
  var target: Vec[Int] = Vec()                 // class labels of training data points

  var is_test_only: Boolean  = false
  var is_linear_kernel: Boolean  = false

  // extras
  var is_libsvm_format: Boolean = false
  var is_libsvm_scale : Boolean = false
  var lower: Float = 0.0f
  var upper: Float = 1.0f

  var first_test_i: Int = 0                     // data points with index in [first_test_i .. N) will be tested to compute error rate
  var end_support_i: Int = -1                   // support vectors are within [0..end_support_i)


  val tmp = File(JSystem.getProperty("java.io.tmpdir"))
  var data_file_name: File = tmp / "svm.data"
  var svm_file_name: File = tmp / "svm.model"
  var output_file_name: File = tmp / "svm.output"

  // 29c
  var precomputed_self_dot_product: Vec[Float] = Vec()

  // extra
  var minAtributes : Vec[Float] = Vec()
  var maxAtributes : Vec[Float] = Vec()
  var maxIter = Int.MaxValue
  var numIter = 0


  // 15b
  def fit(): Unit = {

    alph = Vec.fill(end_support_i)(0.0f)

    /* initialize threshold to zero */
    b = 0.0f

    /* E_i = u_i - y_i = 0 - y_i = -y_i */
    error_cache = Vec.fill(N)(0)
    if (is_linear_kernel)
      w = Vec.fill(d)(0.0f)

    var numChanged = 0
    var examineAll = 1

    numIter = 0

    while ((numChanged > 0 || (examineAll == 1)) && (numIter < maxIter)) {
      numChanged = 0
      if (examineAll == 1) {
        //if (debug) println(s"examineAll")
        var k = 0
        while (k < N){
          numChanged += examineExample(k)
          //if (debug) println(s"examineAll($k) : numChanged = $numChanged")
          k += 1
        }
      }
      else {
        //if (debug) println(s"examine non-bound")
        var k = 0
        while (k < N){
          if (alph(k) != 0 && alph(k) != C)
            numChanged += examineExample(k)
          k += 1
        }
      }
      if (examineAll == 1)
        examineAll = 0
      else if (numChanged == 0)
        examineAll = 1
      if (debug) {
        val err = error_rate()
        println(s"error_rate = $err (accuracy = ${1 - err})")
      }
      // 36d
      if (debug) printDiagnostics()
      if (debug) println(s"examineAll = $examineAll, numChanged = $numChanged")
      numIter += 1
      if (debug)
        if (numIter >= maxIter)
        println(s"exceeded maximum iterations = $maxIter")

    }
    // 36a
    writeModelParameters()
    if (debug) println(s"threshold = $b")

    if (debug) {
      val err = error_rate()
      println(s"error_rate = $err (accuracy = ${1 - err})")
    }
    // 36c
    writeClassificationOutput()
  }


  def predict(): Unit = {
    val err = error_rate()
    if (debug) println(s"Error rate = $err (accuracy = ${1 - err})")
  }

  // 15b

  var learned_func : Int => Float = init_learned_func()

  var dot_product_func : (Int,Int) => Float = init_dotproduct_func()

  var kernel_func : (Int, Int) => Float = init_kernel_func()

  var random = new Random()

  // 18a
  def examineExample(i1: Int): Int = {
    //if (debug) println(s"examineExample @ i1 = $i1")
    val y1 = target(i1)
    val alph1 = alph(i1)
    val E1: Float = if (alph1 > 0 && alph1 < C) {
      //if (debug) println(s"error_cache @ i1 = $i1")
      error_cache(i1)
    } else {
      //if (debug) println(s"learned_func @ i1 = $i1")
      learned_func(i1) - y1
    }

    val r1 = y1 * E1
    //if (debug) println(s"E1 = $E1, r1 = $r1")
    if ((r1 < -tolerance && alph1 < C)
      || (r1 > tolerance && alph1 > 0))
    {
      // Try i2 by three ways; if successful, then immediately return 1;
      if (argMaxE(i1, E1) > 0)
        // Try argmax E1 - E2 18b
        return 1
      else if( scanAllNonBoundSamples(i1, E1) > 0)
        // Try iterating through the non-bound examples 19b
        return 1
      else if( scanAllSamples(i1, E1) > 0)
        // Try iterating through the entire training set 19c
        return 1
    }
    0
  }


  // 18b
  def argMaxE(i1: Int, E1: Float): Int = {
    //if (debug) println(s"argMax(i1 = $i1, E1 = $E1)")
    var i2 = -1
    var tmax = 0.0f
    var k = 0
    while ( k < end_support_i){
      if (alph(k) > 0 && alph(k) < C)
      {
        val E2 = error_cache(k)
        val temp = Math.abs(E1 - E2)
        if (temp > tmax)
        {
          tmax = temp
          i2 = k
        }
      }
      k += 1
    }

    if (i2 >= 0) {
      takeStep(i1, i2)
    } else {
      0
    }
  }

  // 19b
  def scanAllNonBoundSamples(i1: Int, E1: Float): Int = {
    //if (debug) println(s"scanAllNonBoundSamples(i1 = $i1, E1 = $E1)")
    val k0 = (random.nextFloat() * end_support_i).toInt
    var k = k0
    while(k < end_support_i + k0) {
      val i2 = k % end_support_i
      if (alph(i2) > 0 && alph(i2) < C) {
        if (takeStep(i1, i2) > 0)
          return 1
      }
      k += 1
    }
    0
  }

  // 19c
  def scanAllSamples(i1: Int, E1: Float): Int = {
    //if (debug) println(s"scanAllSamples(i1 = $i1, E1 = $E1)")
    val k0 = (random.nextFloat() * end_support_i).toInt
    var k = k0
    while(k < end_support_i + k0) {
      val i2 = k % end_support_i
      if (takeStep(i1, i2) > 0)
        return 1
      k += 1
    }
    0
  }

  // 20
  def takeStep(i1: Int, i2: Int): Int = {
    if (i1 == i2) return 0

    // Look up alph1, y1, E1, alph2, y2, E2 21a
    val (alph1, y1, e1) = lookUp(i1)
    val (alph2, y2, e2) = lookUp(i2)
    val s = y1 * y2

    //Compute L, H 22a
    val (l, h) = clipRange(alph1, y1, alph2, y2)
    if (l == h)
      return 0

    // Compute eta 22b
    // TODO: change sign?
    val k11 = kernel_func(i1, i1)
    val k12 = kernel_func(i1, i2)
    val k22 = kernel_func(i2, i2)
    val eta = 2 * k12 - k11 - k22
    // a1, a2 new values of alpha_1, alpha_2
    var a2 = if (eta < 0) {
      val a2t = alph2 + y2 * (e2 - e1) / eta
      if (a2t < l)
        l
      else if (a2t > h)
        h
      else
        a2t
    }
    else {
      // compute Lobj, Hobj: objective function at a2=L, a2=H 22d
      // TODO: change sign?
      val c1 = eta/2
      val c2 = y2 * (e1-e2)- eta * alph2
      val Lobj = c1 * l * l + c2 * l
      val Hobj = c1 * h * h + c2 * h
      if (Lobj > Hobj+eps)
        l
      else if (Lobj < Hobj-eps)
        h
      else
        alph2
    }
    // TODO: ????
    if (Math.abs(a2-alph2) < eps*(a2+alph2+eps))
      return 0
    var a1 = alph1 - s * (a2 - alph2)
    // TODO: clipping of a1 ????
    if (a1 < 0) {
      a2 += s * a1
      a1 = 0
    }
    else if (a1 > C) {
      val t = a1-C
      a2 += s * t
      a1 = C
    }
    // Update threshold to reflect change in Lagrange multipliers 23a
    val delta_b = updateThreshold(y1, a1, alph1, e1, y2, a2, alph2, e2, k11, k12, k22)
    // Update weight vector to reflect change in a1 and a2, if linear SVM 23c
    updateLinearKernelWeights(i1, y1, a1, alph1, i2, y2, a2, alph2)
    // Update error cache using new Lagrange multipliers 24a
    updateLagrangeMultipliers(i1, y1, a1, alph1, i2, y2, a2, alph2, delta_b)
    alph(i1) = a1      // Store a1 in the alpha array
    alph(i2) = a2      // Store a2 in the alpha array

    1
  }

  // 21a
  def lookUp(i1: Int): (Float, Int, Float) = {
    val alph1 = alph(i1)
    val y1 = target(i1)
    val E1 = if (alph1 > 0 && alph1 < C)
      error_cache(i1)
    else
      learned_func(i1) - y1
    (alph1, y1, E1)
  }

  // 21b
  def clipRange(alph1: Float, y1: Float, alph2: Float, y2: Float ): (Float, Float) = {
    if (y1 == y2) {
      val gamma = alph1 + alph2
      if (gamma > C) {
        val L = gamma-C
        val H = C
        (L, H)
      }
      else {
        val L = 0
        val H = gamma
        (L, H)
      }
    }
    else {
      val gamma = alph1 - alph2
      if (gamma > 0) {
        val L = 0
        val H = C - gamma
        (L, H)
      }
      else {
        val L = -gamma
        val H = C
        (L, H)
      }
    }
  }

  // 23a
  def updateThreshold(y1:Float, a1: Float, alph1:Float, E1:Float,
                      y2:Float, a2: Float, alph2:Float, E2:Float,
                      k11: Float, k12:Float, k22:Float): Float = {
    val bnew = if (a1 > 0 && a1 < C)
      b + E1 + y1 * (a1- alph1) * k11 + y2 * (a2 - alph2) * k12
    else {
      if (a2 > 0 && a2 < C)
        b + E2 + y1 * (a1 - alph1) * k12 + y2 * (a2 - alph2) * k22
      else {
        val b1 = b + E1 + y1 * (a1 - alph1) * k11 + y2 * (a2 - alph2) * k12
        val b2 = b + E2 + y1 * (a1 - alph1) * k12 + y2 * (a2 - alph2) * k22
        (b1 + b2) / 2
      }
    }
    // TODO: where is this used?
    val delta_b = bnew - b
    b = bnew
    delta_b
  }

  // 23c
  def updateLinearKernelWeights(i1: Int, y1: Float, a1: Float, alph1: Float,
                                i2: Int, y2: Float, a2: Float, alph2: Float): Unit = {
    if (is_linear_kernel) {
      val t1 = y1 * (a1 - alph1)
      val t2 = y2 * (a2 - alph2)
      if (is_sparse_data && is_binary) {
        val num1 = sparse_binary_points(i1).id.size
        var p1 = 0
        while(p1 < num1){
          w(sparse_binary_points(i1).id(p1)) += t1
          p1 += 1
        }
        val num2 = sparse_binary_points(i2).id.size
        var p2 = 0
        while (p2 < num2){
          w(sparse_binary_points(i2).id(p2)) += t2
          p2 += 1
        }
      }
      else if (is_sparse_data && !is_binary) {
        val num1 = sparse_points(i1).id.size
        var p1 = 0
        while (p1 < num1){
          w(sparse_points(i1).id(p1)) += t1 * sparse_points(i1).value(p1)
          p1 += 1
        }
        val num2 = sparse_points(i2).id.size
        var p2 = 0
        while (p2 < num2){
          w(sparse_points(i2).id(p2)) += t2 * sparse_points(i2).value(p2)
          p2 += 1
        }
      }
      else {
        var i = 0
        while(i<d){
          w(i) += dense_points(i1)(i) * t1 + dense_points(i2)(i) * t2
          i += 1
        }
      }
    }
  }

  /*
   * Learned Kernel function
   */

  // 24b
  def learned_func_linear_sparse_binary(k: Int): Float = {
    //if (debug) println(s"learned_func_linear_sparse_binary @ $k")
    var s = 0.0f
    var i = 0
    while ( i < sparse_binary_points(k).id.size ){
      s += w(sparse_binary_points(k).id(i))
      i += 1
    }
    s -= b
    s
  }

  // 25a
  def learned_func_linear_sparse_nonbinary(k: Int): Float = {
    //if (debug) println(s"learned_func_linear_sparse_nonbinary @ $k")
    var s = 0.0f
    var i = 0
    while (i < sparse_points(k).id.size) {
      val j = sparse_points(k).id(i)
      val v = sparse_points(k).value(i)
      s += w(j) * v
      i += 1
    }
    s -= b
    s
  }

  // 25b
  def learned_func_linear_dense(k: Int): Float = {
    //if (debug) println(s"learned_func_linear_dense @ $k")
    var s = 0.0f
    var i = 0
    while (i < d) {
      s += w(i) * dense_points(k)(i)
      i += 1
    }
    s -= b
    s
  }

  // 25c
  def learned_func_nonlinear(k: Int): Float = {
    //if (debug) println(s"learned_func_nonlinear @ $k")
    var s = 0.0f
    var i = 0
    while ( i < end_support_i) {
      if (alph(i) > 0) {
        s += alph(i) * target(i) * kernel_func(i, k)
      }
      i += 1
    }
    s -= b
    s
  }

  //26a
  def init_learned_func(): Int => Float = {
    if (is_linear_kernel && is_sparse_data && is_binary)
      learned_func_linear_sparse_binary

    else if (is_linear_kernel && is_sparse_data && !is_binary)
      learned_func_linear_sparse_nonbinary

    else if (is_linear_kernel && !is_sparse_data)
      learned_func_linear_dense

    else if (!is_linear_kernel)
      learned_func_nonlinear

    else
       // Break
      (_:Int) => ???
  }

  /*
   * Dot product
   */

  // 26d
  def dot_product_sparse_binary(i1: Int, i2: Int): Float = {
    var p1 = 0
    var p2 = 0
    var dot = 0
    val num1 = sparse_binary_points(i1).id.size
    val num2 = sparse_binary_points(i2).id.size
    while (p1 < num1 && p2 < num2) {
      val a1 = sparse_binary_points(i1).id(p1)
      val a2 = sparse_binary_points(i2).id(p2)
      if (a1 == a2) {
        dot +=1
        p1 += 1
        p2 += 1
      }
      else if (a1 > a2)
        p2 += 1
      else
        p1 += 1
    }
    dot.toFloat
  }

  // 27a
  def dot_product_sparse_nonbinary(i1: Int, i2: Int): Float = {
    var p1 = 0
    var p2 = 0
    var dot = 0.0f
    val num1 = sparse_points(i1).id.size
    val num2 = sparse_points(i2).id.size
    while ( p1 < num1 && p2 < num2 ) {
      val a1 = sparse_points(i1).id(p1)
      val a2 = sparse_points(i2).id(p2)
      if (a1 == a2) {
        dot += sparse_points(i1).value(p1) * sparse_points(i2).value(p2)
        p1 += 1
        p2 += 1
      }
      else if (a1 > a2) {
        p2 += 1
      }
      else {
        p1 += 1
      }
    }
    dot
  }

  // 27b
  def dot_product_dense(i1: Int, i2: Int): Float = {
    var dot = 0.0F
    var i = 0
    while ( i < d) {
      dot += dense_points(i1)(i) * dense_points(i2)(i)
      i += 1
    }
    dot
  }

  // 26c
  def init_dotproduct_func(): (Int, Int) => Float = {
    if (is_sparse_data && is_binary)
      dot_product_sparse_binary
    else if (is_sparse_data && !is_binary)
      dot_product_sparse_nonbinary
    else if (!is_sparse_data)
      dot_product_dense
    else
      (_:Int, _:Int) => ???
  }


  // 28b
  def rbf_kernel(i1: Int, i2: Int): Float = {
    var s = dot_product_func(i1, i2)
    s *= -2
    s += precomputed_self_dot_product(i1) +
      precomputed_self_dot_product(i2)
    Math.exp(-s / two_sigma_squared).toFloat
  }

  // 28a
  def init_kernel_func(): (Int, Int) => Float = {
    if (is_linear_kernel)
      dot_product_func
    else if (!is_linear_kernel)
      rbf_kernel
    else
      (_:Int, _:Int) => ???
  }

  // 29b
  def init_precomputed_self_dot_product(): Unit = {
    if (!is_linear_kernel) {
      //if (debug) println("init_precomputed_self_dot_product")
      //precomputed_self_dot_product.resize(N)
      precomputed_self_dot_product = Vec.fill(N)(0.0f) // TODO: remove
      var i = 0
      while ( i < N ) {
        precomputed_self_dot_product(i) = 0.0f // TODO: remove
        precomputed_self_dot_product(i) = dot_product_func(i, i)
        i += 1
      }
    }
  }

  /*
   * Args
   */

  def parseArgs(args: Array[String]): Unit = {
    var errflg = 0
    var optind = 0
    val argc = args.size

    val argi = args.toIterator
    while (argi.hasNext){
      val arg = argi.next()
      optind +=1
      arg match {
        case "-n" =>
          N = argi.next().toInt
          optind +=1

        case "-d" =>
          d = argi.next().toInt
          optind +=1

        case "-c" =>
          C = argi.next().toFloat
          optind +=1

        case "-t" =>
          tolerance = argi.next().toFloat
          optind +=1

        case "-e" =>
          eps = argi.next().toFloat
          optind +=1

        case "-p" =>
          two_sigma_squared = argi.next().toFloat
          optind +=1

        case "-f" =>
          data_file_name = File(argi.next())
          optind +=1

        case "-m" =>
          svm_file_name = File(argi.next())
          optind +=1

        case "-o" =>
          output_file_name = File(argi.next())
          optind +=1

        case "-r" =>
          random = new Random(argi.next().toInt)
          optind +=1

        case "-l" =>
          is_linear_kernel = true

        case "-s" =>
          is_sparse_data = true

        case "-b" =>
          is_binary = true

        case "-a" =>
          is_test_only = true

        case "-libsvm" =>
          is_libsvm_format = true

        case "-scale" =>
          val tmp: Array[String] = argi.next().split(",")
          optind +=1
          lower = tmp(0).toFloat
          upper = tmp(1).toFloat
          is_libsvm_scale = true

        case "-i" =>
          maxIter = argi.next().toInt
          optind +=1

        case "?" =>
          errflg += 1
      }
    }

    if ((errflg > 0) || (optind < argc)) {
      print(s"""
      |  usage: ${args(0)}
      | -f data_file_name
      | -m svm_file_name
      | -o output_file_name
      | -n N
      | -d d
      | -c C
      | -t tolerance
      | -e epsilon
      | -p two_sigma_squared
      | -r random_seed
      | -l (is_linear_kernel)
      | -s (is_sparse_data)
      | -b (is_binary)
      | -a (is_test_only)
      | -libsvm (is_libsvm_format)
      | -scale (scale_min,scale_max)
      |      """.stripMargin)
      System.exit(2)
    }
  }

  /*
    Read data
   */


  // Dense format
  // attribute_1_value attribute_2_value ... attribute_d_value target_value = {+1, -1}

  // Sparse format (id_j should be between 1 and d)
  // id_1 val_1 id_2 val_2 ... id_m val_m target_value

  // Sparse binary format (id_j should be between 1 and d)
  //id_1 id_2 ... id_m target_value

  import better.files.File
  import better.files.Dsl.SymbolicOperations

  // 31c
  def readData(data_file: File): Unit = {
    var n = 0
    if (is_test_only) {
      if (debug) println(s"Reading model from: $svm_file_name")
      // Reads in model
      n = read_svm(svm_file_name)
      end_support_i = n
      first_test_i = n
      N += n
    }
    /* TODO: remove
    if (N > 0) {
      // target.reserve(N)
      var i = 0
      while (i < N) {
        target += 0
        i += 1
      }
      if (is_sparse_data && is_binary){
        // sparse_binary_points.reserve(N)
        i = 0
        while (i < N) {
          sparse_binary_points += new sparse_binary_vector()
          i += 1
        }
      }
      else if (is_sparse_data && !is_binary){
        // sparse_points.reserve(N)
        i = 0
        while (i < N) {
          sparse_points += new sparse_vector()
          i += 1
        }
      }
      else {
        // dense_points.reserve(N)
        i = 0
        while (i < N) {
          dense_points += Vec()
          i += 1
        }
      }
    }*/
    if (debug) println(s"Reading data from: $data_file")
    // Reads in data
    n = read_data(data_file)
    if (is_test_only) {
      N = first_test_i + n
    }
    else {
      N = n
      first_test_i = 0
      end_support_i = N
    }
  }

  // 32
  def read_data(file: File): Int = {
    val id = file.lineIterator
    read_data(is_sparse_data, is_libsvm_format, is_binary, id)
  }

  // 32
  def read_data(is_sparse_data: Boolean, is_libsvm_format: Boolean, is_binary: Boolean, id: Iterator[String]): Int = {
    var s = ""
    var n_lines = 0

    while ( id.hasNext ) {
      s = id.next()
      n_lines += 1
      val lineTmp = if (is_libsvm_format) s.split("\\s+|[:]") else s.split("\\s+")
      val line = if (is_libsvm_format) {
        val h = lineTmp.head
        val yl = lineTmp.drop(1)
        yl :+ h
      } else
        lineTmp
      val v: Vec[Float] = line.map(_.toFloat).to[Vec]
      val y = v.last.toInt
      val yy = if ((y == 0) || (y == -1) ) -1 else if (y == 1) +1 else ???
      target += yy
      // remove label
      val n = v.size - 1
      if (is_sparse_data && is_binary) {
        val x = new sparse_binary_vector()
        var i = 0
        while (i < n) {
          if (v(i) < 1 || v(i) > d) {
            println(s"data file error (1): line ${n_lines+1} : attribute index ${v(i).toInt} out of range.")
            System.exit(1)
          }
          x.id += (v(i).toInt - 1)
          i += 1
        }
        sparse_binary_points += x
      }
      else if (is_sparse_data && !is_binary) {
        val x = new sparse_vector()
        var i = 0
        while ( i < n ) {
          if (v(i) < 1 || v(i) > d) {
            println(s"data file error (2): line ${n_lines+1} : attribute index ${v(i).toInt} out of range.")
            System.exit(1)
          }
          x.id += (v(i).toInt - 1)
          x.value += v(i+1)
          i += 2
        }
        sparse_points += x
      }
      else {
        if (n != d) {
          println(s"data file error (3): line ${n_lines+1} has $n attributes; should be d =$d")
          System.exit(1)
        }
        // TODO: BUG ! we need to remove the indices. Use loop as above
        dense_points += v
      }
    }
    n_lines
  }

  // 35
  def read_svm(file: File): Int = {
    // TODO: missing - tolerance
    // TODO: missing - eps
    // TODO: missing - two_sigma_squared

    val is = file.lineIterator
    d = is.next().toInt
    //is_sparse_data = is.next().toBoolean
    //is_binary = is.next().toBoolean
    is_linear_kernel = is.next().toBoolean
    C = is.next().toFloat
    b = is.next().toFloat
    is_libsvm_format = is.next().toBoolean
    is_libsvm_scale = is.next().toBoolean
    lower = is.next().toFloat
    upper = is.next().toFloat
    if (is_libsvm_scale) {
      minAtributes = Vec.fill(d)(0.0f)
      maxAtributes = Vec.fill(d)(0.0f)
      var i = 0
      while (i < d){
        val min_max = is.next().split("\\s+")
        minAtributes(i) = min_max(0).toFloat
        maxAtributes(i) = min_max(1).toFloat
        i += 1
      }
    }
    if (is_linear_kernel) {
      w = new Vec(d)
      var i = 0
      while (i < d){
        w(i) = is.next().toFloat
        i += 1
      }
    }
    else {
      two_sigma_squared = is.next().toFloat
      val n_support_vectors = is.next().toInt
      alph = Vec.fill(n_support_vectors)(0.0F)
      var i = 0
      while (i < n_support_vectors){
        alph(i) = is.next().toFloat
        i += 1
      }
    }

    if (debug) println(s"d = $d")
    if (debug) println(s"is_sparse_data = $is_sparse_data")
    if (debug) println(s"is_binary = $is_binary")
    if (debug) println(s"is_linear_kernel = $is_linear_kernel")
    if (debug) println(s"C = $C")
    if (debug) println(s"b = $b")
    if (debug) println(s"is_libsvm_format = $is_libsvm_format")
    if (debug) println(s"is_libsvm_scale = $is_libsvm_scale")
    if (debug) println(s"lower = $lower")
    if (debug) println(s"upper = $upper")
    read_data(false, false, false, is)
  }

  // 34
  def write_svm(os: File): Unit = {

    // TODO: missing - tolerance
    // TODO: missing - eps
    // TODO: missing - two_sigma_squared

    os < ""  // need to start from beginning of file
    os << d.toString
    //os << is_sparse_data.toString
    //os << is_binary.toString
    os << is_linear_kernel.toString
    os << C.toString
    os << b.toString
    os << is_libsvm_format.toString
    os << is_libsvm_scale.toString
    os << lower.toString
    os << upper.toString
    if (is_libsvm_scale){
      var i = 0
      while (i<d){
        os.append(minAtributes(i).toString + " ")
        os.append(maxAtributes(i).toString + " ")
        os << ""
        i += 1
      }
    }
    if (is_linear_kernel) {
      var i = 0
      while (i<d){
        os << w(i).toString
        i += 1
      }
    }
    else {
      os << two_sigma_squared.toString
      var n_support_vectors = 0
      var i = 0
      // count the number of support vectors
      while (i<end_support_i){
        if (alph(i) > 0)
          n_support_vectors += 1
        i += 1
      }
      // record the number of support vectors
      os << n_support_vectors.toString
      i = 0
      // record the support vectors' alphas
      while (i < end_support_i){
        if (alph(i) > 0)
          os << alph(i).toString
        i += 1
      }
      // record the support vectors
      i = 0
      var current = 0
      while (i<end_support_i){
        if (alph(i) > 0) {
          if (is_sparse_data && is_binary) {
            var j = 0
            while (j < sparse_binary_points(i).id.size ){
              //os << (sparse_binary_points(i).id(j) + 1).toString << " "
              os.append( (sparse_binary_points(i).id(j) + 1).toString + " ")
              j += 1
            }
          }
          else if (is_sparse_data && !is_binary) {
            var j = 0
            while (j<sparse_points(i).id.size){
              //os << (sparse_points(i).id(j)+1).toString <<
              //  " " << sparse_points(i).value(j).toString << " "
              os.append( (sparse_points(i).id(j)+1).toString +
                " " + sparse_points(i).value(j).toString + " ")
              j += 1
            }
          }
          else {
            var j = 0
            while (j<d){
              //os << dense_points(i)(j).toString << " "
              os.append( dense_points(i)(j).toString + " ")
              j += 1
            }
          }
          os << target(i).toString
          current += 1
        }
        i += 1
      }
    }
  }

  // 36a
  def writeModelParameters(): Unit = {
    if (!is_test_only && (svm_file_name.name != "")) {
      write_svm(svm_file_name)
    }
  }

  // 36b
  def error_rate(): Float = {
    var n_total = 0
    var n_error = 0
    var i = first_test_i
    while ( i<N ) {
      val y = target(i) > 0
      val y_hat = learned_func(i) > 0
      //if (debug) println(s"y = $y ? y_hat = $y_hat")
      if ( y_hat != y)
        n_error +=1
      n_total += 1
      i += 1
    }
    // TODO: remove
    println(s"n_total = $n_total n_error = $n_error ")
    (n_error / n_total.toDouble).toFloat
  }

  // 36c
  def writeClassificationOutput(): Unit = {
    output_file_name < "" // clean previous input
    var i = first_test_i
    while ( i < N ) {
      output_file_name << learned_func(i).toString
      i += 1
    }
  }

  // 36d
  def printDiagnostics(): Unit = {
    if (debug) {
      println(s"numIter = $numIter")
      var s = 0.0F
      var i = 0
      while (i < N) {
        s += alph(i)
        i += 1
      }
      var t = 0.0F
      i = 0
      while (i < N) {
        var j = 0
        while (j < N) {
          t += alph(i) * alph(j) * target(i) * target(j) * kernel_func(i, j)
          j += 1
        }
        i += 1
      }
      println(s"Objective function = ${s - t / 2.0}")
      // Lagrange subject to:  alpha_i >= 0
      i = 0
      while (i < N) {
        if (alph(i) < 0)
          println(s"alph[$i] = ${alph(i)} < 0")
        i += 1
      }
      // Lagrange subject to:  Sum (alpha_i * y_i) = 0
      s = 0.0f
      i = 0
      while (i < N) {
        s += alph(i) * target(i)
        i += 1
      }
      println(s"s = $s")
      print(s"error_rate = ${error_rate()}  ")
    }
    // Lagrange subject to:  all 0 <= alpha_i <= C
    var non_bound_support = 0
    var bound_support = 0
    var i = 0
    while (i < N) {
      if (alph(i) > 0) {
        if (alph(i) < C)
          non_bound_support += 1
        else
          bound_support += 1
      }
      i += 1
    }
    print(s"non_bound = $non_bound_support  ")
    print(s"bound_support = $bound_support  ")
    println(s"sparsity = ${(bound_support + bound_support)*1.0 / N}")
    println(s"C = $C")
    println(s"two_sigma_squared = $two_sigma_squared")
  }

  // 37 not referenced

  // 24a
  def updateLagrangeMultipliers(i1: Int, y1: Float, a1:Float, alph1:Float,
                                i2: Int, y2: Float, a2:Float, alph2:Float, delta_b: Float): Unit = {
    val t1 = y1 * (a1 - alph1)
    val t2 = y2 * (a2 - alph2)
    var i = 0
    while ( i < end_support_i ) {
      if (0 < alph(i) && alph(i) < C)
        error_cache(i) += t1 * kernel_func(i1, i) + t2 * kernel_func(i2, i) - delta_b
      i += 1
    }
    error_cache(i1) = 0.0f
    error_cache(i2) = 0.0f
  }

  /*
   * Extras
   */


  def minmax_sparse_binary(): (Vec[Float], Vec[Float]) = {
    val maxs = Vec.fill(d)(1.0f)
    val mins = Vec.fill(d)(0.0f)
    (mins, maxs)
  }

  def minmax_sparse_nonbinary(): (Vec[Float], Vec[Float]) = {
    val maxs = Vec.fill(d)(Float.MinValue)
    val mins = Vec.fill(d)(Float.MaxValue)
    var k = 0
    while (k < N) {
      var i = 0
      while (i < sparse_points(k).id.size) {
        val j = sparse_points(k).id(i)
        val v = sparse_points(k).value(i)
        maxs(j) = Math.max( maxs(j), v)
        mins(j) = Math.min( mins(j), v)
        i += 1
      }
      k += 1
    }
    (mins, maxs)
  }

  def minmax_dense(): (Vec[Float], Vec[Float]) = {
    val maxs = Vec.fill(d)(Float.MinValue)
    val mins = Vec.fill(d)(Float.MaxValue)
    var k = 0
    while (k < N){
      var i = 0
      while (i < d) {
        maxs(i) = Math.max( maxs(i), dense_points(k)(i))
        mins(i) = Math.min( mins(i), dense_points(k)(i))
        i += 1
      }
      k += 1
    }
    (mins, maxs)
  }


  def init_minMaxAttributes(): () => (Vec[Float], Vec[Float]) = {
    if (is_sparse_data && is_binary)
      minmax_sparse_binary _

    else if (is_sparse_data && !is_binary)
      minmax_sparse_nonbinary _

    else if (!is_sparse_data )
      minmax_dense _

    else
      // Break
      () => ???
  }



  def scale(min: Float, max: Float, value: Float) : Float = {
    /* skip single-valued attribute */
    if(min == max)
      value
    else {

      if(value == min)
        lower
      else if(value == max)
        upper
      else
        lower + (((upper-lower) * (value-min)) / (max-min))
    }

  }


  def scale_dense(maxs: Vec[Float], mins: Vec[Float]): Unit = {
    var k = first_test_i
    while (k < N){
      var i = 0
      while (i < d) {
        dense_points(k)(i) = scale( mins(i), maxs(i), dense_points(k)(i))
        i += 1
      }
      k += 1
    }
  }

  def init_scaleAttributes(): (Vec[Float], Vec[Float]) => Unit = {
    if (is_sparse_data && is_binary)
      // Cannot scale, must convert to dense then make sparse again
      ???

    else if (is_sparse_data && !is_binary)
     // Cannot scale, must convert to dense then make sparse again
      ???

    else if (!is_sparse_data )
      scale_dense

    else
    // Break
      (_,_) => ???
  }


  def dense_sparse_binary(dense: Vec[dense_vector]): Unit = {
    var k = first_test_i
    var l = 0
    while (k < N) {
      var i = 0
      while ( i < d ){
        dense(k)(i) = 0.0f
        i += 1
      }
      i = 0
      while ( i < sparse_binary_points(l).id.size ){
        dense(k)(sparse_binary_points(l).id(i)) = 1f
        i += 1
      }
      //println(s"${target(k)} ${dense(k).mkString(",")}")
      k += 1
      l += 1
    }
  }

  def dense_sparse_nonbinary(dense: Vec[dense_vector]): Unit = {
    var k = first_test_i
    var l = 0
    while (k < N) {
      var i = 0
      while ( i < d ){
        dense(k)(i) = 0.0f
        i += 1
      }
      i = 0
      while (i < sparse_points(l).id.size) {
        val j = sparse_points(l).id(i)
        val v = sparse_points(l).value(i)
        dense(k)(j) = v
        i += 1
      }
      //println(s"l = $l : target($k) = ${target(k)} ${dense(k).mkString(",")}")
      k += 1
      l += 1
    }
  }


  def toDense(): Unit = {
    // Target does not change
    // target = Vec()

    val n = N - first_test_i + 1
    if (is_sparse_data && is_binary){
      dense_points = dense_points ++ Vec.fill(n,d)(0.0f)
      dense_sparse_binary(dense_points)

    } else if (is_sparse_data && !is_binary) {
      dense_points = dense_points ++ Vec.fill(n,d)(0.0f)
      dense_sparse_nonbinary(dense_points)
    }
    else if (!is_sparse_data )
      ()

    else
      ???

    is_binary = false
    is_sparse_data = false
  }


  def scale(): Unit = {
    val scaleAttributes = init_scaleAttributes()
    scaleAttributes(maxAtributes, minAtributes)
  }

  def printDense(): Unit = {
    var k = first_test_i
    while (k < N){
      println(s"${target(k)} ${dense_points(k).mkString(",")}")
      k += 1
    }
  }

  def printAllDense(): Unit = {
    var k = 0
    while (k < N){
      if (k == first_test_i) println("Test data")
      println(s"${target(k)} ${dense_points(k).mkString(",")}")
      k += 1
    }
  }

  def printModelDense(): Unit = {
    var k = 0
    while (k < end_support_i){
      println(s"${target(k)} ${dense_points(k).mkString(",")}")
      k += 1
    }
  }

  def printAlphai(): Unit = {
    var i = 0
    while ( i < end_support_i) {
      if (alph(i) > 0)
        println(s"alph($i) = ${alph(i)}" )
      i += 1
    }
  }

  def printSparseVector(sv: sparse_vector): Unit = {
    val v = Vec.fill(d)(0.0f)
    sv.id.indices.foreach{ i =>
      v(sv.id(i)) = sv.value(i)
    }
    println(v.mkString(","))
  }


  def printSparse(): Unit = {
    sparse_points.foreach{
      e => printSparseVector(e)
    }
  }

  def checKernel(k: Int): Unit = {
    var i = 0
    while ( i < end_support_i) {
      //if (k-first_test_i == i){
      //val tmp = kernel_func(i, k)
      val tmp = dot_product_func(i, k)
      println(s"kernel_func($i, $k) = $tmp")
      //}
      i += 1
    }
  }

  def checkKernels(): Unit = {
    var i = 0
    while ( i < N) {
      checKernel(i)
      i += 1
    }
  }

  def main(args: Array[String]): Unit = {
    //import better.files.Dsl._
    //val trainData = cwd / ".." / "data/libsvm/svmguide1"
    ////val testData = cwd / ".." / "data/libsvm/svmguide1.t"
    ////val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    //println(trainData)

    // 29d
    if (debug) println("Parsing args")
    parseArgs(args)
    // 31c
    if (debug) println("read data")
    readData(data_file_name)
    // https://docs.google.com/document/d/1x0A1nUz1WWtMCZb5oVzF0SVMY7a_58KQulqQVT8LaVA/edit

    //printModelDense()
    //printSparse()
    toDense()
    //printDense()
    //printAllDense()
    if (debug) println(s"N = $N, d = $d")
    if (debug) println(s"end_support_i = $end_support_i, first_test_i = $first_test_i")

    if (is_libsvm_scale) {
      val minMax = init_minMaxAttributes()
      if (!is_test_only){
        val (minAtributesT, maxAtributesT) = minMax()
        minAtributes = minAtributesT
        maxAtributes = maxAtributesT
      }
      scale()
      if (debug) println(s"mins: ${minAtributes.mkString(",")}")
      if (debug) println(s"maxs: ${maxAtributes.mkString(",")}")
    }
    //printDense()
    //printAllDense()

    init_precomputed_self_dot_product()
    if (!is_test_only)
      fit()
    else
      predict()
  }

}

