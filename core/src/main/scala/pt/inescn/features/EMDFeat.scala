package pt.inescn.features

import inegi.utils.{Frmng, Read_Data_File}
import pt.inescn.dsp.{EMD, JEmd}
import pt.inescn.plot.JPlot

import scala.collection.mutable.ListBuffer

object EMDFeatTest{

  // MAIN FUNCTION
  //=======================================================================================
  def main(args: Array[String]): Unit = {

    // variaveis constantes
    //val Path="E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv2/rol1_1krpm_0hp_norm_ens1.csv"
    val Path="E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv/Rol1_1000_CmDn_Ens1.csv"
    val PathDst= "E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv/Rol1_1000_CmDn_Ens1_test.csv";
    var FrmSz=256
    var FrmStp=32

    // Reads the data set
    //---------------------
    val rdf = new Read_Data_File(Path, 1, 1)
    /*for( i <- 0 to rdf.Table.size()-1 ) {
      for (j <- 0 to rdf.Hdr.length-1)
        print(rdf.Table.get(i)(j) + " ")
      print("\n")
    }*/

    var Sgnl= new Array[Double](rdf.Table.size())


    for ( i <- 0 to rdf.Table.size()-1 )
      Sgnl(i)=rdf.Table.get(i)(2)


    /*for( i <- 0 to Sgnl.length-1)
      println(Sgnl(i))*/


    //Performs framming
    //---------------------
    //Windowing
    val wndw= Array.fill[Double](FrmSz)(1) //Rectangular window(default)
    val ff= new Frmng
    val frm=ff.FrmngFunc(Sgnl,FrmSz,FrmStp,wndw)

    /*for( i <- 0 to frm.length-1){
      for( j <- 0 to frm(0).length-1)
         print( frm(i)(j) +" ")
      print("\n")
    }*/


    //Computes the  LFCC
    //---------------------
    val Fs=5100
    val NmbFltrs=5
    val MaxFrq=2550
    val MinFrq=10
    val NmbCpCoe=5
    val EMDFeatclss=EMDFeat(20,1000,1)


    for(i<- 0 until frm.length) {
      val emdFeat = EMDFeatclss.comp( frm(i) , "EMDEng",500,10)
      println(emdFeat.toVector)
    }


    /*for(k <- 0 to emdFeat.length-1)
      print( emdFeat(k) + " " )

    //Hdr={'c1','c2','c3','c4','c5'}
    //val wdf = new Write_Data_File(PathDst, rdf.Hdr, rdf.Table)
  }*/

}


 /* var i = 0
  while ( {
    i < data.length
  }) {
    System.out.print(data(i) + ";")
    var j = 0
    while ( {
      j < order
    }) System.out.print(emdData.imfs(j)(i) + ";") {
      j += 1; j - 1
    }
    System.out.println()

    {
      i += 1; i - 1
    }
  }*/

}


case class EMDFeat(order:Int,Nitera:Int,loc:Int){


  def comp(Signal: Array[Double], feat: String , MaxItera:Int, NmbrOfComp:Int): Array[Double] = {

    val EMDFeatclss= EMD( MaxItera, NmbrOfComp)
    val imfs = EMDFeatclss.imfs(Signal)


    /*var x = (0 until Signal.length ).toArray.map(_.toDouble)
    val fig = new JPlot(""); fig.figure()
    fig.wiggle(x,imfs)*/


    var out= new Array[Double](imfs.length)

    val outt=feat match {
      case "EMDEnt" => {

        var i = 0
        var sumall = 0.0
        while (i < imfs.length) {
          var j = 0
          var sum = 0.0
          while (j < imfs(i).length) {
            sum += Math.pow(imfs(i)(j), 2)
            j += 1
          }
          out(i) = sum
          sumall += sum
          i += 1
        }

        i = 0
        while (i < imfs.length) {
          out(i) /= sumall
          out(i) = -out(i) * Math.log(out(i))
          i += 1
        }

      }

      case "EMDSV" => {

        //var out = breeze.linalg.CompleteSVD(imfs)

      }

      case "EMDEng" =>{
        var i = 0
        var sumall = 0.0
        while (i < imfs.length) {
          var j = 0
          var sum = 0.0
          while (j < imfs(i).length) {
            sum += Math.pow(imfs(i)(j), 2)
            j += 1
          }
          out(i) = sum
          sumall += sum
          i += 1
        }
      }
    }

    out
  }

}


