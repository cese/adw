package pt.inescn.features

import org.hipparchus.transform.FastFourierTransformer
import org.hipparchus.transform.DftNormalization
import org.hipparchus.transform.TransformType


object LFCCTest {

  // MAIN FUNCTION
  //=======================================================================================
  /*def main(args: Array[String]): Unit = {

    // variaveis constantes
    //val Path="E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv2/rol1_1krpm_0hp_norm_ens1.csv"
    val Path="E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv/Rol1_1000_CmDn_Ens1.csv"
    val PathDst= "E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv/Rol1_1000_CmDn_Ens1_test.csv";
    var FrmSz=64
    var FrmStp=32

    // Reads the data set
    //---------------------
    val rdf = new Read_Data_File(Path, 1, 1)
    /*for( i <- 0 to rdf.Table.size()-1 ) {
      for (j <- 0 to rdf.Hdr.length-1)
        print(rdf.Table.get(i)(j) + " ")
      print("\n")
    }*/

    var Sgnl= new Array[Double](rdf.Table.size())


    for ( i <- 0 to rdf.Table.size()-1 )
      Sgnl(i)=rdf.Table.get(i)(2)


    /*for( i <- 0 to Sgnl.length-1)
      println(Sgnl(i))*/


    //Performs framming
    //---------------------
    //Windowing
    val wndw= Array.fill[Double](FrmSz)(1) //Rectangular window(default)
    val ff= new Frmng
    val frm=ff.FrmngFunc(Sgnl,FrmSz,FrmStp,wndw)

    /*for( i <- 0 to frm.length-1){
      for( j <- 0 to frm(0).length-1)
         print( frm(i)(j) +" ")
      print("\n")
    }*/


    //Computes the  LFCC
    //---------------------
    val Fs=5100
    val NmbFltrs=5
    val MaxFrq=2550
    val MinFrq=10
    val NmbCpCoe=5
    val LFCCclss=LFCC(Fs,NmbFltrs,MaxFrq,MinFrq,NmbCpCoe,FrmSz)
    val lfcc=LFCCclss.LFCCcomp(frm(0))

    for(k <- 0 to lfcc.length-1)
      print( lfcc(k) + " " )

    //Hdr={'c1','c2','c3','c4','c5'}
    //val wdf = new Write_Data_File(PathDst, rdf.Hdr, rdf.Table)
  }*/

}



/**
  * run/main pt.inescn.features.LFCC
  */
case class LFCC(FsIn: Double,NmbFltrsIn:Int, MaxFrqIn:Double,  MinFrqIn:Double, NmbCpCoeIn:Int, FFTSzIn:Int ) {
  //Default initialization
  private val Fs = FsIn
  val NmbFltrs=NmbFltrsIn
  val MaxFrq=MaxFrqIn
  val MinFrq=MinFrqIn
  val NmbCpCoe=NmbCpCoeIn
  val FFTSz=FFTSzIn
  val PrEmphssFctr = -0.99
  var win=Array.fill[Double](FFTSz)(1)
  var abs_s = Array[Double](FFTSz)
  var Sfltr = new Array[Double](FFTSz)
  var CC=new Array[Double](NmbCpCoe)
  var fft=new FastFourierTransformer(DftNormalization.STANDARD)
  var Fltrs = Array.ofDim[Double](this.NmbFltrs,this.FFTSz)
  var dctMatrix = Array.ofDim[Double](NmbFltrs, NmbCpCoe)

  //Windowing
  for (i <- 0 to FFTSz -1)
    win(i)=0.5*(1-Math.cos(2*Math.PI*i/(FFTSz-1)))    //Hanning window

  Fltrs = Array.ofDim[Double](this.NmbFltrs,this.FFTSz)
  Fltrs = FltrBnk(Fs, NmbFltrs, FFTSz, MaxFrq, MinFrq); // Generates de filter bank
  fft = new FastFourierTransformer(DftNormalization.STANDARD)

  //DCT Matrix
  dctMatrix = Array.ofDim[Double](NmbFltrs, NmbCpCoe)
  for (i <- 0 to NmbFltrs - 1)
    for (j <- 0 to NmbCpCoe - 1)
      dctMatrix(i)(j) = Math.cos( (i+0.5) * (j*Math.PI/NmbFltrs) ) // DCT tipo II

  abs_s= Array[Double](FFTSz)
  Sfltr= new Array[Double](FFTSz)
  CC= new Array[Double](NmbCpCoe)

  //LFCC initialization
  //---------------------------------------------------
  //       LFCC- Linear Frequency Cepstral Coeficients
  //NmbFltrs -
  //Fs       -
  //MaxFrq   -
  //MinFrq   -
  //NmbCpCoe -
  //					OUTPUT
  //----------------------------------------------------

  def LFCCcomp(S:Array[Double]):Array[Double]= {

    //Windowing
    for(i <- 0 to FFTSz-1 )
      S(i)=S(i)*win(i)

    //Pre-emphase filtering
    for(i <- 0 to FFTSz-1 ){
      if(i==0)
        Sfltr(i)=S(0)
      if(i>0 & i<(FFTSz-1)  )
        Sfltr(i)= S(i) + PrEmphssFctr*S(i-1)
      if(i==(FFTSz-1))
        Sfltr(i)= PrEmphssFctr*S(FFTSz-1)
    }

    //FFT Magnitude
    val complexTsig1 = fft.transform(Sfltr, TransformType.FORWARD)
    abs_s = complexTsig1.map(_.abs())

    //Filtering, Energy and Log
    val Elog= new Array[Double](NmbFltrs)
    for (i <- 0 to NmbFltrs-1){
      var sum=0.0
      for (j <- 0 to FFTSz/2 - 1)
        sum+= Math.pow(abs_s(j),2)*Fltrs(i)(j)
      Elog(i)=Math.log(sum)
    }

    //DCT transformation
    val CCfrm = Array.fill[Double](NmbCpCoe)(0)
    for(i <- 0 to NmbCpCoe-1 )
      for(j <- 0 to NmbFltrs-1 )
        CC(i) += dctMatrix(j)(i)*Elog(j)

    CC
  }


  //-----------
  def FltrBnk( Fs:Double, NmbFltrs:Int, FFTSz:Int,  MaxFrqi:Double,  MinFrqi:Double): Array[Array[Double]]={

    val MaxFrq=MaxFrqi*FFTSz/Fs
    val MinFrq=MinFrqi*FFTSz/Fs
    val Lnght=2*(MaxFrq-MinFrq)/(NmbFltrs+1)

    val LowFrq= new Array[Double](NmbFltrs)
    val HigFrq= new Array[Double](NmbFltrs)
    //val Fltrs = Array.ofDim[Double](NmbFltrs,FFTSz)

    var v=MinFrq
    for(i <- 0 to NmbFltrs-1 ){
      LowFrq(i) = v
      HigFrq(i) = LowFrq(i) + Lnght
      v += (Lnght / 2)
    }

    for( i <- 0 to NmbFltrs-1 )
      Fltrs(i) = TrnglrMsk(LowFrq(i), HigFrq(i), Lnght, FFTSz / 2)

    Fltrs
  }


  //----------------------------------------------
  def TrnglrMsk(lk:Double ,hk:Double ,ln:Double,N:Int):Array[Double]={

    val out= new Array[Double](N)
    for (k <- 0  to N-1) {

      if (k <= lk)
        out(k) = 0

      if (k > lk && k <= lk + ln / 2)
        out(k)= 2/ln*(k-lk)

      if (k > lk + ln / 2 && k <= hk)
        out(k)= -2/ln*(k - hk)

      if( k>hk )
        out(k)=0
    }

    out
  }

}


