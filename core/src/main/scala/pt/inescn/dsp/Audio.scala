/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import java.io.BufferedOutputStream
import java.nio.ByteOrder

import javax.sound.sampled.{Mixer, _}
import better.files.File

/**
  * This object contains various uituility functions that allow us to load and use an audio file.
  * Only the container and encoders supported by javax.sound are supported.
  *
  * Created by hmf on 04-07-2017.
  *
  * @see https://github.com/JorenSix/TarsosDSP
  *      (https://github.com/librosa/librosa)
  * @see http://openimaj.org/
  * @see http://www.softsynth.com/
  *
  */
object Audio {

  /**
    * Converts two bytes into an Integer (The system's endianess is Little)
    * Little Endian - smallest address of array are the least significant bytes of the integer
    * Big Endian - smallest address of array are the most significant bytes of the integer
    *
    * https://stackoverflow.com/questions/4768933/read-two-bytes-into-an-integer
    * https://stackoverflow.com/questions/9810010/scala-library-to-convert-numbers-int-long-double-to-from-arraybyte
    *
    * NOTE: this function seems to have problems with negative values, TODO: add tests
    *
    * @param b0 first of two bytes that represent the Short
    * @param b1 second of two bytes that represent the Short
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toInt(b0: Byte, b1: Byte, isBigEndian: Boolean): Int = {
    toShort(Array[Byte](b0, b1), isBigEndian).toInt
  }

  /**
    * Converts the first two elements of the `data` to an [[scala.Short]]
    * @see [[https://docs.oracle.com/javase/8/docs/api/java/lang/Short.html java.lang.Short.BYTES]]
    * @param data of two bytes that represent the [[scala.Short]]
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toShort(data: Array[Byte], isBigEndian: Boolean) : Short = {
    import java.nio.ByteBuffer
    import java.nio.ByteOrder

    val byteBuffer = ByteBuffer.allocateDirect( java.lang.Short.BYTES )
    // by choosing big endian, high order bytes must be put to the buffer before low order bytes
    if (isBigEndian) byteBuffer.order(ByteOrder.BIG_ENDIAN) else byteBuffer.order(ByteOrder.LITTLE_ENDIAN)
    data.foreach( byteBuffer.put )
    byteBuffer.flip
    byteBuffer.getShort
  }

  /**
    * Converts the first four elements of the `data` to an [[scala.Int]]
    * @see [[https://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html java.lang.Integer.BYTES]]
    *
    * @param data of four bytes that represent the [[scala.Int]]
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toInt(data: Array[Byte], isBigEndian: Boolean) : Int = {
    import java.nio.ByteBuffer
    import java.nio.ByteOrder

    val byteBuffer = ByteBuffer.allocateDirect( java.lang.Integer.BYTES )
    // by choosing big endian, high order bytes must be put to the buffer before low order bytes
    if (isBigEndian) byteBuffer.order(ByteOrder.BIG_ENDIAN) else byteBuffer.order(ByteOrder.LITTLE_ENDIAN)
    data.foreach( byteBuffer.put)
    byteBuffer.flip
    byteBuffer.getInt
  }

  /**
    * Converts the first eight elements of the `data` to a [[scala.Long]]
    * @see [[https://docs.oracle.com/javase/8/docs/api/java/lang/Long.html java.lang.Long.BYTES]]
    *
    * @param data of eight bytes that represent the [[scala.Long]]
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toLong(data: Array[Byte], isBigEndian: Boolean) : Int = {
    import java.nio.ByteBuffer
    import java.nio.ByteOrder

    val byteBuffer = ByteBuffer.allocateDirect( java.lang.Long.BYTES )
    // by choosing big endian, high order bytes must be put to the buffer before low order bytes
    if (isBigEndian) byteBuffer.order(ByteOrder.BIG_ENDIAN) else byteBuffer.order(ByteOrder.LITTLE_ENDIAN)
    data.foreach( byteBuffer.put )
    byteBuffer.flip
    byteBuffer.getInt
  }


  /**
    * Converts an array of bytes to a double value. The size of the
    * array is used to determine what integral type is being ([[scala.Short]],
    * [[scala.Int]] or [[scala.Long]]). These ae then converted to a [[scala.Double]]
    *
    * @param data that represents an integral value
    * @param isBigEndian indicate if we are to assume big or little endian order
    * @return
    */
  def toDouble(data: Array[Byte], isBigEndian: Boolean) : Double = {
    data.length match {
      case java.lang.Byte.BYTES => data(0).toDouble
      case java.lang.Short.BYTES => toShort(data, isBigEndian).toDouble
      case java.lang.Integer.BYTES => toInt(data, isBigEndian).toDouble
      case java.lang.Long.BYTES => toLong(data, isBigEndian).toDouble
    }

  }

  /**
    * Returns an integer representation assuming the `arr` contains the bytes in Big Endian format.
    * Big Endian - smallest address of array are the most significant bytes of the integer
    *
    * @param arr - array with the bytes to decode
    * @param off - offset from which to decode the byt array
    * @return
    */
  // Converting directly to integer causes problems with the sign
  //def getIntBigEndian(arr: Array[Byte], off: Int = 0): Int = arr(off) << 8 & 0xFF00 | arr(off + 1) & 0xFF
  def getIntBigEndian(arr: Array[Byte], off: Int = 0): Int = ((arr(off) << 8) | (arr(off + 1) & 0xFF)).toShort.toInt
  /**
    * Returns an integer representation assuming the `arr` contains the bytes in Little Endian format.
    * Little Endian - smallest address of array are the least significant bytes of the integer
    *
    * @param arr - array with the bytes to decode
    * @param off - offset from which to decode the byt array
    * @return
    */
  // Converting directly to integer causes problems with the sign
  //def getIntLittleEndian(arr: Array[Byte], off: Int = 0): Int = arr(off+1) << 8 & 0xFF00 | arr(off) & 0xFF
  //def getIntLittleEndian(arr: Array[Byte], off: Int = 0): Short = (arr(off+1) << 8 | (arr(off) & 0xFF)).toShort
  def getIntLittleEndian(arr: Array[Byte], off: Int = 0): Int = (arr(off+1) << 8 | (arr(off) & 0xFF)).toShort.toInt


  /**
    * This function takes a audio frame and splits it into `numChannels` samples. Each samplesTo has a size
    * of `bytesPerChannelSample` bytes. The samplesTo bytes are composed to form a numerical value. Each
    * samplesTo is then converted to a [[scala.Double]] to facilitate processing. Note that the `frame` data
    * has to be a multiple of `bytesPerChannelSample` samples and must have `numChannels` of those samples.
    *
    * @see https://stackoverflow.com/questions/26824663/how-do-i-use-audio-sample-data-from-java-sound
    *
    * @param bytesPerChannelSample - number of bytes per channel
    * @param isBigEndian - whether the audio data is using big or little Endian.
    * @param frame - a single frame of audio data
    * @return
    */
  def getFrameDataPerChannel(bytesPerChannelSample : Int, isBigEndian : Boolean)(frame : Array[Byte]): Array[Double] = {
    val left_right: Iterator[Double] = frame.grouped(bytesPerChannelSample).map(pp => toDouble(pp, isBigEndian) )
    left_right.toArray
  }

  /**
    * Opens an audio file and returns an
    * [[https://docs.oracle.com/javase/8/docs/api/javax/sound/sampled/AudioInputStream.html javax.sound.sampled.AudioInputStream]]
    * that gives one access to the data. If the file does not exist or the
    * format is not readable, a [[scala.None]] is returned.
    *
    * @param sample - file with audio samples
    * @return
    */
  def open(sample : File): Option[(AudioFileFormat, AudioInputStream)] = {
    if (sample.exists) {
      val f = sample.toJava
      val format: AudioFileFormat = AudioSystem.getAudioFileFormat(f)
      val data: AudioInputStream = AudioSystem.getAudioInputStream(f)
      Some((format, data))
    }
    else
      None
  }

  def printSystemInformation(): Unit = {
    println(s"Byte.SIZE = ${java.lang.Byte.SIZE}")
    println(s"Integer.SIZE = ${Integer.SIZE}")
    println(s"Long.SIZE = ${java.lang.Long.SIZE}")
    println(s"System Endianess = ${ByteOrder.nativeOrder()}")  // ByteOrder.LITTLE_ENDIAN
  }

  def printFileFormat(audioData : Option[(AudioFileFormat, AudioInputStream)]): Unit = {
    val types = AudioSystem.getAudioFileTypes
    val lst = types.mkString(",")
    println( s"Supported audio file formats (containers) (${types.length}) = $lst" )

    audioData match {
      case None =>
        println("No file found")
      case Some((format, data)) =>
        println(s"File format: ${format.toString}")
        println(s"Byte Length (includes header) = ${format.getByteLength}")
        println(s"Frame Length* = ${data.getFrameLength}")
        println(s"Nº channels* = ${data.getFormat.getChannels}")
        println(s"Encoding= ${data.getFormat.getEncoding}")
        println(s"Frame rate (Hz)* = ${data.getFormat.getFrameRate}")
        println(s"Frame size (bytes) = ${data.getFormat.getFrameSize}")
        println(s"Sample rate (Hz) = ${data.getFormat.getSampleRate}")
        println(s"Sample size (bits)= ${data.getFormat.getSampleSizeInBits}")
        println(s"Is big endian = ${data.getFormat.isBigEndian}")
        println(s"Total bytes per channel ok ? ${format.getByteLength % data.getFormat.getChannels == 0}")
    }
  }

  /**
    * This function loads an audio file and stores all the channel data. If a file exists and can
    * be parsed we return the data and the number of frames read. If not, a [[scala.None]] is
    * returned. Note that a matrix is returned for the data. The first index is for the channel read,
    * and the second is for the channels samplesTo.
    *
    * @param sample - audio file name
    * @return - [[scala.Option]] with a tuple containing the channel data and the number of frames read.
    */
  def load(sample : File): Option[(Array[Array[Double]], Int)] = {
    val audioData: Option[(AudioFileFormat, AudioInputStream)] = Audio.open(sample)
    audioData match {
      case None => None
      case Some((_, data)) =>

        val numChannels = data.getFormat.getChannels
        val bytesPerFrame = data.getFormat.getFrameSize
        val bytesPerChannelSample = data.getFormat.getFrameSize / numChannels

        // Get the number of frames that were read into the file
        // IMPORTANT: data.getFrameLength is correct, format.getByteLength seems to
        // have a few extra bytes This may include the additional header data
        val channels = Array.ofDim[Double](numChannels, data.getFrameLength.toInt)
        data.getFrameLength
        // Index of channels
        val i = 0 until numChannels

        // Just read one frame at a time
        // https://docs.oracle.com/javase/tutorial/sound/converters.html
        val numBytes = 1 * bytesPerFrame
        val audioBytes = new Array[Byte](numBytes)
        var numBytesRead = 0
        var idx = 0
        // Try to read numBytes bytes from the file.
        while ( numBytesRead != -1 ) {
          numBytesRead = data.read(audioBytes)
          if (numBytesRead > 0) {
            if (numBytesRead != bytesPerFrame)
              throw new RuntimeException(s"numBytesRead($numBytesRead) != bytesPerFrame($bytesPerFrame)")
            // Convert frame bytes to channel samples
            val frameData: Array[Double] = Audio.getFrameDataPerChannel(
              bytesPerChannelSample,
              isBigEndian = data.getFormat.isBigEndian)(audioBytes)
            // Store the samples of each channel
            i.foreach( j => channels(j)(idx) = frameData(j) )
            idx += 1
          }
        } // while
        Some((channels, idx))
    }
  }


  /**
    * Get the mixer information. Allow one to view which
    * target (mics) ans source (speaker) lines are available.
    * We can use this to select a mixer and a specific line
    * manually.
    */
  def printMixerInfo(): Unit = {
    val mixerInfo = AudioSystem.getMixerInfo
    println("Mixer Info:")
    println(mixerInfo.mkString(";\n"))
    mixerInfo.foreach { mixInf =>
      val mixer = AudioSystem.getMixer(mixInf)
      val sourceLines = mixer.getSourceLineInfo
      println(s"\t$mixInf:")
      println(s"\t\tSource (speakers) Line Info:")
      val tmp1: String = sourceLines.map( "\t\t\t" + _.toString ).mkString(";\n")
      if (tmp1=="") println("\t\t\t----------") else println(tmp1)
      val targetLines = mixer.getTargetLineInfo
      println(s"\t\tTarget (mic) Line Info:")
      val tmp2: String = targetLines.map( "\t\t\t" + _.toString ).mkString(";\n")
      if (tmp2=="") println("\t\t\t----------") else println(tmp2)
    }
  }

  /**
    * Get the mixers that support the required format for
    * a specific target or source line.
    *
    * @see [[javax.sound.sampled.AudioFormat]]
    * @param format Audio format (for example PCM)
    * @param cls either the class of a [[javax.sound.sampled.TargetDataLine]] or
    *            a [[javax.sound.sampled.SourceDataLine]]
    * @tparam T generic class
    * @return an array of compatible mixers
    */
  def getMixers[T](format: AudioFormat, cls:Class[T]): Array[Mixer.Info] = {
    val lineInfo = new DataLine.Info(cls, format)
    if (AudioSystem.isLineSupported(lineInfo)) {
      Array()
    }
    else {
      val mixerInfo = AudioSystem.getMixerInfo
      if (mixerInfo.isEmpty)
        Array()
      else {
        val mixs = mixerInfo.filter{ mixInf =>
          val mixer = AudioSystem.getMixer(mixInf)
          mixer.isLineSupported(lineInfo)
        }
        mixs
      }
    }
  }

  /**
    *  Get the mixers that support the required format for
    * a specific target line.
    *
    * @param format Audio format (for example PCM)
    * @return array of Mixers information
    */
  def getTargetMixers(format: AudioFormat): Array[Mixer.Info] = getMixers(format, classOf[TargetDataLine])

  /**
    * Get the mixers that support the required format for
    * a source line.
    *
    * @param format Audio format (for example PCM)
    * @return array of Mixers information
    */
  def getSourceMixers(format: AudioFormat): Array[Mixer.Info] = getMixers(format, classOf[SourceDataLine])

  /**
    * Get either the mixer's target or source lines that support the required
    * audio format.
    *
    * @param format Audio format (for example PCM)
    * @param mixInf Selected Mixer's information
    * @param lineInfo Function that gets the target or source lines
    * @return Selected mixer and its compatible lines
    */
  def getLines(format: AudioFormat, mixInf: Mixer.Info, lineInfo: Mixer => Array[Line.Info]): (Mixer, Array[Line.Info]) = {
    val mixer = AudioSystem.getMixer(mixInf)
    val lines: Array[Line.Info] = lineInfo(mixer)
    val okLines: Array[Line.Info] = lines.filter(l => mixer.isLineSupported(l))
    (mixer,okLines)
  }

  /**
    * Get the mixer's target lines that support the required
    * audio format.
    *
    * @param format Audio format (for example PCM)
    * @param mixInf Selected Mixer's information
    * @return Selected mixer and its compatible target lines
    */
  def getTargetLines(format: AudioFormat, mixInf: Mixer.Info): (Mixer, Array[Line.Info]) =
    getLines(format, mixInf, (m:Mixer) => m.getTargetLineInfo )

  /**
    * Get the mixer's source lines that support the required
    * audio format.
    *
    * @param format Audio format (for example PCM)
    * @param mixInf Selected Mixer's information
    * @return Selected mixer and its compatible source lines
    */
  def getSourceLines(format: AudioFormat, mixInf: Mixer.Info): (Mixer, Array[Line.Info]) =
    getLines(format, mixInf, (m:Mixer) => m.getSourceLineInfo)

  /**
    * Returns either a audio format compatible target or source line.
    *
    * @param format Audio format (for example PCM)
    * @param cls either the class of a [[javax.sound.sampled.TargetDataLine]] or
    *            a [[javax.sound.sampled.SourceDataLine]]
    * @param lineInfo Function that gets the target or source lines
    * @tparam T generic class
    * @return an array of compatible lines
    */
  def getDefaultLine[T](format: AudioFormat, cls:Class[T], lineInfo: Mixer => Array[Line.Info]): Option[T] = {
    val mixers = getMixers(format, cls)
    if (mixers.isEmpty)
      None
    else {
      val (mixer,lines) = getLines(format, mixers(0), lineInfo )
      if (lines.isEmpty)
        None
      else {
        val target: T = mixer.getLine(lines(0)).asInstanceOf[T]
        Some(target)
      }
    }
  }

  /**
    * Returns an audio format compatible target line.
    *
    * @param format Audio format (for example PCM)
    * @return
    */
  def getDefaultTargetLine(format: AudioFormat): Option[TargetDataLine] =
    getDefaultLine(format: AudioFormat, classOf[TargetDataLine], (m:Mixer) => m.getTargetLineInfo)

  /**
    * Returns an audio format compatible source line.
    *
    * @param format
    * @return
    */
  def getDefaultSourceLine(format: AudioFormat): Option[SourceDataLine] =
    getDefaultLine(format: AudioFormat, classOf[SourceDataLine], (m:Mixer) => m.getTargetLineInfo)

  /**
    * Creates an audio format descriptor of the PCM format.
    * By default we use a 8KHz sampling rate of 1 channel with signed 16 bit samples
    * @see https://docs.oracle.com/javase/8/docs/api/javax/sound/sampled/AudioFormat.html
    * @return [[javax.sound.sampled.AudioFormat]]
    */
  def getPCMFormat(sampleRate:Float = 8000.0F,
                   sampleSizeInBits:Int = 8,
                   channels:Int = 1,
                   signed:Boolean = true,
                   bigEndian:Boolean = false): AudioFormat = {
    new AudioFormat(sampleRate, sampleSizeInBits, channels, signed, bigEndian)
  }


  def getPCMRef(bytesPerChannelSample:Int, isSigned: Boolean) : Double = {
    val range = bytesPerChannelSample match {
      case java.lang.Byte.BYTES =>
        val range = java.lang.Byte.MAX_VALUE - java.lang.Byte.MIN_VALUE
        if (isSigned) range / 2.0 else range
      case java.lang.Short.BYTES =>
        val range = java.lang.Short.MAX_VALUE - java.lang.Short.MIN_VALUE
        if (isSigned) range / 2.0 else range
      case java.lang.Integer.BYTES =>
        val range = java.lang.Integer.MAX_VALUE - java.lang.Integer.MIN_VALUE
        if (isSigned) range / 2.0 else range
      case java.lang.Long.BYTES =>
        val range = java.lang.Long.MAX_VALUE - java.lang.Long.MIN_VALUE
        if (isSigned) range / 2.0 else range
    }
    range
  }

  def scaleTodBA() = {

  }


  /**
    * This is the base interface that should be implemented by anyone
    * that wants to process the audio. Note that the audio is provided
    * as bytes. The processing function must be aware of the number of
    * bytes per sample, bytes per frame and the endianess. See [[load]]
    * and [[getFrameDataPerChannel]] to see how it is done.
    */
  trait AudioProcessor {
    /**
      * Process the correct number bytes that have been loaded into an array
      * @param data array with the sampled data
      * @param bytesRead valid number of bytes in the array
      */
    def process(data: Array[Byte], bytesRead: Int)

    /**
      * Executed just before samples are drawn and processed.
      */
    def start(format: AudioFormat, line: TargetDataLine) : Unit

    /**
      * Indicates when sampling should stop. We can stop
      * sampling based on the number of bytes read or the
      * approximate time that has been sampled in seconds.
      *
      * @param bytes number of bytes currently read
      * @param time approximate sampling time
      * @return true to stop else false
      */
    def stop(bytes: Long, time: Long) : Boolean

    /**
      * Executed right after sampling has been terminated.
      */
    def close() : Unit
  }


  /**
    * This is an audio processor that simply saves the ata to a file for
    * later processing and playback.
    *
    * @param outFile File were data is saved (overwritten if it already exists).
    * @param stopSampling Function used to stop sampling based on either
    *                     the number of bytes read or the approximate
    *                     time sampling has been running.
    */
  case class SaveAudioTo(outFile: File, stopSampling:(Long, Long) => Boolean) extends AudioProcessor {
    var out: BufferedOutputStream = _

    /**
      * Write the number bytes read into a file
      *
      * @param data      array with the sampled data
      * @param bytesRead valid number of bytes in the array
      */
    override def process(data: Array[Byte], bytesRead: Int): Unit = out.write(data, 0, bytesRead)

    /**
      * Open file to store data. Overwrite if it already exists.
      */
    override def start(format: AudioFormat, line: TargetDataLine): Unit = {
      import better.files._
      //import java.io.{File => JFile}

      if (out != null) close()

      val outputstream = outFile.newOutputStream
      out = outputstream.buffered
    }

    /**
      * Indicates when sampling should stop. We can stop
      * sampling based on the number of bytes read or the
      * approximate time that has been sampled in seconds.
      *
      * @param bytes number of bytes currently read
      * @param time  approximate sampling time
      * @return true to stop else false
      */
    override def stop(bytes: Long, time: Long): Boolean = {
      val r = stopSampling(bytes, time)
      if (r) out.flush()
      r
    }

    /**
      * Close the file right after sampling has been terminated.
      */
    override def close(): Unit = {
      out.flush()
      out.close()
      out = null
    }
  }

  /**
    * Calculate approximately how many bytes per second
    * are sampled by the given target line. The sampling
    * rate is determined by the Audio format
    * [[javax.sound.sampled.AudioFormat]] used to start
    * the line sampling.
    *
    * @param line target line setup for a given Audio format
    * @return
    */
  def getBytesPerSec(line: TargetDataLine): Float = {
    val format = line.getFormat
    val samplesSec = format.getSampleRate
    val bits = format.getSampleSizeInBits
    val nuChannels = format.getChannels
    val totalBitsSec = samplesSec * bits * nuChannels
    val totalBytesSec = totalBitsSec / 8
    //println(s"totalBitsSec = $totalBitsSec")
    //println(s"totalBytesSec = $totalBytesSec")
    totalBytesSec
  }

  // TODO: give option to set buffer (% of buffer size)
  // Non-blocing use line.available
  // rate-adapt to the minimum bytes for the given format

  /**
    * This function samples audio with a given format from a Mixer's target line.
    * The data is then processed by the [[AudioProcessor]]. While sampling the
    * total number of bytes read and sampled time is calculated. Both these
    * values are returned is sampling was successful. If not a [[scala.None]] is
    * returned. This function should be execute within its own thread.
    *
    * @param format Audio format (for example PCM)
    * @param proc Instance that will process the data that has been sampled.
    * @param targetLine We can specify exactly which of the mixer's target line
    *                   to use. If none is provided a default will be obtained.
    *                   If none of the target lines are compatible with the
    *                   requested audio format.
    * @return
    */
  def sampleMic(format: AudioFormat, proc: AudioProcessor, targetLine: Option[TargetDataLine] = None, nonBlock:Boolean=true): Option[(Long, Long)] = {
    import javax.sound.sampled.DataLine
    import javax.sound.sampled.LineUnavailableException
    import javax.sound.sampled.TargetDataLine
    import javax.sound.sampled.AudioSystem

    val info = new DataLine.Info(classOf[TargetDataLine], format) // format is an AudioFormat object
    if (!AudioSystem.isLineSupported(info))
      None
    else {
      val frameSize = format.getFrameSize
      // Obtain and open the line.
      try {
        val line = targetLine.fold(AudioSystem.getLine(info).asInstanceOf[TargetDataLine])( e => e)
        line.open(format)

        val totalBytesSec = getBytesPerSec(line)

        // Assume that the TargetDataLine, line, has already
        // been obtained and opened.
        var numBytesRead = 0
        val data = new Array[Byte](line.getBufferSize / 5)

        // Begin audio capture.
        line.start()
        proc.start(format,line)

        var bytesRead = 0L
        var timeMilli = 0L
        // Here, stopped is a global boolean set by another thread.
        while ( !proc.stop(bytesRead, timeMilli)  ) {
          //println(s"Nº Bytes can read without blocking: ${line.available}")
          val bytesToRead = if (nonBlock) Math.min(line.available+frameSize, data.length) else data.length
          if (bytesToRead >= frameSize) {
            // Read the next chunk of data from the TargetDataLine.
            numBytesRead = line.read(data, 0, bytesToRead)
            // Save this chunk of data.
            proc.process(data, numBytesRead)
            bytesRead = bytesRead + numBytesRead
            timeMilli = (bytesRead / totalBytesSec).toLong
          }
        }

        // Stop audio capture
        line.stop()
        line.close()
        proc.close()
        Some((bytesRead, timeMilli))
      } catch {
        case ex: LineUnavailableException => None
      }
    }
  }

  // https://stackoverflow.com/questions/32347274/how-to-play-record-sound-on-from-a-javax-sound-sampled-line
  // https://stackoverflow.com/questions/6002444/java-recording-from-mixer


  // Environmental noise monitoring
  // https://www.nti-audio.com/en/support/know-how/how-are-percentile-statistics-measured
  // http://www.swarthmore.edu/NatSci/sciproject/noise/noisequant.html
}
