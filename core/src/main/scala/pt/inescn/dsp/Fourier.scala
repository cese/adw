/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import java.io.File
import java.time.Duration

import breeze.linalg.{DenseMatrix, DenseVector}
import breeze.plot.{Figure, GradientPaintScale, Plot, plot}
import org.hipparchus.complex.Complex
import pt.inescn.samplers.Base.{LengthSample, Sampler}
import org.hipparchus.transform.FastFourierTransformer
import org.hipparchus.transform.DftNormalization
import org.hipparchus.transform.TransformType

//import scala.collection.immutable.NumericRange

/**
  * Contains a set of functions that are useful for working with time dependent signals.
  * Used with signal processing functions and plotting.
  *
  * TODO:
  * NOTE: we are using hipparchus FFT implementation that requires the signal have the
  * number of samples be equal to a power of 2. This means that a signal must either be
  * truncated or padded with zeros. When performing the Hilbert transform based on the
  * Fourier transform using the FFT, we will either get an analytical signal (envelope)
  * that is shorter or longer than the original signal.
  * We could opt fo the use of the JTransform library that allows a signal with arbitrary
  * length. It also supports multi-core processing fro very large signals. However, it uses
  * specialized arrays and packs the real and imaginary parts into a single array. We will
  * have to analyze if JTransforms ca be used.
  * Additionally, JWave, also requires sampling length as a powwer of 2, however the author
  * refers to a AncientEgyptianDecomposition class. This may also provide an alternate solution.
  * For now, the Hilbert transform based on the FFT will pad the signal.
  *
  *
  * References
  * @see https://www.hipparchus.org/
  * @see https://sites.google.com/site/piotrwendykier/software/jtransforms
  * @see https://github.com/wendykierp/JTransforms
  * @see https://github.com/cscheiblich/JWave
  *      https://wavelet-tour.github.io/
  * @see https://github.com/DiegoCatalano/Catalano-Framework
  * @see https://www.nayuki.io/page/free-small-fft-in-multiple-languages
  * @see https://www.nayuki.io/page/how-to-implement-the-discrete-fourier-transform
  * @see https://www.nayuki.io/page/number-theoretic-transform-integer-dft
  * @see http://vadim2000.chat.ru/soft.htm#Signalgo
  * @see https://github.com/SableRaf/signalfilter
  * @see http://www.berndporr.me.uk/iir/
  * @see http://aquila-dsp.org/
  * @see http://www.scottsarra.org/signal/signal.html
  *
  * Breeze uses JTransforms, see breeze.signal.support.JTransformsSupport
  * See edu.emory.mathcs.jtransforms.fft.{DoubleFFT_1D, DoubleFFT_2D}
  * https://mvnrepository.com/artifact/com.github.wendykierp/JTransforms
  * libraryDependencies += "com.github.wendykierp" % "JTransforms" % "3.1"
  * http://carsomyr.github.io/shared/
  *
  * Note: see  AncientEgyptianDecomposition in JWave
  *
  * Created by hmf on 27-06-2017.
  */
object Fourier {

  def todB(v: Double): Double = 20*Math.log10(v)
  def dBtoLin(v: Double): Double = math.pow(10, v/20.0)

  /**
    * Helper function that determines the sampling delta. All values are in seconds.
    *
    * @param samplesPerSecond    - how many samples to generate per second (sampling rate in Hz).
    * @param sampleTimeInSeconds - how long in seconds we want to samplesTo for in seconds
    * @return (delta, time domain) - delta is the sampling rate in seconds, the tme domain are the x
    *         plot domain in seconds.
    */
  def samplingSpecification(samplesPerSecond: Double, sampleTimeInSeconds: Double): (Double, Seq[Double]) = {
    val delta = 1.0 / samplesPerSecond
    val tmp = (BigDecimal(delta) to sampleTimeInSeconds) by delta map(_.toDouble)
    (delta, tmp)
  }

  /**
    * Returns the number of samples to samplesTo a signal wih a
    * a duration of `samplesPerSecond` if the sampling rate is
    * done at `samplesPerSecond` Hz.
    *
    * @param samplesPerSecond sampling rate in Hz
    * @param sampleTimeInSeconds period during which to samplesTo
    * @return number of samples required
    */
  def numberOfSamplesFor(samplesPerSecond: Double, sampleTimeInSeconds: Double): Int = {
    val delta = 1.0 / samplesPerSecond
    Math.ceil(sampleTimeInSeconds / delta).toInt
  }

  /**
    * Returns the number of samples to samplesTo a single period of a
    * signal wih a top frequency component of `topFrequency` if the
    * sampling rate is done at `samplesPerSecond` Hz.
    *
    * @param samplesPerSecond sampling rate in Hz
    * @param topFrequency maximum frequency of sampled signal
    * @return number of samples required
    */
  def numberOfSamplesFor1Period(samplesPerSecond: Double, topFrequency: Double): Int = {
    val period = 1.0 / topFrequency
    numberOfSamplesFor(samplesPerSecond, period)
  }

  /**
    * This function returns the number of seconds a signal lasts if is consists of `numSamples` that
    * have been samples at a frequency of `samplesPerSecond` Hz.
    *
    * @param samplesPerSecond - sampling rate in Hz
    * @param numSamples - number of samples used
    * @return - the total duration of the samplesTo
    */
  def sampledTime(samplesPerSecond: Double, numSamples: Double): Double = numSamples / samplesPerSecond

  /**
    * Nyquist sampling frequency
    *
    * @param freq - list of frequencies of signals used.
    * @return - Returns twice the maximum.
    */
  def nyquistNamplingFrequency(freq: Double*): Double = freq.max * 2


  /**
    * Assumes the delta is a fraction of seconds. This value is converted
    * into a duration. We use the system's maximum precision - nanoseconds.
    *
    * @param delta - fraction of seconds
    * @return  [[https://docs.oracle.com/javase/8/docs/api/java/time/Duration.html java.time.Duration]]
    */
  def convertFractionSecondsToDuration(delta: Double): Duration = {
    val nanos = (delta * 1e9).toLong
    val sampling_rate = Duration.ofNanos(nanos)
    sampling_rate
  }


  /**
    * Checks if a number is a power of two.
    *
    * @see https://stackoverflow.com/questions/19383248/find-if-a-number-is-a-power-of-two-without-math-function-or-log-function
    * @see https://en.wikipedia.org/wiki/Power_of_two#Fast_algorithm_to_check_if_a_positive_number_is_a_power_of_two
    * @param n - number that may not be a power of 2
    * @return
    */
  def isPowerOf2(n: Int): Boolean = (n > 0) && ((n & (n - 1)) == 0)

  /**
    * Checks if a number is a power of two.
    *
    * @see https://stackoverflow.com/questions/19383248/find-if-a-number-is-a-power-of-two-without-math-function-or-log-function
    * @see https://en.wikipedia.org/wiki/Power_of_two#Fast_algorithm_to_check_if_a_positive_number_is_a_power_of_two
    * @param n - number that may not be a power of 2
    * @return
    */
  def isPowerOf2(n: Long): Boolean = (n > 0) && ((n & (n - 1)) == 0)

  /**
    * Returns the power of 2 closest to `n` (just above)
    *
    * @see https://en.wikipedia.org/wiki/Find_first_set
    * @see http://graphics.stanford.edu/~seander/bithacks.html
    * @see https://stackoverflow.com/questions/5242533/fast-way-to-find-exponent-of-nearest-superior-power-of-2
    * @see https://stackoverflow.com/questions/27583122/how-to-find-the-nearest-number-that-is-power-of-two-to-another-number
    */
  def floorPowerOf2(n: Int): Int = if (n == 0) 0 else 32 - Integer.numberOfLeadingZeros(n - 1)

  /**
    * Returns the power of 2 closest to `n` (just above)
    *
    * @see https://en.wikipedia.org/wiki/Find_first_set
    * @see http://graphics.stanford.edu/~seander/bithacks.html
    * @see https://stackoverflow.com/questions/5242533/fast-way-to-find-exponent-of-nearest-superior-power-of-2
    * @see https://stackoverflow.com/questions/27583122/how-to-find-the-nearest-number-that-is-power-of-two-to-another-number
    */
  def floorPowerOf2(n: Long): Int = if (n == 0) 0 else 64 - java.lang.Long.numberOfLeadingZeros(n - 1)


  /**
    * Returns the power of 2 closest to `n` but not greater than `n`.
    *
    * @param n - number that may not be a power of 2
    * @return
    */
  def truncatePowerOf2(n: Long): Long = {
    if (isPowerOf2(n))
      n
    else {
      val power2 = floorPowerOf2(n)
      Math.pow(2, power2 - 1).toLong
    }
  }

  /**
    * Returns the power of 2 closest to `n` but not greater than `n`.
    *
    * @param n - number that may not be a power of 2
    * @return
    */
  def truncatePowerOf2(n: Int): Int = truncatePowerOf2(n.toLong).toInt

  /**
    * Returns the power of 2 closest to `n` just above `n`.
    *
    * @param n - number that may not be a power of 2
    * @return
    */
  def cielPowerOf2(n: Long): Long = {
    if (isPowerOf2(n))
      n
    else {
      val power2 = floorPowerOf2(n)
      Math.pow(2, power2).toLong
    }
  }


  /**
    * Returns the power of 2 closest to `n` just above `n`.
    *
    * @param n - number that may not be a power of 2
    * @return
    */
  def cielPowerOf2(n: Int): Int = cielPowerOf2(n.toLong).toInt

  /**
    * Takes the sampled signal `sig` and returns either a truncated or extended version.
    * If `new_len` is shorter than the `sig` length, the the `sig` is truncated. If the
    * `new_len` is larger however, the array is padded with 0s. If the length are the same
    * then the same array is returned.
    *
    * @param sig - sampled signal in the time domain
    * @param new_len - new length we want to resize to. If greater than the `sig` length, pad it.
    * @return - nwe copy of the array hat has been either truncated or extended
    */
  def resizeLength(sig: Array[Double], new_len: Int) : Array[Double] = {
    val len = sig.length
    if (new_len == len)
      sig
    else {
      if (new_len < len) {
        sig.slice(0, new_len)
      }
      else {
        val result = Array.ofDim[Double](new_len)
        Array.copy(sig, 0, result, 0, len)
        (len until new_len).foreach( result(_) = 0.0)
        result
      }
    }
  }

  /**
    * Returns the frequency scale for a signal with length `signal_length`
    * that has been sampled at the rate of `fs` Hz
    *
    * @param fs sampling rate in Hz
    * @param signal_length number of samples of the signal (frequency domain, all)
    * @return delta in frequency domain
    */
  def deltaFreq(fs: Double, signal_length: Int): (Double, Double) = {
    val freq = fs / 2 // Don't show all the frequencies
    val deltaFreq = (fs * 1.0) / signal_length // Show them at the correct sampling rate
    (freq, deltaFreq)
  }

  /**
    * Returns the frequency domain for a signal with length `signal_length`
    * that has been sampled at the rate of `fs` Hz
    *
    * @param fs sampling rate in Hz
    * @param signal_length number of samples of the signal (frequency domain, all)
    * @return frequency domain of the signal
    */
  def freqDomain(fs: Double, signal_length: Int): IndexedSeq[Double] = {
    val (freq, deltaFrq) = deltaFreq(fs, signal_length)
    //val frequencies = 0.0 to freq by deltaFrq
    val frequencies = (0 until (signal_length/2)) map ( _ * deltaFrq)
    frequencies
  }

  /**
    * Returns the time scale for a signal with length `signal_length`
    * that has been sampled at the rate of `fs` Hz
    *
    * @param fs sampling rate in Hz
    * @param signal_length number of samples of the signal (frequency domain, all)
    * @return delta in time domain
    */
  def deltaTime(fs: Double, signal_length: Int): (Double, Double) = {
    val deltaTime = 1.0 / fs // sampling rate
    val totalTime = sampledTime( fs, signal_length)
    (totalTime, deltaTime)
  }

  /**
    * Given a signal with length `signal_length` that has been sampled
    * at `fs` Hz, return the index of the frequency `at` of the Fourier
    * transform. Note that we assume the signal is real and the so only
    * half of the frequency band is represented (symmetric around the DC
    * component).
    *
    * @param fs sampling frequency in Hz
    * @param signal_length signal length of FFT
    * @param atFreq frequency whose index we want
    * @return index of the FFT at which the frequency can be found
    */
  def indexOfFrequency(fs: Double, signal_length: Int, atFreq: Double): Int = {
    Math.round(signal_length.toDouble * (atFreq / fs )).toInt
  }

  /**
    * Given a signal with length `signal_length` that has been sampled
    * at `fs` Hz, return the frequency of the index `at` of the Fourier
    * transform. Note that we assume the signal is real and the so only
    * half of the frequency band is represented (symmetric around the DC
    * component)
    *
    * @param fs sampling frequency in Hz
    * @param signal_length signal length of FFT
    * @param atIndex index whose we want frequency
    * @return frequency of the FFT at which the index can be found
    */
  def frequencyOfIndex(fs: Double, signal_length: Int, atIndex: Double): Double = {
    fs * (atIndex / signal_length.toDouble)
  }

  /**
    * Returns the time domain for a signal with length `signal_length`
    * that has been sampled at the rate of `fs` Hz
    *
    * @param fs sampling rate in Hz
    * @param signal_length number of samples of the signal (time domain)
    * @return time domain of the signal
    */
  def timeDomain(fs: Double, signal_length: Int): Seq[Double] = {
    val (totalTime, deltaTme) = deltaTime(fs, signal_length)
    val frequencies = BigDecimal(deltaTme) to totalTime by deltaTme map(_.toDouble)
    frequencies
  }

  /**
    * This function returns the frequency domain that can be used to label the x-axis of the FFT plots
    * (absolute value and phase). This function also returns a new samplesTo signal length that that is a power
    * of 2 as close to `signal_length` as possible (larger or smaller depending if you set pad to true or
    * false respectively). This should be used to calculate the FFT, required by many implementations.
    *
    * Note 1: Because we are dealing with real signals, both absolute
    * value and phase are mirrored around the DC (0Hz) component. We
    * only return the components from 0 (DC) to fs/2
    *
    * Note 2: the domain returned also has the new length that is a power of 2.
    *
    * @param fs            - sampling frequency used on signal
    * @param signal_length - length of the sampled signal
    * @return
    *
    * @see https://stackoverflow.com/questions/4364823/how-do-i-obtain-the-frequencies-of-each-value-in-an-fft/4371627#4371627
    */
  def FFTFreqDomain(fs: Double, signal_length: Int, pad : Boolean = false): (Int, IndexedSeq[Double]) = {
    val len = if (pad) cielPowerOf2(signal_length) else truncatePowerOf2(signal_length)
    val frequencies = freqDomain(fs, len)
    (len, frequencies)
  }

  /**
    * This function calculates the FFT
    *
    * Note 1: `len != xa.length`
    *
    * @param fs            - sampling frequency used on the signal `sig`
    * @param signal_length - number of samples to generate
    * @param sig           - the signal in the time domain from which we can take at least `signal_length` samples
    * @param pad           - if true extend the signal otherwise truncate it to have a size of power of 2
    * @return - a 3 element tuple. The first is the number of samples used by the FFT (power of 2). The second
    *         element is the array of the frequency domain. Note that we assume real signals so we only return
    *         a first half of the frequencies. The last are the FFT components returned as an array of complex
    *         numbers.
    */
  def FFT(fs: Double, signal_length: Int, sig: Array[Double],
          pad: Boolean = false, fftNormalization: DftNormalization=DftNormalization.STANDARD):
  (Int, IndexedSeq[Double], Array[Complex]) = {
    val (len, xa) = Fourier.FFTFreqDomain(fs, signal_length, pad)
    val fft = new FastFourierTransformer(fftNormalization)

    // Perform FFT and get components
    //val tsig: Array[Double] = sig.slice(0, len)
    val tsig: Array[Double] = resizeLength(sig, len)
    val complexTsig = fft.transform(tsig, TransformType.FORWARD)

    (len, xa, complexTsig)
  }

  /**
    * Returns the original signal by performing an inverse Fourier transform.
    *
    * @param sig - signal in the frequency domain (Comlpex) obtained via a Fourier transform
    * @return
    */
  def iFFT(sig: Array[Complex], fftNormalization: DftNormalization=DftNormalization.STANDARD): Array[Complex] = {
    val fft = new FastFourierTransformer(fftNormalization)
    // Perform FFT and get components
    fft.transform(sig, TransformType.INVERSE)
  }

  /**
    * This function calculates the FFT
    *
    * Note 1: `len != xa.length`
    *
    * @param fs            - sampling frequency used on the signal `sig`
    * @param signal_length - number of samples to generate
    * @param sig           - the signal in the time domain from which we can take at least `signal_length` samples
    * @param pad           - if true extend the signal otherwise truncate it to have a size of power of 2
    * @tparam T - Indicates if the samplesTo is finite or not (see [[pt.inescn.samplers.Base.LengthSample]])
    * @return - a 3 element tuple. The first is the number of samples used by the FFT (power of 2). The second
    *         element is the array of the frequency domain. Note that we assume real signals so we only return
    *         a first half of the frequencies. The last are the FFT components returned as an array of complex
    *         numbers.
    *
    *         References
    * @see https://sites.google.com/site/piotrwendykier/software/jtransforms
    * @see https://github.com/wendykierp/JTransforms
    * @see https://github.com/cscheiblich/JWave
    * @see https://www.nayuki.io/page/free-small-fft-in-multiple-languages
    * @see https://www.nayuki.io/page/how-to-implement-the-discrete-fourier-transform
    * @see https://www.nayuki.io/page/number-theoretic-transform-integer-dft
    * @see Java DSP Collection: http://www.source-code.biz/dsp/java/
    * @see https://sourceforge.net/projects/dsplaboratory/
    * @see https://github.com/JorenSix/TarsosDSP
    *      (https://github.com/librosa/librosa)
    *
    *      Breeze uses JTransforms, see breeze.signal.support.JTransformsSupport
    *      See edu.emory.mathcs.jtransforms.fft.{DoubleFFT_1D, DoubleFFT_2D}
    *      https://mvnrepository.com/artifact/com.github.wendykierp/JTransforms
    *      libraryDependencies += "com.github.wendykierp" % "JTransforms" % "3.1"
    *      http://carsomyr.github.io/shared/
    *
    *      Note: see  AncientEgyptianDecomposition in
    */
  def FFT[T <: pt.inescn.samplers.Base.LengthSample](fs: Double, signal_length: Int,
                                                     sig: Sampler[T, Double], pad : Boolean,
                                                     fftNormalization: DftNormalization):
  (Int, IndexedSeq[Double], Array[Complex]) = {
    FFT(fs, signal_length, sig(signal_length).toArray, pad, fftNormalization)
  }

  /**
    * Returns the original signal by performing an inverse Fourier transform.
    *
    * @param sig - signal in the frequency domain (Comlpex) obtained via a Fourier transform
    * @return
    */
  def iFFT[T <: pt.inescn.samplers.Base.LengthSample](fs: Double, signal_length: Int, sig: Sampler[T, Complex],
                                                      fftNormalization: DftNormalization) : Array[Complex] = {
    iFFT(sig(signal_length).toArray, fftNormalization)
  }

  /**
    * Bounded Fourier transform (slow)
    *
    * @see https://www.nayuki.io/page/how-to-implement-the-discrete-fourier-transform
    * @see https://www.nayuki.io/page/free-small-fft-in-multiple-languages
    * @see www.asee.org/public/conferences/1/papers/451/download
    *      (A taste of Java Bounded and Fast Fourier Transforms Rev2)
    *
    * @param inreal  - input signal (real component)
    * @param inimag  - input signal (imaginary component)
    * @return - return the discrete fourier transform as an array o complex numbers
    */
  def dft(inreal: Array[Double], inimag: Array[Double]): Array[Complex] = {
    val n = inreal.length
    val twoPiOverN = 2.0 * Math.PI / n
    var twoPikOverN = .0
    var twoPijkOverN = .0

    val out = new Array[Complex](n)
    var k = 0
    while (k < n) { // For each output element
      var sumreal = 0.0
      var sumimag = 0.0
      twoPikOverN = twoPiOverN * k
      var j = 0
      while (j < n) { // For each input element
        twoPijkOverN = twoPikOverN * j
        sumreal += inreal(j) * Math.cos(twoPijkOverN) + inimag(j) * Math.sin(twoPijkOverN)
        sumimag += -inreal(j) * Math.sin(twoPijkOverN) + inimag(j) * Math.cos(twoPijkOverN)
        j += 1
      }
      out(k) = new Complex(sumreal, sumimag)
      k += 1
    }
    out
  }

  /**
    * Inverse discrete Fourier transform (slow).
    *
    * @see http://www.jot.fm/issues/issue_2009_05/column2.pdf
    * @see http://www.physics.unlv.edu/~pang/comp4/Fourier.java
    * @param in - input signal (array of complex numbers)
    * @return - return the discrete inverse fourier transform as an array o complex numbers
    */
  def idft(in: Array[Complex]): Array[Complex] = {
    val n = in.length
    val twoPiOverN = 2.0 * Math.PI / n
    var twoPikOverN = .0
    var twoPijkOverN = .0

    val out = new Array[Complex](n)
    var k = 0
    while ( k < n ) {
      var sumreal = 0.0
      var sumimag = 0.0
      twoPikOverN = twoPiOverN * k
      var j = 0
      while ( j < n) {
        twoPijkOverN = twoPikOverN * j
        sumreal += in(j).getReal * Math.cos(twoPijkOverN) - in(j).getImaginary * Math.sin(twoPijkOverN)
        sumimag += in(j).getImaginary * Math.cos(twoPijkOverN) + in(j).getReal * Math.sin(twoPijkOverN)
        j += 1
      }
      out(k) = new Complex(sumreal, sumimag)
      k += 1
    }
    out
  }

  /**
    * This function calculates the DFT. It is a naive implementation sued for sanity checking.
    *
    * Note 1: `len != xa.length`
    *
    * @param fs            - sampling frequency used on the signal `sig`
    * @param signal_length - number of samples to generate
    * @param sig           - the signal in the time domain from which we can take at least `signal_length` samples
    * @return - a 3 element tuple. The first is the number of samples used by the FFT (power of 2). The second
    *         element is the array of the frequency domain. Note that we assume real signals so we only return
    *         a first half of the frequencies. The last are the FFT components returned as an array of complex
    *         numbers.
    */
  def DFT(fs: Double, signal_length: Int, sig: Array[Double],
          fftNormalization: DftNormalization=DftNormalization.STANDARD):
  (Int, IndexedSeq[Double], Array[Complex])
  = {
    val xa = Fourier.freqDomain(fs, signal_length)

    // Perform FFT and get components
    val tsig_real = sig.slice(0, signal_length)
    val tsig_image = Array.fill(signal_length)(0.0)

    val complexTsig = dft(tsig_real, tsig_image)
    (signal_length, xa, complexTsig)
  }

  def iDFT(sig: Array[Complex]): Array[Complex] = idft(sig)

  /**
    * This function calculates the DFT. It is a naive implementation sued for sanity checking.
    *
    * Note 1: `len != xa.length`
    *
    * @param fs            - sampling frequency used on the signal `sig`
    * @param signal_length - number of samples to generate
    * @param sig           - the signal in the time domain from which we can take at least `signal_length` samples
    * @param pad           - not used
    * @tparam T - Indicates if the samplesTo is finite or not (see [[pt.inescn.samplers.Base.LengthSample]])
    * @return - a 3 element tuple. The first is the number of samples used by the FFT (power of 2). The second
    *         element is the array of the frequency domain. Note that we assume real signals so we only return
    *         a first half of the frequencies. The last are the FFT components returned as an array of complex
    *         numbers.
    */
  def DFT[T <: pt.inescn.samplers.Base.LengthSample](fs: Double, signal_length: Int,
                                                     sig: Sampler[T, Double],
                                                     pad : Boolean,
                                                     fftNormalization: DftNormalization): (Int, IndexedSeq[Double], Array[Complex]) = {
    DFT(fs, signal_length, sig(signal_length).toArray, fftNormalization)
  }

  /**
    * Absolute values of Complex values (for example of the FFT components)
    *
    * @param fftSig - FFT components (complex numbers)
    * @return
    */
  def abs(fftSig: Array[Complex]): Array[Double] = fftSig.map(_.abs())

  /**
    * Phase or arguments of Complex values (for example of the FFT components)
    *
    * @param fftSig - FFT components (complex numbers)
    * @return
    */
  def phase(fftSig: Array[Complex]): Array[Double] = fftSig.map(_.getArgument)

  /**
    * Instantaneous phase or arguments of Complex values (for example from the components of the Hilbert transform)
    * Used to get the instantaneous phase of the Hilbert analytic signal (in the time domain)
    *
    * @param fftSig - Hilbert transform
    * @return
    */
  def instantaneousPhase(fftSig: Array[Complex]): Array[Double] = fftSig.map( e => Math.atan( e.getImaginary / e.getReal ))

  /**
    * Analytical signal in the time domain. We return the amplitude (envelope) and instantaneous phase.
    * TODO: add Hilbert transform in here and pass in (real) time domain signal
    *
    * @param fftSig - this is the sampled signal in the Complex domain transformed via the Hilbert transform.
    * @return
    */
  def analyticSignal(fftSig: Array[Complex]): (Array[Double], Array[Double]) = (abs(fftSig), instantaneousPhase(fftSig))

  /**
    * Returns the `k` Fourier transform components with the highest absolute values.
    *
    * @param k     - maximum number of Fourier components to select that have the highest absolute value
    * @param abs   - absolute values of the Fourier transform
    * @param phase - argument (phase) of the Fourier transform
    * @return - absolute value and argument of the top k components
    */
  def topAbsoluteComponents(k: Int, abs: Array[Double], phase: Array[Double]): (Array[(Double, Int)], Array[(Double, Int)]) = {
    val half = abs.length / 2
    val sya = abs.take(half).zipWithIndex.sortBy(-_._1).take(k) // top k amplitudes
    val syb = sya.map(p => (phase(p._2), p._2)) // phases of the same component
    (sya, syb)
  }

  /**
    * Returns the `k` Fourier transform components with the highest absolute values.
    *
    * @param k      - maximum number of Fourier components to select that have the highest absolute value
    * @param fftSig - complex numbers that represent the Fourier transform of the sampled signal
    * @return - absolute value and argument of the top k components
    */
  def topAbsoluteComponents(k: Int, fftSig: Array[Complex]): (Array[(Double, Int)], Array[(Double, Int)]) = {
    val ya = abs(fftSig)
    val yb = phase(fftSig)
    topAbsoluteComponents(k, ya, yb)
  }

  /* STFT */


  /**
    * Checks that signal can be reconstructed with inverse STFT
    * @see https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.stft.html
    *
    * @param sigLen length of the signal
    * @param windowSize size of the STFT window
    * @param overlap number of samples of the tme-domain signal that should overlap
    * @return
    */
  def checkCola(sigLen:Int, windowSize:Int, overlap:Int): Boolean = {
    (sigLen - windowSize) % (windowSize-overlap) == 0
  }

  def splitSignalS(sig:Seq[Double], samplingFreq:Double, windowSize:Int, overlap:Int = 0,
                   pad:Boolean = true, extendBoundary:Boolean = true):
  (Int, Int, Array[Array[Double]], IndexedSeq[Double], Boolean) = {
    splitSignal(sig.toArray, samplingFreq, windowSize, overlap, pad, extendBoundary)
  }

  /**
    * Splits the signal into a number of windows. This can be used to split a
    * signal in the time-domain into several windows so that we can generate
    * the short time Fourier transform (STFT).
    *
    * Note that to use this for the STFT all windows sizes will be a power of
    * 2 (jus below or just above the requested size if this is not a power of
    * two). Note that it does not make sense to not extend all windows to the
    * same size (otherwise padding would be wrong in case padding is set to
    * true).
    *
    * @param sig time domain signal
    * @param samplingFreq sampling frequency of `sig` in Hz
    * @param rWindowSize requested size of the window
    * @param overlap number of samples of the tme-domain signal that should overlap
    * @param pad if true then the windows is padded with zeros to a size of
    *            power of 2 equal to or above `rWindowSize`, otherwise
    *            truncated a power of 2 just below `rWindowSize`
    * @param extendBoundary if true and `rWindowSize` does not generate all
    *                       windows with the same size, then the last window
    *                       is padded with zeros so that all wndoes have the
    *                       same size
    * @return an array of arrays. Each sub array is a sampled window
    */
  def splitSignal(sig:Array[Double], samplingFreq:Double, rWindowSize:Int, overlap:Int = 0,
                  pad:Boolean = true, extendBoundary:Boolean = true):
  (Int, Int, Array[Array[Double]], IndexedSeq[Double], Boolean) = {

    // Each window must be a power of 2 so that the FFT can work
    // We can either truncate the windows (pad is false) or pad them (pad is true)
    val (fftWindowSize, xa) = Fourier.FFTFreqDomain(samplingFreq, rWindowSize, pad)

    // If we truncated then make the windows smaller otherwise enlarge by padding 0's
    val windowSize = Math.min(fftWindowSize, rWindowSize)
    /*println("--------------------------------")
    println(s"windowSize = $windowSize")
    println(s"fftWindowSize = $fftWindowSize")*/

    // If we want them to overlap, what is the stride
    val step = windowSize - overlap
    /*println(s"overlap = $overlap")
    println(s"step = $step")*/
    // Get the windows that have yet to be extended and padded
    val rawWindows: Array[Array[Double]] = sig.sliding(windowSize,step).toArray

    // How much should we extend the last windows so that all windows have the same size
    val last = rawWindows.last
    val boundary = windowSize - last.length
    /*println(s"last.length = ${last.length}")
    println(s"boundary = $boundary")*/

    // We need to extend the bounds so that we have constant length windows
    val extendedBoundary = if ((boundary > 0.0) && extendBoundary) {
      val lastWindow = rawWindows.last ++ Array.fill(boundary)(0.0)
      rawWindows(rawWindows.length-1) = lastWindow
      boundary
    } else
      0

    // Now we pad the windows if required
    val stftWindows = if (fftWindowSize > windowSize) {
      val diff = fftWindowSize - windowSize
      //println(s"diff = $diff")
      val afterFill = (diff / 2.0).toInt
      val beforeFill = afterFill + (diff % 2.0).toInt
      val before = Array.fill(beforeFill)(0.0)
      val after = Array.fill(afterFill)(0.0)
      rawWindows.map( e => before ++ e ++ after)
    }
    else
      rawWindows

    // Is the signal invertible?
    val finalWinLen = stftWindows(0).length
    /*println(s"finalWinLen = $finalWinLen")
    val lastFinalWinLen = stftWindows(stftWindows.length-1).length
    println(s"lastFinalWinLen = $lastFinalWinLen")*/
    val len = stftWindows.length * finalWinLen
    val cola = checkCola(len, finalWinLen, overlap)

    // windows and info on those windows
    (step, extendedBoundary, stftWindows, xa, cola)
  }

  /**
    * This function calculates the STFT (Short Time Fourier Transform) of
    * the time domain signal `sig` that has been sampled at `samplingFreq`
    * frequency (Hz). The signal is split into windows of size `windowSize`.
    * The windows may also `overlap` by a given number of samples. The
    * total signal length may not divide whole by the `windowSize` so the
    * last window may be extended with 0's to make all window lengths the
    * same if `extendBoundary` is set to true. Note that all windows must be
    * of a power of 2 in order to use the FFT. If `pad` is set to true, then
    * the `windowSize` is truncated to a power of 2 otherwise it is extended
    * to next higher power of 2. By default extending and padding are set
    * to true which is a good option.
    *
    * When dividing the original signal we create discontinuities which
    * introduces additional frequency components and its multiples. To
    * overcome this a FIR (Finite Impulse Response) filter may be applied
    * to each window (for example Bartlett, Hann, Blackman, etc). Several
    * are available in [[pt.inescn.dsp.Filter]]. For an example of its use
    * search for [[Filter.initHann]] in [[pt.inescn.dsp.FourierSpec]].
    * because these filters are passed in as functions, one can easily
    * implement additional filters. Note that these are filters applied in
    * the time domain.
    * @see https://en.wikipedia.org/wiki/Window_function
    *
    * Note that signals should be detrended before use (trend will
    * generate additional frequency components at the lower bandwidth).
    * Several detrending functions are available in the
    * [[pt.inescn.dsp.Filter]] package. We have basically two available:
    * constant (remove the DC component) and linear detrending such
    * as [[pt.inescn.dsp.Filter.differenceDetrend]] and
    * [[pt.inescn.dsp.Filter.regressionDetrend]]. Other filters mays also
    * also be applied to the `sig` before use (see [[pt.inescn.dsp.Envelope]])
    * and [[pt.inescn.dsp.FilterSpec]] for an example of the use of a
    * Butterworth filter. By default no filtering is used.
    *
    *
    * NOTE: Gabor limit - try and use overlapping windows
    *
    * Spec based on:
    * @see https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.stft.html
    *
    * @param sig time domain signal
    * @param samplingFreq sampling frequency of the signal in Hz
    * @param windowSize requested window size
    * @param filter FIR to be applied to each window in the time domain
    * @param overlap number of samples to overlap each window
    * @param extendBoundary indicate whether or not to make all window have
    *                       the same size
    * @param pad indicates whether to truncate or extend the window size to
    *            a power of 2
    * @return returns a an array of FFTs that can be plotted using [[plotSTFT]]
    *         see also [[plotSTFTMat]]
    */
  def stft(sig:Seq[Double], samplingFreq:Double,
           windowSize:Int, filter : Int => IndexedSeq[Double] = Filter.initNoFilter,
           overlap:Int = 0,extendBoundary:Boolean = true, pad:Boolean = true,
           fftNormalization: DftNormalization=DftNormalization.STANDARD):
  (Array[Array[Complex]], IndexedSeq[Double], Boolean) = {

    // Split the signal into windows. Pad and extend with 0's as required.
    // If we pad then we can either extend ro truncate to a power of 2 length
    // Windows can also overlap
    val (_, _, wins, xa, cola) = Fourier.splitSignalS(sig,samplingFreq,windowSize,overlap,pad,extendBoundary)

    // Initialize the filter weights
    def weights = filter(wins.head.length)
    def applyFilter(sig:Array[Double]): Array[Double] = sig.zip(weights).map(p => p._1 * p._2)
    // Apply the filters
    val filteredWins = wins.map( applyFilter )

    // Prepare the FFT function to apply to each window
    val fft = new FastFourierTransformer(fftNormalization)
    def ffti(s:Array[Double]) = fft.transform(s, TransformType.FORWARD)

    // Calculate the FFT for each window
    def ffts: Array[Array[Complex]] = filteredWins.map( ffti )
    (ffts, xa, cola)
  }

  /* Plotting */

  /**
    *
    * @see https://github.com/nicolas-f/JFreeSVG/blob/master/demo/src/main/java/org/jfree/graphics2d/demo/SVGChartWithAnnotationsDemo1.java
    * @param figure   - the Breeze-Viz figure
    * @param fileName - file to which pot is saved
    * @param dpi      - used for scaling the image.
    */
  def saveAsSVG(figure: Figure, fileName: String, dpi: Int = 72): Unit = {
    import org.jfree.graphics2d.svg.SVGGraphics2D
    import org.jfree.graphics2d.svg.SVGUtils

    // default dpi is 72
    val scale = dpi / 72.0
    val swidth = (figure.width * scale).toInt
    val sheight = (figure.height * scale).toInt


    val g2 = new SVGGraphics2D(swidth, sheight)
    g2.scale(scale, scale)
    figure.drawPlots(g2)
    val f = new File(fileName)
    SVGUtils.writeToSVG(f, g2.getSVGElement)
  }


  type FT[T <: pt.inescn.samplers.Base.LengthSample] =
    (Double, Int, Sampler[T, Double], Boolean, DftNormalization) => (Int, IndexedSeq[Double], Array[Complex])
  type FTA = (Double, Int, Array[Double], Boolean, DftNormalization) => (Int, IndexedSeq[Double], Array[Complex])


  /**
    * Plots the magnitude (amplitude) and phase (argument) of a Complex signal.
    * Can be used for example to show the decomposition of a signal using the
    * Fourier transform. Note that here we assume that `xa` has only half the
    * frequencies + DC component (real signal is symmetrical in frequency). Only
    * those are shown.
    *
    * @param xa - samples signal in the time domain
    * @param fftSig - the time domain signal decomposed into its Complex domain components
    * @param magnitudePlot - plot with the Complex magnitude
    * @param phasePlot - plot with the Complex phase (angle)
    */
  def magnitudeAndPhasePlots(xa: Seq[Double], fftSig: Array[Complex], magnitudePlot: Plot, phasePlot: Plot) : Unit = {
    val ya = Fourier.abs(fftSig)
    val yb = Fourier.phase(fftSig)

    magnitudePlot += plot(xa, ya.take(xa.length))
    magnitudePlot.title = "Abs(Complex)"
    //argPlot.refresh

    // FFT phase is noisy (not usable) when both ral and imaginary parts are (very close to) zero
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    phasePlot += plot(xa, yb.take(xa.length))
    phasePlot.title = "Theta(Complex)"
    //phasePlot.refresh
  }

  def magnitudeAndInstantaneousPhasePlots(xa: Seq[Double], fftSig: Array[Complex], magnitudePlot: Plot, phasePlot: Plot) : Unit = {
    val ya = Fourier.abs(fftSig)
    val yb = Fourier.instantaneousPhase(fftSig)

    magnitudePlot += plot(xa, ya.take(xa.length))
    magnitudePlot.title = "Abs(Complex)"
    //argPlot.refresh

    // FFT phase is noisy (not usable) when both ral and imaginary parts are (very close to) zero
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    phasePlot += plot(xa, yb.take(xa.length))
    phasePlot.title = " Instantaneous Theta(Complex)"
    //phasePlot.refresh
  }

  /**
    * Saves the plot in the figure `fig` to the file named `fileName`
    *
    * @param fig - Figure with the plots
    * @param fileName - filename to which to save the figure
    */
  def plotOrSave(fig: Figure, fileName: String) : Unit = {
    if (fileName != "") {
      if (fileName.toLowerCase.endsWith("svg"))
        saveAsSVG(fig, fileName)
      else
        fig.saveas(fileName)
    }
    else {
      fig.refresh
    }
  }


  /**
    * This function generates a plot with 2 separate graphs. The first the Complex transform's absolute value using
    * for example the Fourier transform components [[org.hipparchus.complex.Complex]] in `fftsig`. The frequency domain
    * used is found in `xa`. The second plot shows the Fourier transform's argument (phase) using for example the
    * Fourier transform components [[org.hipparchus.complex.Complex]] in `fftsig`.
    *
    * Here we only plot half the frequencies because we are assuming a real signal, s frequencies are
    * symmetrical around the DC component.
    *
    * If a `fileName` is provided then the lots are not shown, but they are saved to a file in SVG format.
    * Note that we included the sampling frequency and signal duration to add as titles to the plot.
    *
    * @param samplingFreq - signal sampling frequency in Hz
    * @param fftSig - signal in Fourier components (Complex)
    * @param title - figure's main title
    * @param fileName - save plot to this file
    * @return - a figure that can be shown and/or saved to a file
    */
  def plotMagnitudePhase(samplingFreq: Double, fftSig: Array[Complex], title : String = "", fileName: String = ""): Figure = {
    import breeze.plot._
    val fig = Figure( title )
    fig.visible = fileName == ""

    val p1 = fig.subplot(0)
    val p2 = fig.subplot(2, 1, 1)
    val xa = timeDomain( samplingFreq, fftSig.length)
    magnitudeAndPhasePlots(xa, fftSig, p1, p2)

    plotOrSave(fig, fileName)

    fig
  }

  def plotMagnitudeInstantaneousPhase(samplingFreq: Double, fftSig: Array[Complex], title : String = "", fileName: String = ""): Figure = {
    import breeze.plot._
    val fig = Figure( title )
    fig.visible = fileName == ""

    val p1 = fig.subplot(0)
    val p2 = fig.subplot(2, 1, 1)
    val xa = timeDomain( samplingFreq, fftSig.length)
    magnitudeAndInstantaneousPhasePlots(xa, fftSig, p1, p2)

    plotOrSave(fig, fileName)

    fig
  }

  /**
    * This function generates a plot with 3 separate graphs. The first shows the time domain signal
    * `sig` amd its domain `xx` (both have to have the same length). The second plot shows the Fourier
    * transform's absolute value using the Fourier transform components [[org.hipparchus.complex.Complex]]
    * in `fftsig`. The frequency domain used is found in `xa`. The third plot shows the Fourier transform's
    * argument (phase) using the Fourier transform components [[org.hipparchus.complex.Complex]] in `fftsig`.
    *
    * If a `fileName` is provided then the lots are not shown, but they are saved to a file in SVG format.
    * Note that we included the sampling frequency and signal duration to add as titles to the plot.
    *
    * @param samplingFreq - signal sampling frequency in Hz
    * @param duration - total duration of signal in seconds
    * @param xx - time domain
    * @param sig - signal in time domain
    * @param xa - frequency domain
    * @param fftSig - signal in Fourier components (Complex)
    * @param title - figure's main title
    * @param fileName - save plot to this file
    */
  def plotFourier(samplingFreq: Double, duration: Double,
                  xx: Seq[Double], sig: Array[Double],
                  xa: Seq[Double], fftSig: Array[Complex],
                  title : String = "", fileName: String = ""): Figure = {
    import breeze.plot._
    val fig = Figure( title )
    fig.visible = fileName == ""

    val p1 = fig.subplot(0)
    p1 += plot(xx, sig, name = s"@ $samplingFreq Hz")
    p1.title = s"Signals (t for ${xx.last} sec)"
    p1.legend = true
    // p1.refresh

    val p2 = fig.subplot(2, 1, 1)
    val p3 = fig.subplot(3, 1, 2)
    magnitudeAndPhasePlots(xa, fftSig, p2, p3)

    plotOrSave(fig, fileName)

    fig
  }

  /**
    * Takes in a signal `sig` in the time domain and applies the Fourier `transform` on it.
    * The time and frequency domain signals are then plotted as three separate plots: time domain,
    * frequency amplitude and frequency phase.
    *
    * The sampling frequency `samplingFreq` and `duration` are used calculate the delta time-steps
    * and the time domain shown in the plot. Note that we assume the duration is less than or equal
    * to the signal length. When smaller, the time domain plot only shows that specified duration.
    *
    * A Fourier `transform` is then applied (any function of type [[FTA]]). Note that in the case of
    * the [[FTA]] is a fast fourier transform that requires that the length of the signal be a power
    * of 2, the the samples signal length is truncated if required. In such cases, the frequency domain
    * (delta frequency steps) are also changed accordingly.
    *
    * Note that we either show the plot or save it to file, if a non emtpy filename is provided.
    *
    * @see [[plotFourier]]
    * @param transform - Fourier transform. May be a FFT or an DFT. It is responsible for setting the
    *                  appropriate signal length, if required.
    * @param samplingFreq - sampling frequency in Hz
    * @param duration - number of samples to use (time duration of the signal). Must be less than the signal length.
    * @param sig - signal sampled ion the time domain
    * @param fileName - save plot to this file
    */
  def plotFourierTransform(transform: FTA, samplingFreq: Double,
                           duration: Double,
                           sig: Array[Double],
                           title: String,
                           fileName: String = "",
                           pad : Boolean = false,
                           fftNormalization: DftNormalization=DftNormalization.STANDARD): Figure = {
    val maxDuration = Math.min( duration, sampledTime(samplingFreq, sig.length) )
    val (_, xx) = Fourier.samplingSpecification(samplingFreq, maxDuration)
    val (_, xa, fftSig) = transform(samplingFreq, xx.length, sig, pad, fftNormalization)
    val tmp = sig.slice(0, xx.length)
    plotFourier(samplingFreq, duration, xx, tmp, xa, fftSig, title, fileName)
  }

  /**
    * Takes in a signal `sig` in the time domain and applies the Fourier `transform` on it.
    * The time and frequency domain signals are then plotted as three separate plots: time domain,
    * frequency amplitude and frequency phase.
    *
    * The sampling frequency `samplingFreq` and `duration` are used calculate the delta time-steps
    * and the time domain shown in the plot. Note that we assume the duration is less than or equal
    * and the time domain shown in the plot. Note that we assume the duration is less than or equal
    * to the signal length. When smaller, the time domain plot only shows that specified duration.
    *
    * A Fourier `transform` is then applied (any function of type [[FTA]]). Note that in the case of
    * the [[FTA]] is a fast fourier transform that requires that the length of the signal be a power
    * of 2, the the samples signal length is truncated if required. In such cases, the frequency domain
    * (delta frequency steps) are also changed accordingly.
    *
    * Note that we either show the plot or save it to file, if a non emtpy filename is provided.
    *
    * @see [[plotFourier]]
    * @param transform - Fourier transform. May be a FFT or an DFT. It is responsible for setting the
    *                  appropriate signal length, if required.
    * @param samplingFreq - sampling frequency in Hz
    * @param duration - number of samples to use (time duration of the signal). Must be less than the signal length.
    * @param sig - signal sampled ion the time domain
    * @param fileName - save plot to this file
    */
  def plotFourierTransform[T <: LengthSample](transform: FT[T], samplingFreq: Double,
                                              duration: Double,
                                              sig: Sampler[T, Double],
                                              title: String,
                                              fileName: String,
                                              pad : Boolean,
                                              fftNormalization: DftNormalization): Figure = {
    val (dd, xx) = Fourier.samplingSpecification(samplingFreq, duration)
    val (len, xa, fftSig) = transform(samplingFreq, xx.length, sig, pad, fftNormalization)
    plotFourier(samplingFreq, duration, xx, sig(xx.length).toArray, xa, fftSig, title, fileName)
  }

  /**
    * Takes the frequency components' absolute values and plots them
    * in a heat-like map using a colour gradient. This plot can then
    * be viewed or saved to a file. Use this to plot the result of a
    * STFT (see [[stft]]. The input should be in a [[breeze.linalg.DenseMatrix]]
    * format.
    *
    * @param absFreq Absolute values fo the STFT frequency components
    * @param scale colour scale used to show the various values
    * @param title title of the plot
    * @param fileName save plot to this file
    * @return the plot
    */
  def plotSTFTMat(absFreq: DenseMatrix[Double], scale: GradientPaintScale[Double] = null,
                  title: String = "", fileName: String = ""): Figure = {

    import breeze.plot._
    val fig = Figure( title )
    fig.visible = fileName == ""

    val plot = fig.subplot(0)
    plot += image(absFreq, scale=scale)
    plot.title = title

    Fourier.plotOrSave(fig, fileName)
    fig
  }

  /**
    * Takes the frequency components' absolute values and plots them
    * in a heat-like map using a colour gradient. This plot can then
    * be viewed or saved to a file. Use this to plot the result of a
    * STFT (see [[stft]]. The input should be te absolute value of the
    * complex result generated by the [[stft]] function.
    *
    * @see [[abs]]
    *
    * @param abss The STFT frequency absolute value of the signal
    * @param scale colour scale used to show the various values
    * @param title title of the plot
    * @param fileName save plot to this file
    * @return the plot
    */
  def plotSTFTAbs(abss: Array[Array[Double]], scale: GradientPaintScale[Double] = null,
               title: String = "", fileName: String = "") : Figure = {

    // Real signal so only take half the frequencies + DC
    val freqDomainLen = abss(0).length / 2
    //println(s"freqDomainLen=$freqDomainLen")
    // Get only half of th required frequencies
    val absFreq = abss.map(_.take(freqDomainLen))

    // Create the dense matrix required by the Breeze plot
    val mat = DenseMatrix.zeros[Double](freqDomainLen, absFreq.length)
    // Fill them in with the absolute values of the frequency components
    absFreq.zipWithIndex.foreach( e => mat(::,e._2) += DenseVector(e._1) )

    // JPlot the STFT
    plotSTFTMat(mat, scale, title, fileName)
  }

  /**
    * Takes the frequency components' complex values and plots them
    * in a heat-like map using a colour gradient. This plot can then
    * be viewed or saved to a file. Use this to plot the result of a
    * STFT (see [[stft]]. The input should be in same format as that
    * generated by the [[stft]] function.
    *
    * @param img The STFT frequency and phase components of the signal
    * @param scale colour scale used to show the various values
    * @param title title of the plot
    * @param fileName save plot to this file
    * @return the plot
    */
  def plotSTFT(img: Array[Array[Complex]], scale: GradientPaintScale[Double] = null,
               title: String = "", fileName: String = "") : Figure = {

    val absFreq = img.map( Fourier.abs )
    plotSTFTAbs(absFreq, scale, title, fileName)
  }

}
