package pt.inescn.dsp



import inegi.utils.{Frmng, Read_Data_File}
import org.apache.commons.math3.analysis.interpolation.{BicubicInterpolator, LinearInterpolator, SplineInterpolator}
import org.apache.commons.math3.analysis.polynomials.PolynomialSplineFunction
import pt.inescn.plot.JPlot


/**  root/runMain pt.inescn.dsp.EmdTest
  *
  * @param MaxItera
  * @param MaxModes
  */
case class EMD(MaxItera:Int, MaxModes:Int) {


  def imfs(signal: Array[Double]): Array[Array[Double]] = {


    var imfs = new Array[Array[Double]](MaxModes)
    var k = 0
    var r = signal
    var m = signal
    var mp = signal
    var t = (0 until signal.length).toArray
    var nbit = 0
    var stop_sift = false
    var moyenne = new Array[Double](0)

    while (k < MaxModes || MaxModes == 0) {
      m = r
      mp = m

      moyenne = stop_sifting_fixe(t, m, "Linear", 4)

      while (nbit < MaxItera) {
        m = (m,moyenne).zipped.map(_-_)
        moyenne = stop_sifting_fixe(t, m, "Linear", 4)
        mp = m
        nbit += 1
      }

      imfs(k) = m
      k += 1
      r=(r,m).zipped.map(_-_)
      nbit=0
    }

    imfs
  }


  def stop_sifting_fixe(t:Array[Int],m:Array[Double],INTERP:String,ndirs:Int):  Array[Double] ={
    //[stop_sift,moyenne] = stop_sifting_fixe(t,m,INTERP,MODE_COMPLEX,ndirs);

    val ( envmoy, nem, nzm, amp) = mean_and_amplitude(m,t,INTERP,ndirs)

    envmoy

  }

  def mean_and_amplitude(m:Array[Double],t:Array[Int],INTERP:String,ndirs:Int): (Array[Double],Int,Int,Double) ={

    val NBSYM=2
    var (indmin,indmax,indzer)=extr2(m)

    val nem=indmin.length+indmax.length
    val nzm=indzer.length

     val  (tmin,tmax,mmin,mmax) = boundary_conditions(indmin,indmax,t,m,m,NBSYM);


    //val fig = new JPlot("");fig.figure()
    //var p= new Array[Double](indmin.length)
    //for(i<- 0 until indmin.length){}
    //fig.plot(indmin,m(indmin))


    /*val ( envmin , envmax ) = INTERP match{
      case "LINEAR" =>
        val interpolator = new LinearInterpolator()
        val envminFunc = interpolator.interpolate(tmin.map(_.toDouble),mmin)
        val envmaxFunc = interpolator.interpolate(tmax.map(_.toDouble),mmax)
        ( t.map( x => envminFunc.value(x) ) , t.map( x => envmaxFunc.value(x) )  )
      /*case "BICUBIC"  =>
        val interpolator = new BicubicInterpolator()
        ( interpolator.interpolate( tmin.map(_.toDouble) , mmin ) ,
          interpolator.interpolate( tmax.map(_.toDouble) , mmax ) )*/
      case "SPLINE" =>
        val interpolator = new SplineInterpolator()
        val envminFunc = interpolator.interpolate(tmin.map(_.toDouble),mmin)
        val envmaxFunc = interpolator.interpolate(tmax.map(_.toDouble),mmax)
        ( t.map( x => envminFunc.value(x) ) , t.map( x => envmaxFunc.value(x) )  )
    }*/



    /*val fig1 = new JPlot("");fig1.figure();fig1.holdon()
    fig1.plot(tmax.map(_.toDouble), mmax, "-b", 1.0.toFloat, "")
    fig1.plot(tmin.map(_.toDouble), mmin, "-r", 1.0.toFloat, "")*/


    //val interpolator = new LinearInterpolator()
    val interpolator = new SplineInterpolator()
    val envminFunc = interpolator.interpolate(tmin.map(_.toDouble),mmin)
    val envmaxFunc = interpolator.interpolate(tmax.map(_.toDouble),mmax)

    //fig1.close()

    val envmin = t.map(x => envminFunc.value(x))
    val envmax = t.map(x => envmaxFunc.value(x))
    var envmoy = (envmin,envmax).zipped.map((_*0.5 + _*0.5))
    var ampS = (envmin,envmax).zipped.map(_-_).map(Math.abs(_))
    var amp = ampS.sum/ampS.size/2


    // Visualization
    /*val fig = new JPlot("");fig.figure()
    var curves = new Array[Array[Double]](3)
    var x= (0 until envmin.length ).toArray.map(_.toDouble)
    curves(0)=envmin
    curves(1)=envmax
    curves(2)=envmoy

    fig.holdon()
    fig.plot(x, curves(0), "-b", 1.0.toFloat, "")
    fig.plot(x, curves(1), "-r", 1.0.toFloat, "")
    fig.plot(x, curves(2), "-k", 1.0.toFloat, "")
    fig.plot(x, m, "-g", 1.0.toFloat, "")
    fig.holdoff()
    Thread.sleep(5000)
    fig.close()*/



    (envmoy,nem,nzm,amp)

  }



  // Interpolation bound conditions
  def boundary_conditions(indmin:Array[Int],indmax:Array[Int],t:Array[Int],x:Array[Double],z:Array[Double],nbsym:Int): (Array[Int],Array[Int],Array[Double],Array[Double]) ={


    val lx=x.length-1
    val imaxlen = indmax.length
    val iminlen = indmin.length

    var lmax = new Array[Int](0)
    var lmin = new Array[Int](0)
    var lsym = 0
    var rmax = new Array[Int](0)
    var rmin = new Array[Int](0)
    var rsym = 0


    if (indmin.length + indmax.length < 3){
      println("not enough extrema")
    }

    // Left condictions
    if ( indmax(0) < indmin(0) ){
      if (x(0) > x(indmin(0)) ){
        lmax =  indmax.slice(1, Math.min(imaxlen-1,nbsym)+1 ).reverse
        lmin =  indmin.slice(0, Math.min(iminlen-1,nbsym-1)+1 ).reverse
        lsym =  indmax(0)

       /* println("gggggg")
        println(lmax.toVector)
        println(indmax.toVector)
        println(indmax.slice(1,2).toVector)*/

      }
      else{
        lmax =  indmax.slice(0,Math.min(imaxlen-1,nbsym-1)+1).reverse
        lmin =  indmin.slice(0,Math.min(iminlen-1,nbsym-2)+1).reverse ++ Array[Int](0)
        lsym = 0
      }
    }
    else {
      if (x(0) < x(indmax(0))){
        lmax =  indmax.slice(0,Math.min(imaxlen-1,nbsym-1)+1).reverse
        lmin =  indmin.slice(1,Math.min(iminlen-1,nbsym)+1).reverse
        lsym = indmin(0)
      }
      else{
        lmax = indmax.slice(0,Math.min(imaxlen-1,nbsym-2)+1).reverse ++ Array[Int](0)
        lmin = indmin.slice(0,Math.min(iminlen-1,nbsym-1)+1).reverse
        lsym = 0
      }
    }


    // Right conditions
    if (indmax(imaxlen-1) < indmin(iminlen-1)){
      if ( x(x.length-1) < x(indmax(imaxlen-1)) ){
        rmax =  indmax.slice( Math.max(imaxlen-nbsym,0) , indmax.length-1+1 ).reverse
        rmin =  indmin.slice( Math.max(iminlen-nbsym-1,0),iminlen-2 +1 ).reverse
        rsym = indmax(imaxlen-1)
      }
      else{
        rmax = Array[Int](lx) ++ indmax.slice( Math.max(imaxlen-nbsym+1,0) , imaxlen-1+1 ).reverse
        rmin =  indmin.slice( Math.max(iminlen-nbsym,0),iminlen-1+1).reverse
        rsym = lx
      }
    }
    else {
      if (x(x.length-1) < x(indmin(indmin.length-1))){
        rmax =  indmax.slice( Math.max(imaxlen-nbsym-1,0), imaxlen-2 +1 ).reverse
        rmin =  indmin.slice( Math.max(iminlen-nbsym,0),iminlen-1+1 ).reverse
        rsym = indmax(indmax.length-1)
      }
      else{
        rmax =  indmax.slice(Math.max(imaxlen-nbsym,0),imaxlen-1+1).reverse
        rmin =  Array[Int](lx) ++ indmin.slice(Math.max(iminlen-nbsym-1,0),iminlen-1+1).reverse
        rsym = lx
      }
    }

    var tlmin = lmin.map(2*t(lsym) - t(_))
    var tlmax = lmax.map(2*t(lsym) - t(_))
    var trmin = rmin.map(2*t(rsym) - t(_))
    var trmax = rmax.map(2*t(rsym) - t(_))


    /*println(tlmax.toVector)
    println(lmax.toVector)*/


    // in case symmetrized parts do not extend enough
    if(tlmin(0) > t(0) || tlmax(0) > t(0)){

      if (lsym == indmax(0))
        lmax= indmin.slice(0,Math.min(imaxlen-1,nbsym-1)+1).reverse
      else
        lmin = indmin.slice(0,Math.min(iminlen-1,nbsym-1)+1).reverse

      lsym = 0
      tlmin = lmin.map(2*t(lsym) - t(_))
      tlmax = lmax.map(2*t(lsym) - t(_))
    }

    /*println(trmin.toVector)
    println(trmin.length-1)
    println(trmax.toVector)
    println(trmax.length-1)*/


    if(trmin(trmin.length-1) < t(lx) || trmax(trmax.length-1) < t(lx)){

      if( rsym == indmax(indmax.length-1))
        rmax = indmax.slice(Math.max(imaxlen-nbsym,0),imaxlen-1+1).reverse
      else
        rmin = indmin.slice( Math.max(iminlen-nbsym,0),iminlen-1+1 ).reverse

      rsym = lx
      trmin = rmin.map(2*t(rsym) - t(_))
      trmax = rmax.map(2*t(rsym) - t(_))
    }


    val zlmax = lmax.map(z(_))
    val zlmin = lmin.map(z(_))
    val zrmax = rmax.map(z(_))
    val zrmin = rmin.map(z(_))


    /*val tmin = tlmin ++ indmin.map(t(_)) ++ trmin
    val tmax = tlmax ++ indmax.map(t(_)) ++ trmax

    val zmin = zlmin ++ indmin.map(z(_)) ++ zrmin
    val zmax = zlmax ++ indmax.map(z(_)) ++ zrmax*/


    val tmin = Array(t(0)) ++ indmin.map(t(_)) ++ Array(t(t.length-1))
    val tmax = Array(t(0)) ++ indmax.map(t(_)) ++ Array(t(t.length-1))

    val zmin = Array(z(0)) ++ indmin.map(z(_)) ++ Array(z(z.length-1))
    val zmax = Array(z(0)) ++ indmax.map(z(_)) ++ Array(z(z.length-1))


    (tmin,tmax,zmin,zmax)

  }



  def diff(array: Array[Double]): Array[Double] = {
    var out = new Array[Double](array.length - 1)
    var i = 1
    while (i < array.length) {
      out(i-1)= array(i) - array(i-1)
      i += 1
    }
    out
  }

  def diff(array: Array[Int]): Array[Int] = {
    var out = new Array[Int](array.length - 1)
    var i = 1
    while (i < array.length) {
      out(i-1)= array(i) - array(i-1)
      i += 1
    }
    out
  }





  // Find maxima,minima and zero cross indexes
  def extr(x:Array[Double]): (Array[Int],Array[Int],Array[Int]) ={

    val m = x.length
    var x1=x.slice(0,(m-2))
    var x2=x.slice(1,(m-1))

    var indzer=(x1,x2).zipped.map(_*_).zipWithIndex.filter(_._1 < 0).map(_._2)

    if (!x.find(_==0).isEmpty){

      var iz= x.zipWithIndex.filter(_._1 == 0).map(_._2)

      //var indz= new Array[Int](0)
      var indz = Array[Int]()

      if(diff(iz).find(_ == 1).isEmpty) {
        var zero = x.map(xx => if(xx==0) 0  else 1 )
        var dz = diff(Array(0) ++ zero ++ Array(0))
        var debz = dz.zipWithIndex.filter(_._1 == 1).map(_._2)
        var finz = dz.zipWithIndex.filter(_._1 == -1).map(_._2).map(xx => xx-1)
        indz = (debz, finz).zipped.map(_ + _).map(xx => Math.round(xx*0.5) ).map(_.toInt)
      }else{
        indz=iz
      }
      indzer= (indzer ++ indz).sorted
    }


    var d = diff(x)
    var n = d.length
    var d1 = d.slice(0,n-2)
    var d2 = d.slice(1,n-1)

    var d1d2=(d1,d2).zipped.map(_*_).map( xx => if(xx<0) 0  else 1)
    var d1v=d1.map( xx => if(xx<0) 0  else 1)
    var d1a=d1.map( xx => if(xx>0) 0  else 1)
    var indmin = (d1d2,d1v).zipped.map(_ * _).zipWithIndex.filter(_._1 == 1).map(_._2).map(xx => xx+1)
    var indmax = (d1d2,d1a).zipped.map(_ * _).zipWithIndex.filter(_._1 == 1).map(_._2).map(xx => xx+1)


    // when two or more successive points have the same value we consider only one extremum in the middle of the constant area
    //(only works if the signal is uniformly sampled)

    if (!d.find(_==0).isEmpty) {

      var imax= new Array[Int](0)
      var imin= new Array[Int](0)

      var bad= d.map(xx => if(xx==0) 0  else 1 )
      var dd = diff(Array(0) ++ bad ++ Array(0))
      var debs= dd.zipWithIndex.filter(_._1 == 1).map(_._2)
      var fins = dd.zipWithIndex.filter(_._1 == -1).map(_._2)


      if (debs(0) == 0){
        if (debs.length > 1){
          n=debs.length
          debs = debs.slice(1,debs.length-1)
          fins = fins.slice(1,fins.length-1)
        }
        else{
          debs= new Array[Int](0)
          fins= new Array[Int](0)
        }
      }

      if ( debs.length > 0) {
        if (fins(fins.length-1) == m-1) {
          if( debs.length > 1) {
            debs = debs.slice(0, debs.length - 2)
            fins = fins.slice(0, fins.length - 2)
          }
          else{
            debs= new Array[Int](0)
            fins= new Array[Int](0)
          }
        }
      }


      var lc = debs.length ;
      if (lc > 0) {
        var k=0
        while(k<lc) {
          if (d(debs(k) - 1) > 0) {
            if ( d(fins(k)) < 0 ) {
              imax = (imax ++ Array[Int](Math.round((fins(k) + debs(k)) / 2)) )
            }
            else if (d(fins(k)) > 0) {
              imin = (imin ++ Array[Int]( Math.round((fins(k) + debs(k)) / 2) ) )
            }
          }
          k=k+1
        }
      }


      if (imax.length > 0) {
        indmax=(indmax ++ imax).sorted
      }



      if (imin.length > 0) {
        indmin = (indmin ++ imin).sorted
      }

    }


    // Visualization
    /*val fig = new JPlot(" Extremes");fig.figure()
    var curves = new Array[Array[Double]](3)
    var xx= (0 until x.length ).toArray.map(_.toDouble)
    fig.holdon()
    fig.plot(indmin.map(_.toDouble),indmin.map(x(_)), ".bC", 1.0.toFloat, "")
    fig.plot(indmax.map(_.toDouble),indmax.map(x(_)), ".rC", 1.0.toFloat, "")
    fig.plot(xx,x, "-k", 1.0.toFloat, "")
    fig.holdoff()
    Thread.sleep(5000)
    fig.close()*/


    (indmin,indmax,indzer)


  }

  // Find maxima,minima and zero cross indexes
  def extr2(x:Array[Double]): (Array[Int],Array[Int],Array[Int]) = {

    val indmax =(1 to x.size - 2).filter(i => x(i - 1) < x(i) && x(i) > x(i + 1)).toArray
    val indmin =(1 to x.size - 2).filter(i => x(i - 1) > x(i) && x(i) < x(i + 1)).toArray

    // Visualization
    /*val fig = new JPlot(" Extremes");fig.figure()
    var curves = new Array[Array[Double]](3)
    var xx= (0 until x.length ).toArray.map(_.toDouble)
    fig.holdon()
    fig.plot(indmin.map(_.toDouble),indmin.map(x(_)), ".bC", 1.0.toFloat, "")
    fig.plot(indmax.map(_.toDouble),indmax.map(x(_)), ".rC", 1.0.toFloat, "")
    fig.plot(xx,x, "-k", 1.0.toFloat, "")
    fig.holdoff()
    Thread.sleep(5000)
    fig.close()*/

    (indmin,indmax,Array(0,6,3,2,4,2,3))
  }




}

object EmdTest{
  def main(args: Array[String]): Unit = {

    // variaveis constantes
    //val Path="E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv2/rol1_1krpm_0hp_norm_ens1.csv"
    val Path="E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv/Rol1_1000_CmDn_Ens1.csv"
    val PathDst= "E:/1_WORKSPACE/2_MAESTRA/5_ASSUNTOS/CESE/1_DATASETS/Db_JoelAntunes2_csv/Rol1_1000_CmDn_Ens1_test.csv";
    var FrmSz=256
    var FrmStp=32

    // Reads the data set
    //---------------------
    val rdf = new Read_Data_File(Path, 1, 1)
    /*for( i <- 0 to rdf.Table.size()-1 ) {
      for (j <- 0 to rdf.Hdr.length-1)
        print(rdf.Table.get(i)(j) + " ")
      print("\n")
    }*/

    var Sgnl= new Array[Double](rdf.Table.size())


    for ( i <- 0 to rdf.Table.size()-1 )
      Sgnl(i)=rdf.Table.get(i)(2)


    /*for( i <- 0 to Sgnl.length-1)
      println(Sgnl(i))*/


    //Performs framming
    //---------------------
    //Windowing
    val wndw= Array.fill[Double](FrmSz)(1) //Rectangular window(default)
    val ff= new Frmng
    val frm=ff.FrmngFunc(Sgnl,FrmSz,FrmStp,wndw)

    /*for( i <- 0 to frm.length-1){
      for( j <- 0 to frm(0).length-1)
         print( frm(i)(j) +" ")
      print("\n")
    }*/


    //Computes the  LFCC
    //---------------------
    val Fs=5100
    val NmbFltrs=5
    val MaxFrq=2550
    val MinFrq=10
    val NmbCpCoe=5
    val EMDFeatclss= EMD(2000,30)


    val fig = new JPlot("")
    fig.figure()
    val x = (0 until FrmSz ).toArray.map(_.toDouble)

    for(i<- 0 until frm.length) {
      val imfs = EMDFeatclss.imfs(frm(i))
      fig.wiggle(x,imfs)
      Thread.sleep(5000)
    }

    /*for(k <- 0 to emdFeat.length-1)
      print( emdFeat(k) + " " )

    //Hdr={'c1','c2','c3','c4','c5'}
    //val wdf = new Write_Data_File(PathDst, rdf.Hdr, rdf.Table)
  }*/

  }

}
