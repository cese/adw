/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.samplers

import java.time.{Duration, Instant}

import pt.inescn.samplers.Base.Unbounded
import pt.inescn.dsp.Fourier


/**
  * Created by hmf on 21-06-2017.
  */
object Function {


  /**
    * This is a generic function. It takes a `func` that takes as a parameter
    * a domain value (x values) and maps it to ist codomain (y value). It also
    * take son a `nextDomain`that takes as input an accumulator (the last
    * domain value that was generated and a parameter (`delta`)) and generates
    * the next domain value. The zero function is used generate the first
    * domain value (usually `-delta`) so that a call to next domain value starts
    * a `0`. This ensures that a first call to next will return the proper
    * codomain value when `current` is called.
    *
    *
    * @param func - function that generates the y (codomain) values
    * @param nextDomain - function that generates the x (domain) values
    * @param zero - start just before the domain's `0` (usually 0 - delta).
    * @param delta - steps in domain
    * @param curr
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    * @tparam B - parameters for generating next codomain value
    * @tparam C - current domain value
    */
  class Func[A,B,C](val func: C => A, val nextDomain : (C,B) => C, val zero : B => C, val delta : B, val curr: C)
    extends Unbounded[A] {

    override def reset: Func[A,B,C] = new Func[A,B,C](func, nextDomain, zero, delta, zero(delta))
    override def finished: Boolean = false
    override def current: A = func(curr)
    override def next: Func[A,B,C] = new Func[A,B,C](func, nextDomain, zero, delta, nextDomain(curr, delta))
  }


  /**
    * Ensures that a sampler starts at the correct time (needs to start before the 0)
    * @param delta - sampling rate in seconds
    * @return
    */
  def zeroFunc(delta:Double): Double = -delta

  /**
    * Sampling functions for oscillators
    *
    * @param t - current time step
    * @param dt - delta to move to the next timestamp
    * @return
    */
  def deltaFunc(t: Double, dt: Double): Double = dt + t
  def sinFunc(frequency: Double, amplitude : Double, phase: Double)(t:Double): Double =
    amplitude * Math.sin((2 * Math.PI * frequency * t) + phase)
  def cosFunc(frequency: Double, amplitude : Double, phase: Double)(t:Double): Double =
    amplitude * Math.cos((2 * Math.PI * frequency * t) + phase)

  /**
    * Sin function sampler.
    *
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param delta - sampling rate at whch to generate data (see [[pt.inescn.dsp.Fourier.samplingSpecification]]
    * @param curr - samplesTo at which top start. Mote that this should be one [[delta]] before the actual start
    */
  case class Sin(frequency: Double, amplitude : Double, phase: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double]( sinFunc(frequency, amplitude, phase), deltaFunc, zeroFunc, delta, curr) {
  }

  /**
    * Facilitate object creation by providing default values. Makes sure the initial sampler domain
    * value is correct (see -delta). Also sets frequency to Nyquist's frequency.
    *
    */
  object Sin {
    /**
      * Note that when we samplesTo this data [[pt.inescn.samplers.Base.Sampler.toStream]] and
      * [[pt.inescn.samplers.Base.Sampler.toList]] step to the next state (value). So
      * we need to start one delta before, otherwise we miss that samplesTo.
      *
      * @param frequency - frequency at which the wave oscillates
      * @param amplitude - maximum amplitude of the wave
      * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
      * @param delta - sampling rate at which to generate data (see [[pt.inescn.dsp.Fourier.samplingSpecification]]
      */
    def apply(frequency: Double, amplitude: Double, phase: Double, delta: Double): Sin =
      new Sin(frequency, amplitude, phase, delta, -delta)

    def apply(frequency: Double, amplitude: Double, phase: Double): Sin = {
      val delta = 1.0 / (2.0*frequency)
      new Sin(frequency, amplitude, phase, delta, -delta)
    }
  }

  /**
    * Cosine function sampler.
    *
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param delta - sampling rate at whch to generate data (see [[pt.inescn.dsp.Fourier.samplingSpecification]]
    * @param curr - samplesTo at which top start. Mote that this should be one [[delta]] before the actual start
    */
  case class Cos(frequency: Double, amplitude : Double, phase: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double]( cosFunc(frequency, amplitude, phase), deltaFunc, zeroFunc, delta, curr) {
  }

  /**
    * Facilitate object creation by providing default values. Makes sure the initial sampler domain
    * value is correct (see -delta). Also sets frequency to Nyquist's frequency.
    *
    */
  object Cos {
    /**
      * Note that when we samplesTo this data [[pt.inescn.samplers.Base.Sampler.toStream]] and
      * [[pt.inescn.samplers.Base.Sampler.toList]] step to the next state (value). So
      * we need to start one delta before, otherwise we miss that samplesTo.
      *
      * @param frequency - frequency at which the wave oscillates
      * @param amplitude - maximum amplitude of the wave
      * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
      * @param delta - sampling rate at whch to generate data (see [[pt.inescn.dsp.Fourier.samplingSpecification]]
      */
    def apply(frequency: Double, amplitude: Double, phase: Double, delta: Double): Cos =
      new Cos(frequency, amplitude, phase, delta, -delta)

    def apply(frequency: Double, amplitude: Double, phase: Double): Cos = {
      val delta = 1.0 / (2.0*frequency)
      new Cos(frequency, amplitude, phase, delta, -delta)
    }
  }

  /**
    * Returns a constant value
    *
    * @param const - constant value to return
    * @param delta - sampling rate in seconds
    * @param curr - current timestamp
    */
  case class Const(const: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double](_ => const, deltaFunc, zeroFunc, delta, curr) {
  }

  object Const {
    def apply(const: Double, delta: Double): Const = new Const(const, delta, -delta)
  }


  /**
    * y(t) = m*t + c
    */
  def lineFunc(m: Double, c: Double)(t:Double) : Double = m*t + c
  /**
    * y(t) = c*exp(n*t)
    */
  def expFunc(n: Double, c: Double)(t:Double) : Double = c*Math.exp(n*t)
  /**
    * y(t) = c*power(base, n*t)
    */
  def powerFunc(base:Double, n: Double, c: Double)(t:Double) : Double = c*Math.pow(base, n*t)
  /**
    * y(t) = c*log_b(nt)
    */
  def logFunc(n: Double, b: Double, c: Double)(t:Double) : Double = c*(Math.log(n*t)/Math.log(b))


  /**
    * @see [[lineFunc]]
    */
  case class Line(m: Double, c: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double]( lineFunc(m,c), deltaFunc, zeroFunc, delta, curr) {
  }

  /**
    * @see [[lineFunc]]
    */
  object Line {
    def apply(m: Double, c: Double, delta: Double): Line = Line(m, c, delta, -delta)
  }

  /**
    * @see [[expFunc]]
    */
  case class Exp(n: Double, c: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double]( expFunc(n,c), deltaFunc, zeroFunc, delta, curr) {
  }

  /**
    * @see [[expFunc]]
    */
  object Exp {
    def apply(n: Double, c: Double, delta: Double) : Exp = Exp(n, c, delta, -delta)
  }

  /**
    * @see [[powerFunc]]
    */
  case class Power(base:Double, n: Double, c: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double]( powerFunc(base,n,c), deltaFunc, zeroFunc, delta, curr) {
  }

  /**
    * @see [[powerFunc]]
    */
  object Power {
    def apply(base: Double, n: Double, c: Double, delta: Double) : Power = Power(base, n, c, delta, -delta)
  }


  /**
    * @see [[logFunc]]
    */
  case class Log(n: Double, b: Double, c: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double]( logFunc(n,b,c), deltaFunc, zeroFunc, delta, curr) {
  }

  /**
    * @see [[logFunc]]
    */
  object Log {
    def apply(n: Double, b: Double, c: Double, delta: Double): Log = Log(n, b, c, delta, -delta)
  }


  /**
    * Ensures that a samper starts at the correct time (needs to start before the 0)
    * @param start - initial timestamp
    * @param delta - sampling rate in seconds
    * @return
    */
  def zeroFunc(start: Instant)(delta:Duration): Instant = start.minusNanos(delta.toNanos)
  def timeStampFunc(a:Instant): Instant = a
  def timeStampDeltaFunc(curr: Instant, delta: Duration): Instant = curr.plusNanos(delta.toNanos)

  /**
    * Generates timestamps ([[https://docs.oracle.com/javase/8/docs/api/java/time/Instant.html java.time.Instant]]
    * starts at `start`. The timestamps are generated at the requested samplesTo rate defined by the [[delta]]
    *
    * @param start - time at wich t start generating the timestamps
    * @param delta - sampling rate
    * @param curr - current time stamp. Next timestamp is curr + delta.
    * @return - a sequence of timestamps at a specified rate
    */
  case class TimeStamp(start: Instant, override val delta : Duration, override val curr: Instant)
    extends Func[Instant, Duration, Instant](
      timeStampFunc,
      timeStampDeltaFunc,
      zeroFunc(start),
      delta, curr) {
  }

  object TimeStamp {
    def apply(start: String, delta: Double): TimeStamp = {
      val startInstant = Instant.parse(start)
      val sampling_rate = Fourier.convertFractionSecondsToDuration(delta)
      TimeStamp(startInstant, sampling_rate, zeroFunc(startInstant)(sampling_rate))
    }
    def apply(start: Instant, delta: Double): TimeStamp = {
      val sampling_rate = Fourier.convertFractionSecondsToDuration(delta)
      TimeStamp(start, sampling_rate, zeroFunc(start)(sampling_rate))
    }
    def apply(start: Instant, delta: Duration): TimeStamp = {
      TimeStamp(start, delta, zeroFunc(start)(delta))
    }
    def apply(start: String, delta: Duration): TimeStamp = {
      val startInstant = Instant.parse(start)
      TimeStamp(startInstant, delta, zeroFunc(startInstant)(delta))
    }
  }


  /**
    * @see https://en.wikipedia.org/wiki/Chirp
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param deltaAmp - ratio of increase in amplitude
    * @param deltaFreq - ratio of increase in frequency
    * @param t - current time
    * @return
    * @see [[linearChirpFunc]]
    */
  def linearChirpFunc(frequency: Double, amplitude : Double, phase: Double, deltaAmp: Double, deltaFreq: Double)(t:Double): Double = {
    val currentAmplitude = (t * deltaAmp) + amplitude
    val currentFrequency = (t * deltaFreq) + frequency
    currentAmplitude * Math.sin((2 * Math.PI * currentFrequency * t) + phase)
  }

  /**
    * @see https://en.wikipedia.org/wiki/Chirp
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param deltaAmp - ratio of increase in amplitude
    * @param deltaFreq - ratio of increase in frequency
    * @param t - current time
    * @return
    * @see [[linearChirpFunc]]
    */
  def expChirpFunc(frequency: Double, amplitude : Double, phase: Double, deltaAmp: Double, deltaFreq: Double)(t:Double): Double = {
    val currentAmplitude = amplitude * Math.pow(deltaAmp, t)
    val currentFrequency = frequency * Math.pow(deltaFreq, t)
    currentAmplitude * Math.sin((2 * Math.PI * currentFrequency * t) + phase)
  }

  /**
    * Generates a chirp signal with a linear growth in frequency and/or amplitude. For no growth, set the
    * values of [[deltaAmp]] and [[deltaFreq]] to greater than 1. For a constant value, use a value of `1.0`.
    * For a reduction, use a value less than `1.0.`
    *
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param delta - sampling rate at whch to generate data (see [[pt.inescn.dsp.Fourier.samplingSpecification]]
    * @param curr - samplesTo at which top start. Mote that this should be one [[delta]] before the actual start
    * @param deltaAmp - linear scale increment to amplitude
    * @param deltaFreq - linear scale increment to frequency
    * @see [[linearChirpFunc]]
    */
  case class LinearChirp(frequency: Double, amplitude : Double, phase: Double, deltaAmp: Double, deltaFreq: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double](
      linearChirpFunc(frequency, amplitude, phase, deltaAmp, deltaFreq), deltaFunc, zeroFunc, delta, curr) {
  }

  object LinearChirp {
    /**
      * Note that when we samplesTo this data [[pt.inescn.samplers.Base.Sampler.toStream]] and
      * [[pt.inescn.samplers.Base.Sampler.toList]] step to the next state (value). So we need to start one
      * delta before, otherwise we miss that samplesTo.
      *
      * @param frequency - frequency at which the wave oscillates
      * @param amplitude - maximum amplitude of the wave
      * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
      * @param delta - sampling rate at whch to generate data (see [[pt.inescn.dsp.Fourier.samplingSpecification]]
      */
    def apply(frequency: Double, amplitude: Double, phase: Double, deltaAmp: Double, deltaFreq: Double, delta: Double): LinearChirp =
      new LinearChirp(frequency, amplitude, phase, deltaAmp, deltaFreq, delta, -delta)

    def apply(frequency: Double, amplitude: Double, phase: Double, deltaAmp: Double, deltaFreq: Double): LinearChirp = {
      val delta = 1.0 / (2.0*frequency)
      new LinearChirp(frequency, amplitude, phase, deltaAmp, deltaFreq, delta, -delta)
    }
  }

  /**
    * Generates a chirp signal with a linear growth in frequency and/or amplitude. For no growth, set the
    * values of [[deltaAmp]] and [[deltaFreq]] to greater than 1. For a constant value, use a value of `1.0`.
    * For a reduction, use a value less than `1.0.`
    *
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param delta - sampling rate at whch to generate data (see [[pt.inescn.dsp.Fourier.samplingSpecification]]
    * @param curr - samplesTo at which top start. Mote that this should be one [[delta]] before the actual start
    * @param deltaAmp - linear scale increment to amplitude
    * @param deltaFreq - linear scale increment to frequency
    * @see [[linearChirpFunc]]
    */
  case class ExpChirp(frequency: Double, amplitude : Double, phase: Double, deltaAmp: Double, deltaFreq: Double, override val delta : Double, override val curr: Double)
    extends Func[Double, Double,Double](
      expChirpFunc(frequency, amplitude, phase, deltaAmp, deltaFreq), deltaFunc, zeroFunc, delta, curr) {
  }

  object ExpChirp {
    /**
      * Note that when we samplesTo this data [[pt.inescn.samplers.Base.Sampler.toStream]] and
      * [[pt.inescn.samplers.Base.Sampler.toList]] step to the next state (value). So
      * we need to start one delta before, otherwise we miss that samplesTo.
      *
      * @param frequency - frequency at which the wave oscillates
      * @param amplitude - maximum amplitude of the wave
      * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
      * @param delta - sampling rate at whch to generate data (see [[pt.inescn.dsp.Fourier.samplingSpecification]]
      */
    def apply(frequency: Double, amplitude: Double, phase: Double, deltaAmp: Double, deltaFreq: Double, delta: Double): ExpChirp =
      new ExpChirp(frequency, amplitude, phase, deltaAmp, deltaFreq, delta, -delta)

    def apply(frequency: Double, amplitude: Double, phase: Double, deltaAmp: Double, deltaFreq: Double): ExpChirp = {
      val delta = 1.0 / (2.0*frequency)
      new ExpChirp(frequency, amplitude, phase, deltaAmp, deltaFreq, delta, -delta)
    }
  }

}
