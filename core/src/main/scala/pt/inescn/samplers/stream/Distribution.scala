/** *****************************************************************************
  * Copyright (C) 2018 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.samplers.stream

import org.hipparchus.random.{JDKRandomGenerator, RandomDataGenerator, RandomGenerator}
import pt.inescn.utils.RandomCollection

import scala.collection.AbstractIterator

object Distribution {

  def getSeed: Long = {
    val now = java.time.LocalDateTime.now
    val seed = now.toEpochSecond(java.time.ZoneOffset.UTC)
    seed
  }

  def getGenerator(seed: Long): RandomDataGenerator = RandomDataGenerator.of( new JDKRandomGenerator(seed) )
  def getGenerator: RandomDataGenerator = RandomDataGenerator.of( new JDKRandomGenerator(getSeed) )

  // Real Distributions

  def normal(mean: Double, sd: Double)(implicit rnd : RandomGenerator): Iterable[Double] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextNormal(mean,sd) ).toIterable
  }

  def uniform(lower: Double, upper: Double)(implicit rnd : RandomGenerator): Iterable[Double] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextUniform(lower,upper) ).toIterable
  }

  def gamma(shape: Double, scale: Double)(implicit rnd : RandomGenerator): Iterable[Double] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextGamma(shape,scale) ).toIterable
  }

  /**
    *
    * @param shape - also referred to as the standard deviation
    * @param scale - also referred to as the scale
    * @param rnd - random generator (use constant seed for testing)
    */
  def logNormal(shape: Double, scale: Double)(implicit rnd : RandomGenerator): Iterable[Double] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextLogNormal(shape, scale) ).toIterable
  }

  def exponential(mean: Double)(implicit rnd : RandomGenerator): Iterable[Double] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextExponential(mean) ).toIterable
  }

  def beta(alpha: Double, beta: Double)(implicit rnd : RandomGenerator): Iterable[Double] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextBeta(alpha, beta) ).toIterable
  }

  // Integral

  def zipf(numberOfElements: Int, exponent: Double)(implicit rnd : RandomGenerator): Iterable[Int] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextZipf(numberOfElements, exponent) ).toIterable
  }

  def poisson(mean: Double)(implicit rnd : RandomGenerator): Iterable[Int] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextPoisson(mean) ).toIterable
  }

  def intUniform(start: Int, end: Int)(implicit rnd : RandomGenerator): Iterable[Int] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextInt(start,end) ).toIterable
  }

  // Long distributions

  def longUniform(start: Long, end: Long)(implicit rnd : RandomGenerator): Iterable[Long] = {
    val gen: RandomDataGenerator = RandomDataGenerator.of(rnd)
    Iterator.continually( gen.nextLong(start,end) ).toIterable
  }

  // Proportional Discrete distributions

  def proportional[A](mp : Map[A,Double] = Map[A,Double]())(implicit rnd : RandomGenerator): Iterable[A] = {
    val convertable = new Iterable[A] {
      private val rndColl = RandomCollection[A](mp, rnd)

      override def iterator: Iterator[A] = {

        val converter: AbstractIterator[A] = new AbstractIterator[A] {

          override def hasNext: Boolean = true
          override def next():A = rndColl.next
        } // iterator
        converter
      }
    }
    convertable
  }

  // Mixed distributions

  def mixed[A](mp : Map[Iterable[A],Double] = Map[Iterable[A],Double]())(implicit rnd : RandomGenerator):
  Iterable[A] = {
    val convertable = new Iterable[A] {
      private val mpi = mp.map{ case (i,v) => (i.toIterator, v) }
      private val rndColl = RandomCollection[Iterator[A]](mpi, rnd)

      override def iterator: Iterator[A] = {

        val converter: AbstractIterator[A] = new AbstractIterator[A] {

          override def hasNext: Boolean = true
          override def next():A = rndColl.next.next
        } // iterator
        converter
      }
    }
    convertable
  }

}
