/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.samplers.stream

import java.time.{Duration, Instant, ZonedDateTime}

import pt.inescn.etl.stream.Load.Row

import scala.collection.AbstractIterator

object Enumeration {

  def continually[A](const:A): Iterable[A] = Iterator.continually(const).toIterable

  def forever[A](const:List[A]): Iterable[A] = {

    val loop = new Iterable[A] {

      val looper: AbstractIterator[A] = new AbstractIterator[A] {
        var iter = const.toIterator
        override def hasNext: Boolean = {
          if (iter.hasNext)
            true
          else {
            iter = const.iterator
            iter.hasNext
          }
        }

        override def next(): A = iter.next()
      }
      override def iterator: Iterator[A] = looper
    }
    loop
  }

  def range(start:Double, end:Double, step: Double): Iterable[Double] =
    Ops.map(BigDecimal(start) to end by step)( (in:BigDecimal) => in.toDouble)

  def from(start:Double, step: Double): Iterable[Double] = Stream.iterate(start)( i => i + step)


  def range(start:Int, end:Int, step: Int = 1): Iterable[Int] = Iterable.range(start,end+step,step)
  def from(start:Int, step: Int = 1): Iterable[Int] = Iterator.from(start,step).toIterable

  def from(start: Instant, delta: Duration): Iterator[Instant] = Iterator.iterate(start)(c => c.plus(delta) )
  def fromZone(start: ZonedDateTime, delta: Duration): Iterator[ZonedDateTime] = Iterator.iterate(start)(c => c.plus(delta) )
  def sampleZone(): Iterator[ZonedDateTime] = Iterator.continually(ZonedDateTime.now())

}
