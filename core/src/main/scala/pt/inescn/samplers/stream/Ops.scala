package pt.inescn.samplers.stream

import scala.collection.AbstractIterator
import scala.util.Random

object Ops {

  /*
     Flattening arbitrarily nested tuples
     Macro magic with Shapeless
     see: https://github.com/milessabin/shapeless/blob/master/examples/src/main/scala/shapeless/examples/flatten.scala
   */

  import shapeless._
  import ops.tuple.FlatMapper
  //import syntax.std.tuple._


  /*
     We need to flatten arbitrarily nested tuples. However the example in:
       https://github.com/milessabin/shapeless/blob/master/examples/src/main/scala/shapeless/examples/flatten.scala
     does not because it will also flatten case classes within the tuples. This is because case
     classes automatically extend Product and Serializable. To solve the issue we mus explicitly
     match tuples by adding `IsTuple`. Example above only requires this change in `Flatten`

      see https://stackoverflow.com/questions/52836858/flattening-arbitrarily-nested-tuples-containing-case-class-elements-using-shapel
   */

  trait LowPriorityFlatten extends Poly1 {
    implicit def default[T] = at[T](Tuple1(_))
  }
  object Flatten extends LowPriorityFlatten {
    implicit def caseTuple[P <: Product : IsTuple](implicit lfm: Lazy[FlatMapper[P, Flatten.type]]) =
      at[P](lfm.value(_))
  }

  type InTuple[T] = shapeless.poly.Case[Flatten.type, T::HNil]
  type OutTuple[T] = PolyDefns.Case[Flatten.type, T :: HNil]#Result

  def flatten[T](i:Iterable[T])(implicit cse : InTuple[T]) : Iterable[cse.Result] = {
    val convertable: Iterable[cse.Result] = new Iterable[cse.Result] {

      override def iterator: AbstractIterator[cse.Result] = {

        val converter: AbstractIterator[cse.Result] = new AbstractIterator[cse.Result] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): cse.Result = {
            val in = iter1.next()
            Ops.Flatten(in)
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  def flattenByMAp[A](i:Iterable[A])(implicit cse : InTuple[A]): Iterable[cse.Result] = map[A, cse.Result](i)(e => Flatten(e))

  def empty[A]: Iterable[A] = Iterable.empty

  def map[A,B](i: Iterable[A])(f: A => B): Iterable[B] = {
    val convertable = new Iterable[B] {
      override def iterator: Iterator[B] = {
        val converter: AbstractIterator[B] = new AbstractIterator[B] {
          private val iter = i.toIterator
          override def hasNext: Boolean = iter.hasNext
          override def next(): B = f(iter.next())
        } // iterator
        converter
      }
    }
    convertable
  }

  def until[A,B](i: Iterable[A], f: (B,A) => (B,Boolean), zero:B): Iterable[A] = {
    val convertable = new Iterable[A] {
      override def iterator: Iterator[A] = {
        val converter: AbstractIterator[A] = new AbstractIterator[A] {
          private val iter = i.toIterator
          private var stop = false
          private var nxt = iter.next()
          private var z: B = zero

          override def hasNext: Boolean = !stop && iter.hasNext
          override def next(): A = {
            val tmp = nxt
            nxt = if (iter.hasNext) iter.next() else nxt
            val r = f(z, nxt)
            z = r._1
            stop = r._2
            tmp
          }
        } // iterator
        converter
      }
    }
    if (i.iterator.hasNext) convertable else empty
  }

  def randomize[A,B](i: Iterable[A]): Iterable[A] = {
    val convertable = Random.shuffle(i)
    if (i.iterator.hasNext) convertable else empty
  }


  def repeat[A,B](i: Iterable[A]): Iterable[A] = {
    val convertable = new Iterable[A] {
      override def iterator: Iterator[A] = {
        val converter: AbstractIterator[A] = new AbstractIterator[A] {
          private var iter = i.toIterator
          override def hasNext: Boolean = {
            if (!iter.hasNext) iter = i.toIterator
            true
          }
          override def next(): A = iter.next()
        } // iterator
        converter
      }
    }
    convertable
  }

  def repeatFor[A,B](n: Int)(i: Iterable[A]): Iterable[A] = {
    val convertable = new Iterable[A] {
      override def iterator: Iterator[A] = {
        var cnt = 0
        val converter: AbstractIterator[A] = new AbstractIterator[A] {
          private var iter = i.toIterator
          override def hasNext: Boolean = {
            if (!iter.hasNext) {
              iter = i.toIterator
              cnt = cnt + 1
            }
            if (cnt < n) true else false
          }
          override def next(): A = iter.next()
        } // iterator
        converter
      }
    }
    convertable
  }

  def cartesian[A,B](i1:Iterable[A], i2:Iterable[B]): Iterable[(A,B)] = {
    if (!i1.iterator.hasNext || !i2.iterator.hasNext)
      empty[(A,B)]
    else {
      val convertable = new Iterable[(A,B)] {

        override def iterator: Iterator[(A,B)] = {

          val converter: AbstractIterator[(A, B)] = new AbstractIterator[(A,B)] {
            private var iter1 = i1.iterator
            private val iter2 = i2.iterator
            private var b: B = iter2.next() // TODO: will fail with exception if empty !!

            override def hasNext: Boolean = iter1.hasNext || iter2.hasNext

            override def next(): (A,B) = {
              if (!iter1.hasNext){
                iter1 = i1.iterator
                val a = iter1.next()
                b = iter2.next()
                (a, b)
              } else {
                val a = iter1.next()
                (a, b)
              }
            } // next
          } // iterator
          converter
        }
      }
      convertable
    }
  }

  def and[A,B](i1:Iterable[A], i2:Iterable[B]): Iterable[(A,B)] = {
    val convertable = new Iterable[(A,B)] {

      override def iterator: Iterator[(A,B)] = {

        val converter: AbstractIterator[(A, B)] = new AbstractIterator[(A,B)] {
          private val iter1 = i1.iterator
          private val iter2 = i2.iterator

          override def hasNext: Boolean = iter1.hasNext && iter2.hasNext
          override def next(): (A,B) = (iter1.next(), iter2.next())
        } // iterator
        converter
      }
    }
    if (!i1.iterator.hasNext || !i2.iterator.hasNext) empty[(A,B)] else convertable
  }

  def or[A,B](i1:Iterable[A], i2:Iterable[B]): Iterable[(A,B)] = {
    val convertable = new Iterable[(A,B)] {

      override def iterator: Iterator[(A,B)] = {

        val converter: AbstractIterator[(A, B)] = new AbstractIterator[(A,B)] {
          private var iter1 = i1.iterator
          private var iter2 = i2.iterator

          override def hasNext: Boolean = iter1.hasNext || iter2.hasNext
          override def next(): (A,B) =
            (iter1.hasNext, iter2.hasNext) match {
              case (true, true) =>
                (iter1.next, iter2.next)
              case (false, false) =>
                throw new RuntimeException("or(Iterable, Iterable) has both empty iterators: should not occur")
              case (false, true) =>
                iter1 = i1.iterator
                (iter1.next, iter2.next)
              case (true, false) =>
                iter2 = i2.iterator
                (iter1.next, iter2.next)
            }
        } // iterator
        converter
      }
    }
    if (!i1.iterator.hasNext || !i2.iterator.hasNext) empty[(A,B)] else convertable
  }


  def pairLeft[A,B](i1:Iterable[A], i2:Iterable[Iterable[B]]): Iterable[(A,B)] = {
    val convertable = new Iterable[(A,B)] {

      override def iterator: Iterator[(A,B)] = {

        val converter: AbstractIterator[(A, B)] = new AbstractIterator[(A,B)] {
          private val iter1 = i1.iterator
          private val iter2: Iterator[Iterable[B]] = i2.iterator

          private var inner = if (iter2.hasNext) iter2.next().toIterator else empty.toIterator
          private var left = iter1.next()

          override def hasNext: Boolean =
            iter1.hasNext && iter2.hasNext || inner.hasNext

          override def next(): (A,B) = {
            left = if (inner.hasNext) left else iter1.next()
            inner = if (inner.hasNext) inner else iter2.next().toIterator
            val right: B = inner.next()
            (left, right)
          }
        } // iterator
        converter
      }
    }
    if (!i1.iterator.hasNext) empty else convertable
  }

  def pairRight[A,B](i2:Iterable[Iterable[B]], i1:Iterable[A]): Iterable[(B,A)] = {
    val convertable = new Iterable[(B,A)] {

      override def iterator: Iterator[(B,A)] = {

        val converter: AbstractIterator[(B,A)] = new AbstractIterator[(B,A)] {
          private val iter1 = i1.iterator
          private val iter2: Iterator[Iterable[B]] = i2.iterator

          private var inner = if (iter2.hasNext) iter2.next().toIterator else empty.toIterator
          private var left = iter1.next()

          override def hasNext: Boolean =
            iter1.hasNext && iter2.hasNext || inner.hasNext

          override def next(): (B,A) = {
            left = if (inner.hasNext) left else iter1.next()
            inner = if (inner.hasNext) inner else iter2.next().toIterator
            val right: B = inner.next()
            (right,left)
          }
        } // iterator
        converter
      }
    }
    if (!i1.iterator.hasNext) empty else convertable
  }

  def append[A](i1:Iterable[A], i2:Iterable[A]): Iterable[A] = {
    val convertable = new Iterable[A] {

      override def iterator: Iterator[A] = {

        val converter: AbstractIterator[A] = new AbstractIterator[A] {
          private val iter1 = i1.iterator
          private val iter2 = i2.iterator

          override def hasNext: Boolean = iter1.hasNext || iter2.hasNext
          override def next(): A =
            if (iter1.hasNext)
              iter1.next()
            else
              iter2.next()
        } // iterator
        converter
      }
    }
    if (!i1.iterator.hasNext && !i2.iterator.hasNext) empty[A] else convertable
  }

  implicit class POps[A](left:Iterable[A]) {

    def ##[B](right:Iterable[B]): Iterable[(A, B)] = cartesian(left, right)
    def &&[B](right:Iterable[B]): Iterable[(A, B)] = and(left, right)
    def !![B](right:Iterable[B]): Iterable[(A, B)] = or(left, right)

    def #&[B](right:Iterable[Iterable[B]]): Iterable[(A, B)] = pairLeft(left, right)

    def +++(right:Iterable[A]): Iterable[A] = append(left, right)

    def sum(a: A, b: A)(implicit num: Numeric[A]) : A = {
      import num._
      a + b
    }
    def subtract(a: A, b: A)(implicit num: Numeric[A]) : A = {
      import num._
      a - b
    }
    def multiply(a: A, b: A)(implicit num: Numeric[A]) : A = {
      import num._
      a * b
    }
    def divide(a: A, b: A)(implicit num: Fractional[A]) : A = {
      import num._
      a / b
    }

    def op(leftp:Iterable[A], rightp:Iterable[A], f:(A,A) => A): Iterable[A] = {
      // This is lazy, see library source code
      val tmp: Iterable[(A, A)] = leftp.zip(rightp)
      // Ensure it is lazy, use our map
      map( tmp)( (e:(A,A)) => f(e._1,e._2) )
    }

    def +(right:Iterable[A])(implicit num: Numeric[A]): Iterable[A] = op(left, right, sum)
    def -(right:Iterable[A])(implicit num: Numeric[A]): Iterable[A] = op(left, right, subtract)
    def *(right:Iterable[A])(implicit num: Numeric[A]): Iterable[A] = op(left, right, multiply)
    def /(right:Iterable[A])(implicit num: Fractional[A]): Iterable[A] = op(left, right, divide)

    def until[B](z:B)( f: (B,A) => (B,Boolean) ) : Iterable[A] = Ops.until(left, f, z)
  }

  implicit class POpsRight[A](left:Iterable[Iterable[A]]) {

    def &#[B](right: Iterable[B]): Iterable[(A, B)] = pairRight(left, right)
  }

}
