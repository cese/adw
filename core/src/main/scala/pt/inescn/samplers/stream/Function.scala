package pt.inescn.samplers.stream

object Function {

  /**
    * y = amplitude* sin(2*pi*freq + phase)
    *
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param t - current time
    * @return
    */
  def sin(frequency: Double, amplitude : Double, phase: Double)(t:Double): Double =
    amplitude * Math.sin((2 * Math.PI * frequency * t) + phase)

  def cos(frequency: Double, amplitude : Double, phase: Double)(t:Double): Double =
    amplitude * Math.cos((2 * Math.PI * frequency * t) + phase)

  /**
    * y(t) = m*t + c
    */
  def line(m: Double, c: Double = 0.0)(t:Double) : Double = m*t + c
  /**
    * y(t) = c*exp(n*t)
    */
  def exp(n: Double, c: Double = 1.0)(t:Double) : Double = c*Math.exp(n*t)
  /**
    * y(t) = c*power(base, n*t)
    */
  def power(base:Double, n: Double = 1.0, c: Double = 1.0)(t:Double) : Double = c*Math.pow(base, n*t)
  /**
    * y(t) = c*log_b(nt)
    */
  def log(b: Double, n: Double = 1.0, c: Double = 1.0)(t:Double) : Double = c*(Math.log(n*t)/Math.log(b))

  /**
    * Generates a chirp signal with a linear growth in frequency and/or amplitude. For variation, set
    * the values of `deltaAmp` and `deltaFreq` to value different than `0.0`. For a constant value,
    * use a value of `0.0`.
    *
    * @see https://en.wikipedia.org/wiki/Chirp
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param deltaAmp - ratio of increase in amplitude
    * @param deltaFreq - ratio of increase in frequency
    * @param t - current time
    * @return
    * @see [[expChirp]]
    */
  def linearChirp(frequency: Double, amplitude : Double, phase: Double, deltaAmp: Double, deltaFreq: Double)(t:Double): Double = {
    val currentAmplitude = (t * deltaAmp) + amplitude
    val currentFrequency = (t * deltaFreq) + frequency
    currentAmplitude * Math.sin((2 * Math.PI * currentFrequency * t) + phase)
  }

  /**
    * Generates a chirp signal with a linear growth in frequency and/or amplitude. For variation, set
    * the values of `deltaAmp` and `deltaFreq` to value different than `0.0`. For a constant value,
    * use a value of `0.0`.
    *
    * @see https://en.wikipedia.org/wiki/Chirp
    * @param frequency - frequency at which the wave oscillates
    * @param amplitude - maximum amplitude of the wave
    * @param phase - phase at which the wav starts. Note that to start later, provide a negative value (in radians)
    * @param deltaAmp - ratio of increase in amplitude
    * @param deltaFreq - ratio of increase in frequency
    * @param t - current time
    * @return
    * @see [[linearChirp]]
    */
  def expChirp(frequency: Double, amplitude : Double, phase: Double, deltaAmp: Double, deltaFreq: Double)(t:Double): Double = {
    val currentAmplitude = amplitude * Math.pow(deltaAmp, t)
    val currentFrequency = frequency * Math.pow(deltaFreq, t)
    currentAmplitude * Math.sin((2 * Math.PI * currentFrequency * t) + phase)
  }


}
