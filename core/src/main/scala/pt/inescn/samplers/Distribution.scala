/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.samplers

import pt.inescn.samplers.Base.{Infinite, Sampler, Unbounded}
import org.hipparchus.random.{JDKRandomGenerator, RandomDataGenerator, RandomGenerator}
import pt.inescn.utils.RandomCollection


/**
  * Created by hmf on 24-05-2017.
  */
object Distribution {

  /**
    * A hack to circumvent the Scala restriction that the overloaded constructors
    * must have as its first line a call to the (base) constructor.
    */
  def getSeed: Long = {
    val now = java.time.LocalDateTime.now
    val seed = now.toEpochSecond(java.time.ZoneOffset.UTC)
    seed
  }

  def getGenerator(seed: Long): RandomDataGenerator = RandomDataGenerator.of( new JDKRandomGenerator(seed) )
  def getGenerator: RandomDataGenerator = RandomDataGenerator.of( new JDKRandomGenerator(getSeed) )


  // Real Distributions

  /**
    * TODO: BUG - current returns a new/different value
    *
    * @param rnd - random generator (use constant seed for testing)
    * @tparam S - F-Bounded type
    */
  abstract class Real[S <: Real[S]](rnd : RandomGenerator)
    extends Unbounded[Double] { self : S =>

    val randomDataGenerator: RandomDataGenerator = RandomDataGenerator.of(rnd)

    def zero: Double = Double.NaN
    override def finished: Boolean = false
    override def next: S = this
  }

  case class Normal(mean: Double, sd: Double, rnd : RandomGenerator) extends Real[Normal](rnd) {
    override def reset: Normal = Normal(mean, sd, rnd)
    override def current: Double = randomDataGenerator.nextNormal(mean,sd)
  }

  object Normal {
    def apply(mean: Double, sd: Double): Normal = new Normal(mean, sd, getGenerator)
  }

  case class Uniform(lower: Double, upper: Double, rnd : RandomGenerator) extends Real[Uniform](rnd) {
    override def reset: Uniform = Uniform(lower, upper, rnd)
    override def current: Double = randomDataGenerator.nextUniform(lower, upper)
  }

  object Uniform {
    def apply(lower: Double, upper: Double): Uniform = new Uniform(lower, upper, getGenerator)
  }

  case class Gamma(shape: Double, scale: Double, rnd : RandomGenerator) extends Real[Gamma](rnd) {
    override def reset: Gamma = Gamma(shape, scale, rnd)
    override def current: Double = randomDataGenerator.nextGamma(shape, scale)
  }

  object Gamma {
    def apply(shape: Double, scale: Double): Gamma = new Gamma(shape, scale, getGenerator)
  }

  /**
    *
    * @param shape - also referred to as the standard deviation
    * @param scale - also referred to as the scale
    * @param rnd - random generator (use constant seed for testing)
    */
  case class LogNormal(shape: Double, scale: Double, rnd : RandomGenerator) extends Real[LogNormal](rnd) {
    override def reset: LogNormal = LogNormal(shape, scale, rnd)
    override def current: Double = randomDataGenerator.nextLogNormal(shape, scale)
  }

  object LogNormal {
    def apply(shape: Double, scale: Double): LogNormal = new LogNormal(shape, scale,getGenerator)
  }

  case class Exponential(mean: Double, rnd : RandomGenerator) extends Real[Exponential](rnd) {
    override def reset: Exponential = Exponential(mean, rnd)
    override def current: Double = randomDataGenerator.nextExponential(mean)
  }

  object Exponential {
    def apply(mean: Double): Exponential = new Exponential(mean,getGenerator)
  }

  case class Beta(alpha: Double, beta: Double, rnd : RandomGenerator) extends Real[Beta](rnd) {
    override def reset: Beta = Beta(alpha, beta, rnd)
    override def current: Double = randomDataGenerator.nextBeta(alpha, beta)
  }

  object Beta {
    def apply(alpha: Double, beta: Double): Beta = new Beta(alpha,beta,getGenerator)
  }

  // Bounded

  abstract class Discrete[S <: Discrete[S,T], T]
    extends Unbounded[T] { self : S =>
  }

  // Integer Distributions

  abstract class Integral[S <: Integral[S]](val rnd : RandomGenerator)
    extends Unbounded[Int] { self : S =>

    val randomDataGenerator: RandomDataGenerator = RandomDataGenerator.of(rnd)

    def zero: Int = Int.MinValue
    override def finished: Boolean = false
    override def next: S = this
  }

  case class Zipf(numberOfElements: Int, exponent: Double, override val rnd : RandomGenerator) extends Integral[Zipf](rnd) {
    override def reset: Zipf = Zipf(numberOfElements, exponent, rnd)
    override def current: Int = randomDataGenerator.nextZipf(numberOfElements, exponent)
  }

  object Zipf {
    def apply(numberOfElements: Int, exponent: Double): Zipf = new Zipf(numberOfElements, exponent,getGenerator)
  }

  case class Poisson(mean: Double, override val rnd : RandomGenerator) extends Integral[Poisson](rnd) {
    override def reset: Poisson = Poisson(mean, rnd)
    override def current: Int = randomDataGenerator.nextPoisson(mean)
  }

  object Poisson {
    def apply(mean: Double): Poisson = new Poisson(mean,getGenerator)
  }

  case class IntUniform(start: Int, end: Int, override val rnd : RandomGenerator) extends Integral[IntUniform](rnd) {
    override def reset: IntUniform = IntUniform(start, end, rnd)
    override def current: Int = randomDataGenerator.nextInt(start,end)
  }

  object IntUniform {
    def apply(start: Int, end: Int): IntUniform = new IntUniform(start,end,getGenerator)
  }

  // Long distributions

  abstract class LongInt[S <: LongInt[S]](rnd : RandomGenerator)
    extends Unbounded[Long] { self : S =>

    val randomDataGenerator: RandomDataGenerator = RandomDataGenerator.of(rnd)

    def zero: Long = Long.MinValue
    override def finished: Boolean = false
    override def next: S = this
  }

  case class LongUniform(start: Long, end: Long, rnd : RandomGenerator) extends LongInt[LongUniform](rnd) {
    override def reset: LongUniform = LongUniform(start, end, rnd)
    override def current: Long = randomDataGenerator.nextLong(start,end)
  }

  object LongUniform {
    def apply(start: Long, end: Long): LongUniform = new LongUniform(start,end,getGenerator)
  }

  // Bounded distributions

  case class DiscreteUniform[T](s: Seq[T], len: Int, rnd : RandomGenerator)
    extends Unbounded[T] {

    val randomDataGenerator: RandomDataGenerator = RandomDataGenerator.of(rnd)

    def zero: T = s.head
    override def finished: Boolean = false
    override def next: DiscreteUniform[T] = this
    override def reset: DiscreteUniform[T] = DiscreteUniform[T](s, s.length, rnd)
    override def current: T = {
      val idx = randomDataGenerator.nextInt(0,len-1)
      s(idx)
    }
  }

  object DiscreteUniform {
    def apply[T](s: Seq[T]): DiscreteUniform[T] = DiscreteUniform(s,s.length,getGenerator)
    def apply[T](s: Seq[T], rnd:RandomGenerator): DiscreteUniform[T] = DiscreteUniform(s,s.length,RandomDataGenerator.of(rnd))
  }

  /**
    * This class allows us to generate an infinite sequence of discrete elements. We can assign weights
    * to each discrete element so that a specific distribution of elements are generated. We can set the
    * random generator used to generate the random events. The actual distribution is generated by the
    * class [[pt.inescn.utils.RandomCollection]]
    *
    * @param rndTmp - any random generator of the type [[RandomGenerator]]
    * @param rnd - the class that actually generates the appropriate distribution
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    * @see [[pt.inescn.utils.RandomCollection]]
    */
  case class Proportional[A] private(rndTmp : RandomGenerator, rnd : RandomCollection[A])
      extends Unbounded[A] {

    def add(weight: Double, cl: A): Proportional[A] = Proportional(rndTmp, rnd.add(weight, cl) )

    override def finished: Boolean = false
    override def reset: Proportional[A] = this // Proportional[A](rndTmp, mp)
    override def next: Proportional[A] = this
    override def current: A = rnd.next
  }

  object Proportional {

    def apply[A](rndTmp : RandomGenerator, mp : Map[A,Double] = Map[A,Double]()): Proportional[A] = {
      val rndColl = RandomCollection[A](mp, rndTmp)
      Proportional[A](rndTmp, rndColl)
    }
  }

  /**
    * Represents a sampling of a single lem,ent ort value.
    *
    * @param original the constant value
    * @tparam A - elements that will be sampled. May be numeric, nominal or ordinal.
    */
  case class Const[A] private (original: A) extends Unbounded[A] {

    def zero: A = original

    override def reset = Const(original)

    override def finished: Boolean = false

    override def current: A = original

    override def next: Const[A] = this
  }

  case class ApplyFunc[A,B,S<:Sampler[Infinite, A]] private (sampler: S, func: A => B) extends Unbounded[B] {

    override def reset = ApplyFunc(sampler.reset, func)

    override def finished: Boolean = false

    override def current: B = func(sampler.current)

    override def next: ApplyFunc[A, B, S] = this
  }

}
