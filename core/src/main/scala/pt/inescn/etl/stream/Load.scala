package pt.inescn.etl.stream


import java.time.{Duration, LocalDateTime, Period}

import scala.collection.{AbstractIterator, mutable}
import better.files.File
import com.github.tototoshi.csv.{CSVFormat, CSVWriter}
import pt.inescn.utils.ADWError

import scala.collection.mutable.ArraySeq
import scala.language.existentials
import scala.language.implicitConversions
import scala.reflect.runtime.universe._
import scala.reflect.ClassTag


/**
  * runMain pt.inescn.etl.stream.LoadX
  */
object Load {

  def paramInfo[T: TypeTag](x: T): Unit = {
    val targs = typeOf[T] match { case TypeRef(_, _, args) => args }
    println(s"type of $x (${x.getClass}) has type arguments $targs")
  }

  implicit def paramInfoX[A](x:A)(implicit tag: TypeTag[A]): Unit = {
    val targs = tag.tpe match { case TypeRef(utype, usymbol, args) => args }
    println(s"type of $x (${x.getClass}) has type arguments $targs")
  }

  // https://www.cakesolutions.net/teamblogs/ways-to-pattern-match-generic-types-in-scala
  // https://stackoverflow.com/questions/15095727/in-scala-2-10-how-do-you-create-a-classtag-given-a-typetag
  // https://stackoverflow.com/questions/18729321/how-to-get-classtag-form-typetag-or-both-at-same-time
  // https://stackoverflow.com/questions/11494788/how-to-create-a-typetag-manually/11495793#11495793
  // https://stackoverflow.com/questions/11494788/how-to-create-a-typetag-manually
  sealed trait Val[V] { self =>
    val v: V

    override def toString: String = s"Val($v)"

    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case other: Val[V] =>
          v.equals(other.v)
        case _ =>
          false
      }
    }

    override def hashCode(): Int = v.hashCode()

    def arr(n: Int)(implicit tag1: ClassTag[V]): Array[V] = Array.fill[V](n)(v)
    //def arr0(n: Int): Array[Any] = Array.fill[Any](n)(v)
    def arr0(n: Int): mutable.ArraySeq[V] = ArraySeq.fill[V](n)(v)
    def vec(n: Int): Vector[V] = Vector.fill[V](n)(v)

    def cast[T]:T = v.asInstanceOf[T]

    // TODO: for easy use in for comprehensions
    // https://github.com/scala/scala/blob/v2.12.6/src/library/scala/Option.scala#L1
    // https://www.scala-lang.org/api/2.12.6/scala/collection/generic/FilterMonadic.html
    // https://stackoverflow.com/questions/35761043/how-to-make-your-own-for-comprehension-compliant-scala-monad
    def map[R](f: V => R): Val[R] = f(v)
    def flatMap[R](f: V => Val[R]): Val[R] = f(v)
    def filter(p: V => Boolean): Val[V] = if (p(v)) this else Empty[V](v)
    def withFilter(p: V => Boolean): WithFilter = new WithFilter(p)

    /** We need a whole WithFilter class to honor the "doesn't create a new
      *  collection" contract.
      */
    class WithFilter(p: V => Boolean) {
      def map[B](f: V => B): Val[B] = self filter p map f
      def flatMap[B](f: V => Val[B]): Val[B] = self filter p flatMap f
      def foreach[U](f: V => U): Unit = self filter p foreach f
      def withFilter(q: V => Boolean): WithFilter = new WithFilter(x => p(x) && q(x))
    }
  }


  case class Empty[A](override val v:A) extends Val[A]
  case class ErrVal(override val v:String) extends Val[String]


  object Val {
    def apply[T](e:T): Val[T] = new Val[T] {
      override val v:T = e
    }
  }

  implicit def pack[A](s:A) : Val[A] = new Val[A] {
    override val v: A = s
  }

  // No class tag available
  implicit def unpack[T](t:Val[_]): Either[String,T] = {
    try {
      val v = t.v.asInstanceOf[T]
      Right(v)
    } catch {
      // When used via implicits should never reach this
      case e: Exception => Left(e.getMessage)
    }
  }

  def unpackErr[T](t:Val[_]): Either[ADWError, T] = {
    try {
      val v = t.v.asInstanceOf[T]
      Right(v)
    } catch {
      // When used via implicits should never reach this
      case e: Exception => Left(ADWError(e.getMessage))
    }
  }

  implicit def test[A,B,X](av: Val[X], f: A => B)(implicit a: Val[X] => Either[String,A]): Either[String,B] = {
    println(s"TEST $av")
    a(av).flatMap( e => Right(f(e)) )
  }


  def applyArg[A,B](f:A => B, v: Val[_]): Val[_] = {
    val va = unpack[A](v)
    va match {
      case Left(m1) =>
        ErrVal("applyArg[A,B]: arg1" + m1)
      case Right(arg1) =>
        val vb: B = f(arg1)
        val avb = pack[B](vb)
        avb
    }
  }

  def applyArgs[A1,A2,B](f:(A1,A2) => B, v1: Val[_], v2: Val[_]): Val[_] = {
    val va1 = unpack[A1](v1)
    val va2 = unpack[A2](v2)
    (va1, va2) match {
      case (Left(m1), Left(m2)) =>
        ErrVal("applyArgs[A1,A2,B]: arg1" + m1 + "arg2: " + m2)
      case (Left(m1), _) =>
        ErrVal("applyArgs[A1,A2,B]: arg1" + m1)
      case (_, Left(m2)) =>
        ErrVal("applyArgs[A1,A2,B]: arg2: " + m2)
      case (Right(arg1), Right(arg2)) =>
        val vb = f(arg1, arg2)
        val avb = pack[B](vb)
        avb
    }
  }

  case class Row(r:Map[String,Val[_]]) {
    /**
      * Number of fields (columns) ion the stream row
      * @return size of row
      */
    val size: Int = r.size

    /**
      * Get the field (column) names
      * @return list of column names
      */
    val colNames: List[String] = r.keys.toList
    val colNamesSet: Set[String] = r.keys.toSet

    /**
      * Get the field (column) types
      * @return
      */
    val colTypes: Map[String, Class[T] forSome {type T}] = r.map{ case (k,e) => (k, e.v.getClass) }.toMap


    /**
      * Get a value with a given name
      * @param k name of a stream field
      * @return stream field value
      */
    def apply(k:String): Val[_] = r(k)
    def get(k:String): Option[Val[_]] = r.get(k)

    /**
      * Add a new field to the row. If the field exists, it is replaced.
      * @param e pair field name and field value to add
      * @return new stream row with the new column and its value
      */
    def +(e: (String,Val[_])) = Row(r + (e._1 -> e._2))
    /**
      * Remove a new field to the row
      * @param k name of a stream field
      * @return new stream row without the column and its value
      */
    def -(k: String) = Row(r - k)

    /**
      * Combine two rows into a single one
      * @param e row to combine with
      * @return single stream row all the names and values
      */
    def ++(e: Row): Row = {
      val tmp = e.r.keys.foldLeft(r){ case (acc,k) => acc - k }
      Row(tmp ++ e.r)
      // Row(r ++ e.r)
    }

    /**
      * Change a give stream field value into another value
      * @param k field (column) name to change
      * @param v value to assign to the field (column)
      * @return new updated stream row
      */
    def updated(k:String,v:Val[_]) = Row(r.updated(k,v))

    /**
      * Creates a row with consisting of a subset of columns.
      * If a column is missing an exception will occur.
      *
      * @param ks columns to select and place in new Row
      * @return
      */
    def project(ks: String*): Row = {
      val fields: Map[String, Val[_]] = ks.map{ k => k -> r(k) }.toMap
      Row(fields)
    }

    def ignore(ks: String*): Row = {
      val kset = ks.toSet
      ignoreSet(kset)
    }

    def filter(ks: String*): Row = project(ks:_*)

    def ignoreSet(kset: Set[String]): Row = {
      val fields: Map[String, Val[_]] = r.filter{ case (k,_) => ! kset.contains(k) }
      Row(fields)
    }

    /**
      * Converts all field and value pairs applying a conversion function
      * @param f the conversion function
      * @return a stream row that was transformed by the conversion function
      */
    def map(f:((String,Val[_])) => (String,Val[_])): Row = Row( r.map(f) )

    /**
      * If no column names are given, use all of them.
      *
      * @param cols column names to use
      * @return set of all column names if no columns given, otherwise use
      *         the original list
      */
    private def checkColNames(cols:Seq[String]): Set[String] = if (cols.isEmpty) r.keys.toSet else cols.toSet

    /**
      * Convert the columns into a vector of the same type. If the type conversion fails
      * or the source type is incompatible, then the field is ignored.
      *
      * @param f conversion function that establishes type conversion
      * @param cols columns were to apply the function `f`.
      *             If empty use all columns.
      * @tparam A type of value to unpack
      * @tparam B type of value to pack (wrap)
      * @return new Row with the `cols` replaced with he new values
      */
    def vec[A,B](f: A => B, cols:String*): Vector[B] = {
      val acc = List[B]()
      val cls = checkColNames(cols)
      val t = r.filterKeys( cls.contains ).foldLeft(acc) { case (acum,(k,v)) =>
        // Get the row element
        val va: Either[String, A] = unpack(v)
        va.fold( { msg =>
          // If it is not the correct type, ignore
          acum
        }, { oa: A =>
          // If it is the correct type, use it
          val noa: B = f(oa)
          noa:: acum
        })
      }
      t.toVector
    }

    /**
      * Convert the columns into an array of the same type. If the type conversion fails
      * or the source type is incompatible, then the field is ignored.
      *
      * @param f conversion function that establishes type conversion
      * @param cols columns were to apply the function `f`.
      *             If empty use all columns.
      * @tparam A type of value to unpack
      * @tparam B type of value to pack (wrap)
      * @return new Row with the `cols` replaced with he new values
      */
    def arr[A,B:ClassTag](f: A => B, cols:String*): Array[B] = {
      vec(f,cols:_*).toArray[B]
    }

    /**
      * Apply a single parameter function to one or more fields. Replace
      * those fields' old values with the new values.
      *
      * Note: last dummy implicit used to avoid duplicate method error.
      * @see // https://stackoverflow.com/questions/3307427/scala-double-definition-2-methods-have-the-same-type-erasure
      *
      * @param f conversion function that establishes type conversion
      * @param cols columns were to apply the function `f`.
      *             If empty use all columns.
      * @tparam A type of value to unpack
      * @tparam B type of value to pack (wrap)
      * @return new Row with the `cols` replaced with the new values
      */
    def apply[A,B](f: A => B, cols:String*)(implicit d: DummyImplicit): Row = {
      val cls = checkColNames(cols)
      val clsPairs: List[(String, String)] = cls.zip(cls).toList
      apply(f,clsPairs:_*)
    }

    /**
      * Apply a single parameter function to one or more fields. Place
      * those fields' new values in a new field (column).
      *
      * @param f conversion function that establishes type conversion
      * @param cols columns were to apply the function `f`.
      *             If empty use all columns. The first element is
      *             the column to apply `f`, the second is the new column's
      *             name were the result is placed.
      * @tparam A type of value to unpack
      * @tparam B type of value to pack (wrap)
      * @return new Row with the `cols` added with the new values
      */
    def apply[A,B](f: A => B, cols:(String,String)*): Row = {
      // Get the argument (v1) and the destination (v2)
      val colArgs = cols.map { case (v1,v2) =>
        val t1 = r.get(v1).toRight(v1)
        val t = t1 match {
          case Left(e1) => Left((v2,s"Columns not found: ($e1)"))
          case Right(e1) => Right((v2,e1))
        }
        t
      }
      // If the argument is valid, apply the function
      val t: Seq[(String, Val[_])] = colArgs.map { args =>
        val out = args match {
            // place the result in the destination (v2)
            case Left((o,msg)) =>
              // parameters not found
              o -> ErrVal(msg)
            case Right((o,p1)) =>
              val r = applyArg(f,p1)
              o -> r
          }
        out
      }
      // Add new output
      Row(r ++ t.toMap)
    }

    /**
      * Apply a single parameter function to one or more fields. Place
      * those fields' new values in a new field (column).
      *
      * @param f conversion function that establishes type conversion
      * @param cols columns were to apply the function `f`.
      *             If empty use all columns. The first and second elements
      *             are the columns to apply to `f`, the third is the new
      *             column's name were the result is placed.
      * @tparam A1 type of value of first parameter to unpack
      * @tparam A2 type of value of second parameter to unpack
      * @tparam B type of value to pack (wrap)
      * @return new Row with the `cols` added with the new values
      */
    def apply[A1,A2,B](f: (A1,A2) => B, cols:(String,String,String)*): Row = {

      // Get the arguments (v1, v2) and the destination (v3)
      val colArgs = cols.map { case (v1,v2,v3) =>
        val t1 = r.get(v1).toRight(v1)
        val t2 = r.get(v2).toRight(v2)
        val t = (t1, t2) match {
          case (Left(e1), Left(e2)) => Left((v3,s"Columns not found: ($e1, $e2)"))
          case (Left(e1), _) => Left((v3,s"Column not found: ($e1, _)"))
          case (_, Left(e2)) => Left((v3,s"Columns not found: (_, $e2)"))
          case (Right(e1), Right(e2)) => Right((v3,e1,e2))
        }
        t
      }
      // If the arguments are valid, apply the function
      val t: Seq[(String, Val[_])] = colArgs.map { args =>
        val out = args match {
          // place the result in the destination (v3)
          case Left((o,msg)) =>
            // parameters not found
            o -> ErrVal(msg)
          case Right((o,p1,p2)) =>
            val r = applyArgs(f,p1,p2)
            o -> r
        }
        out
      }
      // Add new output
      Row(r ++ t.toMap)
    } // apply

    /**
      * Folds over all fields in the stream row. The accumulator is a also
      * a stream row. Allows one to filter and construct a new row.
      *
      * @param acc stream row used as accumulator
      * @param f selection and transformation function
      * @return a new stream row
      */
    def foldLeft[T](acc:Row)(f:(Row,(String,Val[_])) => Row): Row = {
      def ff(acc: Map[String, Val[_]], e: (String, Val[_])): Map[String, Val[_]] = {
        val tmp = f(Row(acc),e)
        tmp.r
      }
      Row(r.foldLeft(acc.r)(ff))
    }

  }

  /**
    * Smart constructors for [[Row]].
    */
  object Row {
    def apply(): Row = new Row(Map[String,Val[_]]())

    def apply(r: (String, Val[_])*): Row = new Row(r.toMap)

    // https://stackoverflow.com/questions/3307427/scala-double-definition-2-methods-have-the-same-type-erasure
    def apply[T](r: (String, T)*)(implicit d: DummyImplicit): Row = {
      val tmp = r.map{ case (k,v) =>
        val t:Val[T] = v
        (k,t)
      }.toMap
      new Row(tmp)
    }
  }

  case class Mapper(op: Row => Row) {

    def apply(r:Row): Row = op(r)

    def ++(other:Mapper): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous: Row = op(r)
        val previousOther: Row = other.op(previous)
        previous ++ previousOther
      }
      Mapper(tmp)
    }

    def updated(k:String,e:Val[_]): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous: Row = op(r)
        previous.updated(k,e)
      }
      Mapper(tmp)
    }

    def project(ks: String*): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous: Row = op(r)
        previous.project(ks: _*)
      }
      Mapper(tmp)
    }

    def ignore(ks: String*): Mapper = {
      val kset = ks.toSet
      ignoreSet(kset)
    }

    def ignoreSet(kset: Set[String]): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous: Row = op(r)
        previous.ignoreSet(kset)
      }
      Mapper(tmp)
    }

    def vec[A,B](col:String, f: A => B, cols:String*): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous: Row = op(r)
        val tmp: Val[Vector[B]] = previous.vec(f, cols: _*)
        previous.v ++ Row(col -> tmp)
      }
      Mapper(tmp)
    }

    def arr[A,B:ClassTag](col:String, f: A => B, cols:String*): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous: Row = op(r)
        val tmp: Val[_] = previous.arr[A,B](f, cols: _*)
        previous.v ++ Row(col -> tmp)
      }
      Mapper(tmp)
    }

    def apply(f: Row ⇒ Row) : Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous = op(r)
        f(previous)
      }
      Mapper(tmp)
    }

    def apply[A,B](f: A => B, cols: String*)(implicit d: DummyImplicit): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous = op(r)
        previous(f, cols: _*)
      }
      Mapper(tmp)
    }

    def apply[A,B](f: A => B, cols: (String, String)*): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous = op(r)
        previous(f, cols: _*)
      }
      Mapper(tmp)
    }

    def apply[A1,A2,B](f: (A1,A2) => B, cols:(String,String,String)*): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous = op(r)
        previous(f, cols: _*)
      }
      Mapper(tmp)
    }

    def foldLeft(acc:Row)(f:(Row,(String,Val[_])) => Row): Mapper = {
      val tmp: Row => Row = (r: Row) => {
        val previous = op(r)
        previous.foldLeft(acc)(f)
      }
      Mapper(tmp)
    }

  }

  object Mapper {

    val idMap: Mapper = {
      val id: Row => Row = (r: Row) => r
      new Mapper(id)
    }

    def apply(): Mapper = idMap
  }


  type OldName = String
  type NewName = String

  // TODO: add fold operation (foldDown, like map)
  // TODO: catch and process row indexing errors
  class Frame(override val v: Row,
              val ops: Mapper,
              val src: Iterable[Row] ) extends Val[Row] {

    /*
     * BUG: due to the chance that a the Scala's map is not fully lazy, we
     * have opted to use our own Iterable (first reference). However there
     * seems to be a BUG that cannot be explained (see second reference).
     * And we are forced to use the code below to ensure correct functioning.
     * We kee the commented code for future reference.
     *
     * @see https://users.scala-lang.org/t/why-is-my-implementation-of-iterable-not-lazy/2744
     * @see https://users.scala-lang.org/t/wierd-beghaviour-with-iterators-iteration-stalls-and-restarts/3377
     */

    private def lazyMap[A,B](i: Iterator[A])(f: A => B): Iterable[B] = {
      val convertable = new Iterable[B] {
        override def iterator: Iterator[B] = {
          val converter: AbstractIterator[B] = new AbstractIterator[B] {
            private val lazyIter = i
            override def hasNext: Boolean = {
              lazyIter.hasNext
            }
            override def next(): B = {
              f(lazyIter.next())
            }
          } // iterator
          converter
        }
      }
      convertable
    }


    private def lazyMap[A,B](i: Iterable[A])(f: A => B): Iterable[B] = lazyMap(i.toIterator)(f)

    /*
    // This version works.
    private def lazyMap[A,B](i: Iterable[A])(f: A => B): Iterable[B] = {
      val convertable = new Iterable[B] {
        private val iter = i.toIterator
        override def iterator: Iterator[B] = {
          val converter: AbstractIterator[B] = new AbstractIterator[B] {
            override def hasNext: Boolean = iter.hasNext
            override def next(): B = {
              f(iter.next())
            }
          } // iterator
          converter
        }
      }
      convertable
    }

    // This fails, only one record is extracted
    private def lazyMap[A,B](i: Iterable[A])(f: A => B): Iterable[B] = {
      val convertable = new Iterable[B] {
        override def iterator: Iterator[B] = {
          val iter = i.toIterator
          val converter: AbstractIterator[B] = new AbstractIterator[B] {
            override def hasNext: Boolean = iter.hasNext
            override def next(): B = {
              f(iter.next())
            }
          } // iterator
          converter
        }
      }
      convertable
    }

    // This fails, only one record is extracted
    private def lazyMap[A,B](i: Iterable[A])(f: A => B): Iterable[B] = {
      val convertable = new Iterable[B] {
        override def iterator: Iterator[B] = {
          val converter: AbstractIterator[B] = new AbstractIterator[B] {
            private val iter = i.toIterator
            override def hasNext: Boolean = iter.hasNext
            override def next(): B = {
              f(iter.next())
            }
          } // iterator
          converter
        }
      }
      convertable
    }
    */

    /**
      * Funcion to lazily filter values from a frame
      * Verification performed in the hasNext() routine - rather than only checking if there
      * is an element next, one needs to ensure that there exists an element next, such that it
      * satisfies the predicate f.
      * @param i frame
      * @param f predicate function
      * @tparam A
      * @return filtered frame
      */

    private def lazyFilter[A](i: Iterator[A])(f: A => Boolean): Iterable[A] = {
      val convertable = new Iterable[A] {
        override def iterator: Iterator[A] = {
          val converter: AbstractIterator[A] = new AbstractIterator[A] {
            private val lazyIter = i
            private var tmp: Option[A] = None

            override def hasNext: Boolean = {
              var useIt = false
              var hasNextTest = true
              while (!useIt && hasNextTest) {
                hasNextTest = lazyIter.hasNext
                if (hasNextTest){
                  val t = lazyIter.next()
                  useIt = f(t)
                  tmp = if (useIt) Some(t) else None
                }
              }
              hasNextTest && useIt
            }

            override def next(): A = {
              tmp.fold(throw new RuntimeException("No next element satisfies filter predicate"))(e => e)
            }
          } // iterator
          converter
        }
      }
      convertable
    }

    private def lazyFilter[A](i: Iterable[A])(f: A => Boolean): Iterable[A] = lazyFilter(i.toIterator)(f)



    /**
      * Utility function to create an empty iterator.
      * @return
      */
    private def emptyIterable: Iterable[Row] = {
      val src = new Iterable[Row] {
        override def iterator: Iterator[Row] = new AbstractIterator[Row] {
          override def hasNext: Boolean = false

          override def next(): Row = Row()
        }
      }
      src
    }

    /**
      * Converts each row to a string. Stream may potentially
      * be infinite so we also return an Iterable. We can
      * limit the number of elements we extract and convert.
      *
      * @param take number of elements to extract and convert
      * @return stream of strings
      */
    def mkStrings(take:Int=0): Iterable[String] = {
      val tmp = lazyMap(iterable){ i:Row => i.toString }
      if (take > 0) tmp.take(take) else tmp
    }

    /**
      * Converts each row to a string. Stream may potentially
      * be infinite so we also return an Iterable. We can
      * limit the number of elements we extract and convert.
      *
      * @param take number of elements to extract and convert
      * @return string representation of rows
      */
    def toString(take:Int): String = {
      val ellipse = s"... took $take elements. May exist more ..."
      s"${this.getClass.getName}(\n" + mkStrings(100).mkString(",\n") + s"\n$ellipse)"
    }

    override def toString: String = toString(100)

    /**
      * @return number of fields in the stream frame
      */
    def colSize: Int = v.size

    /**
      * Currently active names
      *
      * @return the fields' names
      */
    def colIDs: Seq[String] = v.r.keys.toSeq

    /**
      * The keys are the original column names.
      * @return the map from the field name to the expect type
      */
    lazy val colTypes: Map[String, Class[_]] = v.colTypes.map( e => e._1 -> e._2).toMap

    /**
      * Give the client access to the stream data. It first applies all of
      * the pending conversions to the fields.
      *
      * NOTE: The iterator's map operation is **not** lazy. To get around
      * this, we first access the iterator and use that for mapping. However
      * now we are forced to return an Iterable, so an additional call is
      * required.
      *
      * NOTE: we have performance issues. The "toIterable" consumes
      * much time (from 1 sec to several hundreds). Trying to debug
      * the code does not help because I cannot step into the underlying
      * implementation (even when I force step-into). This function
      * is called in several places: drop, filter, foldMap, window,
      * extend. All these functions are at risk.
      *
      * NOTE: to avoid on-lazy mapping we have substituted the library's
      * map with out own map implementation.
      *
      * @see https://users.scala-lang.org/t/why-is-my-implementation-of-iterable-no-lazy/2744
      * @return iterator access to the frame's fields
      */
    def iterable: Iterable[Row] = {
      /*
      // One way to circumvent issue with Scala's laziness
      src.toIterator.map { row =>
        // Apply the transforms
        val tmp = ops(row)
        println(tmp)
        tmp
      }.toIterable
      */
      lazyMap(src){ ops(_) }
    }

    /**
      * Creates the operation that renames a single field or column
      * of a [[Row]]
      *
      * @param fromTo name change pair (old to new)
      * @return partial function that changes Row's column name
      */
    private def renameColumn(fromTo: (OldName, NewName)): Row => Row = (row:Row) => {
      val (from, to) = fromTo
      val tmp = row(from)
      val r0 = row - from
      r0.updated(to, tmp)
    }

    /**
      * Creates the operation that renames a several fields or columns
      * of a [[Row]]
      *
      * @param fromTo name change pairs (old to new)
      * @return partial function that changes Row's column name
      */
    private def renameColumns(fromTo: Seq[(OldName, NewName)]): Row => Row = (row:Row) => {
      fromTo.foldLeft(row) { case (r,(from, to)) =>
        val tmp = r(from)
        val r0 = r - from
        r0.updated(to, tmp)
      }
    }

    /**
      * Rename the frame's field.
      *
      * @see [[renameCols]]
      * @param fromTo tuple whose first name is the field to rename
      *               and the second is new field's name
      * @return returns a stream frame with the the renamed field
      */
    def renameCol(fromTo: (OldName, NewName)): Frame = {
      val rename = renameColumn(fromTo)
      val nv = rename(v)
      val newOps = ops( rename )
      new Frame(nv, newOps, src)
    }

    /**
      * Renames a set of fields.
      *
      * @see [[renameCol]]
      * @param fromTo sequence of tuple whose first name is the field to
      *               rename and the second is new field's name
      * @return returns a stream frame with the the renamed fields
      */
    def renameCols(fromTo: (OldName, NewName)*): Frame = {
      val rename = renameColumns(fromTo)
      val nv = rename(v)
      val newOps = ops( rename )
      new Frame(nv, newOps, src)
    }


    /**
      * Change the value of a column (field).
      * @param k column name
      * @param e new value
      * @return row with old value overwritten
      */
    def updated(k: String, e: Val[_]): Frame = {
      val newOps = ops.updated(k, e)
      val nv = v.updated(k,e)
      new Frame(nv, newOps, src)
    }

    /**
      * Returns a new data consisting of only the selected
      * fields of the original data frame.
      *
      * @param colNamesU selected columns
      * @return column data frame with selected rows only
      */
    def project(colNamesU: String*): Frame = {
      val newOps = ops.project(colNamesU: _*)
      val nv = Row( colNamesU.map(k => k -> v.r( k ) ):_* )
      new Frame(nv, newOps, src)
    }

    /**
      * Create a new stream that ignores (removes) the listed columns.
      *
      * @param colNamesU names of columns to remove
      * @return
      */
    def ignore(colNamesU: String*): Frame = {
      val kset = colNamesU.toSet

      val newOps = ops.ignoreSet(kset)

      val fields = v.r.filter{ case (k,_) => ! kset.contains(k) }

      val nv = Row( fields )
      new Frame(nv, newOps, src)
    }

    /**
      * Converts all elements of the selected columns into a vector.
      * Each vector row has all its element of the same type. The
      * result is placed into a new column.
      *
      * @param col new column to add
      * @param f conversion for all elements of vector row
      * @param cols columns to add as row in vector
      * @tparam A convert from this type
      * @tparam B convert to this type
      * @return vector with each row consisting of selected row columns
      */
    def vec[A,B](col: String, f: A => B, cols: String*)(implicit defB:B): Frame = {
      val newOps = ops.vec(col, f, cols: _*)
      val typeVec: Val[Vector[B]] = Vector.fill[B](cols.length)(defB)
      val ncol = v.r + ( col -> typeVec)
      val nv = Row(  ncol )
      new Frame(nv, newOps, src)
    }

    /**
      * Converts all elements of the selected columns into a array.
      * Each array row has all its element of the same type. The
      * result is placed into a new column.
      *
      * @param col new column to add
      * @param f conversion for all elements of array row
      * @param cols columns to add as row in array
      * @tparam A convert from this type
      * @tparam B convert to this type
      * @return array with each row consisting of selected row columns
      */
    def arr[A,B:ClassTag](col: String, f: A => B, cols: String*)(implicit defB:B): Frame = {
      val newOps = ops.arr[A,B](col, f, cols: _*)
      val typeVec: Val[Array[B]] = Array.fill[B](cols.length)(defB)
      val ncol = v.r + ( col -> typeVec)
      val nv = Row(  ncol )
      new Frame(nv, newOps, src)
    }

    /**
      * Returns a new data consisting of only the selected
      * fields of the original data frame.
      *
      * @param cols selected columns
      * @return column data frame with selected rows only
      */
    def apply[A,B](cols: String*): Frame = project(cols:_*)

    private def toDefault[B](pairs: Seq[String], default:B): Seq[(String, B)] = {
      pairs.map( e => (e, default))
    }


    /**
      * This is a general function that generates a new [[Frame]]'s `v` member
      * that stores the column names and column types. It accepts a `A` type
      * that is a single element or some tuple of any arity. The function
      * `getOutArg` is then used to select the output argument (column name).
      * It then checks if this output column is a new column or an existing
      * column. Irrespective of whether or not it is an old or new column,
      * the type is changed to be a `B`.
      * We then add the new column ond replace the old column (with the new
      * type) in the frame's `v`
      *
      * @param getOutArg selects the output argument of the columns used as
      *                  parameters (usually the last one)
      * @param cols columns used as parameters to one of the apply functions
      * @param defB value of the apply's function output
      * @tparam A tuple of the input
      * @tparam B type of the apply's function output
      * @return the new `v` column type information
      */
    private def updateColInfo[A,B](getOutArg: A => NewName)(cols: Seq[A],defB:B): Row
    = {
      val defaultValB = pack[B](defB)

      // We may either create a new output variable or overwrite it
      val (outOldColsTmp, outNewColsTmp) = cols.partition( e => v.r.contains(getOutArg(e)) )
      val outNewCols = outNewColsTmp.map(k => getOutArg(k))
      val outOldCols = outOldColsTmp.map(k => getOutArg(k))

      // Overwrite type of new output column
      val newTypes = toDefault(outNewCols,defaultValB)

      // Overwrite type of old output column
      val oldTypes = toDefault(outOldCols,defaultValB)

      // Add new column type or overwrite existing types of arguments
      val argTypes = oldTypes ++ newTypes
      val allArgTypes = v.r ++ argTypes.toMap
      val nv = Row(allArgTypes)
      nv
    }

    /**
      * Extracts the first output parameter from the one parameter
      * used to indicate both the input columns and the output column
      * of the [[Frame]]. It then constructs a frame's type [[Row]]
      * with the possibly new type of the output. Here the output column
      * is always replaced.
      *
      * @param cols column names of the function's parameters
      * @param defB default value used to set the typ+e of tyhe output value
      * @tparam B type of output column
      * @return type [[Row]] with new or replaced columns types
      */
    private def updateColInfo1[B](cols: Seq[OldName],defB:B): Row
    = updateColInfo[OldName,B](i => i)(cols, defB)


    /**
      * Applies a function to one or more columns. Applying the function
      * may or may not change the column's type. Here we only allow for
      * one parameter (column name), so the output value will always
      * overwrite the column's value.
      *
      * @param f function to apply to the columns
      * @param cols column names on which to apply the function
      * @param d used to avoid multiple definitions compilation errors
      * @param defB default value to set the columns underlying type
      * @tparam A type of column before change
      * @tparam B type of column after change
      * @return frame with the columns changes according to the conversion
      *         function
      */
    def apply[A,B](f: A => B, cols: String*)(implicit d: DummyImplicit, defB:B): Frame = {
      // We may have added new column names and/or changed column types
      val types = updateColInfo1(cols, defB)
      // Operate on the original column names
      val newOps = ops(f, cols: _*)
      new Frame(types, newOps, src)
    }


    /**
    /**
      * Extracts the first output parameter from the two parameters
      * used to indicate the one input columns and one output column
      * of the [[Frame]]. It then constructs a frame's type [[Row]]
      * with the possibly new type of the output and may add the
      * output column as a new column.
      *
      * @param cols column names of the function's parameters
      * @param defB default value used to set the type of the output value
      * @tparam B type of output column
      * @return type [[Row]] with new or replaced columns types
      */
      */
    private def updateColInfo2[B](cols: Seq[(OldName, NewName)], defB:B): Row
    = updateColInfo[(OldName, NewName),B](i => i._2)(cols, defB)

    /**
      * Applies a function to one or more columns. Applying the function
      * may or may not change the output column's type. Here we allow for
      * two parameter (column names), the second column name (output) may or
      * may not be a new column name. Irrespective of whether or not it is a
      * new column or it is overwritten, the new column's value may be of a
      * new or different type from the existing column.
      *
      * @param f function to apply to one column at a time
      * @param cols column names (two parameters) on which to apply the
      *             function
      * @param defB default value to set the columns underlying type
      * @tparam A type of input column
      * @tparam B type of output column
      * @return frame with the columns changes according to the conversion
      *         function
      */
    def apply[A,B](f: A => B, cols: (String, String)*)(implicit defB: B): Frame = {
      // We may have added new column names and/or changed column types
      val types = updateColInfo2(cols, defB)
      // Operate on the original column names
      val newOps = ops(f, cols: _*)
      new Frame(types, newOps, src)
    }


    /**
      * Extracts the first output parameter from the three parameters
      * used to indicate the two input columns and one output column
      * of the [[Frame]]. It then constructs a frame's type [[Row]]
      * with the possibly new type of the output and may add the
      * output column as a new column.
      *
      * @param cols column names of the function's parameters
      * @param defB default value used to set the type of the output value
      * @tparam B type of output column
      * @return type [[Row]] with new or replaced columns types
      */
    private def updateColInfo3[B](cols: Seq[(OldName, OldName, NewName)], defB:B): Row
    = updateColInfo[(OldName, OldName, NewName),B](i => i._3)(cols, defB)


    /**
      * Applies a function to one or more column pairs. Applying the function
      * may or may not change the output column's type. Here we allow for
      * three parameter (column names), the third column name (output)  may or
      * may not be a new column name. Irrespective of whether or not it is a
      * new column or it is overwritten, the new column's value may be of a new
      * or different type from the existing column.
      *
      * @param f function to apply on two columns at a time
      * @param cols column names (three parameters) on which to apply the
      *             function
      * @param defB default value to set the columns underlying type
      * @tparam A1 type of 1st input column
      * @tparam A2 type of 2nd input column
      * @tparam B type of output column
      * @return frame with the columns changes according to the conversion
      *         function
      */
    def apply[A1,A2,B](f: (A1,A2) => B, cols:(String,String,String)*)
                      (implicit defB: B): Frame = {
      // We may have added new column names and/or changed column types
      val types = updateColInfo3(cols, defB)
      // Operate on the original column names
      val newOps = ops(f, cols: _*)
      new Frame(types, newOps, src)
    }


    /**
      * Filters the elements from the stream. It is not possible
      * to delay this as an operation on each row, we must go through
      * the stream and pick each row one at a time. During this
      * iteration, all pending operations are executed.
      *
      * @param f return true to keep the row otherwise return false
      * @return only returns the rows that have passed the filter
      */
    def filterWith(f:Row => Boolean): Frame = {
      // Note that we use our version of the iterable to first
      // convert the values before applying the filter
      //val dropped: Iterable[Row] = iterable.filter(f)
      val dropped: Iterable[Row] = lazyFilter(iterable)(f)
      // Transform already applied, so rest mapper
      new Frame(v, Mapper(), dropped)
    }

    /**
      * Ignores the first `n` elements. It is not possible to delay this as
      * an operation on each row, we must go through the stream and remove
      * each row one at a time. During this iteration, all pending operations
      * are executed.
      *
      * @param n number of elements to ignore
      * @return
      */
    def drop(n:Int): Frame = {
      // Make sure you apply prior transforms
      val dropped: Iterable[Row] = iterable.drop(n)
      // Transform already applied, so rest mapper
      new Frame(v, Mapper(), dropped)
    }

    /**
      * Automates the creation of a new data frame. It maps the rows of
      * this data frame in a new data frame by applying the function `op`
      * to each row lazily. We have to provide a `fields` [[Row]] to
      * indicate the name and types of the new fields.
      *
      * NOTE: no checks are made to ensure that the field names and
      * type indicated in the `fields` parameter are the same as those
      * that are actually generated.
      *
      * @param cols Row that indicates the new field's names and types
      * @param op the function used to map the current row into a new row
      * @return a new frame with the mapped row
      */
    def map(op: Row ⇒ Row, cols: (String,Val[_])*): Frame = {
      val newCols = Row(cols:_*)
      // Operate on the original column names
      val newOps = ops( op )
      val nv = v ++ newCols
      new Frame(nv, newOps, src)
    }

    /**
      * Automates the creation of a new data frame. It maps the rows of
      * this data frame in a new data frame by applying the function `op`
      * to each row lazily. We have to provide a `fields` [[Row]] to
      * indicate the name and types of the new fields.
      *
      * In this version the `op` can receive and return an out-of-bound
      * value. This can be used to hold "state" when traversing the
      * frame's rows'.
      *
      * NOTE: no checks are made to ensure that the field names and
      * type indicated in the `fields` parameter are the same as those
      * that are actually generated.
      *
      * @param acc accumulator that is passed on to ach call of the `op`
      * @param cols Row that indicates the new field's names and types
      * @param op the function used to map the current row into a new row
      * @tparam A type of the value that is passed on to `op`
      * @return a new frame with the mapped row
      */
    def foldMap[A](acc:A)(op: (A, Row) ⇒ (A,Row), cols: (String,Val[_])*): Frame = {
      // calling iterable will apply any pending operations
      val iter = iterable.iterator

      var acum = acc

      // Create an Iterable that will allow us to access the
      // iterator. On each iteration we convert all elements
      // of the same column to a vector of these elements
      val convertable = new Iterable[Row] {

        // Create the iterator using the underlying iterator to
        // generate and iterate through the windows
        override def iterator: Iterator[Row] = {
          val converter = new AbstractIterator[Row] {

            override def hasNext: Boolean = iter.hasNext

            override def next(): Row = {
              // Get the row
              val r = iter.next()
              val (nacum,fr) = op(acum,r)
              // non-mutability hidden
              acum = nacum
              fr
            }
          }
          converter
        }
      }
      val newCols = Row(cols:_*)
      val nv = v ++ newCols
      val defaultOps = Mapper()
      // Reset the conversion operations because they have already been applied
      new Frame(nv, defaultOps, convertable)
    }

    /**
      * We can fold over every column in each row. Each row will start
      * the fold with the initially provided accumulator. Make sure this
      * is immutable so that each fold works independently.
      *
      * @param acc accumulator used for each row
      * @param f accumulator function
      * @return a new frame were each row consists of the accumulated row
      */
    def foldLeft(acc:Row)(f:(Row,(String,Val[_])) => Row): Frame = {
      val newOps = ops.foldLeft(acc){ case (ac,(k,e)) => f(ac,(k,e))}
      new Frame(acc, newOps, src)
    }

    /**
      * Creates a sliding window of the stream data. To create the sliding
      * window we have to indicate the window size and the stride's size.
      * These values may also be selected to create sliding windows that
      * overlap. The window's data is buffered in a
      * [[scala.collection.immutable.Vector]].
      *
      * During this transformation, the conversion functions are also
      * applied. This allows us to tracks the field types correctly using
      * the current mapper.
      *
      * NOTE: do not use a `size` parameter because it is shadowed by the
      * Iterator's size method.
      *
      * NOTE: We use scala's Vector instead of the Java Array due to type
      * erasure
      *
      * @param ssize Size if the window
      * @param step Stride length
      * @return returns data frame of sliding windows
      */
    def window(ssize: Int, step: Int): Frame = {

      // Create an Iterable that will allow us to access the
      // iterator. On each iteration we convert all elements
      // of the same column to a vector of these elements
      val convertable = new Iterable[Row] {

        // Create the iterator using the underlying iterator to
        // generate and iterate through the windows
        override def iterator: Iterator[Row] = {
          val converter = new AbstractIterator[Row] {
            // NOTE: the use of a map prior to sliding seems to be done non-lazily
            // We generate the windows lazily here creating an underlying iterator
            // calling iterable will apply any pending operations
            val iter: Iterator[Iterable[Row]] = iterable.sliding(ssize, step)

            override def hasNext: Boolean = iter.hasNext

            override def next(): Row = {
              // Get the window
              val d: Iterable[Row] = iter.next()
              // Change it to an array for easier indexing
              val seq: Array[Row] = d.toArray
              // Create the vector versions of the columns to hold the window
              val ttmp: Row = v.map { case (k, e) =>
                // Use a mutable array to avoid out-of-memory exception with Vector
                // Use ArraySeq because it is polymorphic (Java array is invariant)
                val arr: Val[ArraySeq[_]] = e.arr0(ssize)
                (k, arr)
              }
              // For each row in the window add the elements to the appropriate column
              val newMap = seq.indices.foldLeft(ttmp) { (acc, i) =>
                // get window row at i
                val tmp: Row = seq(i)
                // For each column place the value in the correct column array
                val nAcc = tmp.foldLeft(acc) { (ac: Row, e) =>
                  val (k: String, ve: Val[_]) = e
                  val ne: Either[String, ArraySeq[Any]] = unpack[ArraySeq[Any]](ac(k))
                  val nv = ne match {
                    case Left(err) =>
                      ErrVal(s"Error window($size,$step) @ $i: " + err)
                    case Right(arr) =>
                      // change mutable array
                      arr.update(i, ve.v)
                      Val(arr)
                  }
                  // change mutable array
                  val newAcc = ac.updated(k, nv)
                  newAcc
                }
                nAcc
              }
              newMap
            }
          }
          converter
        }
      }
      // Create the vector versions of the columns to hold the window, used for typing
      val nv: Row = v.map { case (k, e) =>
        val arr: Val[Array[_]] = e.arr(ssize)
        (k, arr)
      }

      val defaultOps = Mapper()
      // Reset the conversion operations because they have already been applied
      new Frame(nv, defaultOps, convertable)
    }

    // TODO: add the reverse of a window (restriction size = stride)

    /**
      * Combines at least 2 stream frames. It does this by combining
      * the field typing, the conversion mappers, the field names
      * mapper and the conversion functions mapper.
      *
      * The stream data is obtained by combining the rows during
      * iteration. Iteration will end as soon as one of combined frames
      * (iterators) ends.
      *
      * NOTE: if the stream frames have fields with the same name, only
      * one field will be kept. No guarantees are given as to which frame's
      * field will be used.
      *
      * We have two options to process the underlying streams. Either use the
      * stream's iterator to apply the underlying ops and then combine the
      * results or first combine the streams to create the full row and then
      * apply the combined op on that final row. We have opted for the second
      * (latter) option.
      *
      * @param s1 first frame to combine
      * @param s2 combines with these frames
      * @return a single stream consisting of the combined stream frames
      */
    def extend(s1:Frame, s2:Frame*): Frame = {
      // Get the header and tag the type
      val cols = s2.foldLeft(s1.v){ case (acc,si) => acc ++ si.v }

      // Operations to apply
      val nops = Mapper()

      // the data
      // val data: List[Iterable[Row]] = List(s1.src) ++ s2.map(_.src)
      val data: List[Iterable[Row]] = List(s1.iterable) ++ s2.map(_.iterable) // or

      // Create iterator on data
      val nsrc = new Iterable[Row] {
        override def iterator: Iterator[Row] = new AbstractIterator[Row] {
          private val iter: List[Iterator[Row]] = data.map(_.iterator)

          override def hasNext: Boolean = iter.forall(p => p.hasNext)

          override def next(): Row =
            iter.map(_.next).reduce((a, b) => a ++ b)
        }
      }

      new Frame(cols, nops, nsrc)
    }

    /**
      * Combines at least 2 stream frames.
      *
      * @see [[extend]]
      * @param that combines with these frames
      * @return a single stream consisting of the combined stream frames
      */
    def || (that:Frame*): Frame = extend(this, that:_*)


    def append(s1:Frame, s2:Frame*): Frame = {

      //val sameHeaders = s2.forall( f => f.v == s1.v)
      val sameHeaders = s2.forall( f => f.colTypes == s1.colTypes)
      if (!sameHeaders)
        Frame.empty
      else {
        // All frames have the same fields/columns
        val cols = s1.v

        // Operations to apply, none. Execute them now
        val nops = Mapper()

        // the data
        // val data: List[Iterable[Row]] = List(s1.src) ++ s2.map(_.src)
        val data: Iterator[Iterable[Row]] = Iterator(s1.iterable) ++ s2.map(_.iterable) // or execute the ops

        // Create iterator on data
        val nsrc = new Iterable[Row] {

          private def nextTopIterator(i: Iterator[Iterator[Row]]): Iterator[Row] =
            if (i.isEmpty) emptyIterable.iterator else i.next()

          override def iterator: Iterator[Row] = new AbstractIterator[Row] {
            private val readers: Iterator[Iterator[Row]] = data.map { e => e.iterator }
            private var iter: Iterator[Row] = nextTopIterator(readers)

            override def hasNext: Boolean =
              if (iter.hasNext)
                true
              else {
                iter = nextTopIterator(readers)
                iter.hasNext
              }

            override def next(): Row = iter.next()
          }
        }
        new Frame(cols, nops, nsrc)
      }
    }

    def ++ (that:Frame*): Frame = append(this, that:_*)

  }

  object Frame {
    import com.github.tototoshi.csv.CSVFormat
    import com.github.tototoshi.csv.CSVReader

    /**
      * Creates a stream from a set sequence of streams. Each source stream
      * must be named. All fields must be of the same type. A client can
      * iterate throw the data [[Row]]s
      *
      * @param data tuple containing the field name and field's data.
      * @tparam T the type of all the frame's fields. Start with
      *           strings when working with files for example.
      * @return a stream frame
      */
    def apply[T](data: (String, Iterator[T])*)(implicit default: T): Frame = {
      // Crate the row with column name and row type info
      //val defaults: Array[Val[T]] = Array.ofDim(data.size)
      val defVal:Val[T] = pack[T](default)
      val defaults: Array[Val[T]] = Array.fill(data.size)(defVal)
      val cols = Row( data.zip(defaults).map( e => (e._1._1, e._2) ).toMap )
      // Default operation is the identity
      val ops = Mapper()
      // Create iterator on data
      val src = new Iterable[Row] {
        override def iterator: Iterator[Row] = new AbstractIterator[Row] {

          override def hasNext: Boolean = data.forall( p => p._2.hasNext)

          override def next(): Row = {
            val cols = data.map { case (k,v) =>
              val e: Val[T] = v.next()
              (k,e)
            }
            Row(cols.toMap)
          }
        }
      }
      new Frame(cols, ops, src)
    }

    /**
      * Creates a stream from a set sequence of streams. Each source stream
      * must be named.
      *
      * Note: last dummy implicit used to avoid duplicate method error.
      * @see // https://stackoverflow.com/questions/3307427/scala-double-definition-2-methods-have-the-same-type-erasure
      *
      * @param data tuple containing the field name and field's data.
      * @return a stream frame
      */
    def apply(data: (String, Seq[String])*)(implicit d: DummyImplicit): Frame = {
      val iters: Seq[(String, Iterator[String])] = data.map{ case (k,v) => k -> v.toIterator }
      apply[String](iters:_*)
    }

    /* CSV files */

    /**
      * Utility function to create an empty iterator.
      * @return
      */
    private def emptyIterable: Iterable[Row] = {
      val src = new Iterable[Row] {
        override def iterator: Iterator[Row] = new AbstractIterator[Row] {
          override def hasNext: Boolean = false

          override def next(): Row = Row()
        }
      }
      src
    }

    /**
      * Creates an empty stream frame.
      * @return Creates an empty stream frame.
      */
    def empty = new Frame(Row(), Mapper(), emptyIterable)

    /**
      * Creates a stream frame *header* from a CSV data file. Assume a
      * header always exists. The header is used to obtain the field's
      * names. Note that all field names wll be trimmed and made lower
      * case.
      *
      * @param file CSV format data file with header
      * @param format Contains CSV parsing configuration information
      *               (example the separator character)
      * @return the underlying data reader stream, the field names header
      *         and an empty stream frame with the header information
      */
    private def csvInfo(file: File, format: CSVFormat): (CSVReader, Seq[String], Frame) = {
      val reader: CSVReader = CSVReader.open( file.toJava )(format)
      val headerI: List[Seq[String]] = reader.iterator.take(1).toList
      if (headerI.length == 1){
        // Get the header and tag the type
        // Column names
        val header: Seq[String] = headerI.head.map(_.trim)
        // Columns types (String)
        val colPairs = List.fill(header.length)(Val(""))
        val colTuples = header.zip(colPairs)
        // Row with column info
        val cols = Row(colTuples:_*)
        // Default operation is the identity
        val ops: Mapper = Mapper()
        // Create iterator on data
        val src = emptyIterable
        // Note that we have already consumed the header,
        // a call to the iterator will continue with the next line
        (reader, header, new Frame(cols, ops, src))
      } else {
        // Cannot parse, so empty iterator
        (reader, List(), empty)
      }
    }

    /**
      * Creates the iterator over the data of the CSV data.
      * Assumes that [[csvInfo]] already consumed the header.
      *
      * @see [[csv]], [[csv]], [[extendCSVs]]
      *
      * @param reader underlying data reader iterator (header
      *               already consumed)
      * @param header field names header
      * @return [[Row]] iterator over the data
      */
    private def csvIterator(reader: CSVReader, header: Seq[String]):
    Iterable[Row] = {
      val src = new Iterable[Row] {
        override def iterator: Iterator[Row] = new AbstractIterator[Row] {
          private val iter: Iterator[Seq[String]] = reader.iterator

          override def hasNext: Boolean = iter.hasNext

          override def next(): Row = {
            val row: Seq[Val[String]] = iter.next().map(Val[String])
            val cols: Row = Row(header.zip(row):_*)
            cols
          }
        }
      }
      src
    }

    /**
      * Creates a stream frame from a CSV data file.
      * Assume the CVS file has a header that is used
      * to name all the fields. All field names are
      * trimmed and converted to lower case.
      *
      * @see [[csv]], [[csv]], [[extendCSVs]]
      *
      * @param file CSV format data file with header
      * @param format Contains CSV parsing configuration information
      *               (example the separator character)
      * @return stream frame with the file data
      */
    def csv(file: File)(implicit format: CSVFormat): Frame = {
      val (reader, header, info) = csvInfo(file, format)
      val src = csvIterator(reader, header)
      new Frame(info.v, info.ops, src)
    }


    /**
      * Used to split the data files into training and test sets.
      */
    object SplitOptions {

      def all[T](i: Iterator[T]): Iterator[T] = i
      def takeOnly[T](takeN: Int)(i: Iterator[T]): Iterator[T] = i.take(takeN)
      def dropOnly[T](dropN: Int)(i: Iterator[T]): Iterator[T] = i.drop(dropN)
      def takeThenDrop[T](takeN: Int, dropN:Int)(i: Iterator[T]): Iterator[T] = i.slice(takeN, dropN)
      def dropThenTake[T](takeN: Int, dropN:Int)(i: Iterator[T]): Iterator[T] = i.slice(dropN, dropN + takeN)
    }

    /**
      * Creates a stream frame from a sequence of CSV data files whose
      * filenames (for example) have been parsed for additional (constant)
      * features. The [[Row]] of each file will consist of the file's fields
      * and the set of features parsed from the file names.
      *
      * NOTE: the header will be constructed from the first file's header
      * and file name feature set. The header and file name format of the
      * rest of the files are assumed to be the same and are neither checked
      * or used.
      *
      * @see [[pt.inescn.etl.stream.DataFiles.fileNamesfeatures]],
      *     [[pt.inescn.etl.stream.DataFiles.ignoreParseErrors]]
      * @see [[csvInfo]], [[csv]], [[csv]], [[extendCSVs]]
      *
      * @param parsesData contains a sequence with a tuple.The first element is
      *                   the file handle and the second the [[Row]] containing
      *                   the features encoded in the file name.
      * @param format Contains CSV parsing configuration information
      *               (example the separator character)
      * @return stream frame with the files data
      */
    def extendCSVs(format: CSVFormat, parsesData:Iterator[(File, Row)], sliceOp: Iterator[Row] => Iterator[Row] = SplitOptions.all): Frame = {

      // Get the first entry
      // NB: once you buffer, don't use the underlying iterator !
      val fileData = parsesData.buffered
      val header: (File, Row) = fileData.head
      // Get the header info (column name and types and the default mapper)
      val (_, _, multiCols) = csvInfo(header._1,format)
      // We assume all columns are correct, so duplicate the first one
      val cols = multiCols.v ++ header._2
      val ops = multiCols.ops
      // Now go through all the files and get the iterator for each
      val srcs: Iterator[Iterable[Row]] = fileData.map {
        case (f,d) =>
          // Get information about the data in the file
          val (reader, header, _) = csvInfo(f,format)
          // Create the file data iterator
          val src:Iterable[Row] = csvIterator(reader, header)
          // d has the features (row) obtained from the data file name
          // add those features to the files data features (row)
          src.map(_ ++ d )
      }
      // Create a single top-level iterator that scans each file separately
      val src: Iterable[Row] = new Iterable[Row] {

        private def nextTopIterator(i: Iterator[Iterator[Row]]): Iterator[Row] =
          if (i.isEmpty) emptyIterable.iterator else i.next()

        override def iterator: Iterator[Row] = new AbstractIterator[Row] {
          private val readers: Iterator[Iterator[Row]] = srcs.map{ e => sliceOp(e.iterator) }
          private var iter: Iterator[Row] = nextTopIterator(readers)

          override def hasNext: Boolean =
            if (iter.hasNext)
              true
            else {
              iter = nextTopIterator(readers)
              iter.hasNext
            }

          override def next(): Row = iter.next()
        }
      }
      // Assume all headers are the same
      new Frame(cols, ops, src)
    }

    /**
      * Creates a stream frame from a sequence of CSV data files.
      * The data is read and processed in the order of the fle sequence.
      * Assume the CVS files have a header that is used to name
      * all the fields. All field names are trimmed and converted
      * to lower case.
      *
      * NOTE: the header will be constructed from the first file.
      * The header of the rest of the files are assumed to be the
      * same and are neither checked or used.
      *
      * @see [[csv]], [[csv]], [[extendCSVs]]
      *
      * @param files CSV format data files with header
      * @param format Contains CSV parsing configuration information
      *               (example the separator character)
      * @return stream frame with the files data
      */
    def csvs(format: CSVFormat, files:Seq[File], sliceOp: Iterator[Row] => Iterator[Row] = SplitOptions.all): Frame = {
      val parsedData: Iterator[(File, Row)] = files.map(f => (f, Row())).toIterator
      extendCSVs(format, parsedData, sliceOp)
    }


    /**
      * This function is used load the files that were split into a training
      * and test set. These sets can, for example, be selected according to
      * a set of features that are parsed from the file name. The data in
      * the file must be in the CSV format and the parsing is done according
      * to the specified CSV `format` (separator, etc.).
      *
      * The [[Row]] associated to each file will be appended to the data
      * [[Row]] of the file content. This information is usually obtained from
      * the parsed file name.
      *
      * When using this function we should ensure that no data is leaked
      * between the training and test data sets. This means that the records
      * in the training set **must** **not** appear in the test set.
      *
      * In addition to this we can slice the data from each file to ensure no
      * leaked data occurs when the same files appear in both the train and
      * test sets. More concretely we can use only the first `nLearnSamples`
      * records from the training data. We can also drop the first
      * `nIgnoreTestSamples` of the test data and sample the next
      * `nTestSamples` available samples.
      *
      * The data is returned as a single stream consisting of all the training
      * data first then the test data.
      *
      * @param format CSV formatting used
      * @param learn training data sets
      * @param test test data sets
      * @param nLearnSamples number of samples to read from the start of the
      *                      training data sets
      * @param nIgnoreTestSamples number of samples dropped from the start of
      *                           the test data sets
      * @param nTestSamples number of samples read from the rest of the test
      *                     data sets
      * @param shuffle if true shuffles the data sets
      * @return a single stream with trainning data first and then test data
      */
    def splitExtendCSVs(format: CSVFormat,
                           learn:Iterator[(File, Row)],
                           test:Iterator[(File, Row)],
                           nLearnSamples: Int,
                           nIgnoreTestSamples: Int,
                           nTestSamples: Int,
                           shuffle: Boolean = true): (Frame, Frame) = {

      // Make data exclusive (No leaking !!)
      val learnFrames = if (nLearnSamples > 0)
        extendCSVs(format, learn, SplitOptions.takeOnly(nLearnSamples))
      else
        extendCSVs(format, learn, SplitOptions.all)
      val testFrames = if ((nIgnoreTestSamples > 0) && (nTestSamples > 0)) {
        // Sample for testing
        //println(s"testFrames: dropThenTake($nIgnoreTestSamples, $nTestSamples)")
        extendCSVs(format, test, SplitOptions.dropThenTake(nIgnoreTestSamples, nTestSamples))
      } else if ((nIgnoreTestSamples > 0) && (nTestSamples <= 0)) {
        //println(s"testFrames: dropThenTake($nIgnoreTestSamples)")
        extendCSVs(format, test, SplitOptions.dropOnly(nIgnoreTestSamples))
      }
      else {
        //println(s"testFrames: all")
        // Use all of the available data for testing
        extendCSVs(format, test, SplitOptions.all)
      }

      // Make sure we don't leak test data
      (learnFrames, testFrames)
    }



  } // Frame


  /**
    * Saves a data stream of [[Row]]s to a CSV file. The data is saved in
    * CSV format using a given `format`. A header is saved as a first line
    * in the file. If no header is required, use an empty sequence. Note that
    * all values in [[Val]] are first converted to a string.
    *
    * @param f file that will contain the data. If it exists, it is overwritten.
    * @param format CSV format used (example separator character used).
    * @param header CSV file header
    * @param data data stream of [[Row]]s
    */
  def save(f:File, format: CSVFormat, header: Seq[String], data:Iterator[Row]): Unit = {
    val tmp: CSVWriter = CSVWriter.open(f.toJava)(format)

    if (header.nonEmpty) tmp.writeRow(header)
    data.foreach { e =>
      val line = e.r.map { case (_,v) => v.v.toString }.toSeq
      tmp.writeRow(line)
    }
    tmp.close()
  }

  /**
    * Saves a data [[Frame]] to a CSV file. The data is saved in
    * CSV format using a given `format`. A header consist of the Frame's
    * column names and is saved as a first line in the file.
    *
    * @param f file that will contain the data. If it exists, it is overwritten.
    * @param format CSV format used (example separator character used).
    * @param data data [[Frame]] consisting of a stream of [[Row]]s
    */
  def save(f:File, format: CSVFormat, data:Frame): Unit =
    save(f, format, data.colIDs, data.iterable.toIterator)

  /*
     Default values used for creating default Val types
   */

  implicit val defaultStr: String = ""
  implicit val defaultBool: Boolean = false
  implicit val defaultInt: Int = Int.MinValue
  implicit val defaultLong: Long = Long.MinValue
  implicit val defaultFloat: Float = Float.NaN
  implicit val defaultDouble: Double = Double.NaN
  implicit val defaultDateTime: LocalDateTime = LocalDateTime.MIN
  implicit val defaultPeriod: Period = Period.ZERO
  implicit val defaultDuration: Duration = Duration.ZERO

  /*
  def main(args: Array[String]): Unit = {

    // Ok 1
    val r5: Val[String] = "100"
    println(s"r5: Val(${r5.v}) : ${r5.v.getClass}")
    val r6: Val[Double] = 200.0
    println(s"r6: Val(${r6.v}) : ${r6.v.getClass}")
    val r7: Val[Array[Double]] = Array(300.0, 301, 302)
    println(s"r7: Val(${r7.v.mkString(",")}) : ${r7.v.getClass}")

    val r0 = Row("x" -> r5, "y" -> r6, "z" -> r7)
    println(r0)


  }
*/
}
