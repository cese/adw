package pt.inescn.etl.stream

import java.io.IOException
import java.util.concurrent.locks.ReentrantLock

import com.rabbitmq.client._
import pt.inescn.app.RabbitConfigs.{ReceiveAppConfig, SendAppConfig}
import pt.inescn.app.{Configs, Utils}
import pt.inescn.etl.stream.Load.{Mapper, Row, Val}
import pt.inescn.utils.ADWError
import pt.inescn.etl.stream.Load.Frame

import scala.collection.AbstractIterator
import scala.collection.mutable.ListBuffer

/**
  * This object contains the functions to create a [[Row]] iterable that allows
  * a client to consume data from a RabbitMQ message broker synchronously. These
  * iterables either poll the message broker explicitly or use a callback
  * function that is appropriately synchronized.
  *
  * @see [[inegi.ReceiveApp]] for examples.
  */
object RabbitMQ {

  /*
    * Loading connection configurations
    */

  /**
    * Connection parameters used to open connection with the RabbitMQ message
    * broker. This includes the host, port number and login credentials.
    *
    * @param params dictionary that stores the tuple parameter key and
    *               parameter value
    */
  case class Config(params: Map[String, String] = Map()) {

    val HOST = "host"

    def host: String = params(HOST)

    def setHost(hostSet: String): Config = {
      val temp = params.updated(HOST, hostSet)
      Config(temp)
    }

    val VHOST = "vhost"

    def vhost: String = params(VHOST)

    def setvHost(hostSet: String): Config = {
      val temp = params.updated(VHOST, hostSet)
      Config(temp)
    }

    val PORT = "port"

    def port: Int = params(PORT).toInt

    def setPort(portSet: Int): Config = {
      val temp = params.updated(PORT, portSet.toString)
      Config(temp)
    }

    val QUEUE = "queue"

    def queueName: String = params(QUEUE)

    def setQueueName(queueSet: String): Config = {
      val temp = params.updated(QUEUE, queueSet)
      Config(temp)
    }

    val QUEUE1 = "queue1"

    def queueName1: String = params(QUEUE1)

    def setQueueName1(queueSet: String): Config = {
      val temp = params.updated(QUEUE1, queueSet)
      Config(temp)
    }

    val QUEUE2 = "queue2"

    def queueName2: String = params(QUEUE2)

    def setQueueName2(queueSet: String): Config = {
      val temp = params.updated(QUEUE2, queueSet)
      Config(temp)
    }

    val QUEUE3 = "queue3"

    def queueName3: String = params(QUEUE3)

    def setQueueName3(queueSet: String): Config = {
      val temp = params.updated(QUEUE3, queueSet)
      Config(temp)
    }

    val QUEUE4 = "queue4"

    def queueName4: String = params(QUEUE4)

    def setQueueName4(queueSet: String): Config = {
      val temp = params.updated(QUEUE4, queueSet)
      Config(temp)
    }

    private def exists(queueName: String): Boolean = params.contains(queueName)

    def queueNames: List[String] = {
      val temp = List(queueName)

      val temp2 = if (exists(QUEUE1)) queueName1 :: temp else temp
      val temp3 = if (exists(QUEUE2)) queueName2 :: temp2 else temp2
      val temp4 = if (exists(QUEUE3)) queueName3 :: temp3 else temp3
      val temp5 = if (exists(QUEUE4)) queueName4 :: temp4 else temp4
      temp5
    }

    val USERNAME = "user"

    def user: String = if (params.contains(USERNAME)) params(USERNAME) else ""

    def setUserName(userSet: String): Config = {
      val temp = params.updated(USERNAME, userSet)
      Config(temp)
    }

    val PASS = "password"

    def password: String = if (params.contains(PASS)) params(PASS) else ""

    def setPassword(passSet: String): Config = {
      val temp = params.updated(PASS, passSet)
      Config(temp)
    }

    val TOPIC = "topic"

    def topic: String = params(TOPIC)

    def setTopic(topicSet: String): Config = {
      val temp = params.updated(TOPIC, topicSet)
      Config(temp)
    }

    val VARID = "varID"

    def varID: String = params(VARID)

    def varID(varIDSet: String): Config = {
      val temp = params.updated(VARID, varIDSet)
      Config(temp)
    }

    val VARTs = "varTs"

    def varTs: String = params(VARTs)

    def varTs(varTsSet: String): Config = {
      val temp = params.updated(VARTs, varTsSet)
      Config(temp)
    }

    val TearID = "tear"

    def tearID: String = params(TearID)

    def tearID(tearIDSet: String): Config = {
      val temp = params.updated(TearID, tearIDSet)
      Config(temp)
    }

    val LowRange = "range_lb"

    def lowrange: String = params(LowRange)

    def lowrange(lowrangeSet: String): Config = {
      val temp = params.updated(LowRange, lowrangeSet)
      Config(temp)
    }

    val UpperRange = "range_ub"

    def uprange: String = params(UpperRange)

    def uprange(uprangeSet: String): Config = {
      val temp = params.updated(UpperRange, uprangeSet)
      Config(temp)
    }
  }

  def from(filepath: String): Config = {
    try {
      val temp = Configs.loadFrom(filepath)
      Config(temp)
    }
    catch {
      case e: Exception =>
        throw new RuntimeException("Cannot access the file: " + filepath, e)
    }
  }

  def boot(filename: String): Config = {
    val tmp = Configs.bootstrap(filename)
    tmp match {
      case Some(f) => Config(f)
      case None => throw new RuntimeException("Cannot access the file: " + filename)
    }
  }


  /**
    * Contains the information and state for the connection to
    * a RabbitMQ message broker.
    *
    * @param queueNames     queue name of messages
    * @param channel        open channel to read and write messages
    * @param conn           open connection to the message broker
    * @param error          record of any errors that may have occured during
    *                       connection set-up
    * @param waitMultiplier the time in seconds used to multiply to the
    *                       exponential back-off waiting used when polling
    *                       for new messages.
    */
  case class RabbitConnection(queueNames: List[String],
                              queueName: String,
                              channel: Channel,
                              conn: Connection,
                              error: ListBuffer[String] = ListBuffer(),
                              waitMultiplier: Int = 1,
                              topic: String = "",
                              varID: String = "",
                              varTs: String = "",
                              tearID:String = "",
                              lowRange:Double = 0,
                              upRange:Double = 0)


  /**
    * Opens a connection with RabbitMQ server. If successful it returns
    * a live connection otherwise an error is reported.
    *
    * @param conf configuration of the application
    * @return Ether a right of [[RabbitConnection]] if the connection could be
    *         established, otherwise a left of an [[ADWError]]
    */
  def connect(conf: SendAppConfig): Either[ADWError, RabbitConnection] = {
    val factory = new ConnectionFactory
    // defines connection host
    factory.setHost(conf.connect.host)
    if (conf.connect.vhost != "") factory.setVirtualHost(conf.connect.vhost)
    factory.setPort(conf.connect.port)
    // login credentials
    if (conf.connect.user != "") factory.setUsername(conf.connect.user)
    if (conf.connect.password != "") factory.setPassword(conf.connect.password)
    try {
      // create a connection instance
      val conn: Connection = factory.newConnection
      // the connection interface can be used to open a channel
      val channel: Channel = conn.createChannel
      // Per consumer limit
      channel.basicQos(10)
      // Use this queue (will be created if ir does not exist)
      val queueName = conf.connect.queueName
      //queueNames are used to publish to multiple queues at once
      val queueNames = conf.connect.queueNames
      val iterConn = RabbitConnection(queueNames, queueName, channel, conn, ListBuffer())
      Right(iterConn)
    } catch {
      case e: java.lang.Exception =>
        val msg = e.getMessage + (if (conf.verbose > 0) "\n" + e.getStackTrace.mkString("\n") else "")
        Left(ADWError(msg))
    }
  }

  /**
    * Opens a connection with RabbitMQ server. If successful it returns
    * a live connection otherwise an error is reported.
    *
    * @param conf configuration of the application
    * @return Ether a right of [[RabbitConnection]] if the connection could be
    *         established, otherwise a left of an [[ADWError]]
    */
  def connect(conf: ReceiveAppConfig): Either[ADWError, RabbitConnection] = {
    val factory = new ConnectionFactory // create a connection instance
    factory.setHost(conf.connect.host) // defines connection host, in this case, local
    factory.setPort(conf.connect.port)
    if (conf.connect.vhost != "") factory.setVirtualHost(conf.connect.vhost)

    if (conf.connect.user != "") factory.setUsername(conf.connect.user)
    if (conf.connect.password != "") factory.setPassword(conf.connect.password)
    try {
      val conn: Connection = factory.newConnection // connection interface
      val channel: Channel = conn.createChannel // the connection interface can be used to open a channel
      // Per consumer limit
      channel.basicQos(10)

      /*
      In case one is subscribing block variables, the config file has the queuename id
      In case one is subscribing independent variables, the config file has the topic but the queuename needs to be created
       */
      val (queueName, topic) = if (conf.connect.queueName != "") (conf.connect.queueName, "")
      else {
        /*One cannot subscribe an exchange. Rather, one can subscribe a queue
        to which an exchange is redirecting messages. For such purpose, one can create temporary queues.*/

        //create a non-durable, exclusive, autodelete queue with a generated name
        val queueName = channel.queueDeclare().getQueue
        val topic: String = conf.connect.topic
        //bind the queue to the exchange, with the routing key from the config file
        channel.queueBind(queueName, "amq.topic", topic)
        (queueName, topic)
      }

      if (conf.connect.varID != "") factory.setUsername(conf.connect.user)

      val varID = conf.connect.varID
      val varTs = conf.connect.varTs
      val tearID = conf.connect.tearID
      val lowRange = if (conf.connect.lowrange != "") conf.connect.lowrange.toDouble else Double.NaN
      val upRange = if (conf.connect.lowrange != "") conf.connect.uprange.toDouble else Double.NaN
      //queueNames are used to subscribe multiple queues at once
      val queueNames = conf.connect.queueNames

      val iterConn = RabbitConnection(queueNames, queueName, channel, conn, ListBuffer(), conf.waitMultiplier,
        topic, varID, varTs, tearID, lowRange, upRange)
      Right(iterConn)
    } catch {
      case e: java.lang.Exception =>
        val msg = e.getMessage + (if (conf.verbose > 0) "\n" + e.getStackTrace.mkString("\n") else "")
        Left(ADWError(msg))
    }
  }


  /**
    * Close the connection and the channel.
    *
    * @param conn [[RabbitConnection]] with the connection and channel to close.
    */
  def close(conn: RabbitConnection): Unit = {
    conn.channel.close()
    conn.conn.close()
  }


  /**
    * This function takes a `Map[String, Vector[String] ]` converts it into a
    * sequence of [[Row]]. Each key of the map represents a Row's column (field).
    * Each vector represents several records of that Row's columns. Each map is
    * therefore converted into a sequence of Rows. Each row can be consumed via
    * an iterator that represents this sequence.
    *
    * @see [[pt.inescn.app.Utils.toJSONIterator()]], [[inegi.SendApp]]
    * @param Msg Map with the message's data records
    * @return iterator with Rows containing a message's data records
    */
  def splitMap(Msg: Map[String, Vector[String]]): Iterator[Row] = {
    val keys = Msg.keys.toList
    val bufferSize = Msg(keys.head).length
    val buf: ListBuffer[Row] = ListBuffer()
    var i = 0
    while (i <= bufferSize - 1) {
      buf += Row(Msg.map { case (e1, e2) => (e1, Val(e2(i))) })
      i += 1
    }
    buf.toIterator
  }


  /*
  These variables and the two stop function are remainings of previous versions of poll and callback;
  they are maintained for the fact that they are used in specific function calls, not related with
  poll and callback, which is now defined differently
    */
  private val lock     = new ReentrantLock // Reader/writer lock
  private val notFull  = lock.newCondition // Signal when we need writer to get message
  private val notEmpty = lock.newCondition // Signal when we need reader to process message

  // Message counter. Also used as a stop flag.
  // when  0: no data in buffer, writer needs to get message and place it in
  //          the buffer
  // When  1: one message in the buffer (no more are stored)
  // When -1: Stop processing. No need to update the counter.
  private var cnt      = 0


  /**
    * Closes the channel and connection to the message broker.
    * This allows the client application to terminate. If the
    * callback API is used, we first cancel the callback so that
    * the minimum number of messages are lost. Any pending
    * callback are executed and the corresponding messages are
    * lost.
    *
    * @param conf Message broker's communication configuration
    * @param con  Message broker's communication channel
    */
  def stop(conf: ReceiveAppConfig, con: RabbitConnection): Unit = {
    //def stop(conf: ReceiveAppConfig, con: RabbitConnection, cbState: CallbackState): Unit = {
    conf.iter match {
      case conf.CALLBACK =>
        // Deactivate the consumer ASAP, no more callbacks invoke
        con.channel.basicCancel("myConsumerTag")
        // We don't need the connection because we stop here
        close(con)
        // The consumer calls may be locked or waiting
        lock.lock()
        // lets flag the end for these calls
        cnt = -1
        // If the iterator's writer is waiting
        // Lets signal the end of the writer
        // Note that some pending messages already received will be lost
        notEmpty.signal()

        // This is not required after a take(x), but is useful
        // when called from another thread - early stopping
        // If the iterator's reader is waiting
        // Lets signal the end of the reader
        // signal the reader to process the flag
        notFull.signal()
        lock.unlock()
      case _ =>
        // Just close the channel and connection
        close(con)
    }

  }

  def stop(conf: ReceiveAppConfig, con: Either[ADWError, RabbitConnection]): Unit = {
    con.map(c => stop(conf, c))

  }

  /** Diff approach: base trait connect
    * Trait ConnectState
    * CallbackState extends ConnectState
    * PollingState extends ConnectState
    * */

  trait ConnectState {
    def frame: Frame
    def stop //(con: RabbitConnection)
    def conn: RabbitConnection
  }

  class PollingManualState(con: RabbitConnection, toIter: String => Iterator[Row]) extends ConnectState {


    val frame = pollingManualFrame

    override def stop = close(con)  // Just close the channel and connection

    override def conn = con

    private def pollingManualFrame: Frame = {
      val iter = pollManual//(con, toIter)
      val row = iter.toIterator.next()
      val frame = new Frame(row, Mapper(), iter)
      frame
    }

    private def pollManual: Iterable[Row] = new Iterable[Row] {
      private val backoff = Array(0.001, 0.01, 0.1, 1, 10, 100, 1000) // Exponential back-off wait multiplier
      private var cnt = 0 // Used to increase back-off wait multiplier
      private val backoffMax = backoff.length - 1

      private val queueName = conn.queueName // Message broker's queue name
      private val channel = conn.channel // Message broker's communication channel
      private var msg: GetResponse = _ // Data sent by message broker's
      private var iterRow: Iterator[Row] = Iterator() // Temporary storage of decomposed message

      /**
        * Creates an iterator the "pulls" data from the message broker.
        * It does this any using the synchronous "pull" API. If no
        * (not enough) data is available it will fail (throw an exception).
        * If we iterate over a fixed number of elements that do exist, the
        * iteration terminates.
        *
        * @return Iterator of [[Row]]
        */
      override def iterator: Iterator[Row] = new AbstractIterator[Row] {

        /**
          * We are consuming a data stream but we do not want to
          * block on the reception of data. Use this to peek if
          * a message exists. It none exists on the buffer we check
          * for messages from the message broker and store it in a temporary
          * buffer and initialize an internal iterator that parsers and
          * converts this buffer into one or more [[Row]]. We assume all data
          * is sent as strings. As long as this buffer has [[Row]]s we
          * return these one at a time via next.
          *
          * @return True if a buffered a message exists or if a
          *         new messages can be buffered.
          */
        override def hasNext: Boolean = true

        /**
          * We "pull" messages from the message buffer. If not data is
          * available throw an exception. This method should only be
          * called after a call to hasNext.
          *
          * @return the next Row that is available for processing
          */
        override def next(): Row = {
          if (iterRow.hasNext) {
            iterRow.next()
          }
          else {
            var temp = true
            while (temp) {
              // "Pull" another message
              msg = channel.basicGet(queueName, true)
              if (msg != null) {
                // If we have data place it into the temporary buffer
                val buffer = msg.getBody.map(_.toChar).mkString
                // Decompose it into the records
                iterRow = toIter(buffer)
                /* Acknowledge reception
                val deliveryTag = msg.getEnvelope.getDeliveryTag
                channel.basicAck(deliveryTag, false)*/
                temp = false
              } else {
                // No data available, calculate time to wait
                val t = backoff(cnt) * conn.waitMultiplier
                // Wait
                Thread.sleep(t.toLong)
                // Increase back-off wait time
                cnt = if (cnt >= backoffMax) backoffMax else cnt + 1
              }
            }
            iterRow.next()
          }
        } // next
      }
    }
  }

  class PollingState(con: RabbitConnection, toIter: String => Iterator[Row]) extends ConnectState {


    val frame = pollingFrame

    override def stop = close(con)  // Just close the channel and connection

    override def conn = con

    private def pollingFrame: Frame = {
      //val f = if (con.topic != "") Utils.toStringIterator_noHeader(con.varTs,con.varID) _ else Utils.toStringIterator _
      val iter = poll//(con, toIter)
      val row = iter.toIterator.next()
      val frame = new Frame(row, Mapper(), iter)
      frame
    }

    /** ***************************************
      * Pull API iterator
      * ***************************************/

    /**
      * Creates a [[Row]] iterator that allows a client to consume data from the
      * message broker synchronously. It will "pull" data from the message broker
      * if it exists and decomposes it into a sequence of records each converted
      * into a [[Row]]. We assume that all data is received as a String. If no
      * data is available, this function will wait indefinitely for the next
      * data. It uses an exponential back-off wait time (via `Thread.sleep`).
      *
      * We use RabbitMQ's "Pull" API. No provision is made for automated
      * reconnection if communications failures occur.
      *
      * @see https://www.rabbitmq.com/api-guide.html#getting
      * @return An iterable of [[Row]]
      */
    private def poll: Iterable[Row] = new Iterable[Row] {

      private val backoff = Array(0.001, 0.01, 0.1, 1, 10, 100, 1000) // Exponential back-off wait multiplier
      private var cnt = 0 // Used to increase back-off wait multiplier
      private val queueName = conn.queueName // Message broker's queue name
      private val channel = conn.channel // Message broker's communication channel
      private var msg: GetResponse = _ // Data sent by message broker's
      private var iterRow: Iterator[Row] = Iterator() // Temporary storage of decomposed message

      private val backoffMax = backoff.length - 1

      /**
        * Creates an iterator the "pulls" data from the message broker.
        * It does this any using the synchronous "pull" API. If no
        * (not enough) data is available it will wait indefinitely. If
        * we iterate over a fixed number of elements that do exist, the
        * iteration terminates.
        *
        * @return Iterator of [[Row]]
        */
      override def iterator: Iterator[Row] = new AbstractIterator[Row] {

        /**
          * We are consuming a data stream so data is always expected
          *
          * @return Always true
          */
        override def hasNext: Boolean = true

        /**
          * We "pull" messages from the message broker. If data exists we store
          * it in a temporary buffer and initialize an internal iterator that
          * parsers and converts this buffer into one or more [[Row]]. We assume
          * all data is sent as strings. As long as this buffer has [[Row]]s we
          * return these one at a time.
          * If not data is available we wait for a given time and poll the
          * message broker again. The wait time is increased exponentially.
          *
          * @return the next Row that is available for processing
          */
        override def next(): Row = {
          var temp = true

          // Return a record from the decomposed message if their is one
          if (iterRow.hasNext) {
            iterRow.next()
          }
          else {
            // Temporary buffer exhausted, get another message
            while (temp) {
              // "Pull" another message
              msg = channel.basicGet(queueName, true)
              if (msg != null) {
                // If we have data place it into the temporary buffer
                val buffer = msg.getBody.map(_.toChar).mkString
                // Decompose it into the records
                iterRow = toIter(buffer)
                /* Acknowledge reception
                val deliveryTag = msg.getEnvelope.getDeliveryTag
                channel.basicAck(deliveryTag, false)*/
                // Reset the message pull-wait
                cnt = 0
                temp = false
              }
              else {
                // No data available, calculate time to wait
                val t = backoff(cnt) * conn.waitMultiplier
                // Wait
                Thread.sleep(t.toLong)
                // Increase back-off wait time
                cnt = if (cnt >= backoffMax) backoffMax else cnt + 1
              }
            }
            iterRow.next()
          }
        }
      }
    }
  }

  /**
    * @param con Message broker's communication channel
    * @param toIter function to use in the iterator creation
    */
  class CallbackState(con: RabbitConnection, toIter: String => Iterator[Row]) extends ConnectState {


    val lock = new ReentrantLock // Reader/writer lock
    val notFull = lock.newCondition // Signal when we need writer to get message
    val notEmpty = lock.newCondition // Signal when we need reader to process message
    var cnt = 0
    val autoAck = true // false // Constant - manually acknowledge messages
    val consumerTag = "myConsumerTag" // Constant - callback ID
    var iterRow = Iterator[Row]() // Temporary message buffer (writer)
    var msg = "" // Temporary message buffer (reader)
    var callBackOn = false // Keep track of callbacks. Just use one.

    val frame = callbackFrame

    override def conn = con

    override def stop = {
      // Deactivate the consumer ASAP, no more callbacks invoke
      con.channel.basicCancel("myConsumerTag")
      // We don't need the connection because we stop here
      close(con)
      // The consumer calls may be locked or waiting
      lock.lock()
      // lets flag the end for these calls
      cnt = -1
      // If the iterator's writer is waiting
      // Lets signal the end of the writer
      // Note that some pending messages already received will be lost
      notEmpty.signal()

      // This is not required after a take(x), but is useful
      // when called from another thread - early stopping
      // If the iterator's reader is waiting
      // Lets signal the end of the reader
      // signal the reader to process the flag
      notFull.signal()
      lock.unlock()
    }

    private def callbackFrame = {
      //val f = if (con.topic != "") Utils.toStringIterator_noHeader(con.varTs, con.varID) _ else Utils.toStringIterator _
      val iter = callBack//(toIter)
      val row = iter.toIterator.next()
      val frame = new Frame(row, Mapper(), iter)
      frame
    }

    /** ***************************************
      * Callback iterator
      * ***************************************/

    /**
      * Creates a [[Row]] iterator that allows a client to consume data from the
      * message broker synchronously. It will "receive" data from the message
      * broker via a callback and decomposes it into a sequence of records each
      * converted into a [[Row]]. We assume that all data is received as a String.
      * If no data is available, this function will wait indefinitely for the next
      * data.
      *
      * The data consumption and processing is done using the single reader and
      * write pattern. A write reads data from the message broker if the previous
      * one has been processed and places it in the buffer. It then signals the
      * reader that data is available for processing and wait for the buffer to
      * be empty. The reader processes the buffer when full. It then signals the
      * write that the buffer is now empty and needs replenishing and waits for
      * the next buffer full ready signal.
      *
      * @see https://www.rabbitmq.com/api-guide.html
      * @return An iterable of [[Row]]
      */
    private def callBack: Iterable[Row] = new Iterable[Row] {
      // Make sure that only one callback is active at any time
      // Any other iterable will not receive any more data
      //if (callBackOn) con.channel.basicCancel(consumerTag)
      if (callBackOn) con.channel.basicCancel(consumerTag)

      // Initialize the callback
      con.channel.basicConsume(con.queueName, autoAck, consumerTag,
        new DefaultConsumer(con.channel) {
          @throws[IOException]
          override def handleDelivery(consumerTag: String,
                                      envelope: Envelope,
                                      properties: AMQP.BasicProperties,
                                      body: Array[Byte]): Unit = {
            val deliveryTag = envelope.getDeliveryTag
            try {
              lock.lock() // locked because receive messages
              while (cnt == 1) {
                notFull.await()
              }
              //msg = body.map(_.toChar).mkString
              msg = body.map(_.toChar).mkString
              if (cnt != -1) {
                //con.channel.basicAck(deliveryTag, false)
                cnt = cnt + 1
                notEmpty.signal() // not Empty so can be read in the other side
              }
            }
            finally {
              lock.unlock()
            }
          }
        })
      callBackOn = true

      /**
        * Creates an iterator the "receives" data from the message broker.
        * It does this any using the asynchronous "callback" API. If no
        * (not enough) data is available it will wait indefinitely. If
        * we iterate over a fixed number of elements that do exist, the
        * iteration terminates.
        *
        * @return Iterator of [[Row]]
        */
      override def iterator: Iterator[Row] = new AbstractIterator[Row] {

        /**
          * We are consuming a data stream so data is always expected
          *
          * @return Always true
          */
        override def hasNext: Boolean = {
          true
        }

        /**
          * We "receive" messages from the message broker. If data exists we
          * store it in a temporary buffer and initialize an internal iterator
          * that parsers and converts this buffer into one or more [[Row]]. We
          * assume all data is sent as strings. As long as this buffer has
          * [[Row]]s we return these one at a time.
          * If not data is available we wait indefinitely for the callback to
          * execute. The callback also waits until the buffer has been
          * processed before placing the next message their.
          *
          * @return the next Row that is available for processing
          */
        override def next(): Row = {
          if (iterRow.hasNext) {
            iterRow.next()
          }
          else {
            try {
              lock.lock() // locked to copy the messages in this side
              while (cnt == 0) {
                notEmpty.await() // not empty so wait to process the messages
              }
              iterRow = toIter(msg)

              if (cnt != 0) {
                cnt -= 1
                notFull.signal() // messages are already processed so can read more
              }
            }
            finally {
              lock.unlock() // unlocked the reception
            }
            iterRow.next()

          }
        }
      }
    }
  }
}
