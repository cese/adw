package pt.inescn.utils
import pt.inescn.plot.JPlot


object NemenyiPlot{

  var CRITICAL_VALUES = Array(
    Array(2.576, 1.960, 1.645),  // 2
    Array(2.913, 2.344, 2.052),  // 3
    Array(3.113, 2.569, 2.291),  // 4
    Array(3.255, 2.728, 2.460),  // 5
    Array(3.364, 2.850, 2.589),  // 6
    Array(3.452, 2.948, 2.693),  // 7
    Array(3.526, 3.031, 2.780),  // 8
    Array(3.590, 3.102, 2.855),  // 9
    Array(3.646, 3.164, 2.920),  // 10
    Array(3.696, 3.219, 2.978),  // 11
    Array(3.741, 3.268, 3.030),  // 12
    Array(3.781, 3.313, 3.077),  // 13
    Array(3.818, 3.354, 3.120),  // 14
    Array(3.853, 3.391, 3.159),  // 15
    Array(3.884, 3.426, 3.196),  // 16
    Array(3.914, 3.458, 3.230),  // 17
    Array(3.941, 3.489, 3.261),  // 18
    Array(3.967, 3.517, 3.291),  // 19
    Array(3.992, 3.544, 3.319),  // 20
    Array(4.015, 3.569, 3.346),  // 21
    Array(4.037, 3.593, 3.371),  // 22
    Array(4.057, 3.616, 3.394),  // 23
    Array(4.077, 3.637, 3.417),  // 24
    Array(4.096, 3.658, 3.439),  // 25
    Array(4.114, 3.678, 3.459),  // 26
    Array(4.132, 3.696, 3.479),  // 27
    Array(4.148, 3.714, 3.498),  // 28
    Array (4.164, 3.732, 3.516),  // 29
    Array (4.179, 3.749, 3.533),  // 30
    Array(4.194, 3.765, 3.550),  // 31
    Array (4.208, 3.780, 3.567),  // 32
    Array(4.222, 3.795, 3.582),  // 33
    Array(4.236, 3.810, 3.597),  // 34
    Array(4.249, 3.824, 3.612),  // 35
    Array (4.261, 3.837, 3.626),  // 36
    Array (4.273, 3.850, 3.640),  // 37
    Array(4.285, 3.863, 3.653),  // 38
    Array(4.296, 3.876, 3.666),  // 39
    Array(4.307, 3.888, 3.679),  // 40
    Array(4.318, 3.899, 3.691),  // 41
    Array(4.329, 3.911, 3.703),  // 42
    Array (4.339, 3.922, 3.714),  // 43
    Array(4.349, 3.933, 3.726),  // 44
    Array(4.359, 3.943, 3.737),  // 45
    Array(4.368, 3.954, 3.747),  // 46
    Array(4.378, 3.964, 3.758),  // 47
    Array(4.387, 3.973, 3.768),  // 48
    Array(4.395, 3.983, 3.778),  // 49
    Array(4.404, 3.992, 3.788),  // 50
  )






  def rankdata(data:Array[Double]): Array[Int] ={

    //Sort by index
    var sorter:Array[Int]=data.zipWithIndex.sortBy(_._1).map(_._2)

    //Sort by rank 1 to N
    var inv = new Array[Int](sorter.length)
    var i=0
    while(i<sorter.length){
      inv(sorter(i))=i+1
      i+=1
    }


    /*println("n=")
    println(data.toVector)
    println("a=")
    println(sorter.toVector)
    println("i")
    println(inv.toVector)
    println("")*/

    inv
  }


  //Compute order by along one colunms
  def computeOrder(M: Array[Array[Double]]): Array[Array[Int]] ={

    var Mt=M.transpose

    //Rank each line
    var rankeddata= new Array[Array[Int]](Mt.length)
    var i=0
    while(i<Mt.length){
      rankeddata(i) = rankdata(Mt(i))
      i+=1
    }

    rankeddata = rankeddata.transpose


    // Change to weight ( the first as more weigth )
    /*var sz = rankeddata(0).length
    var j=0
    i=0
    while( i < rankeddata.length ){
      j=0
      while( j < sz ){
        rankeddata(i)(j) = Math.abs( rankeddata(i)(j) - sz ) + 1
        j+=1
      }
      i+=1
    }*/


    /*
    var m= Array[Double](rankeddata.length)
    var k=0
    while(k<rankeddata.length){
      var a=0
      m(k)=0
      while(a<sz){
        m(k)+=rankeddata(k)(a)
      }
      m(k)/=sz
      k+=1
    }
    ordered*/

    println(rankeddata.length)

    for( h <- 0 to rankeddata.length-1 ){
      println( rankeddata(h).toVector )
    }



    rankeddata

  }



  def computeCD(nrow:Int,ncol:Int, pvalue:Double): Double ={

    //Critical distance
    var col_idx = pvalue match{
      case 0.01 => 0
      case 0.05 => 1
      case 0.10 => 2
    }

    val row_idx = ncol - 2
    var  cv:Double= CRITICAL_VALUES(row_idx)(col_idx)
    var  cd = cv*Math.sqrt( (ncol * (ncol + 1)) / (6.0 * nrow))*0.4

    cd
  }


  def Plot(order:Array[Array[Int]],Nms:Array[String],CD:Double): JPlot={

    val fig = new JPlot("")
    fig.grid("off", "off") // grid on;
    fig.font("Arial", 12)
    fig.holdon()


    var orderMean =  Array.fill[Double](order.length)(0)
    var i=0 ; var j=0
    while( i<order.length ){
      j=0
      while( j < order(0).length ){
        orderMean(i)+=order(i)(j)
        j+=1
      }
      orderMean(i)/=order(0).length
      i+=1
    }

    orderMean=Array(1.0) ++ orderMean.sorted ++ Array(orderMean.length.toDouble)
    var y = Array.fill[Double](orderMean.length)(0)

    println(orderMean.toSeq)
    println(y.toSeq)

    var brdr=1                    // X Margins from the main line
    val a=0.2                     // X Margins of the labels from the main line
    val maxRank=orderMean.length  // Rank maximo
    val ymin= 0.4                 //

    fig.xlim(1-brdr,maxRank+brdr-2)
    fig.ylim(-1, 1)
    fig.plot(orderMean,y, "Rk", 5.0f, "AAPL")  // Main Line
    fig.setX_visible(false); fig.setY_visible(false) // Hide xx and yy axis

    // Values
    val v:Array[Double]= new Array[Double](maxRank-2)
    i=1
    while( i<=v.length ){ v(i-1)=i ; i+=1 }
    val yv= Array.fill[Double](maxRank-2)(0)
    fig.plot(v,yv,"Lk", 2.0f, "AAPL")


    // Annotation
    i=0
    while( i<v.length ){
      fig.text(v(i).toString,v(i),0.06, 14, "Arial")
      i+=1
    }

    val nr = Math.round(Nms.length/2)
    val nl = Nms.length-nr
    val re = 1/Nms.length.toDouble  // labels separation

    // Left labels
    i=0
    while( i<nr ) {
      fig.text(Nms(i),1-a-0.2,-(i*re+ymin), 14, "Arial")                                                     // Labels
      fig.plot( Array(orderMean(i+1),orderMean(i+1)),Array(0,-(i*re+ymin)),"k", 2.0f,"" )      // Vertical lines
      fig.plot( Array(1-a,orderMean(i+1))  ,  Array(-(i*re+ymin),-(i*re+ymin))  ,"k", 2.0f,"" )// Horizantal lines
      i+=1
    }


    // Right labels
    i=nr
    while( i<Nms.length ) {
      fig.text(Nms(i),maxRank+a-2+0.2,-((i-nr)*re+ymin), 14, "Arial")
      fig.plot( Array(orderMean(i+1),orderMean(i+1)),Array(0,-((i-nr)*re+ymin)),"k", 2.0f,"" )
      fig.plot( Array(maxRank+a-2,orderMean(i+1)), Array(-((i-nr)*re+ymin),-((i-nr)*re+ymin))  ,"k", 2.0f,"" )
      i+=1
    }

    // Group bars
    var g=0.0
    var level= -0.20
    var token = orderMean(1)
    var lastvalue = orderMean(1)

    for (n <- 2 to Nms.length) {
      if ( math.abs( orderMean(n) - orderMean(n-1) ) < CD ) {
        fig.plot( Array(orderMean(n), orderMean(n-1)), Array(level + g, level + g), "-k", 5.0f, "")
      }
      else
        g += -0.04
    }

    // CD bar
    var ypos=.5
    fig.plot(Array(1,1+CD), Array(ypos,ypos),  "-k", 3.0f, "")
    fig.plot(Array(1,1), Array(ypos-0.03,ypos+0.03), "-k", 3.0f, "")
    fig.plot(Array(1+CD,1+CD), Array(ypos-0.03,ypos+0.03), "-k", 3.0f, "")
    fig.text("CD = "+"%.2f".format(CD).toString.replace(",","."),1+CD/2, ypos+0.1,14,"Arial")

    fig

  }

  // MAIN FUNCTION
  //=======================================================================================
  def main(args: Array[String]): Unit = {

    var TestMtx = Array(
      Array(1.2, 2.1 , 3.4, 1.8, 1.2 ),
      Array(5.2, 3.1 , 1.4, 4.7, 7.1 ),
      Array(7.2, 9.1 , 1.5, 2.1, 2.1 ),
      Array(8.2, 2.1 , 6.4, 3.2, 3.9 ),
      Array(3.2, 2.1 , 3.2, 5.6, 3.4 ),
    )

    var Nms = Array( "Alg1","Alg2","Alg3","Alg4","Alg5")


    //Lines  - methods
    //Columns- experimentations
    var CD = computeCD(TestMtx.length,TestMtx(0).length,0.05 )
    //println(CD)

    var order= computeOrder(TestMtx)

    var fig=Plot(order,Nms,CD)
    fig.saveas("C:/Users/RSousa1/Desktop/test.png", 1000, 500)

  }

}








