/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.utils

import org.hipparchus.random.{RandomDataGenerator, RandomGenerator}

import scala.collection.immutable.TreeMap

/**
  * This class generates a discrete sequence of elements with a given distribution. One either
  * provides a map from a weight to given element. The weights can be for example the percentage
  * of occurrences (distribution) of the elements.
  *
  * @param random - random generator that is sued to select the weight
  * @param mp - the map that holds the accumulated weight assigned to an element
  * @param total - sum of all the weights (used to calculate the accumulated weights)
  * @tparam T - the type of the elements that will be generated
  */
case class RandomCollection[T] private (random : RandomGenerator, mp : TreeMap[Double, T], total : Double) {

  def add(weight: Double, cl: T): RandomCollection[T] = {
    val tmp_total = total + weight
    val tmp_mp = mp + (tmp_total -> cl)
    RandomCollection(random, tmp_mp, tmp_total)
  }

  def proportions: TreeMap[Double, T] = mp

  def next : T = {
    val value = random.nextDouble * total
    val lower = mp.from(value)
    lower.head._2
  }

}

object RandomCollection {

  /**
    * If we provide a map of weights for the desired elements, we need to calculate the
    * accumulated weight for each symbol. It is equivalent to adding each element's weight
    * individually as in [[pt.inescn.utils.JRandomCollection.add]]
    *
    * @param nmp - initializer of the weights for each element we ant to generate
    * @tparam T - type of element to generate
    * @return
    */
  private def addWeights[T](nmp : Map[T,Double]): (Double, TreeMap[Double, T]) = {
    nmp.foldLeft( (0.0, TreeMap[Double, T]()) ) { case (acc, (classi,k)) =>
      val total: Double = acc._1 + k
      val nmp1: TreeMap[Double, T] = acc._2 + (total -> classi)
      (total, nmp1)
    }
  }

  def apply[T](mp : Map[T,Double] = Map[T,Double](), random : RandomGenerator = new RandomDataGenerator())
  : RandomCollection[T] = {
    val (total, nmp) = addWeights(mp)
    RandomCollection[T](random, nmp, total)
  }

}