/*******************************************************************************
 * Copyright (C) 2017 INESC-TEC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
 package pt.inescn.utils

import java.io.{PrintWriter, StringWriter}
import java.lang.{System => JSystem}
import java.nio.file.Paths
import java.util.UUID

import better.files.File

object Utils {

  /**
    * This function generates a unique file name.
    *
    * @param tmp - directory wherein the file is created.
    * @return
    */
  def temporaryFilename(tmp: File): String = {
    val uuid = this.synchronized(UUID.randomUUID.toString)
    Paths.get(tmp.toString, uuid).toString
  }

  /**
    * Used for timing a single call.
    *
    * @param block - code to execute
    * @tparam R - output of executed code
    * @return - Returns the output of the code `block` nanoseconds
    *         of time it took to execute the `block`.
    */
  def time[R](block: => R): (R, Long) = {
    val t0 = JSystem.nanoTime()
    val result = block // call-by-name
    val t1 = JSystem.nanoTime()
    //println( s"Elapsed time t1: $t1 ns" )
    //println( s"Elapsed time t2: $t1 ns" )
    //println( "Elapsed time: " + ( t1 - t0 ) + "ns" )
    //println("Elapsed time: " + (t1 - t0) / 1e9 + "sec")
    (result, t1-t0)
  }

  import scala.concurrent.duration._

  /**
    * Used for timing a single call
    *
    * @param block - code to execute
    * @tparam R - output of executed code
    * @return - Returns the output of the code `block` nanoseconds
    *         of time it took to execute the `block`.
    */
  def timeOf[R](block: => R): (R, FiniteDuration) = {
    val (result,elapsed) = time(block)
    val elapsed_sec = elapsed.nanoseconds
    (result, elapsed_sec)
  }

  /**
    * Breaks down the [[scala.concurrent.duration.FiniteDuration]] into
    * days, hours, minutes, seconds, milliseconds and nanoseconds.
    *
    * TODO: the JDK API will change in JDK 9. Use the toXXXPart
    * functions. This is because their is no toSeconds method.
    * In addition it makes it clear what the values really
    * are.
    *
    * @see https://bugs.openjdk.java.net/browse/JDK-8142936
    * @see http://hg.openjdk.java.net/jdk9/jdk9/jdk/rev/7f644a5d554a
    * @param elapsed
    * @return
    */
  def decompose(elapsed:FiniteDuration): (Long, Long, Long, Long, Long, Int) = {
    val SECONDS_PER_MINUTE: Int = 60 // java.time.LocalTime.SECONDS_PER_MINUTE
    val MINUTES_PER_HOUR: Int = 60 // java.time.LocalTime.MINUTES_PER_HOUR

    val duration = java.time.Duration.ofNanos(elapsed.toNanos)
    val days = duration.toDays // toDaysPart()
    val hours = duration.toHours % 24 // toHoursPart()
    val minutes = duration.toMinutes % MINUTES_PER_HOUR // toMinutesPart()
    val seconds = duration.getSeconds % SECONDS_PER_MINUTE // toSecondsPart()
    val millis = duration.getNano / 1000000L // toMillisPart(){
    val nanos = duration.getNano % 1000000 // toNanosPart()
    (days, hours, minutes, seconds, millis, nanos)
  }

  def decomposeToString(elapsed:FiniteDuration): String = {
    val (days, hours, minutes, seconds, millis, nanos) = decompose(elapsed)
    s"$days days + $hours hours + $minutes minutes + $seconds seconds + $millis millis + $nanos nanos"
  }

  import java.time.ZoneId

  val zoneID_UTC : ZoneId = java.time.ZoneId.of( "UTC" )

  import java.util.Date

  /**
   * Utility function to generate a `java.util.Date` used prior to JDK 8.
   * For example: json4s.orf still uses the `java.util.Date` date.
   */
  def makeData( year: Short, month: Short, date: Short, hrs: Short, min: Short, sec: Short = 0, milli: Short = 0 ) : Date = {
    import java.util.Calendar
    import java.util.TimeZone

    val cal = Calendar.getInstance( TimeZone.getTimeZone( "UTC" ) )
    cal.clear()
    cal.set( Calendar.YEAR, year )
    cal.set( Calendar.MONTH, month - 1 )
    cal.set( Calendar.DATE, date )
    cal.set( Calendar.HOUR, hrs )
    cal.set( Calendar.MINUTE, min )
    cal.set( Calendar.SECOND, sec )
    cal.set( Calendar.MILLISECOND, milli )

    cal.getTime
  }

  import java.time.Instant

  def makeInstance( year: Short, month: Short, day: Short, hrs: Short = 0, min: Short = 0, sec: Short = 0, nano: Int = 0 ) : Instant = {
    val r1 = java.time.LocalDateTime.of( year, month, day, hrs, min, sec, nano )
    // parsedDate.atStartOfDay(off).toInstant() // for java.time.Local
    r1.atZone( zoneID_UTC ).toInstant
  }

  def parseInstantUTC(date: String): Instant = {
    import kantan.csv._
    import kantan.csv.ops._
    //import kantan.csv.java8._
    // Import the NAB data file parser implicitly
    import pt.inescn.utils.NABUtils._
    val instant = date.unsafeReadCsv[List, Instant](rfc)
    instant.head
  }

  def printStackTrace(e:Throwable): String = {
    val sw = new StringWriter
    e.printStackTrace(new PrintWriter(sw))
    sw.toString
  }

}

