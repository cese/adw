package pt.inescn.scratchpad

/**
  * root/runMain pt.inescn.scratchpad.AggregateFunc
  */
object AggregateFunc {

  case class Exec()

  case class Task[I,O]()

  sealed trait Op[I,O]

  final case class NoOp[I,O](s: String) extends Op[I,O]
  final case class Func[I,O](t: Task[I,O]) extends Op[I,O]
  //final case class AggregateLin[I,T,O](f: Op[I,T], t: Task[(Exec,T),O]) extends Op[I,O]

  /*
  // Solution 1 with
  final case class AggregateLin[I,T,O](f: Op[I,T], t: Task[(Exec,T),O]) extends Op[I,O]
  implicit class OpOps[I,T,O](o: Op[(Exec,T),O]) {
    def |:(that: Op[I,T]): Op[I,O] = (that, o) match {
      case (op:Op[I,T], Func(t)) => AggregateLin[I,T,O](op, t)
      case _ => NoOp("")
    }
   }*/

  /*
  // Fails with
  // final case class AggregateLin[I,T,O](f: Op[I,T], t: Task[(Exec,T),O]) extends Op[I,O]
  implicit class OpOps[I,T,O](o: Op[T,O]) {
    def |:[E](that: Op[I, E]): Op[I, O] = (that, o) match {
      case (op: Op[I, E], Func(t)) => AggregateLin[I, E, O](op, t)
      case _ => NoOp("")
    }
  }*/

  import scala.language.higherKinds

  /*
  //final case class AggregateLin[I,E,T,O](f: Op[I,E], t: Task[(Exec,T),O])(implicit ev: E =:= Tuple2[Exec,T]) extends Op[I,O]
  //final case class AggregateLin[I,F[_,_],E,T,O](f: Op[I,E], t: Task[F[Exec,T],O]) extends Op[I,O]
  implicit class OpOps[I,E,T,O](o: Op[T,O]) {
    def |:(that: Op[I, E]): Op[I, O] = (that, o) match {
      case (op: Op[I, E], Func(t)) => AggregateLin(op, t)
      case _ => NoOp("")
    }
  }*/


  // Solution 2 with
  final case class AggregateLin[I,F[_,_],T,O](f: Op[I,T], t: Task[F[Exec,T],O]) extends Op[I,O]
  implicit class OpOps[I,F[_,_],T,O](o: Op[F[Exec,T],O]) {
    def |:(that: Op[I,T]): Op[I,O] = (that, o) match {
      case (op:Op[I,T], Func(t)) => AggregateLin[I,F,T,O](op, t)
      case _ => NoOp("")
    }
   }

  //import scala.language.existentials
  // Not symmetric
  //implicitly[(_,Int) =:= (_,Int)]  //  Cannot prove that (Any, Int) =:= Tuple2[_, Int]
  //implicitly[(_,Int) =:= Tuple2[_,Int]] // Cannot prove that (Any, Int) =:= Tuple2[_, Int]
  implicitly[Tuple2[_,Int] =:= (_,Int)]  // Ok
  //implicitly[Tuple2[_,Int] =:= Tuple2[_,Int]]  // Ok

  def main(args: Array[String]): Unit = {
    val tsk1 = Func(Task[String,Double]())
    //val exec1 = Exec()
    val tsk2 = Func(Task[(Exec,Double),Double]())
    tsk1 |: tsk2
  }
}
