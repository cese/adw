package pt.inescn.scratchpad

/**
  * runMain pt.inescn.scratchpad.OpiTest2
  */
object OpiTest2 {


  trait Monoid[T] {
    def unit: T
    def op(a:T, b:T) : T
  }
  object Monoids {
    implicit object stringMonoid extends Monoid[String] {
      def op(x: String, y: String): String = if (x=="") y
      else if (y == "")
        x
      else
        x + "," + y
      def unit: String = ""
    }
    implicit object intMonoid extends Monoid[Int] {
      def op(x: Int, y: Int): Int = x + y
      def unit: Int = 0
    }
  }

  sealed trait Op[A,P,I,IO,P2,O]
  case class NoOp[A,P1,I,O1,P2,O2](s:String) extends Op[A,P1,I,O1,P2,String]
  case class Func[A,P,I,IO,P2,O](p:P, f:(P,I) => (A,O)) extends Op[A,P,I,IO,P2,O]
  case class All[A,P,I,IO,P2,O](f:List[Func[A,P,I,_,_,O]]) extends Op[A,P,I,IO,P2,O]
  case class SeqOp[A,P,I,O,P2,O2](f1:List[Op[A,P,I,_,_,O]], f2:List[Op[A,P2,O,_,_,O2]]) extends Op[A,P,I,O,P2,O2]
  case class CrossOp[A,P,I,O,P2,O2](f1:List[Op[A,P,I,_,_,O]], f2:List[Op[A,P2,O,_,_,O2]]) extends Op[A,P,I,O,P2,O2]
  case class Iff[A,P,I,IO,P2,O](iff:(A,I,Option[P]) => Boolean, f:Op[A,P,I,_,_,O], zero:O) extends Op[A,P,I,IO,P2,O]

  sealed case class Exec[A,P,I,O](fs:Iterable[(A,I) => (A,O)])

  //def compilex[A,P1,I,O1,P2,O2](o:Op[A,P1,I,O1,P2,O2])(implicit m: Monoid[A]):Exec[A,P1,I,O1,P2,O2] = o match {
  def compilex[A,P1,I,O1,P2,O2](o:Op[A,P1,I,O1,_,O2])(implicit m: Monoid[A]):Exec[A,P1,I,O2] = o match {
    case e:NoOp[_,_,_,_,_,_] =>
      def func(acc0:A,i:I) = {
        (acc0, e.s)
      }
      Exec( List(func _) )
    case Func(p,f) =>
     def func(acc0:A,i:I) = {
        val (acc1,o1) = f(p,i)
        (m.op(acc0,acc1), o1)
      }
      Exec( List(func _) )
    case Iff(iff,Func(p,f),z) =>
      def func(acc0:A,i:I) = {
        if (iff(acc0, i, Some(p))) {
          val (acc1, o1) = f(p,i)
          (m.op(acc0, acc1), o1)
        } else
          (acc0, z)
      }
      Exec( List(func _) )
    case Iff(iff,f,z) =>
      val tmp0 = compilex(f)
      val tmp: Iterable[(A,I)=>(A,O2)] = tmp0.fs.map { fi =>
        def func(acc0:A,i:I) = {
          if (iff(acc0,i,None)) {
            val (acc1,o1) = fi(acc0,i)
            (m.op(acc0,acc1), o1)
          } else
            (acc0,z)
        }
        func _
      }
      Exec( tmp )
    case All(fs) =>
      val tmp: Iterable[(A,I)=>(A,O2)] = fs.view.map { fi =>
        def func(acc0:A,i:I) = {
          val (acc1,o1) = fi.f(fi.p,i)
          (m.op(acc0,acc1), o1)
        }
        func _
      }
      Exec( tmp )
    case SeqOp(l1,l2) =>
      val tmp0 = l1.view.zip(l2.view).flatMap { case (f1s, f2s) =>
        val tmp1 = compilex(f1s)
        val tmp2 = compilex(f2s)
        val tmp3 = tmp1.fs.zip(tmp2.fs).map { case (f1,f2) =>
          def func(acc0:A,i:I) = {
            val (acc1,o1) = f1(acc0,i)
            val (acc2,o2) = f2(acc1,o1)
            (m.op(acc0, m.op(acc1,acc2)), o2)
          }
          func _
        }
        tmp3
      }
      Exec( tmp0 )
    case CrossOp(l1,l2) =>
      val tmp0 = l1.view.flatMap { f1s =>
        l2.view.flatMap{ f2s =>
          val tmp1 = compilex(f1s)
          val tmp2 = compilex(f2s)
          val tmp3 = tmp1.fs.flatMap { f1 =>
            tmp2.fs.map { f2 =>
              def func(acc0:A,i:I) = {
                val (acc1,o1) = f1(acc0,i)
                val (acc2,o2) = f2(acc1,o1)
                (m.op(acc0, m.op(acc1,acc2)), o2)
              }
              func _
            }
          }
          tmp3
        }
      }
      Exec( tmp0 )
  }

  def f[A,P,I,IO,P2,O](p:P, f:(P,I) => (A,O)) = Func(p,f)

  def exec[A,P1,I,O1,P2,O2](i: Seq[I], acc:A, f:Exec[A,P1,I,O2]): Seq[(A, O2)] = {
    i.flatMap { i =>
      f.fs.map( f => f(acc,i) )
    }
  }

  def main(args: Array[String]): Unit = {
    import Monoids._

    def func0(a:String,b:Int): (String,Int) = (a+"A2",b+1)

    val f0a: Func[String, String, Int, Nothing, Nothing, Int] = Func("X", (a:String, b:Int) => (a+"A2",b+1))
    val f0ac = compilex(f0a)
    println(f0ac)

    val in1 = List(1,2,3)
    val r1 = in1.flatMap { i =>
      f0ac.fs.map( f => f("",i) )
    }
    println(r1)


    val f0: Func[String, String, Int, Nothing, Nothing, Int] = f("X",func0)
    val f0c = compilex(f0)
    println( exec(in1, "", f0c) )
  }
}
