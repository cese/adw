package pt.inescn.scratchpad

import scala.reflect.runtime.universe._

/**
  * This is the exploration and testing of an means to pack and unpack values
  * to and from a wrapper `Val` automatically. It is important to note that
  * we assume that type information will not always be fully available. So
  * the information of the wrappers's inner type may be unknown. Unpacking
  * such a value may result in a run-time failure.
  *
  * @see https://stackoverflow.com/questions/1154571/scala-abstract-types-vs-generics
  *      https://typelevel.org/blog/2015/07/13/type-members-parameters.html
  *      https://stackoverflow.com/questions/26053319/how-to-capture-t-from-typetagt-or-any-other-generic-in-scala
  *      https://stackoverflow.com/questions/19386964/i-want-to-get-the-type-of-a-variable-at-runtime
  *
  * root/ runMain pt.inescn.scratchpad.AutoVal
  */
object AutoVal {

  import scala.language.implicitConversions


  /**
    * The wrapper class. We use it store values of different types in a
    * container for example.
    *
    * @tparam V type of the inner (wrapped) value.
    */
  sealed trait Val[V] {
    val v: V
  }

  /**
    * Pack a value automatically.
    *
    * @param s value to be wrapped
    * @tparam A type of the wrapped value
    * @return returns a `Vaĺ` that wraps the value
    */
  implicit def pack[A](s:A) : Val[A] = new Val[A] {
    println(s"Packing $s")
    override val v: A = s
  }

  /**
    * Used for debugging type information. See what type
    * information the compiler has access to and we can use.
    * Can only deal with simple types.
    *
    * @param t value of a given type
    * @param tag use reflection not to loose type information
    * @tparam A type of the value
    * @return string with type information
    */
  implicit def detectSimple[A](t:A)(implicit tag: TypeTag[A]): String = tag.tpe match {
    case TypeRef(utype, usymbol, args) =>
      s"Simple: ($utype, $usymbol, $args) = $t [${t.getClass}]"
  }

  import scala.language.higherKinds

  /**
    * Used for debugging type information. See what type
    * information the compiler has access to and we can use.
    *
    * @param t value of a given type
    * @param tagVal use reflection not to loose type information
    * @tparam F type of container
    * @tparam B inner type (wrapped value)
    * @return string with type information
    */
  implicit def detect[F[_] <: Val[_],B](t:F[B])(implicit tagVal: TypeTag[F[B]]): String = tagVal.tpe match {
    case TypeRef(utype, usymbol, args) =>
      val v = t.v.asInstanceOf[B]
      s"Higher: ($utype, $usymbol, $args) = $v [${v.getClass}]"
  }


  // https://gist.github.com/retronym/228673


  sealed trait LowPriority {
    // TODO: can we prioritize selection of full or not
    // TODO: we don't need an implicit parameter, just instantiate
    implicit def testFull[A,B](av: Val[A], f: A => B)(implicit a: Val[A] => Either[String,A]): Either[String,B] = {
      println(s"TEST FULL $av")
      a(av).flatMap( e => Right(f(e)) )
    }
  }

  object HighPriority extends LowPriority {
    /**
      * Example of how to automatically find the implicit conversion. Here we
      * assume that the `Val[_]` does not have its inner type visible. For
      * example, we got it from a container of type `Map[String, Val[_] ]`.
      * Note that is the types are incompatible, we will get a run-time error.
      *
      * @param av wrapper value we want to unpack
      * @param f the conversion function that actually sets the type for
      *          unpacking
      * @param a evidence that we have a function that can unpack the wrapped
      *          value
      * @tparam A type to which we want o unwrap the value
      * @tparam B type to which we want to convert the unwrapped value. Can
      *           be the same as `A` (just unpack)
      * @return hte value of type `B`
      */
    implicit def test[A,B,X](av: Val[X], f: A => B)(implicit a: Val[X] => Either[String,A]): Either[String,B] = {
      println(s"TEST $av")
      a(av).flatMap( e => Right(f(e)) )
    }
  }

  def choose[A,B,X](av: Val[X], f: A => B)(implicit a: (Val[X], A=>B) => Either[String,B]): Either[String,B] = {
     a(av, f)
  }

  /**
    * This function automatically unpacks a value and tries to convert it to
    * a given type. May fail with a [[java.lang.ClassCastException]]
    * exception.
    *
    * @param t wrapper whose value is to be unpacked
    * @tparam T convert the unwrapped value to this type
    * @return either an error message or the unpacked value with the
    *         correct type
    */
  implicit def unpack[T](t:Val[_]): Either[String,T] = {
    println(s"UNPACK --------------------------- t = ${t.v}")
    try {
      val v = t.v.asInstanceOf[T]
      Right(v)
    } catch {
      // When used via implicits should never reach this
      case e:Exception => Left(e.getMessage)
    }
  }

  case class ErrVal(override val v:String) extends Val[String]

  case class Row(r:Map[String,Val[_]]) {

    def apply[A,B](f: A => B, cols:String*)(implicit a: Val[_] => Either[String,A], b: B => Val[B]): Row = {
      val cls = cols.toSet
      val t = r.filterKeys( cls.contains ).map { case (k,v) =>
        // Get the row element
        val va = a(v)
        println(s"VA ================ $va")
        val e = va.fold( { msg =>
          // If it is not the correct type, error
          ErrVal(msg)
        }, { oa =>
          // If it is the correct type, use it
          val vb = f(oa)
          val avb = b(vb)
          avb
        })
        (k, e)
      }
      // Replace what we changed
      Row(r ++ t)
    }

  }

  /*
  sealed trait LowPriority {
    /**
      * This function automatically unpacks a value and tries to convert it to
      * a given type. May fail with a [[java.lang.ClassCastException]]
      * exception.
      *
      * @param t wrapper whose value is to be unpacked
      * @tparam T convert the unwrapped value to this type
      * @return either an error message or the unpacked value with the
      *         correct type
      */
    implicit def unpack[T](t:Val[T]): Either[String,T] = {
      println(s"UNPACK --------------------------- t = ${t.v}")
      try {
        val v = t.v.asInstanceOf[T]
        Right(v)
      } catch {
        // When used via implicits should never reach this
        case e:Exception => Left(e.getMessage)
      }
    }
  }

  object HighPriority extends LowPriority {
    /**
      * This function automatically unpacks a value and tries to convert it to
      * a given type. May fail with a [[java.lang.ClassCastException]]
      * exception.
      *
      * @param t wrapper whose value is to be unpacked
      * @tparam T convert the unwrapped value to this type
      * @return either an error message or the unpacked value with the
      *         correct type
      */
    implicit def unpackFull[T,X](t:Val[X]): Either[String,T] = {
      println(s"UNPACK FULL --------------------------- t = ${t.v}")
      try {
        val v = t.v.asInstanceOf[T]
        Right(v)
      } catch {
        // When used via implicits should never reach this
        case e:Exception => Left(e.getMessage)
      }
    }
  }*/

  /**
    * Demonstration/test code. Check that the `Val` whose inner type is not
    * accessible to the compiler, can still be cast during run-time to a
    * desired type. May fail during run-time.
    *
    * @param args not used
    */
  def main(args: Array[String]): Unit = {

    //import LowPriority._
    import HighPriority._

    // Ok 1
    val r5: Val[String] = "100"
    println(s"r5: Val(${r5.v}) : ${r5.v.getClass}")
    val r6: Val[Double] = 200.0
    println(s"r6: Val(${r6.v}) : ${r6.v.getClass}")
    val r7: Val[Array[Double]] = Array(300.0, 301, 302)
    println(s"r7: Val(${r7.v.mkString(",")}) : ${r7.v.getClass}")
    // Just testing the function
    println(detectSimple(100))
    // Comparing results for different function signatures
    // Here we have all the type information
    // Notice that when we use higher-order types we have access ti the packed type
    println(s"simple(r5) = ${detectSimple(r5)}")
    println(s"detect(r5) = ${detect(r5)}")
    println(s"detect(r6) = ${detect(r6)}")
    println(s"detect(r7) = ${detect(r7)}")

    // Now we do not have access to the contained data
    val rs = Map("x" -> r5, "y" -> r6, "z" -> r7)
    println(s"rs = $rs")
    val s5 = rs("x")
    val s6 = rs("y")
    val s7 = rs("z")
    // Here we notice that the type B is in fact unbound
    // The args indicate an unbound type
    println("Detecting")
    println(s"detect(s5) = ${detect(s5)}")
    println(s"detect(s6) = ${detect(s6)}")
    println(s"detect(s7) = ${detect(s7)}")

    // Unpack does the same thing
    println("Unpacking")
    println(s"unpack(s5) = ${unpack(s5)}")
    println(s"unpack(s6) = ${unpack(s6)}")
    println(s"unpack(s7) = ${unpack(s7)}")

    // We can use unpacking if the packed type is know at compile time
    // If we apply a incorrect type in the anonymous function, compilation will fail
    println("Implicit unpacking: well typed Val")
    val u0 = test(r5, (e:String) => e.toInt)
    val u1 = test(r6, (e:Double) => e)
    val u2 = test(r7, (e:Array[Double]) => e)
    println(u0)
    println(u1)
    println(u2)
    /*
    // This will fail during run-time
    val u3 = test(r6, (e:String) => e.toInt)
    println(u3)
    */
    /*
    // This will fail during compile-time
    val u3 = testFull(r6, (e:String) => e.toInt)
    println(u3)
    */

    val c0 = choose(r5, (e:String) => e.toInt)
    println(c0)
    /*
    // Fails during compile-time (selects low priority)
    val c1 = choose(r6, (e:String) => e.toInt)
    println(c1)
    */

    // We can try use unpacking if the packed type is unknown at compile time
    // The type `A` of the anonymous function does not bind to the unpack's `T`
    // Note that the Val's inner type must not be used because it is in effect unknown
    println("Implicit unpacking: untyped Val")
    val v0 = test(s5, (e:String) => e.toInt)
    val v1 = test(s6, (e:Double) => e)
    val v2 = test(s7, (e:Array[Double]) => e)
    println(v0)
    println(v1)
    println(v2)

    /*
    // Should compile but fails.
    // Fails during compile-time
    val c2 = choose(s6, (e:String) => e.toInt)(testFull _)
    //val c2 = choose(s6, (e:String) => e.toInt)(test _)
    println(c2)
    */
  }


}
