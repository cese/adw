package pt.inescn.scratchpad

import pt.inescn.etl.Transform._
import better.files.Dsl._
import pt.inescn.etl.LightSVM.{SparseFold, ValidateDepVar}
import pt.inescn.models.Data.SparseMatrixData
import pt.inescn.utils.ADWError

// We need to bring in shapeless "compile time reflection"
import kantan.csv.generic._
import pt.inescn.etl.Load._




object Fft {

  //  def save(csvWriter: CSVWriter, filePath: String, data1: Array[Double], data2: Array[Double], data3: Array[Double]): Unit ={
  //    var listOfRecords = new ListBuffer[Array[String]]()
  //    var i = 0
  //    while(i<data1.length){
  //      listOfRecords += Array(data1(i).toString, data2(i).toString, data3(i).toString)
  //      i = i+1
  //    }
  //
  //    import scala.collection.JavaConverters._
  //    csvWriter.writeAll(listOfRecords.toList.asJava)
  //  }
  //
  //  def saveLine(pathName: String, text: String): Unit ={
  //    import java.io._
  //
  //    val fileName = cwd + pathName
  //    val w = new BufferedWriter(new FileWriter(fileName, true)) //true: append
  //    w.write(text+"\n")
  //    w.close()
  //  }
  //
  //  def fftAbs(fileName: String, fft: FastFourierTransformer, data : Array[Double]) : Array[Double] = {
  //    val len = data.length
  //    println(len + " -> " + truncatePowerOf2(len))
  //    val dataToUse = data.take(truncatePowerOf2(len))
  //    val result = fft.transform(dataToUse, TransformType.FORWARD).map(_.abs())
  //    saveLine(fileName, result mkString ", ")
  //    result
  //  }
  //
  //  def fftWindow(fileName: String, fft: FastFourierTransformer, windowSize: Int, stepSize: Int, data : Array[Double]) : ListBuffer[Array[Double]] = {
  //    val windows = data.sliding(windowSize, stepSize).toList
  //    val result = new ListBuffer[Array[Double]]()
  //    for(window <- windows) {
  //      result += fftAbs(fileName,fft,window)
  //    }
  //    result
  //  }
  //
  //  def createFFT(dataName: String, windowSize: Int, stepSize: Int): Unit ={
  //    import better.files.Cmds.cwd
  //    import pt.inescn.etl.Load.TimeSeriesFrame3
  //    import org.hipparchus.transform.{DftNormalization, FastFourierTransformer}
  //
  //    val data = cwd / "/data/inegi/ensaios_rolamento1/ensaio1_rol1_b+f+fft_pp.csv" //dataName)
  //    val rr = TimeSeriesFrame3().load(data)
  //    val d = rr.getOrElse(TimeSeriesFrame3())
  //    val fft = new FastFourierTransformer(DftNormalization.STANDARD)
  //
  //    fftWindow("/data/inegi/fft_x.csv",fft,windowSize,stepSize,d.value1.toArray)
  ////    val result2 = fftWindow("/data/inegi/fft_y.csv",,fft,windowSize,stepSize,d.value2.toArray)
  ////    val result3 = fftWindow("/data/inegi/fft_z.csv",,fft,windowSize,stepSize,d.value3.toArray)
  //  }
  //
  //  def denseToSparse(mat0: Array[Array[Double]]): Array[(Double, Map[Int, Double])] ={
  //    var mat = new ArrayBuffer[(Double, Map[Int, Double])]
  //    for(r_idx <- mat0.indices){
  //      var ms = Map.empty[Int,Double]
  //      for(c_idx <- mat0(r_idx).indices){
  //        ms += (c_idx -> mat0(r_idx)(c_idx))
  //      }
  //      mat = mat ++ ArrayBuffer((r_idx.toDouble, ms))
  //    }
  //    mat.toArray
  //  }
  //
  //  def saveSparse(mat: Array[(Double, Map[Int, Double])], pathFile: String): Unit ={
  //    for(row <- mat){
  //      var write = "" + row._1
  //      for(value <- row._2){
  //        write += " " + value._1 + ":" + value._2
  //      }
  //      saveLine(pathFile,write)
  //    }
  //  }
  //
//  def using[A <: { def close(): Unit }, B](resource: A)(f: A => B): B =
//  try {
//    f(resource)
//  } finally {
//    resource.close()
//  }
//
//  def readDense(filePath: String): Array[Array[Double]] ={
//    val rows = ArrayBuffer[Array[Double]]()
//    var size = 0
//    using(io.Source.fromFile(cwd+filePath)) { source =>
//      for (line <- source.getLines) {
//        var this_row = line.split(",").map(_.trim).map(x => x.toDouble)
//        if(size == 0){
//          size = this_row.length
//        } else {
//          if(this_row.length < size){
//            this_row = this_row ++ Array.fill(size-this_row.length)(0.0)
//          }
//        }
//        rows += this_row
//      }
//    }
//    rows.toArray
//  }

  def main(args: Array[String]): Unit = {

    //        val dataName = "data/inegi/ensaios_rolamento1/ensaio1_rol1_b+f+fft_pp.csv"
    //
    //        val windowSize = 1024
    //        val stepSize = windowSize
    //        createFFT(dataName,windowSize,stepSize)
    //            val dense = readDense("/data/inegi/fft_x.csv")
    //      println(dense.length)
    //    val sparse = denseToSparse(dense)
    //    saveSparse(sparse,"/data/inegi/sparse2.csv")
    //
    //    val trainData = cwd / "data/inegi/sparse2.csv"
    //
    //    val tmp1 = Source.svm[MultiClassification, SparseMatrixData]
    //    val a1ar1: Either[ADWError, SparseMatrixData[MultiClassification]] = tmp1.map(trainData)
    //
    //    println(a1ar1)
    val dense_1 = Array(
      Array(0.0, 1.0, 2.0),
      Array(3.0, 4.0, 5.0),
      Array(6.0, 7.0, 8.0)
    )

    val sparse = denseToSparse(dense_1)

    import pt.inescn.etl.Save._
    saveSparse(sparse,"/data/inegi/sparse2.csv")

    val trainData = cwd / "data/inegi/sparse2.csv"
    val tmp1 = Source.libSVMSparse(ValidateDepVar.binaryClassification, SparseFold.binaryClassFail)

    val a1ar1: Either[ADWError, SparseMatrixData] = tmp1.map(trainData)
    val a1a: SparseMatrixData = a1ar1.right.get
    // Data order has been reversed

    val len = sparse.length
    println(len)
    for(i <- sparse.indices){
      println("=== " + i + " ===")
      println(i+ ": " + sparse(i))
      val a1a_r: (Double, a1a.SparseFeature) = a1a(math.abs(i-len)-1)
      println(math.abs(i-len)-1+ ": " + a1a_r)
      println(sparse(i)==a1a_r)
    }
  }

}
