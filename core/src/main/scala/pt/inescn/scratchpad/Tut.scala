package pt.inescn.scratchpad

import pt.inescn.etl.stream.Load._
import pt.inescn.search.stream.TestTasks._
import pt.inescn.search.stream.UtilTasks._
import pt.inescn.search.stream.Pipes._
import pt.inescn.search.stream.Tasks._
import pt.inescn.utils.ADWError
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._


/**
  * root/ runMain pt.inescn.scratchpad.Tut
  *
  */
object Tut {

  def main(args: Array[String]): Unit = {
    val input = "input"

    def toTaskX[I,O,P1](func:(P1,I) => Out[O], p:P1, nm:TaskInfo): Task[I,O] = new Task[I,O] {
      //override type Params = List[P1]

      override val name: String = nm.funcName
      override val params: Params = Map( nm.params.head -> p )
      def f(i:I):Out[O] = func(p,i)
    }


    // The function
    def concatFunc(ap:String, i:String):Either[ADWError, String] = Right(i + ap)
    //def concatFunc(ap:Char, i:String):Either[ADWError, String] = Right(i + ap)
    // Its parameter value
    val ap = "_3"
    //val ap = '3'

    val tmp: (TaskInfo, (String, String) => Either[ADWError, String]) = nameFunc(concatFunc _)
    //val tmp: (TaskInfo, (Char, String) => Either[ADWError, String]) = nameFunc(concatFunc _)
    val name = tmp._1
    val f = tmp._2
    val tr: Func[String,String] = Func( toTask1(f, ap, name) )
    tr.t.f(input)

    val tq = toTask1(f, ap, name)
    tq.f(input)

    val ts = T(nameFunc(concatFunc _), ap)
    ts.t.f(input)

    val concat1 =  T1(concatFunc _, ap)
    concat1.t.f(input)

    //BUG: see https://users.scala-lang.org/t/type-mismatch-with-implicit-macro-how-to-diagnose-this/3922
    // Create the task
    //val concat0 =  pt.inescn.search.stream.Tasks.T(concatFunc _, ap)
    //concat0.t.f(input)


    /*

    // The function
    def concatFunc(ap:String, i:String):Either[ADWError, String] = Right(i + ap)
    // The parameter values we want to evaluate
    val ap = Iterable("_1", "_2", "_3")
    // Create the task
    val concat =  T(concatFunc _, ap)
    val op: Pipes.Op[String, String, Unit] = concat.f.head
    val func = op.asInstanceOf[Pipes.Func[String, String, Unit]]
    println(func.t.f(input))
    */

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

/*
    // Pipe
    val s1 = concat("0", "x", "y", "z") ->: concat("1", "x") ->: concat("2", "y") ->: concat("3", "z")

    // compile
    val c1 = compile(s1)
    val ex1 = c1.toList
    ex1.size

    // execute
    val r1 = execDebug(c1)(Frame(testData: _*))
    val r1l = r1.toList
    r1l.size



    val s3 = load(testData) *:
      All(concat("1", "x"), concat("2", "y")) ->:
      All(concat("3", "z"), concat("4", "z"))

    // compile
    val c3 = compile(s3)
    val c3l = c3.toList
    println(c3l)
    println(c3l.size)

    // execute
    val r3 = execDebug(c3)(empty).toList.mkString(";\n")
    println(r3)
    /*val r3l = r3.toList
    println(r3l)
    println(r3l.size)
    */
*/

    /*
    import scala.language.existentials

    val op = load(testData) *: toDouble("x", "y", "z") *:
      All(xDouble(1.0, "x"), xDouble(2.0, "x")) *:
      All(xDouble(3.0, "x"), xDouble(4.0, "x")) *:
      sum("x")
    val func = maxSum("x")

    // Debugging the operator |:
    val s1 = Aggregate(op, func.t)
    // Example of adding verbose output to execution
    //val p = Par(Func(func.t), parallelism = 0, verbose = 1)(Par.scheduler)
    //val s1 = Aggregate(op, p)

    // compile new pipes
    val c1l = compile(s1).toList
    // should be 1
    c1l.size

    // execute the pipes (one result)
    val r1l = execDebug(c1l.toIterable)(empty).toList
    // should be 1
    r1l.size

    // get the result
    val r1r = r1l.head
    val Right((r1r_1, r1r_2)) = r1r._1.right.get
    // should be Val(24.0)
    r1r_2

    val s2 = op |: func

    // compile new pipes
    val c2l = compile(s2).toList
    // should be 1
    c2l.size

    // execute the pipes (one result)
    val r2l = execDebug(c2l.toIterable)(empty).toList
    // should be 1
    r2l.size

    // get the result
    val r2r = r2l.head
    val Right((r2r_1, r2r_2)) = r2r._1.right.get
    // should be Val(24.0)
    r2r_2
    */

    /*
    import scala.language.existentials
    import scala.concurrent.duration.Duration

    val op = load(testData) *: toDouble("x", "y", "z") *: All(xDouble(1.0, "x"), xDouble(2.0, "x")) *: All(xDouble(3.0, "x"), xDouble(4.0, "x")) *: sum("x")
    val func = maxSum("x")

    val waitFor = Duration.Inf // 5000 millis
    val verbose = 0  // > 0 to print output
    val cores = Runtime.getRuntime.availableProcessors
    val scheduler = monix.execution.Scheduler.Implicits.global  // could be used implicitly

    val s1 = Aggregate(op, func.t, cores, waitFor, verbose, scheduler)

    // compile new pipes
    val c1 = compile(s1)
    val c1l = c1.toList
    // aggregator takes the 4 result and returns 1
    c1l.size

    // execute the pipes (one result)
    val r1 = execDebug(c1)(empty)
    val r1l = r1.toList
    // should be 1
    r1l.size

    // get the result
    val r1r = r1l.head
    val Right((r1r_1, r1r_2)) = r1r._1.right.get
    // this is maximum metric value obtained
    // should be Val(24.0)
    r1r_2

    import monix.execution.Scheduler.Implicits.global  // used implicitly
    // Parallel execution: implicit scheduler
    val agg = Par(func, verbose = verbose, duration = waitFor, parallelism = cores)

    val s2 = op ||: agg

    // compile new pipes
    val c2: Iterable[Either[ADWError, Exec[Any, Either[ADWError, (Executing, Val[_])]]]] = compile(s2)
    val c2l = c2.toList
    // aggregator takes the 4 result and returns 1
    c2l.size

    // execute the pipes (one result)
    val r2: Iterable[(Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing)] =
      execDebug(c2)(empty)
    val r2l = r2.toList
    // should be 1
    r2l.size

    // get the result
    val r2r: (Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing) = r2l.head
    // this is maximum metric value obtained
    val Right((r2r_1, r2r_2)) = r2r._1.right.get
    // should be Val(24.0)
    r1r_2

    import pt.inescn.samplers.stream.Ops
    import pt.inescn.samplers.stream.Ops._
    val pi0s = Ops.empty[Double]
    */

    //val r1 = pack(0.0)
    val r1 = pack("error")
    println(r1)
    println(r1.getClass)
    println("????????????????")
    //val d4 = unpack[(Int,String)](r1)
    val d4: Either[String, Int] = unpack[Int](r1)
    println(d4)
    println(d4.getClass)
    /*val d5 = d4.right.get + 1
    println(d5)
    println(d5.getClass)*/
  }

}
