package pt.inescn.scratchpad

import scala.meta._
import org.scalameta.logger // useful for debugging

/*
   https://www.youtube.com/watch?v=tXWBx2yVIEQ
   https://github.com/scalameta/scalameta/issues/1515

   "scalameta/paradise for 2.12.6 is on it's way to Maven Central, please note that scalameta/paradise is no longer
   under development and will not support 2.13. Please migrate to
   https://docs.scala-lang.org/overviews/macros/annotations.html those macro annotations are supported natively by the
   compiler in 2.13 under the scalac option -Ymacro-annotations scala/scala#6606"
     . https://docs.scala-lang.org/overviews/macros/annotations.html
     . https://github.com/scala/scala/pull/6606

 */

/**
  * runMain pt.inescn.scratchpad.AutoTask
  * ~:clean;runMain pt.inescn.scratchpad.AutoTask
  */
object AutoTask {

  import scala.language.implicitConversions


  /* Auto task */

  trait Task[I,O,P] {
    def name:String
    def params:List[P]
    def id:String = s"$name"
    def trace:String = s"$name($params)"
    def pre(i:I): Either[String,Task[I,O,P]] = Right(this)
    def f(i:I): Either[String,O]
  }

  def toTask2[I,O,P](func:(List[P],I) => O,p:List[P],nm:String=""): Task[I,O,P] = new Task[I,O,P] {
    override val name: String = if (nm=="") this.getClass.toString else nm
    override val params: List[P] = p
    def f(i:I):Either[String,O] = Right(func(params,i))
  }

  def toTask1[I,O](func:I => O,nm:String=""): Task[I,O,Nothing] = new Task[I,O,Nothing] {
    override val name: String = if (nm=="") this.getClass.toString else nm
    override val params: List[Nothing] = List.empty
    def f(i:I):Either[String,O] = Right(func(i))
  }

  implicit class toTaskC1[I,O](f:I => O) {
    def T: Task[I,O,Nothing] = toTask1(f)
  }

  implicit class toTaskCP1[I,O,P](param_func:((List[P],I) => O, List[P])) {
    private val f = param_func._1
    private val p = param_func._2
    def T: Task[I,O,P] = toTask2(f,p)
  }

  implicit class toTaskC2[I,O,P](named_func: (String, I => O)) {
    private val name = named_func._1
    private val f = named_func._2
    def T: Task[I,O,Nothing] = toTask1(f, name)
  }

  implicit class toTaskCP2[I,O,P](named_func: (String, (List[P],I) => O, List[P])) {
    private val name = named_func._1
    private val f = named_func._2
    private val p = named_func._3
    def T: Task[I, O, P] = toTask2(f, p, name)
  }

  implicit def toTaskD1[I,O](f:I => O): Task[I, O, Nothing] = toTask1(f)

  implicit def toTaskDP1[I,O,P](param_func:((List[P],I) => O, List[P])): Task[I,O,P] = {
    val f = param_func._1
    val p = param_func._2
    toTask2(f,p)
  }

  implicit def toTaskD2[I,O,P](named_func: (String, I => O)): Task[I,O,Nothing] = {
    val name = named_func._1
    val f = named_func._2
    toTask1(f, name)
  }

  implicit def toTaskDP2[I,O,P](named_func: (String, (List[P],I) => O, List[P])): Task[I, O, P]= {
    val name = named_func._1
    val f = named_func._2
    val p = named_func._3
    toTask2(f, p, name)
  }


  implicit class TaskOps[I1,O1,P1](o: Task[I1,O1,P1]) {
    def ->:[O2,P2](that: Task[O1,O2,P2]): Task[I1, O2, P1] = {
      new Task [I1,O2,P1] {
        override val name: String = o.name + " ->: " + that.name
        override val params: List[P1] = o.params
        def f(b:I1): Either[String, O2] = {
          val first = o.f(b)
          first.flatMap(e => that.f(e))
        }
      }
    }

    def andThen[O2,P2](that: Task[O1,O2,P2]): Task[I1, O2, P1] = that ->: o
  }


  import scala.language.implicitConversions

  trait NamedFunc[I,O]{ self =>
    def name:String
    def func(i:I): O

    def -:[IN](that: NamedFunc[IN,I]): NamedFunc[IN,O] = {
      new NamedFunc[IN,O] {
        override val name: String = that.name + " ->: " + self.name
        override def func(b:IN): O = {
          val first:I = that.func(b)
          self.func(first)
        }
      }
    }

    def andNext[Out](that: NamedFunc[O,Out]): NamedFunc[I,Out] = {
      new NamedFunc[I,Out] {
        override val name: String = self.name + " andNext " + that.name
        override def func(b:I): Out = {
          val first:O = self.func(b)
          that.func(first)
        }
      }
    }
  }


  implicit def toNamedFunc[I,O](named_func: (String, I => O)): NamedFunc[I,O] = {
    val nm = named_func._1
    val f = named_func._2
    new NamedFunc[I,O] {
      override val name: String = nm
      override def func(b:I): O = {
        f(b)
      }
    }
  }

  /*
  // Only works in Dotty
  implicit class FuncOps[T,IO,O](t: T)(implicit conv: T => NamedFunc[IO,O]) {
    private val o = conv(t)
    def -:[I](that: NamedFunc[I,IO]): NamedFunc[I,O] = {
      new NamedFunc[I,O] {
        override val name: String = that.name + " ->: " + o.name
        override def func(b:I): O = {
          val first:IO = that.func(b)
          o.func(first)
        }
      }
    }

    def andNext[I](that: NamedFunc[O,I]): NamedFunc[IO,I] = {
      new NamedFunc[IO,I] {
        override val name: String = o.name + " andNext " + that.name
        override def func(b:IO): I = {
          val first:O = o.func(b)
          that.func(first)
        }
      }
    }
  }
*/
  /* Did not work
  implicit class FuncOps[IO,O](t: (String, IO => O))(implicit conv: ((String, IO => O)) => NamedFunc[IO,O]) {
  */
  implicit class FuncOps[A,B,IO,O](t: (A,B))(implicit conv: ((A,B)) => NamedFunc[IO,O]) {
    private val o = conv(t)
    def -:[I](that: NamedFunc[I,IO]): NamedFunc[I,O] = that.andNext(o)
    def andNext[I](that: NamedFunc[O,I]): NamedFunc[IO,I] = o.andNext(that)
  }

  def f(a:Int):Int = -a
  val h: Int => Double = (a:Int) => (2*a).toDouble

  val g: (List[Int], Int) => Int =
    (a:List[Int], b:Int) => {
      if (a.isEmpty)
        b
      else
        a.head * b
    }

  def callFunc[I,O](f:I => O, i:I):O =
    f(i)


  def printTask[I,O,P](t:Task[I,O,P]): Unit = {
    println(s"t.id    = ${t.id}")
    println(s"t.trace = ${t.trace}")
  }


  def main(args: Array[String]): Unit = {

    /*
     * Basic experiments with implicit conversion from scala functions to
     * NamedFunc. Problem here is that the scalac need to make the implicit
     * type more explicit (cannot use a generic type, which works in Dotty)
     */

    val t1h:NamedFunc[Int,Double] = ("h", h)
    val t1f:NamedFunc[Int,Int] = ("f", f _)

    // f then h, t1h is the class (right assoc.)
    val itt1_2b = t1f -: t1h
    println(itt1_2b.name)

    // f then h, t1h is the class (right assoc.)
    val itt1_2a = ("f", f _) -: t1h
    println(itt1_2a.name)

    // Implicit conversion not working
    val itt1_2c = t1f -: ("h", h)
    println(itt1_2c.name)

    val itt1_2d = ("f", f _) -: ("h", h)
    println(itt1_2d.name)

    // f then h, ("f", f _) is the class (left assoc.)
    val itt1_2e = ("f", f _) andNext t1h
    println(itt1_2e.name)

    // f then h, t1f is the class (left assoc.)
    val itt1_2f = t1f andNext t1h
    println(itt1_2f.name)

    /*
       Implicit Task conversions
     */

    val t1: Task[Int, Int, Int] = toTask2(g, List(1))
    println(t1.f(3))
    printTask(t1)

    val t2: Task[Int, Int, Int] = toTask2(g, List(1), "g")
    println(t2.f(2))
    printTask(t2)

    val t3: Task[Int, Int, Nothing] = toTask1(f, "f")
    println(t3.f(1))
    printTask(t3)

    val it1:Task[Int,Int,Nothing] = (f _).T
    println(it1.f(1))
    printTask(it1)

    val it2:Task[Int,Int,Nothing] = ("f", f _).T
    println(it2.f(1))
    printTask(it2)

    val it3: Task[Int, Int, Int] = (g, List(1)).T
    println(it3.f(2))
    printTask(it3)

    val it1_2 = (f _).T ->: (g, List(3)).T
    println(it1_2.f(2))
    printTask(it1_2)

    val itn1_2 = ("f", f _).T ->: ("g", g, List(3)).T
    println(itn1_2.f(2))
    printTask(itn1_2)

    val tit1: Task[Int,Int,_] = ("f", f _)
    println(tit1.f(2))
    printTask(tit1)

    val tit2: Task[Int,Int,_] = ("g", g, List(3))
    println(tit2.f(2))
    printTask(tit2)

    val its1_2 = tit1 ->: tit2
    println(its1_2.f(2))
    printTask(its1_2)

    /* Cannot get this to work
    //val itt1_2 = ("f", f _) ->: ("g", g, List(3))
    val itt1_2 = ("f", f _) andThen ("g", g, List(3))
    println(itt1_2.f(2))
    printTask(itt1_2)
    */

    val addition = q"x + y"
    println(addition)
    val fdef = q"def f(a:Int):Int = -a"
    println(fdef)
    fdef match {
      case q"def f(a:Int):Int = -a" =>
        println("Ok")
      case _ =>
        println("Ko")
    }

  }

}


/*

// Scalac + dotty

object Test {
  import scala.language.implicitConversions

  trait NamedFunc[I,O]{ self =>
    def name:String
    def func(i:I): O

    def -:[IN](that: NamedFunc[IN,I]): NamedFunc[IN,O] = {
      new NamedFunc[IN,O] {
        override val name: String = that.name + " ->: " + self.name
        override def func(b:IN): O = {
          val first:I = that.func(b)
          self.func(first)
        }
      }
    }

    def andNext[Out](that: NamedFunc[O,Out]): NamedFunc[I,Out] = {
      new NamedFunc[I,Out] {
        override val name: String = self.name + " andNext " + that.name
        override def func(b:I): Out = {
          val first:O = self.func(b)
          that.func(first)
        }
      }
    }
  }

  implicit def toNamedFunc[I,O](named_func: (String, I => O)): NamedFunc[I,O] = {
    val nm = named_func._1
    val f = named_func._2
    new NamedFunc[I,O] {
      override val name: String = nm
      override def func(b:I): O = {
        f(b)
      }
    }
  }

  implicit class FuncOps[A,B,IO,O](t: (A,B))(implicit conv: ((A,B)) => NamedFunc[IO,O]) {
    private val o = conv(t)
    def -:[I](that: NamedFunc[I,IO]): NamedFunc[I,O] = that.andNext(o)
    def andNext[I](that: NamedFunc[O,I]): NamedFunc[IO,I] = o.andNext(that)
  }



  def f(a:Int):Int = -a
  val h: Int => Double = (a:Int) => (2*a).toDouble

  def main(args: Array[String]): Unit = {

    val n1:NamedFunc[Int,Int] = ("f", f _)
    val n2:NamedFunc[Int,Double] = ("h", h)

    // n2 is assigned implicit class member t (right associative)
    val itn1_2 = n1 -: n2
    println(itn1_2.name)
    println(itn1_2.func(2))

    // ("h", h) is assigned implicit class member t (right associative)
    val itt1_2 = ("f", f _) -: ("h", h)
    println(itt1_2.name)
    println(itt1_2.func(2))

    // ("f", f _) is assigned implicit class member t (left associative)
    val ist1_2 = ("f", f _) andNext ("h", h)
    println(ist1_2.name)
    println(ist1_2.func(2))
  }

}

// Dotty Only

object Test {
  import scala.language.implicitConversions

  trait NamedFunc[I,O]{
    def name:String
    def func(i:I): O
  }

  implicit def toNamedFunc[I,O](named_func: (String, I => O)): NamedFunc[I,O] = {
    val nm = named_func._1
    val f = named_func._2
    new NamedFunc[I,O] {
      override val name: String = nm
      override def func(b:I): O = {
        f(b)
      }
    }
  }

  implicit class FuncOps[T,IO,O](t: T)(implicit conv: T => NamedFunc[IO,O]) {
    private val o = conv(t)
    def -:[I](that: NamedFunc[I,IO]): NamedFunc[I,O] = {
      new NamedFunc[I,O] {
        override val name: String = that.name + " ->: " + o.name
        override def func(b:I): O = {
          val first:IO = that.func(b)
          o.func(first)
        }
      }
    }

    def andNext[I](that: NamedFunc[O,I]): NamedFunc[IO,I] = {
      new NamedFunc[IO,I] {
        override val name: String = o.name + " andNext " + that.name
        override def func(b:IO): I = {
          val first:O = o.func(b)
          that.func(first)
        }
      }
    }
  }



  def f(a:Int):Int = -a
  val h: Int => Double = (a:Int) => (2*a).toDouble

  def main(args: Array[String]): Unit = {

    val n1:NamedFunc[Int,Int] = ("f", f _)
    val n2:NamedFunc[Int,Double] = ("h", h)

    // n2 is assigned implicit class member t (right associative)
    val itn1_2 = n1 -: n2
    println(itn1_2.name)
    println(itn1_2.func(2))

    // ("h", h) is assigned implicit class member t (right associative)
    val itt1_2 = ("f", f _) -: ("h", h)
    println(itt1_2.name)
    println(itt1_2.func(2))

    // ("f", f _) is assigned implicit class member t (left associative)
    val ist1_2 = ("f", f _) andNext ("h", h)
    println(ist1_2.name)
    println(ist1_2.func(2))
  }

}
 */

/*
// https://users.scala-lang.org/t/combined-use-of-implicit-conversion-and-implicit-class-failing/3090/2
// Works wih dotty, not scalac


object Test {
  import scala.language.implicitConversions

  trait NamedFunc[I,O]{
    def name:String
    def func(i:I): O
  }

  implicit def toNamedFunc[I,O](named_func: (String, I => O)): NamedFunc[I,O] = {
    val nm = named_func._1
    val f = named_func._2
    new NamedFunc[I,O] {
      override val name: String = nm
      override def func(b:I): O = {
        f(b)
      }
    }
  }

  /*
  implicit class FuncOps[I1,O1](o: NamedFunc[I1,O1]) {
    def -:[O2,P2](that: NamedFunc[O1,O2]): NamedFunc[I1,O2] = {
      new NamedFunc[I1,O2] {
        override val name: String = o.name + " ->: " + that.name
        override def func(b:I1): O2 = {
          val first = o.func(b)
          that.func(first)
        }
      }
    }

    def andNext[O2,P2](that: NamedFunc[O1,O2]): NamedFunc[I1,O2] = that -: o
  }*/
implicit class FuncOps[T,I1,O1](t: T)(implicit conv: T => NamedFunc[I1,O1]) {
  private val o = conv(t)
  def -:[O2,P2](that: NamedFunc[O1,O2]): NamedFunc[I1,O2] = {
    new NamedFunc[I1,O2] {
      override val name: String = o.name + " ->: " + that.name
      override def func(b:I1): O2 = {
        val first = o.func(b)
        that.func(first)
      }
    }
  }

  def andNext[O2,P2](that: NamedFunc[O1,O2]): NamedFunc[I1,O2] = that -: o
}



  def f(a:Int):Int = -a
  val h: Int => Int = (a:Int) => 2*a

  val g: (List[Int], Int) => Int =
    (a:List[Int], b:Int) => {
      if (a.isEmpty)
        b
      else
        a.head * b
    }

  def main(args: Array[String]): Unit = {

    val n1:NamedFunc[Int,Int] = ("f", f _)
    val n2:NamedFunc[Int,Int] = ("h", h)

    val itn1_2 = n1 -: n2

    //val itt1_2 = ("f", f _) -: ("g", g, List(3))
    val itt1_2 = ("f", f _) andNext ("h", h)
    println(itt1_2.func(2))
  }

}
 */