package pt.inescn.scratchpad

import scala.collection.mutable
import scala.reflect.ClassTag
import scala.language.implicitConversions


/**
  * sbt "root/runMain pt.inescn.scratchpad.ValVec"
  */
object ValVec {

  sealed trait Val[V] { self =>
    val v: V

    override def toString: String = s"Val($v)"

    override def equals(obj: scala.Any): Boolean = {
      obj match {
        case other: Val[V] =>
          v.equals(other.v)
        case _ =>
          false
      }
    }

    override def hashCode(): Int = v.hashCode()

    def arr(n: Int)(implicit tag1: ClassTag[V]): Array[V] = Array.fill[V](n)(v)
    def arr0(n: Int): Array[Any] = Array.fill[Any](n)(v)
    def vec(n: Int): Vector[V] = Vector.fill[V](n)(v)

    def cast[T]:T = v.asInstanceOf[T]
  }


  case class Empty[A](override val v:A) extends Val[A]
  case class ErrVal(override val v:String) extends Val[String]


  object Val {
    def apply[T](e:T): Val[T] = new Val[T] {
      override val v:T = e
    }
  }

  implicit def pack[A](s:A) : Val[A] = new Val[A] {
    override val v: A = s
  }

  // No class tag available
  implicit def unpack[T](t:Val[_]): Either[String,T] = {
    try {
      val v = t.v.asInstanceOf[T]
      Right(v)
    } catch {
      // When used via implicits should never reach this
      case e: Exception => Left(e.getMessage)
    }
  }

  def main(args: Array[String]): Unit = {

    val v1: Val[_] = pack(1)
    val v2: Val[_] = pack(2)

    val varr0: Val[Vector[_]] = v1.vec(2)
    val arr0 = unpack[Vector[_]](varr0).right.get
    val narr0 = arr0.updated(0,v1.v)
    println(narr0.mkString(","))

    val varr1: Val[Array[_]] = v1.arr0(2)
    val arr1 = unpack[Array[Any]](varr1).right.get
    arr1.update(0,v2.v)
    println(arr1.mkString(","))
    println(arr1(0).getClass)
    println(s"arr = ${arr1.toVector.mkString(",")}")

    val tst1 = Val(Array("1", "1", "1", "1"))
    println(tst1)
    println(tst1.v)
    val tst2 = Val(Vector("1", "1", "1", "1"))
    println(tst2)
    println(tst2.v)
    val tst3 = Val(mutable.ArrayBuffer("1", "1", "1", "1"))
    println(tst3)
    println(tst3.v)
    val tst4 = Val(mutable.ArraySeq("1", "1", "1", "1"))
    println(tst4)
    println(tst4.v)


  }


}
