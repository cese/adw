package pt.inescn.scratchpad
/*
//package inesctec.beincpps.services



import java.util.Date
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import com.rabbitmq.client._
import java.io.IOException
import java.io.IOException
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.Connection
import com.rabbitmq.client.Channel
import com.rabbitmq.client.MessageProperties


/**
  * root/runMain pt.inescn.scratchpad.RabbitTests
 */

object sender {
  private val TASK_QUEUE_NAME = "task_queue"

  private def getMessage(strings: Array[String]): String = {
    if (strings.length < 1) return "Hello World!"
    joinStrings(strings, " ")
  }

  private def joinStrings(strings: Array[String], delimiter: String): String = {
    val length = strings.length
    if (length == 0) return ""
    val words = new StringBuilder(strings(0))
    var i = 1
    while ( {
      i < length
    }) {
      words.append(delimiter).append(strings(i))

      {
        i += 1; i - 1
      }
    }
    words.toString
  }


  def main(argv: Array[String]): Unit = {
    import com.rabbitmq.client.ConnectionFactory
    val factory = new ConnectionFactory
    factory.setHost("localhost")
    val connection = factory.newConnection
    val channel = connection.createChannel
    import com.rabbitmq.client.MessageProperties
    channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null)

    val message = getMessage(argv)

    channel.basicPublish("", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes)
    System.out.println(" [x] Sent '" + message + "'")

    channel.close

  }
}



object worker {
  private val TASK_QUEUE_NAME: String = "task_queue"

  private def doWork(task: String): Unit = {
    for (ch <- task.toCharArray) {
      if (ch == '.') Thread.sleep(1000)
    }
  }

  def main(args: Array[String]): Unit = {
    import com.rabbitmq.client.ConnectionFactory
    val factory = new ConnectionFactory
    factory.setHost("localhost")
    val connection = factory.newConnection
    val channel = connection.createChannel

    channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null)
    System.out.println(" [*] Waiting for messages. To exit press CTRL+C")

    channel.basicQos(1)


    val consumer = new DefaultConsumer(channel) {
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]): Unit = {
        val message = new String(body, "UTF-8")
        System.out.println(" [x] Received '" + message + "'")
        try
          doWork(message)
        finally {
          System.out.println(" [x] Done")
          channel.basicAck(envelope.getDeliveryTag, false)
        }
      }
    }

  }

}
*/