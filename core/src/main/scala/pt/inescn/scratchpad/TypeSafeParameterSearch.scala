package pt.inescn.scratchpad

import pt.inescn.samplers.Base.{Sampler, _}
import pt.inescn.samplers.Enumeration.{ListRange, RangeOf, Single}
import pt.inescn.samplers.Distribution.{Const, DiscreteUniform, IntUniform, Uniform}


/**
  * Show the pattern of using classes that assign and hide a base class type parameter.
  * Lets assume we want to "tag" `BaseSampler[L <: S, T]` and then create specific sub-classes
  * of this type were `L` is assigned a specific `S`. In the example here `BSampler1` is
  * tagged with the type `S1` and `BSampler2` is tagged with the type `S2`.
  *
  * We now want to create a DSL of sorts consisting of several type, each one will return a
  * sampler with a specific type `S`. The DSL consists of the class `UseSampler[T]` and its
  * sub-classes `BSampler1` and `BSampler2` hat have been tagged with `S1` and `S2` respectively.
  * Note that we want each `UseSampler[T]` to be able to generate a `BaseSampler[L<:S, T]`. So
  * we define the method `def getSampler: BaseSampler[_, T]` that can be overridden.
  *
  * IMPORTANT: note that the return type is defined **existentially**. In other words we are say
  * that the function will return a `BaseSampler[L <: S, T]`, if there exists one (were `L <: S`).
  * If we used the **universal** quantifier so:
  *
  * {{{
  * sealed trait UseSampler[T] {
  *   def getSampler[L <: S]: BaseSampler[L,T]
  * }
  * }}}
  *
  * or so:
  *
  * {{{
  * sealed trait UseSampler[L <: S, T] {
  *   def getSampler: BaseSampler[L,T]
  * }
  * }}}
  *
  * we are stating that for all `L<:S` `getSampler` will return the same `BaseSampler[L,T]` for
  * all possible `S`. This is is not true. `BSampler1` has only type `S1` and `BSampler2` has only
  * type `S2`. The compiler will complaint that the types have `incompatible type` stating that in:
  *
  * {{{
  * final case class Option1[T](v: T) extends UseSampler[T] {
  *   override def getSampler[L<:S]: BaseSampler[L, T] = BSampler1[T](v)
  *                                                                  ^
  *   }
  * }}}
  *
  * {{{
  * [error]  found   : pt.inescn.scratchpad.Exp0.BSampler1[pt.inescn.scratchpad.Exp0.S1,T]
  * [error]  required: pt.inescn.scratchpad.Exp0.BaseSampler[L,T]
  * [error]     override def getSampler[L<:S]: BaseSampler[L, T] = BSampler1[T](v)
  * [error]                                                                    ^
  * [info] pt.inescn.scratchpad.Exp0.BSampler1[pt.inescn.scratchpad.Exp0.S1,T] <: pt.inescn.scratchpad.Exp0.BaseSampler[L,T]?
  * [info] false}}}
  *
  * @see https://users.scala-lang.org/t/how-to-ensure-subclass-relationship-when-extending-base-with-more-typed-parameters/1563
  */
object Exp0 {

  sealed trait S
  sealed trait S1 extends S
  sealed trait S2 extends S

  abstract class BaseSampler[L <: S, T]

  class BSampler1[L <: S, T] private (x: T) extends BaseSampler[L, T]

  object BSampler1 {
    def apply[T](v: T): BSampler1[S1, T] = new BSampler1[S1, T](v)
  }

  class BSampler2[L <: S, T] private (x: T) extends BaseSampler[L, T]

  object BSampler2 {
    def apply[T](x: T): BSampler2[S2, T] = new BSampler2[S2, T](x)
  }

  sealed trait UseSampler[T] {
    def getSampler: BaseSampler[_, T]
    //def getSampler[L <: S]: BaseSampler[L, T]
  }

  final case class Option1[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler1[S1, T] = BSampler1[T](v)
    //override def getSampler[L<:S]: BaseSampler[L, T] = BSampler1[T](v)
  }

  final case class Option2[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler2[S2, T] = BSampler2[T](v)
    //override def getSampler[L<:S]: BaseSampler[L, T] = BSampler2[T](v)
  }

  val bs2: BSampler2[S2, Int] = BSampler2(1)
  val bs1: BSampler1[S1, String] = BSampler1("One")

  val t1: Option1[Int] = Option1(10)
  val t2: Option2[String] = Option2("Ten")
  val r1: BSampler1[S1, Int] = t1.getSampler
  val r2: BSampler2[S2, String] = t2.getSampler
}

/**
  * Same as above, but we remove the boilerplate by making use of the `case class`.
  */
object Exp1 {

  sealed trait S
  sealed trait S1 extends S
  sealed trait S2 extends S

  abstract class BaseSampler[L <: S, T]

  case class BSampler1[L <: S, T](x: T) extends BaseSampler[L, T]
  case class BSampler2[L <: S, T](x: T) extends BaseSampler[L, T]

  sealed trait UseSampler[T] {
    def getSampler: BaseSampler[_, T]
  }

  final case class Option1[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler1[S1, T] = BSampler1(v)
  }

  final case class Option2[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler2[S2, T] = BSampler2(v)
  }

  val bs2: BSampler2[S2, Int] = BSampler2(1)
  val bs1: BSampler1[S1, String] = BSampler1("One")

  val t1: Option1[Int] = Option1(10)
  val t2: Option2[String] = Option2("Ten")
  val r1: BSampler1[S1, Int] = t1.getSampler
  val r2: BSampler2[S2, String] = t2.getSampler
}

/**
  * Same as above but we assign the "tag" directly in the `BSampler1` a `BSampler2` constructors.
  */
object Exp2 {

  sealed trait S
  sealed trait S1 extends S
  sealed trait S2 extends S

  abstract class BaseSampler[L <: S, T]

  case class BSampler1[T](x: T) extends BaseSampler[S1, T]
  case class BSampler2[T](x: T) extends BaseSampler[S2, T]

  sealed trait UseSampler[T] {
    def getSampler: BaseSampler[_, T]
  }

  final case class Option1[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler1[T] = BSampler1(v)
  }

  final case class Option2[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler2[T] = BSampler2(v)
  }

  val bs2: BSampler2[Int] = BSampler2(1)
  val bs1: BSampler1[String] = BSampler1("One")

  val t1: Option1[Int] = Option1(10)
  val t2: Option2[String] = Option2("Ten")
  val r1: BSampler1[Int] = t1.getSampler
  val r2: BSampler2[String] = t2.getSampler
}

/**
  * This is an attempt to combine various `BaseSampler[L <: S, T]`. Note that the type `S`
  * is used as a *Phantom* type so it is only available at compile time. However it also not
  * available at runtime for a function with the signature:
  * `def combine[L1 <: S, L2 <: S, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2])`
  * because here we want to match on the phantom types `L1` an `L2` and matching on instances is
  * done in runtime. So how can we do this? We have several options:
  * - Reflection
  * - Shapeless
  * - Macros
  * - Carry an instance of the (now not) phantom type
  *
  * In the code below we have in comments the use of reflection. Reflection requires that the
  * type information be carried during runtime and then used as instances in the match. In this
  * case the compiler cannot help us and we will get runtime errors during execution.
  *
  * In the code below we altered the `BaseSampler[L <: S, T]` class to carry an instance of the
  * phantom type `S`. In this case, assuming the phantom type is a sealed trait, the compiler
  * can now check for and report cases that have not been foreseen. Note however that the compiler
  * reports these as *warnings* and not errors.
  *
  * Note: because we check all cases of the phantom states, we must have means of either reporting
  * invalid combinations as errors or dealing wit them in some other way. We have opted to return
  * an `Empty` sampler that will produce invalid (empty) samples.
  *
  * To see the failures during runtime for the commented code, run the main function the usual way.
  *
  * NOTE: the abstract class `BaseSampler[L <: S, T]` also has the abstract member `val x: T` so that
  * it can be overridden by the extending samplers. If we set the value `x` in the constructor
  * parameter list, overriding would will work.
  *
  * @see http://typelevel.org/blog/2016/09/19/variance-phantom.html
  * @see https://stackoverflow.com/questions/1094173/how-do-i-get-around-type-erasure-on-scala-or-why-cant-i-get-the-type-paramete
  */
object Exp3 {

  sealed trait S
  sealed trait S1 extends S
  sealed trait S2 extends S

  abstract class BaseSampler[L <: S, T]{
    val x : T
    val y : S
  }

  case class Empty[T](override val x: T) extends BaseSampler[S1, T] {
    override val y: S = new S1{}
  }

  case class BSampler1[T](override val x: T) extends BaseSampler[S1, T] {
    override val y: S = new S1{}
  }
  case class BSampler2[T](override val x: T) extends BaseSampler[S2, T] {
    override val y: S = new S2{}
  }
  case class BSampler3[T](override val x: T) extends BaseSampler[S1, T] {
    override val y: S = new S1{}
  }
  case class BSampler4[T](override val x: T) extends BaseSampler[S2, T] {
    override val y: S = new S2{}
  }
  case class ComplexSampler[L <: S, T1, T2](a: T1, b: T2) extends BaseSampler[L, (T1,T2)] {
    override val y: S = new S1{}
    override val x : (T1,T2) = (a,b)
  }

  // http://typelevel.org/blog/2016/09/19/variance-phantom.html
  // https://gist.github.com/jkpl/5279ee05cca8cc1ec452fc26ace5b68b
  // https://stackoverflow.com/questions/6601779/existential-types-and-pattern-matching-in-scala
  // https://stackoverflow.com/questions/38618071/how-to-avoid-non-variable-type-argument-is-unchecked-since-it-is-eliminated-by
  // https://stackoverflow.com/questions/1094173/how-do-i-get-around-type-erasure-on-scala-or-why-cant-i-get-the-type-paramete
  // https://stackoverflow.com/questions/25222989/erasure-elimination-in-scala-non-variable-type-argument-is-unchecked-since-it
  // https://github.com/scala/scala/pull/5774
  // https://stackoverflow.com/questions/12218641/scala-what-is-a-typetag-and-how-do-i-use-it
  // https://stackoverflow.com/questions/12093752/scala-macros-cannot-create-typetag-from-a-type-t-having-unresolved-type-parame
  // TODO: use macros or shapeless (https://stackoverflow.com/questions/1094173/how-do-i-get-around-type-erasure-on-scala-or-why-cant-i-get-the-type-paramete)
  //def combine[L1 <: S, L2 <: S, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2]) = ComplexSampler[S1, T1, T2](a.x, b.x)
  def combine[L1 <: S, L2 <: S, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2]):
    BaseSampler[_ >: S1 with S2 <: S, (T1, T2)] = {
    (a.y, b.y) match {
      case (_:S1, _:S1) => ComplexSampler[S1, T1, T2](a.x, b.x)
      case (_:S2, _:S2) => ComplexSampler[S2, T1, T2](a.x, b.x)
      case (_, _) => Empty((a.x, b.x))
    }
  }
  /*
  // Ok
  import scala.reflect.runtime.universe._
  def combine[L1 <: S : TypeTag, L2 <: S : TypeTag, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2]) = {
    if ((typeOf[L1] == typeOf[S1]) && (typeOf[L2] == typeOf[S1]))
      ComplexSampler[S1, T1, T2](a.x, b.x)
    else
      throw new RuntimeException("Incorrect S types.")
  }
  // Ok
  import scala.reflect.runtime.universe._
  def combine[L1 <: S : TypeTag, L2 <: S : TypeTag, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2]) = {
    (a, b) match {
      case (_: BaseSampler[L1,T1], _: BaseSampler[L2,T2]) if (typeOf[L1] == typeOf[S1]) && (typeOf[L2] == typeOf[S1])
      => ComplexSampler[S1, T1, T2](a.x, b.x)
    }
  }
  // Ok
  import scala.reflect.{ClassTag, classTag}
  def combine[L1 <: S : ClassTag, L2 <: S : ClassTag, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2]) = {
    (a, b) match {
      case (_: BaseSampler[L1,T1], _: BaseSampler[L2,T2]) if (classTag[L1] == classTag[S1]) && (classTag[L2] == classTag[S1])
      => ComplexSampler[S1, T1, T2](a.x, b.x)
    }
  }
  // non-variable type argument pt.inescn.scratchpad.Exp3.S1 in type pattern pt.inescn.scratchpad.Exp3.BaseSampler[pt.inescn.scratchpad.Exp3.S1,T1] is unchecked since it is eliminated by erasure
  def combine[L1 <: S, L2 <: S, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2]) = {
    (a, b) match {
      case (_: BaseSampler[S1,T1], _: BaseSampler[S1,T2]) => ComplexSampler[S1, T1, T2](a.x, b.x)
    }
  }
  // Does not check tag
  def combine[L1 <: S, L2 <: S, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2]) = {
      (a,b) match {
        case (BSampler1(aa), BSampler1(bb)) => ComplexSampler[S1,T1,T2](aa,bb)
      }
  }*/

  sealed trait UseSampler[T] {
    def getSampler: BaseSampler[_, T]
  }

  final case class Option1[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler1[T] = BSampler1(v)
  }

  final case class Option2[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler2[T] = BSampler2(v)
  }

  val bs2: BSampler2[Int] = BSampler2(1)
  val bs1: BSampler1[String] = BSampler1("One")

  val t1: Option1[Int] = Option1(10)
  val t2: Option2[String] = Option2("Ten")
  val r1: BSampler1[Int] = t1.getSampler
  val r2: BSampler2[String] = t2.getSampler
  //val r3: ComplexSampler[S1, Int, String] = combine(r1, r2)
  //val r3: ComplexSampler[_ >: S1 with S2 <: S, Int, String] = combine(r1, r2)
  //val r3: (BaseSampler[_$1, (Int, String)] with Product with Serializable) forSome {type _$1 >: S1 with S2 <: S} = combine(r1, r2)
  val r3: BaseSampler[_ >: S1 with S2 <: S, (Int, String)] = combine(r1, r2) // r3 = Empty((10,Ten))

  /*
  def main(args: Array[String]) {
    println(s"r3 = $r3")
  }*/

}

/**
  * This is an example were unlike `Exp3` we use the Phantom type during compile time, when it is
  * still available. As such, we do not need an instance of the tag `S`. We also set the tag in the
  * `BaseSampler[L <: S, T]` directly when it is extended. The function `combine` will only be
  * selected and used bu the compiler according to the implicit evidence. Note that the code:
  *   {{{{val r4 = combine(r1,r2) }}}
  * is commented out because compilation will fail here. The reason being that we only want
  * to combine tags of the same type and `r1` has `L = S1` and `r1` has `L = S2`.
  *
  * Note: one small wart is that the compiler complains that the implicit parameter is never used.
  * We could try to use `implicitly` but have not found out how to use it with several parameters.
  * References:
  * @see https://stackoverflow.com/questions/20131589/context-bounds-with-two-generic-parameters
  * @see https://stackoverflow.com/questions/4373070/how-do-i-get-an-instance-of-the-type-class-associated-with-a-context-bound/4373153#437315
  * @see  https://stackoverflow.com/questions/20131589/context-bounds-with-two-generic-parameters
  * @see http://www.scala-lang.org/old/node/5310
  * @see https://stackoverflow.com/questions/22714609/using-scala-implicitly-for-type-equality
  * @see http://typelevel.org/blog/2014/01/18/implicitly_existential.html
  */
object Exp4 {

  sealed trait S
  sealed trait S1 extends S
  sealed trait S2 extends S

  abstract class BaseSampler[L <: S, T]{
    val x : T
  }

  case class Empty[T](override val x: T) extends BaseSampler[S1, T]
  case class BSampler1[T](override val x: T) extends BaseSampler[S1, T]
  case class BSampler2[T](override val x: T) extends BaseSampler[S2, T]
  case class BSampler3[T](override val x: T) extends BaseSampler[S1, T]
  case class BSampler4[T](override val x: T) extends BaseSampler[S2, T]
  case class ComplexSampler[L <: S, T1, T2](a: T1, b: T2) extends BaseSampler[S1, (T1,T2)] {
    override val x : (T1,T2) = (a,b)
  }

  //def combine[L1 <: S, L2 <: S, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2])(implicit ev1: L1 =:= L2 ):
  def combine[L1 <: S, L2 <: S, T1, T2](a: BaseSampler[L1,T1], b: BaseSampler[L2,T2]) :
    ComplexSampler[L1, T1, T2] =
    ComplexSampler[L1, T1, T2](a.x, b.x)

  sealed trait UseSampler[T] {
    def getSampler: BaseSampler[_, T]
  }

  final case class Option1[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler1[T] = BSampler1(v)
  }

  final case class Option2[T](v: T) extends UseSampler[T] {
    override def getSampler: BSampler2[T] = BSampler2(v)
  }

  val bs2: BSampler2[Int] = BSampler2(1)
  val bs1: BSampler1[String] = BSampler1("One")

  val t1: Option1[Int] = Option1(10)
  val t2: Option2[String] = Option2("Ten")
  val r1: BSampler1[Int] = t1.getSampler
  val r2: BSampler2[String] = t2.getSampler
  //val r4 = combine(r1,r2) // Ok, compilation fails
  val r5 = combine(r1,bs1) // Ok, compilation completes

  /*
  def main(args: Array[String]) {
    println(s"r3 = $r5") // r3 = ComplexSampler(10,One)
  }*/

}

// TODO: http://typelevel.org/blog/2016/09/19/variance-phantom.html

object Exp5a {
  sealed trait S
  sealed trait S1 extends S
  sealed trait S2 extends S

  // Need covariance due to BY
  abstract class B[+L <: S, T]{
    def get: T
  }
  case class B1[T](x: T) extends B[S1,T] {
    override def get: T = x
  }
  case class B2[T](x: T) extends B[S2,T] {
    override def get: T = x
  }
  case class B3[T](x: T) extends B[S1,T] {
    override def get: T = x
  }
  case class BX[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2]) extends B[L1,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class BY[L<:S, T1, T2](a: B[L,T1], b:B[L,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class BZ[L<:S,L1<:S,L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }

  // IMPORTANT: the =:= does not affect basic type inference. Removing the implicit evidence does not change
  // whether or not BX or By can be created via the products. However, the implicit evidence does avoid
  // calling the products with different S.

  //def product1[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2])(implicit ev : L1 =:= L2): BX[L1, L2, T1, T2] = BX(a,b)
  def product1[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2]) = BX(a,b)
  //def product2[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2])(implicit ev : L1 =:= L2): BY[S, T1, T2] = BY[S,T1,T2](a,b)
  def product2[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2]) = BY(a,b)
  //def product3[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2])(implicit ev : L1 =:= L2): BZ[S1,L1,L2,T1,T2] = BZ[S1,L1,L2,T1,T2](a,b)
  def product3[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2]): BZ[S1,L1,L2,T1,T2] = BZ[S1,L1,L2,T1,T2](a,b)

  val b1: B1[String] = B1("One")
  val b2: B2[String] = B2("Two")
  val b3: B3[Int] = B3(1)

  //val p1: BX[S1, S2, String, String] = product1(b1, b2) // Ok, compilation fails
  val p2: BX[S1, S1, String, Int] = product1(b1, b3)

  // Why is S returned instead of the most specific class? To understand this, consider that type inference does
  // not include the implicit evidence. So in product2 we have `a` with L1 and `b` with L2. Because the compiler
  // does not know that L1 == L2 it returns the more general S (least general upper bound).
  //val p3: BY[S, String, String] = product2(b1, b2) // Ok, compilation fails
  val p4: BY[S, String, Int] = product2(b1, b3)

  // We can however have product that ensures that both parameters have the same type parameter, but the resulting
  // type parameter is not in any way associated with the type parameters of these function parameters. One way to
  // to add more independent type parameters for the function parameters and set the return parameter type explicitly.
  //val p5: BZ[S1, String, String] = product3(b1, b2) // Ok, compilation fails
  val p6: BZ[S1, S1, S1, String, Int] = product3(b1, b3)
}


/*
      1) Finite * Finite -> Finite
      2) Infinite * Infinite -> Infinite
      3) Finite * Infinite -> Empty
      4) Infinite * Finite -> Infinite

      Can always convert (3) to (4) and return the pair inverted
      What mus we do if we have a set of Finite / Infinite samplers?
 */

object Exp5b {

  /*
  // https://gist.github.com/retronym/341475
  case class L[A, B]() {
    def ToLub[AA >: A <: L, BB >: B <: L, L] = new { type LUB = L }
  }

  val intBoolLub = L[Int, Boolean].ToLub
  */

  sealed trait S
  sealed trait Finite extends S
  sealed trait Infinite extends S

  // Need covariance due to BY
  abstract class B[+L <: S, T]{
    def get: T
  }
  case class B1[T](x: T) extends B[Finite,T] {
    override def get: T = x
  }
  case class B2[T](x: T) extends B[Infinite,T] {
    override def get: T = x
  }
  case class B3[T](x: T) extends B[Finite,T] {
    override def get: T = x
  }
  case class BX[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2]) extends B[L1,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class BY[L<:S, T1, T2](a: B[L,T1], b:B[L,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class BZ[L<:S,L1<:S,L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }

  case class Cartesian[L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L1,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class And[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class Or[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }

  type Combine[L1<:S, L2<:S, T1, T2] = (B[L1,T1], B[L2,T2]) => B[L1,(T1,T2)]

  // Finite * Finite
  //implicit def product0[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2])(implicit ev1 : L1 =:= L2): Cartesian[L1, L2, T1, T2] =
  implicit def product0[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2]): Cartesian[L1, L2, T1, T2] =
    Cartesian(a,b)


  def combine[L1<:S, L2<:S, T1, T2](a: B[L1,T1], b:B[L2,T2])(implicit f : Combine[L1,L2,T1,T2]) = f(a,b)

  val b1: B1[String] = B1("One")
  val b2: B2[String] = B2("Two")
  val b3: B3[Int] = B3(1)
  val b4: B2[Int] = B2(2)

  // https://gist.github.com/retronym/341475

  val p0: Cartesian[Finite, Finite, String, Int] = Cartesian(b1,b3)
  val p1: Cartesian[Finite, Finite, String, Int] = product0(b1,b3)
  val p2: B[Finite, (String, Int)] = combine(b1, b3)(product0)
  val p3: B[Finite, (String, Int)] = combine(b1, b3)

  val q0: Cartesian[Infinite, Infinite, String, Int] = Cartesian(b2,b4)
  val q1: Cartesian[Infinite, Infinite, String, Int] = product0(b2,b4)
  val q2: B[Infinite, (String, Int)] = combine(b2, b4)(product0)
  val q3: B[Infinite, (String, Int)] = combine(b2, b4)

}

object Exp5d {

  sealed trait S
  sealed trait Finite extends S
  sealed trait Infinite extends S
  object Fin extends Finite
  object Inf extends Infinite

  abstract class B[L <: S, T]{
    def get: T
  }
  case class B1[T](x: T) extends B[Finite,T] {
    override def get: T = x
  }
  case class B2[T](x: T) extends B[Infinite,T] {
    override def get: T = x
  }

  case class Cartesian[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class And[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class Or[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }

  def cartesian[O<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b: B[L2,T2], c:O): Cartesian[O, L1, L2, T1, T2] = Cartesian[O,L1,L2,T1,T2](a,b)
  def and[O<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b: B[L2,T2], c:O): And[O, L1, L2, T1, T2] = And[O,L1,L2,T1,T2](a,b)
  def or[O<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b: B[L2,T2], c:O): Or[O, L1, L2, T1, T2] = Or[O,L1,L2,T1,T2](a,b)

  val b1: B1[String] = B1("One") // Finite
  val b2: B2[String] = B2("Two") // Infinite
  val b3: B1[Int] = B1(1)
  val b4: B2[Int] = B2(2)

  // Grid search
  val p0: Cartesian[Finite,Finite, Finite, String, Int] = Cartesian(b1, b3)
  val p1: Cartesian[Infinite, Infinite, Infinite, String, Int] = Cartesian(b2, b4)

  val p3: Cartesian[Finite, Finite, Finite, String, Int] = cartesian(b1,b3,Fin)
  val p4: Cartesian[Infinite, Infinite, Infinite, String, Int] = cartesian(b2,b4,Inf)
  val p5: Cartesian[Infinite, Finite, Infinite, String, Int] = cartesian(b1,b4,Inf)
  val p6: Cartesian[Infinite, Infinite, Finite, String, Int] = cartesian(b2,b3,Inf)
}


object Exp5c {

  abstract class B[T]{
    def get: T
  }
  case class B1[T](x: T) extends B[T] {
    override def get: T = x
  }
  case class B2[T](x: T) extends B[T] {
    override def get: T = x
  }

  case class Cartesian[T1,T2](a: B[T1], b:B[T2]) extends B[(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }

  sealed trait Param[T]{
    def toSampler: B[T]
  }
  final case class Single[T](e: T) extends Param[T] {
    override def toSampler: B[T] = B1(e)
  }
  final case class Composite[T1,T2](a: Param[T1], b: Param[T2]) extends Param[(T1,T2)] {
    override def toSampler: B[(T1,T2)] = Cartesian(a.toSampler, b.toSampler)
  }

  def func1[T1,T2](p1:Param[T1], p2:Param[T2], v: Int): B[_]= v match {
    case 1 => p1.toSampler
    case 2 => p2.toSampler
    case 3 =>
      val rr: Cartesian[T1, T2] = Cartesian(p1.toSampler, p2.toSampler)
      rr
  }

  import scala.language.implicitConversions

  // http://www.oweinreese.com/blog/2016/1/10/type-recursion-and-functors
  implicit def convert0[T](s: Single[T]): B[T] = s.toSampler
  implicit def convert1[T1,T2](s: Composite[T1,T2]): B[(T1,T2)] = s.toSampler

  def convert[P<:Param[_],T](p:P)(implicit f: P => B[_]) : B[_] = f(p)

  /*
  def func2[T1,T2](p1:Param[T1], p2:Param[T2], v: Int): B[_] = v match {
    case 1 => p1.toSampler
    case 2 => p2.toSampler
    case 3 =>
      val p11 = func2(p1,p2,1)
      val p12 = func2(p1,p2,2)
      val rr: Cartesian[T1, T2] = Cartesian(p11,p12)
      rr
  }
  */

  val b1: B1[String] = B1("One")
  val b2: B2[String] = B2("Two")
  val b3: B1[Int] = B1(1)
  val b4: B2[Int] = B2(2)

  val p1: Single[String] = Single(b1.x)
  val p2: Single[String] = Single(b2.x)
  val p3: Single[Int] = Single(b3.x)
  val p4: Single[Int] = Single(b4.x)

  val t1: B[_] = func1(p1,p3,1)
  val t2: B[_] = func1(p1,p3,2)
  val t3: B[_] = func1(p1,p3,3)

  val i1: B[String] = convert0(p1)
  val i2: B[String] = convert0(p2)
  val i3: B[Int] = convert0(p3)
  val i4: B[Int] = convert0(p4)

  val c1: Composite[String, String] = Composite(p1,p2)
  val c2: Composite[Int, Int] = Composite(p3,p4)
  val c3: Composite[String, Int] = Composite(p1,p4)

  val i5: B[_] = convert(p1)(convert0)
  val i6: B[_] = convert(c1)(convert1)
  val i7: B[_] = convert(p1)
  val i8: B[_] = convert(c1)

  /*
  def main(args: Array[String]) {
    println(s"t1 = $t1") // t1 = B1(One)
    println(s"t2 = $t2") // t2 = B1(1)
    println(s"t3 = $t3") // t3 = Cartesian(B1(One),B1(1))
    println(s"i7 = $i7") //
    println(s"i8 = $i8") //
  }*/
}

/*
object Exp6 {

  sealed trait S
  sealed trait Finite extends S
  sealed trait Infinite extends S
  object Fin extends Finite
  object Inf extends Infinite

  abstract class B[L <: S, T]{
    type LN <: L
    def get: T
  }
  case class B1[T](x: T) extends B[Finite,T] {
    override def get: T = x
  }
  case class B2[T](x: T) extends B[Infinite,T] {
    override def get: T = x
  }

  case class AppendB[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class Cartesian[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class And[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }
  case class Or[L<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b:B[L2,T2]) extends B[L,(T1,T2)] {
    override def get: (T1,T2) = (a.get,b.get)
  }

  def append[O<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b: B[L2,T2], c:O): AppendB[O, L1, L2, T1, T2] = AppendB[O,L1,L2,T1,T2](a,b)
  def cartesian[O<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b: B[L2,T2], c:O): Cartesian[O, L1, L2, T1, T2] = Cartesian[O,L1,L2,T1,T2](a,b)
  def and[O<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b: B[L2,T2], c:O): And[O, L1, L2, T1, T2] = And[O,L1,L2,T1,T2](a,b)
  def or[O<:S,L1<:S,L2<:S,T1,T2](a: B[L1,T1], b: B[L2,T2], c:O): Or[O, L1, L2, T1, T2] = Or[O,L1,L2,T1,T2](a,b)


  val b1: B1[String] = B1("One") // Finite
  val b2: B2[String] = B2("Two") // Infinite
  val b3: B1[Int] = B1(1)
  val b4: B2[Int] = B2(2)

  // Grid search
  val p0: Cartesian[Finite,Finite, Finite, String, Int] = Cartesian(b1, b3)
  val p1: Cartesian[Infinite, Infinite, Infinite, String, Int] = Cartesian(b2, b4)

  val p3: Cartesian[Finite, Finite, Finite, String, Int] = cartesian(b1,b3,Fin)
  val p4: Cartesian[Infinite, Infinite, Infinite, String, Int] = cartesian(b2,b4,Inf)
  val p5: Cartesian[Infinite, Finite, Infinite, String, Int] = cartesian(b1,b4,Inf)
  val p6: Cartesian[Infinite, Infinite, Finite, String, Int] = cartesian(b2,b3,Inf)

  sealed trait Param[T]{
    def toSampler: B[_, T]
  }
  final case class Composite[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)] {
    override def toSampler: B[_,(T1,T2)] = ???
  }
  final case class Splice[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)] {
    override def toSampler: B[_,(T1,T2)] = ???
  }
  final case class Combine[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)] {
    override def toSampler: B[_,(T1,T2)] = ???
  }
  final case class ValidRange[T](start: T, end: T, delta: T) extends Param[T] {
    override def toSampler: B[_,T] = ???
  }
  final case class Options[T](e: Seq[T]) extends Param[T] {
    override def toSampler: B[_,T] = ???
  }

  def grid(p:Param[_]): B[_,_]= p match {
    case r@ValidRange(_,_,_) => r.toSampler
    case r@Options(_) => r.toSampler
    case Composite(a,b) =>
      val aa = grid(a)
      val bb = grid(b)
      val rr: Cartesian[Finite, Any, Any, Any, Any] = cartesian(aa, bb, Fin)
      rr
  }

  // Get each parameter from the algorithm
  // Assign a sampler to each parameter (discrete, random or function. Function can be combined with discrete or random)
  val kernels = Options(List("0","1"))
  val kernel0_p1 = ValidRange(0,1,0.01)
  val kernel0_p2 = ValidRange(1,10,1)
  val kernel1_p1 = ValidRange(0,100,10)
  val kernel1_p2 = ValidRange(1,10,1)
  val kernel0 = Composite(kernel0_p1, kernel0_p2)
  val kernel1 = Composite(kernel1_p1, kernel1_p2)
  val kernelsx = Splice(kernel0, kernel1)

}
*/

object Exp7a {

  sealed trait Sample[T]
  case class Limits[T](a:T) extends Sample[T]
  case class Listing[T](a:Seq[T]) extends Sample[T]
  case class Cart[T1,T2](a:Sample[T1],b:Sample[T2]) extends Sample[(T1,T2)]

  def linear[T](r : Range[T], delta : T): Sample[T] = Limits(r.start)
  //def linear[T](r : ValidRange[T], delta : T)(implicit num: Numeric[T]): Sample[T] = Limits(r.start)
  def list[T](l : Options[T]): Listing[T] = Listing(l.e)

  def cartesian[T1, T2](a: Sample[T1], b: Sample[T2]): Cart[T1, T2] = Cart[T1, T2](a, b)


  sealed trait Param[T]
  final case class Range[T](start: T, end: T, delta: T) extends Param[T]
  final case class Options[T](e: Seq[T]) extends Param[T]
  final case class Composite[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)]

  val p1 = Range(1, 10, 1)
  val p2 = Options(List("a", "b", "c"))
  val p3: Composite[Int, String] = Composite(p1, p2)

  def gridSearch[T](p: Param[T]): Sample[T] = p match {
    case g@Range(_, _, delta) =>
      val r: Sample[T] = linear(g, delta)
      r
    case l@Options(_) =>
      val r = list(l)
      r
    case Composite(paramA, paramB) =>
      val a = gridSearch(paramA)
      val b = gridSearch(paramB)
      val c = cartesian(a, b)
      c
  }

  val s1: Sample[Int] = gridSearch(p1)
  val s2: Sample[String] = gridSearch(p2)
  val s3: Sample[(Int, String)] = gridSearch(p3)

}


object Exp7b {

  sealed trait S
  sealed trait Finite extends S
  sealed trait Infinite extends S
  object Fin extends Finite { override def toString: String = "Finite" }
  object Inf extends Infinite { override def toString: String = "Infinite" }

  sealed trait Sample[T]{
    val len : S
  }
  case class Limits[T](a:T) extends Sample[T]{override val len: S = Fin}
  case class Listing[T](a:Seq[T]) extends Sample[T]{override val len: S = Fin}
  case class Cart[T1,T2](a:Sample[T1],b:Sample[T2]) extends Sample[(T1,T2)]{override val len: S = Fin}

  def linear[T](r : Range[T], delta : T): Sample[T] = Limits(r.start)
  //def linear[T](r : ValidRange[T], delta : T)(implicit num: Numeric[T]): Sample[T] = Limits(r.start)
  def list[T](l : Options[T]): Listing[T] = Listing(l.e)
  def cartesian[T1, T2](a: Sample[T1], b: Sample[T2]): Cart[T1, T2] = Cart[T1, T2](a, b)

  case class ILimits[T](a:T) extends Sample[T]{override val len: S = Inf}
  case class IListing[T](a:Seq[T]) extends Sample[T]{override val len: S = Inf}
  case class ICart[T1,T2](a:Sample[T1],b:Sample[T2]) extends Sample[(T1,T2)]{override val len: S = Inf}

  def ilinear[T](r : Range[T], delta : T): Sample[T] = ILimits(r.start)
  def ilist[T](l : Options[T]): IListing[T] = IListing(l.e)
  def icartesian[T1, T2](a: Sample[T1], b: Sample[T2]): ICart[T1, T2] = ICart[T1, T2](a, b)

  sealed trait Param[T]
  final case class Range[T](start: T, end: T, delta: T) extends Param[T]
  final case class Options[T](e: Seq[T]) extends Param[T]
  final case class Composite[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)]

  val p1: Range[Int] = Range(1, 10, 1)
  val p2: Options[String] = Options(List("a", "b", "c"))
  val p3: Composite[Int, String] = Composite(p1, p2)

  def gridSearch[T](p: Param[T]): Sample[T] = p match {
    case g@Range(_, _, delta) =>
      val r: Sample[T] = linear(g, delta)
      r
    case l@Options(_) =>
      val r = list(l)
      r
    case Composite(paramA, paramB) =>
      val a = gridSearch(paramA)
      val b = gridSearch(paramB)
      val c = cartesian(a, b)
      c
  }

  val s1: Sample[Int] = gridSearch(p1)
  val s2: Sample[String] = gridSearch(p2)
  val s3: Sample[(Int, String)] = gridSearch(p3)
  val s4: Sample[(Int, (Int, String))] = gridSearch(Composite(p1,p3))

  def guassianSearch[T](p: Param[T]): Sample[T] = p match {
    case g@Range(_, _, delta) =>
      val r: Sample[T] = ilinear(g, delta)
      r
    case l@Options(_) =>
      val r = ilist(l)
      r
    case Composite(paramA, paramB) =>
      val a = guassianSearch(paramA)
      val b = guassianSearch(paramB)
      val c = icartesian(a, b)
      c
  }

  val s5: Sample[Int] = guassianSearch(p1)
  val s6: Sample[String] = guassianSearch(p2)
  val s7: Sample[(Int, String)] = guassianSearch(p3)
  val s8: Sample[(Int, (Int, String))] = gridSearch(Composite(p1,p3))

  def mixedSearch[T](p: Param[T]): Sample[T] = p match {
    case g@Range(_, _, delta) =>
      val r: Sample[T] = ilinear(g, delta)
      r
    case l@Options(_) =>
      val r = list(l)
      r
    case Composite(paramA, paramB) =>
      val a = mixedSearch(paramA)
      val b = mixedSearch(paramB)
      val c = (a.len, b.len) match {
        case (Fin, Fin) => cartesian(a, b)
        case (Inf, Inf) => icartesian(a, b)
        case _ => icartesian(a, b)
      }
      c
  }

  val s9: Sample[(Int, (Int, String))] = mixedSearch(Composite(p1,p3))
/*
  def main(args: Array[String]) {
    println("-------------------------------------")
    println(s4)
    println(s8)
    println(s9)
  }
*/
}

/*
// http://www.cakesolutions.net/teamblogs/ways-to-pattern-match-generic-types-in-scala
// https://stackoverflow.com/questions/20125975/scala-match-type-argument-for-an-object
// https://stackoverflow.com/questions/20125975/scala-match-type-argument-for-an-object
object Exp7c {

  sealed trait Sampler[T]
  case class IntUser(s:Int,e:Int) extends Sampler[Int]
  case class DoubleUser(s:Double,e:Double) extends Sampler[Double]
  case class Cartesian[T1,T2](a:Sampler[T1], b:Sampler[T2]) extends Sampler[(T1,T2)]

  sealed trait Param[T]
  final case class ValidRange[T](start: T, end: T, delta: T) extends Param[T]
  final case class Options[T](e: Seq[T]) extends Param[T]
  final case class Composite[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)]

  val p1: ValidRange[Int] = ValidRange(1, 10, 1)
  val p2: Options[String] = Options(List("a", "b", "c"))
  val p3: Composite[Int, String] = Composite(p1, p2)

  def useInt(p : ValidRange[Int]): Sampler[Int] = IntUser(p.start, p.end)
  def useDouble(p : ValidRange[Double]): Sampler[Double] = DoubleUser(p.start, p.end)

  def process[T](p: Param[T]): Sampler[T] = p match {
    case g@ValidRange(_:Int, _:Int, delta:Int) =>
      val r = useInt(g)
      r
    case g@ValidRange(_:Double, _:Double, delta:Double) =>
      val r = useDouble(g)
      r
    case c@Composite(paramA, paramB) =>
      val a = process(paramA)
      val b = process(paramB)
      val c = Cartesian(a, b)
      c
  }
}
*/

object Exp7d {

  // Generates the samples
  sealed trait Sampler[+T]
  case class IntUser(s:Int,e:Int) extends Sampler[Int]
  case class DoubleUser(s:Double,e:Double) extends Sampler[Double]
  case class StringUser(e:Seq[String]) extends Sampler[String]
  case class Cartesian[T1,T2](a:Sampler[T1], b:Sampler[T2]) extends Sampler[(T1,T2)]

  // Search space constraints
  sealed trait Param[T]
  // Base - causes problems with "match may not be exhaustive"
  // class ValidRange[T] protected (val start: T, val end: T, val delta: T) extends Param[T]
  // class Options[T] protected (val e: Seq[T]) extends Param[T]
  // Combinators
  final case class Composite[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)]

  // Typed base
  final case class IntegerRange(start: Int, end: Int, delta: Int) extends Param[Int]
  final case class DoubleRange(start: Double, end: Double, delta: Double) extends Param[Double]
  final case class StringOptions(e: Seq[String]) extends Param[String]

  val p1 = IntegerRange(1, 10, 1)
  val p2 = StringOptions(List("a", "b", "c"))
  val p3: Composite[Int, String] = Composite(p1, p2)

  def useInt(p : IntegerRange): Sampler[Int] = IntUser(p.start, p.end)
  def useDouble(p : DoubleRange): Sampler[Double] = DoubleUser(p.start, p.end)
  def useStringOptions(p : StringOptions): Sampler[String] = StringUser(p.e)

  def process[T](p: Param[T]): Sampler[T] = p match {
    case g@IntegerRange(_, _, delta) => useInt(g)
    case g@DoubleRange(_, _, delta) => useDouble(g)
    case o@StringOptions(_) => useStringOptions(o)
    case c@Composite(paramA, paramB) =>
      val a = process(paramA)
      val b = process(paramB)
      val c = Cartesian(a, b)
      c
  }

  val s1: Sampler[Int] = process(p1)
  val s2: Sampler[String] = process(p2)
  val s3: Sampler[(Int, String)] = process(p3)
  val s4: Sampler[(Int, (Int, String))] = process(Composite(p1,p3))
  /*
  def main(args: Array[String]) {
      println("-------------------------------------")
      println(s1)
      println(s2)
      println(s3)
      println(s4)
    }
    */
}


object Exp7f {

  def intRange(r : IntRange, numSamples : Int): RangeOf[Int] = {
    val delta = (r.end - r.start) / numSamples.toDouble
    RangeOf(r.start, r.end, Math.floor(delta).toInt)
  }
  def doubleRange(r : DoubleRange, numSamples : Int): RangeOf[Double] = {
    val delta = (r.end - r.start) / numSamples.toDouble
    RangeOf(r.start, r.end, delta)
  }
  def const[T](r : Constant[T]): Single[T] = Single[T](r.c)
  def items[T](l : Items[T]): ListRange[T] = ListRange(l.e)
  def cartesian[L <: LengthSample, T1, T2](a: Sampler[L,T1], b: Sampler[L,T2]): Cartesian[L, T1, T2] =
    Cartesian[L, T1, T2](a, b)

  def uniformIntRange(r : IntRange): IntUniform = IntUniform(r.start, r.end)
  def uniformDoubleRange(r : DoubleRange): Uniform = Uniform(r.start, r.end)
  def uniformItems[T](l : Items[T]): DiscreteUniform[T] = DiscreteUniform(l.e)
  def uniformConst[T](r : Constant[T]): Const[T] = Const[T](r.c)
  def and[L <: LengthSample, T1, T2](a: Sampler[L,T1], b: Sampler[L,T2]): And[L, T1, T2] = And[L, T1, T2](a, b)

  sealed trait Param[T]
  final case class Constant[T](c: T) extends Param[T]
  final case class Items[T](e: List[T]) extends Param[T]
  final case class Composite[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)]
  final case class PairThese[T1,T2](l: Seq[(Param[T1],Param[T2])]) extends Param[(T1,T2)]
  final case class IntRange(start: Int, end: Int) extends Param[Int]
  final case class DoubleRange(start: Double, end: Double) extends Param[Double]

  val p1: Param[Int] = IntRange(1, 10)
  val p2: Param[String] = Items(List("a", "b", "c"))
  val p3: Composite[Int, String] = Composite(p1, p2)

  val q1 = Constant("kernel 1")
  val q2 = Constant("kernel 2")
  val q3 = Constant("kernel 3")
  val q4 = Items(List("k1p1", "k1p2", "p3"))
  val q5 = Items(List("k2p1", "k2p2", "p3"))
  val q6 = Items(List("k3p1", "k3p2", "k3p4"))
  val q7: Seq[(Constant[String], Items[String])] = List((q1,q4), (q2,q5), (q3,q6))
  val q8: PairThese[String, String] = PairThese(q7)

  val numSamples = 9

  def gridSearch[L <: LengthSample, T](numSamples: Int)(p: Param[T]): Sampler[Finite, T] = p match {
    case r@Constant(_) => const(r)
    case r@IntRange(_, _) => intRange(r,numSamples)
    case r@DoubleRange(_, _) => doubleRange(r,numSamples)
    case l@Items(_) => items(l)
    case c@Composite(paramA, paramB) =>
      val a = gridSearch(numSamples)(paramA)
      val b = gridSearch(numSamples)(paramB)
      val c = cartesian(a, b)
      c
    case c@PairThese(prs) =>
      val r1= prs.map { e =>
        val a = gridSearch(numSamples)(e._1)
        val b = gridSearch(numSamples)(e._2)
        cartesian(a, b)
      }
      val r2 = r1.toVector
      val r3 = AppendB(r2)
      r3
  }

  val s1: Sampler[Finite, Int] = gridSearch(numSamples)(p1)
  val s2: Sampler[Finite, String] = gridSearch(numSamples)(p2)
  val s3: Sampler[Finite, (Int, String)] = gridSearch(numSamples)(p3)
  val s4: Sampler[Finite, (Int, (Int, String))] = gridSearch(numSamples)(Composite(p1,p3))
  val s5: Sampler[Finite, (String, String)] = gridSearch(numSamples)(q8)

  def uniformSearch[T](p: Param[T]): Sampler[Infinite,T] = p match {
    case r@Constant(_) => uniformConst(r)
    case r@IntRange(_, _) => uniformIntRange(r)
    case r@DoubleRange(_, _) => uniformDoubleRange(r)
    case l@Items(_) => uniformItems(l)
    case c@Composite(paramA, paramB) =>
      val a = uniformSearch(paramA)
      val b = uniformSearch(paramB)
      val c = and(a, b)
      c
    case c@PairThese(prs) =>
      val r1 = prs.map { e =>
        val a = uniformSearch(e._1)
        val b = uniformSearch(e._2)
        and(a, b)
      }
      val r2 = r1.toVector
      val r3 = AppendU(r2)
      r3
  }

  val s6: Sampler[Infinite, Int] = uniformSearch(p1)
  val s7: Sampler[Infinite, String] = uniformSearch(p2)
  val s8: Sampler[Infinite, (Int, String)] = uniformSearch(p3)
  val s9: Sampler[Infinite, (Int, (Int, String))] = uniformSearch(Composite(p1,p3))
  val s10: Sampler[Infinite, (String, String)] = uniformSearch(q8)

  /*
  def main(args: Array[String]) {
    println("-------------------------------------")
    println(s1)
    println(s2)
    println(s3)
    println(s4)
    println(s5)

    //val v1 = s1.toList
    //println(v1.mkString(","))

    //val v2 = s2.toList
    //println(v2.mkString(","))

    val v3 = s3.toList
    println(v3.mkString(","))

    val v5 = s5.toList
    println(v5.mkString(","))

    println("......................................")
    println(s6)
    println(s7)
    println(s8)
    println(s9)

    val v8 = s8.toStream.take(30)
    println(v8.mkString(","))

    val v10 = s10.toStream.take(20)
    println(v10.mkString(","))
  }*/

}

object TypeSafeParameterSearch {
  sealed trait Param[T]
  final case class Constant[T](c: T) extends Param[T]
  final case class Items[T](e: List[T]) extends Param[T]
  final case class Composite[T1,T2](paramA: Param[T1], paramB: Param[T2]) extends Param[(T1,T2)]
  final case class PairThese[T1,T2](l: Seq[(Param[T1],Param[T2])]) extends Param[(T1,T2)]
  final case class IntRange(start: Int, end: Int) extends Param[Int]
  final case class DoubleRange(start: Double, end: Double) extends Param[Double]


  def intRange(r : IntRange, numSamples : Int): RangeOf[Int] = {
    val delta = (r.end - r.start) / numSamples.toDouble
    RangeOf(r.start, r.end, Math.floor(delta).toInt)
  }
  def doubleRange(r : DoubleRange, numSamples : Int): RangeOf[Double] = {
    val delta = (r.end - r.start) / numSamples.toDouble
    RangeOf(r.start, r.end, delta)
  }
  def const[T](r : Constant[T]): Single[T] = Single[T](r.c)
  def items[T](l : Items[T]): ListRange[T] = ListRange(l.e)
  def cartesian[L <: LengthSample, T1, T2](a: Sampler[L,T1], b: Sampler[L,T2]): Cartesian[L, T1, T2] =
    Cartesian[L, T1, T2](a, b)

  def gridSearch[L <: LengthSample, T](numSamples: Int)(p: Param[T]): Sampler[Finite, T] = p match {
    case r@Constant(_) => const(r)
    case r@IntRange(_, _) => intRange(r,numSamples)
    case r@DoubleRange(_, _) => doubleRange(r,numSamples)
    case l@Items(_) => items(l)
    case c@Composite(paramA, paramB) =>
      val a = gridSearch(numSamples)(paramA)
      val b = gridSearch(numSamples)(paramB)
      val c = cartesian(a, b)
      c
    case c@PairThese(prs) =>
      val r1= prs.map { e =>
        val a = gridSearch(numSamples)(e._1)
        val b = gridSearch(numSamples)(e._2)
        cartesian(a, b)
      }
      val r2 = r1.toVector
      val r3 = AppendB(r2)
      r3
  }

  /**
    * Base class that represent all algorithms. Note that the Mode `Model` is an abstract class.
    * This means that the type is opaque to all users of the any extending Algorithms. This
    * means that the model is a dependent type: one cannot use a model generated by one
    * algorithm (event of the the same type). NOTE: this means that the algorithm is responsible
    * for saving and loading the model.
    *
    * @tparam I
    * @tparam O
    */
  trait Algorithm[I,O] {
    abstract class M // Model
    type P <: Param[_]
    type V

    val params: P

    def fit(data: I, params: V): M
    def predict(data: I, m: M): O
  }

  case class Algo1 () extends Algorithm[Int, Double] {
    case class MM(v:Double) extends M
    override type P = PairThese[String, String]
    override type V = (String, String)

    type PP = PairThese[String, String]
    type VV = (String, String)

    val q1 = Constant("kernel 1")
    val q2 = Constant("kernel 2")
    val q3 = Constant("kernel 3")
    val q4 = Items(List("k1p1", "k1p2", "p3"))
    val q5 = Items(List("k2p1", "k2p2", "p3"))
    val q6 = Items(List("k3p1", "k3p2", "k3p4"))
    val q7: Seq[(Constant[String], Items[String])] = List((q1,q4), (q2,q5), (q3,q6))
    val q8 = PairThese(q7)

    override val params: PairThese[String, String] = q8

    override def fit(data: Int, params: (String,String)): M = MM(data.toDouble * 1.3)
    override def predict(data: Int, m: M): Double = data.toDouble * 1.3
  }

  object Algorithms {
    def algo1 = Algo1()
  }


  def main(args: Array[String]): Unit = {
    val numSamples = 9
    val a1 = Algorithms.algo1
    val a2 = Algorithms.algo1
    val ps1: Sampler[Finite, (String, String)] = gridSearch(numSamples)(a1.params)
    val pa1: List[(String, String)] = ps1.toList
    val m1: a1.M = a1.fit(0,pa1(0))
    val m2: a2.M = a2.fit(0,pa1(0))
    //val m1: Double = a1.fit(0,pa1(0))
    val p1: Double = a1.predict(0,m1)
    println(p1)
    val p2: Double = a2.predict(0,m2)
    println(p2)
    //val p3: Double = a2.predict(0,m1) // compilation fails as required
    //println(p3)
  }

}


/*
object TypeSafeParameterSearch {

  trait Parameters extends Product

  sealed trait Param[T]
  // TODO: add name and description to each
  //final case class SetOf(subParams:Params) extends Param
  //final case class Composite(param: Param, subParams: SetOf) extends Param
  final case class Composite[T1,T2](param: Param[T1], subParams: Param[T2]) extends Param[(T1,T2)]
  final case class ValidRange[T](start: T, end: T) extends Param[T]
  final case class Options[T](e: Seq[T]) extends Param[T]

  sealed abstract class Params
  final case class ::[+H <: Param[_],+T <: Params](h: H, t:T) extends Params {
    def head : H = h
    def tail : T = t
    def :#:[A <: Param[_]](e: A): ::[A, ::[H, T]] = ::(e,this)
  }
  sealed trait End extends Params {
    def head = throw new RuntimeException("Not implementable")
    def tail = throw new RuntimeException("Not implementable")
    def :#:[A <: Param[_]](e: A): ::[A, End] = ::(e,this)
  }

  case object End extends End

  val t0: ::[ValidRange[Double], End] = ValidRange(0.0,1.5) :#: End
  val t1: ::[ValidRange[Int], ::[ValidRange[Double], End]] = ValidRange(0,10) :#: t0
  val t2: ::[Options[String], ::[ValidRange[Int], ::[ValidRange[Double], End]]] = Options(List("a", "b", "c")) :#:  t1
  val e1: Options[String] = t2.head
  val e2: ::[ValidRange[Int], ::[ValidRange[Double], End]] = t2.tail

  object SearchHelpers {
    def linear[T](r : ValidRange[T], delta : T)(implicit num: Numeric[T]) = {
      RangeSampler[T](r.start, r.end, delta)
    }
    def list[T](l : Options[T]) = ListRange(l.e)
/*
    def grid[T,T1,T2](p:Param[T], delta: T)(implicit num: Numeric[T]) :  Sampler[_,T]= p match {
      case r@ValidRange(_,_) => linear(r,delta)
      case r@Options(_) => list(r)
      case r@Composite(a,b) =>
        val aa: Sampler[_, T1] = grid(a, delta)
        val bb: Sampler[_, T2] = grid(b, delta)
        CartesianProduct[_,T1,T2](aa, bb)
    }
*/
  }

  /*

    The example below shows how to avoid the ability to directly instantiate a concrete algorithm.
    This allows us to make the type parameters abstract and therefore dependent types. The result is
    tha we can only provide a given model to the specific predict function and the correct set of
    parameters to the fit function.

    see https://stackoverflow.com/questions/38097490/scala-case-class-private-constructor-isnt-private
   */

  trait Algorithm[I,O] {
    type Model // Model
    type Parameters <: Params
    type Parameters

    val search: Parameters

    def fit(data: I, search: Parameters): Model
    def predict(data: I, m: Model): O
  }

  case class MyAlgorithm private () extends Algorithm[Int, Double] {
    override type Model = Double
    override type Parameters = ::[Options[String], ::[ValidRange[Int], ::[ValidRange[Double], End]]]
    override type Parameters = (String, (Int, Double))

    override val search: Parameters = Options(List("a", "b", "c")) :#: ValidRange(0,10) :#: ValidRange(0.0,1.5) :#: End


    override def fit(data: Int, search: Parameters): Model = data.toDouble * 1.3
    override def predict(data: Int, m: Model): Double = data.toDouble * 1.3
  }

  object MyAlgorithm {
    private def apply(): MyAlgorithm = new MyAlgorithm()
    def create: MyAlgorithm = new MyAlgorithm()
  }

  object Algorithm {
    def myAlgorithm: MyAlgorithm /*Algorithm[Int,Double]*/ = MyAlgorithm.create // MyAlgorithm()
  }

  // Won't compile due to private and override of the case apply
  // val alg0 = MyAlgorithm()
  val alg1 = Algorithm.myAlgorithm // MyAlgorithm()
  val alg2: Algorithm[Int, Double] = Algorithm.myAlgorithm // MyAlgorithm()
  val tp1: Options[String] = alg1.search.head
  val tp2: ValidRange[Int] = alg1.search.tail.head
  val tp3: ValidRange[Double] = alg1.search.tail.tail.head
  val pa1: ListRange[String] = SearchHelpers.list(tp1)
  val pa2: RangeSampler[Int] = SearchHelpers.linear(tp2, 1)
  val pa3: RangeSampler[Double] = SearchHelpers.linear(tp3, 0.001)
  val grid1: CartesianProduct[Base.Finite, String, (Int, Double)] = CartesianProduct(pa1, CartesianProduct(pa2, pa3))
  val pas1: (String, (Int, Double)) = grid1.toStream.toIterator.next
  val m1 = alg1.fit(1, pas1)
  val p1 = alg1.predict(1, m1)
  // Compilation fails as required
  //val p2 = alg2.predict(1, m1)
  // Bypassing the dependent type check: if we create a concrete type
  // then we have access/know the concrete parameter type and can therefor
  // create this and pass it on to the predict call
  //val p3 = alg0.predict(1, 0.1)
  // Passing the general (abstract) type will not work,
  // fails wth compilation even when alg0 is available
  //val p4 = alg0.predict(1, m1)


  //class Person private (name: String)
  //val p = new Person("Mercedes")

  //class Person private ()
  //val p = new Person()

  //case class Person private ()
  //val p = new Person()

  /*
  trait Primate[X]{
    type Gender

    def getGender : Gender
  }
  case class Person private () extends Primate[Double]{
    override type Gender = Boolean

    def getGender : Gender = ???
  }
  val p = new Person()*/

  // Hiding the case class apply to prevent construction
  /*
  case class Bar private (value: String)
  object Bar {
    private def apply(v: String) = new Bar(v) // makes the method private
    def create(value: String): Bar = new Bar(value)
  }

  val bar0 = Bar("abc") // compile error
  val bar1 = new Bar("abc") // compile error
  val bar2 = Bar.create("abc") // ok
*/

  /*
    Using type parameters that are overridden is not "safe". If we use the constructor
    or return the class instance, then dependent types will not work. We must the:
    a) make the constructor private
    b) provide a companion object that returns the base trait
    http://nieradzik.me/path-dependent-types.html

    Alternatively use abstract inner classes
    http://danielwestheide.com/blog/2013/02/13/the-neophytes-guide-to-scala-part-13-path-dependent-types.html
   */

  /* http://nieradzik.me/path-dependent-types.html */
  trait Client {
    type Connection

    def connect: Connection
    def disconnect(connection: Connection): Unit
  }

  case class ClientOps() extends Client {
    override type Connection = Int

    override def connect: Connection = 42
    override def disconnect(connection: Connection): Unit = {}
  }

  // IMPORTANT: when using type values we must return the base trait
  object Client {
    def apply(): Client = new ClientOps
  }

  // Otherwise compilation will not fail
  val client  = ClientOps() // Client()
  val client2 = ClientOps() // Client()

  val connection: client.Connection = client.connect

  client.disconnect(connection)
  client2.disconnect(connection)

  // As it should be
  val client3  = Client()
  val client4 = Client()

  // Compilation fails
  //client3.disconnect(connection)
  //client4.disconnect(connection)

  // This is an alternate solution that does NOT require hiding the constructor
  // and explicitly returning a base trait

  trait ClientB {
    abstract class Connection(){
      type T
    }

    def connect: Connection
    def disconnect(connection: Connection): Unit
  }

  case class ClientOpsB() extends Client {
    class Connection(v: Connection#T) {
      type T = Int
    }

    override def connect: Connection = new Connection(42)
    override def disconnect(connection: Connection): Unit = {}
  }

  val clientB  = ClientOpsB()
  val clientB2 = ClientOpsB()

  val connectionb: clientB.Connection = clientB.connect

  clientB.disconnect(connectionb)
  // Compilation fails:
  // [error] /home/hmf/git/adw/src/main/scala/pt/inescn/scratchpad/TypeSafeParameterSearch.scala:155: type mismatch;
  // [error]  found   : pt.inescn.scratchpad.TypeSafeParameterSearch.clientB.Connection
  // [error]  required: pt.inescn.scratchpad.TypeSafeParameterSearch.clientB2.Connection
  //clientB2.disconnect(connectionb)

  val connectiona = new clientB.Connection(100)

  clientB.disconnect(connectiona)
  // Compilation fails
  // clientB2.disconnect(connectiona)


}
*/