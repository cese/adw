package pt.inescn.scratchpad

/**
  * runMain pt.inescn.scratchpad.OpiTest
  */
object OpiTest {


  trait Monoid[T] {
    def unit: T
    def op(a:T, b:T) : T
  }
  object Monoids {
    implicit object stringMonoid extends Monoid[String] {
      def op(x: String, y: String): String = if (x=="") y
      else if (y == "")
        x
      else
        x + "," + y
      def unit: String = ""
    }
    implicit object intMonoid extends Monoid[Int] {
      def op(x: Int, y: Int): Int = x + y
      def unit: Int = 0
    }
  }

  sealed trait Opi[A,P,I,O]
  //case class NoOpi[A,P1,I,O](s:String) extends Opi[A,P1,I,O]
  case class NoOpi[A,P1,I](s:String) extends Opi[A,P1,I,String]
  case class Funci[A,P,I,O](p:P, f:(P,I) => (A,O)) extends Opi[A,P,I,O]
  case class Alli[A,P,I,O](f:List[Funci[A,P,I,O]]) extends Opi[A,P,I,O]
  case class SeqOpi[A,P,I,O,P2,O2](f1:List[Opi[A,P,I,O]], f2:List[Opi[A,P2,O,O2]]) extends Opi[A,P,I,O2]
  case class CrossOpi[A,P,I,O,P2,O2](f1:List[Opi[A,P,I,O]], f2:List[Opi[A,P2,O,O2]]) extends Opi[A,P,I,O2]

  // TODO: remove option, add as standard parameter?
  // TODO: make (A,I) a non-tuple?
  case class Iffi[A,P,I,IO,P2,O](iff:(A,I,Option[P]) => Boolean, f:Opi[A,P,I,O], zero:O) extends Opi[A,P,I,O]

  sealed case class Exec[A,I,O](fs:List[(A,I) => (A,O)])

  def compilex[A,P1,I,O1,P2,O2](o: Opi[A,P1,I,O1])(implicit m: Monoid[A]): Exec[A,I,O1] = o match {
    case e:NoOpi[A,P1,I] =>
      def func(acc0:A,i:I) = {
        (acc0, e.s)
      }
      Exec( List(func _) )
    case Funci(p,f) =>
     def func(acc0:A,i:I) = {
        val (acc1,o1) = f(p,i)
        (m.op(acc0,acc1), o1)
      }
      Exec( List(func _) )
    case Iffi(iff,Funci(p,f),z) =>
      def func(acc0:A,i:I) = {
        if (iff(acc0, i, Some(p))) {
          val (acc1, o1) = f(p,i)
          (m.op(acc0, acc1), o1)
        } else
          (acc0, z)
      }
      Exec( List(func _) )
    case Iffi(iff,f,z) =>
      val tmp0 = compilex(f)
      val tmp: List[(A,I)=>(A,O1)] = tmp0.fs.map { fi =>
        def func(acc0:A,i:I) = {
          if (iff(acc0,i,None)) {
            val (acc1,o1) = fi(acc0,i)
            (m.op(acc0,acc1), o1)
          } else
            (acc0,z)
        }
        func _
      }
      Exec( tmp )
    case Alli(fs) =>
      val tmp: List[(A,I)=>(A,O1)] = fs.map { fi =>
        def func(acc0:A,i:I) = {
          val (acc1,o1) = fi.f(fi.p,i)
          (m.op(acc0,acc1), o1)
        }
        func _
      }
      Exec( tmp )
    case SeqOpi(l1,l2) =>
      val tmp0 = l1.zip(l2).flatMap { case (f1s, f2s) =>
        val tmp1 = compilex(f1s)
        val tmp2 = compilex(f2s)
        val tmp3 = tmp1.fs.zip(tmp2.fs).map { case (f1,f2) =>
          def func(acc0:A,i:I) = {
            val (acc1,o1) = f1(acc0,i)
            val (acc2,o2) = f2(acc1,o1)
            (m.op(acc0, m.op(acc1,acc2)), o2)
          }
          func _
        }
        tmp3
      }
      Exec( tmp0 )
    case CrossOpi(l1,l2) =>
      val tmp0 = l1.flatMap { f1s =>
        l2.flatMap{ f2s =>
          val tmp1 = compilex(f1s)
          val tmp2 = compilex(f2s)
          val tmp3 = tmp1.fs.flatMap { f1 =>
            tmp2.fs.map { f2 =>
              def func(acc0:A,i:I) = {
                val (acc1,o1) = f1(acc0,i)
                val (acc2,o2) = f2(acc1,o1)
                (m.op(acc0, m.op(acc1,acc2)), o2)
              }
              func _
            }
          }
          tmp3
        }
      }
      Exec( tmp0 )
  }

  def main(args: Array[String]): Unit = {
    import Monoids._

    val func = Funci("X",(a:String,b:Int) => (a+"A",b+1))
    val funcc = compilex(func)
    println(funcc)

    val in1 = List(1,2,3)
    val r1 = in1.flatMap { i =>
      funcc.fs.map( f => f("",i) )
    }
    println(r1)
  }
}
