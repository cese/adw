package pt.inescn.scratchpad

import pt.inescn.search.stream.Pipes.All
import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.Pipes._
import pt.inescn.samplers.stream.Ops._
import pt.inescn.utils.ADWError

import scala.collection.AbstractIterator

import pt.inescn.macros.TaskMacro
// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._

/*
      val convertable = new Iterable[Row] {

        // Create the iterator using the underlying iterator to
        // generate and iterate through the windows
        override def iterator: Iterator[Row] = {
          val converter = new AbstractIterator[Row] {

            override def hasNext: Boolean = iter.hasNext

            override def next(): Row = {
              // Get the row
              val r = iter.next()
              val (nacum,fr) = op(acum,r)
              // non-mutability hidden
              acum = nacum
              fr
            }
          }
          converter
        }
      }

  implicit class toTaskP1[I,O,P1](param_func:((P1,I) => Out[O], P1)) {
    private val f = param_func._1
    private val p = param_func._2
    def T: Func[I,O,Unit] = Func( toTask1(f,p) )
  }

 */




/**
  * root/runMain pt.inescn.scratchpad.PipeSearch
  *
  * TODO: provide a lazy map (don use the Builder pattern)
  */
object PipeSearch {


  // Macro magic
  // https://github.com/milessabin/shapeless/blob/master/examples/src/main/scala/shapeless/examples/flatten.scala
  import shapeless._
  import ops.tuple.FlatMapper
  import syntax.std.tuple._

  trait LowPriorityFlatten extends Poly1 {
    implicit def default[T] = at[T](Tuple1(_))
  }
  object flatten extends LowPriorityFlatten {
    implicit def caseTuple[P <: Product](implicit lfm: Lazy[FlatMapper[P, flatten.type]]) =
      at[P](lfm.value(_))
  }

  implicit class toFlatten[P1,P2,P3](i:Iterable[((P1,P2),P3)]) {
    def F = i.map( flatten(_) )
  }

  /*
  implicit def lazyMapP3[I,O](i:Iterable[I]) {
    val convertable = new Iterable[O] {

      override def iterator: AbstractIterator[O] = {

        val converter = new AbstractIterator[O] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next() = {
            val in: I = iter1.next()
            flattenT(in)
          }
        } // iterator
        converter
      }
    }
    convertable
  }
*/

  def flattenList[P1,P2,P3](l:List[((P1,P2),P3)]): List[(P1,P2,P3)] = {
    l.map( flatten(_) )
  }

  def flattenIterable[P1,P2,P3](l:Iterable[((P1,P2),P3)]): Iterable[(P1,P2,P3)] = {
    l.map( flatten(_) )
  }


  def flattenIterableG[T](l:Iterable[T]) = {
    l.map( flatten(_) )
  }

  def flattenIterableH[T](l:Iterable[T])(implicit cse : shapeless.poly.Case[PipeSearch.flatten.type, T::HNil]):
  Iterable[PolyDefns.Case[flatten.type, T :: HNil]#Result] = {
    l.map( flatten(_)(cse) )
  }

  type InTuple[T] = shapeless.poly.Case[flatten.type, T::HNil]
  type OutTuple[T] = PolyDefns.Case[flatten.type, T :: HNil]#Result

  def flattenIterableI[T](l:Iterable[T])(implicit cse : InTuple[T]):
  Iterable[OutTuple[T]] = {
    l.map( flatten(_)(cse) )
  }


  def lazyMapP3[T](i:Iterable[T])(implicit cse : InTuple[T]) : Iterable[OutTuple[T]] = {
    val convertable = new Iterable[OutTuple[T]] {

      override def iterator: AbstractIterator[OutTuple[T]] = {

        val converter = new AbstractIterator[OutTuple[T]] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next(): OutTuple[T] = {
            val in = iter1.next()
            PipeSearch.flatten(in)
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  /*
  def flattenIterator[P1,P2,P3](l:Iterator[((P1,P2),P3)]): Iterator[(P1,P2,P3)] = {
    l.map( flatten(_) )
  }

  def lazyMapP3[P1,P2,P3](i:Iterable[((P1,P2),P3)]) {
    val convertable = new Iterable[(P1,P2,P3)] {

      override def iterator: AbstractIterator[(P1,P2,P3)] = {

        val converter = new AbstractIterator[(P1,P2,P3)] {
          private val iter1 = i.iterator

          override def hasNext: Boolean = iter1.hasNext
          override def next() = {
            val in: ((P1,P2),P3) = iter1.next()
            PipeSearch.flatten(in)
          }
        } // iterator
        converter
      }
    }
    convertable
  }
  */


  def main(args: Array[String]): Unit = {

    val test1 = List(((1,2),3), ((4,5),6))
    val ftest1: List[(Int,Int,Int)] = test1.map( flatten(_) )
    println(ftest1.mkString(","))
    val ftest1b: List[(Int,Int,Int)] = flattenList(test1)
    println(ftest1b.mkString(","))
    val test2 = List(((1,2),3), ((4,5),6)).toIterator
    //val ftest2: Iterator[(Int,Int,Int)] = test2.map( flatten(_) )
    //val ftest2: TraversableOnce[(Int,Int,Int)] = test2.map( flatten(_) )
    //val ftest2 = lazyMapP3(test2.toIterable)(flatten(_))
    //val ftest2 = lazyMapP3(test2.toIterable)
    val ftest2: Iterator[(Int,Int,Int)] = test2.map( flatten[((Int, Int), Int)](_) )
    println(ftest2.mkString(","))

    val test3 = List(((1,2),3), ((4,5),6)).toIterable
    val ftest3: Iterable[(Int,Int,Int)] = test3.map( flatten(_) )
    println(ftest3.mkString(","))
    val ftest4 = test3.map( flatten(_) )
    println(ftest4.mkString(","))
    val ftest5 = flattenIterable( test3 )
    println(ftest5.mkString(","))
    val ftest6 = flattenIterableG( test3 )
    println(ftest6.mkString(","))
    val ftest7 = lazyMapP3(test3)
    println(ftest7.mkString(","))

    val pi1s = 1 to 2 by 1
    val pi2s = 1 to 3 by 1
    val pis1 = cartesian(pi1s, pi2s)
    val p1s1l = pis1.toList
    println(p1s1l.mkString(","))
    val pis1op = pi1s ## pi2s
    println(pis1op.toList.mkString(","))

    val pi0s = empty[Double]
    val pis2 = cartesian(pi0s, pi2s)
    val p1s2l = pis2.toList
    println(p1s2l.mkString(","))
    val pis2op = pi0s ## pi1s ## pi2s
    println(pis2op.toList.mkString(","))

    val pi3s = 'a' to 'c' by 1
    val pis3 = cartesian(pis1, pi3s)
    val pis3l = pis3.toList
    println(pis3l.mkString(","))
    val pis3op = pis1 ## pi3s
    println(pis3op.toList.mkString(","))

    val pis4op = pi1s && pi3s
    println(pis4op.toList.mkString(","))

    val pis5op = pi3s && pi1s
    println(pis5op.toList.mkString(","))

    val pis6op = pi1s !! pi3s
    println(pis6op.toList.mkString(","))

    val pis7op = pi3s !! pi1s
    println(pis7op.toList.mkString(","))


    println("f")
    val f = pis3l.map( flatten(_) )
    println(f.mkString(","))


    val p1s = BigDecimal(1.0) to 10 by 1.0 map(_.toDouble)

    val func1: All[Double,Double] = T((p1:Double, i:Double) => Right(p1 * i), p1s)
    val ops1: Iterable[Either[ADWError, Exec[Double, Double]]] = compile(func1)
    val result1: Iterable[(Executing, Double)] = exec(ops1)(1.0)
    println(result1.mkString(","))

    val p2s = BigDecimal(1.0) to 100 by 10.0 map(_.toDouble)
    val psFunc2: Iterable[(Double, Double)] = p1s && p2s
    val func2: All[Double,Double] = T((p1:Double, p2:Double, i:Double) => Right(p1 * p2 * i), psFunc2)
    val ops2: Iterable[Either[ADWError, Exec[Double, Double]]] = compile(func2)
    val result2: Iterable[(Executing, Double)] = exec(ops2)(1.0)
    println(result2.mkString(","))

    val p3s = 'a' to 'j' by 1
    val psFunc3X: Iterable[((Double, Double), Char)] = p1s && p2s && p3s
    val psFunc3 = (p1s && p2s && p3s).map{ flatten(_) }
    //val psFunc3 = (p1s && p2s && p3s).F
    val func3 = T(
      (p1:Double, p2:Double, p3:Char, i:Double) => {
        val tmp = p1 * p2 * i
        Right(tmp.toString + p3)
      }, psFunc3)
    val ops3 = compile(func3)
    val result3 = exec(ops3)(1.0)
    println(result3.mkString(","))

    println("OK")
  }

}
