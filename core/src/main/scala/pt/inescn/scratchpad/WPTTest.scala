package pt.inescn.scratchpad

import jwave.Transform
import jwave.transforms.WaveletPacketTransform
import jwave.transforms.wavelets.haar.Haar1

/**
  * sbt "root/runMain pt.inescn.scratchpad.WPTTest"
  */
object WPTTest {

  def main(args: Array[String]): Unit = {
    val t = new Transform(new WaveletPacketTransform(new Haar1()))

    //val arrTime = Array(1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0)
    val arrTime = Array(1.0, 1.0, 1.0, 1.0)

    val arrHilb = t.forward(arrTime) // 1-D WPT Haar forward
    println(arrHilb.mkString(","))

    val arrReco = t.reverse(arrHilb) // 1-D WPT Haar reverse
    println(arrReco.mkString(","))

    /*
    val matTime =
      Array(
        Array(1.0, 1.0, 1.0, 1.0),
        Array(1.0, 1.0, 1.0, 1.0),
        Array(1.0, 1.0, 1.0, 1.0),
        Array(1.0, 1.0, 1.0, 1.0))*/
    val matTime =
      Array(
        Array(1.0, 2.0, -1.0, 1.0),
        Array(1.0, 2.0, -1.0, 1.0),
        Array(1.0, 2.0, -1.0, 1.0),
        Array(1.0, 2.0, -1.0, 1.0))

    val matHilb = t.forward(matTime) // 2-D WPT Haar forward
    matHilb.foreach(e => println(e.mkString(",")))

    val matReco = t.reverse(matHilb) // 2-D WPT Haar reverse
    matReco.foreach(e => println(e.mkString(",")))


  }
}
