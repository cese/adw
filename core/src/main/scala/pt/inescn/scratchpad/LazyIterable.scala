package pt.inescn.scratchpad

import scala.collection.AbstractIterator

/**
  * root/runMain pt.inescn.scratchpad.LazyIterable
  *
  * @see https://users.scala-lang.org/t/wierd-beghaviour-with-iterators-iteration-stalls-and-restarts/3377
  */
object LazyIterable {

  /*
  // Ok
  private def lazyMap1[A,B](i: Iterable[A])(f: A => B): Iterable[B] = {
    val convertable = new Iterable[B] {
      private val iter = i.toIterator
      override def iterator: Iterator[B] = {
        val converter: AbstractIterator[B] = new AbstractIterator[B] {
          override def hasNext: Boolean = iter.hasNext
          override def next(): B = {
            f(iter.next())
          }
        } // iterator
        converter
      }
    }
    convertable
  }
  */

  // Fails
  private def lazyMap1[A,B](i: Iterable[A])(f: A => B): Iterable[B] = {
    val convertable = new Iterable[B] {
      override def iterator: Iterator[B] = {
        val converter: AbstractIterator[B] = new AbstractIterator[B] {
          private val iter = i.toIterator
          override def hasNext: Boolean = iter.hasNext
          override def next(): B = {
            f(iter.next())
          }
        } // iterator
        converter
      }
    }
    convertable
  }

  /*
  // Fails
  private def lazyMap1[A,B](i: Iterable[A])(f: A => B): Iterable[B] = {
    val convertable = new Iterable[B] {
      override def iterator: Iterator[B] = {
        val iter = i.toIterator
        val converter: AbstractIterator[B] = new AbstractIterator[B] {
          override def hasNext: Boolean = iter.hasNext
          override def next(): B = {
            f(iter.next())
          }
        } // iterator
        converter
      }
    }
    convertable
  }*/


  def window[A](i: Iterable[A], ssize: Int, step: Int): Iterable[Iterable[A]] = {

    val convertable = new Iterable[Iterable[A]] {

      override def iterator: Iterator[Iterable[A]] = {
        val converter = new AbstractIterator[Iterable[A]] {
          val iter: Iterator[Iterable[A]] = lazyMap1(i)(e => e).sliding(ssize, step)

          override def hasNext: Boolean = iter.hasNext
          override def next(): Iterable[A] = iter.next()
        }
        converter
      }
    }
    convertable
  }

  def main(args: Array[String]): Unit = {

    //val expected = List.range(0,1000).map(_ * 1).sliding(5,5).map(_.sum).toList.last
    //println(expected)

    val n = 40
    val l1: Iterable[Int] = /*Stream.from(0).take(n)*/ Iterable.range(0,n)
    val l1m1: Iterable[Int] = lazyMap1(l1)( _ * 1)
    // 4985
    //val l1m2: Iterable[Iterable[Int]] = l1m1.sliding(5, 5).toIterable
    val l1m2: Iterable[Iterable[Int]] = window(l1m1, 5, 5)
    val l1m3 = lazyMap1(l1m2){ e:Iterable[Int] => println(s"e = $e"); e.toList.sum }
    val l1m4 = window(l1m3,5,5)
    println("Not executed ------------------- ")
    println(l1m4.last)
  }
}
