package pt.inescn.scratchpad

/**
  * https://users.scala-lang.org/t/is-it-possible-to-store-and-retreive-function-definitions-for-later-composition/2394/3
  *
  * runMain pt.inescn.scratchpad.DynamicPipes2
  */
object DynamicPipes2 {

  object Funcs {
    def func1(a:Int): Double = a.toDouble
    def func2(a:Double): Double = a * 2
    def func3(a:Double): String = a.toString
  }

  // Note the 'U', this is intended to be existential
  final case class ResPair[T, U, R](next: T => U, tree: AChoiceTree[U, R])

  final case class AChoiceTree[T, R](
                                      done: Option[T =:= R],
                                      // this existential means each `ResPair` in the map can have an
                                      // entirely different 2nd tparam (U); accordingly, there is no way
                                      // to tell what the Us internal to an AChoiceTree are
                                      choose: Map[String, ResPair[T, _, R]]
                                    )

  object AChoiceTree {
    /** Return the goal, or None if we weren't at an endpoint when choices
      * was exhausted. You don't need a `start` value to calculate a
      * function, though; you could even use a `tree` and `choices` to
      * calculate a `Option[scalaz.data.AList[Function1, T, R]]`.
      *
      * You don't have to drive this with a `List`; you can step through
      * the tree at your leisure. Just keep in mind that the internal
      * tree types are existential; *none* of the internal trees of an
      * `AChoiceTree[T, R]` have the same type.
      *
      * There are three reasons for failure: you ran out of choices
      * before getting to a terminus, you got to a terminus but still
      * had [valid or invalid] choices left, or you tried an invalid
      * choice.  You can differentiate between these as you like; I did
      * not for this example.
      */
    @annotation.tailrec
    def interpret[T, R](tree: AChoiceTree[T, R],
                        choices: List[String],
                        start: T): Option[R] =
      choices match {
        case Nil => tree.done map (_(start))
        case s :: ss => tree.choose.get(s) match {
          case None => None
          case Some(p: ResPair[T, _, R]) => // look up "variable type pattern"
            interpret(p.tree, ss, p.next(start))
        }
      }

    def finalFunction[T, R](f: T => R): ResPair[T, _, R] =
      ResPair(f, AChoiceTree(terminus, Map()))
    //ResPair[T,U,R](f,      AChoiceTree(terminus, Map()))
    //ResPair[T,U,R](f:T=>U, AChoiceTree(terminus, Map()))
    //ResPair[T,U,R](f,      AChoiceTree[U,R](terminus, Map()))
    //ResPair[T,U,R](f,      AChoiceTree[U,R](terminus[R], Map()))
    //ResPair[T,U,R](f,      AChoiceTree[U,R](terminus[R]:Option[R=:=R], Map()))
    //ResPair[T,U,R](f,      AChoiceTree[U,R](terminus[R]:Option[R=:=R] =:= done[U=:=R], Map()))

    def terminus[R]: Option[R =:= R] = Some(implicitly)

    import Funcs._

    val tree: AChoiceTree[Int, String] =
      AChoiceTree(None, Map("func1" -> ResPair(func1,
        AChoiceTree(None, Map("func3" -> finalFunction(func3),"func2" -> ResPair(func2,
            AChoiceTree(None, Map("func3" -> finalFunction(func3)))))))))

    // 1st and 4th are only ones that should succeed
    val trials: (Option[String], Option[String], Option[String], Option[String]) = (
      interpret(tree, List("func1", "func3"), 3),
      interpret(tree, List("func1"), 1),
      interpret(tree, List("func1", "func3", "func2"), 4),
      interpret(tree, List("func1", "func2", "func3"), 8))

    val final1: ResPair[Int, _, Double] = finalFunction(func1)
    val terminal1: AChoiceTree[Int, Int] = AChoiceTree[Int,Int](terminus, Map())
  }

  def main(args: Array[String]): Unit = {
    println(AChoiceTree.trials)

    println(AChoiceTree.final1)
    println(AChoiceTree.terminal1)
  }

}