package pt.inescn.scratchpad

/*
/**
  * runMain pt.inescn.scratchpad.CirceDelay
  */
object CirceDelay {
  import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

  case class Func[I,O](f: I => O)

  sealed trait Foo {
    type P
    type I
    type O
    val param: P
    def toJson: Json
    def toJsonW: Json
    //def fromJason(j:String): Either[Error, S]
    def getFunc:Func[I,O]
  }

  case class Wrapper(v:Foo)

  case class Bar(override val param: Vector[String]) extends Foo {
    type P = Vector[String]
    type I = String
    type O = P
    override def toJson: Json = this.asJson
    override def toJsonW: Json = Wrapper(this).asJson
    override def getFunc: Func[String, Vector[String]] = Func((i:String) => Vector(i))
  }
  case class Qux(param: Option[Double]) extends Foo {
    type P = Option[Double]
    type I = String
    type O = P

    override def toJson: Json = this.asJson
    override def toJsonW: Json = Wrapper(this).asJson
    override def getFunc: Func[String, Option[Double]] = Func((i:String) => Some(i.toDouble))
  }

  def main(args: Array[String]): Unit = {
    val foo: Foo = Qux(Some(14.0))
    val bar: Foo = Bar(Vector("a","b","c"))

    val json = foo.asJson.noSpaces
    println(s"json = $json")
    val json1 = bar.asJson.noSpaces
    println(s"json1 = $json1")

    //val decodedFoo = decode[Foo[Option[Double]]](json)
    //println(decodedFoo)
    val decodedFoo = decode[Foo](json)
    println(s"decodedFoo = $decodedFoo")
    val decodedBar = decode[Foo](json1)
    println(s"decodedBar = $decodedBar")

    // This is were it fails, type error
    val func1: Func[Foo#I, Foo#O] = decodedFoo.right.get.getFunc
    val func2: Func[Foo#I, Foo#O] = decodedBar.right.get.getFunc
  }
}
*/

object CirceDelay {
  import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

  sealed trait Foo[S <: Foo[S,P], P] {
    val param: P
    def toJson: Json
    def toJsonW: Json
    def fromJason(j:String): Either[Error, S]
  }

  case class Wrapper[S<:Foo[S,P],P](v:Foo[S,P])

  case class Bar(override val param: Vector[String]) extends Foo[Bar,Vector[String]] {
    override def toJson: Json = this.asJson
    override def toJsonW: Json = Wrapper(this).asJson
    override def fromJason(j:String): Either[Error, Bar] = {
      val w = decode[Wrapper[Bar,Vector[String]]](j)
      //println(w)
      //w.right.get.v.asInstanceOf[Bar]
      w.flatMap(e => Right(e.v.asInstanceOf[Bar]))
    }
  }
  case class Qux(param: Option[Double]) extends Foo[Qux,Option[Double]] {
    override def toJson: Json = this.asJson
    override def toJsonW: Json = Wrapper(this).asJson

    override def fromJason(j:String): Either[Error, Qux] = {
      val w = decode[Wrapper[Qux,Option[Double]]](j)
      //println(w)
      //w.right.get.v.asInstanceOf[Qux]
      w.flatMap(e => Right(e.v.asInstanceOf[Qux]))
    }
  }

  // Problem: parameter type means no json decoder found
  /*case class Outer[X](inner:Foo[_,X]) extends Foo[Outer[X],Int] {
    override val param: Int = 100

    override def toJson: Json = ???
    override def toJsonW: Json = ???
    override def fromJason(j: String): Either[Error, Outer[X]] = ???
  }*/

  def main(args: Array[String]): Unit = {
    val foo: Foo[Qux, Option[Double]] = Qux(Some(14.0))
    val bar = Bar(Vector("a","b","c"))

    val json = foo.asJson.noSpaces
    println(json)

    //val decodedFoo = decode[Foo[Option[Double]]](json)
    //println(decodedFoo)

    // Here is were it fails, we need to make explicit the bar/foo object
    println(s"foo = $foo")
    println(s"foo.toJason = ${foo.toJson}")
    val foojw = foo.toJsonW
    println(s"foo.toJasonW = $foojw")
    val foow = foo.fromJason(foojw.noSpaces)
    println(s"foo.fromJason = $foow")
    println(s"bar = $bar")
    println(s"bar.toJason = ${bar.toJson}")
    val barjw = bar.toJsonW
    println(s"bar.toJasonW = $barjw")
    // Oops
    //val barw = foo.fromJason(barjw.noSpaces)
    val barw = bar.fromJason(barjw.noSpaces)
    println(s"bar.fromJason = $barw")

  }
}

