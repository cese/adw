package pt.inescn.scratchpad

import scala.reflect.runtime.universe._

/**
  * This is the exploration and testing of an means to pack and unpack values
  * to and from a wrapper `Val` automatically. It is important to note that
  * we assume that type information will not always be fully available. So
  * the information of the wrappers's inner type may be unknown. Unpacking
  * such a value may result in a run-time failure.
  *
  * @see https://stackoverflow.com/questions/1154571/scala-abstract-types-vs-generics
  *      https://typelevel.org/blog/2015/07/13/type-members-parameters.html
  *      https://stackoverflow.com/questions/26053319/how-to-capture-t-from-typetagt-or-any-other-generic-in-scala
  *      https://stackoverflow.com/questions/19386964/i-want-to-get-the-type-of-a-variable-at-runtime
  *
  * runMain pt.inescn.scratchpad.AutoChooseVal
  */
object AutoChooseVal {

  import scala.language.implicitConversions



  sealed trait Val[V] {
    val v: V
  }

  /**
    * Pack a value automatically.
    *
    * @param s value to be wrapped
    * @tparam A type of the wrapped value
    * @return returns a `Vaĺ` that wraps the value
    */
  implicit def pack[A](s:A) : Val[A] = new Val[A] {
    println(s"Packing $s")
    override val v: A = s
  }



  // https://gist.github.com/retronym/228673


  sealed trait LowPriority {

    implicit def testFull[A,B](av: Val[A], f: A => B): Either[String,B] = {
      println(s"TEST FULL $av")
      try {
        val v = av.v.asInstanceOf[A]
        Right(f(v))
      } catch {
        // When used via implicits should never reach this
        case e:Exception => Left(e.getMessage)
      }
    }
  }

  object HighPriority extends LowPriority {
    implicit def test[A,B,X](av: Val[X], f: A => B): Either[String,B] = {
      println(s"TEST $av")
      try {
        val v = av.v.asInstanceOf[A]
        Right(f(v))
      } catch {
        // When used via implicits should never reach this
        case e:Exception => Left(e.getMessage)
      }
    }
  }

  def choose[A,B,X](av: Val[X], f: A => B)(implicit a: (Val[X], A=>B) => Either[String,B]): Either[String,B] = {
     a(av, f)
  }

  def main(args: Array[String]): Unit = {

    //import LowPriority._
    import HighPriority._

    // Ok 1
    val r5: Val[String] = "100"
    println(s"r5: Val(${r5.v}) : ${r5.v.getClass}")
    val r6: Val[Double] = 200.0
    println(s"r6: Val(${r6.v}) : ${r6.v.getClass}")
    val r7: Val[Array[Double]] = Array(300.0, 301, 302)
    println(s"r7: Val(${r7.v.mkString(",")}) : ${r7.v.getClass}")
    // Just testing the function


    // We can use unpacking if the packed type is know at compile time
    // If we apply a incorrect type in the anonymous function, compilation will fail
    println("Implicit unpacking: well typed Val")
    val u0 = testFull(r5, (e:String) => e.toInt)
    val u1 = testFull(r6, (e:Double) => e)
    val u2 = testFull(r7, (e:Array[Double]) => e)
    println(u0)
    println(u1)
    println(u2)

    // This will fail during run-time
    val u3 = test(r6, (e:String) => e.toInt)
    println(s"u3 = $u3")

    /*
    // This will fail during compile-time
    val u4 = testFull(r6, (e:String) => e.toInt)
    println(s"u4 = $u4")
    */

    val c0 = choose(r5, (e:String) => e.toInt)
    println(c0)

    // Fails during compile-time (selects low priority)
    val c1 = choose(r6, (e:String) => e.toInt)
    println(s"c1 = $c1")


    // Now we do not have access to the contained data
    val rs = Map("x" -> r5, "y" -> r6, "z" -> r7)
    println(s"rs = $rs")
    val s5 = rs("x")
    val s6 = rs("y")
    val s7 = rs("z")

    // We can try use unpacking if the packed type is unknown at compile time
    // The type `A` of the anonymous function does not bind to the unpack's `T`
    // Note that the Val's inner type must not be used because it is in effect unknown
    println("Implicit unpacking: untyped Val")
    val v0 = test(s5, (e:String) => e.toInt)
    val v1 = test(s6, (e:Double) => e)
    val v2 = test(s7, (e:Array[Double]) => e)
    println(v0)
    println(v1)
    println(v2)

    // Fails during compile-time
    // Compiles
    //val c2 = choose(r6, (e:Double) => e.toInt)(testFull)
    // Does no compile
    //val c2 = choose(r6, (e:String) => e.toInt)(testFull)
    // Compiles and runs
    //val c2 = choose(s6, (e:String) => e.toInt)(test)
    // Should compile by auto-selecting test (ok)
    //val c2 = choose(s6, (e:String) => e.toInt)
    // Should fail but compiles
    val c2 = choose(r6, (e:String) => e.toInt)
    println(c2)

  }


}
