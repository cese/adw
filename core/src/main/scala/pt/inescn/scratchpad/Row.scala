package pt.inescn.scratchpad

object Row {

  // https://github.com/obkson/dotty-records
  //   https://github.com/obkson/dotty-records/blob/master/src/main/scala/records/Record.scala
  // https://github.com/obkson/wreckage

  /*
      root/runMain pt.inescn.scratchpad.Row
   */
  def main(args: Array[String]): Unit = {
    import shapeless._

    // https://github.com/milessabin/shapeless/wiki/Feature-overview:-shapeless-2.0.0
    import shapeless._ ; import syntax.singleton._ ; import record._

    val row1 =
        ("x" ->> "100") ::
        ("y" ->> "200") ::
        ("z" ->>  300.0) ::
        HNil

    val x1:String = row1.get("x")
    println(s"x1 = $x1")

    val row2 =
      ("f" ->> true) ::
        ("p" ->> 0.5) ::
        HNil

    val row3 = row1 ++ row2
    println(row3)

    println(row3 + ("i" ->> 101) )

  }

}
