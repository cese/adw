package pt.inescn.scratchpad


import java.awt.Color

import org.hipparchus.random.JDKRandomGenerator
import pt.inescn.models.svm.Kernels.{Kernel, HashMapKernelCache, LinearF, RBFF}
import pt.inescn.models.svm.{LASVM, Load}
import pt.inescn.models.svm.LASVM.{Algo, Algo1, Algo2}
import pt.inescn.samplers.stream.Distribution
import pt.inescn.samplers.stream.Ops._
import smile.classification.SVM
import smile.math.kernel.{GaussianKernel, LinearKernel, MercerKernel}
import smile.plot._

/**
  * root/runMain pt.inescn.scratchpad.LASVMTests
  */
object LASVMTests {

  def blob2D(n: Int, label: Float,
             mean1: Float, deviation1: Float,
             mean2: Float, deviation2: Float) : (Array[Array[Float]], Array[Float]) = {
    val seed = new JDKRandomGenerator(9876)
    val samples1 = Distribution.normal(mean1, deviation1)(seed).toStream
    val samples2 = Distribution.normal(mean2, deviation2)(seed).toStream
    val samples = samples1 !! samples2
    val x = samples.take(n).map(v => Array(v._1.toFloat, v._2.toFloat)).toArray
    val y = Array.fill(n)(label)
    (x,y)
  }

  def blobs2D(n1: Int, label1: Float,
              mean11: Float, deviation11: Float,
              mean12: Float, deviation12: Float,
              n2: Int, label2: Float,
              mean21: Float, deviation21: Float,
              mean22: Float, deviation22: Float): (Array[Array[Float]], Array[Float]) = {
    val blob1 = blob2D(n1, label1, mean11, deviation11, mean12, deviation12)
    val blob2 = blob2D(n2, label2, mean21, deviation21, mean22, deviation22)
    val x = blob1._1 ++ blob2._1
    val y = blob1._2 ++ blob2._2
    (x, y)
  }


  val eps = 9e-6

  def testDotCache(): Unit = {
    val (x,y) = blobs2D(10, 1f, 0.5f, 0.25f, 1.5f, 0.25f,
      10, -1f, 1.5f, 0.25f, 0.5f, 0.25f)

    //println(Kernels.dot(Array(0.0f, 0.0f), Array(1.0f, 0.0f)))
    //println( Kernels.dot(x(0), x(0)) )

    val K1 = LinearF
    //val K = RBFF(1.0f)
    val KCache1 = new HashMapKernelCache(x, K1)

    // Test if cache value calculated ok
    //println(s"KCache.K(0,0) = ${KCache1(0,0)}")
    assert( Math.abs(KCache1(0,0) - 2.568119) <= eps)

    // Test if cached value read ok
    assert( KCache1(1,1) == KCache1(1,1) )
    assert( KCache1(1,19) == KCache1(1,19) )
    assert( KCache1(19,1) == KCache1(19,1) )

    val K2 = RBFF(1.0f)
    val KCache2 = new HashMapKernelCache(x, K2)

    // Test if cache value calculated ok
    //println(s"KCache.K(0,0) = ${KCache2(0,0)}")
    assert( Math.abs(KCache2(0,0) - 1.0) <= eps)

    // Test if cached value read ok
    assert( KCache2(1,1) == KCache2(1,1) )
    assert( KCache2(1,19) == KCache2(1,19) )
    assert( KCache2(19,1) == KCache2(19,1) )

  }

  /**
    * This function shows that SMILE boundary is stable for this small and
    * simple linearly separable example. The boundary always seems to have
    * the same slope and is correctly centred.
    *
    * @see test2DBlobSeparableKLinear
    */
  def testSMILE_LASVM_1(): Unit = {
    import smile.classification.SVM
    import smile.math.kernel.LinearKernel

    // TODO: remove
    val (xx,yy) = blobs2D(10, 1f, 0.5f, 0.25f, 1.5f, 0.25f,
      10, -1f, 1.5f, 0.25f, 0.5f, 0.25f)
    //val px: Array[Array[Double]] = x.map(_.map(_.toDouble))
    //val py = y.map(_.toInt)

    val x = Array( Array(0.5183529257774353, 1.5163869857788086),
      Array(0.5373819470405579, 1.387535810470581),
      Array(0.31604915857315063, 1.6519373655319214),
      Array(0.3523474335670471, 1.4081544876098633),
      Array(0.6315290927886963, 1.4651329517364502),
      Array(0.15004847943782806, 1.2614046335220337),
      Array(0.11366268247365952, 1.7131949663162231),
      Array(0.7353114485740662, 1.522897720336914),
      Array(0.27534568309783936, 1.4563539028167725),
      Array(0.6298583745956421, 1.4252253770828247),
      Array(1.51835298538208, 0.5163870453834534),
      Array(1.537381887435913, 0.38753584027290344),
      Array(1.3160492181777954, 0.6519374251365662),
      Array(1.3523473739624023, 0.4081544578075409),
      Array(1.6315290927886963, 0.4651329815387726),
      Array(1.1500484943389893, 0.2614046335220337),
      Array(1.1136627197265625, 0.7131949663162231),
      Array(1.735311508178711, 0.5228977203369141),
      Array(1.2753456830978394, 0.45635390281677246),
      Array(1.629858374595642, 0.4252253472805023) )
    val y = Array(1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1)


    val px = x
    val py = y

    val svm2 = new SVM(new LinearKernel(), 1)
    val ppy = py.map(e => if (e < 0.0) 0 else e)
    svm2.learn(px, ppy)
    svm2.finish()
    //println(s"svm2.getSupportVectors.size() = ${svm2.getSupportVectors.size()}")
    val ps = svm2.predict(px)
    val ok = ps.zip(ppy).count{ case (yhat, yo) => if (yo == yhat) true else false}.toFloat
    //println(s"svm2 error = ${(1-(ok/ppy.length))*100}% ; accuracy = ${(ok/ppy.length)*100}%")
    assert(ok/ppy.length == 1.0)
    //val svs: util.ListIterator[SVM[Array[Double]]#SupportVector] = svm2.getSupportVectors.listIterator()


    /*
    val lower = Array(0.0, 0.2)
    //println(s"${lower.mkString(",")}")
    val upper = Array(2.0, 1.8)
    //println(s"${upper.mkString(",")}")

    // X1 axis
    val stepx1 = (upper(0) - lower(0)) / 20.0
    val xs0 = (BigDecimal(lower(0)) to upper(0) by stepx1).map(_.toDouble).toArray
    // X2 axis
    val stepx2 = (upper(1) - lower(1)) / 20.0
    val xs1 = (BigDecimal(lower(1)) to upper(1) by stepx2).map(_.toDouble).toArray

    val zs: Array[Array[Double]] = xs0.map{ x =>
      xs1.map{ y =>
        val p = svm2.predict( Array(x, y) ).toDouble
        //println(s"x = $x, y = $y = $p")
        p
      }
    }*/

    //println(s"xs.length = ${xs0.length}")
    //println(s"ys.length = ${xs1.length}")
    //window.canvas.add( new Contour(zs) )

    /*val win = plot(px, ppy, svm2) // Ok
    win.frame.setVisible(true)*/

  }


  def makeGrid(gridSize: Int, upper: Array[Double], lower: Array[Double]): Array[Array[Double]] = {
    val stepx = (upper(0) - lower(0)) / gridSize
    val stepy = (upper(1) - lower(1)) / gridSize
    val xs2 = (BigDecimal(lower(0)) to upper(0) by stepx).map(_.toDouble).toArray
    val ys2 = (BigDecimal(lower(1)) to upper(1) by stepy).map(_.toDouble).toArray
    val grid: Array[Array[Double]] = xs2.flatMap{ x =>
      ys2.map{ y => Array(x, y) }
    }
    grid
  }


  /**
    * Used for debugging the SVM algorithm small 2D problems.
    *
    * @param x - data in 2D dimensions
    * @param y - label (class of each point)
    * @param lower - lower plot limit
    * @param upper - upper plot limits
    * @param svm1 - SVM algorithm
    * @param w - linear weights
    */
  def linear2DDebugPlot(x: Array[Array[Float]], y: Array[Float],
                        lower: Array[Double], upper: Array[Double],
                        svm1: Algo1[Array[Float]], w: Array[Float]): Unit = {

    // https://stats.stackexchange.com/questions/5056/computing-the-decision-boundary-of-a-linear-svm-model
    def decisionFuncBoundary1(x:Double): Double = (-svm1.b - (x*w(0))) / w(1)
    def decisionFuncBoundary2(x:Double): Double = (-svm1.b - (x*w(1))) / w(0)
    def decisionFuncDown(x:Double): Double = ((-svm1.b - 1) - (x*w(0))) / w(1)
    def decisionFuncUp(x:Double): Double = ((-svm1.b + 1) - (x*w(0))) / w(1)

    // X1 axis
    val stepx1 = (upper(0) - lower(0)) / 20.0
    val xs0 = (BigDecimal(lower(0)) to upper(0) by stepx1).map(_.toDouble).toArray
    // X2 axis
    val stepx2 = (upper(1) - lower(1)) / 20.0
    val xs1 = (BigDecimal(lower(1)) to upper(1) by stepx2).map(_.toDouble).toArray
    // Should be the same
    val decisionBoundary1 = xs0.map{x => Array(x, decisionFuncBoundary1(x))}
    val decisionBoundary2 = xs1.map{x => Array(decisionFuncBoundary2(x), x)}
    val decisionBoundaryUp = xs0.map{x => Array(x, decisionFuncUp(x))}
    val decisionBoundaryDown = xs0.map{x => Array(x, decisionFuncDown(x))}

    // Look at 2D debug plot
    val px: Array[Array[Double]] = x.map(_.map(_.toDouble))
    val py = y.map(_.toInt)
    val window = plot(px, py, Array('@', 'q'), Array(Color.BLUE, Color.RED))
    window.canvas.setAxisLabels("x1", "x2")
    //val lower = window.canvas.getLowerBounds
    //println(s"${lower.mkString(",")}")
    //val upper = window.canvas.getUpperBounds
    //println(s"${upper.mkString(",")}")
    //println(s"${lower.mkString(",")}")

    window.canvas.add(new LinePlot(decisionBoundary1, Line.Style.LONG_DASH))
    window.canvas.add(new LinePlot(decisionBoundary2, Line.Style.DOT))
    window.canvas.add(new LinePlot(decisionBoundaryUp, Line.Style.DASH))
    window.canvas.add(new LinePlot(decisionBoundaryDown, Line.Style.DASH))
    window.frame.setVisible(true)

    val gridSize = 30
    val grid = makeGrid(gridSize, upper, lower)

    // Debug - checking approximate class output
    /*
    val z0: Array[Int] = grid.map{ x =>
      val p = svm1.predictRaw( x.map(_.toFloat) )
      //val p = svm1.predict( x.map(_.toFloat) )
      //if (Math.abs(p) <= 0.1) println(s"x = ${x.mkString(",")}, p = $p")
      p.toInt
    }
    println(s"z0.distinct = ${z0.distinct.mkString(",")}")
    plot(grid, z0.map(_.toString))
    */

    /*
    // Use the primal linear weight
    val z0: Array[Int] = grid.map{ x =>
      val p = svm1.predictLinearW( w, x.map(_.toFloat) )
      //if (Math.abs(p) <= 0.1) println(s"x = ${x.mkString(",")}, p = $p")
      if (p > 0) 1 else 0
      //p.toInt
    }
    println(s"z0.distinct = ${z0.distinct.mkString(",")}")
    val window2 = plot(px, py, Array('@', 'q'), Array(Color.BLUE, Color.RED))
    window2.canvas.setAxisLabels("x1", "x2")
    window2.canvas.add(new ScatterPlot(grid, z0, Array('-', '+'), Array(Color.BLUE, Color.RED)))
     */

    // Use the dual alpha_i
    val z: Array[Int] = grid.map{ x: Array[Double] =>
      val p = svm1.predict( x.map(_.toFloat) )
      //println(s"x = ${x.mkString(",")}, p = $p")
      p
    }
    //println(s"z.distinct = ${z.distinct.mkString(",")}")
    //val window2 = plot(grid, z.map(_.toString))
    //val window2 = plot(grid, z, Array('.', '-'), Array(Color.RED, Color.BLUE))
    window.canvas.add(new ScatterPlot(grid, z, Array('-', '+'), Array(Color.BLUE, Color.RED)))

    // The test point closes to the border
    //window.canvas.add(new Point('#', testX(0), testX(1)))
    //window2.canvas.add(new Point('#', testX(0), testX(1)))

  }


  /**
    * This test shows that the naive LASVM version Algo1 is quite unstable. It has
    * reduced errors but the decision boundary varies widely. Compare the results
    * with the SMILE version.
    *
    * @see testSMILE_LASVM_1
    */
  def test2DBlobSeparableKLinear(): Unit = {

    // TODO: remove
    val (xx,yy) = blobs2D(10, 1f, 0.5f, 0.25f, 1.5f, 0.25f,
      10, -1f, 1.5f, 0.25f, 0.5f, 0.25f)
    /*val px: Array[Array[Double]] = xx.map(_.map(_.toDouble))
    val py = yy.map(_.toInt)
    val window = plot(px, py, Array('+', 'o'), Array(Color.RED, Color.BLUE, Color.CYAN))
    window.canvas.setAxisLabels("x1", "x2")*/

    val x = Array(
      Array(0.5183529f, 1.516387f),
      Array(0.53738195f, 1.3875358f),
      Array(0.31604916f, 1.6519374f),
      Array(0.35234743f, 1.4081545f),
      Array(0.6315291f, 1.465133f),
      Array(0.15004848f, 1.2614046f),
      Array(0.11366268f, 1.713195f),
      Array(0.73531145f, 1.5228977f),
      Array(0.27534568f, 1.4563539f),
      Array(0.6298584f, 1.4252254f),
      Array(1.518353f, 0.51638705f),
      Array(1.5373819f, 0.38753584f),
      Array(1.3160492f, 0.6519374f),
      Array(1.3523474f, 0.40815446f),
      Array(1.6315291f, 0.46513298f),
      Array(1.1500485f, 0.26140463f),
      Array(1.1136627f, 0.71319497f),
      Array(1.7353115f, 0.5228977f),
      Array(1.2753457f, 0.4563539f),
      Array(1.6298584f, 0.42522535f))

    val y = Array(1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0).map(_.toFloat)

    //println(s"Data size = ${x.length}")

    // Linear kernel
    val K = LinearF
    val KCache = new HashMapKernelCache(x, K)

    val svm1 = new Algo1(KCache, x, y,
      C = 1,
      pickSample = LASVM.pickRandomSample,
      epsilon = 0.001f,
      //cacheSize: Int,
      initSampleSize = 5,
      onlineEpochs = 2,
      maxFinishIter = 1000,
      debug = false)

    svm1.fit()

    val (_,_,error1) = svm1.learnErrorRate()
    //println(s"svm1.error_rate() = $error1")
    assert(error1 <= 0.1) // usually 0

    //svm1.dprintDiagnostics(-1)
    val ow = svm1.updateLinearW(x)
    //ow.foreach( e => println(s"(Linear Model) w = ${e.mkString(",")}"))
    val w = ow.get
    val error2 = svm1.linearErrorRate(w)(x)
    //println(s"svm1.linear_error_rate(w)(x) = $error2")
    assert(error2 <= 0.1) // usually 0


    // Check that the linear weight are correct - prediction with dual and primal must be the same
    //x.foreach(e => println(e.mkString(",")))
    val testX = Array(1.1136627f,0.71319497f)
    val test = svm1.predictRaw(testX)
    val test2 = svm1.predictLinearW(w, testX)
    //println(s"svm1.predictRaw(testX) = $test")
    //println(s"svm1.predictLinearW(w, testX) = $test2")
    assert(Math.abs(test - test2) <= eps)

    //val lower = window.canvas.getLowerBounds
    val lower = Array(0.0, 0.2)
    //val upper = window.canvas.getUpperBounds
    val upper = Array(2.0, 1.8)
    //println(s"${lower.mkString(",")}")


    // https://stats.stackexchange.com/questions/5056/computing-the-decision-boundary-of-a-linear-svm-model
    def decisionFuncBoundary1(x:Double): Double = (-svm1.b - (x*w(0))) / w(1)
    // X1 axis
    val stepx1 = (upper(0) - lower(0)) / 20.0
    val xs0 = (BigDecimal(lower(0)) to upper(0) by stepx1).map(_.toDouble).toArray
    // Should be the same
    val decisionBoundary1 = xs0.map{x => Array(x, decisionFuncBoundary1(x))}

    // Get a sample
    val wX: Double = xs0(1)
    // Predict label
    val wY: Array[Double] = decisionBoundary1(1)
    // predict with linear kernel weights (primal)
    val wPred = svm1.predictLinearW(w, wY.map(_.toFloat))
    val wPred1 = svm1.predictRaw(wY.map(_.toFloat))
    //println(s"${wY.mkString(",")} should be 0 : $wPred : raw $wPred1")
    assert(wPred < eps)
    assert(wPred1 < eps)

    // Where is the decision boundary?
    // For a sample x1 determine the x2 at the border
    val x2 = (-svm1.b - (w(0)*wX)) / w(1)
    // Must be the same as calculated above
    //println(s"x2 = $x2 == ${wY(1)}")
    assert(Math.abs(x2 - wY(1)) < eps)
    // Given (x1,x2) at the decision boundary, we should get a raw prediction of 0
    val wPred3 = svm1.predictLinearW(w, Array(wX.toFloat, x2.toFloat))
    //println(s"wPred3 = $wPred3")
    assert(wPred3 <= eps)
    // Same thing if we calculate it manually
    val yhat = wX*w(0) + x2*w(1) + svm1.b
    //println(s"yhat = $yhat")
    assert(yhat <= eps)

    // Look at 2D debug plot
    //linear2DDebugPlot(x, y, lower, upper,  svm1, w)

  }


  def test2DBlobSeparableKGaussian(): Unit = {

    // TODO: remove
    val (xx,yy) = blobs2D(10, 1f, 0.5f, 0.25f, 1.5f, 0.25f,
      10, -1f, 1.5f, 0.25f, 0.5f, 0.25f)
    /*val px: Array[Array[Double]] = x.map(_.map(_.toDouble))
    val py = y.map(_.toInt)
    val window = plot(px, py, Array('+', 'o'), Array(Color.RED, Color.BLUE, Color.CYAN))
    window.canvas.setAxisLabels("x1", "x2")*/

    val x = Array(
      Array(0.5183529f, 1.516387f),
      Array(0.53738195f, 1.3875358f),
      Array(0.31604916f, 1.6519374f),
      Array(0.35234743f, 1.4081545f),
      Array(0.6315291f, 1.465133f),
      Array(0.15004848f, 1.2614046f),
      Array(0.11366268f, 1.713195f),
      Array(0.73531145f, 1.5228977f),
      Array(0.27534568f, 1.4563539f),
      Array(0.6298584f, 1.4252254f),
      Array(1.518353f, 0.51638705f),
      Array(1.5373819f, 0.38753584f),
      Array(1.3160492f, 0.6519374f),
      Array(1.3523474f, 0.40815446f),
      Array(1.6315291f, 0.46513298f),
      Array(1.1500485f, 0.26140463f),
      Array(1.1136627f, 0.71319497f),
      Array(1.7353115f, 0.5228977f),
      Array(1.2753457f, 0.4563539f),
      Array(1.6298584f, 0.42522535f))

    val y = Array(1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0).map(_.toFloat)

    //println(s"Data size = ${x.length}")

    // Guassian kernel
    val K = RBFF(1.0f)
    val KCache = new HashMapKernelCache(x, K)

    val svm1 = new Algo1(KCache, x, y,
      C = 1,
      pickSample = LASVM.pickRandomSample,
      epsilon = 0.001f,
      //cacheSize: Int,
      initSampleSize = 5,
      onlineEpochs = 1,
      maxFinishIter = 1000,
      debug = false)

    svm1.fit()

    val (_,_,error1) = svm1.learnErrorRate()
    //println(s"svm1.error_rate() = $error1")
    assert(error1 < 0.1) // usually 0

  }



  /**
    * Both Alg1 nd SMILE SVM seem to have the same errors when we get a good
    * 25% error. However SMILE seems to be more stable. It consistently
    * produces 25% error rates.
    */
  def test2DBlobNonSeparableKLinear(): Unit = {

    val (xx,yy) = blobs2D(10, 1f, 0.5f, 0.5f, 0.8f, 0.6f,
      10, -1f, 1.5f, 0.5f, 0.6f, 0.6f)
    /*val px: Array[Array[Double]] = x.map(_.map(_.toDouble))
    val py = y.map(_.toInt)
    val window = plot(px, py, Array('+', 'o'), Array(Color.RED, Color.BLUE, Color.CYAN))
    window.canvas.setAxisLabels("x1", "x2")*/

    val x = Array(
      Array(0.5367059f, 0.83932894f),
      Array(0.57476383f, 0.53008604f),
      Array(0.13209833f, 1.1646498f),
      Array(0.20469487f, 0.5795707f),
      Array(0.7630582f, 0.7163192f),
      Array(-0.19990304f, 0.22737111f),
      Array(-0.27267462f, 1.3116679f),
      Array(0.9706229f, 0.8549546f),
      Array(0.050691355f, 0.6952493f),
      Array(0.7597167f, 0.62054086f),
      Array(1.5367059f, 0.63932896f),
      Array(1.5747639f, 0.33008605f),
      Array(1.1320983f, 0.9646498f),
      Array(1.2046949f, 0.37957072f),
      Array(1.7630582f, 0.5163192f),
      Array(0.800097f, 0.027371129f),
      Array(0.7273254f, 1.111668f),
      Array(1.9706229f, 0.6549546f),
      Array(1.0506914f, 0.49524936f),
      Array(1.7597167f, 0.42054084f)
    )
    val y = Array(1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f)

    //x.foreach( e => println(s"Array(${e.mkString(", ")})") )
    //println(y.mkString(","))

    //println(s"Data size = ${x.length}")

    // Linear kernel
    val K = LinearF
    val KCache = new HashMapKernelCache(x, K)

    val svm1 = new Algo1(KCache, x, y,
      C = 1,
      pickSample = LASVM.pickRandomSample,
      epsilon = 0.001f,
      //cacheSize: Int,
      initSampleSize = 5,
      onlineEpochs = 1,
      maxFinishIter = 1000,
      debug = false)

    svm1.fit()

    val expected_error = 0.25

    val (_, _, error1) = svm1.learnErrorRate()
    //println(s"svm1.error_rate() = $error1")
    assert(error1 <= expected_error)

    //svm1.dprintDiagnostics(-1)
    val ow = svm1.updateLinearW(x)
    //ow.foreach( e => println(s"(Linear Model) w = ${e.mkString(",")}"))
    val w = ow.get
    val error2 = svm1.linearErrorRate(w)(x)
    //println(s"svm1.linear_error_rate(w)(x) = $error2")
    assert(error2 <= expected_error)


    // Check that the linear weight are correct - prediction with dual and primal must be the same
    //x.foreach(e => println(e.mkString(",")))
    val testX = Array(1.1136627f,0.71319497f)
    val test = svm1.predictRaw(testX)
    val test2 = svm1.predictLinearW(w, testX)
    //println(s"svm1.predictRaw(testX) = $test")
    //println(s"svm1.predictLinearW(w, testX) = $test2")
    assert(Math.abs(test - test2) <= eps)

    //val lower = window.canvas.getLowerBounds
    val lower = Array(-0.5, 0.0)
    //val upper = window.canvas.getUpperBounds
    val upper = Array(2.0, 1.4)
    //println(s"${lower.mkString(",")}")


    // https://stats.stackexchange.com/questions/5056/computing-the-decision-boundary-of-a-linear-svm-model
    def decisionFuncBoundary1(x:Double): Double = (-svm1.b - (x*w(0))) / w(1)
    // X1 axis
    val stepx1 = (upper(0) - lower(0)) / 20.0
    val xs0 = (BigDecimal(lower(0)) to upper(0) by stepx1).map(_.toDouble).toArray
    // Should be the same
    val decisionBoundary1 = xs0.map{x => Array(x, decisionFuncBoundary1(x))}

    // Get a sample
    val wX: Double = xs0(1)
    // Predict label
    val wY: Array[Double] = decisionBoundary1(1)
    val ys = wY.map(_.toFloat)
    // predict with linear kernel weights (primal)
    val wPred = svm1.predictLinearW(w, ys)
    //println(s"${w.mkString(",")}")
    //println(s"${ys.mkString(",")}")
    val wPred1 = svm1.predictRaw(ys)
    //println(s"${wY.mkString(",")} should be 0 : $wPred : raw $wPred1")
    assert(wPred < eps)
    assert(wPred1 < eps)

    // Where is the decision boundary?
    // For a sample x1 determine the x2 at the border
    val x2 = (-svm1.b - (w(0)*wX)) / w(1)
    // Must be the same as calculated above
    //println(s"x2 = $x2 == ${wY(1)}")
    assert(Math.abs(x2 - wY(1)) < eps)
    // Given (x1,x2) at the decision boundary, we should get a raw prediction of 0
    val wPred3 = svm1.predictLinearW(w, Array(wX.toFloat, x2.toFloat))
    //println(s"wPred3 = $wPred3")
    assert(wPred3 <= eps)
    // Same thing if we calculate it manually
    val yhat = wX*w(0) + x2*w(1) + svm1.b
    //println(s"yhat = $yhat")
    assert(yhat <= eps)

    // Look at 2D debug plot
    //linear2DDebugPlot(x, y, lower, upper,  svm1, w)


    val px: Array[Array[Double]] = x.map(_.map(_.toDouble))
    val py = y.map(_.toInt)

    val svm2 = new SVM(new LinearKernel(), 1)
    val ppy = py.map(e => if (e < 0.0) 0 else e)
    svm2.learn(px, ppy)
    svm2.finish()
    //println(s"svm2.getSupportVectors.size() = ${svm2.getSupportVectors.size()}")
    val ps = svm2.predict(px)
    //println(s"${ppy.mkString(",")}")
    //val hat = ps.map{ p => if (p > 0) 1 else 0}
    //println(s"${hat.mkString(",")}")
    val ok = ps.zip(ppy).count{ case (yhat, yo) => if (yo == yhat) true else false}.toFloat
    //println(s"svm2 error = ${(1-(ok/ppy.length))*100}% ; accuracy = ${(ok/ppy.length)*100}%")
    assert((1.0 - ok/ppy.length) <= expected_error)

  }


  /**
    * SMILE consistently gets lower error rates at 5%. Lowest of
    * Alg1 seems to be 15% and it varies quite  bit. We need to
    * evaluate this.
    */
  def test2DBlobNonSeparableKGaussian(): Unit = {

    // TODO: remove
    val (xx,yy) = blobs2D(10, 1f, 0.5f, 0.5f, 0.8f, 0.6f,
      10, -1f, 1.5f, 0.5f, 0.6f, 0.6f)
    /*val px: Array[Array[Double]] = x.map(_.map(_.toDouble))
    val py = y.map(_.toInt)
    val window = plot(px, py, Array('+', 'o'), Array(Color.RED, Color.BLUE, Color.CYAN))
    window.canvas.setAxisLabels("x1", "x2")*/

    val x = Array(
      Array(0.5367059f, 0.83932894f),
      Array(0.57476383f, 0.53008604f),
      Array(0.13209833f, 1.1646498f),
      Array(0.20469487f, 0.5795707f),
      Array(0.7630582f, 0.7163192f),
      Array(-0.19990304f, 0.22737111f),
      Array(-0.27267462f, 1.3116679f),
      Array(0.9706229f, 0.8549546f),
      Array(0.050691355f, 0.6952493f),
      Array(0.7597167f, 0.62054086f),
      Array(1.5367059f, 0.63932896f),
      Array(1.5747639f, 0.33008605f),
      Array(1.1320983f, 0.9646498f),
      Array(1.2046949f, 0.37957072f),
      Array(1.7630582f, 0.5163192f),
      Array(0.800097f, 0.027371129f),
      Array(0.7273254f, 1.111668f),
      Array(1.9706229f, 0.6549546f),
      Array(1.0506914f, 0.49524936f),
      Array(1.7597167f, 0.42054084f)
    )
    val y = Array(1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f,-1.0f)

    //println(s"Data size = ${x.length}")

    // Linear kernel
    //val K = RBFF(10f)
    val K = RBFF(2f)  // 2sigma^2
    val KCache = new HashMapKernelCache(x, K)

    val svm1 = new Algo1(KCache, x, y,
      C = 1,
      pickSample = LASVM.pickRandomSample,
      epsilon = 0.001f,
      //cacheSize: Int,
      initSampleSize = 5,
      onlineEpochs = 1,
      maxFinishIter = 1000,
      debug = false)

    svm1.fit()

    val expected_error = 0.25

    val (_, _, error1) = svm1.learnErrorRate()
    //println(s"svm1.error_rate() = $error1")
    assert(error1 <= expected_error)


    // Check that the linear weight are correct - prediction with dual and primal must be the same
    //x.foreach(e => println(e.mkString(",")))
    val testX = Array(1.1136627f,0.71319497f)
    val test = svm1.predictRaw(testX)
    //println(s"svm1.predictRaw(testX) = $test")
    assert(test <= expected_error)


    val px: Array[Array[Double]] = x.map(_.map(_.toDouble))
    val py = y.map(_.toInt)

    val svm2 = new SVM(new GaussianKernel(1), 1)
    //val svm2 = new SVM(new GaussianKernel(Math.sqrt(5.0f)), 1)
    val ppy = py.map(e => if (e < 0.0) 0 else e)
    svm2.learn(px, ppy)
    svm2.finish()
    //println(s"svm2.getSupportVectors.size() = ${svm2.getSupportVectors.size()}")
    val ps = svm2.predict(px)
    //println(s"${ppy.mkString(",")}")
    //val hat = ps.map{ p => if (p > 0) 1 else 0}
    //println(s"${hat.mkString(",")}")
    val ok = ps.zip(ppy).count{ case (yhat, yo) => if (yo == yhat) true else false}.toFloat
    //val psx = svm2.predict(testX.map(_.toDouble))
    //println(s"psx = $psx")
    //println(s"svm2 error = ${(1-(ok/ppy.length))*100}% ; accuracy = ${(ok/ppy.length)*100}%")
    assert((1.0 - ok/ppy.length) <= expected_error)

    /*println(s"x = ")
    x.foreach(e => println(e.mkString(",")))
    println(s"y = ${y.mkString(",")}")
    */

  }

  def testSMILEKernels(): Unit = {
    val x = Array(
      Array( 0.5367059f,  0.83932894f ),
      Array( 0.57476383f, 0.53008604f ),
      Array( 0.13209833f, 1.1646498f ),
      Array( 0.20469487f, 0.5795707f ),
      Array( 0.7630582f,  0.7163192f ),
      Array( -0.19990304f,0.22737111f ),
      Array( -0.27267462f,1.3116679f ),
      Array( 0.9706229f,  0.8549546f ),
      Array( 0.050691355f,0.6952493f ),
      Array( 0.7597167f,  0.62054086f ),
      Array( 1.5367059f,  0.63932896f ),
      Array( 1.5747639f,  0.33008605f ),
      Array( 1.1320983f,  0.9646498f ),
      Array( 1.2046949f,  0.37957072f ),
      Array( 1.7630582f,  0.5163192f ),
      Array( 0.800097f,   0.027371129f ),
      Array( 0.7273254f,  1.111668f ),
      Array( 1.9706229f,  0.6549546f ),
      Array( 1.0506914f,  0.49524936f ),
      Array( 1.7597167f,  0.42054084f) )

    val px: Array[Array[Double]] = x.map(_.map(_.toDouble))

    val ksmile = new GaussianKernel(1)

    val K = RBFF(2f) // 2*sigma^2
    val kCache = new HashMapKernelCache(x, K)

    assert( Math.abs(ksmile.k(px(0),px(10)) - kCache.K(x(0),x(10))) < eps)
    assert( Math.abs(ksmile.k(px(11),px(15)) - kCache.K(x(11),x(15))) < eps)
    assert( Math.abs(ksmile.k(px(19),px(15)) - kCache.K(x(19),x(15))) < eps)
    assert( Math.abs(ksmile.k(px(10),px(10)) - kCache.K(x(10),x(10))) < eps)

  }

  case class TrainResults(ok: Double, error: Double, accuracy: Double, nsv:Double, numOverruns: Double)
  case class EvalData( trainX: Array[Array[Double]], trainY: Array[Double], testX: Array[Array[Double]], testY: Array[Double])
  case class AggregateResults(mean: TrainResults, sd: TrainResults, max: TrainResults, min: TrainResults, sum: TrainResults)


  def smileSVM(kernel : MercerKernel[Array[Double]], C: Double)
              (data: EvalData): TrainResults = {

    val trainX = data.trainX
    val trainY = data.trainY.map(_.toInt)
    val testX = data.testX
    val testY = data.testY.map(_.toInt)

    // Train
    val svm = new SVM(kernel, C)
    val ppy = trainY.map(e => if (e < 0.0) 0 else e)
    svm.learn(trainX, ppy)
    svm.finish()
    // Bound and unbound support vectors
    //println(s"svm2.getSupportVectors.size() = ${svm.getSupportVectors.size()}")

    // Test
    val ps = svm.predict(testX)
    //println(s"${ppy.mkString(",")}")
    val hat = ps.map{ p => if (p > 0) 1 else 0}
    //println(s"yhat = ${hat.mkString(",")}")
    val testy = testY.map(e => if (e < 0.0) 0 else e)
    //println(s"testy = ${testy.mkString(",")}")
    val cntOk = hat.zip(testy).count{ case (yhat, yo) => if (yo == yhat) true else false}
    //println(s"cntOk = $cntOk")
    val ok = cntOk.toFloat
    val accuracy = ok/testy.length
    val error = 1-accuracy
    //println(s"svm learn performance: ok = $ok ; error = $error ; accuracy = $accuracy")
    TrainResults(cntOk, error, accuracy, svm.getSupportVectors.size(), 0)
  }


  def lasvmAlg1(kernel : Kernel[Array[Float]], C: Double)(x: Array[Array[Float]], y: Array[Float]): Algo1[Array[Float]] = {
    val KCache = new HashMapKernelCache(x, kernel)
    val svm = new Algo1(KCache, x, y,
      C = C.toFloat,
      pickSample = LASVM.pickRandomSample,
      epsilon = 0.001f,
      //cacheSize: Int,
      initSampleSize = 5,
      onlineEpochs = 1,
      maxFinishIter = 1000, // Int.MaxValue, //1000,
      debug = false)
    svm
  }

  def lasvmAlg2(kernel : Kernel[Array[Float]], C: Double)(x: Array[Array[Float]], y: Array[Float]): Algo1[Array[Float]] = {
    val KCache = new HashMapKernelCache(x, kernel)
    val svm = new Algo2(KCache, x, y,
      C = C.toFloat,
      pickSample = LASVM.pickRandomSample,
      epsilon = 0.001f,
      //cacheSize: Int,
      initSampleSize = 5,
      onlineEpochs = 1,
      maxFinishIter = 1000, // Int.MaxValue, //1000,
      debug = false)
    svm
  }

  def laSVM(algo: (Array[Array[Float]], Array[Float]) => Algo[Array[Float]])
           (data: EvalData): TrainResults = {


    val trainX = data.trainX.map(_.map(_.toFloat))
    val trainY = data.trainY.map(_.toFloat)
    val testX = data.testX.map(_.map(_.toFloat))
    val testY = data.testY.map(_.toFloat)

    // Train
    val svm1 = algo(trainX, trainY)
    svm1.fit()

    // Bound and unbound support vectors
    val (non_bound_support, bound_support, incorrect_support) = svm1.numSupportVector()
    //println(s"svm1.numSupportVector() = non_bound_support: $non_bound_support, bound_support: $bound_support, incorrect_support: $incorrect_support")

    // Test
    val yhat = svm1.predictions(testX)
    //println(s"yhat = ${hat.mkString(",")}")
    val testy = testY.map(e => if (e < 0.0) 0 else e)
    //println(s"testy = ${testy.mkString(",")}")
    val cntOk = yhat.zip(testy).count{ case (hat, yo) => if (yo == hat) true else false}
    //println(s"cntOk = $cntOk")
    val ok = cntOk.toFloat
    val accuracy = ok/testy.length
    val error = 1-accuracy
    //println(s"svm learn performance: ok = $ok ; error = $error ; accuracy = $accuracy")
    TrainResults(cntOk, error, accuracy, non_bound_support + bound_support, svm1.numOverruns)
  }

  def stats(e: List[Double]): (Double, Double, Double, Double, Double) = {
    val n = e.length
    val sum = e.sum
    val mean = sum /  n
    val max = e.max
    val min = e.min
    val r2 = e.map(v => (v - mean) * (v - mean) ).sum
    val sd = Math.sqrt(r2 / n)
    (mean, sd, max, min, sum)
  }

  /**
    * Runs the experiments several times and calculates thr averages and
    * deviations.
    *
    * @see AggregateResults
    *
    * @param n - number of times experiment is executed
    * @param getData - returns the train and test data. If both are the same
    *                we are measuring the train data
    * @param trainResults - results of the execution
    * @return the mean, standard deviation, maximum and minimum values of the
    *         results
    */
  def measureError(n: Int,
                  getData: () => EvalData,
                  trainResults: EvalData => TrainResults): AggregateResults = {
     val results = (0 until n).foldLeft(List[TrainResults]()){ (acc,_) =>
       val data = getData()
       trainResults( data ) :: acc
     }
    val ok = results.map( r => r.ok)
    val nsv = results.map( r => r.nsv)
    val accuracy = results.map( r => r.accuracy)
    val error = results.map( r => r.error)
    val over = results.map( r => r.numOverruns)

    val statsOk = stats(ok)
    val statsNsv = stats(nsv)
    val statsAccuracy = stats(accuracy)
    val statsError = stats(error)
    val statsOver = stats(over)

    val mean = TrainResults(statsOk._1, statsError._1, statsAccuracy._1, statsNsv._1, statsOver._1)
    val sd   = TrainResults(statsOk._2, statsError._2, statsAccuracy._2, statsNsv._2, statsOver._2)
    val max  = TrainResults(statsOk._3, statsError._3, statsAccuracy._3, statsNsv._3, statsOver._3)
    val min  = TrainResults(statsOk._4, statsError._4, statsAccuracy._4, statsNsv._4, statsOver._4)
    val sum  = TrainResults(statsOk._5, statsError._5, statsAccuracy._5, statsNsv._5, statsOver._5)
    AggregateResults(mean, sd, max, min, sum)
  }


  def trainErrorData(x:Array[Array[Double]], y:Array[Double]): EvalData = EvalData(x, y, x, y)

  def printResult(k1: String, C: Double, r1: AggregateResults): Unit = {
    println(f"$k1 C = $C, Error: ${r1.mean.error}%1.3f +/- (${r1.sd.error}%1.3f) max = ${r1.max.error}%1.2f min = ${r1.min.error}%1.2f")
    println(f"$k1 C = $C, SV: ${r1.mean.nsv}%1.3f +/- (${r1.sd.nsv}%1.3f) max = ${r1.max.nsv}%1.0f min = ${r1.min.nsv}%1.0f")
    println(f"$k1 C = $C, overRun: ${r1.mean.numOverruns}%1.3f +/- (${r1.sd.numOverruns}%1.3f) max = ${r1.max.numOverruns}%1.0f min = ${r1.sum.numOverruns}%1.0f sum = ${r1.sum.numOverruns}%1.0f")
  }

  def main(args: Array[String]): Unit = {
    /*
    import better.files.Dsl._
    import smile.data.SparseDataset
    import smile.read

    val trainData = cwd / ".." / "data/libsvm/svmguide1"
    //val testData = cwd / ".." / "data/libsvm/svmguide1.t"
    ////val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    println(trainData)

    import smile.data._
    val datas: SparseDataset = read.libsvm(trainData.pathAsString)
    val xs = datas.toArray
    println(datas.response())
    val tp: Array[Int] = Array.fill(1)(1)
    val ys = datas.toArray(tp)
    //println(data.getDescription)
    println(ys.head)
    println(xs.head.mkString(","))
    println(ys.tail(0))
    println(xs.tail(0).mkString(","))
    datas.unzipDouble
    */

    // https://stackoverflow.com/questions/2189974/java-program-terminates-unexpectedly-without-any-error-message
    try {
      /*
      testSMILEKernels()

      testDotCache()
      testSMILE_LASVM_1()

      test2DBlobSeparableKLinear()
      test2DBlobSeparableKGaussian()

      test2DBlobNonSeparableKLinear()
      test2DBlobNonSeparableKGaussian()
      */

/*
      /*
      This is a typical run. What is interesting is that SMILE always produces
      the same results on the first run. It seems to use a fixed seed for its
      sampling. More runs will show that SMILE also produces worse average
      results.
      If we limit the troublesome finish to a maximum of 1000 iterations we
      can conclude the following:
      1. LASVM in general seems to require a large number of samples.
      2. The naive Alg1 seems to be competitive in terms of accuracy with the
         SMILE SVM version
      3. SMILE usually collects fewer support vectors
      4. SMILE is faster because Alg1 because the Alg1 'finish()' seems to
         keep selecting a given pair that require many iterations until
         one or both become non-violating (reach alpha = 0 or alpha = 1)
      5. Alg1 seems to have less variability in its error

      SMILE Gaussian Kernel (ˠ = 1,0000) C = 1.0, Error: 0.19500001072883605 +/- (0.15882379344882464) max = 0.5 min = 0.050000011920928955
      SMILE Gaussian Kernel (ˠ = 1,0000) C = 1.0, SV: 10.9 +/- (1.6401219466856725) max = 13.0 min = 8.0

      LASVM-1 RBFF(2.0) C = 1.0, Error: 0.2600000023841858 +/- (0.14628739066499336) max = 0.5 min = 0.10000002384185791
      LASVM-1 RBFF(2.0) C = 1.0, SV: 13.2 +/- (1.2489995996796797) max = 15.0 min = 11.0
       */
      val x1 = Array(
        Array( 0.5367059,  0.83932894 ),
        Array( 0.57476383, 0.53008604 ),
        Array( 0.13209833, 1.1646498 ),
        Array( 0.20469487, 0.5795707 ),
        Array( 0.7630582,  0.7163192 ),
        Array( -0.19990304,0.22737111 ),
        Array( -0.27267462,1.3116679 ),
        Array( 0.9706229,  0.8549546 ),
        Array( 0.050691355,0.6952493 ),
        Array( 0.7597167,  0.62054086 ),
        Array( 1.5367059,  0.63932896 ),
        Array( 1.5747639,  0.33008605 ),
        Array( 1.1320983,  0.9646498 ),
        Array( 1.2046949,  0.37957072 ),
        Array( 1.7630582,  0.5163192 ),
        Array( 0.800097,   0.027371129 ),
        Array( 0.7273254,  1.111668 ),
        Array( 1.9706229,  0.6549546 ),
        Array( 1.0506914,  0.49524936 ),
        Array( 1.7597167,  0.42054084) )
      val y1 = Array(1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0,-1.0)

      // Compare (x1, y1) using Alg1 and SMILE SVM
      val C = 1.0
      // SMILE
      val k1 = new GaussianKernel(1)
      val test1 = smileSVM(k1, C) _
      // Alg1
      val k2 = RBFF(2f)  // 2sigma^2
      val alg1 = lasvmAlg1(k2, C) _
      val test2 = laSVM(alg1) _
      // Alg2
      val alg2 = lasvmAlg2(k2, C) _
      val test3 = laSVM(alg2) _

      // For good comparison use 500
      val n = 500
      val r1 = measureError(n, () => trainErrorData(x1, y1), test1 )
      val r2 = measureError(n, () => trainErrorData(x1, y1), test2 )
      println(" -------------------------------------------------------------------------- ")
      val r3 = measureError(n, () => trainErrorData(x1, y1), test3 )

      printResult(s"SMILE $k1", C, r1)
      printResult(s"LASVM-1 $k2", C, r2)
      printResult(s"LASVM-2 $k2", C, r3)
*/

      import better.files.Dsl._
      val trainDataFile = cwd / ".." / "data/libsvm/svmguide1"
      //val tmp = File(JSystem.getProperty("java.io.tmpdir"))
      println(trainDataFile)
      val d = 4
      val (target, data, n1) = Load.readLightSVM(trainDataFile, d, is_libsvm_format = true)
      println(s"n1 = $n1")
      //println(s"target = ${target.mkString(",")}")
      //println(s"data= ${data.mkString(",")}")
      assert(n1 == 3089)
      assert(target(0) == 1)
      assert(target(2000-1) == 1)
      assert(target(2001-1) == -1)
      assert(target(3089-1) == -1)
      //println(s"data(0) = ${data(0).mkString(",")}")
      assert(data(0) sameElements Array(2.617300e+01f, 5.886700e+01f, -1.894697e-01f, 1.251225e+02f))

      val (mins, maxs) = Load.minMax(d, data)
      println(s"mins = ${mins.mkString(",")}")
      println(s"maxs = ${maxs.mkString(",")}")
      assert(mins sameElements Array( 0.0f, -4.555206f, -0.7524385f, 8.157474f))
      assert(maxs sameElements Array( 297.05f, 581.0731f, 0.7170606f, 180.0f))

      val scaleFunc = Load.scale(-1f, +1f, mins, maxs) _
      scaleFunc(d, data)
      val (smins, smaxs) = Load.minMax(d, data)
      println(s"smins = ${smins.mkString(",")}")
      println(s"smaxs = ${smaxs.mkString(",")}")
      assert(smins sameElements Array( -1f, -1f, -1f, -1f))
      assert(smaxs sameElements Array( +1f, +1f, +1f, +1f))


      val expected_error = 1.0 - 0.9684 // 0.968499999 Algo1 (sometimes on par with libSVM)
      //val expected_error = 1.0 - 0.96875 // 96.875 libSVM
      //val expected_error = 1.0 - 0.97325 // SMILE


      val C = 2.0
      // svmguide1: RBF kernel, libSVM gamma = 2 = SMO 1/(2sigma²) = 0.5, C = 2

      // SMILE LASVM

      val sk = new GaussianKernel(0.5)
      val svm2 = new SVM(sk, C)
      val ppy = target.map(e => if (e <= 0.0) 0 else e)
      val px = data.map(_.map(_.toDouble))
      svm2.learn(px, ppy)
      svm2.finish()
      println(s"svm2.getSupportVectors.size() = ${svm2.getSupportVectors.size()}")
      val ps = svm2.predict(px)
      val ok = ps.zip(ppy).count{ case (yhat, yo) => if (yo == yhat) true else false}.toFloat
      val accuracy1 = ok/ppy.length
      val error1 = 1.0 - accuracy1
      println(s"svm2 error = ${error1*100}% ; accuracy = ${accuracy1*100}%")
      assert(error1 <= expected_error)

      val sigma_squared_x_2 = 2.0*Math.pow(0.5f,2.0f)
      val K = RBFF(sigma_squared_x_2.toFloat)

      println(s"sigma_squared_x_2 = $sigma_squared_x_2")
      println(s"sigma_squared_x_2 = $sk")
      assert(sigma_squared_x_2  == 0.5)

      val v1 = Array(1.0,2,3.5,4,5,6,7,8,9)
      val v2 = Array(1.0,2,3,  4,5,6,7,8,9)
      val check1 = sk.k(v1,v2)
      val check2 = K.apply(v1.map(_.toFloat), v2.map(_.toFloat))
      println(s"$check1 == $check2")

      // Algo1 LASVM

      val KCache = new HashMapKernelCache(data, K)
      val y = target.map(_.toFloat)
      val svm1 = new Algo1(KCache, data, y,
        C = C.toFloat,
        pickSample = LASVM.pickRandomSample,
        epsilon = 0.001f,
        //cacheSize: Int,
        initSampleSize = 5,
        onlineEpochs = 1,
        maxFinishIter = 1000, // Int.MaxValue, //1000,
        debug = false)
      svm1.learn()

      val (n, nerror, error2) = svm1.learnErrorRate()
      println(s"svm1.error_rate() = $error2")
      println(s"svm1.error_accuracy = ${1.0 -error2}")
      assert(error2 <= expected_error)
      //svm1.dprintDiagnostics(-1)

      // Positive class
      val testv1 = data(0)
      val test1r = svm1.predictRaw(testv1)
      val test1p = svm1.predict(testv1)
      println(s"svm1.predictRaw(test1) = $test1r")
      println(s"svm1.predict(test1) = $test1p")
      assert(test1r > 0)
      assert(test1p == 1)

      // Negative class
      val testv2 = data(data.length-1)
      val test2r = svm1.predictRaw(testv2)
      val test2p = svm1.predict(testv2)
      println(s"svm1.predictRaw(test2) = $test2r")
      println(s"svm1.predict(test2) = $test2p")
      assert(test2r < 0)
      assert(test2p == 0)

      // Find out what was misclassified
      val preds = svm1.predictions(data)
      val failed = svm1.errorIdxs(preds)
      val cntFailed = failed.length
      println(s"Nº errors = $cntFailed == ${(cntFailed * 0.1) / data.length}")
      assert(cntFailed == nerror)

      // Test data set

      val testDataFile = cwd / ".." / "data/libsvm/svmguide1.t"
      val (testTarget, testData, n2) = Load.readLightSVM(testDataFile, d, is_libsvm_format = true)
      println(testDataFile)
      println(s"n2 = $n2")
      assert(n2 == 4000)
      println(s"test(0) = ${testData(0).mkString(",")} == ${testTarget(0)}")
      println(s"test(2000) = ${testData(2000).mkString(",")} == ${testTarget(2000)}")
      println(s"test(3999) = ${testData(3999).mkString(",")} == ${testTarget(3999)}")

      // Scale test data using train data statistics
      //val (mins1, maxs1) = Load.minMax(d, data)
      //val scaleFunc1 = Load.scale(-1f, +1f, mins1, maxs1) _
      //scaleFunc1(d, testData)
      scaleFunc(d, testData)
      val (tsmins, tsmaxs) = Load.minMax(d, testData)
      println(s"tsmins = ${tsmins.mkString(",")}")
      println(s"tsmaxs = ${tsmaxs.mkString(",")}")
      // Not always guaranteed (must be checked)
      //assert(tsmins sameElements Array( -0.6459183f,-0.8803571f,-1.0122918f,-1.0f))
      //assert(tsmaxs sameElements Array( 1.0f,1.0108767f,1.066937f,1.0291855f))

      // SMILE: has bugs
      // see https://github.com/haifengl/smile/issues/434

      //val ppy2 = testTarget.map(e => if (e <= 0.0) 0 else e).reverse  // reverse 0.246 error else 0.97
      //val px2 = testData.map(_.map(_.toDouble)).reverse
      val ppy2 = ppy.reverse // with ppy ok else reverse 0.6877 error
      val px2 = px.reverse // with px ok
      val svm3 = svm2 // new SVM(sk, C)
      svm3.learn(px2, ppy2)
      svm3.finish()
      println(s"svm2.getSupportVectors.size() = ${svm3.getSupportVectors.size()}")

      val ps2 = svm3.predict(px2)
      val ok2 = ps2.zip(ppy).count{ case (yhat, yo) => if (yo == yhat) true else false}.toFloat
      val accuracy3 = ok2/ppy2.length
      val error3 = 1.0 - accuracy3
      println(s"svm2 error = ${error3*100}% ; accuracy = ${accuracy3*100}%")
      println(s"$error3 <= expected error = $expected_error")
      //assert(error3 <= expected_error)

      val (n4, nerror4, error4) = svm1.errorRate(testTarget.map(_.toFloat), testData)
      println(s"n = $n4")
      println(s"svm1.error_rate() = $error4 ($nerror4)")
      println(s"svm1.error_accuracy = ${1.0 - error4}")
      assert(n4 == n2)
      println(s"$error4 <= expected error = $expected_error")
      assert(error4 <= expected_error)


    } catch {
      case ex: Throwable =>
        System.err.println("Uncaught exception - " + ex.getMessage)
        ex.printStackTrace(System.err)
    }
  }

}
