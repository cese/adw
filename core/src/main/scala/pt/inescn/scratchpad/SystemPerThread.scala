package pt.inescn.scratchpad

import java.nio.channels._

import monix.eval.Coeval
import monix.execution.CancelableFuture



/**
  * root / runMain pt.inescn.scratchpad.SystemPerThread
  *
  * @see https://stackoverflow.com/questions/33631419/replace-a-class-within-the-java-class-library-with-a-custom-version
  *      http://docs.oracle.com/javase/1.5.0/docs/api/java/util/concurrent/locks/Condition.html
  *      https://stackoverflow.com/questions/1537116/illegalmonitorstateexception-on-wait-call
  *
  */
object SystemPerThread {
  import monix.execution.Cancelable
  import monix.execution.schedulers.SchedulerService

  import scala.concurrent.{Await, Future}
  import scala.util.{Failure, Success}

  def defaultScheduler(): Unit = {
    import scala.concurrent.duration._
    import monix.execution.Scheduler.{global => scheduler}

    val c: Cancelable = scheduler.scheduleOnce(5.seconds) {
      println("Hello, world!")
    }
    // java.lang.IllegalMonitorStateException
    this.synchronized(this.wait(6000))
    c.cancel()
  }

  def explicitSchedulerTerminate1(): Unit = {
    import monix.execution.ExecutionModel.AlwaysAsyncExecution
    import monix.execution.Scheduler

    // Specify an optional ExecutionModel
    lazy val scheduler: SchedulerService =
      Scheduler.computation(
        parallelism = 10,
        executionModel = AlwaysAsyncExecution
      )

    // Issues
    // We schedule execution imediatelly so fill up the 10 threads at once
    // Delay means no more tasks can be scheduled and executed
    // These tasks must execute and terminate. If they do not, the queue
    // is starved and nothing else gets queue and executed.
    // We mus wait for termination of all tasks (executing, queued and to be queued)

    //import java.util.concurrent.TimeUnit
    import scala.concurrent.duration._
    (0 to 20).foreach { id =>
       scheduler.scheduleOnce(0.seconds) {
         val s = Math.max(1, id/2.0).toLong
         println(s"Executing task $id for $s seconds....")
         SECONDS.sleep(s)
         println(s"Task $id done")
       }
    }

    // We can wait only on queued tasks using awaitTermination
    // IMPORTANT: we must use an alternate scheduler to execute this asynchronous
    // task because the worker scheduler may have its queue full and waiting
    // pending executions (limited CPU resources), and so cannot execute
    // this wait task.
    // IMPORTANT: the time set in awaitTermination will allow for additional task
    // to be scheduled and activated unless we shutdown the scheduler
    // IMPORTANT: This does not guarantee that all tasks are completed. If the
    // time-out is to short or the waiting scheduler is interrupted, tasks are
    // lost
    import monix.execution.Scheduler.global
    println("Start waiting")
    //val termination: Future[Boolean] = scheduler.awaitTermination(10,MINUTES,global)
    val termination: Future[Boolean] = scheduler.awaitTermination(10,SECONDS,global)

    // We can execute futures as usual but we need to provide an implicit execution context
    // So lets register a simple callback
    import scala.concurrent.ExecutionContext.Implicits.{global => execContext}
    termination.onComplete( r => println(s"Wait ended with: $r"))

    // No more tasks can be scheduled
    scheduler.shutdown()

    // We now wait on active tasks (any pending scheduled tasks are ignored)
    // Note that we wait in the main thread so we do not exit before termination
    Await.result(termination, Duration.Inf)
    println(s"wait complete: ${termination.isCompleted}")
    // only true if all pending tasks have finished and shutdown was called
    println(s"scheduler terminated: ${scheduler.isTerminated}")
    println(s"scheduler shutdown: ${scheduler.isShutdown}")

    // This gives the system time to execute the callback because it
    // executes asynchronously in another context
    this.synchronized(this.wait(1000))
  }


  def explicitSchedulerTerminate2(): Unit = {
    import monix.execution.Scheduler

    // Specify an optional ExecutionModel
    // see: https://github.com/monix/monix/issues/327
    // We used a daemon scheduler to ensure we do not exit main too early
    // You can make this implicit to be automatically used elsewhere
    lazy val scheduler: SchedulerService = Scheduler.forkJoin(
      name="my-forkjoin",
      parallelism=10,
      maxThreads=128,
      daemonic=false
    )

    // Issues
    // We schedule execution imediatelly so fill up the 10 threads at once
    // Delay means no more tasks can be scheduled and executed
    // These tasks must execute and terminate. If they do not, the queue
    // is starved and nothing else gets queue and executed.
    // We mus wait for termination of all tasks (executing, queued and to be queued)

    //import java.util.concurrent.TimeUnit
    import scala.concurrent.duration._
    (0 to 20).foreach { id =>
      scheduler.scheduleOnce(0.seconds) {
        val s = Math.max(1, id/2.0).toLong
        println(s"Executing task $id for $s seconds....")
        SECONDS.sleep(s)
        println(s"Task $id done")
      }
    }

    // All tasks scheduled at this point. Execution will start later

    // We can wait only on all executing tasks. Note however that
    // at this point the scheduler may have pending tasks that
    // have not initiated execution.
    // Here any scheduled/pending tasks will NOT be executed first
    // before termination. Only executing tasks will finish
    // allow some time for scheduling
    this.synchronized(this.wait(1000))


    // Now ignore all pending ones. Note that here we do NOT
    // wait in the main thread. So we exit early and shutdown
    // is not executed correctly
    // see: https://github.com/monix/monix/issues/327
    // We used a daemon scheduler to ensure we do not exit main too early
    scheduler.shutdown()
    println("No more scheduling.")
    // only true if all pending tasks have finished and shutdown was called
    println(s"scheduler terminated: ${scheduler.isTerminated}")
    println(s"scheduler shutdown: ${scheduler.isShutdown}")

    // To wait on executing tasks explicitly use awaitTermination
    // This is only useful if we use  daemonic scheduler and don't want
    // to exit the main application before the tasks have terminated
    // To ensure all tasks have executed loop on `scheduler.isTerminated`
  }

  // https://monix.io/docs/2x/eval/task.html
  def taskTest0(): Unit ={
    import monix.eval.Task
    import monix.execution.CancelableFuture
    import concurrent.duration._

    val task1: Task[Int] = Task(1 + 1).delayExecution(1.second)

    import monix.execution.Scheduler.Implicits.global

    val result1: CancelableFuture[Int] = task1.runToFuture
    result1.onComplete {
      case Success(value) =>
        println(s"value = $value")
      case Failure(ex) =>
        System.err.println(s"ERROR: ${ex.getMessage}")
    }

    val task2 = Task { println("Effect!"); "Result" }

    task2.foreach { result => println(result) }

    // Or we can use for-comprehensions
    for (result <- task2) {
      println(result)
    }


    // Using services

    // For actual execution of tasks
    import monix.execution.Scheduler

    /*
    import java.util.concurrent.ScheduledExecutorService
    import scala.concurrent.ExecutionContext
    import java.util.concurrent.Executors
    import monix.execution.UncaughtExceptionReporter
    import monix.execution.ExecutionModel.AlwaysAsyncExecution

    // Will schedule things with delays
    lazy val scheduledExecutor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()

    // For actual execution of tasks
    lazy val executorService: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

    // Logs errors to stderr or something
    lazy val uncaughtExceptionReporter: UncaughtExceptionReporter = UncaughtExceptionReporter(executorService.reportFailure)

    lazy val scheduler1: Scheduler = Scheduler(
      scheduledExecutor,
      executorService,
      uncaughtExceptionReporter,
      AlwaysAsyncExecution
    )
    lazy val scheduler2: Scheduler = Scheduler(scheduledExecutor, executorService)
    lazy val scheduler3: Scheduler = Scheduler(executorService)

    lazy val service1: SchedulerService = {
      val javaService = Executors.newScheduledThreadPool(10)
      Scheduler(javaService)
    }

    lazy val service2 = {
      val javaService = Executors.newScheduledThreadPool(10)
      Scheduler(javaService, AlwaysAsyncExecution)
    }

    // Fork join Pool
    lazy val service3 =Scheduler.computation(parallelism=10)
    // Specify an optional ExecutionModel
    lazy val service4 = Scheduler.computation(
      parallelism = 10,
      executionModel = AlwaysAsyncExecution)

    // Cached thread pool
    lazy val service5 = Scheduler.io()
    // Giving it a name
    lazy val service6 = Scheduler.io(name="my-io")
    // Explicit execution model
    lazy val service7: SchedulerService = Scheduler.io(
      name="my-io",
      executionModel = AlwaysAsyncExecution
    )

    //lazy val service7: SchedulerService = Scheduler.cached()
    //lazy val service7: SchedulerService = Scheduler.fixedPool()
    //lazy val service7: SchedulerService = Scheduler.singleThread()
    lazy val service9 = Scheduler.fixedPool(name="my-fixed", poolSize=10)
    */
    lazy val service8 = Scheduler.singleThread(name="my-thread")

    import monix.execution.Scheduler.{global => waitScheduler}
    println("Start waiting")
    val termination: Future[Boolean] = service8.awaitTermination(1,SECONDS,waitScheduler)
    Await.result(termination, Duration.Inf)
    println("finished")
  }

  def taskTest1(): Unit = {
    import scala.concurrent.Await
    import monix.eval.Task
    import concurrent.duration._

    import monix.execution.Scheduler.Implicits.global

    //val task = Task.fork(Task.eval("Hello!"))
    val task = Task.eval("Hello!").executeAsync
    val future = task.runToFuture

    println(Await.result(future, 3.seconds))

    // Or by using foreach
    //val completed = for (r <- task) { println(s"Completed! => $r") }
    val completed = task.foreachL( r =>  println(s"Completed! => $r") ).runToFuture

    Await.result(completed, 3.seconds)

    // Not needed
    // Tasks are not daemons, so don't exit main too soon
    // java.lang.IllegalMonitorStateException
    //this.synchronized(this.wait(1000))

    // Coeval
    // "evaluated immediately on the current logical thread, if allowed by the execution model"
    val task3 = Task.eval("Hello!")

    // Replace with start
    //val tryingNow: Coeval[Either[CancelableFuture[String], String]] = task3.coeval
    //println(tryingNow) // Coeval[Either[CancelableFuture[String],String]] = ???
    //Coeval(task.runSyncMaybe)
    val tryingNow: Coeval[Either[Task[String], String]] = Coeval(task3.runSyncStep)

    tryingNow.value match {
      case Left(futureToExec) =>
        // No luck, this Task really wants async execution
        futureToExec.foreach(r => println(s"Async: $r"))
      case Right(result) =>
        println(s"Got lucky: $result")
    }

    val ta = Task(1 + 1).delayExecution(1.second)
    val tb = Task(10).delayExecution(1.second)

    //val result1 = Task.chooseFirstOfList(Seq(ta, tb)).runToFuture
    val result1 = Task.raceMany(Seq(ta, tb)).runToFuture
    result1.foreach(r => println(s"Winner: $r"))
    Await.result(result1, 3.seconds)

    import scala.util.Random

    val randomEven: Task[Int] = {
      Task.eval(Random.nextInt())
        .restartUntil(_ % 2 == 0)
    }
    val result2 = randomEven.runToFuture
    result2.foreach(println)
    Await.result(result2, 1.seconds)

    val task2 = Task(1)

    val withFinishCb = task2.doOnFinish {
      case None =>
        println("Was success!")
        Task.unit
      case Some(ex) =>
        println(s"Had failure: $ex")
        Task.unit
    }
    val result3 = withFinishCb.runToFuture
    result3.foreach(e => println(s"onFinish = $e"))
    Await.result(result3, 1.seconds)

    // Dealing with exceptions

    // NOTE: if we used an exception value (not delayed execution)
    // the the exception is thrown in the main thread
    val task4 = Task(Random.nextInt).flatMap { _ =>
      throw new IllegalStateException("Error on async.")
    }
    task4.runAsync(r => Task( println(s"Exception or not: $r") ))

    val task5 = Task(2).delayExecution(1.second)

    task5.runAsync{ r =>
      throw new IllegalStateException(r.toString)
    }

    val source = Task("Hello!").delayExecution(10.seconds)

    // Triggers error if the source does not
    // complete in 3 seconds after runOnComplete
    // Also see source.timeoutTo, source.onErrorHandleWith,
    // source.onErrorRecoverWith, source.onErrorRestart,
    //  source.onErrorRestartIf
    val timedOut: Task[String] = source.timeout(3.seconds)
    timedOut.runAsync(r => Task( println(s"Time-out a task: $r") ))

    // Tasks are not daemons, so don't exit main too soon
    // java.lang.IllegalMonitorStateException
    this.synchronized(this.wait(10000))

    println("Still alive!")
  }

  def parallelTasks0(): Unit = {
    // On evaluation a Scheduler is needed
    import monix.execution.Scheduler.Implicits.global
    // For Task
    import monix.eval._
    // For Observable
    import monix.reactive._

    val items = 0 until 1000

    // The list of all tasks needed for execution
    val tasks = items.map(i => Task(i * 2))
    // Processing in parallel
    //val aggregate = Task.gather(tasks).map(_.toList)
    // Nothing execute yet
    // Note: this simple test shows that tasks are executed in order?
    val aggregate: Task[List[Int]] = Task.gatherUnordered(tasks).map(_.toList)

    // Evaluation: asynchronous
    import monix.execution.CancelableFuture
    val r1: CancelableFuture[Unit] = aggregate.foreachL(println).runToFuture
    //=> List(0, 2, 4, 6, 8, 10, 12, 14, 16,...

    // This appears before the result is printed
    println(s"r1.isCompleted = ${r1.isCompleted}")
    // Printing occur while we wait
    this.synchronized(this.wait(1000))
    // Printing finished
    println(s"r1.isCompleted = ${r1.isCompleted}")

    // Imposing parallelism

    // The list of all tasks needed for execution
    val tasks1 = items.map(i => Task(i * 2))
    // Building batches of 10 tasks to execute in parallel:
    val batches1 = tasks1.sliding(10,10).map(b => Task.gatherUnordered(b))
    // Sequencing batches, then flattening the final result
    val aggregate1 = Task.sequence(batches1).map(_.flatten.toList)

    // Evaluation:
    val r2 = aggregate1.foreachL(println).runToFuture

    // This appears before the result is printed
    println(s"r2.isCompleted = ${r2.isCompleted}")
    // Printing occur while we wait
    this.synchronized(this.wait(1000))
    // Printing finished
    println(s"r2.isCompleted = ${r2.isCompleted}")

    // Batched Observables

    // The `bufferIntrospective` will do buffering, up to a certain
    // `bufferSize`, for as long as the downstream is busy and then
    // stream a whole sequence of all buffered events at once
    val source = Observable.range(0,1000).bufferIntrospective(256)

    // Processing in batches, powered by `Task`
    val batched: Observable[Long] = source.flatMap { items =>
      // The list of all tasks needed for execution
      val tasks = items.map(i => Task(i * 2))
      // Building batches of 10 tasks to execute in parallel:
      //val batches = tasks.sliding(10,10).map(b => Task.gather(b))
      val batches: Iterator[Task[List[Long]]] = tasks.sliding(10,10).map(b => Task.gatherUnordered(b))
      // Sequencing batches, then flattening the final result
      val aggregate: Task[Iterator[Long]] = Task.sequence(batches).map(_.flatten)
      // Converting into an observable, needed for flatMap
      //Observable.fromTask(aggregate).flatMap((ii: Iterator[Long]) => Observable.fromIterator(ii))
      Observable.fromTask(aggregate).flatMap((ii: Iterator[Long]) => Observable.apply(ii.toList:_*))
    }

    // Evaluation:
    val r4 = batched.toListL.foreachL(println).runToFuture
    //=> List(0, 2, 4, 6, 8, 10, 12, 14, 16,...

    // This appears before the result is printed
    println(s"r4.isCompleted = ${r4.isCompleted}")
    // Printing occur while we wait
    this.synchronized(this.wait(1000))
    // Printing finished
    println(s"r4.isCompleted = ${r4.isCompleted}")

    // Observable.mapAsync

    val source2 = Observable.range(0,1000)
    // The parallelism factor needs to be specified
    val processed2 = source2.mapParallelUnordered(parallelism = 10) { i =>
      Task(i * 2)
    }

    // Evaluation:
    val r5: CancelableFuture[Unit] = processed2.toListL.foreachL(println).runToFuture
    //=> List(2, 10, 0, 4, 8, 6, 12...

    // This appears before the result is printed
    println(s"r5.isCompleted = ${r5.isCompleted}")
    // Printing occur while we wait
    this.synchronized(this.wait(1000))
    // Printing finished
    println(s"r5.isCompleted = ${r5.isCompleted}")

    // Observable.mergeMap

    val source3 = Observable.range(0,1000)
    // The parallelism factor needs to be specified
    val processed3 = source3.mergeMap { i =>
      val ss: Observable[Long] = Observable.eval(i * 2)
      val oo: Observable[Long] = ss.executeAsync  // make sure we use a separate thread
      oo
    }

    // Evaluation:
    val r6: CancelableFuture[Unit] = processed3.toListL.foreachL(println).runToFuture

    // This appears before the result is printed
    println(s"r6.isCompleted = ${r6.isCompleted}")
    // Printing occur while we wait
    this.synchronized(this.wait(1000))
    // Printing finished
    println(s"r6.isCompleted = ${r6.isCompleted}")

    // Consumer.loadBalancer

    // A consumer that folds over the elements of the stream,
    // producing a sum as a result
    val sumConsumer: Consumer.Sync[Long, Long] = Consumer.foldLeft[Long,Long](0L)(_ + _)

    // For processing sums in parallel, useless of course, but can become
    // really helpful for logic sprinkled with I/O bound stuff
    val loadBalancer: Consumer[Long, Long] = Consumer.loadBalance(parallelism=10, sumConsumer)
                                                     .map(_.sum)

    val observable: Observable[Long] = Observable.range(0, 100000)
    // Our consumer turns our observable into a Task processing sums, w00t!
    val task: Task[Long] = observable.consumeWith(loadBalancer)

    // Consume the whole stream and get the result
    val r7: CancelableFuture[Long] = task.runToFuture
    r7.foreach(println)  // Returns Unit. Cannot check if complete
    //=> 4999950000

    // This appears before the result is printed
    println(s"r7.isCompleted = ${r7.isCompleted}")
    // Printing occur while we wait
    this.synchronized(this.wait(1000))
    // Printing finished
    println(s"r6.isCompleted = ${r7.isCompleted}")
  }

  def longStreamOfTasks(): Unit = {

    // JVM CPU count
    val numThreads = java.lang.Runtime.getRuntime.availableProcessors()
    println(s"numThreads = $numThreads")

    // Default work-stealing scheduler
    // Uses numThreads by default
    //import monix.execution.Scheduler.global
    // Default backed by a work stealing scheduler
    //import scala.concurrent.ExecutionContext.Implicits.{ global => execContext }

    // https://docs.scala-lang.org/overviews/core/futures.html
    /*
    scala.concurrent.context.minThreads=2
    scala.concurrent.context.maxThreads=30
    scala.concurrent.context.numThreads
    */
    /*
    System.setProperty("scala.concurrent.context.minThreads",numThreads.toString)
    System.setProperty("scala.concurrent.context.maxThreads",(numThreads*3).toString)
    System.setProperty("scala.concurrent.context.numThreads",(numThreads*2).toString)*/

    /*
    import scala.concurrent.ExecutionContext
    import java.util.concurrent.Executors
    implicit val ec = ExecutionContext.fromExecutorService(Executors.newWorkStealingPool(numThreads*2))
    */

    /* // Testing OOM (forced)
    // 14 minutes, count = 40525352
    val very_large = 999999999
    val search = Stream.from(0).take(very_large)
    println(search)
    val oom = search.map{ p => println(p) ; p * 2}
    println(oom.head)
    println(oom(10))
    println(oom(1000))
    println(oom(10000))
    println(oom(100000))
    println(oom(very_large))
    */

    import monix.eval._
    import scala.concurrent.duration._
    import scala.concurrent.blocking
    /*
      Blocking seems to increase CPU parallelism.
      We ge about 50ms for 0.5sec * 1000 = 500 sec total
      500 sec total / 10 processes = 50 sec
     */
    def task1(last:Int)(i:Long): Task[Long] = {
      Task {
        if (i == last - 1)
          println(s" - $i --------------")
        else
          println(i)
        blocking {
          MILLISECONDS.sleep(500)
        }
        i * 2
      }
    }
    /*
      NOT blocking seems to decrease CPU parallelism.
      We ge about 62ms for 0.5sec * 1000 = 500 sec total
      Should get 500 sec total / 10 processes = 50 sec
     */
    def task2(last:Int)(i:Long): Task[Long] = {
      Task {
        if (i == last - 1)
          println(s" - $i --------------")
        else
          println(i)
        MILLISECONDS.sleep(500)
        i * 2
      }
    }

    def cpuIntensive(n:Int): Unit = {
      ( 1 to n ).foreach( x => math.log( x * 2 / 3 ) )
    }

    /*
       Use top -H
     */
    def task3(last:Int)(i:Long): Task[Long] = {
      Task {
        if (i == last - 1)
          println(s" - $i --------------")
        else
          println(i)
        //cpuIntensive(15000000) // 10 threads CPU 92%
        //cpuIntensive(30000000) // 10 threads CPU 92%
        //cpuIntensive(90000000) // 10 threads CPU 94-95%
        cpuIntensive(100000000) // 10 threads CPU 95%
        i * 2
      }
    }

    // For Task
    import monix.execution.Scheduler.Implicits.global
    import monix.reactive.Observable
    import scala.concurrent.Await

    val last = 1000
    val source1 = Observable.range(0,last)
    // The parallelism factor needs to be specified
    val processed1: Observable[Long] = source1.mapParallelUnordered(parallelism = 10) { i =>
      Task {
        if (i==last-1)
          println(s" - $i --------------")
        else
          println(i)
        blocking {
          //MILLISECONDS.sleep(1500)
          MILLISECONDS.sleep(15)
        }
        i * 2
      }
    }
    val t1: Task[Unit] = processed1.completedL
    println("runToFuture")
    val r1: CancelableFuture[Unit] = t1.runToFuture
    println("Await")
    Await.result(r1, 1.minute)

    import pt.inescn.utils.Utils
    import monix.execution.CancelableFuture

    Utils.time {
      val processed1 = source1.mapParallelUnordered(parallelism = 10)( task1(last) )
      val t1: Task[Unit] = processed1.completedL
      println("runToFuture")
      val r1: CancelableFuture[Unit] = t1.runToFuture
      println("Await")
      Await.result(r1, 1.minute)
    }

    Utils.time {
      val processed1 = source1.mapParallelUnordered(parallelism = 10)( task2(last) )
      val t1: Task[Unit] = processed1.completedL
      println("runToFuture")
      val r1: CancelableFuture[Unit] = t1.runToFuture
      println("Await")
      Await.result(r1, 5.minute)
    }

    Utils.time {
      val processed1 = source1.mapParallelUnordered(parallelism = 10)( task3(last) )
      val t1: Task[Unit] = processed1.completedL
      println("runToFuture")
      val r1: CancelableFuture[Unit] = t1.runToFuture
      println("Await")
      Await.result(r1, 10.minute)
    }

    // https://www.quora.com/How-can-I-lock-a-file-in-Linux-using-java
    // "So... Linux supports 2 forms of advisory file locking, and mandatory file locking is not generally used (not
    // all filesystems support it, the filesystem must be mounted with the 'mand' option, and the file must have the
    // setgid bit set and the group execute bit unset - even then, mandatory file locking is known to have race
    // conditions associated with it).\n\n
    // The thing about advisory file locking is that it will be ignored unless something is looking for it to be set.
    // Moreover, Linux provides two different system calls that implement two different advisory locks (that are not
    // interchangable), and Java only uses one (the one based off the fcntl() system call).\n\n
    // SO... You can lock a file using the Java FileLock class, but in doing so, you should understand that only Java
    // is going to heed it, under normal circumstances (e.g., it's not going to prevent an application outside of Java
    // from modifying the file)."
    // https://stackoverflow.com/questions/128038/how-can-i-lock-a-file-using-java-if-possible
    // https://bugs.eclipse.org/bugs/show_bug.cgi?id=168394
    // https://stackoverflow.com/questions/21899889/how-to-lock-a-file-in-linux-unix-by-a-java-program
    // https://unix.stackexchange.com/questions/20104/is-there-any-way-to-prevent-deletion-of-certain-files-from-user-owned-directory
    // https://stackoverflow.com/questions/11201921/unable-to-lock-files-on-linux-using-java-nio-channels-filelock

    import java.lang.{System => JSystem}
    import better.files.File
    //import better.files.Dsl._

    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    val resultFile = tmp / "thread_test.txt"
    resultFile.delete(true)
    resultFile.createIfNotExists()
    val channel: FileChannel = resultFile.newFileChannel(File.OpenOptions.append)

    val lock = channel.lock()
    println(s"resultFile.isReadLocked() = ${resultFile.isReadLocked()}")
    println(s"resultFile.isWriteLocked() = ${resultFile.isWriteLocked()}")
    println(s"lock.isValid = ${lock.isValid}")
    println(s"lock.isShared = ${lock.isShared}")
    lock.release()
    println(s"resultFile.isReadLocked() = ${resultFile.isReadLocked()}")
    println(s"resultFile.isWriteLocked() = ${resultFile.isWriteLocked()}")
    println(s"lock.isValid = ${lock.isValid}")
    println(s"lock.isShared = ${lock.isShared}")

    /*
    // Cannot use it to lock n threads
    // Exception will be thrown here
    val lock1 = channel.lock()
    println(s"resultFile.isReadLocked() = ${resultFile.isReadLocked()}")
    println(s"resultFile.isWriteLocked() = ${resultFile.isWriteLocked()}")
    println(s"lock.isValid = ${lock.isValid}")
    println(s"lock.isShared = ${lock.isShared}")
    val lock2 = channel.lock()
    println(s"resultFile.isReadLocked() = ${resultFile.isReadLocked()}")
    println(s"resultFile.isWriteLocked() = ${resultFile.isWriteLocked()}")
    println(s"lock.isValid = ${lock.isValid}")
    println(s"lock.isShared = ${lock.isShared}")
    lock1.release()
    println(s"resultFile.isReadLocked() = ${resultFile.isReadLocked()}")
    println(s"resultFile.isWriteLocked() = ${resultFile.isWriteLocked()}")
    println(s"lock.isValid = ${lock.isValid}")
    println(s"lock.isShared = ${lock.isShared}")
    lock2.release()
    println(s"resultFile.isReadLocked() = ${resultFile.isReadLocked()}")
    println(s"resultFile.isWriteLocked() = ${resultFile.isWriteLocked()}")
    println(s"lock.isValid = ${lock.isValid}")
    println(s"lock.isShared = ${lock.isShared}")
    */

    ///val uuid = this.synchronized(UUID.randomUUID.toString)
    //Paths.get(tmp.toString, "SVMScaleRange_" + uuid).toString

    import java.io.PrintWriter
    import pt.inescn.search.ParamSearch.ParamSelect
    import scala.util.Random

    def func4(last:Long)(i:Long): Long = {
      if (i == last - 1)
        println(s" - $i --------------")
      else
        println(i)
      val rnd = Random.nextInt(100)
      cpuIntensive(rnd)
      //cpuIntensive(100)
      i
    }

    def makeTask[S,T](file: PrintWriter, func : S => T)(i: S) : Task[T] = {
      def log[U]: U => Unit = ParamSelect.logResult(file)
      Task {
        val rr: T = func(i)
        blocking(log(rr.toString))
        rr
      }
    }

    val numTasks = 10L
    val results: PrintWriter = resultFile.newPrintWriter()
    val source2 = Observable.range(0,numTasks)
    def task4: Long => Task[Long] = makeTask(results,func4(numTasks))
    Utils.time {
      val processed1 = source2.mapParallelUnordered(parallelism = 10)( task4 )
      val t1: Task[Unit] = processed1.completedL
      println("runToFuture")
      val r1: CancelableFuture[Unit] = t1.runToFuture
      println("Await")
      Await.result(r1, 1.minute)
    }

    results.close()
    println(resultFile.lineIterator.mkString(","))
    val tt = resultFile.lineIterator.map( _.toLong ).toList.sorted
    println(tt.mkString(","))
    val orderedContinuous = (0L to numTasks).zip(tt).forall{ p => p._1 == p._2 }
    println(s"orderedContinuous = $orderedContinuous")

    // println((0L to 3).zip(List(0,3,5)).forall{ p => p._1 == p._2 })

    val very_large = 99999 // 999999999
    val results3: PrintWriter = resultFile.newPrintWriter()
    def params = Stream.from(0).map(_.toLong).take(very_large)
    println(params)
    val source3 = Observable.fromIterable(params)
    Utils.time {
      val processed1 = source3.mapParallelUnordered(parallelism = 10)( task4 )
      val t1: Task[Unit] = processed1.completedL
      println("runToFuture")
      val r1: CancelableFuture[Unit] = t1.runToFuture
      println("Await")
      Await.result(r1, 1.minute)
    }

    results3.close()
    val tt3 = resultFile.lineIterator.map( _.toLong ).toList.sorted
    println(tt3.mkString(","))
    val orderedContinuous3 = (0L to very_large).zip(tt).forall{ p => p._1 == p._2 }
    println(s"orderedContinuous3 = $orderedContinuous3")

  }

  /*
      runMain pt.inescn.scratchpad.SystemPerThread
   */
  def main(args: Array[String]): Unit = {
    //defaultScheduler()
    //explicitSchedulerTerminate1()
    //explicitSchedulerTerminate2()
    //taskTest0()
    //taskTest1()
    //parallelTasks0()
    longStreamOfTasks()
  }

}
