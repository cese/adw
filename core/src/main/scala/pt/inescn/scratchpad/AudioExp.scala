package pt.inescn.scratchpad

import java.awt.{Color, Graphics}

import better.files.File.root
import javax.sound.sampled.AudioFormat.Encoding
import javax.sound.sampled.{AudioFormat, TargetDataLine}
import javax.swing.WindowConstants
import pt.inescn.dsp.Audio
import pt.inescn.dsp.Audio.AudioProcessor


/**
  * runMain pt.inescn.scratchpad.AudioExp
  *
    ffplay -f s8 -ar 8k -ac 1 testsample1.pcm
    ffplay -f s8 -ar 8k -ac 1 -autoexit testsample1.pcm
    -ar is the samplesTo rate
    -ac is the number of channels.
    -f s16le signed 16 bit little endian (see ffplay -formats)
       note that for 8 bits endianess is does nt make sense
    -autoexit automatically stop at the clip's end

    ffplay -f s16le -ar 44.1k -ac 1 -autoexit red_traffic.pcm
  */
object AudioExp {

  import javax.swing.JComponent
  import javax.swing.JFrame
  import javax.swing.JPanel
  import javax.swing.SwingUtilities
  import javax.swing.border.EmptyBorder
  import java.awt.BorderLayout
  import java.awt.Dimension

  // TODO: Add continuous sound wave plot
  class MeterGUI extends  JComponent /*JPanel*/ {
    private var meterWidth = 10
    private var amp = 0.0
    private var peak = 0.0

    def setAmplitude(namp: Double): Unit = {
      amp = Math.abs(namp)
      repaint()
    }

    def setPeak(npeak: Double): Unit = {
      peak = Math.abs(npeak)
      repaint()
    }

    def setMeterWidth(nmeterWidth: Int): Unit = {
      meterWidth = nmeterWidth
    }

    override def paintComponent(g: Graphics): Unit = {
      super.paintComponent(g)

      val w = Math.min(meterWidth, getWidth)
      val h = getHeight
      val x = getWidth / 2 - w / 2
      val y = 0

      g.setColor(Color.LIGHT_GRAY)
      g.fillRect(x, y, w, h)

      g.setColor(Color.BLACK)
      g.drawRect(x, y, w - 1, h - 1)

      //println(s"paint amp = $amp")
      val a = Math.round(amp * (h - 2)).toInt
      g.setColor(Color.GREEN)
      g.fillRect(x + 1, y + h - 1 - a, w - 2, a)

      val p = Math.round(peak * (h - 2)).toInt
      println(s"paint peak = $peak, p = $p")
      g.setColor(Color.RED)
      g.drawLine(x + 1, y + h - 1 - p, x + w - 1, y + h - 1 - p)
    }

    override def getMinimumSize: Dimension = {
      val min = super.getMinimumSize
      if (min.width < meterWidth) min.width = meterWidth
      if (min.height < meterWidth) min.height = meterWidth
      min
    }

    override def getPreferredSize: Dimension = {
      val pref = super.getPreferredSize
      pref.width = meterWidth
      pref
    }

    override def setPreferredSize(pref: Dimension): Unit = {
      super.setPreferredSize(pref)
      setMeterWidth(pref.width)
    }

    def start(meterThread: Thread): Unit = {

      SwingUtilities.invokeLater(() => {
        val frame = new JFrame("Meter")
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)

        val content = new JPanel(new BorderLayout)
        content.setBorder(new EmptyBorder(25, 50, 25, 50))

        setPreferredSize(new Dimension(9, 100))
        content.add(this, BorderLayout.CENTER)

        frame.setContentPane(content)
        frame.pack()
        frame.setLocationRelativeTo(null)
        frame.setVisible(true)

        //new Thread(new Recorder(meter)).start()
        meterThread.start()
      })
    }

  }


  // NOTE: decibels requires we us the volume (RMS) of the signal. We can gt that in
  // real-time by using an envelope follower. Note also that for a sample (even RMS)
  // value of 0, the log of that will be -infinity. In order to avoid this we shouls
  // calculate all stats in linear scale and only then scale for visualization and/or
  // comparison
  // https://github.com/cmusphinx/sphinx4/blob/master/sphinx4-core/src/main/java/edu/cmu/sphinx/frontend/util/Microphone.java
  case class Meter(format: AudioFormat, stopSampling:(Long, Long) => Boolean, gui: MeterGUI) extends AudioProcessor {

    if (! (format.getEncoding == Encoding.PCM_SIGNED) ||
          (format.getEncoding == Encoding.PCM_UNSIGNED) ||
          (format.getEncoding == Encoding.PCM_FLOAT))
    throw new RuntimeException("Only handles PCM format (signed, unsigned, float).")

    private val numChannels = format.getChannels
    private val bytesPerFrame = format.getFrameSize
    private val bytesPerChannelSample = format.getFrameSize / numChannels

    // IMPORTANT: data.getFrameLength is correct, format.getByteLength seems to have a few extra bytes
    // This may include the additional header data
    private val channels = Array.ofDim[Double](numChannels, format.getFrameSize)
    // Index of channels
    private val i = 0 until numChannels
    // We want to scale to dBA. Note that this should be calibrated for a specific MiC + ADC
    // We simplify this and simply set it to the maximum range of an encoding
    private val ref = Audio.getPCMRef(bytesPerChannelSample,
      (format.getEncoding == Encoding.PCM_SIGNED) ||
      (format.getEncoding == Encoding.PCM_FLOAT))


    def stats(l:List[Double]): List[Double] = {
      val max = l.max
      val min = l.min
      val sorted = l.sorted
      val avg = sorted.sum / (1.0 * l.length)
      val median = sorted(l.length / 2)
      List(max, min, avg, median)
    }

    def allStats(channels:IndexedSeq[List[Double]]): IndexedSeq[List[Double]] = channels.map(stats)

    def rms(l:List[Double]): Double = {
      Math.sqrt(l.map(e => e*e).sum / l.length)
    }

    def scaleTodBA(bytesPerChannelSample:Double, ref:Double, l:List[Double]): List[Double] = {
      l.map{e =>
        val tmp = Math.signum(e)*20*Math.log10(Math.abs(e)/ref)
        val ee = if (e <= 0.0) Double.NegativeInfinity else tmp
        //if (tmp.isNaN) println(s"e = $e / $ee (${Math.abs(e)}) and ref = $ref")
        tmp
      }
    }

    // https://stackoverflow.com/questions/11540094/sound-level-rms
    // https://stackoverflow.com/questions/26574326/how-to-calculate-the-level-amplitude-db-of-audio-signal-in-java/26576548
    // https://stackoverflow.com/questions/26574326/how-to-calculate-the-level-amplitude-db-of-audio-signal-in-java/26576548

    /**
      * Process the correct number bytes that have been loaded into an array
      *
      * @param data      array with the sampled data
      * @param bytesRead valid number of bytes in the array
      */
    override def process(data: Array[Byte], bytesRead: Int): Unit = {
      println(s"bytesRead = $bytesRead")
      if (bytesRead % bytesPerFrame != 0)
        throw new RuntimeException(s"numBytesRead($bytesRead) != bytesPerFrame($bytesPerFrame)")
      // Convert frame bytes to channel samples
      val frameData = Audio.getFrameDataPerChannel(
        bytesPerChannelSample,
        isBigEndian = format.isBigEndian)(data)
      // Store the samples of each channel
      val channels = i.map{ chan =>  frameData.drop(chan).sliding(1,numChannels).map(_(0)).toList }
      //val statsPerChannel = allStats(channels)
      //println(statsPerChannel.mkString(","))
      //println(scaleTodBA(bytesPerChannelSample,ref, statsPerChannel(0)).mkString(","))
      val channnelsRMS = channels.map( rms )
      val channelsPeak = channels.map( _.max )
      //println(channnelsRMS.mkString(","))
      val rms0 = (channnelsRMS(0) / ref).toFloat
      val peak0 = (channelsPeak(0) / ref).toFloat
      //println(s"rms0 = $rms0, peak0 = $peak0")

      setMeterOnEDT(rms0,peak0)
    }


    // Update via event dispatch thread
    def setMeterOnEDT(rms: Double, peak: Double): Unit = {
      SwingUtilities.invokeLater(() => {
        gui.setAmplitude(rms)
        gui.setPeak(peak)
      })
    }

    /**
      * Executed just before samples are drawn and processed.
      */
    override def start(format: AudioFormat, line: TargetDataLine): Unit = ()

    /**
      * Indicates when sampling should stop. We can stop
      * sampling based on the number of bytes read or the
      * approximate time that has been sampled in seconds.
      *
      * @param bytes number of bytes currently read
      * @param time  approximate sampling time
      * @return true to stop else false
      */
    override def stop(bytes: Long, time: Long): Boolean = {
      val r = stopSampling(bytes, time)
      r
    }

    /**
      * Executed right after sampling has been terminated.
      */
    override def close(): Unit = ()
  }


  def record(fileName:String, forTime:Long, chan:Int = 1): Unit = {
    // codec: Uncompressed 16-bit PCM audio
    // channels: Stereo
    // sampling rate: 44100 Hz
    // Bitrate : N/A
    Audio.printMixerInfo()

    val file = root/"tmp"/fileName
    file.delete(true)

    // Stop after 5 seconds have been recorded
    def stop(bytes: Long, time: Long) = time >= forTime
    // Save samples to a file
    val proc = Audio.SaveAudioTo(file, stop)

    // Format with we sample and record the audio (no conversion)
    val format = Audio.getPCMFormat(sampleRate=44100, sampleSizeInBits=16, channels=chan)
    // Lets get a target line that supports this format
    val targetLine = Audio.getDefaultTargetLine(format)

    val r: Option[(Long, Long)] = Audio.sampleMic(format, proc, targetLine)
    println(r)
  }


  // TODO: set-p the mic sampling so that it does not block
  def meterThread(gui: MeterGUI, forTime:Long, chan:Int = 1): Thread = {
    // codec: Uncompressed 16-bit PCM audio
    // channels: Stereo
    // sampling rate: 44100 Hz
    // Bitrate : N/A
    Audio.printMixerInfo()

    // Format with we sample and record the audio (no conversion)
    val format = Audio.getPCMFormat(sampleRate=44100, sampleSizeInBits=16, channels=chan)
    // Lets get a target line that supports this format
    val targetLine = Audio.getDefaultTargetLine(format)

    // Stop after forTime seconds have been recorded else keep going
    def stop(bytes: Long, time: Long) = if (forTime > 0) time >= forTime else false
    val m = Meter(format,stop,gui)

    // Prepare sampling thread
    val thrd = new Thread {
      override def run(): Unit = {
        val r: Option[(Long, Long)] = Audio.sampleMic(format, m, targetLine)
        //println(r)
      } // run
    } // thread
    thrd
  }

  def main(args: Array[String]): Unit = {

    //val seconds = 60 * 10
    //record("red_traffic.pcm", seconds)
    //record("black_traffic.pcm", seconds)
    //val seconds = 5
    //record("check.pcm", seconds)

    // Instantiate the mete GUI
    val gui = new MeterGUI()
    // Kee sampling
    val seconds = -5
    // Create the sampling thread that uses the meter
    val thrd = meterThread(gui,seconds)
    // launch the GUI (it will start te sampling when ready)
    gui.start(thrd)
  }
}
