package pt.inescn.scratchpad

object ComposeLoops {
  //Works in Dotty
  //See: https://github.com/scala/bug/issues/10716

  /*
  def func1[A, B](a: Iterable[A]): B => Unit =
    (b: B) => {
      val ai = a.iterator
      while (ai.hasNext) {
        println((b, ai.next()))
      }
    }


  def func2[A, B](a: Iterable[A], b: Iterable[B]): () => Unit =
    () => {
      val ai = a.iterator
      while (ai.hasNext) {
        func1(b)(ai.next())
      }
    }

  def func2f[A, B](a: Iterable[A], fb: ((B, A)) => Unit): B => Unit =
    (b: B) => {
      val ai = a.iterator
      while (ai.hasNext) {
        fb((b, ai.next()))
      }
    }


  def main(args: Array[String]): Unit = {
    val tf1: () => Unit = func2(List(1, 2, 3, 4), List('a', 'b', 'c'))
    tf1()
    val tf2: Any => Unit = func2f(List(1, 2, 3, 4), func1(List('a', 'b', 'c')))
    tf2(())
    val tf3: Any => Unit = func2f(List(1.1, 2.2, 3.3, 4.4), func2f(List(1, 2, 3, 4), func1(List('a', 'b', 'c'))))
    tf3(())
  }

  */
}
