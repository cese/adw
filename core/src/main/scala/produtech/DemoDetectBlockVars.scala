package produtech

import pt.inescn.etl.stream.Load._
import pt.inescn.app.RabbitConfigs.{ReceiveAppConfig}
import pt.inescn.etl.stream.RabbitMQ.RabbitConnection
import pt.inescn.utils.ADWError
import java.text.SimpleDateFormat
import java.util.Calendar

import com.rabbitmq.client.MessageProperties

import scala.collection.mutable.ArraySeq

object DemoDetectBlockVars {

  private val format = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")

  def now: String = {
    val now = Calendar.getInstance
    format.format(now.getTime)
  }

  def epochToDateTime(millisString: String) = {
    format.format(millisString.toLong)
  }

  def taskDetectCheck(f:Frame, conn: RabbitConnection, conf: ReceiveAppConfig,
                     queueName:String, publishAll: Boolean)
  : Either[ADWError, Frame] = {

    def checking(row: Row):Row = {

      /*val v = "det"
      val timestamp = "timestamp"         // or "Timestamp" - correct var assignment for compatibility
      //val v_varVal = unpack[ArraySeq[ArraySeq[Double]]](row(v))
      val v_varVal = unpack[Double](row(v))
      val v_timestamp = unpack[ArraySeq[ArraySeq[String]]](row(timestamp))*/


      val v = "det"
      val v_varVal = unpack[Double](row(v))

      val (v_timestamp, varType) = if (conn.tearID == "NewMachine") {

        val vType = "accelerometer_real"

        val ts = "timestamp"
        val int_v_ts = unpack[ArraySeq[ArraySeq[String]]](row(ts))

        val v_ts = for {
          intvts <- int_v_ts

          datetime = intvts.map(_.map(epochToDateTime))
        } yield datetime

        (v_ts, vType)
      } else {

        val vType = "accelerometer_simul"

        val ts = "Timestamp"
        val v_ts = unpack[ArraySeq[ArraySeq[String]]](row(ts))

        (v_ts, vType)
      }

      val e = for {
        varTimestamp <- v_timestamp
        varVal <- v_varVal

        data = if (publishAll) {
          /*
          Notice that at this point the frame contains windows of windows (ie, ArraySeq of ArraySeq - see unpack)
          There are 2 timestamps published:
          the first time stamp of the first window of windows
          the last time stamp of the last window of windows
         */
          val firstTS = varTimestamp(0)(0)
          val lastTSLen = varTimestamp(varTimestamp.length-1).length
          val lastTS = varTimestamp(varTimestamp.length-1)(lastTSLen-1)
          List(varType, now.toString, conn.tearID,
            firstTS, lastTS, varVal.toString)
        }
        else {
          if (varVal == 1) {
            /*
            Notice that at this point the frame contains windows of windows (ie, ArraySeq of ArraySeq - see unpack)
            There are 2 timestamps published:
            the first time stamp of the first window of windows
            the last time stamp of the last window of windows
         */
            val firstTS = varTimestamp(0)(0)
            val lastTSLen = varTimestamp(varTimestamp.length-1).length
            val lastTS = varTimestamp(varTimestamp.length-1)(lastTSLen-1)
            List(varType, now.toString, conn.tearID,
              firstTS, lastTS, varVal.toString)
          }
          else {
            List()
          }
        }

      } yield data

      e.fold(
        // print error
        err => if (conf.verbose > 0) println(err),
        { msg =>
          if (msg.nonEmpty) {
            val header = "VarType,Send_Timestamp,TearID,Reading_Timestamp_ini,Reading_Timestamp_fin,Predicted_value"
            val temp = (header concat "\n" concat msg.mkString(",")).getBytes()
            //msg.mkString(",").getBytes()
            conn.channel.basicPublish("", queueName, MessageProperties.TEXT_PLAIN, temp)
            if (conf.verbose > 0) {
              println(s"ALERT:\n $e")
            }
          }
        }
      )
      row
    }

    Right(f.map(checking))

  }
}
