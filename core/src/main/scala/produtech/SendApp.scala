package produtech

import java.time.{Duration, Instant, ZoneId, ZonedDateTime}

import scala.collection.mutable.ArraySeq
import better.files.File
import com.github.tototoshi.csv.DefaultCSVFormat
import io.circe.syntax._
import io.circe.Json
import com.rabbitmq.client.{Channel, Connection, MessageProperties}
import pt.inescn.etl.stream.Load.Frame
import pt.inescn.samplers.stream.{Enumeration, Ops}
import pt.inescn.utils.ADWError
import pt.inescn.etl.stream.Load._
import pt.inescn.etl.stream.{DataFiles, Load, RabbitMQ}
import pt.inescn.app.RabbitConfigs._
import pt.inescn.app.Utils
import pt.inescn.etl.stream.RabbitMQ.RabbitConnection


/**
  * This application simulates sensor data by sending data to the RabbitMQ
  * server. These data are loaded from one or more files. The files to load
  * are indicated providing the path of a single filename or a combination
  * of the path to a directory with the data files and a globbing selection
  * string.
  *
  * An example file to be loaded is for example:
  *   `data\inegi\ensaios_rolamentos_3`
  *
  * The files contain data in CSV format. This data is loaded, parsed and
  * converted to a message format. Here is example that is converted to
  * JSON:
  *
  * '{ "machineID":"maquina",
  *    "sensorID":"sensor",
  *    "typeOfMeasures":"RAW",
  *    "timeHorizon_begin":"1904-01-01T00:00:48.300000",
  *    "timeHorizon_end":"1904-01-01T00:00:48.300000",
  *    "acceleration_x":[ 0.000000,-0.156434,-0.309017,-0.453990,-0.587785,
  *                      -0.707107,-0.809017,-0.891007,-0.951057,-0.987688,
  *                      -1.000000,-0.987688],
  *    "acceleration_y":[ 0.000000,-0.470542,-0.936907,-1.394956,-1.840623,
  *                      -2.269953,-2.679134,-3.064535,-3.422735,-3.750555,
  *                      -4.045085,-4.303710],
  *    "acceleration_z":[-9.510565,-9.387339,-9.250772,-9.101060,-8.938415,
  *                      -8.763067,-8.575267,-8.375280,-8.163392,-7.939904,
  *                      -7.705132,-7.459412],
  *    "timestamps":["1904-01-01T00:00:48.300000","1904-01-01T00:00:48.301000",
  *                  "1904-01-01T00:00:48.302000","1904-01-01T00:00:48.303000",
  *                  "1904-01-01T00:00:48.304000","1904-01-01T00:00:48.305000",
  *                  "1904-01-01T00:00:48.306000","1904-01-01T00:00:48.307000",
  *                  "1904-01-01T00:00:48.308000","1904-01-01T00:00:48.309000",
  *                  "1904-01-01T00:00:48.310000","1904-01-01T00:00:48.311000"]}'
  *
  * We load and send stream data that holds sensor readings. Each record
  * contains, among other data, the sensor readings and their respective
  * time-stamps. Note that due to bandwidth issues, these records may be
  * buffered and sent in mini-batches. The example above shows how 12
  * records are grouped into a single message.
  *
  * The user must set either a single filename or a path and globing filter
  * in the arguments in order to load the data. To load a single file use
  * the `-dir` flag:
  * `-dir ../data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv`
  *
  * To load a list of files use the `-filer` flag:
  *  `-dir ../data/inegi/ensaios_rolamentos_3/ -filter *.csv`
  *
  * Note that if several files are used the data will be loaded in the
  * order they are selected and the data concatenated. However if the
  * `-shuffle` flag is set to true (>1) then the file loading order will
  * be random.
  *
  * When loaded usually all of the data is sent (barring any communications
  * errors). However during system's testing and debugging we may be
  * interested in sending a limited number of messages.  We can send a
  * fixed number of messages using the `-numMessages` flag. If this is a
  * valid integer this is the maximum number of messages sent (if the data
  * files have less, all data is sent). If this flag is not set all of the
  * data is sent. If the flag is set to "forever" then all of the data is
  * repeatedly send to the message broker.
  *
  * The `-bufferSize` is used to indicate how many records are buffered and
  * sent in  a single message.
  *
  * The messages are sent to specific queue of a message broker. The
  * connection configuration information is stored in a text file:
  *   `adw/core/src/main/scala/inegi/SendConfig`
  * It contains information such as the host name or IP, the message
  * broker's queue name, and access credentials. These configurations
  * may be set or overridden in the command line. These include the
  * flags: `-host`, `-password`, `-port` `-userName` and `-queueName`.
  *
  * In addition to the flags above we also have: `-verbose` (print additional
  * information to the console indicating applications actions'), `-machineID`
  * (sets the identifier of the sensor), `-sensorID` (ID of th sensor) and
  * `-timeStampStart` (the initial timestamp used in the data records, default
  * is the current date and time).
  *
  * For a complete list of command line arguments use the -help argument.
  *
  * If the parsing of the commandline fails, a list of errors are accumulated
  * abd shown. The application then terminates.
  *
  * Here are two examples of command lines that load a single file:
  * root/runMain inegi.SendApp -verbose 1 -bufferSize 2 -machineID m1 -sensorID s1 -host localhost -numMessages 100 -dir ../data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv
  * root/runMain inegi.SendApp -verbose 1 -bufferSize 2 -machineID m1 -sensorID s1 -numMessages 100 -dir ../data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv
  * root/runMain inegi.SendApp -verbose 1 -bufferSize 2 -machineID m1 -sensorID s1 -numMessages forever -throttle 30 -dir ../data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv
  *
  * If the message broker is RabbitMQ, we can inspect and manipulate the queues
  * using its Web GUI. To use this connect to the server's GUI port so:
  * `http://localhost:15672`
  * `http://WWW.XXX.YYY.ZZZ:15672/#/queues`
  */
object SendApp {


  /**
    * The functions create a timestamp stream to add to the final frame of
    * messages. If the [[ReceiveAppConfig]] entered by the user has a timeStamp
    * then the start will use that time otherwise current timeStamp is used
    * as the initial timestamp. If [[ReceiveAppConfig]] also has the timeSample
    * rate defined, then that is used as the difference between timestamps,
    * otherwise a difference of 0.5 seconds is used.
    */
  def timeStampStreamGeneration(conf: SendAppConfig): Iterator[ZonedDateTime] = {

    // Was a timestamp defined?
    if (conf.timeStampStart != "") {
      val start: Instant = Instant.parse( conf.timeStampStart )
      val zone: ZoneId = ZoneId.systemDefault()
      Enumeration.fromZone(start.atZone(zone), Duration.parse(conf.timeSample))
    }
    else {
      // otherwise use the current date and time
      //val start = Instant.now( ) // UTC only
      val start = ZonedDateTime.now() // default Zone
      Enumeration.fromZone(start, Duration.parse(conf.timeSample))
    }
  }

  object MyFormat extends DefaultCSVFormat {
    override val delimiter = ',' // ';'
  }

  // data we collect from the filenames
  case class FileFilter(damage: String, bearing: String, speed: String, exp: String)

  // take the filename attributes we want to filter on
  def classes(r: Row): FileFilter = {
    val damage = Load.unpack[String](r("damage")).right.get
    val bearing = Load.unpack[String](r("bearing")).right.get
    val speed = Load.unpack[String](r("speed")).right.get
    val exp = Load.unpack[String](r("exp")).right.get
    FileFilter(damage, bearing, speed, exp)
  }

  // select training data
  def learnFilter(r: FileFilter): Boolean = {
    (r.damage == "norm") && (r.bearing == "1" || r.bearing == "2")
  }

  // select test data
  def testFilter(r: FileFilter): Boolean = r.bearing == "3"

  /**
    * This function uses a path and filename globbing that will be used to load a set of data files to create a
    * frame with them lines. It uses filters to split files in two groups: Test and Train that can be used
    * later to do some prediction, for example.
    *
    * NOTE: hardcoded for the case of ADIRA's experimental data output
    *
    * @param conf configuration SendAppConfig
    * @param dir directory of files that will be used
    * @return frame with the messages
    */
  def createFrameWithGlobbing(conf: SendAppConfig, dir: File): Either[ADWError, Frame] = {
    val matchNames = conf.filter
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList
    val n_errors = DataFiles.countParseErrors(features1.toIterator)
    assert(n_errors == 0)
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)

    val (trainData: Iterator[(File, Row)], testData: Iterator[(File, Row)]) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, conf.shuffle)
    val emptyFrame = Frame(("", List()))
    val data = DataFiles.splitExtendCSVsData(MyFormat, trainData, testData, 0, 0, 0, conf.shuffle, emptyFrame)
    data
  }

  /**
    * This function writes the Row contents in a String (CSV format)
    * @param row row to be converted to string
    * @return string with comma-separated fields
    */
  def rowToStr(row:Row): String = {
    val colNames:List[String] = row.colNames
    val str = colNames.mkString(",") + "\n"
    val fstCol = unpack[ArraySeq[String]](row(colNames(0))).right.get

    def rowIterator (i:Int, acc:String):String = {
      (0 until row.size).foldLeft(acc){case (acc,j) =>
        if (j == row.size-1)
          acc + unpack[ArraySeq[String]](row(colNames(j))).right.get(i) + "\n"
        else
          acc + unpack[ArraySeq[String]](row(colNames(j))).right.get(i) + ","
      }
    }
    (0 until fstCol.size).foldLeft(str){(acc, j) =>
      rowIterator(j, acc)
    }
  }

  /**
    * This function gets the Byte encoding of a String (CSV format).
    * Note the message broker receives the messages encoded as Byte.
    * @param e string to get the encoding
    * @param verbose if greater than 0 print out debug messages
    * @return encoded string
    */
  def getStringByte(e: String, verbose: Int): Array[Byte] = {
    val temp = e.getBytes()
    if (verbose > 0) println(e)
    temp
  }


  /**
    * This function creates an initial frame with values that will be constant in all messages (machine ID, sensor ID,
    * type of Measures, time Stamps, counter) and after it verifies if the filter, that indicates if the app will
    * use blogging or not, has a value or not. If filter has value, the frame will be created with the files
    * that match with the filter, if filter has no value, the frame will be created with one file only. All messages
    * are converted in JSON format.
    * @param conf configuration SendAppConfig
    * @return either an error, if the created frame (with globbing) failed, or an iterator with rows (each one
    *         corresponds to a line of the frame - the messages)
    */
  def sendIterator[O](conf: SendAppConfig, conv: Row => O ): Either[ADWError, Iterator[O]] = {
    val dir: File      = File(conf.dir)
    val machineID      = Enumeration.continually(conf.machineID).toIterator
    val sensorID       = Enumeration.continually(conf.sensorID).toIterator
    val typeOfMeasures = Enumeration.continually("accelerometer").toIterator
    val timeStamps     = timeStampStreamGeneration(conf)
    // This is memory efficient, but does memoization
    //val count = Stream.from(1).map(e => e.toString).toIterator
    // Mapping on an Iterable or Iterator accumulates the data, so use lazy Ops.map
    val count          = Ops.map(Enumeration.from(1))(e => e.toString).toIterator
    val frameWithConstants = Frame(
      "timeStamps" -> timeStamps.map(e => e.toString),
      "machineID" -> machineID,
      "sensorID" -> sensorID,
      "typeOfMeasures" -> typeOfMeasures,
      "counter" -> count)

    val data: Either[ADWError, Frame] = if (conf.filter != "") {
      createFrameWithGlobbing(conf, dir)
    } else {
      val matchNames = conf.filter
      val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList
      val n_errors = DataFiles.countParseErrors(features1.toIterator)
      if (n_errors > 0)
        Left(ADWError("Error parsing filenames."))
      else {
        val features2 = DataFiles.ignoreParseErrors(features1.toIterator)
        val data = Frame.extendCSVs(MyFormat, features2)
        Right(data)
      }
    }
    data match {
      case Left(e) =>
        println(e)
        Left(e)

      case Right(s) =>
        val frame2 = s || frameWithConstants
        // Covert all columns to string
        val frameToStr = frame2.map { r: Row =>
          r.map { case (e1, e2) => (e1, unpack(e2).right.get.toString) }
        }
        if (conf.verbose > 0) println("Frame with constants and data created")
        // Create sliding window
        val read3: Iterator[O] = frameToStr.window(conf.bufferSize, conf.bufferSize)
          .iterable
          .toIterator
          // Each window is converted to a Map of arrays columns
          .map { r =>
          conv(r)
        }
        Right(read3)
    }
  }

  /**
    * This function sends the messages (all or a defined number in the
    * SendAppConfig) contained in the iterator.When all the messages are send,
    * the channel and the connection are closed.
    *
    * @param verbose if greater than 0 print out debug messages
    * @param numMessages number of messages to send (must be an integer value)
    * @param conn message broker connection
    * @param channel message broker channel
    * @param queueNames message broker queue name
    * @param iter iterator with the messages
    */
  def send[O](verbose: Int,
           numMessages: String,
           bufferSize: Int,
           conn: Connection,
           channel: Channel,
           queueNames : List[String],
           throttleWait: Long,
           iter: Either[ADWError,
           Iterator[O]], conv: (O, Int) => Array[Byte]): Unit = {

    iter match {
      case Left(e) => println(e)
      case Right(i) =>
        if (verbose > 0) println("Printing the messages")
        val ni = if (numMessages != "") i.take(numMessages.toInt) else i
        ni.foreach {
          e:O =>
            val temp:Array[Byte] = conv(e, verbose)
            queueNames.foreach{ d =>
              channel.basicPublish("", d , MessageProperties.TEXT_PLAIN, temp)
            }
            if (throttleWait > 0) Thread.sleep(throttleWait*bufferSize)
        }
    }
    if (verbose > 0) println("All messages were sent")
  }

  def connectAndSend(conf: SendAppConfig): Unit = {
    val connection = RabbitMQ.connect(conf)
    connection match {
      case Left(e) =>
        println(s"No connection possible: ${e.msg}")
      case Right(RabbitConnection(queueNames, queueName, channel, conn, _, _, _, _, _, _, _, _)) =>
        // Check if we want this keep sending data (simulates machine)
        val (delta, numMessages) = if (conf.numMessages.trim.toLowerCase == conf.FOREVER) (0, "")
        else
          (1, conf.numMessages.trim.toLowerCase)

        try {
          var cnt = 0
          while (cnt < 1) {
            val iter = SendApp.sendIterator(conf, rowToStr)
            SendApp.send(conf.verbose, numMessages, conf.bufferSize, conn, channel, queueNames, conf.throttleWait, iter, getStringByte)

            cnt += delta
          }
        }
        finally {
          channel.close()
          conn.close()
          if (conf.verbose > 0) println("Channel and Connection closed")
        }
    }

  }


  def main(args: Array[String]): Unit = {
    // Start with default and update flag values
    val filename = "src/main/java/produtech/SendConfig"
    val sendApp = SendAppConfig(filename)
    // Parse command line parameters
    val temp3 = sendApp.parse(args)

    // execute according to command line parameters
    if (temp3.showHelp) {
      temp3.printHelp()
    } else if (temp3.error.nonEmpty) {
      println(s"Errors found:")
      temp3.error.foreach(s => println("\t"+s))
      println("Terminating.")
    }
    else {
      if (temp3.dir.trim == "") {
        val temp4 = temp3.copy(error = temp3.error += "You must introduce a directory")
        if (temp3.verbose > 0) println("error:" + " " + temp4.error)
      }
      else {
        connectAndSend(temp3)
      }
    }
  }
}

