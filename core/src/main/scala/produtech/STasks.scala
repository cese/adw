package produtech

import java.{lang, util}
import java.util.{HashMap => JHashMap}
import java.util.{Map => JMap}

import collection.JavaConverters._
import pt.inescn.etl.stream.{Load, RabbitMQ}
import pt.inescn.etl.stream.RabbitMQ.{RabbitConnection, connect}
import pt.inescn.utils.{ADWError, JEither, JRight}

import scala.collection.mutable
import cats.syntax.functor._
import io.circe.{Decoder, Encoder}
import io.circe.parser.decode
import io.circe.generic.auto._
import io.circe.syntax._
import pt.inescn.app.RabbitConfigs._
import pt.inescn.etl.stream.Load.Frame
import pt.inescn.search.stream.Tasks._



/**
  * Provides Java wrappers for the platform's Scala Tasks. For an example of
  * how to use these tasks see [[produtech.DynamicInvoke]]. The [[STasks.STask]]
  * is the interface used to access and execute the pipeline functions. A pipeline
  * is nothing more than a list of tasks that will execute in sequence. Look
  * at [[produtech.DynamicCompile]] to see what methods can be applied to the
  * tasks including the execution of the pipeline.
  *
  */
object STasks {

  /**
    * This interface is used to describe a task and allow for its execution.
    * Each implementation of this task must define:
    * . The name of the task
    * . The input type and the output type (only one input parameter can be
    *   used)
    * . The name and type of the parameters (defined in a map)
    * . The conversion functions of the parameters (defined in a map, one for
    *   each parameter)
    * . The input parameter conversion function
    * . The `apply` function that will do the actual work
    *
    * The `apply` is the task function that does thw work. It will usually be
    * invoked via the `exec` method that will automatically convert the
    * parameters. It also checksif the input and output values are of the
    * appropriate type. If not a runt-time exception will be raised.
    *
    */
  trait STask {
    /**
      * Task identifier
      */
    val name: String
    /**
      * Type used for input
      */
    val inClass: java.lang.Class[_]
    /**
      * Type used for output
      */
    val outClass: java.lang.Class[_]
    /**
      * Descriptions of the parameters (variable name and variable description pairs)
      */
    val paramsInfo: JMap[String, String] = new JHashMap[String,String]()
    /**
      * Converters of the parameters (variable name and variable description pairs)
      */
    val paramsConv: Map[String, String => Any]
    /**
      * Converter for the input type
      */
    val inConv: String => Any


    /**
      * The task's function that ill do the actual computation. This will
      * act as a wrapper for the Scala code. It will assume that the type
      * are correct but should be check using the `asInstanceOf[_]`
      * method. Note that this may raise an exception if the type is
      * incorrect.
      *
      * @param params Map with the parameter name and value in the required
      *               type
      * @param i the input of the task in the required type
      * @return returns the output as is. If it is to be used in Java make
      *         sure it is compatible and/or accessible.
      */
    def apply(params: Map[String,Any], i: Any): Any

    def exec(params: JMap[String,String], i: Any): Any = {
      val inParams = params.asScala.map{ case (k,v) => (k, paramsConv(k)(v)) }.toMap

      if (! inClass.isAssignableFrom(i.getClass))
        throw new RuntimeException(s"Incorrect input type: expected $inClass but got ${i.getClass}")

      val out = apply(inParams, i)
      if (! outClass.isAssignableFrom(out.getClass))
        throw new RuntimeException(s"Incorrect output type: expected $outClass but got ${out.getClass}")
      out
    }
  }

  object STConstFrame extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "isanom"
    paramsInfo.put(p1, "isanom string: (constant) value anomaly takes")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toString)
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.UtilTasks.constFrame$
      val anomVal = params(p1).asInstanceOf[String]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = constFrame$(anomVal, input.right())
        newFrame match {
          case Right(f) =>
            JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
    /*
    import pt.inescn.samplers.stream.Enumeration
    val isanom = Enumeration.continually("b").toIterator

    val constantFrame = Frame("isanomaly" -> isanom)*/
  }


  /**
    * Test task that adds the `p1` integer parameter to the integer input `i`
    * and produces a integer output.
    * Parameters:
    *   p1 - value to add
    * Returns a Double
    */
  object STAdd extends STask {

    private val intT = new java.lang.Integer(1).getClass

    override val name: String = s"${this.getClass.getName}.exec(p1:Integer in:Integer): Integer"
    /*override val inClass: java.lang.Class[_] = intT
    override val outClass: java.lang.Class[_] = intT*/

    override val inClass: Class[_ <: Integer] = intT
    override val outClass: Class[_ <: Integer] = intT

    val p1 = "p1"
    paramsInfo.put(p1, "Integer: Add this to the input.")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i:String) => i.toInt)
    )
    override val inConv: String => Any = (in:String) => in.toInt

    override def apply(params: Map[String, Any], i: Any): Any = {
      val p1v = params(p1).asInstanceOf[Int]
      val iv = i.asInstanceOf[Int]
      val out: Int = p1v + iv
      new Integer(out)
    }
  }


  /**
    * Test task that subtracts the `p1` double parameter to the integer input
    * `i` and produces a double output.
    * Parameters:
    *   p1 - value to subtract
    * Returns a Double
    */
  object STSubtract extends STask {

    private val intT = new java.lang.Integer(1).getClass
    private val doubleT = new java.lang.Double(1.0).getClass

    override val name: String = s"${this.getClass.getName}.exec(p1:Double, in:Integer): Double"
    /*override val inClass: java.lang.Class[_] = intT
    override val outClass: java.lang.Class[_] = doubleT*/


    override val inClass: Class[_ <: Integer] = intT
    override val outClass: Class[_ <: lang.Double] = doubleT

    val p1 = "p1"
    paramsInfo.put(p1, "Double: Subtract this from the input.")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i:String) => i.toDouble)
    )
    override val inConv: String => Any = (in:String) => in.toInt

    override def apply(params: Map[String, Any], i: Any): Any = {
      val p1v = params(p1).asInstanceOf[Double]
      val iv = i.asInstanceOf[Int]
      val out = iv - p1v
      new java.lang.Double(out)
    }
  }

  /**
    * Task that load data from a CSV file.
    * Parameters:
    *   file - full path to CSV file
    * Returns a [[Frame]]
    */
  object STLoadFrame extends STask {
    import com.github.tototoshi.csv.DefaultCSVFormat
    import better.files._

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    val empty = Frame(("", List()))

    private val stringT = new java.lang.String("").getClass
    private val frameT = empty.getClass

    override val name: String = s"${this.getClass.getName}.exec(p1:String): Frame"
    /*override val inClass: java.lang.Class[_] = stringT
    override val outClass: java.lang.Class[_] = frameT*/

    override val inClass: Class[_ <: String] = stringT
    override val outClass: Class[_ <: Frame] = frameT

    val p1 = "file"
    paramsInfo.put("file", "String: CSV file to load.")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i:String) => i)
    )
    override val inConv: String => Any = (in:String) => in

    override def apply(params: Map[String, Any], i: Any): Any = {
      val p1v = params(p1).asInstanceOf[String]
      //val iv = i.asInstanceOf[String]
      val out: Frame = Frame.csv(File(p1v))
      out
    }

  }

  /**
    * Task that load data from a CSV file.
    * Parameters:
    *   file - full path to CSV file
    * Inout:
    *   Not used
    * Returns a Right of a [[Frame]] if the file can be loaded otherwise
    * returns a Left with the error.
    */
  object STMaybeLoadFrame extends STask {
    import com.github.tototoshi.csv.DefaultCSVFormat
    import better.files._

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    val empty = Frame(("", List()))

    private val stringT = new java.lang.String("").getClass
    //private val frameT = JEither.Left(ADWError("")).getClass
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String): JEither[ADWError,Frame]"
    /*override val inClass: java.lang.Class[_] = stringT
    override val outClass: java.lang.Class[_] = frameT*/


    override val inClass: Class[_ <: String] = stringT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "file"
    paramsInfo.put("file", "String: CSV file to load.")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i:String) => i)
    )
    override val inConv: String => Any = (in:String) => in

    override def apply(params: Map[String, Any], i: Any): Any = {
      val p1v = params(p1).asInstanceOf[String]
      //val iv = i.asInstanceOf[String]
      val out: Frame = Frame.csv(File(p1v))
      JEither.Right(out)
    }

  }

  /**
    * Test task that multiplies the `p1` double parameter to the integer input
    * `i` and produces a double output.
    * Parameters:
    *   p1 - value to subtract
    * Input:
    *   Not used
    * Returns a Double
    */
  object STMultiply extends STask {

    private val intT = new java.lang.Integer(1).getClass //defining class
    private val doubleT = new java.lang.Double(0.0).getClass //defining class

    override val name: String = s"${this.getClass.getName}.exec(p1:Double in:Integer): Double" //task name
    /*override val inClass: java.lang.Class[_] = intT //input class
    override val outClass: java.lang.Class[_] = doubleT //output class*/

    override val inClass: Class[_ <: Integer] = intT //input class
    override val outClass: Class[_ <: lang.Double] = doubleT //output class

    val p1 = "p1"
    paramsInfo.put(p1, "Double: Multiply this to input") //adding to paramsInfo dic the parameter information

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toDouble)
    ) // convert p1 in double

    override val inConv: String => Any = (in: String) => in.toInt //convert input in Int

    override def apply(params: Map[String, Any], i: Any): Any = { // what the task do
      val p1v = params(p1).asInstanceOf[Double]
      val iv = i.asInstanceOf[Int]
      val out = iv * p1v
      new java.lang.Double(out)
    }
  }

  /**
    * Receives data from the RabbitMQ message broker.
    * Parameters:
    *   configs - configurations to connect to the RabbitMQ broker
    * Returns
    *   Frame with the messages
    * */
  object STReceive2 extends STask {

    object GenericDerivation {
      implicit val encodeEvent: Encoder[CmdParams] = Encoder.instance {
        case foo@ReceiveAppConfig(_,_, _, _, _, _, _, _, _, _, _, _) => foo.asJson
        case foo@SendAppConfig(_, _, _, _, _, _, _, _, _, _, _, _, _, _) => throw new RuntimeException ("SendAppConfig where ReceiveAppConfig required")
      }

      implicit val decodeEvent: Decoder[CmdParams] =
        List[Decoder[CmdParams]](
          Decoder[ReceiveAppConfig].widen
        ).reduceLeft(_ or _)
    }


    private val connections = mutable.Map[String, (ReceiveAppConfig, RabbitConnection)]()

    import pt.inescn.etl.stream.Load._

    private val stringT = new java.lang.String("").getClass
    private val frameT = classOf[JEither[ADWError, Frame]]


    override val name: String = s"${this.getClass.getName}.exec(p1:String in:String): Frame" //task name
    //override val inClass: java.lang.Class[_] = stringT //input class -> it will not be used

    override val inClass: Class[_ <: String] = stringT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "configs"
    paramsInfo.put(p1, "String: Receive Configurations") // string Json
    val p2 = "connection"
    paramsInfo.put(p1, "String: Connection name (used for shutdown only)") // string Json

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) =>  decode[ReceiveAppConfig](i).right.get ), // conversion to case class
      (p2, (i: String) =>  i )
    )

    override val inConv: String => Any = (_: String) => "" // do nothing ->  this task have not an input

    override def apply(params: Map[String, Any], i: Any): Any = { // what the task do
      val recAppConf = params(p1).asInstanceOf[ReceiveAppConfig]
      //val r = connectAndMakeFrame(recAppConf, Utils.toStringIterator)//Utils.toJSONIterator)
      val r = ReceiveApp2.makeFrame(recAppConf)
      r match {
        case Left(e) => Left(e)
        case Right(conn) =>
          //case Right((frame,conn)) =>
          // record connection by name
          connections.put(paramsInfo.get(p2),(recAppConf, conn.conn))
          // Return the Frame
          JEither.Right(conn.frame)
      }
    }


    def stop(): Unit = {
      val (recAppConf, conn) = connections(paramsInfo.get(p2))
      RabbitMQ.stop(recAppConf, conn)
      //RabbitMQ.close(conn)
    }
  }


  /**
    * Send messages to RabbitMQ message broker. Used to simulate
    * sensor data.
    * Parameters:
    *  configs - Send configurations
    *  dir - directory containing the CSV files with the data to send
    *  filter - globbing filter used to select the files in the directory
    * Input:
    *   not used
    * Returns
    *  a string "all messages were sent"
    */
  /*object STSend extends STask {

    object GenericDerivation {
      implicit val encodeEvent: Encoder[CmdParams] = Encoder.instance {
        case foo@SendAppConfig(_,_,_,_,_,_,_,_,_,_,_,_,_,_) => foo.asJson
        case foo@ReceiveAppConfig(_, _, _, _, _, _, _, _, _, _, _)
      }

      implicit val decodeEvent: Decoder[CmdParams] =
        List[Decoder[CmdParams]](
          Decoder[SendAppConfig].widen
        ).reduceLeft(_ or _)
    }

    private val stringT = new java.lang.String("").getClass


    override val name: String = s"${this.getClass.getName}.exec(p1:String in:String): Frame" //task name
    /*override val inClass: java.lang.Class[_] = stringT //input class -> it will not be used
    override val outClass: java.lang.Class[_] = stringT*/

    override val inClass: Class[_ <: String] = stringT
    override val outClass: Class[_ <: String] = stringT

    val p1 = "configs"
    paramsInfo.put(p1, "String: Send Configurations") // string Json

    val p2 = "dir"
    paramsInfo.put(p2, "String: directory with files to send ")

    val p3 = "filter"
    paramsInfo.put(p3, "String: file selection. With or without Globbing") // *.csv


    override val paramsConv: Map[String, String => Any] = Map(
      //(p1, (i: String) => decode[CmdParams[SendAppConfig]](i).right.get),
      (p1, (i: String) => decode[SendAppConfig](i).right.get ),
      (p2, (i:String) => i.toString),
      (p3, (i:String) => i.toString)
    )

    override val inConv: String => Any = (_: String) => "" // do nothing ->  this task have not an input

    /*def send(conf: SendAppConfig, params: Map[String, Any]): Either[ADWError, Iterator[Json]] = {

      val dir = params(p2).asInstanceOf[String]
      val matchNames = params(p3).asInstanceOf[String]
      val temp = conf.copy(dir = dir, filter = matchNames)
      SendApp.sendIterator(temp, SendApp.rowToJSON)
    }*/
    /*diff: function returns String (CSV format) instead of String (Json)*/
    def send(conf: SendAppConfig, params: Map[String, Any]): Either[ADWError, Iterator[String]] = {

      val dir = params(p2).asInstanceOf[String]
      val matchNames = params(p3).asInstanceOf[String]
      val temp = conf.copy(dir = dir, filter = matchNames)
      SendApp.sendIterator(temp, SendApp.rowToStr)
    }

    override def apply(params: Map[String, Any], i: Any): Any = {
      val sendAppConf = params(p1).asInstanceOf[SendAppConfig]
      val dir = params(p2).asInstanceOf[String]
      val matchNames = params(p3).asInstanceOf[String]
      val temp = sendAppConf.copy(dir = dir, filter = matchNames)
      SendApp.connectAndSend(temp)
      ""
    }
  }*/


  /**
    * Utility functions used to manage strings that represent
    * sequences of tokens.
    *
    */
  object SplitColumnNames {

    /**
      * Splits a string of tokens separated by a "," and then pairs
      * every 2 consecutive tokens inti a tuple.
      *
      * @param colNames string of tokens separated by a comma
      * @return sequence of tuple of string tokens (trimmed)
      */
    def splitColNamesToSeq(colNames: String): Seq[(String, String)] = {
      val x = splitColNames(colNames)
      val tmp: Iterator[(String, String)] = x.sliding(2,2).map(s => (s.head, s(1)))
      tmp.toSeq
    }

    /**
      * Splits a string of tokens separated by a ",".
      *
      * @param colNames string of tokens separated by a comma
      * @return sequence of string tokens (trimmed)
      */
    def splitColNames(colNames: String): Seq[String] = {
      val x = colNames.split(",").flatMap { e =>
        val y = e.split(" -> ").map(_.trim)
        y
      }
      x.toSeq

    }
  }

  /**
    * Changes one or more columns names. Each rename consist of a tuple
    * column names (old -> new). The old name is replaced with the new one.
    * Parameters:
    *   ColumnRenames - column names (old -> new)
    * Input:
    *   Frame
    * Returns
    *   Either an error or a Frame with the new column names
    */
  object STRenameColumns extends STask {
    import pt.inescn.etl.stream.Load._

    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "ColumnRenames"
    paramsInfo.put(p1, "String Sequence: Rename columns.")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => SplitColumnNames.splitColNamesToSeq(i))
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.UtilTasks.renameColumns$

      val colNames = params(p1).asInstanceOf[Seq[(String,String)]]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = renameColumns$(ColumnRenames(colNames), input.right)
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }



  /**
    * Converts a Frame's label column from string to boolean.
    * Assumes the string is false if it has the value "norm"
    * or true otherwise.
    *
    * Parameters:
    *   Label - Column name tha has a label (dependent variable) whose
    *        type will be converted to boolean.
    * Input
    *   Frame
    * Output
    *   Either an error or a Frame with the label columns converted to boolean
    */
  object STConvertLabelValue extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "Label"
    paramsInfo.put(p1, "LabelName string: Label to convert to boolean")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toString)
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.UtilTasks.convertLabelValue$
      val labelName = params(p1).asInstanceOf[String]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = convertLabelValue$(ColumnName(labelName), input.right())
        newFrame match {
          case Right(f) =>
            JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }


  /**
    * Convert one or more columns of type boolean to double
    * Parameters:
    *  ColumnNames - Column names to change type
    * Input
    *   Frame
    * Returns
    *   Either an error or a Frame with the new column types
    */
  object STBoolToDouble extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "ColumnNames"
    paramsInfo.put(p1, "String Sequence: Columns (boolean) to convert to double.")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => SplitColumnNames.splitColNames(i))
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.UtilTasks.boolToDouble$

      val colNames = params(p1).asInstanceOf[Seq[String]]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = boolToDouble$(ColumnNames(colNames), input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }


  /**
    * Convert one or more columns to double
    * Parameters:
    *  ColumnNames - Column names to change type
    * Input
    *   Frame
    * Returns
    *   Either an error of a Frame with the new column types
    */
  object STToDouble extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "ColumnNames"
    paramsInfo.put(p1, "String Sequence: Columns to convert to double.")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => SplitColumnNames.splitColNames(i))
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.UtilTasks.toDouble$
      val colNames = params(p1).asInstanceOf[Seq[String]]


      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = toDouble$(ColumnNames(colNames), input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }

  /**
    * Converts a table stream to a stream of sliding windows with a specific
    * length and stride. The same columns are maintained without change.
    *
    * Parameters:
    *   size - indicates the length of the window
    *   stride - indicates stride of the window
    * Input:
    *   Frame
    * Return:
    *   Either an error or a Frame of windows
    */
  object STWindow extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1&p2:Integers in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "size"
    paramsInfo.put(p1, "Integer: Window Length")

    val p2 = "stride"
    paramsInfo.put(p2, "Integer: Window Stride")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toInt),
      (p2, (i: String) => i.toInt)
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.UtilTasks.window$

      val size = params(p1).asInstanceOf[Int]
      val stride = params(p2).asInstanceOf[Int]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = window$(SlidingWindow(size, stride), input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }


  /**
    * Calculates the mode of a set of columns
    * Parameters:
    *   ColumnNames - compute mode on these column names
    *   ColumnName - new columns created and prepends this text to original column names
    * Input:
    *   Frame
    * Returns:
    *   Either an error or a Frame with the mode values
    */
  object STModeStep extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1&p2:Integers in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT


    val p1 = "ColumnName"
    paramsInfo.put(p1, "String: Column Name")

    val p2 = "ColumnNames"
    paramsInfo.put(p2, "StringSequence: Column Names to calculate the mode")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toString),
      (p2, (i: String) => SplitColumnNames.splitColNames(i))
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.TasksFCC.modeStep$

      val colName = params(p1).asInstanceOf[String]
      val colNames = params(p2).asInstanceOf[Seq[String]]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = modeStep$(ColumnNames(colNames), ColumnName(colName), input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }

  }

  /**
    * Calculates the FFT of a sliding window stored in a Double's Vector
    *
    * Parameters:
    *   Fs          - sampling frequency (in Hz)
    *   NmbCpCoe    - Number of cepstral coefficients
    *   MaxFrq      - Maximum frequency of the filter bank(in Hz)
    *   MinFrq      - Minimum frequency  of the filter bank (in Hz)
    *   NmbFltrs    - Number of filters of the filter bank( equal to NmbCpCoe)
    *   ColumnNames - compute FFT on these column names
    *   ColumnName  - new columns created and prepends this text to original column names
    * Input:
    *   Frame
    * Returns:
    *   Either an error or a Frame with the new values
    */
  object STLfccStep extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1&p2:Integers in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "Fs"
    paramsInfo.put("Fs", "Double: sampling frequency (in Hz)")
    val p2 = "NmbFltrs"
    paramsInfo.put("NmbFltrs", "Integer: Number of filters of the filter bank( equal to NmbCpCoe)")
    val p3 = "MaxFrq"
    paramsInfo.put("MaxFrq", "Double: Maximum frequency of the filter bank(in Hz) ")
    val p4 = "MinFrq"
    paramsInfo.put("MinFrq", "Double: Minimum frequency of the filter bank(in Hz) ")
    val p5 = "NmbCpCoe"
    paramsInfo.put("NmbCpCoe", "Integer: Number of cepstral coefficients")
    val p6 = "ColumnNames"
    paramsInfo.put("ColumnNames", "String: compute FFT on these column names")
    val p7 = "ColumnName"
    paramsInfo.put("ColumnName", "String: compute FFT on these column names")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toDouble),
      (p2, (i: String) => i.toInt),
      (p3, (i: String) => i.toDouble),
      (p4, (i: String) => i.toDouble),
      (p5, (i: String) => i.toInt),
      (p6, (i: String) => SplitColumnNames.splitColNames(i)),
      (p7, (i: String) => i)  )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.TasksFCC.lfccStep$

      val fs = params(p1).asInstanceOf[Double]
      val nmbFltrs = params(p2).asInstanceOf[Int]
      val maxFrq = params(p3).asInstanceOf[Double]
      val minFrq = params(p4).asInstanceOf[Double]
      val nmbCpCoe = params(p5).asInstanceOf[Int]
      val columnNames = params(p6).asInstanceOf[Seq[String]]
      val columnName = params(p7).asInstanceOf[String]


      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = lfccStep$(fs, nmbFltrs, maxFrq, minFrq, nmbCpCoe,
          ColumnNames(columnNames), ColumnName(columnName), input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }

  /**
    * Concatenate several columns values into a single column value
    * Parameters:
    *   ColumnNames - name of column to concatenate
    *   ColumnName  - new columns created and prepends this text to original column names
    * Input:
    *   Frame
    * Returns:
    *   Either an error or a Frame with the new values
    */
  object STConcatenateStep extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1&p2:Integers in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "ColumnName"
    paramsInfo.put("ColumnName", "StringSequence: Column Name")

    val p2 = "ColumnNames"
    paramsInfo.put("ColumnNames", "String: Column Names to calculate the mode")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i),
      (p2, (i: String) => SplitColumnNames.splitColNames(i))
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.TasksFCC.concatenateStep$

      val colNames = params(p2).asInstanceOf[Seq[String]]
      val colName = params(p1).asInstanceOf[String]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = concatenateStep$(ColumnNames(colNames), ColumnName(colName), input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }


  /**
    * Apply the median filter to a set of columns
    *
    * Parameters:
    *   NmbOfFeatures - Number of colunms
    *   Stride        - step of the window
    *   ColumnNames   - Columns that will be processed
    *   ColumnName    - new columns created and prepends this text to original column names
    * Input:
    *   Frame
    * Returns:
    *   Either an error or a Frame with the new values
    */
  object  STMedianStep extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1&p2:Integers in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT


    val p1 = "NmbOfColumns"
    paramsInfo.put(p1, "Int: Number of colunms")

    val p2 = "Stride"
    paramsInfo.put(p2, "Int: step of the window ")

    val p3 = "cols"
    paramsInfo.put(p3, "String: Columns that will be processed")

    val p4 = "col"
    paramsInfo.put(p4, "String:string to combine with the colunms names")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toInt),
      (p2, (i: String) => i.toInt),
      (p3, (i: String) => SplitColumnNames.splitColNames(i)),
      (p4, (i: String) => i)
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.TasksFCC.medianStep$

      val nmbOfColumns = params(p1).asInstanceOf[Int]
      val stride = params(p2).asInstanceOf[Int]
      val cols = params(p3).asInstanceOf[Seq[String]]
      val col = params(p4).asInstanceOf[String]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = medianStep$(nmbOfColumns, stride, ColumnNames(cols), ColumnName(col), input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }

  /**
    * Update the on-line anomaly detection model.
    *
    * Parameters:
    *   FtrsNmbr        - Number of features
    *   AnomThres1      - Threshold 1
    *   AnomThres2      - Threshold 2
    *   NTrainFaseExmpl - Number of train examples
    *   ColumnNames     - Columns that will be processed
    *   ColumnName      - new columns created and prepends this text to original column names
    * Input:
    *   Frame
    * Returns:
    *   Either an error or a Frame with the new values
    */
  object STAnomalyDetectionTrainStep extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1&p2:Integers in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "FtrsNmbr"
    paramsInfo.put("FtrsNmbr", "Int: Number of features")

    val p2 = "AnomThres1"
    paramsInfo.put("AnomThres1", "Double: Threshold 1")

    val p3 = "AnomThres2"
    paramsInfo.put("AnomThres2", "Double: Threshold 2")

    val p4 = "NTrainFaseExmpl"
    paramsInfo.put("NTrainFaseExmpl", "Int: Number of train examples")

    val p5 = "cols"
    paramsInfo.put("cols", "String: Columns that will be processed")

    val p6 = "col"
    paramsInfo.put("col", "String:string to combine with the colunms names")


    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toInt),
      (p2, (i: String) => i.toDouble),
      (p3, (i: String) => i.toDouble),
      (p4, (i: String) => i.toInt),
      (p5, (i: String) => SplitColumnNames.splitColNames(i)),
      (p6, (i: String) => i)
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.TasksFCC.anomalyDetectionTrainStep$

      val ftrsNmbr = params(p1).asInstanceOf[Int]
      val anomthrs1 = params(p2).asInstanceOf[Double]
      val anomthrs2 = params(p3).asInstanceOf[Double]
      val ntrainexam = params(p4).asInstanceOf[Int]
      val cols = params(p5).asInstanceOf[Seq[String]]
      val col = params(p6).asInstanceOf[String]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = anomalyDetectionTrainStep$(ftrsNmbr, anomthrs1, anomthrs2,
          ntrainexam, ColumnNames(cols), ColumnName(col), input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }

  /**
    * Calculates the FFT of a sliding window stored in a Doubles vector.
    * Parameters:
    *   tvColumnName  - Column with oracle labeling (true label)
    *   pvColumnName  - Column with predicted labeling
    *   cols          - not used
    * Input:
    *   Frame
    * Returns:
    *   Either an error or a Frame with the new values
    */
  object STAnomanlyDetectionEvalStep extends STask {
    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1&p2:Integers in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "tvColumnName"
    paramsInfo.put("tvColumnName", "String: Column Name")

    val p2 = "pvColumnName"
    paramsInfo.put("pvColumnName", "String: Column Name")

    val p3 = "cols"
    paramsInfo.put("cols", "String: Column Names")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i),
      (p2, (i: String) => i),
      (p3, (i: String) => SplitColumnNames.splitColNames(i))
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.TasksFCC.anomDetectionEvalStep$

      val tvcolName = params(p1).asInstanceOf[String]
      val pvcolName = params(p2).asInstanceOf[String]
      val cols = params(p3).asInstanceOf[Seq[String]]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        val newFrame = anomDetectionEvalStep$(ColumnName(tvcolName), ColumnName(pvcolName), ColumnNames(cols),
          input.right())
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }

  /**
    * Rename columns to expected name - automatically identify column ID and target name.
    * Works for every machine, provided it follows the convention for variable id.
    * Notice the variable ID and timestamp ID are provided in the config file.
    * The variable ID provided is the structure however there are 3 vars, so the convention is:
    * structure_ID_X, structure_ID_Y and structure_ID_Z;
    * After acquiring the ID for the three variables, a map is created and it works as [[STRenameColumns]]
    * Parameters:
    *
    * Input:
    *   Frame
    * Returns:
    *   Either an error or a Frame with the new column names
    */
  object STGetIDRenameColumns extends STask {
    import pt.inescn.etl.stream.Load._

    private val frameT = classOf[JEither[ADWError, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

    val p1 = "ColumnRenames"
    paramsInfo.put(p1, "")

    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) => i.toString)
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.UtilTasks.renameColumns$

      def targetID(idseq: Seq[String]) = {
        val init: Seq[String] = Seq()
        val rgx = "[. \\s]"
        idseq.foldLeft(init){(acc,cur) =>
          val targetID = cur.split(rgx).takeRight(1)(0)
          acc :+ targetID
        }
      }

      //val colNames = params(p1).asInstanceOf[Seq[(String,String)]]

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {
        /**
          * get the string (map format) for the column renames
          * */
        def getRenamePairs(i:Frame) = {
          val colID = i.colIDs
          val tgtID = targetID(colID)
          val init:Seq[(String,String)] = Seq()

          colID.indices.foldLeft(init){(acc,cur) =>
            acc :+ (colID(cur),tgtID(cur))
          }
        }

        val colNames = getRenamePairs(input.right)
        val newFrame = renameColumns$(ColumnRenames(colNames), input.right)
        newFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }
  }


  /**
    * Checks if a failure is detected and publishes message to the alarm queue.
    * It is assumed that task [[STAnomalyDetectionTrainStep]] is previously performed (otherwise the detection is not
    * performed, thus there is no signal processing and no failure detected).
    *
    * Parameters:
    *   configs     - Config to establish connection to the message broker where the alarm is to be published (in case there is any)
    *   queueName   - ID of the alarm queue, where the alarms are to be published
    *   publishAll  - boolean indicating if it is to be published only the signals where a failure is detected
    *                 (publishAll = F), or all (publishAll = T) (the message has a field indicating the predicted value)
    * Input:
    *   Frame
    * Returns:
    *
    * */
  object STDetectCheck extends STask {
    import pt.inescn.etl.stream.Load._

    object GenericDerivation {
      implicit val encodeEvent: Encoder[CmdParams] = Encoder.instance {
        case foo@ReceiveAppConfig(_,_,_,_,_,_,_,_,_,_,_,_) => foo.asJson
        case foo@SendAppConfig(_, _, _, _, _, _, _, _, _, _, _, _, _, _) => throw new RuntimeException ("SendAppConfig where ReceiveAppConfig required")
      }

      implicit val decodeEvent: Decoder[CmdParams] =
        List[Decoder[CmdParams]](
          Decoder[ReceiveAppConfig].widen
        ).reduceLeft(_ or _)
    }

    private val connections = mutable.Map[String, (ReceiveAppConfig, RabbitConnection)]()

    private val frameT = classOf[JRight[String, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JRight[String, Frame]] = frameT
    override val outClass: Class[JRight[String, Frame]] = frameT

    val p1 = "configs"
    paramsInfo.put(p1, "String: Receive Configurations")
    val p2 = "queueName"
    paramsInfo.put(p2, "String: Publish queue")
    val p3 = "publishAll"
    paramsInfo.put(p3, "String: Publish all messages")


    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) =>  decode[ReceiveAppConfig](i).right.get ),
      (p2, (i: String) =>  i.toString ),
      (p3, (i: String) =>  decode[Boolean](i).right.get )
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {

      val input = i.asInstanceOf[JRight[String, Frame]]
      if (input.isRight) {

        val recAppConf = params(p1).asInstanceOf[ReceiveAppConfig]

        val conn = connect(recAppConf).right.get

        val queueName = params(p2).asInstanceOf[String]
        val publishAll = params(p3).asInstanceOf[Boolean]

        val r = DemoDetectBlockVars.taskDetectCheck(input.right, conn,recAppConf,queueName,publishAll)
        r match {
          case Left(e) => Left(e)
          case Right(f) => {
            connections.put(paramsInfo.get(p2),(recAppConf, conn))
            JEither.Right(f)
          }
        }

      }
      else {
        JEither.Left(ADWError)
      }
    }

    def stop(): Unit = {
      val (recAppConf, conn) = connections(paramsInfo.get(p2))
      RabbitMQ.stop(recAppConf, conn)
    }
  }

  /**
    * Get the ID of the independent variable (from config file), and convert the column to Double.
    * Notice [[STToDouble]] cannot be used as, as a parameter, it requires the columnID to be converted to Double;
    * This task is designed for the case of independent variables, where their ID varies and it is required to get its
    * ID from the config and only after can the toDouble function be called.
    * Parameters:
    *    configs   - File where the ID is specified
    * Input
    *   Frame
    * Returns
    *   Either an error of a Frame with the new column types
    * */
  object STToDouble_GetIDInd extends STask {
    import pt.inescn.etl.stream.Load._

    private val connections = mutable.Map[String, (ReceiveAppConfig, RabbitConnection)]()
    private val frameT = classOf[JRight[String, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JRight[String, Frame]] = frameT
    override val outClass: Class[JRight[String, Frame]] = frameT

    val p1 = "configs"
    paramsInfo.put(p1, "String: Receive Configurations")


    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) =>  decode[ReceiveAppConfig](i).right.get )
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import pt.inescn.search.stream.UtilTasks.toDouble$

      //val input = i.asInstanceOf[JRight[String, Frame]]
      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {

        val recAppConf = params(p1).asInstanceOf[ReceiveAppConfig]
        val conn = connect(recAppConf).right.get
        val cols = conn.varID
        val colNames = ColumnNames(Seq(cols))

        val doubFrame = toDouble$(colNames, input.right())
        doubFrame match {
          case Right(f) => {
            connections.put(paramsInfo.get(p1),(recAppConf, conn))
            JEither.Right(f)}
          case Left(e) => JEither.Left(e)
        }

      }
      else {
        JEither.Left(ADWError)
      }
    }

    def stop(): Unit = {
      val (recAppConf, conn) = connections(paramsInfo.get(p1))
      RabbitMQ.stop(recAppConf, conn)
    }
  }

  /**
    * Filters rows from a frame, if in that row the frame has a column with empty fields.
    * Parameters:
    *   varType   - Type of variable: "block" or "ind". String
    *
    * Input:
    *   Frame
    * Returns:
    *   Filtered Frame
    *
    */
  object STFilterFormat extends STask {
    import pt.inescn.etl.stream.Load._

    private val frameT = classOf[JEither[ADWError, Frame]]
//    private val connections = mutable.Map[String, (ReceiveAppConfig, RabbitConnection)]()

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JEither[ADWError, Frame]] = frameT
    override val outClass: Class[JEither[ADWError, Frame]] = frameT

//    val p1 = "configs"
//    paramsInfo.put(p1, "String: Receive Configurations")
//    val p2 = "varType"
//    paramsInfo.put(p1, "String: Type of variable - 'block', 'ind'")
    /*val p2 = "varTS"
    paramsInfo.put(p2, "String: Variable timestamp ID. Available in the config file")
    val p3 = "varID"
    paramsInfo.put(p3, "String: Variable ID. Available in the config file")*/

    override val paramsConv: Map[String, String => Any] = Map(
//      (p1, (i: String) =>  decode[ReceiveAppConfig](i).right.get ),
      /*(p2, (i: String) => i.toString)
      (p2, (i: String) => i.toString),
      (p3, (i: String) => i.toString)*/
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {
      import produtech.DetectFilterFormat.formatFilter

      val input = i.asInstanceOf[JEither[ADWError, Frame]]
      if (input.isRight) {

        // establish connection to load the values from config files
//        val recAppConf = params(p1).asInstanceOf[ReceiveAppConfig]
//        val conn = connect(recAppConf).right.get
//        val varTs = conn.varTs
//        val varID = conn.varID
        // close the connection once values are loaded
//        stop()

//        val varType = params(p2).asInstanceOf[String]

        val filteredFrame = formatFilter(input.right)//, varType, varTs, varID)

        filteredFrame match {
          case Right(f) => JEither.Right(f)
          case Left(e) => JEither.Left(e)
        }
      }
      else {
        JEither.Left(ADWError)
      }
    }

    /*def stop(): Unit = {
      val (recAppConf, conn) = connections(paramsInfo.get(p1))
      RabbitMQ.stop(recAppConf, conn)
    }*/

  }

  /**
    * Verifies if the values the variable takes conform to the required range.
    * Parameters:
    *   configs     - Config to establish connection to the message broker where the alert is to be published (in case there is any)
    *   queueName   - ID of the alert queue, where the alerts are to be published
    *   publishAll  - boolean indicating if it is to be published only the signals where a reading is out of the admissible range
    *                 (publishAll = T), or all (publishAll = F) (the message has a field indicating both the range and read value)
    * Input:
    *   Frame
    * Returns:
    *
    * */
  object STRangeCheck extends STask {
    import pt.inescn.etl.stream.Load._

    object GenericDerivation {
      implicit val encodeEvent: Encoder[CmdParams] = Encoder.instance {
        case foo@ReceiveAppConfig(_,_,_,_,_,_,_,_,_,_,_,_) => foo.asJson
        case foo@SendAppConfig(_, _, _, _, _, _, _, _, _, _, _, _, _, _) => throw new RuntimeException ("SendAppConfig where ReceiveAppConfig required")
      }

      implicit val decodeEvent: Decoder[CmdParams] =
        List[Decoder[CmdParams]](
          Decoder[ReceiveAppConfig].widen
        ).reduceLeft(_ or _)
    }

    private val connections = mutable.Map[String, (ReceiveAppConfig, RabbitConnection)]()

    private val frameT = classOf[JRight[String, Frame]]

    override val name: String = s"${this.getClass.getName}.exec(p1:String in:Frame): Frame"
    override val inClass: Class[JRight[String, Frame]] = frameT
    override val outClass: Class[JRight[String, Frame]] = frameT

    val p1 = "configs"
    paramsInfo.put(p1, "String: Receive Configurations")
    val p2 = "queueName"
    paramsInfo.put(p2, "String: Publish queue")
    val p3 = "publishAll"
    paramsInfo.put(p3, "String: Publish all messages")


    override val paramsConv: Map[String, String => Any] = Map(
      (p1, (i: String) =>  decode[ReceiveAppConfig](i).right.get ),
      (p2, (i: String) =>  i.toString ),
      (p3, (i: String) =>  decode[Boolean](i).right.get )
    )

    override val inConv: String => Any = (_: String) => ""

    override def apply(params: Map[String, Any], i: Any): Any = {

      val input = i.asInstanceOf[JRight[String, Frame]]
      if (input.isRight) {

        val recAppConf = params(p1).asInstanceOf[ReceiveAppConfig]

        val conn = connect(recAppConf).right.get

        val queueName = params(p2).asInstanceOf[String]
        val publishAll = params(p3).asInstanceOf[Boolean]

        val r = DemoDetectIndVars.taskRangeCheck(input.right, conn,recAppConf,queueName,publishAll)
        r match {
          case Left(e) => Left(e)
          case Right(f) => {
            connections.put(paramsInfo.get(p2),(recAppConf, conn))
            JEither.Right(f)
          }
        }

      }
      else {
        JEither.Left(ADWError)
        //JRight.Left(ADWError)
      }
    }

    def stop(): Unit = {
      val (recAppConf, conn) = connections(paramsInfo.get(p2))
      RabbitMQ.stop(recAppConf, conn)
    }
  }

  /**
    * Utilities to facilitate integration with Java.
    */
  object FrameUtils {
    import collection.JavaConverters._

    /**
      * Project columns of frame (all other columns are removed).
      *
      * @param javaList list of columns to keep
      * @param frame input Frame from which to remove columns
      * @return new Frame that only have the selected columns
      */
    def project(javaList: java.util.List[String] , frame: Frame): Frame = {
      frame.project(javaList.asScala: _*)
    }

    /**
      * Return a Java wrapper of the Scala iterator
      * @param frame Frame input
      * @return Java iterator that can be used to consume and process Frame data
      */
    def toIterator(frame: Frame): util.Iterator[Load.Row] = {
      val scala = frame.iterable.toIterator
      val java = scala.asJava
      java
    }
  }


}





