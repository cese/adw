package produtech

import pt.inescn.app.RabbitConfigs._
import pt.inescn.etl.stream.Load.{Frame, _}
import pt.inescn.etl.stream.RabbitMQ._
import pt.inescn.utils.ADWError
import pt.inescn.app.Utils


/**
  * This application consumes data from a message broker and processes it.
  * By default all of the data that is available is consumed. However during
  * system's testing and debugging we may be interested in receiving a limited
  * number of messages. We can receive a fixed number of messages using
  * the `-numMessages` flag.
  *
  * The data is sent either in JSON or String (CSV) format - see [[SendApp]] for
  * more information - notice that functions [[SendApp.send]] and [[SendApp.sendIterator]]
  * are parameterized functions [O], which specifies the encoding in JSON
  * or String (CSV) format.
  * Each of these messages may contain several records. Each record is
  * converted into a [[Row]]. This client accesses the sequence of Row via an
  * iterator.
  *
  * The messages are received from a specific queue of a message broker. The
  * connection configuration information is stored in a text file:
  *   `adw/core/src/main/scala/inegi/ReceiveConfig`
  * It contains information such as the host name or IP, the message
  * broker's queue name, and access credentials. These configurations
  * may be set or overridden in the command line. These include the
  * flags: `-host`, `-password`, `-port` `-userName` and `-queueName`.
  *
  * In addition to the flags above we also have: `-verbose` (print additional
  * information to the console indicating applications actions') and
  * `-waitMultiplier` (time in seconds to multiply to exponential back-off
  * wait time).
  *
  * Data reception is handled synchronously however this can be done either
  * using a *Pull* API or a *Callback* API. The `-iter` commandline parameter
  * may be use to set the data consumer to use the *pull* API (`poll`) or the
  * *callback* API (`CallBack`). Note that when polling we can set the maximum
  * time to wait for the next message when polling (`-waitMultiplier`). Note
  * that this parameter is only necessary when using the polling API.
  *
  * For a complete list of command line arguments use the -help argument.
  *
  * If the parsing of the commandline fails, a list of errors are accumulated
  * abd shown. The application then terminates.
  *
  * Here are two examples of command lines that load a single file:
  * root/runMain inegi.ReceiveApp -verbose 1 -iter CallBack
  * root/runMain inegi.ReceiveApp -verbose 1 -iter poll -waitMultiplier 2 -numMessages 1
  * root/runMain inegi.ReceiveApp -verbose 1 -host 111.222.333.444 -queueName produtech_critical -userName inesc -password adira_1n35c -iter CallBack -waitMultiplier 2
  *
  * @see [[pt.inescn.etl.stream.RabbitMQ]] for information on the use of the iterators and
  *      the functions used to convert the JSON messages to [[Row]] iterators
  */
object ReceiveApp {

  /**
    * Takes the configuration of a message broker and a connection to this
    * message broker and creates a [[Frame]] that can be used to read and
    * process the data available in that message broker. depending on the
    * configuration it either reads data from message broker asynchronously
    * (via a callback function) or it polls the message broker.
    *
    * Note that all data ios read and processed synchronously via the [[Frame]]
    *
    * @param conf Configuration of a message broker
    * @param con Connection to a message broker
    * @return [[Frame]] that allows a client to read process the data from a
    *        message broker.
    */

  def makeFrame(conf: ReceiveAppConfig, con: RabbitConnection): ConnectState = {
    val f = if (con.topic != "") Utils.toStringIterator_noHeader(con.varTs,con.varID) _ else Utils.toStringIterator _
    conf.iter match {
      case conf.CALLBACK => new CallbackState(con, f)
      case _ => new PollingState(con, f)
    }
  }

  /**
    * The [[ReceiveAppConfig]] is used to create a [[Frame]] that is connected
    * to a RabbitMQ (message broker) queue. The data is read through an iterator
    * that consumes the messages synchronously.
    *
    * @param conf configuration ReceiveAppConfig
    * @return If a connection can be opened a Frame that allows us to consume
    *         messages from the message broker and the connection information
    *         that can be used to close the channel later are returned.
    *         Otherwise a Left with an error message is returned.
    */
  def makeFrame(conf: ReceiveAppConfig): Either[ADWError, ConnectState] = {
    val con = connect(conf)
    con match {
      case Left(e) =>
        if (conf.verbose > 0) {
          println("Connection error occurred. Terminating.")
          println(e.msg)
        }
        Left(e)
      case Right(iterCon) =>
        val conState = makeFrame(conf, iterCon)
        if (conf.verbose > 0) {
          println("Frame with messages created. Starting to process..")
        }
        Right(conState)
    }
  }

  def processData(conf: ReceiveAppConfig): Unit = {
    makeFrame(conf).map{ case con =>
      if (conf.numMessages <= 0) {
        con.frame.iterable.foreach(println)
        //f.foreach(println)
        //f.iterable.toIterator.foreach(println)
      }
      else {
        con.frame.iterable.take(conf.numMessages).foreach(println)
        //f.iterable.toIterator.take(conf.numMessages).foreach(println)
        // Close the connection so the application can terminate
        con.stop
      }
    }
  }

  def main(args: Array[String]): Unit = {

    // Start with default and update flag values
    val filename = "src/main/java/produtech/ReceiveConfig"
    val default = ReceiveAppConfig(filename)
    // Parse command line parameters
    val recAppConf = default.parse(args)

    // execute according to command line parameters
    if (recAppConf.showHelp) {
      recAppConf.printHelp()
    } else if (recAppConf.error.nonEmpty) {
      println(s"Errors found:")
      recAppConf.error.foreach(s => println("\t"+s))
      println("Terminating.")
    }
    else {
      processData(recAppConf)
    }
  }
}