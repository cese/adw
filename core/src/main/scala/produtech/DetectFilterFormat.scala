package produtech

import pt.inescn.etl.stream.Load.Frame
import pt.inescn.utils.ADWError

object DetectFilterFormat {

  /**
    * Filters rows from a frame, if in that row the frame has a column with empty fields.
    *
    * @param f frame
    * @return filtered frame
    */
  def formatFilter (f:Frame): Either[ADWError, Frame] = {

    /**
      * For "block" variables, we do not know the column IDs beforehand.
      * We expect there are 4 columns: vibration: x, y, z; timestamp (does not use 4 as parameter)
      * For "ind" variables, we know the column IDs beforehand - which are available in the config files.
      * We expect there are 2 columns: timestamp and value reading.
      * Using [[detectFormatFilter]] we cover both cases by:
      * Column IDs are extracted from the frame, then the values in each row are obtained by mapping
      * these column IDs.
      * For all columns values then checks if the fileds are non-empty
      * //@param f frame
      * //@return filtered frame
      */
    def detectFormatFilter(f: Frame
                          ): Either[ADWError, Frame] = {

      val filteredFrame = f.filterWith { r =>
        // get column names
        val cols = r.colNames
//        val colsLen = cols.length
//        println(s"*******colsLen+++++++$colsLen")

        // instead of getting the value individually, as in the case one knows the 2 column ids:
        // val colTS: Either[String, String] = r(conn.varTs)
        // get the values for all columns by mapping
        val row_values = cols.map(rv => r(rv))

        // instead of performing the operation individually, as in the case one knows the 2 column ids:
        // eval = valTS.toString.length > 0 && valTemp.toString.length > 0
        // use fold to run through all vals and accumulate the value of the operation bu folding through the collection
        // true as the null initial param for the logic operation &&

        // notice the values in the row are returned within the Val[_], if not unpacked.
        // as unpacking is not required in this operation, we assume that an empty value,
        // has length 5, as when converted to string, without unpacking, it will be
        // "Val()" - length = 5
        val eval = row_values.forall(curr => {
          curr.toString.length > 5
        })
        eval
      }

      Right(filteredFrame)
    }

    /**
      * This routine can be used for the case we know beforehand the column IDs.
      * For "ind" variables, we know the column IDs beforehand, and these are two -
      * timestamp and value reading, whose IDs are in the config files.
      *
      * This function is to be deprecated.
      *
      * @param f
      * @return
      */
    def detectValuesFormatInd(f: Frame,
                              varTs: String, varID: String
                             ): Either[ADWError, Frame] = {

      val filteredFrame = f.filterWith { r =>
        val colTS: Either[String, String] = r(varTs)
        val colTemp: Either[String, String] = r(varID)

        val t = for {
          valTS <- colTS
          valTemp <- colTemp

          eval = valTS.toString.length > 0 && valTemp.toString.length > 0

        } yield eval

        t.fold(_ => false, s => s)
      }
      Right(filteredFrame)
    }

    val filteredFrame = detectFormatFilter(f)

    filteredFrame
  }

}
