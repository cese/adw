package produtech

import scala.collection.mutable.ArraySeq
import io.circe.Json
import io.circe.syntax._
import pt.inescn.etl.stream.Load._
import pt.inescn.etl.stream.RabbitMQ
import pt.inescn.app.RabbitConfigs._
import pt.inescn.etl.stream.RabbitMQ.RabbitConnection


/**
  * This application works like SendApp but differs in the output -
  * the messages are encoded as String in the CSV rather than Json.
  * As such, connection function connectAndSend was kept, whereas the
  * other required functions are called from SendApp.
  *
  * Here an example of a command line that loads a single file:
  * root/runMain produtech.SendApp2 -verbose 1 -bufferSize 2 -machineID m1 -sensorID s1 -numMessages 3 -dir ../data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv
  */

object SendApp2 {

  /**
    * This function encodes a Row iterator into Json format.
    * @param row row iterator to be converted
    * @return Json string
    */

  def rowToJSON (row: Row ): Json = {
    val x: Map[String, ArraySeq[String]] = row.r.map { case (e1, e2) =>
      (e1, unpack[ArraySeq[String]](e2).right.get)
    }
    val json = x.asJson
    json
  }

  /**
    * This function gets the Byte encoding of a String (Json format).
    * Note the message broker receives the messages encoded as Byte.
    * @param e Json string to get the encoding
    * @param verbose if greater than 0 print out debug messages
    * @return encoded Json string
    */
  def getJsonByte(e: Json, verbose: Int): Array[Byte] = {
    val temp = e.noSpaces.getBytes()
    if (verbose > 0) println(e.noSpaces)
    temp
  }

  /**
    * This function writes the Row contents in a String (CSV format)
    * @param row row to be converted to string
    * @return string with comma-separated fields
    */
  def rowToStr(row:Row): String = {
    val colNames:List[String] = row.colNames
    val str = colNames.mkString(",") + "\n"
    val fstCol = unpack[ArraySeq[String]](row(colNames(0))).right.get

    def rowIterator (i:Int, acc:String):String = {
      (0 until row.size).foldLeft(acc){case (acc,j) =>
        if (j == row.size-1)
          acc + unpack[ArraySeq[String]](row(colNames(j))).right.get(i) + "\n"
        else
          acc + unpack[ArraySeq[String]](row(colNames(j))).right.get(i) + ","
      }
    }
    (0 until fstCol.size).foldLeft(str){(acc, j) =>
      rowIterator(j, acc)
    }
  }

  /**
    * This function gets the Byte encoding of a String (CSV format).
    * Note the message broker receives the messages encoded as Byte.
    * @param e string to get the encoding
    * @param verbose if greater than 0 print out debug messages
    * @return encoded string
    */
  def getStringByte(e: String, verbose: Int): Array[Byte] = {
    val temp = e.getBytes()
    if (verbose > 0) println(e)
    temp
  }

  /**
    * This function calls the SendApp object function sendIterator and send
    */
  def connectAndSend(conf: SendAppConfig): Unit = {
    val connection = RabbitMQ.connect(conf)
    connection match {
      case Left(e) =>
        println(s"No connection possible: ${e.msg}")
      case Right(RabbitConnection(queueNames, queueName, channel, conn, _, _, _, _, _, _, _, _)) =>
        // Check if we want this keep sending data (simulates machine)
        val (delta, numMessages) = if (conf.numMessages.trim.toLowerCase == conf.FOREVER) (0, "")
        else
          (1, conf.numMessages.trim.toLowerCase)

        try {
          var cnt = 0
          while (cnt < 1) {
            //String (CSV format)
            val iter = SendApp.sendIterator(conf, rowToStr)
            SendApp.send(conf.verbose, numMessages, conf.bufferSize, conn, channel, queueNames, conf.throttleWait, iter, getStringByte)
            //String (Json format)
            /*val iter = SendApp.sendIterator(conf, rowToJSON)
            SendApp.send(conf.verbose, numMessages, conf.bufferSize, conn, channel, queueNames, conf.throttleWait, iter, getJsonByte)*/
            cnt += delta
          }
        }
        finally {
          channel.close()
          conn.close()
          if (conf.verbose > 0) println("Channel and Connection closed")
        }
    }

  }

  def main(args: Array[String]): Unit = {
    // Start with default and update flag values
    //val filename = "src/main/scala/produtech/SendConfig2"
    val filename = "src/main/scala/produtech/SendConfig2_Tests"
    val sendApp = SendAppConfig(filename)
    // Parse command line parameters
    val temp3 = sendApp.parse(args)

    // execute according to command line parameters
    if (temp3.showHelp) {
      temp3.printHelp()
    } else if (temp3.error.nonEmpty) {
      println(s"Errors found:")
      temp3.error.foreach(s => println("\t"+s))
      println("Terminating.")
    }
    else {
      if (temp3.dir.trim == "") {
        val temp4 = temp3.copy(error = temp3.error += "You must introduce a directory")
        if (temp3.verbose > 0) println("error:" + " " + temp4.error)
      }
      else {
        connectAndSend(temp3)
      }
    }
  }
}