package produtech

import pt.inescn.app.RabbitConfigs._
import pt.inescn.app.Utils
import pt.inescn.etl.stream.Load.{Frame, _}
import pt.inescn.etl.stream.RabbitMQ._
import pt.inescn.utils.ADWError

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

/**
  Block Varibales call:
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear1_block
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -filename src/main/scala/produtech/ReceiveConfig2_tear1_block -vartype block
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear1_block
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -filename src/main/scala/produtech/ReceiveConfig2_tear1_block -vartype block

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear2_block
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear2_block

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear3_block
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear3_block

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear4_block
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear4_block

  Multiple files (block vars)(all machines):
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear1_block src/main/scala/produtech/ReceiveConfig2_tear2_block src/main/scala/produtech/ReceiveConfig2_tear3_block src/main/scala/produtech/ReceiveConfig2_tear4_block
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -vartype block -filename src/main/scala/produtech/ReceiveConfig2_tear1_block src/main/scala/produtech/ReceiveConfig2_tear2_block src/main/scala/produtech/ReceiveConfig2_tear3_block src/main/scala/produtech/ReceiveConfig2_tear4_block

  Independent variables call:
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters -vartype ind

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -vartype ind

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 -vartype ind

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -vartype ind

  Multiple files (ind vars):
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -vartype ind

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear2_varMeters src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear2_varMeters src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3 -vartype ind

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear3_varMeters src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear3_varMeters src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3 -vartype ind

  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear4_varMeters src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear4_varMeters src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3 -vartype ind

  Multiple files (ind vars)(all machines):
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear2_varMeters src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear3_varMeters src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear4_varMeters src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3 -vartype ind
  root/runMain produtech.ReceiveApp2 -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear2_varMeters src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear3_varMeters src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear4_varMeters src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3 -vartype ind
  * */
object ReceiveApp2 {

  /**
    * Takes the configuration of a message broker and a connection to this
    * message broker and creates a [[Frame]] that can be used to read and
    * process the data available in that message broker. depending on the
    * configuration it either reads data from message broker asynchronously
    * (via a callback function) or it polls the message broker.
    *
    * Note that all data ios read and processed synchronously via the [[Frame]]
    *
    * @param conf Configuration of a message broker
    * @param con Connection to a message broker
    * @return [[Frame]] that allows a client to read process the data from a
    *        message broker.
    */
  def makeFrame(conf: ReceiveAppConfig, con: RabbitConnection): ConnectState = {
    val f =
      if (con.tearID == "NewMachine") Utils.toJSONIterator_single _ else
        (if (con.topic != "") Utils.toStringIterator_noHeader(con.varTs,con.varID) _ else Utils.toStringIterator _)
    conf.iter match {
      case conf.CALLBACK => new CallbackState(con, f)
      case _ => new PollingState(con, f)
    }
  }

  /**
    * The [[ReceiveAppConfig]] is used to create a [[Frame]] that is connected
    * to a RabbitMQ (message broker) queue. The data is read through an iterator
    * that consumes the messages synchronously.
    *
    * @param conf configuration ReceiveAppConfig
    * @return If a connection can be opened a Frame that allows us to consume
    *         messages from the message broker and the connection information
    *         that can be used to close the channel later are returned.
    *         Otherwise a Left with an error message is returned.
    */
  def makeFrame(conf: ReceiveAppConfig): Either[ADWError, ConnectState] = {
    val con = connect(conf)
    con match {
      case Left(e) =>
        if (conf.verbose > 0) {
          println("Connection error occurred. Terminating.")
          println(e.msg)
        }
        Left(e)
      case Right(iterCon) =>
        val conState = makeFrame(conf, iterCon)
        if (conf.verbose > 0) {
          println("Frame with messages created. Starting to process..")
        }
        Right(conState)
    }
  }

  def processDataIndVars(conf: ReceiveAppConfig): Unit = {
    makeFrame(conf).map{ case con =>
      if (conf.numMessages <= 0) {
        con.frame.iterable.foreach(println)
      }
      else {
        con.frame.iterable.take(conf.numMessages).foreach(println)
        con.stop
      }
    }
  }

  def processDataBlockVars(conf: ReceiveAppConfig): Unit = {
    makeFrame(conf).map{ case con =>
      if (conf.numMessages <= 0) {
        con.frame.iterable.foreach(println)
      }
      else {
        con.frame.iterable.take(conf.numMessages).foreach(println)
        con.stop
      }
    }
  }

  /**
    * This function is used to get all filenames passed in as argument in the command line.
    * Considering that the flag filename can be anywhere, as a result is passed the remainder
    * of the array, which can be appended afterwards
    * */
  def getFilenames(args: Array[String])= {
    val init: Seq[String] = Seq()
    val remainder: Seq[String] = Seq()
    val indFile = args.indexOf("-filename")
    if (indFile>=0) {
      //check if there is a flag after the filename flag
      val indNextFlag = args.indexWhere(_ contains "-",indFile+1)
      val ubound = if (indNextFlag > indFile) indNextFlag else args.length
      ((indFile+1 until ubound).foldLeft(init) { (acc, cur) =>
        acc :+ args(cur)
      },
      (ubound until args.length).foldLeft(remainder) {(acc,cur) =>
        acc :+ args(cur)
      }
      )
    } else (init,remainder)//(init,-1)

  }

  def main(args: Array[String]): Unit = {

    if (args.contains("-help")) {
      println(ReceiveAppConfigHelp)
      return
    }

    val (filenames,remainder) = getFilenames(args)
    if (filenames.isEmpty) {
      println("Errors found:\nNo config filepath.\nTerminating.")
      return
    }

    val list = filenames.map{ f =>
      val default = ReceiveAppConfig(f)
      /*
      not assuming -filename is the last flag:
      one cannot assume the relative position of the flag, as such one needs to compose the array to parse in:
      1- args up to -filename flag
      2- appended of the current filename, f
      3- appended of the remainder of the args
      */
      val argsWithFN = args.take(args.indexOf("-filename")+1) :+ f
      val parseArgs = argsWithFN ++ remainder
      // Parse command line parameters
      val recAppConf = default.parse(parseArgs)
      // execute according to command line parameters
      if (recAppConf.error.nonEmpty) {
        val errStr = recAppConf.error.indices.foldLeft(""){(acc,cur) =>
          "\t" + acc.concat(recAppConf.error(cur))
        }
        Left("Errors found:\n" + errStr + "\nTerminating." + "\n Help:\n" + ReceiveAppConfigHelp)
      }
      else {
        val f: Future[Unit] = Future {
          if (recAppConf.varType == "ind") processDataIndVars(recAppConf) else processDataBlockVars(recAppConf)
        }
        Right(f)
      }
    }

    val (errs, futures) = list.partition{
      case Left(_) => true
      case Right(_) => false    //if this case is not exhaustive it yields an exception in the right case
    }

    if (errs.nonEmpty) {
      errs.foreach {case  Left(e) => println(e)}
    }
    else {
      val fut: Seq[Future[Unit]] = futures.map {case Right(n) => n}
      val futs = Future.sequence(fut)

      //Await.result(futs, Duration.Inf)
      Await.ready(futs, Duration.Inf)
    }

  }
}