package produtech


import java.util.{List => JList}
import java.util.{Map => JMap}
import pt.inescn.utils.JEither

import collection.JavaConverters._
import scala.language.existentials

import STasks._

/**
  * This object contains methods to:
  * . Compose a list of functions - execute one after the other passing the
  *   output of one into another
  * . Check if the parameters can be correctly converted to the expected types
  * . Check if the input and output types of a sequence of tasks are compatible
  *
  * The tasks themselves are found in [produtech.STasks].
  * Examples of usage in Java can be found in [produtech.DynamicInvoke]
  *
  * Note: functions whose names end with a number are to be used in pure scala
  * (usually marked private).The other functions serve as wrappers so that they
  * can be used in Java.
  *
  * To execute:
  *
  * sbt "root/ runMain produtech.DynamicInvoke"
  *
  * Research references:
  * https://stackoverflow.com/questions/1469958/scala-how-do-i-dynamically-instantiate-an-object-and-invoke-a-method-using-refl
  * https://stackoverflow.com/questions/2060395/is-there-any-scala-feature-that-allows-you-to-call-a-method-whose-name-is-stored
  * https://www.baeldung.com/java-method-reflection
  * https://users.scala-lang.org/t/create-class-object-dynamically-reflection/3335
  * https://lampwww.epfl.ch/~michelou/scala/using-scala-from-java.html
  */
object DynamicCompile {


  /**
    * Checks if the task `tsk` can convert the expected parameters. If it can
    * do so successfully the Scala/Java parameters are returned as a right
    * otherwise a left is returned with an error message. Note that if any
    * failure occurs we always get a left result.
    *
    * @param tsk task that will check if it can parse its parameters
    * @param parms map of parameters that will be used by the task
    * @return left with an error message or right with the converted parameters
    */
  private def taskParamsOk1(tsk: STask, parms:JMap[String,String]): Either[JList[String], JList[_]] = {
    // check if conversion of parameters are ok
    val params = parms.asScala
    // For each task collect conversion test results
    val convs = tsk.paramsInfo.asScala.map{ case(k,_) =>
      try {
        // try to convert, error results in an exception
        val v = params(k)
        val p = tsk.paramsConv(k)(v)
        // No problem
        Right(p)
      } catch {
        case e: Throwable =>
          // error found, most likely the format is incorrect
          Left(s"Task(${tsk.name}): parameter $k = $e")
      }
    }

    val errors = convs.filter{ case Left(_) => true ; case _ => false }.map(_.left.get).toList
    if (errors.nonEmpty) {
      // if we have parameter conversion errors stop
      Left(errors.asJava)
    }
    else {
      val ok = convs.filter{ case Right(_) => true ; case _ => false }.map(_.right.get).toList
      Right(ok.asJava)
    }
  }

  /**
    * Checks if the task `tsk` can convert the expected parameters. If it can
    * do so successfully the Scala/Java parameters are returned as a right
    * otherwise a left is returned with an error message. Note that if any
    * failure occurs we always get a left result.
    *
    * @param tsk task that will check if it can parse its parameters
    * @param parms map of parameters that will be used by the task
    * @return left with an error message or right with the converted parameters
    */
  def taskParamsOk(tsk: STask, parms:JMap[String,String]): JEither[JList[String], JList[_]] = {
    val isOk = taskParamsOk1(tsk, parms)
    isOk match {
      case Left(e) => JEither.Left(e)
      case Right(e) => JEither.Right(e)
    }
  }

  /**
    * Checks if the list of tasks can convert their expected parameters. If
    * they can do so successfully the Scala/Java parameters of each task are
    * returned as a list in right (ne map per element) otherwise a left is
    * returned with an error message. Errors are accumulated for each task.
    * Note that if any failure occurs we always get a left result.
    *
    * @param tsks list of task that will check if they can parse their parameters
    * @param parms list of parameters (maps) that will be used by the tasks
    * @return left with an error message or right with the converted parameters (one for each task)
    */
  private def paramsOk1(tsks: JList[STask], parms:JList[JMap[String,String]]): Either[JList[String], JList[JList[_]]] = {
    // check if conversion of parameters are ok
    val tasks = tsks.asScala.toList
    val params = parms.asScala.toList
    val task_params = tasks.zip(params)
    // For each task and its parameters collect conversion test results
    val convParams = task_params.map{ case (t,ps) => taskParamsOk1(t,ps) }
    val errors: List[String] = convParams.filter{ case Left(_) => true ; case _ => false }.flatMap(_.left.get.asScala.toList)
    if (errors.nonEmpty) {
      // if we have parameter conversion errors record it
      Left(errors.asJava)
    }
    else {
      // Otherwise record the conversion result
      val ok = convParams.filter{ case Right(_) => true ; case _ => false }.map(_.right.get)
      Right(ok.asJava)
    }
  }

  /**
    * Checks if the list of tasks can convert their expected parameters. If
    * they can do so successfully the Scala/Java parameters of each task are
    * returned as a list in right (ne map per element) otherwise a left is
    * returned with an error message. Errors are accumulated for each task.
    * Note that if any failure occurs we always get a left result.
    *
    * @param tsks list of task that will check if they can parse their parameters
    * @param parms list of parameters (maps) that will be used by the tasks
    * @return left with an error message or right with the converted parameters (one for each task)
    */
  def paramsOk(tsks: JList[STask], parms:JList[JMap[String,String]]): JEither[JList[String], JList[JList[_]]] = {
    val isOk = paramsOk1(tsks, parms)
    isOk match {
      case Left(e) => JEither.Left(e)
      case Right(e) => JEither.Right(e)
    }
  }

  /**
    * Checks if the list of tasks that form a pipe can successfully be executed in sequence.
    * It checks that the parameters can be correctly converted, that the output of the
    * prior function has the expected input type of the next function in the list. If it can
    * it return the types of the input (first task) and output (last task) of the composed
    * function.
    *
    * @param tsks list of task that will check if they can sequenced
    * @param parms list of parameters (maps) that will be used by the tasks
    * @return left with an error message or right with the expected input and output types
    */
  def canCompose(tsks: JList[STask], parms:JList[JMap[String,String]]): JEither[JList[String], JList[_]] = {

    // check if conversion of parameters are ok
    val errors = paramsOk1(tsks: JList[STask], parms:JList[JMap[String,String]])
    errors match {
      case Left(e) => JEither.Left(e)
      case Right(_) =>
        // Parameter conversion is OK
        val tasks = tsks.asScala
        // Get pairs of functions
        val pairs = tasks.sliding(2)
        val tmp = pairs.map{ t =>
          val t1 = t.head
          val t2 = t(1)
          // Check that the output of the first function is compatible with the input of the second
          if (t1.outClass != t2.inClass)
            Some(s"Task(${t1.name}):${t1.outClass} -> Task(${t2.name}(in:${t2.inClass})): incompatible types")
          else
            None
        }
        val errors = tmp.filter(_.isDefined).map(_.get).toList.asJava
        if (errors.size > 0)
        // if we have errors stop
          JEither.Left(errors)
        else {
          val expectedIn = tasks.head.inClass
          val expectedOut = tasks.last.outClass
          val expected = List(expectedIn, expectedOut).asJava
          JEither.Right(expected)
        }
    }
  }


  /**
    * Executes the tasks in sequence (order of the list). The output of one task
    * is passed onto the next and so on until the output has been generated.
    * No checks are made to see if the parameters are in the correct format nor
    * if the inputs and outputs of the tasks are compatible. Use [[canCompose]]
    * to do this.
    *
    * @param tsks list of tasks to be executed in sequence
    * @param parms list of parameters, one of reach task. Parameters are passed on
    *              as a map of key variable name and variable value
    * @param in he input for the first task
    * @return returns either a left string error message or right output value
    */
  private def composeExec1(tsks: JList[STask], parms:JList[JMap[String,String]], in:String): Either[String,Any] = {
    // Pair the task and its parameters
    val tasks = tsks.asScala.toList
    val params = parms.asScala.toList
    val task_params = tasks.zip(params)
    try {
      // Get the input (conversion may fail)
      val z = tasks.head.inConv(in)
      // Execute each task and pass output to the next one
      val result = task_params.foldLeft(z){ case (acc,(t:STask,p:JMap[String, String])) =>
        val out = t.exec(p,acc)
        // If out is Frame it will be consumed
        //System.out.println(t.name + "( p = " + p + " in = " + acc + ") => " + out)
        out
      }
      Right(result)
    } catch {
      case e:Throwable => Left(e.toString)
    }

  }

  /**
    * Executes the tasks in sequence (order of the list). The output of one task
    * is passed onto the next and so on until the output has been generated.
    * No checks are made to see if the parameters are in the correct format nor
    * if the inputs and outputs of the tasks are compatible. Use [[canCompose]]
    * to do this.
    *
    * @param tsks list of tasks to be executed in sequence
    * @param parms list of parameters, one of reach task. Parameters are passed on
    *              as a map of key variable name and variable value
    * @param in he input for the first task
    * @return returns either a left string error message or right output value
    */
  def composeExec(tsks: JList[STask], parms:JList[JMap[String,String]], in:String): JEither[String,Any] = {
    val result = composeExec1(tsks, parms, in)
    result match {
      case Left(e) => JEither.Left(e)
      case Right(e) => JEither.Right(e)
    }
  }


  /*def main(args: Array[String]): Unit = {
  }*/

}
