package produtech

import java.text.SimpleDateFormat
import java.util.Calendar

import com.rabbitmq.client.MessageProperties
import produtech.ReceiveApp2.makeFrame
import pt.inescn.app.RabbitConfigs.{ReceiveAppConfig, ReceiveAppConfigHelp}
import pt.inescn.etl.stream.RabbitMQ.RabbitConnection
import pt.inescn.search.stream.Tasks.ColumnNames
import pt.inescn.utils.ADWError
import pt.inescn.etl.stream.Load._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._

/**
  Independent variables call:
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts

  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts

  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -filename src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts

  Multiple files:
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts

  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear2_varMeters src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear2_varMeters src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts

  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear3_varMeters src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear3_varMeters src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts

  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear4_varMeters src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear4_varMeters src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts

  Multiple files (all machines):
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter CallBack -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear2_varMeters src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear3_varMeters src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear4_varMeters src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  root/runMain produtech.DemoDetectIndVars -verbose 1 -iter poll -numMessages 15 -filename src/main/scala/produtech/ReceiveConfig2_tear1_varMeters src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear2_varMeters src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear3_varMeters src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3 src/main/scala/produtech/ReceiveConfig2_tear4_varMeters src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2 src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3 -publishQueue outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts
  * */

object DemoDetectIndVars {

  case class RangeParams (lbound: Double,// = 0,
                          ubound: Double)// = 50)

  private val format = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")

  def now: String = {
    val now = Calendar.getInstance
    format.format(now.getTime)
  }

  /**
    * Function used to check flag "-publish"
    * */
  def publishTypeAll(args: Array[String])= {
    val index = args.indexOf("-publish")
    if (index>=0) {
      if (args(index+1) == "all") true else false
    }
    else false
  }

  def taskRangeCheck(f:Frame, conn: RabbitConnection, conf: ReceiveAppConfig,
                     //p: RangeParams, queueName:String, publishAll: Boolean)
                     queueName:String, publishAll: Boolean)
                    : Either[ADWError, Frame] = {

    val p = new DemoDetectIndVars.RangeParams(conn.lowRange,conn.upRange)

    def rangeChecking(row: Row):Row = {

      val varType = "temperature_simul"

      val v = conn.varID
      val timestamp = conn.varTs
      val v_varVal = unpack[Double](row(v))
      val v_timestamp = unpack[String](row(timestamp))

      val e = for {
        varTimestamp <- v_timestamp
        varVal <- v_varVal

        data = if (publishAll) {
          List(varType, now.toString, conn.tearID , v,
            p.lbound + "-" + p.ubound,
            varTimestamp,
            varVal.toString)
        }
        else {
          if (!(p.lbound < varVal && varVal < p.ubound)) {
            List(varType, now.toString, conn.tearID , v,
              p.lbound + "-" + p.ubound,
              varTimestamp,
              varVal.toString)
          }
          else {
            List()
          }
        }

        /*data = if (!(p.lbound < varVal && varVal < p.ubound)) {
          List(now.toString, conn.tearID , v,
            p.lbound + "-" + p.ubound,
            varTimestamp,
            varVal.toString)
        }
        else {
          List()
        }*/
      } yield data

      e.fold(
        // print error
        err => if (conf.verbose > 0) println(err),
        { msg =>
          if (msg.nonEmpty) {
            val header = "VarType,Send_Timestamp,TearID,VarID,Range,Reading_Timestamp, Observation_value"
            val temp = (header concat "\n" concat msg.mkString(",")).getBytes()
              //msg.mkString(",").getBytes()
            conn.channel.basicPublish("", queueName, MessageProperties.TEXT_PLAIN, temp)
            if (conf.verbose > 0) {
              println(s"ALERT:\n $e")
            }
          }
        }
      )
      row
    }

    Right(f.map(rangeChecking))

  }

  /**
    * Converts one or more string columns to double
    * @param cols column names to be converted
    */
  def toDouble(cols: ColumnNames, i: Frame) : Either[ADWError, Frame] = {
    Right(i((x:String) => x.toDouble, cols.cols:_*))
  }

  def printFrame(f:Frame)= {
    f.iterable.take(10).foreach(println)
  }

  def processData(conf: ReceiveAppConfig, publishQueue: String, publishAll: Boolean) = {
    val constate = makeFrame(conf)

    constate match {
      case Left(e) => e
      case Right(cons) =>

        //val params = new DemoDetectIndVars.RangeParams(cons.conn.lowRange,cons.conn.upRange)
        val cols = cons.conn.varID
        val colNames = ColumnNames(Seq(cols))
        val newFrame = for {
          doubFrame <- toDouble(colNames,cons.frame)
          //nFrame <- taskRangeCheck(doubFrame, cons.conn, conf, params, publishQueue, publishAll)
          nFrame <- taskRangeCheck(doubFrame, cons.conn, conf, publishQueue, publishAll)
        } yield nFrame

        //note that in the task (taskRangeCheck) the flag verbose is checked already;
        //as such this is performed only due to lazy frame processing
        if (conf.numMessages <= 0) {
          newFrame.fold(e => e,  f => f.iterable.foreach( _ => ()))
        }
        else {
          newFrame.fold(e => e,  _.iterable.take(conf.numMessages).foreach( _ => ()))
        }

        cons.stop
    }
  }

  /**
    * This function is used to get all filenames passed in as argument in the command line.
    * Considering that the flag filename can be anywhere, as a result is passed the remainder
    * of the array, which can be appended afterwards
    * */
  def getFilenames(args: Array[String])= {
    val init: Seq[String] = Seq()
    val remainder: Seq[String] = Seq()
    val indFile = args.indexOf("-filename")
    if (indFile>=0) {
      //check if there is a flag after the filename flag
      val indNextFlag = args.indexWhere(_ contains "-",indFile+1)
      val ubound = if (indNextFlag > indFile) indNextFlag else args.length
      ((indFile+1 until ubound).foldLeft(init) { (acc, cur) =>
        acc :+ args(cur)
      },
        (ubound until args.length).foldLeft(remainder) {(acc,cur) =>
          acc :+ args(cur)
        }
      )
    } else (init,remainder)//(init,-1)

  }

  def main(args: Array[String]): Unit = {


    if (args.contains("-help")) {
      println(ReceiveAppConfigHelp)
      return
    }

    val (filenames,remainder) = getFilenames(args)
    if (filenames.isEmpty) {
      println("Errors found:\nNo config filepath provided.\nTerminating.")
      return
    }

    val publishAll = publishTypeAll(args)

    //Queue where alerts are to be published
    val publishQueue = if (args.indexOf("-publishQueue") >= 0) {
      args(args.indexOf("-publishQueue")+1)
    } else {
      println("Errors found:\nNo publish queue provided.\nTerminating.")
      return
    }

    val list = filenames.map{ f =>
      val default = ReceiveAppConfig(f)
      /*
      not assuming -filename is the last flag:
      one cannot assume the relative position of the flag, as such one needs to compose the array to parse in:
      1- args up to -filename flag
      2- appended of the current filename, f
      3- appended of the remainder of the args
      */
      val argsWithFN = args.take(args.indexOf("-filename")+1) :+ f
      val parseArgs = argsWithFN ++ remainder
      // Parse command line parameters
      val recAppConf = default.parse(parseArgs)
      // execute according to command line parameters
      if (recAppConf.error.nonEmpty) {
        val errStr = recAppConf.error.indices.foldLeft(""){(acc,cur) =>
          "\t" + acc.concat(recAppConf.error(cur))
        }
        Left("Errors found:\n" + errStr + "\nTerminating." + "\n Help:\n" + ReceiveAppConfigHelp)
      }
      else {
        val f: Future[Unit] = Future {
          processData(recAppConf, publishQueue, publishAll)
        }
        Right(f)
      }
    }

    val (errs, futures) = list.partition{
      case Left(_) => true
      case Right(_) => false    //if this case is not exhaustive it yields an exception in the right case
    }

    if (errs.nonEmpty) {
      errs.foreach {case  Left(e) => println(e)}
    }
    else {
      val fut: Seq[Future[Unit]] = futures.map {case Right(n) => n}
      val futs = Future.sequence(fut)

      Await.ready(futs, Duration.Inf)
    }

  }
}
