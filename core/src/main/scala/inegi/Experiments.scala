package inegi


import better.files.Dsl.cwd
import better.files.File
import breeze.plot.{Figure, GradientPaintScale, PaintScale}
import com.github.tototoshi.csv.DefaultCSVFormat
import org.hipparchus.complex.Complex
import org.hipparchus.transform.DftNormalization
import pt.inescn.dsp.{Filter, Fourier}
import pt.inescn.dsp.Fourier.plotFourier
import pt.inescn.etl.Transform
import pt.inescn.etl.stream.DataFiles
import pt.inescn.etl.stream.Load._
import pt.inescn.samplers.Function
import pt.inescn.search.stream.Pipes._
import pt.inescn.search.stream.Tasks.{Param => _}
import pt.inescn.search.stream.TestTasks._
import pt.inescn.utils.ADWError
import pt.inescn.app.Utils


/**
  * root/runMain inegi.Experiments param1 param2
  * root/runMain inegi.Experiments 1
  */
object Experiments {

  object MyFormat extends DefaultCSVFormat {
    override val delimiter = ',' // ';'
  }

/*
    // samplesTo at twice the Nyquist frequency and samplesTo for 2.2 seconds
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1)
    val fs = 50 * samplingFreq  // we increase the
    val duration = 3
    val (_, xx) = Fourier.samplingSpecification(fs, duration)

    val sig1 = Distribution.Normal(1, 1, new JDKRandomGenerator(98765))

    val (len,xa,fftSig) = Fourier.FFT(fs, xx.length, sig1, pad = false)
    val ya = Fourier.abs(fftSig)
    val yb = Fourier.phase(fftSig)

    val (sya, _) = Fourier.topAbsoluteComponents(40, ya, yb)
    //def frequencyAt(fs: Double, len: Int)(i : Int) = i * ((fs*1.0)/len)
    val threshold = a1 * (len/2) * 0.6  // 60% of the smallest amplitude
    val signifcantAmp = sya.filter( p => p._1 > threshold)

 */

  def maxAbsStftsAbs(stft: Array[Array[Double]]): Array[Double] = {
    val maxs = stft.map{ abs =>
      val tmp = abs.sortBy( -_ )
      tmp(0)
    }
    maxs.sortBy( -_ )
  }


  def maxAbsStfts(stft: Array[Array[Complex]]): Array[(Double, Int)] = {
    val maxs = stft.flatMap{ fft =>
      val tmp = Fourier.topAbsoluteComponents(1, fft)._1
      //println(fft.mkString(","))
      //println(tmp.mkString(","))
      tmp
    }
    maxs.sortBy( p => -p._1)
  }


  def showNormalize(title:String, norm: Array[Double] => Array[Double], stfts:Array[Array[Complex]]): Figure = {
    val stfts1 = stfts.map( e => norm( Fourier.abs(e)) )
    val maxAbssGood1: Array[Double] = maxAbsStftsAbs(stfts1)
    val maxAbsGood1 = maxAbssGood1(0)
    val colors1 = GradientPaintScale(lower=0, upper=maxAbsGood1, gradient = PaintScale.BlueToRed)
    Fourier.plotSTFTAbs(stfts1, colors1, title)
  }

  // Wavelet transform
  // Multi-resolution analysis

  /**
    * Notes: Signal F0
    *
    * 1. Resolution is limited so a frequency component may be "spread" over
    * one or more components of the FFT. As an example, the 10Hz sine wave
    * should only have one component the 10Hz. But at a sampling rate of
    * 400Hz and an FFT size of 64 components, we have a resolution of
    * 6.35Hz. This means that the maximum absolute values will be found in
    * the 1st (6.35Hz) and second component (12.9Hz). This is visible
    * in the STFT.
    *
    * 2. When the STFT windows do not match up exactly with the the signal
    * length, then the last window must be extended (padded). In this case,
    * the FFT components are not correct. We should not use this window
    * to characterize the signal.
    *
    * 3. When using the STFT we introduce discontinuities in the signal that
    * introduce spurious frequency components (unless we split at exactly at
    * the start and finish of a period, which for multiple waves at different
    * non-multiple frequencies is not possible). These frequency components
    * are NOT the same among the various STFT windows. To see this in action
    * remove the Hann filter fro the STFT.
    *
    */
  def exp0(): Unit ={
    val f1 = 10 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(0)
    val f2 = 20 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val samplingFreq = 10 * Fourier.nyquistNamplingFrequency(f1, f2)
    val duration = 2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,duration)

    val good = Function.Sin(f1, a1, phase1, dd) // good
    val bad = Function.Sin(f2, a2, phase2, dd)  // bad

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, good(xx.length))
    plt += plot(xx, bad(xx.length))
    plt.title = s"DSL Ops @ $samplingFreq"
    plt.refresh*/

    val (lenGood,xaGood,fftSigGood) = Fourier.FFT(samplingFreq, xx.length, good, pad = false, DftNormalization.STANDARD)
    plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaGood, fftSigGood, title="Pure Single Frequency")
    println(s"xaGood length = $lenGood == ${xaGood.length}")

    // Add DC component
    //def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) =
    // Fourier.FFT(fs, signal_length, sig, pad=pad, DftNormalization.STANDARD)
    //Fourier.plotFourierTransform(fft, samplingFreq, duration, good(xx.length).toArray.map(_+0.5), "Example Y")

    val (_,xaBad,fftSigBad) = Fourier.FFT(samplingFreq, xx.length, bad, pad = false, DftNormalization.STANDARD)
    //plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaBad, fftSigBad, title="Bad")

    // Signal at 20 Hz means that we have to samplesTo at least 1/20 seconds to get a full period (0.05 seconds)
    // So how many samples do we need to get using the selected sampling rate?
    val f1Period = 1.0 / f1
    val (_,xx1) = Fourier.samplingSpecification(samplingFreq,f1Period)

    println(s"samplingFreq ($duration s) = $samplingFreq Hz")
    println(s"Original signal ($duration s) = ${xx.length} samples")
    println(s"Good signal (1T = $f1Period) = ${xx1.length} samples")

    //stft(good, samplingFreq, windowSize, overlap, extendBoundary, pad = true)
    val winSize = 3 * xx1.length
    val windowCount = xx.length.toDouble / winSize
    println(s"Good winSize = $winSize samples")
    println(s"Good windowCount = $windowCount samples")
    // overlap=5,
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length).map(_+0.5), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    val (stfts, xa, cola) = Fourier.stft(good(xx.length), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length), samplingFreq, winSize, pad=false)
    val FFTwindowCount = xx.length.toDouble / stfts(0).length
    println(s"xa.length = ${xa.length}")
    println(s"stfts(0).length = ${stfts(0).length}")
    println(s"Good FFTwindowSize = ${stfts(0).length} samples")
    println(s"Good FFTwindowCount = $FFTwindowCount samples")

    val maxAbssGood: Array[(Double, Int)] = maxAbsStfts(stfts)
    val maxAbsGood = maxAbssGood(0) // sortedAbssGood(0)
    println(s"maxAbssGood = ${maxAbssGood.mkString(",")}")
    println(s"maxAbsGood = $maxAbsGood")

    val (_, freqResolution) = Fourier.deltaFreq(samplingFreq, stfts(0).length)
    val topComponent = Fourier.frequencyOfIndex(samplingFreq, stfts(0).length, maxAbsGood._2)
    val expectedTopComponent = Fourier.indexOfFrequency(samplingFreq, stfts(0).length, f1)
    println(s"topComponent = $topComponent Hz")
    println(s"expectedTopComponent = $expectedTopComponent")
    println(s"freqResolution = $freqResolution Hz")

    // JPlot the STFT (indirectly)
    import breeze.plot._

    val colors = GradientPaintScale(lower=0, upper=maxAbsGood._1, gradient = PaintScale.BlueToRed)
    Fourier.plotSTFT(stfts, colors, title = "Pure Single Frequency")
  }

  /**
    * Notes: Signal with F0 + constant offset
    *
    * 1. When the signal is not detrended (in this case we add a constant
    * component), we see that the STFT windows do not show the same absolute
    * values. We need to detrend by removing the DC component. Note that we
    * still need a filter due to the window splitting - filtering won't
    * solve both issues.
    *
    */
  def exp1(): Unit ={
    val f1 = 10 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(0)
    val f2 = 20 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val samplingFreq = 10 * Fourier.nyquistNamplingFrequency(f1, f2)
    val duration = 2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,duration)

    val offset = Function.Const(0.2,dd)
    val good = Function.Sin(f1, a1, phase1, dd) + offset // good
    val bad = Function.Sin(f2, a2, phase2, dd)  + offset // bad

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, good(xx.length))
    plt += plot(xx, bad(xx.length))
    plt.title = s"DSL Ops @ $samplingFreq"
    plt.refresh*/

    val (lenGood,xaGood,fftSigGood) = Fourier.FFT(samplingFreq, xx.length, good, pad = false, DftNormalization.STANDARD)
    plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaGood, fftSigGood, title="Pure Single Frequency + offset")
    println(s"xaGood length = $lenGood == ${xaGood.length}")

    // Add DC component
    //def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) =
    // Fourier.FFT(fs, signal_length, sig, pad=pad, DftNormalization.STANDARD)
    //Fourier.plotFourierTransform(fft, samplingFreq, duration, good(xx.length).toArray.map(_+0.5), "Example Y")

    val (_,xaBad,fftSigBad) = Fourier.FFT(samplingFreq, xx.length, bad, pad = false, DftNormalization.STANDARD)
    //plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaBad, fftSigBad, title="Bad")

    // Signal at 20 Hz means that we have to samplesTo at least 1/20 seconds to get a full period (0.05 seconds)
    // So how many samples do we need to get using the selected sampling rate?
    val f1Period = 1.0 / f1
    val (_,xx1) = Fourier.samplingSpecification(samplingFreq,f1Period)

    println(s"samplingFreq ($duration s) = $samplingFreq Hz")
    println(s"Original signal ($duration s) = ${xx.length} samples")
    println(s"Good signal (1T = $f1Period) = ${xx1.length} samples")

    //stft(good, samplingFreq, windowSize, overlap, extendBoundary, pad = true)
    val winSize = 3 * xx1.length
    val windowCount = xx.length.toDouble / winSize
    println(s"Good winSize = $winSize samples")
    println(s"Good windowCount = $windowCount samples")
    // overlap=5,
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length).map(_+0.5), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    val (stftsNoDetrend, xaNoDetrend, colaNoDetrend) = Fourier.stft(good(xx.length), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    val detrended_xx = Filter.removeDC(good(xx.length).toArray)
    val (stfts, xa, cola) = Fourier.stft(detrended_xx, samplingFreq, winSize, pad=false, filter = Filter.initHann)
    val FFTwindowCount = xx.length.toDouble / stfts(0).length
    println(s"xa.length = ${xa.length}")
    println(s"stfts(0).length = ${stfts(0).length}")
    println(s"Good FFTwindowSize = ${stfts(0).length} samples")
    println(s"Good FFTwindowCount = $FFTwindowCount samples")

    val maxAbssGood: Array[(Double, Int)] = maxAbsStfts(stfts)
    val maxAbsGood = maxAbssGood(0) // sortedAbssGood(0)
    println(s"maxAbssGood = ${maxAbssGood.mkString(",")}")
    println(s"maxAbsGood = $maxAbsGood")

    val (_, freqResolution) = Fourier.deltaFreq(samplingFreq, stfts(0).length)
    val topComponent = Fourier.frequencyOfIndex(samplingFreq, stfts(0).length, maxAbsGood._2)
    val expectedTopComponent = Fourier.indexOfFrequency(samplingFreq, stfts(0).length, f1)
    println(s"topComponent = $topComponent Hz")
    println(s"expectedTopComponent = $expectedTopComponent")
    println(s"freqResolution = $freqResolution Hz")

    // JPlot the STFT (indirectly)
    import breeze.plot._

    val colors = GradientPaintScale(lower=0, upper=maxAbsGood._1, gradient = PaintScale.BlueToRed)
    Fourier.plotSTFT(stfts, colors, title = "Pure Single Frequency + offset + Detrend")
    Fourier.plotSTFT(stftsNoDetrend, colors, title = "Pure Single Frequency + offset + No detrend")

    //val half = stfts(0).length/2
    //println(half)
    //println(Fourier.abs(stfts(0).take(half)).mkString(",\n"))
    //println(Fourier.abs(stfts(1)).mkString(",\n"))
    //println("--")
  }

  /**
    * Notes: Signal F0 + constant offset + UNIT FFT scaling
    *
    * 1. Use if the FFT scaling does not help. It just changes the absolute
    * scales of the FFT. We still need to use detrending of the offset.
    *
    * 2. Applying normalization/scaling on each window does not help. Open
    * issue is if we should do this normalization/scale at the signal and
    * not the window level
    */
  def exp2(): Unit ={
    val f1 = 10 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(0)
    val f2 = 20 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val samplingFreq = 10 * Fourier.nyquistNamplingFrequency(f1, f2)
    val duration = 2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,duration)

    val offset = Function.Const(0.2,dd)
    val good = Function.Sin(f1, a1, phase1, dd) + offset // good
    val bad = Function.Sin(f2, a2, phase2, dd)  + offset // bad

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, good(xx.length))
    plt += plot(xx, bad(xx.length))
    plt.title = s"DSL Ops @ $samplingFreq"
    plt.refresh*/

    val (lenGood,xaGood,fftSigGood) = Fourier.FFT(samplingFreq, xx.length, good, pad = false, DftNormalization.UNITARY)
    plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaGood, fftSigGood, title="Unitary FFT scaling for F0 + Offset")
    println(s"xaGood length = $lenGood == ${xaGood.length}")

    // Add DC component
    //def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) =
    // Fourier.FFT(fs, signal_length, sig, pad=pad, DftNormalization.STANDARD)
    //Fourier.plotFourierTransform(fft, samplingFreq, duration, good(xx.length).toArray.map(_+0.5), "Example Y")

    val (_,xaBad,fftSigBad) = Fourier.FFT(samplingFreq, xx.length, bad, pad = false, DftNormalization.UNITARY)
    //plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaBad, fftSigBad, title="Bad")

    // Signal at 20 Hz means that we have to samplesTo at least 1/20 seconds to get a full period (0.05 seconds)
    // So how many samples do we need to get using the selected sampling rate?
    val f1Period = 1.0 / f1
    val (_,xx1) = Fourier.samplingSpecification(samplingFreq,f1Period)

    println(s"samplingFreq ($duration s) = $samplingFreq Hz")
    println(s"Original signal ($duration s) = ${xx.length} samples")
    println(s"Good signal (1T = $f1Period) = ${xx1.length} samples")

    //stft(good, samplingFreq, windowSize, overlap, extendBoundary, pad = true)
    val winSize = 3 * xx1.length
    val windowCount = xx.length.toDouble / winSize
    println(s"Good winSize = $winSize samples")
    println(s"Good windowCount = $windowCount samples")
    // overlap=5,
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length).map(_+0.5), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    //val (stfts, xa, cola) = Fourier.stft(good(xx.length), samplingFreq, winSize, pad=false, filter = Filter.initHann)
    //val detrended_xx = Filter.removeDC(good(xx.length).toArray)
    /*val (stfts, xa, cola) = Fourier.stft(detrended_xx, samplingFreq, winSize,
      pad=false, filter=Filter.initHann, fftNormalization=DftNormalization.STANDARD)*/
    val (stfts, xa, cola) = Fourier.stft(good(xx.length), samplingFreq, winSize,
      pad=false, filter=Filter.initHann, fftNormalization=DftNormalization.UNITARY)
    val FFTwindowCount = xx.length.toDouble / stfts(0).length
    println(s"xa.length = ${xa.length}")
    println(s"stfts(0).length = ${stfts(0).length}")
    println(s"Good FFTwindowSize = ${stfts(0).length} samples")
    println(s"Good FFTwindowCount = $FFTwindowCount samples")

    val maxAbssGood: Array[(Double, Int)] = maxAbsStfts(stfts)
    val maxAbsGood = maxAbssGood(0) // sortedAbssGood(0)
    println(s"maxAbssGood = ${maxAbssGood.mkString(",")}")
    println(s"maxAbsGood = $maxAbsGood")

    val (_, freqResolution) = Fourier.deltaFreq(samplingFreq, stfts(0).length)
    val topComponent = Fourier.frequencyOfIndex(samplingFreq, stfts(0).length, maxAbsGood._2)
    val expectedTopComponent = Fourier.indexOfFrequency(samplingFreq, stfts(0).length, f1)
    println(s"topComponent = $topComponent Hz")
    println(s"expectedTopComponent = $expectedTopComponent")
    println(s"freqResolution = $freqResolution Hz")

    // JPlot the STFT (indirectly)
    import breeze.plot._

    val colors = GradientPaintScale(lower=0, upper=maxAbsGood._1, gradient = PaintScale.BlueToRed)
    Fourier.plotSTFT(stfts, colors, title = "Unitary FFT scaling for F0 + Offset + No Detrend")

    /*
    val half = stfts(0).length/2
    println(half)
    //println(Fourier.abs(stfts(0).take(half)).mkString(",\n"))
    println(Fourier.abs(stfts(1)).mkString(",\n"))
    println("--")
    */

    // Apply normalization to the values to see if it improves frequency resolution

    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + Normalize", Transform.normalize, stfts)
    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + Scale(-1,1)", v => Transform.scale(-1.0,1,v), stfts)
    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + softmax", v => Transform.softmax(v), stfts)
    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + softmax(2.1)", v => Transform.softLambdaMax(v, lambda = 2.1), stfts)
    showNormalize("Unitary FFT scaling for F0 + Offset + No Detrend + logit(1,1,0)", v => Transform.logit(v), stfts)
  }




  // https://stackoverflow.com/questions/15349439/how-to-append-or-prepend-an-element-to-a-tuple-in-scala/15349965#15349965
  def exp3(): Unit = {

    val dir = cwd / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"
    //val matchNames = "rol1_rpm3000_hp0_norm_mm0_exp3.csv"
    val files = DataFiles.parseFileNames(dir, matchNames, Utils.parseFileName).toList
    //println(files.mkString(";\n"))
    //println(files.size)
    assert( files.size == 90 )
    val parserOk = files.filter( _.isRight ).map(_.right.get)
    //println(parserOk.mkString(";\n"))
    //println(parserOk.size)
    assert( parserOk.size == 90 )
    val (filesp,features,units) = parserOk.unzip3
    //println(filesp.mkString(";\n"))
    //println(features.mkString(";\n"))
    //println(units.mkString(";\n"))
    //println(filesp.size)

    /*
    val sorted = filesp.sortBy(!_.toString.contains("_norm_"))
    println(sorted.mkString(";\n"))
    val lengths = sorted.map(f => (f,f.lineIterator.size - 1))
    println(lengths.mkString(";\n"))
    */

    // split files into good/bad
    // create cv split 80/20 for each good/bad (randomize)
    // Problem with time-stamp (how to generate them in tis case?)
    // load the data
    // repeat the experiments?

    def sample_len(cv_split:Double, len:Int): (Int, Int) = {
      val train_len = Math.round(len*cv_split).toInt
      val test_len = len - train_len
      (train_len, test_len)
    }

    def groupByExperiment(samples:Seq[File]): Map[String, Seq[File]] = {
      samples.groupBy{ f =>
        val name = f.name
        val exp_pos = name.indexOf("_exp")
        name.substring(0,exp_pos)
      }
    }

    def splitLearnTest(cv_split:Double, samples: Map[String, Seq[File]]) = {
      samples.map{ case (k,l) =>
        val (learn_len, _) = sample_len(cv_split, l.size)
        val (learn, test) = l.splitAt(learn_len)
        (k,learn,test)
      }
    }

    // https://en.wikipedia.org/wiki/Reservoir_sampling
    // https://en.wikipedia.org/wiki/Rejection_sampling
    //  see: https://stackoverflow.com/questions/2218005/how-to-get-a-random-line-of-a-text-file-in-java
    //       How to avoid reading the whole file

    // Prepare learn and test data
    // Split into good/bad classes, anomaly detection may be based on positive only
    val (goodt, badt) = filesp.partition(_.toString.contains("_norm_"))
    // Group by experiments
    val goodg = groupByExperiment(goodt)
    val badg = groupByExperiment(badt)
    // Use some experiments to learn and others to test
    val cv_split = 0.8
    val good_splits = splitLearnTest(cv_split, goodg)
    val bad_splits = splitLearnTest(cv_split, badg)
    val (_,good_learnt,good_testt) = good_splits.unzip3
    val (_,bad_learnt,bad_testt) = bad_splits.unzip3
    // Generate the learn and test sets
    val learnt = good_learnt ++ bad_learnt
    //val testt = good_testt ++ bad_testt
    // Make sure the experiments are randomized
    val learn = scala.util.Random.shuffle(learnt.flatten)
    //val test = scala.util.Random.shuffle(testt.flatten)
    //println(learn.mkString(",\n"))

    // Each file has has a delta-time that indicates the offset in nanoseconds
    // to the next sample. For a given order of the files we want to generate
    // a time-stamp for each sample of each file so that they are unique.They
    // can then be used to index the data and help debug detection errors


    // We will use the offset to be the same as the sampling
    // rate found in the files
    val offset = 0.000195
    val tmp3 = DataFiles.generateTimeStampOffsets(offset, learn)
    //val tmp4 = generateTimeStampOffsets(offset, test)

    // User specified task

    // Assume this is the sampling date and time
    //val stamp = LocalDateTime.parse( "2018-05-14 10:49:58.701", format)

    // sliding window parameters
    //val winSize = 256
    //val stride = 128

    // FFT parameters
    // time between samples
    //val rate = offset
    // sampling frequency
    //val fs = 1 / rate
    // pad window to be power of 2
    //val pad = true

    // Filter bank parameters
    //val numFilters = 13
    //val minFreq = 0.0
    //val maxFreq = 20000.0
    //val filterParam = filterP("Hann", winSize, rate, numFilters, minFreq, maxFreq)

    /*
       NOTE: the current version of the Frame applies all column conversions
       in order. These conversions are performed any iteration or mapping is
       done. So set these up first and then apply the mapping functions.
    */

    // Load the file
    val s3 = loadExtraINEGICSVs(MyFormat, learn.take(2).map(_.toString).toList, tmp3.toList) /* TODO ->:
    //val s3 = loadExtraINEGICSVsf(learn.map(_.toString).toList, tmp3.toList) ->:
      // column conversion functions. These will be execute first
      // irrespective of whether we apply mapping function or not
      renameColumnsf("Time(s)" -> "t") ->:
      toDoublef("t", "x", "y", "z", "delta") ->:
      xDoublef(1e9,"t", "delta") ->:
      doubleToLongf("t", "delta") ->:
      longToDurationf("t", "delta") ->:
      addDurationf("t","delta", "t_delta") ->:
      addDurationf("t","delta", "t_dt") ->:
      addDateTimeToDurationf(stamp, "t_dt") ->:
      convertLabelValuef("damage") ->:
      addCounterColumnf("counter") ->:
      renameColumnsf("damage" -> "label") ->:
      // Mapping function, make sure we have
      // applied all columns conversions first
      // sliding window
      windowf(winSize, stride) ->:
      // FFT of the columns, add results as new columns using FFT suffix
      fftStepf("FFT", fs, pad, "x", "y", "z") ->:
      // Apply filter bank, replace the FFT columns
      filterBankStepf(filterParam, "xFFT", "yFFT", "zFFT")
      // TODO: calculate energy on each applied bank
      // TODO: perform DCT on energy vector
      */
    //println(s3)

    // compile new pipes
    val c3 = compile(s3)
    assert(c3.iterator.hasNext)
    //println(c3.right.get.tracks)

    // execute the pipes
    val r3: Iterable[(Either[ADWError, Frame], Executing)] = execDebug(c3)(Frame(("",List())))
    //println(r3)

    //val read3 = r3.drop(20478).take(5).toIndexedSeq
    val read3 = r3.take(1).toIndexedSeq
    //val samples_per_file = 20480
    //val read3 = r3.take(samples_per_file + 5).toIndexedSeq
    println(read3.mkString(";\n"))
    //println(learn.take(2))

  }

  val experiments: Map[String, () => Unit] = Map(
    "0" -> exp0 _,
    "1" -> exp1 _,
    "2" -> exp2 _,
    "3" -> exp3 _
    //"4" -> exp4 _
  )

  def main(args: Array[String]): Unit ={
    for (id <- args){
      if (experiments.contains(id)) {
        println(s"Executing experiment $id.")
        experiments(id)()
      }
      else
        println(s"Experiment arg $id not found.")
    }
  }
}
