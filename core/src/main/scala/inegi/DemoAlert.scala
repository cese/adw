package inegi

import java.text.SimpleDateFormat
import java.time.{Duration, LocalDateTime, ZonedDateTime}
import java.util
import java.util.Calendar

import scala.concurrent.ExecutionContext.Implicits.global
import javafx.application.{Application, Platform}
import javafx.scene.Scene
import javafx.scene.layout.{ColumnConstraints, GridPane, Priority, RowConstraints, VBox}
import javafx.stage.Stage
import javafx.scene.control.{Label, TextArea}
import pt.inescn.app.RabbitConfigs._
import pt.inescn.etl.stream.Load._
import pt.inescn.etl.stream.RabbitMQ._
import pt.inescn.app.Utils
import pt.inescn.etl.stream.Load
import pt.inescn.plot.JPlotAlarmInterface

import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.util.{Failure, Success}


/**
  * This application consumes data from a message broker and processes it.
  * By default all of the data that is available is consumed. However during
  * system's testing and debugging we may be interested in receiving a limited
  * number of messages. We can receive a fixed number of messages using
  * the `-numMessages` flag.
  *
  * The data is sent in JSON format (see [[SendApp]] for more information).
  * Each of these JSON messages may contain several records. Each record is
  * converted into a [[Row]]. This client accesses the sequence of Row via an
  * iterator.
  *
  * The messages are received from a specific queue of a message broker. The
  * connection configuration information is stored in a text file:
  *   `adw/core/src/main/scala/inegi/ReceiveConfig`
  * It contains information such as the host name or IP, the message
  * broker's queue name, and access credentials. These configurations
  * may be set or overridden in the command line. These include the
  * flags: `-host`, `-password`, `-port` `-userName` and `-queueName`.
  *
  * In addition to the flags above we also have: `-verbose` (print additional
  * information to the console indicating applications actions') and
  * `-waitMultiplier` (time in seconds to multiply to exponential back-off
  * wait time).
  *
  * Data reception is handled synchronously however this can be done either
  * using a *Pull* API or a *Callback* API. The `-iter` commandline parameter
  * may be use to set the data consumer to use the *pull* API (`poll`) or the
  * *callback* API (`CallBack`). Note that when polling we can set the maximum
  * time to wait for the next message when polling (`-waitMultiplier`). Note
  * that this parameter is only necessary when using the polling API.
  *
  * For a complete list of command line arguments use the -help argument.
  *
  * If the parsing of the commandline fails, a list of errors are accumulated
  * abd shown. The application then terminates.
  *
  * Here are two examples of command lines that load a single file:
  * root/runMain inegi.DemoAlert -verbose 1 -iter CallBack
  * root/runMain inegi.DemoAlert -verbose 1 -iter poll -waitMultiplier 2 -numMessages 1
  * root/runMain inegi.DemoAlert -verbose 1 -host 111.222.333.444 -queueName adira_alerts -userName inesc -password adira_1n35c -iter CallBack -waitMultiplier 2
  *
  * @see [[pt.inescn.etl.stream.RabbitMQ]] for information on the use of the iterators and
  *      the functions used to convert the JSON messages to [[Row]] iterators
  */
object DemoAlert {

  var recAppConf: ReceiveAppConfig = _
  val QUEUE_ADIRA_ACCELEROMETER = "adira_accelerometer"

  def printMsg(ta: TextArea)(s: String): Unit = {
    javafx.application.Platform.runLater(() => {
      ta.appendText(s + "\n")
    })
  }


  /**
    * Test method that reads a message broker's queue. We assume
    * the data is a string. This data is converted via `iter`
    * function. The converted data is then written to a GUI window
    * and the console.
    *
    * @param conf configuration to connect to the message broker's queue
    * @param log function to log data in GUI
    * @param toIter function to convert string data to an `Iterator[Row]`
    */
  def processData(conf: ReceiveAppConfig, log: Row => Unit, toIter: String => Iterator[Row]): Unit = {
    ReceiveApp.connectAndMakeFrame(conf, toIter).map{ case con => //(f,con) =>
      if (conf.numMessages <= 0) {
        con.frame.iterable.foreach{ m =>
        //f.iterable.foreach{ m =>
          log(m)
          println(m)
        }
        //f.iterable.foreach(println)
        //f.foreach(println)
        //f.iterable.toIterator.foreach(println)
      }
      else {
        con.frame.iterable.take(conf.numMessages).foreach{ m =>
        //f.iterable.take(conf.numMessages).foreach{ m =>
          log(m)
          println(m)
        }
        //f.iterable.take(conf.numMessages).foreach(println)
        //f.iterable.foreach(println)
        //f.iterable.toIterator.take(conf.numMessages).foreach(println)
        // Close the connection so the application can terminate
        //stop(conf, con)
        con.stop
      }
    }
  }

  case class AlertData(start: ZonedDateTime, end: ZonedDateTime, machineId: String, sensorId: String, det: Double)

  def alertData(row: Row): Either[String, AlertData] = {
    val v_start = Load.unpack[String](row("0"))
    val v_end = Load.unpack[String](row("1"))
    val v_machineID = Load.unpack[String](row("2"))
    val v_sensorID = Load.unpack[String](row("3"))
    val v_det = Load.unpack[String](row("4"))
    //val v_now       = Load.unpack[String](row("5"))

    for {
      sstart <- v_start
      send <- v_end
      machineID <- v_machineID
      sensorID <- v_sensorID
      sdet <- v_det
      start = ZonedDateTime.parse(sstart)
      end = ZonedDateTime.parse(send)
      det = sdet.toDouble
    } yield AlertData(start, end, machineID, sensorID, det)
  }

  case class SensorRowData(timestamp: ZonedDateTime, machineId: String, sensorId: String, X: Double, Y: Double, Z: Double)

  object SensorRowData {
    def apply(timestamp: ZonedDateTime): SensorRowData = {
      new SensorRowData(timestamp, "", "", 0.0, 0.0, 0.0)
    }
  }

  def sensorData(row: Row): Either[String, SensorRowData] = {
    val v_timestamp = Load.unpack[String](row("0"))
    val v_machineID = Load.unpack[String](row("1"))
    val v_sensorID = Load.unpack[String](row("2"))
    val v_x = Load.unpack[String](row("3"))
    val v_y = Load.unpack[String](row("4"))
    val v_z = Load.unpack[String](row("5"))
    //val v_now       = Load.unpack[String](row("6"))

    for {
      timestamp <- v_timestamp
      machineID <- v_machineID
      sensorID <- v_sensorID
      sx <- v_x
      sy <- v_y
      sz <- v_z
      stamp = ZonedDateTime.parse(timestamp)
      x = sx.toDouble
      y = sy.toDouble
      z = sz.toDouble
    } yield SensorRowData(stamp, machineID, sensorID, x, y, z)
  }


  /**
    * Checks if `sd` is withing the time window [`start` .. `end`].
    * The check is inclusive on the `start` and exclusive on the
    * `end`.
    *
    * @param sd    - signal timestamp
    * @param start - start of time window
    * @param end   - end of time window
    * @return true if the signal timestamp `sd` is within the time window
    */
  def within(sd: ZonedDateTime, start: ZonedDateTime, end: ZonedDateTime): Boolean = {
    (sd.isEqual(start) || sd.isAfter(start)) && (sd.isEqual(end) || sd.isBefore(end))
  }

  /**
    * In order to tag a signal with a fail/no-fail flag alert, these alerts
    * must be available before we process the signal. We therefore assume we
    * have waited for and read the last (least recent) alert data before we
    * process the signal. The alert data contains the time window within
    * which a flag value has been assigned (failure and non-failure). We
    * then check if the signal data exists (no blocking is used). If it
    * does we check if the signal's timestamp lies between the alert's
    * window (includes window limits). In this case we can process the signal
    * and the corresponding failure/no-failure prediction. If the signal's
    * timestamp occurs before the alert's window, we ignore it. If the
    * signal's timestamp occurs after the alert's window then we drop this
    * signal (we could cache but this would introduce delays) and terminate
    * processing. At this point the next alert data should be read (with
    * locking) and this function should be called again with this new alert.
    *
    * @see [[DemoDetect]]
    * @param log   log data to a GUI window (experimental)
    * @param data  not blocking data stream
    * @param alert last (least recent) alert read (with blocking)
    */
  def testDataWithAlert(log: AlertData => Unit, data: Iterator[Row])(alert: AlertData): Unit = {
    //log(alert)
    //println(s"Alert check : $alert --------------------------------------- ")
    var haveAlert = true
    var lastNext: LocalDateTime = LocalDateTime.now()
    while (data.hasNext && haveAlert) {
      // Get the next data record
      val next = LocalDateTime.now()
      //println(s"Read sensor data $next")
      val nextData = data.next()
      val parsed = LocalDateTime.now()
      //println(s"Parse sensor data $parsed")
      val sensData = sensorData(nextData)
      //println(s"Has next data: $nextData")
      sensData.fold(e =>
        // Error
        println(s"Sensor data error: $e"),
        { sd =>
          if (sd.timestamp.isBefore(alert.start)) {
            // Current alert is for later data, ignore
            //println(s"Ignoring : $sd isBefore $alert")
            val ignored = LocalDateTime.now()
            //println(s"Ignore data $ignored")
            val diff = Duration.between(next, ignored)
            println(s"Ignoring time = $diff")
          } else if (within(sd.timestamp, alert.start, alert.end)) {
            // Data within alert window, use it
            //println(s"Process : $sd within $alert")
            val process = LocalDateTime.now()
            //println(s"Process data $process")

            //Update interface
            println(sd)
            AlertInter.plotAlarmInterfaceStep(1000, sd, alert.det)

            //println("Update:" + alert.det.toString )
            val plotted = LocalDateTime.now()
            //println(s"Updated data $plotted")
            val diff = Duration.between(next, plotted)
            //println(s"Processing time = $diff")
          } else {
            // Data after alert, we loose this record's alert
            //println(s"No alert available yet for $sd within $alert")
            // So stop and get the next alert
            haveAlert = false
          }
        })
      val diff = Duration.between(lastNext, next)
      println(s"Total time = $diff")
      lastNext = next
    }
  }


  class AlarmInterface {
    // Create some sample data
    var ay1: Array[Double] = new Array[Double](1)
    var ay2: Array[Double] = new Array[Double](1)
    var ay3: Array[Double] = new Array[Double](1)
    val y1: util.List[Double] = new util.LinkedList[Double]
    val y2: util.List[Double] = new util.LinkedList[Double]
    val y3: util.List[Double] = new util.LinkedList[Double]

    val bufferSz: Int = 10000

    val x: Array[Double] = new Array[Double](bufferSz)
    val yybuffer: Array[ListBuffer[Double]] = new Array[ListBuffer[Double]](4)
    val yyarray: Array[Array[Double]] = new Array[Array[Double]](4)
    val xbuffer: ListBuffer[Double] = ListBuffer()
    val figw: JPlotAlarmInterface = new JPlotAlarmInterface("")
    val yy: Array[Array[Double]] = new Array[Array[Double]](3)
    figw.ylim(-10, 10);
    figw.holdon()


    var i = 0
    while (i < yybuffer.length) {
      yybuffer(i) = ListBuffer[Double]()
      i += 1
    }

    var a = 10
    var av = 0.0
    var pt = -10.0
    var blink = 0
    var counter = 0
    var counter1 = 0


    def plotAlarmInterfaceStep(bufferSz: Int, Signal: SensorRowData, AlarmVal: Double): Unit = {

      //==================
      if (counter < bufferSz) {
        xbuffer += counter
        yybuffer(0) = yybuffer(0) :+ Signal.X
        yybuffer(1) = yybuffer(1) :+ Signal.Y
        yybuffer(2) = yybuffer(2) :+ Signal.Z
      }
      else {
        yybuffer(0) = yybuffer(0) :+ Signal.X
        yybuffer(0).remove(0)
        yybuffer(1) = yybuffer(1) :+ Signal.Y
        yybuffer(1).remove(0)
        yybuffer(2) = yybuffer(2) :+ Signal.Z
        yybuffer(2).remove(0)
      }

      av = AlarmVal
      counter += 1

      counter1 += 1
      if (counter1 == 100) {

        //print(yyarray.length)

        var j = 0
        while (j < yyarray.length - 1) {
          yy(j) = yybuffer(j).toArray
          j += 1
        }

        /*if (av == 1) {
          if (pt == -10) pt = bufferSz
          else pt -= 100

          if (pt == 0) {
            pt = -10
            av = 0
          }
        }*/

        val xp = Array(-10.0, -10.0)
        figw.wiggle(xbuffer.toArray, yy, xp, bufferSz)

        // Alarm lamp
        if (av == 0)
          figw.setState(1)
        else if (blink == 0) {
          figw.setState(3)
          blink = 1
        }
        else {
          figw.setState(0)
          blink = 0
        }

        val CF = figw.meanCrestFactor(yy)
        val ClF = figw.meanClearanceFactor(yy)
        val IF = figw.meanImpulseFactor(yy)
        val SF = figw.meanShapeFactor(yy)

        figw.updateGaugesValue(CF, ClF, IF, SF)
        figw.updateDisplayValues(CF, ClF, IF, SF)

        //Thread.sleep(10)
        counter1 = 0
      }
    }
  }

  val AlertInter = new AlarmInterface();


    /**
      * This is the main routine that processes the alerts. We can only show
      * a signal's state (failure or no-failure) after the alert has been
      * generated. When the data is processed by the analytics algorithm it
      * first sends the original signal data to a specific signal storage queue.
      * Some time after that it sends the alerts to a specific alerts storage
      * queue. Note that the signal will usually be queue before the alerts.
      * So we assume that these will exists first. As such the alerts are
      * extracted from the queue first (blocking if one is not available yet).
      *
      * An alert assigns a fail/no-fail flag to a time window. For each alert
      * we extract the signal if it exists (using a non-blocking check). If
      * the signal's timestamp occurs before the alert's window we drop this
      * signal. It if occurs within the alert's window, we assign the alerts
      * flag flag to that signal. If the signal occurs after the alert, we
      * also drop thr signal (loose it) and look for the next alert, blocking
      * if necessary. Note that for visualization purposes the loss of a single
      * sample at high sampling rates is not a problem. For very low sampling
      * rates the untagged signal sample should be cached.
      *
      * @param conf   main configuration file (application flags and RabbitMQ
      *               parameters)
      * @param log    log data to a GUI window (experimental)
      * @param toIter transforms queue input into a stream of [[Row]]
      * @see [[testDataWithAlert]]
      */
    def processDataAlerts(conf: ReceiveAppConfig, log: AlertData => Unit, toIter: String => Iterator[Row]): Unit = {
      // Get the sensor data from a data logger
      val rabbitMQConf = conf.connect.setQueueName(QUEUE_ADIRA_ACCELEROMETER)
      val dataConf = conf.copy(connect = rabbitMQConf)
      // Make data access non-locking (need to call hasNext)
      val manualConf = dataConf.copy(iter = conf.MANUAL)

      // Create the sensor data reader
      //val data =  ReceiveApp.connectAndMakeFrame(dataConf, toIter)
      val data = ReceiveApp.connectAndMakeFrame(manualConf, toIter)
      val dataIter = if (data.isRight) data.right.get else throw new RuntimeException("processDataAlerts: No data frame available.")
      // Alerts data (uses locking), create the reader
      val alerts = ReceiveApp.connectAndMakeFrame(conf, toIter)
      //val alerts = ReceiveApp.connectAndMakeFrame(manualConf, toIter)


      //val AlertInter = new AlarmInterface()


      // This function will check which data have an alert or not
      def process: AlertData => Unit = DemoAlert.testDataWithAlert(log, dataIter.frame.iterable.toIterator)

      // Go through the alerts and collect the data that occur within the alert's window.
      // If so update the status
      alerts.map { con =>
        if (conf.numMessages <= 0) {
          con.frame.iterable.foreach { m =>
            alertData(m).fold(e => println(s"Error $e parsing alert: $m"), ad => process(ad))
          }
          //f.iterable.foreach(println)
          //f.foreach(println)
          //f.iterable.toIterator.foreach(println)
        }
        else {
          con.frame.iterable.take(conf.numMessages).foreach { m =>
            alertData(m).fold(e => println(s"Error $e parsing alert: $m"), ad => process(ad))
          }
          //f.iterable.take(conf.numMessages).foreach(println)
          //f.iterable.foreach(println)
          //f.iterable.toIterator.take(conf.numMessages).foreach(println)
          // Close the connection so the application can terminate
          con.stop
        }
      }

    }


    val format = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss")

    def now: String = {
      val now = Calendar.getInstance
      format.format(now.getTime)
    }

    /**
      * Crates a GUI text window. Allows us to log to a graphical
      * text box (experimental).
      *
      * @return
      */
    def makeGUIControl: (TextArea, VBox) = {
      val label = new Label("Latest Alerts")

      val logs = new TextArea(s"$now: Started....\n")
      logs.setEditable(false)
      //logs.setMouseTransparent(true)
      //logs.setFocusTraversable(false)

      val layout = new VBox()
      VBox.setVgrow(logs, Priority.ALWAYS)
      layout.getChildren.addAll(label, logs)

      (logs, layout)
    }


    /**
      * Creates a JAvaFX application. It is set up so that closing the window
      * will exit (terminate) the application. It then creates a simple GUI
      * with a labeled frame, under this it places a graphical logging console,
      * and finally launches the data processing function as a thread. If we
      * only consume a fixed (finite) size of data, yhe GUI application
      * automatically terminates.
      *
      * Note that this function cannot take parameters.
      */
    class GUI extends Application {

      // https://stackoverflow.com/questions/24320014/how-to-call-launch-more-than-once-in-java
      override def stop(): Unit = {
        super.stop()
        // TODO: stop connection here
        // Because the connection and/or message consumption
        // is still pending, we need to force the exit
        System.exit(0)
      }

      override def init(): Unit = {
        super.init()
      }

      override def start(primaryStage: Stage): Unit = {

        primaryStage.setOnCloseRequest(_ => {
          // Will let stop execute
          println("Platform.exit()")
          Platform.exit()
        })

        val root = new GridPane
        val (log, ui) = makeGUIControl
        GridPane.setConstraints(ui, 0, 0)
        // Fill the whole width
        val column0 = new ColumnConstraints()
        column0.setHgrow(Priority.ALWAYS)
        root.getColumnConstraints.addAll(column0)

        // Fill the whole height
        val row0 = new RowConstraints()
        row0.setVgrow(Priority.ALWAYS)
        root.getRowConstraints.addAll(row0)

        root.getChildren.addAll(ui)


        primaryStage.setTitle("ADIRA Alerts")
        val scene = new Scene(root)
        primaryStage.setScene(scene)
        primaryStage.show()

        /*def printLog: Row => Unit = { r: Row =>
          val s = r.toString
          printMsg(log)(s)
        }*/

        def printAlertLog: AlertData => Unit = { a: AlertData =>
          printMsg(log)(a.toString)
        }

        //val p = Future(processData(recAppConf, printLog, Utils.toCSVStringIterator))
        val p = Future(processDataAlerts(recAppConf, printAlertLog, Utils.toCSVStringIterator))
        p.onComplete {
          case Success(_) =>
            // Should only reach here if we read a fixed number of messages
            ()
          case Failure(exception) =>
            println(s"Data processing thread failed: ${exception.getMessage}")
            println(s"""${exception.getStackTrace.mkString(",\n")}""")
            println("Platform.exit()")
            Platform.exit()
            ()
        }

      }

      def launch(args: String*): Unit = {
        Application.launch()
      }

    }


    /**
      * Main function - reads the configuration and initializes the GUI.
      *
      * Running an experiment:
      *
      * 1. Simulate the data input
      * root/runMain inegi.SendApp -verbose 1 -bufferSize 2 -numMessages 5000 -machineID m1 -sensorID s1 -dir ../data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv
      *
      * 2. Start the ML pipeline
      * root/runMain inegi.DemoDetect -verbose 1 -iter poll -waitMultiplier 2
      *
      * 3. Wait for and show alerts and corresponding sensor data
      * root/runMain inegi.DemoAlert -verbose 1 -iter poll -waitMultiplier 2
      *
      * @see [[DemoDetect]]
      * @param args command line arguments
      */
    def main(args: Array[String]): Unit = {
      import better.files.Dsl._
      println(cwd)

      // Start with default and update flag values
      val filename = "src/main/scala/inegi/AlertConfig"
      val default = ReceiveAppConfig(filename)
      // Parse command line parameters
      recAppConf = default.parse(args)

      // execute according to command line parameters
      if (recAppConf.showHelp) {
        recAppConf.printHelp()
      } else if (recAppConf.error.nonEmpty) {
        println(s"Errors found:")
        recAppConf.error.foreach(s => println("\t" + s))
        println("Terminating.")
      }
      else {
        Application.launch(classOf[GUI], args: _*)
        // Below initializes IDE twice
        //val fx = new IDE()
        //fx.launch(args:_*)
      }
    }
  }

