package inegi

import java.text.SimpleDateFormat
import java.time.{Duration, LocalDateTime}
import java.util
import java.util.Calendar

import com.rabbitmq.client.MessageProperties
import inegi.ReceiveApp.makeFrame
import pt.inescn.app.RabbitConfigs.ReceiveAppConfig
import pt.inescn.app.Utils
import pt.inescn.etl.stream.Load
import pt.inescn.etl.stream.Load.{ErrVal, Frame, Row, unpack}
import pt.inescn.etl.stream.RabbitMQ.{RabbitConnection, connect, stop}
import pt.inescn.plot.JPlotAlarmInterface
import pt.inescn.search.stream.Pipes.{compile, execDebug}
import pt.inescn.search.stream.Tasks.{ColumnNames, T}
import pt.inescn.search.stream.TasksFCC._
import pt.inescn.search.stream.UtilTasks.{project, renameColumns, toDouble, window}
import pt.inescn.utils.ADWError

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}


// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._

/**
  * This application consumes data from a message broker and processes it.
  * By default all of the data that is available is consumed. However during
  * system's testing and debugging we may be interested in receiving a limited
  * number of messages. We can receive a fixed number of messages using
  * the `-numMessages` flag.
  *
  * The messages are received from a specific queue of a message broker. The
  * connection configuration information is stored in a text file:
  *   `adw/core/src/main/scala/inegi/ReceiveConfig`
  * It contains information such as the host name or IP, the message
  * broker's queue name, and access credentials. These configurations
  * may be set or overridden in the command line. These include the
  * flags: `-host`, `-password`, `-port` `-userName` and `-queueName`.
  *
  * In addition to the flags above we also have: `-verbose` (print additional
  * information to the console indicating applications actions') and
  * `-waitMultiplier` (time in seconds to multiply to exponential back-off
  * wait time).
  *
  * Data reception is handled synchronously however this can be done either
  * using a *Pull* API or a *Callback* API. The `-iter` commandline parameter
  * may be use to set the data consumer to use the *pull* API (`poll`) or the
  * *callback* API (`CallBack`). Note that when polling we can set the maximum
  * time to wait for the next message when polling (`-waitMultiplier`). Note
  * that this parameter is only necessary when using the polling API.
  *
  * For a complete list of command line arguments use the -help argument.
  *
  * If the parsing of the commandline fails, a list of errors are accumulated
  * abd shown. The application then terminates.
  *
  * Here are two examples of command lines that load a single file:
  * root/runMain inegi.DemoDetect -verbose 1 -iter CallBack
  * root/runMain inegi.DemoDetect -verbose 1 -iter poll -waitMultiplier 2 -numMessages 1
  * root/runMain inegi.DemoDetect -verbose 1 -bufferMsgSize 100 -iter poll -waitMultiplier 2 -numMessages 1
  * root/runMain inegi.DemoDetect -verbose 1 -host 111.222.333.444 -queueName adira_inegi -userName inesc -password adira_1n35c -iter CallBack -waitMultiplier 2
  *
  * @see [[pt.inescn.etl.stream.RabbitMQ]] for information on the use of the iterators and
  *      the functions used to convert the JSON messages to [[pt.inescn.etl.stream.Load.Row]] iterators
  *      [[ReceiveApp]]
  *
  */
object DemoDetect {

  /**
    * Parameters for the LFCC deployment
    *
    * @param noiseLevel - noise levels in decibels (do not use in deployment)
    * @param FFTSz - Size of the FFT sliding window
    * @param FFTStp - Stride of the FFT sliding window
    * @param Fs - Sampling frequency used on the input signal
    * @param NmbFltrs - Number of filters to use in the filter bank
    * @param MaxFrq - band filter: maximum frequency
    * @param MinFrq - band filter: minimum frequency
    * @param NmbCpCoe - number of coefficients to create
    * @param FrmSz2 - smoothing sliding window: filter window size
    * @param FrmStp2 - smoothing sliding window: filter window stride
    */
  case class LFCCParams( noiseLevel: Double = 0, //dB
                         FFTSz: Int         = 128,
                         FFTStp: Int        = 64,
                         Fs:Double          = 5100,
                         NmbFltrs: Int      = 5,
                         MaxFrq: Double     = 2550.0,
                         MinFrq: Double     = 10.0,
                         NmbCpCoe: Int      = 5,
                         FrmSz2: Int        = 128, // Median window
                         FrmStp2: Int       = 1
                       )

  val QUEUE_ADIRA_DATA = "adira_data"
  val QUEUE_ADIRA_ALERTS = "adira_alerts"
  val QUEUE_ADIRA_ACCELEROMETER = "adira_accelerometer"

  private val format = new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")

  def now: String = {
    val now = Calendar.getInstance
    format.format(now.getTime)
  }

  /**
    * Task to read data from the RabbitMQ message broker.
    *
    * @param conf ReceiveApp configuration
    * @param con active connection to the message broker
    * @param i - input (not used)
    * @return Either an error or the Frame that will allow one to process the data
    */
  def readInput(conf: ReceiveAppConfig,
                con: Either[ADWError, RabbitConnection],
                i:Frame):  Either[ADWError,Frame] = {
    makeFrame(conf, con, Utils.toJSONIterator _) match {
      //case Right((f,_)) => Right(f)
      case Right(f) => Right(f.frame)
      case Left(e) => Left(e)
    }
  }


  /**
    * This task takes the predictions generated by the ML pipe placed in
    * the stream in `i`, extracts specific fields from this data and writes
    * it to the queue `queueName` using the connection configuration
    * `ReceiveAppConfig` and live connection `con`.
    *
    * The data that is placed on the que represents a possible alert - it
    * provides two timestamps that indicate the start and end of the time
    * window for which the prediction is made. It also indicates the machine
    * ID and sensor ID used by the model. It also includes a prediction
    * that is either 0 (no failure) or 1 (failure) detected. We also include
    * the current timestamp - this can be used to gauge latency and
    * processing time.
    *
    * Note that we need to send the non-alerts in order to ensure correct
    * synchronization with the sensor data (available in another queue).
    *
    * @see [[writeOutput0]]
    *
    * @param conf ReceiveApp configuration
    * @param queueName name of the que to which to write the data
    * @param con active connection to the message broker
    * @param i - input with the models' predictions
    * @return returns the data from the input `i` without change so that it
    *         can be processed by any of the pipes tasks the proceeded this
    *         one.
    */
  def writeOutput1(conf: ReceiveAppConfig,
                   queueName: String,
                   con: Either[ADWError, RabbitConnection],
                   i:Frame): Either[ADWError, Frame] = {
    con match {
      case Left(e) =>
        Left(ADWError(s"writeOutput1 : ${e.msg})"))
      case Right(conn) =>
        var lastNext: LocalDateTime = LocalDateTime.now()
        Right(i.map{ row =>
          val next = LocalDateTime.now()
          // Check if we detected a failure
          // val class = Load.unpack[String](r("damage")).right.get
          val v_det = row(RowNames.DET)
          val d: Either[String, Double] = Load.unpack[Double](v_det)

          d.foreach{ vd =>
            //println(s"writeOutput1: Det = $vd @ now ${ZonedDateTime.now()} ")
            //if (vd > 0) {
            // Get relevant information
            val v_timestamp = Load.unpack[mutable.ArraySeq[mutable.ArraySeq[_]]](row(RowNames.TIMESTAMP))
            val v_machineID = Load.unpack[mutable.ArraySeq[mutable.ArraySeq[_]]](row(RowNames.MACHINE_ID))
            val v_sensorID = Load.unpack[mutable.ArraySeq[mutable.ArraySeq[_]]](row(RowNames.SENSOR_ID))
            // extract values and create data to send
            val e = for {
              timestamp <- v_timestamp
              machineID <- v_machineID
              sensorID <- v_sensorID
              last_1 = timestamp.size - 1
              last_2 = timestamp(0).size - 1
              data = List(timestamp(0)(0), timestamp(last_1)(last_2), machineID(0)(0), sensorID(0)(0), vd.toString, now.toString)
            } yield data.mkString(",")
            // Send that to the GUI App if no errors occurred
            e.fold(
              // print error
              err => if (conf.verbose > 0) println(err),
              { msg =>
                val temp = msg.getBytes()
                conn.channel.basicPublish("", queueName, MessageProperties.TEXT_PLAIN, temp)
                if (conf.verbose > 0) { println(s"ALERT:\n $e") }
              }
            )
            //}
          }
          val diff = Duration.between(lastNext, next)
          println(s"Total time = $diff")
          lastNext = next
          // Same row to next step, no changes
          row
        })
    }
  }

  def zipColumnsCSVWithHeader(colsHeaders: List[String], cols : List[mutable.ArraySeq[String]]) : mutable.ArraySeq[String] = {
    val n = cols.head.length
    val buffer = mutable.ArraySeq.fill(n+1)("")
    cols.head.indices.foreach{ i =>
      val row: List[String] = cols.map(col => col(i) )
      val csvRow = row.mkString(",")
      buffer(i+1) = csvRow
    }
    buffer(0) = colsHeaders.mkString(",")
    buffer
  }

  def zipColumnsCSV(colsHeaders: List[String], cols : List[mutable.ArraySeq[String]]) : mutable.ArraySeq[String] = {
    val n = cols.head.length
    val buffer = mutable.ArraySeq.fill(n)("")
    cols.head.indices.foreach{ i =>
      val row: List[String] = cols.map(col => col(i) )
      val csvRow = row.mkString(",")
      buffer(i) = csvRow
    }
    buffer
  }

  /**
    * This task takes the predictions generated by the ML pipe placed in
    * the stream in `i`, extracts specific fields from this data and writes
    * it to the queue `queueName` using the connection configuration
    * `ReceiveAppConfig` and live connection `con`.
    *
    * The data that is placed on the que represents a possible alert - it
    * provides two timestamps that indicate the start and end of the time
    * window for which the prediction is made. It also indicates the machine
    * ID and sensor ID used by the model. It also includes a prediction
    * that is either 0 (no failure) or 1 (failure) detected. We also include
    * the current timestamp - this can be used to gauge latency and
    * processing time.
    *
    * Note that we need to send the non-alerts in order to ensure correct
    * synchronization with the sensor data (available in another queue).
    *
    * Unlike [[writeOutput1]] here we assume a windowed set of predictions
    * are the input. The size of this window is `bufferSize`
    *
    * @see [[writeOutput0]]
    *
    * @param bufferSize Size of input window
    * @param conf ReceiveApp configuration
    * @param queueName name of the que to which to write the data
    * @param con active connection to the message broker
    * @param i - input with the models' predictions
    * @return returns the data from the input `i` without change so that it
    *         can be processed by any of the pipes tasks the proceeded this
    *         one.
    */
  def writeOutput2(bufferSize: Int)
                  (conf: ReceiveAppConfig,
                   queueName: String,
                   con: Either[ADWError, RabbitConnection],
                   i:Frame): Either[ADWError, Frame] = {
    con match {
      case Left(e) =>
        Left(ADWError(s"writeOutput1 : ${e.msg})"))
      case Right(conn) =>
        var lastNext: LocalDateTime = LocalDateTime.now()
        Right(i.map{ row =>
          val next = LocalDateTime.now()
          // Check if we detected a failure
          // val class = Load.unpack[String](r("damage")).right.get
          val v_det = row(RowNames.DET)
          val d: Either[String, mutable.ArraySeq[Double]] = Load.unpack[mutable.ArraySeq[Double]](v_det)

          d.foreach{ vd =>
            // generate time-stamp with buffer length
            val nowVal = mutable.ArraySeq.fill(bufferSize)(now.toString)
            //println(s"writeOutput1: Det = $vd @ now ${ZonedDateTime.now()} ")
            //if (vd > 0) {
            // Get relevant information
            val v_timestamp = Load.unpack[mutable.ArraySeq[mutable.ArraySeq[mutable.ArraySeq[String]]]](row(RowNames.TIMESTAMP))
            val v_machineID = Load.unpack[mutable.ArraySeq[mutable.ArraySeq[mutable.ArraySeq[String]]]](row(RowNames.MACHINE_ID))
            val v_sensorID = Load.unpack[mutable.ArraySeq[mutable.ArraySeq[mutable.ArraySeq[String]]]](row(RowNames.SENSOR_ID))
            // extract values and create data to send
            val e = for {
              timestamp <- v_timestamp
              machineID <- v_machineID
              sensorID <- v_sensorID
              last_1 = timestamp.size - 1
              last_2 = timestamp(0).size - 1
              starts = timestamp.map(t => t(0)(0))
              ends = timestamp.map(t => t(last_1)(last_2))
              machId = machineID.map(t => t(0)(0))
              sensId = sensorID.map(t => t(0)(0))
              //data = List(timestamp(0)(0), timestamp(last_1)(last_2), machineID(0)(0), sensorID(0)(0), vd.toString, nowStr)
              /*data = List(starts.mkString(","), ends.mkString(","), machId.mkString(","),
                           sensId.mkString(","), vd.mkString(","), nowStr)*/
              data: mutable.ArraySeq[String] = zipColumnsCSV(
                List("start", "end", RowNames.MACHINE_ID, RowNames.SENSOR_ID, RowNames.DET, "processed"),
                List(starts, ends, machId, sensId, vd.map(_.toString), nowVal)
              )
            } yield data.mkString("\n")
            // Send that to the GUI App if no errors occurred
            e.fold(
              // print error
              err => if (conf.verbose > 0) println(err),
              { msg =>
                val temp = msg.getBytes()
                conn.channel.basicPublish("", queueName, MessageProperties.TEXT_PLAIN, temp)
                if (conf.verbose > 0) { println(s"ALERT:\n $e") }
              }
            )
            //}
          }
          val diff = Duration.between(lastNext, next)
          println(s"Total time = $diff")
          lastNext = next
          // Same row to next step, no changes
          row
        })
    }

  }

  /**
    * This task takes in the sensor data and funnels it to another queue
    * for later processing. It will later be mapped to the predictions
    * (alerts) and used for near real-time visualization. Not all the input
    * data is written to the new queue. We only store the relevant sensor
    * (accelerometer) datum which includes: the timestamp, the machine ID
    * on which the sensor is placed, the sensor ID and the accelerator
    * amplitudes in the X, Y and Z axis.
    *
    * NOTE: data can only be processed via the RabbitMQ message broker by
    * more than one client if we use topics (filters) however such messages
    * cannot be stored or persisted. We therefore use one queue to process
    * the data and send it to another queue for storage and later
    * processing. Note that this delay is not a problem because the latency
    * that is introduced is less than the compute time of the alerts. This
    * way we can always vew the unread data and its alerts if the
    * visualization application terminates unexpectedly.
    *
    * @see [[writeOutput1]]
    *
    * @param conf ReceiveApp configuration
    * @param queueName name of the que to which to write the data
    * @param con active connection to the message broker
    * @param i - input with the sensor's data
    * @return
    */
  def writeOutput0(conf: ReceiveAppConfig,
                   queueName: String,
                   con: Either[ADWError, RabbitConnection],
                   i:Frame): Either[ADWError, Frame] = {
    con match {
      case Left(e) =>
        Left(ADWError(s"writeOutput1 : ${e.msg})"))
      case Right(conn) =>
        Right(i.map{ row =>
          // Check if we detected a failure
          // val class = Load.unpack[String](r("damage")).right.get
          val v_timestamp = Load.unpack[String](row(RowNames.TIMESTAMP))
          val v_machineID = Load.unpack[String](row(RowNames.MACHINE_ID))
          val v_sensorID = Load.unpack[String](row(RowNames.SENSOR_ID))
          val v_x = Load.unpack[String](row(RowNames.X))
          val v_y = Load.unpack[String](row(RowNames.Y))
          val v_z = Load.unpack[String](row(RowNames.Z))

          val e = for {
            timestamp <- v_timestamp
            machineID <- v_machineID
            sensorID <- v_sensorID
            x <- v_x
            y <- v_y
            z <- v_z
            data = List(timestamp, machineID, sensorID, x, y, z, now)
          } yield data.mkString(",")
          // Send that to the GUI App if no errors occurred
          e.fold(
            // print error
            err => if (conf.verbose > 0) println(err),
            { msg =>
              val temp = msg.getBytes()
              conn.channel.basicPublish("", queueName, MessageProperties.TEXT_PLAIN, temp)
              //if (conf.verbose > 0) { println(s"DATA:\n $e") }
            }
          )
          // Same row to next step, no changes
          row
        })
    }
  }

  val bufferSize = 5000
  val buffer: ArrayBuffer[String] = mutable.ArrayBuffer.fill(bufferSize)("")
  var bufferIdx = 0

  def writeOutput0Buffered(conf: ReceiveAppConfig,
                   queueName: String,
                   con: Either[ADWError, RabbitConnection],
                   i:Frame): Either[ADWError, Frame] = {
    con match {
      case Left(e) =>
        Left(ADWError(s"writeOutput1 : ${e.msg})"))
      case Right(conn) =>
        Right(i.map{ row =>
          // Check if we detected a failure
          // val class = Load.unpack[String](r("damage")).right.get
          val v_timestamp = Load.unpack[String](row(RowNames.TIMESTAMP))
          val v_machineID = Load.unpack[String](row(RowNames.MACHINE_ID))
          val v_sensorID = Load.unpack[String](row(RowNames.SENSOR_ID))
          val v_x = Load.unpack[String](row(RowNames.X))
          val v_y = Load.unpack[String](row(RowNames.Y))
          val v_z = Load.unpack[String](row(RowNames.Z))

          val e = for {
            timestamp <- v_timestamp
            machineID <- v_machineID
            sensorID <- v_sensorID
            x <- v_x
            y <- v_y
            z <- v_z
            data = List(timestamp, machineID, sensorID, x, y, z, now)
          } yield data.mkString(",")
          // Send that to the GUI App if no errors occurred
          e.fold(
            // print error
            err => if (conf.verbose > 0) println(err),
            { msg =>

              // Add item if their is space
              if (bufferIdx < bufferSize){
                buffer(bufferIdx) = msg
                bufferIdx += 1
              }

              // If buffer is full send it off
              if (bufferIdx >= bufferSize){
                //val temp = msg.getBytes()
                val temp = buffer.mkString("\n").getBytes()
                conn.channel.basicPublish("", queueName, MessageProperties.TEXT_PLAIN, temp)
                //if (conf.verbose > 0) { println(s"DATA:\n $e") }
                bufferIdx = 0
              }
            }
          )
          // Same row to next step, no changes
          row
        })
    }

  }




  /**
    * Creates a Pipe that generates a single pipeline. This pipeline
    * implements an anomaly detection algorithm that indicates when an
    * anomaly has detected or not. An anomaly is assigned to a time window
    * and not a single sample.
    *
    * In order to facilitate visualization of the signals and show
    * when an anomaly has been detected, the sensor data that is used for
    * visualization is funneled to a persistent queue at the start of the
    * pipeline.
    *
    * The last task also stores the generate prediction - one for each
    * sliding window that is processed. The stored data is later
    * synchronized and visualized.
    *
    * @see [[DemoAlert]]
    *
    * @param conf ReceiveApp configuration
    * @param con active connection to the message broker
    * @param p LFCC anomaly detector parameters
    * @return Either an error or the Frame that will allow one to
    *         process the data
    */
  def pipeLineLFCC(conf: ReceiveAppConfig,
                   con: Either[ADWError, RabbitConnection],
                   p: LFCCParams): Either[ADWError, Frame] = {

    val bufferMsgSize = conf.bufferMsgSize

    val source = T(readInput _, conf, con)
    //val sink0 = T(writeOutput0 _, conf, QUEUE_ADIRA_ACCELEROMETER, con)
    val sink0 = T(writeOutput0Buffered _, conf, QUEUE_ADIRA_ACCELEROMETER, con)
    //val sink1 = T(writeOutput1 _, conf, QUEUE_ADIRA_ALERTS, con)
    val sink1 = T(writeOutput2(bufferMsgSize) _, conf, QUEUE_ADIRA_ALERTS, con)

    val s = source ->:
      sink0 ->:
      renameColumns("Time(s)" -> "t")  ->:            //Rename column
      toDouble("t", "X", "Y", "Z") ->:
      valMapStep( Map("b" -> 1.0 , "norm" -> 0.0 ), "damage" ) ->:
      //plotStreamStep(20000,"X", "Y", "Z") ->:
      //plotAlarmInterfaceStep( 3000, "X","Y","Z","damage") ->:
      window(p.FFTSz,p.FFTStp) ->:                                                      // return vectors of frames 4x1x16(x,y,z,IsAnomaly)
      addWhiteGaussianNoiseStep("WgnAdd",p.noiseLevel,1,"X","Y","Z")  ->:     // Add white noise to each frame
      modeStep("Mode","damage")  ->:
      lfccStep("Lfcc",p.Fs, p.NmbFltrs, p.MaxFrq, p.MinFrq,p.NmbCpCoe,"XWgnAdd","YWgnAdd","ZWgnAdd") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      //emdFeatStep("emdEn","XWgnAdd","YWgnAdd","ZWgnAdd")  ->:
      concatenateStep("Lfcc","XWgnAddLfcc","YWgnAddLfcc","ZWgnAddLfcc") ->:
      //concatenateStep("EmdEn","XWgnAddEmdEn","YWgnAddEmdEn","ZWgnAddEmdEn") ->:
      window(p.FrmSz2,p.FrmStp2) ->:                          //return vectors of features 64x15(lfcc)
      medianStep("Median",p.NmbCpCoe*3,p.FrmSz2,"Lfcc") ->:   //1x15 (lfccmedian,damagemedian)
      modeStep("Mode","damageMode") ->:                                     // (isanomalymode)
      anomalyDetectionTrainStep("Det",p.NmbCpCoe*3,0.9,0.9,200,"LfccMedian") ->:
      anomDetectionEvalStep( "damageModeMode" , "Det" , "far" )  ->:
    //plotStreamStep(20000,"XWgnAdd","YWgnAdd","ZWgnAdd")
    //plotAlarmInterfaceStep( 3000, "X","Y","Z","damage")
      project(ColumnNames(List("timeStamps", "machineID", "sensorID", "damageModeMode", "Det" , "far"))) ->:
      window(bufferMsgSize, bufferMsgSize) ->:
      sink1

    // compile new pipes
    val c = compile(s)

    // Assume we have a finite set of pipelines
    val cl = c.toList
    if (conf.verbose  > 0) println(s"Compiled ${cl.length} pipelines")
    // We will only execute one pipeline
    if (cl.size > 1){
      Left(ADWError(s"Expected single pipeline Pipe but have ${cl.size} of them."))
    } else {
      // Execute the pipes
      val r = execDebug(cl)(Frame(("",List())))

      // Assume we only have pipeline:
      r.head._1
    }
  }

  /**
    * Initialization function that opens the connection to the message
    * broker and prepares the queues for writing data (sensor data and
    * the predicted anomaly detections). It then asserts anf compiles
    * the anomaly detection pipeline. The resulting stream is the
    * processed by this pipeline.
    *
    * @param conf ReceiveApp configuration
    * @param p LFCC anomaly detector parameters
    */
  def processData(conf: ReceiveAppConfig, p: LFCCParams): Unit = {

    val con = connect(conf)
    con match {
      case Left(ADWError(e)) =>
        println(s"Error: terminating - $e")
      case Right(conn) =>
        // Make sure the alerts queue exists
        conn.channel.queueDeclare(QUEUE_ADIRA_ALERTS, true, false, false, null)
        // Make sure the alerts accelerometer data queue exists
        conn.channel.queueDeclare(QUEUE_ADIRA_ACCELEROMETER, true, false, false, null)
        // Create processing pipeline
        val pipeline = pipeLineLFCC(conf, con, p)
        // Process data (we can also use the 'iterator.last')
        pipeline.map{ f =>
          if (conf.numMessages <= 0) {
            // .project("timeStamps", "machineID", "sensorID", "det")
            //f.iterable.foreach(println)
            f.iterable.foreach(_ => print("."))
            //f.foreach(println)
            //f.iterable.toIterator.foreach(println)
          }
          else {
            //f.iterable.take(conf.numMessages).foreach(println)
            f.iterable.take(conf.numMessages).foreach(_ => print("."))
            //f.iterable.toIterator.take(conf.numMessages).foreach(println)
            // Close the connection so the application can terminate
            stop(conf, con)
          }
        }
    }
  }


  /**
    * Main function - reads the configuration and initializes the ML pipeline.
    *
    * Running an experiment:
    *
    * 1. Simulate the data input
    * root/runMain inegi.SendApp -verbose 1 -bufferSize 2 -numMessages 5000 -machineID m1 -sensorID s1 -dir ../data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv
    *
    * 2. Start the ML pipeline
    * root/runMain inegi.DemoDetect -verbose 1 -iter poll -waitMultiplier 2
    *
    * 3. Wait for and show alerts and corresponding sensor data
    * root/runMain inegi.DemoAlert -verbose 1 -iter poll -waitMultiplier 2
    *
    * @see [[DemoAlert]]
    *
    * @param args command line arguments
    */
  def main(args: Array[String]): Unit = {

    // Start with default and update flag values
    val filename = "src/main/scala/inegi/ReceiveConfig"
    val default = ReceiveAppConfig(filename)
    // Parse command line parameters
    val recAppConf = default.parse(args)

    // execute according to command line parameters
    if (recAppConf.showHelp) {
      recAppConf.printHelp()
    } else if (recAppConf.error.nonEmpty) {
      println(s"Errors found:")
      recAppConf.error.foreach(s => println("\t"+s))
      println("Terminating.")
    }
    else {
      processData(recAppConf, LFCCParams())

    }
  }

}
