package inegi

import java.lang.{System => JSystem}
import pt.inescn.plot.JPlot
import pt.inescn.utils.NemenyiPlot
import pt.inescn.samplers.stream.Ops.POps
import pt.inescn.samplers.stream.Ops.flatten
import better.files.Dsl.cwd
import better.files.File
import com.github.tototoshi.csv.{CSVFormat, DefaultCSVFormat}
import pt.inescn.etl.stream.{DataFiles, Load}
import pt.inescn.etl.stream.Load._
import pt.inescn.utils.NemenyiPlot.{Plot, computeCD, computeOrder}
import pt.inescn.utils.Table.Table2Latex
import pt.inescn.search.stream.Pipes._
import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.UtilTasks._
import pt.inescn.search.stream.TasksFCC._
import pt.inescn.search.stream.TestTasks.loadExtraINEGICSVs
import pt.inescn.utils.ADWError
import pt.inescn.app.Utils
import pt.inescn.search.stream.Pipes
import pt.inescn.utils.ADWError
import pt.inescn.app.Utils

// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._




/**
  * This file consist of a set of experiments that evaluate the performance
  * of the anomaly (outlier) detection algorithm. The data splitting protocol
  * used makes sure we do not leak data to/from the training set. We do this
  * by making sure that the bearings used in training are **not** used in the
  * test data set.
  *
  * Results:
  * 24/10/2018 - We investigate how stable the FCCs ae by checking how the
  * results vary if we use only a single experiment, use all 5 experiments
  * done under the same conditions of speed, vary bearings and experiments
  * under the same speed and finally vary the bearings (and experiments) and
  * the speed. We see that:
  *
  * Test 1: We see that the FCCs perform well for a single bearing/experiment
  *
  * Test 2: Their is a difference in experiments (same bearing in non-failure
  * and failure mode) because the very simple Cantelli based detection
  * algorithm has varying performance and can have accuracy as low as 0.868
  * and false alarm rate as high as 0.131.
  *
  * Test 3: uses different bearings at the same speed and we see an even
  * greater decrease in performance with an accuracy of 0.5136 and a false
  * alarm rate of 0.4863. Surprisingly, we can hypothesize that the bearings'
  * performance are in fact different.
  *
  * Test 4: uses the same bearings at different speeds. Here we expect even
  * greater decreases in performance. However we only observe a slight
  * decrease in performance with an accuracy of ???? and a false alarm rate
  * of ???. Unsurprisingly we confirm that the very simple Cantelli test
  * cannot deal with different operating frequencies. We need a more
  * sophisticated algorithm for anomaly detection (that can deal with concept
  * drift also)
  *
  * Test 5: uses different bearings at different speeds. Here we expect the
  * greatest decreases in performance. However we only observe a slight
  * decrease in performance with an accuracy of 0.5013 and a false alarm rate
  * of 0.4986 (as compared to tests 3 and 4). We cannot conclude much from
  * these because the performance of the other tests are already signficianlty
  * bad (slightly better than a random choice of 50%).
  *
  * TODO: we need to change the test protocol so that the training set only
  * contain non-failures but the test data set that have both the failure and
  * non-failure modes mixed.
  *
  * root/runMain inegi.ADIRAI40Experiments param1 param2
  * root/runMain inegi.ADIRAI40Experiments 1
  */
object ADIRAI40Experiments {


  /**
    * root/runMain inegi.ADIRAI40Experiments 1
    *
    * Executing experiment 1.
    * Execute pipe. read3.size = 510
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.9117647058823529)
    * r2_far = Right(0.08823529411764706)
    *
    * Executing experiment 1.
    * Execute pipe. read3.size = 510
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.9117647058823529)
    * r2_far = Right(0.08823529411764706)
    */
  def exp1(): Unit = {

    // Data files with experimental data
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    // Collect the file names and the features encoded in each of those file names
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList
    // We should have all the files (no parsing errors)
    val n_errors = DataFiles.countParseErrors(features1.toIterator)
    assert(n_errors == 0)
    // Get what we parsed correctly
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)

    /*
       Test protocol:
       . select all experiments of a single bearing working at the same speed
       . place all the non-faulty records at the start
       . place all the faulty records at the end
       . apply the pipeline with the standard parameters (refer to paper)
     */

    case class FileFilter(damage:String, bearing:String, speed:String, exp:String)

    // What is used to tag the class
    // See pt.inescn.app.Utils.Utils for file feature_names : "bearing", "speed", "load", "damage", "damage_size", "exp"
    def classes(r: Row) : FileFilter = {
      val damage= Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      val speed = Load.unpack[String](r("speed")).right.get
      val exp = Load.unpack[String](r("exp")).right.get
      FileFilter(damage, bearing, speed, exp)
    }

    // Collect the non-faulty data
    def learnFilter(r: FileFilter) : Boolean = {
      (r.damage == "norm") && (r.bearing == "1") && (r.speed == "1000") && (r.exp == "1")
    }
    // Collect the faulty data
    def testFilter(r: FileFilter) : Boolean = {
      (r.damage != "norm") && (r.bearing == "1") && (r.speed == "1000") && (r.exp == "1")
    }

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    // Select the data files and shuffle them (note that we do not mix faulty and non-faulty data)
    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = true)

    // Create a pipe task that loads the file data
    val loader = T(DataFiles.splitExtendCSVsData _, MyFormat, trainData, testData, 0, 0, 0, false)

    // Pipeline parameters
    // Gets 2048 lines
    // val offset = 0.000195
    val FFTSz = 256
    val Fs = 5100
    val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val FrmSz2 = 128
    val FrmStp2 = 1

    // Full learning pipeline
    val s3 = loader ->:
      //renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly")  ->: // Rename column
      renameColumns("Time(s)" -> "t","damage" -> "isanomaly")  ->:    // Rename column
      convertLabelValue("isanomaly") ->:                              // convert types (to boolean)
      boolToDouble("isanomaly") ->:                                   // convert types (to double)
      toDouble("t", "X", "Y", "Z" )  ->:                              // convert types
      window(FFTSz,64) ->:                                            // vectors of frames 4x1x16(x,y,z,IsAnomaly)
      modeStep("mode","isanomaly") ->:                                // TODO: comment (isanomalymode)
      lfccStep("lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc") ->:             // 1x15 (lfcc)
      window(FrmSz2,FrmStp2) ->:                                      // vectors of features 64x15(lfcc)
      medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc") ->:               // 1x15 (lfccmedian,damagemedian)
      modeStep("mode","isanomalymode")  ->:                           // (isanomalymode) // TODO: ??, repeated?
      anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian") ->: // TODO: ??
      anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" )    // TODO comment + plot

    // compile new pipes
    val c3 = compile(s3)
    //val c3l = c3.toList
    //println(c3l)

    // Execute the pipes
    val r3 = execDebug(c3)(Frame(("",List())))
    //println(r3)

    // Get the results (not executed yet)
    // We only grab one (first) result
    val result: Frame = r3.head._1.right.get
    //println(result.colTypes)

    // Collect all the results (executes now)
    val read3 = result.project("bearing", "det","isanomalymodemode","acc","far").iterable.toIterator.toIndexedSeq
    println(s"Execute pipe. read3.size = ${read3.size}")

    // check if the bearing selection is correct
    //println(read3.mkString(","))

    // Grab the last stream record, it will contain the prediction
    // and the (prequential) performance evaluation
    val k = read3.size - 1
    val r2 = read3(k)

    // The accuracy varies substantially through the time-series (stream)
    // For this model it is a global evaluation (includes all past accuracy values)

    // prediction
    val r2_det: Either[String, Double] = unpack[Double](r2("det"))
    println(s"r2_det = $r2_det")

    // label
    val r2_isanomalymodemode: Either[String, Double] = unpack[Double](r2("isanomalymodemode"))
    println(s"r2_isanomalymodemode = $r2_isanomalymodemode")

    // accuracy
    val r2_acc: Either[String, Double] = unpack[Double](r2("acc"))
    println(s"r2_acc = $r2_acc")

    // false alarm rate
    val r2_far: Either[String, Double] = unpack[Double](r2("far"))
    println(s"r2_far = $r2_far")
  }


  /**
    * Variability between experiments
    *
    * root/runMain inegi.ADIRAI40Experiments 2
    *
    * Executing experiment 2.
    * Execute pipe. read3.size = 3070
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.8684039087947882)
    * r2_far = Right(0.13159609120521173)
    *
    * Executing experiment 2.
    * Execute pipe. read3.size = 3070
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.9700325732899023)
    * r2_far = Right(0.02996742671009772)
    *
    * Executing experiment 2.
    * Execute pipe. read3.size = 3070
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.7514657980456027)
    * r2_far = Right(0.2485342019543974)
    *
    * Executing experiment 2.
    * Execute pipe. read3.size = 3070
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.8882736156351791)
    * r2_far = Right(0.11172638436482085)
    *
    * Executing experiment 2.
    * Execute pipe. read3.size = 3070
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.9530944625407166)
    * r2_far = Right(0.046905537459283386)
    */
  def exp2(): Unit = {

    // Data files with experimental data
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    // Collect the file names and the features encoded in each of those file names
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList
    // We should have all the files (no parsing errors)
    val n_errors = DataFiles.countParseErrors(features1.toIterator)
    assert(n_errors == 0)
    // Get what we parsed correctly
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)

    /*
       Test protocol:
       . select all experiments of a single bearing working at the same speed
       . place all the non-faulty records at the start
       . place all the faulty records at the end
       . apply the pipeline with the standard parameters (refer to paper)
     */

    case class FileFilter(damage:String, bearing:String, speed:String)

    // What is used to tag the class
    // See pt.inescn.app.Utils.Utils for file feature_names : "bearing", "speed", "load", "damage", "damage_size", "exp"
    def classes(r: Row) : FileFilter = {
      val damage= Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      val speed = Load.unpack[String](r("speed")).right.get
      FileFilter(damage, bearing, speed)
    }

    // Collect the non-faulty data
    def learnFilter(r: FileFilter) : Boolean = { (r.damage == "norm") && (r.bearing == "1") && (r.speed == "1000") }
    // Collect the faulty data
    def testFilter(r: FileFilter) : Boolean = { (r.damage != "norm") && (r.bearing == "1") && (r.speed == "1000")  }

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    // Select the data files and shuffle them (note that we do not mix faulty and non-faulty data)
    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = true)

    // Create a pipe task that loads the file data
    val loader = T(DataFiles.splitExtendCSVsData _, MyFormat, trainData, testData, 0, 0, 0, false)

    // Pipeline parameters
    // Gets 2048 lines
    // val offset = 0.000195
    val FFTSz = 256
    val Fs = 5100
    val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val FrmSz2 = 128
    val FrmStp2 = 1

    // Full learning pipeline
    val s3 = loader ->:
      //renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly")  ->: // Rename column
      renameColumns("Time(s)" -> "t","damage" -> "isanomaly")  ->:    // Rename column
      convertLabelValue("isanomaly") ->:                              // convert types (to boolean)
      boolToDouble("isanomaly") ->:                                   // convert types (to double)
      toDouble("t", "X", "Y", "Z" )  ->:                              // convert types
      window(FFTSz,64) ->:                                            // vectors of frames 4x1x16(x,y,z,IsAnomaly)
      modeStep("mode","isanomaly") ->:                                // TODO: comment (isanomalymode)
      lfccStep("lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc") ->:             // 1x15 (lfcc)
      window(FrmSz2,FrmStp2) ->:                                      // vectors of features 64x15(lfcc)
      medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc") ->:               // 1x15 (lfccmedian,damagemedian)
      modeStep("mode","isanomalymode")  ->:                           // (isanomalymode) // TODO: ??, repeated?
      anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian") ->: // TODO: ??
      anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" )    // TODO comment + plot

    // compile new pipes
    val c3 = compile(s3)
    //val c3l = c3.toList
    //println(c3l)

    // Execute the pipes
    val r3 = execDebug(c3)(Frame(("",List())))
    //println(r3)

    // Get the results (not executed yet)
    // We only grab one (first) result
    val result: Frame = r3.head._1.right.get
    //println(result.colTypes)

    // Collect all the results (executes now)
    val read3 = result.project("bearing", "det","isanomalymodemode","acc","far").iterable.toIterator.toIndexedSeq
    println(s"Execute pipe. read3.size = ${read3.size}")

    // check if the bearing selection is correct
    //println(read3.mkString(","))

    // Grab the last stream record, it will contain the prediction
    // and the (prequential) performance evaluation
    val k = read3.size - 1
    val r2 = read3(k)

    // The accuracy varies substantially through the time-series (stream)
    // For this model it is a global evaluation (includes all past accuracy values)

    // prediction
    val r2_det: Either[String, Double] = unpack[Double](r2("det"))
    println(s"r2_det = $r2_det")

    // label
    val r2_isanomalymodemode: Either[String, Double] = unpack[Double](r2("isanomalymodemode"))
    println(s"r2_isanomalymodemode = $r2_isanomalymodemode")

    // accuracy
    val r2_acc: Either[String, Double] = unpack[Double](r2("acc"))
    println(s"r2_acc = $r2_acc")

    // false alarm rate
    val r2_far: Either[String, Double] = unpack[Double](r2("far"))
    println(s"r2_far = $r2_far")
  }

  /**
    * Variability between bearings (and experiments)
    *
    * root/runMain inegi.ADIRAI40Experiments 3
    *
    * Executing experiment 3.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.5124604012671594)
    * r2_far = Right(0.48753959873284053)
    *
    * Executing experiment 3.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.5143611404435058)
    * r2_far = Right(0.4856388595564942)
    *
    * Executing experiment 3.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.6232312565997888)
    * r2_far = Right(0.3767687434002112)
    *
    * Executing experiment 3.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.53220696937698)
    * r2_far = Right(0.4677930306230201)
    *
    * Executing experiment 3.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.5136219640971489)
    * r2_far = Right(0.4863780359028511)
    */
  def exp3(): Unit = {

    // Data files with experimental data
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    // Collect the file names and the features encoded in each of those file names
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList
    // We should have all the files (no parsing errors)
    val n_errors = DataFiles.countParseErrors(features1.toIterator)
    assert(n_errors == 0)
    // Get what we parsed correctly
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)

    /*
       Test protocol:
       . select all experiments of a single bearing working at the same speed
       . place all the non-faulty records at the start
       . place all the faulty records at the end
       . apply the pipeline with the standard parameters (refer to paper)
     */

    case class FileFilter(damage:String, bearing:String, speed:String)

    // What is used to tag the class
    // See pt.inescn.app.Utils.Utils for file feature_names : "bearing", "speed", "load", "damage", "damage_size", "exp"
    def classes(r: Row) : FileFilter = {
      val damage= Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      val speed = Load.unpack[String](r("speed")).right.get
      FileFilter(damage, bearing, speed)
    }

    // Collect the non-faulty data
    def learnFilter(r: FileFilter) : Boolean = { (r.damage == "norm") && (r.speed == "1000") }
    // Collect the faulty data
    def testFilter(r: FileFilter) : Boolean = { (r.damage != "norm") && (r.speed == "1000")  }

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    // Select the data files and shuffle them (note that we do not mix faulty and non-faulty data)
    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = true)

    // Create a pipe task that loads the file data
    val loader = T(DataFiles.splitExtendCSVsData _, MyFormat, trainData, testData, 0, 0, 0, false)

    // Pipeline parameters
    // Gets 2048 lines
    // val offset = 0.000195
    val FFTSz = 256
    val Fs = 5100
    val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val FrmSz2 = 128
    val FrmStp2 = 1

    // Full learning pipeline
    val s3 = loader ->:
      //renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly")  ->: // Rename column
      renameColumns("Time(s)" -> "t","damage" -> "isanomaly")  ->:    // Rename column
      convertLabelValue("isanomaly") ->:                              // convert types (to boolean)
      boolToDouble("isanomaly") ->:                                   // convert types (to double)
      toDouble("t", "X", "Y", "Z" )  ->:                              // convert types
      window(FFTSz,64) ->:                                            // vectors of frames 4x1x16(x,y,z,IsAnomaly)
      modeStep("mode","isanomaly") ->:                                // TODO: comment (isanomalymode)
      lfccStep("lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc") ->:             // 1x15 (lfcc)
      window(FrmSz2,FrmStp2) ->:                                      // vectors of features 64x15(lfcc)
      medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc") ->:               // 1x15 (lfccmedian,damagemedian)
      modeStep("mode","isanomalymode")  ->:                           // (isanomalymode) // TODO: ??, repeated?
      anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian") ->: // TODO: ??
      anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" )    // TODO comment + plot

    // compile new pipes
    val c3 = compile(s3)
    //val c3l = c3.toList
    //println(c3l)

    // Execute the pipes
    val r3 = execDebug(c3)(Frame(("",List())))
    //println(r3)

    // Get the results (not executed yet)
    // We only grab one (first) result
    val result: Frame = r3.head._1.right.get
    //println(result.colTypes)

    // Collect all the results (executes now)
    val read3 = result.project("bearing", "det","isanomalymodemode","acc","far").iterable.toIterator.toIndexedSeq
    println(s"Execute pipe. read3.size = ${read3.size}")

    // check if the bearing selection is correct
    //println(read3.mkString(","))

    // Grab the last stream record, it will contain the prediction
    // and the (prequential) performance evaluation
    val k = read3.size - 1
    val r2 = read3(k)

    // The accuracy varies substantially through the time-series (stream)
    // For this model it is a global evaluation (includes all past accuracy values)

    // prediction
    val r2_det: Either[String, Double] = unpack[Double](r2("det"))
    println(s"r2_det = $r2_det")

    // label
    val r2_isanomalymodemode: Either[String, Double] = unpack[Double](r2("isanomalymodemode"))
    println(s"r2_isanomalymodemode = $r2_isanomalymodemode")

    // accuracy
    val r2_acc: Either[String, Double] = unpack[Double](r2("acc"))
    println(s"r2_acc = $r2_acc")

    // false alarm rate
    val r2_far: Either[String, Double] = unpack[Double](r2("far"))
    println(s"r2_far = $r2_far")
  }


  /**
    * Variability between speed (same bearing and different experiments)
    *
    * root/runMain inegi.ADIRAI40Experiments 4
    *
    * Executing experiment 4.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.6877507919746568)
    * r2_far = Right(0.3122492080253432)
    *
    * Executing experiment 4.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.593558606124604)
    * r2_far = Right(0.40644139387539596)
    *
    * Executing experiment 4.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.580570221752904)
    * r2_far = Right(0.4194297782470961)
    *
    * Executing experiment 4.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.6866948257655755)
    * r2_far = Right(0.3133051742344245)
    *
    * Executing experiment 4.
    * Execute pipe. read3.size = 9470
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.5803590285110877)
    * r2_far = Right(0.41964097148891233)
    **/
  def exp4(): Unit = {

    // Data files with experimental data
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    // Collect the file names and the features encoded in each of those file names
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList
    // We should have all the files (no parsing errors)
    val n_errors = DataFiles.countParseErrors(features1.toIterator)
    assert(n_errors == 0)
    // Get what we parsed correctly
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)

    /*
       Test protocol:
       . select all experiments of a single bearing working at the same speed
       . place all the non-faulty records at the start
       . place all the faulty records at the end
       . apply the pipeline with the standard parameters (refer to paper)
     */

    case class FileFilter(damage:String, bearing:String, speed:String)

    // What is used to tag the class
    // See pt.inescn.app.Utils.Utils for file feature_names : "bearing", "speed", "load", "damage", "damage_size", "exp"
    def classes(r: Row) : FileFilter = {
      val damage= Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      val speed = Load.unpack[String](r("speed")).right.get
      FileFilter(damage, bearing, speed)
    }

    // Collect the non-faulty data
    def learnFilter(r: FileFilter) : Boolean = { (r.damage == "norm") && (r.bearing == "1") }
    // Collect the faulty data
    def testFilter(r: FileFilter) : Boolean = { (r.damage != "norm")  && (r.bearing == "1")}

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    // Select the data files and shuffle them (note that we do not mix faulty and non-faulty data)
    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = true)

    // Create a pipe task that loads the file data
    val loader = T(DataFiles.splitExtendCSVsData _, MyFormat, trainData, testData, 0, 0, 0, false)

    // Pipeline parameters
    // Gets 2048 lines
    // val offset = 0.000195
    val FFTSz = 256
    val Fs = 5100
    val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val FrmSz2 = 128
    val FrmStp2 = 1

    // Full learning pipeline
    val s3 = loader ->:
      //renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly")  ->: // Rename column
      renameColumns("Time(s)" -> "t","damage" -> "isanomaly")  ->:    // Rename column
      convertLabelValue("isanomaly") ->:                              // convert types (to boolean)
      boolToDouble("isanomaly") ->:                                   // convert types (to double)
      toDouble("t", "X", "Y", "Z" )  ->:                              // convert types
      window(FFTSz,64) ->:                                            // vectors of frames 4x1x16(x,y,z,IsAnomaly)
      modeStep("mode","isanomaly") ->:                                // TODO: comment (isanomalymode)
      lfccStep("lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc") ->:             // 1x15 (lfcc)
      window(FrmSz2,FrmStp2) ->:                                      // vectors of features 64x15(lfcc)
      medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc") ->:               // 1x15 (lfccmedian,damagemedian)
      modeStep("mode","isanomalymode")  ->:                           // (isanomalymode) // TODO: ??, repeated?
      anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian") ->: // TODO: ??
      anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" )    // TODO comment + plot

    // compile new pipes
    val c3 = compile(s3)
    //val c3l = c3.toList
    //println(c3l)

    // Execute the pipes
    val r3 = execDebug(c3)(Frame(("",List())))
    //println(r3)

    // Get the results (not executed yet)
    // We only grab one (first) result
    val result: Frame = r3.head._1.right.get
    //println(result.colTypes)

    // Collect all the results (executes now)
    val read3 = result.project("bearing", "det","isanomalymodemode","acc","far").iterable.toIterator.toIndexedSeq
    println(s"Execute pipe. read3.size = ${read3.size}")

    // check if the bearing selection is correct
    //println(read3.mkString(","))

    // Grab the last stream record, it will contain the prediction
    // and the (prequential) performance evaluation
    val k = read3.size - 1
    val r2 = read3(k)

    // The accuracy varies substantially through the time-series (stream)
    // For this model it is a global evaluation (includes all past accuracy values)

    // prediction
    val r2_det: Either[String, Double] = unpack[Double](r2("det"))
    println(s"r2_det = $r2_det")

    // label
    val r2_isanomalymodemode: Either[String, Double] = unpack[Double](r2("isanomalymodemode"))
    println(s"r2_isanomalymodemode = $r2_isanomalymodemode")

    // accuracy
    val r2_acc: Either[String, Double] = unpack[Double](r2("acc"))
    println(s"r2_acc = $r2_acc")

    // false alarm rate
    val r2_far: Either[String, Double] = unpack[Double](r2("far"))
    println(s"r2_far = $r2_far")
  }


  /**
    * Variability between speed (different bearing and different experiments)
    *
    * root/runMain inegi.ADIRAI40Experiments 5
    *
    * Executing experiment 5.
    * Execute pipe. read3.size = 28670
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.5013254272758981)
    * r2_far = Right(0.49867457272410187)
    *
    * Executing experiment 5.
    * Execute pipe. read3.size = 28670
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.527310777816533)
    * r2_far = Right(0.47268922218346704)
    *
    * Executing experiment 5.
    * Execute pipe. read3.size = 28670
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.5303453086850366)
    * r2_far = Right(0.4696546913149634)
    *
    * Executing experiment 5.
    * Execute pipe. read3.size = 28670
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.5267875828392048)
    * r2_far = Right(0.47321241716079526)
    *
    * Executing experiment 5.
    * Execute pipe. read3.size = 28670
    * r2_det = Right(1.0)
    * r2_isanomalymodemode = Right(1.0)
    * r2_acc = Right(0.54227415416812)
    * r2_far = Right(0.45772584583188003)
    */
  def exp5(): Unit = {

    // Data files with experimental data
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    // Collect the file names and the features encoded in each of those file names
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList
    // We should have all the files (no parsing errors)
    val n_errors = DataFiles.countParseErrors(features1.toIterator)
    assert(n_errors == 0)
    // Get what we parsed correctly
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)

    /*
       Test protocol:
       . select all experiments of a single bearing working at the same speed
       . place all the non-faulty records at the start
       . place all the faulty records at the end
       . apply the pipeline with the standard parameters (refer to paper)
     */

    case class FileFilter(damage:String, bearing:String, speed:String)

    // What is used to tag the class
    // See pt.inescn.app.Utils.Utils for file feature_names : "bearing", "speed", "load", "damage", "damage_size", "exp"
    def classes(r: Row) : FileFilter = {
      val damage= Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      val speed = Load.unpack[String](r("speed")).right.get
      FileFilter(damage, bearing, speed)
    }

    // Collect the non-faulty data
    def learnFilter(r: FileFilter) : Boolean = { r.damage == "norm" }
    // Collect the faulty data
    def testFilter(r: FileFilter) : Boolean = { r.damage != "norm"  }

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    // Select the data files and shuffle them (note that we do not mix faulty and non-faulty data)
    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = true)

    // Create a pipe task that loads the file data
    val loader = T(DataFiles.splitExtendCSVsData _, MyFormat, trainData, testData, 0, 0, 0, false)

    // Pipeline parameters
    // Gets 2048 lines
    //val offset = 0.000195
    val FFTSz = 256
    val Fs = 5100
    val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val FrmSz2 = 128
    val FrmStp2 = 1

    // Full learning pipeline
    val s3 = loader ->:
      //renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly")  ->: // Rename column
      renameColumns("Time(s)" -> "t","damage" -> "isanomaly")  ->:    // Rename column
      convertLabelValue("isanomaly") ->:                              // convert types (to boolean)
      boolToDouble("isanomaly") ->:                                   // convert types (to double)
      toDouble("t", "X", "Y", "Z" )  ->:                              // convert types
      window(FFTSz,64) ->:                                            // vectors of frames 4x1x16(x,y,z,IsAnomaly)
      modeStep("mode","isanomaly") ->:                                // TODO: comment (isanomalymode)
      lfccStep("lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc") ->:             // 1x15 (lfcc)
      window(FrmSz2,FrmStp2) ->:                                      // vectors of features 64x15(lfcc)
      medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc") ->:               // 1x15 (lfccmedian,damagemedian)
      modeStep("mode","isanomalymode")  ->:                           // (isanomalymode) // TODO: ??, repeated?
      anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian") ->: // TODO: ??
      anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" )    // TODO comment + plot

    // compile new pipes
    val c3 = compile(s3)
    //val c3l = c3.toList
    //println(c3l)

    // Execute the pipes
    val r3 = execDebug(c3)(Frame(("",List())))
    //println(r3)

    // Get the results (not executed yet)
    // We only grab one (first) result
    val result: Frame = r3.head._1.right.get
    //println(result.colTypes)

    // Collect all the results (executes now)
    val read3 = result.project("bearing", "det","isanomalymodemode","acc","far").iterable.toIterator.toIndexedSeq
    println(s"Execute pipe. read3.size = ${read3.size}")

    // check if the bearing selection is correct
    //println(read3.mkString(","))

    // Grab the last stream record, it will contain the prediction
    // and the (prequential) performance evaluation
    val k = read3.size - 1
    val r2 = read3(k)

    // The accuracy varies substantially through the time-series (stream)
    // For this model it is a global evaluation (includes all past accuracy values)

    // prediction
    val r2_det: Either[String, Double] = unpack[Double](r2("det"))
    println(s"r2_det = $r2_det")

    // label
    val r2_isanomalymodemode: Either[String, Double] = unpack[Double](r2("isanomalymodemode"))
    println(s"r2_isanomalymodemode = $r2_isanomalymodemode")

    // accuracy
    val r2_acc: Either[String, Double] = unpack[Double](r2("acc"))
    println(s"r2_acc = $r2_acc")

    // false alarm rate
    val r2_far: Either[String, Double] = unpack[Double](r2("far"))
    println(s"r2_far = $r2_far")
  }

  def exp6(): Unit = {

    //assert(false)

    // Open file
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)

    // Gets 2048 lines
    //val offset = 0.000195
    val FFTSz = 256
    val Fs = 5100
    val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val FrmSz2 = 128
    val FrmStp2 = 1

    type FileFilter = (String,String)

    // What is used to tag the class
    def classes(r: Row) : FileFilter = {
      val clas = Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      (clas, bearing)
    }

    // Check if experiments are a problem
    def learnFilter(r: FileFilter) : Boolean = { (r._1 == "norm") && (r._2 == "1") }
    def testFilter(r: FileFilter) : Boolean = { (r._1 != "norm") && (r._2 == "1") }

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = false)

    // Lets read `numberOfSamples` from all the files for training
    //val numberOfSamples = 2048 //Start of file: number of samples to learn

    // We generate training data so that each file uses only the first 'numberOfSamples'
    // The test files will ignore those first 'numberOfSamples' and use the rst of the data
    // for testing (test sample size = 0). Datafiles are shuffled by default
    //val (learn, test) = Frame.splitExtendCSVs(MyFormat, features2, classes, learnFilter, testFilter, numberOfSamples, 0, shuffle = false)

    def splitExtendCSVsX(format: CSVFormat,
                         trainData:Iterator[(File, Row)],
                         testData:Iterator[(File, Row)],
                         nLearnSamples: Int,
                         nIgnoreTestSamples: Int,
                         nTestSamples: Int,
                         shuffle: Boolean,
                         i:Frame ): Either[ADWError,Frame] = {
      val (learn, test) = Frame.splitExtendCSVs(format, trainData, testData,
        nLearnSamples, nIgnoreTestSamples, nTestSamples, shuffle)
      Right(learn ++ test)
    }

    /*
        Notes:
        1. Results with only the training data and default parameters (see article).
           We selected only bearings 1 (rol1) and 2 (rol2) and after processing we get
           a total of k = 9470. We use the last record to get the results. All available
           sample are used.
           The results are:
             read3.size = 9470
             r2_det = Right(1.0)
             r2_isanomalymodemode = Right(0.0)
             r2_acc = Right(0.014994720168954593)
             r2_far = Right(0.9850052798310454)
           We expected (as in the case of a single train + test of one bearing only)
             read3.size = 3000
             r2_det = Right(0.0)
             r2_isanomalymodemode = Right(0.0)
             r2_acc = > Right(0.9)
             r2_far = < Right(0.091)
          With shuffle off:
            Experiments and rpm's still shuffled
             read3.size = 9470
             r2_det = Right(1.0)
             r2_isanomalymodemode = Right(0.0)
             r2_acc = Right(0.024076029567053854)
             r2_far = Right(0.9759239704329461)
           TODO: why such a bad result? Are their significant differences between the bearings, between experiments?
           What about the shuffled order (all get mixed even when shuffling is off)?

        2. Results with only the test data and default parameters (see article).
           We selected only bearings 3 (rol3) and after processing (10 min) we
           get a total of k = 9470
           The results are:
             read3.size = 9470
             r2_det = Right(1.0)
             r2_isanomalymodemode = Right(0.0)
             r2_acc = Right(0.40369588173178456)
             r2_far = Right(0.5013727560718056)
           We expected (as in the case of a single train + test of one bearing only)
             read3.size = 3000
             r2_det = Right(0.0)
             r2_isanomalymodemode = Right(0.0)
             r2_acc = > Right(0.9)
             r2_far = < Right(0.091)
          With shuffle off:
            Experiments and rpm's still shuffled
           TODO: why such a bad result? Variability in the experiments?
           What about the shuffled order (all get mixed even when shuffling is off)?

        3. Train and test data.
           Train has bearings 1 and 2 with no failures. Test only has only bearing 3 but
           with and without failures.
            read3.size = 19070
            r2_det = Right(1.0)
            r2_isanomalymodemode = Right(1.0)
            r2_acc = Right(0.26019926586261144)
            r2_far = Right(0.7398007341373886)


        // TODO: select train + test from single file.
        // Use part for learning and part testing
        // USe only one single file (exp1, rpm1000)

     */

    val loader = T(splitExtendCSVsX _, MyFormat, trainData, testData, 0, 0, 0, false)

    val s3 = loader ->:
      //renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly")  ->: // Rename column
      renameColumns("Time(s)" -> "t","damage" -> "isanomaly")  ->:    // Rename column
      convertLabelValue("isanomaly") ->:                              // convert types (to boolean)
      boolToDouble("isanomaly") ->:                                   // convert types (to double)
      toDouble("t", "X", "Y", "Z" )  ->:                              // convert types
      window(FFTSz,64) ->:                                            // vectors of frames 4x1x16(x,y,z,IsAnomaly)
      modeStep("mode","isanomaly") ->:                                // TODO: comment (isanomalymode)
      lfccStep("lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc") ->:             // 1x15 (lfcc)
      window(FrmSz2,FrmStp2) ->:                                      // vectors of features 64x15(lfcc)
      medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc") ->:               // 1x15 (lfccmedian,damagemedian)
      modeStep("mode","isanomalymode")  ->:                           // (isanomalymode) // TODO: ??, repeated?
      anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian") ->: // TODO: ??
      anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" )    // TODO comment + plot

    // compile new pipes
    val c3 = compile(s3)
    //val c3l = c3.toList

    // Execute the pipes
    val r3 = execDebug(c3)(Frame(("",List())))
    //println(r3)

    // Get the results (not executed yet)
    val result: Frame = r3.head._1.right.get
    //println(result.colTypes)

    // Collect some of the results (executes now)
    //val k = 8510 - 1 // 10 // Max is 3000
    //val read3 = result.project("bearing", "det","isanomalymodemode","acc","far").iterable.toIterator.take(k+1).toIndexedSeq
    val read3 = result.project("bearing", "det","isanomalymodemode","acc","far").iterable.toIterator.toIndexedSeq
    println(s"read3.size = ${read3.size}")
    val k = read3.size - 1

    // check if the bearing selection is correct
    //println(read3.mkString(","))

    // The accuracy varies substantially through the time-series (stream)
    // For this model it is a global evaluation (includes all past accuracy values)
    val r2 = read3(k)

    // prediction
    val r2_det: Either[String, Double] = unpack[Double](r2("det"))
    println(s"r2_det = $r2_det")

    // label
    val r2_isanomalymodemode: Either[String, Double] = unpack[Double](r2("isanomalymodemode"))
    println(s"r2_isanomalymodemode = $r2_isanomalymodemode")

    // accuracy
    val r2_acc: Either[String, Double] = unpack[Double](r2("acc"))
    println(s"r2_acc = $r2_acc")

    // false alarm rate
    val r2_far: Either[String, Double] = unpack[Double](r2("far"))
    println(s"r2_far = $r2_far")
  }

  def exp7():Unit={

    //Experience parametrization
    //==============================================================================================
    val dir: File  = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    //val dir = File("C:/ADW_2018.12.06/data/inegi/ensaios_rolamentos_3")
    //val dir = cwd / "data/inegi/anomaly_detection_data"
    //val dir = cwd / "data/inegi/anomaly_detection_with_noise_levels/DB_JoelAntunes2_0dB"
    //val matchNames = "rol1_rpm1000_hp0_b_mm0_exp1"
    val matchNames = "rol1_rpm1000_hp0_*_mm0_exp1.csv"

    val noiseLevel= 0 //dB
    val FFTSz = 128
    val Fs = 5100
    val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val FrmSz2 = 128 // Median window
    val FrmStp2 = 1


    //==================================================================================================================
    val files = DataFiles.parseFileNames(dir, matchNames, Utils.parseFileName).toList
    val parserOk = files.filter( _.isRight ).map(_.right.get)
    val (filesp,features,units) = parserOk.unzip3

    println("\nFiles-------------------------")
    filesp.indices.foreach( i => filesp(i))

    println("\nFeatures----------------------")
    features.indices.foreach(i => println(features(i)))

    println("\nUnits-------------------------")
    units.indices.foreach(i => println(units(i)))

    val offset = 0.000195
    //val tmp3 = DataFiles.generateTimeStampOffsets(offset, filesp)
    ///println(tmp3.toVector)
    //---------

    // Stream compose
    //------------------------------------------------------
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList    //Parses a list file names looking for features that are separated by an underscore
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)


    // Creates train and test datasets
    //------------------------------------------------------
    type FileFilter = (String,String)
    def classes(r: Row) : FileFilter = {
      val clas = Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      (clas, bearing)
    }

    def learnFilter(r: FileFilter) : Boolean = { (r._1 == "norm") && (r._2 == "1") }
    def testFilter(r: FileFilter) : Boolean = { (r._1 != "norm") && (r._2 == "1") }


    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = false)


    def splitExtendCSVsX(format: CSVFormat,                // Specifications of the csv file
                         trainData:Iterator[(File, Row)],  // Train Dataset
                         testData:Iterator[(File, Row)],   // Test Dataset
                         nLearnSamples: Int,               //
                         nIgnoreTestSamples: Int,
                         nTestSamples: Int,
                         shuffle: Boolean,          // Shuffle  the examples
                         i:Frame ): Either[ADWError,Frame] = {

      val (learn, test) = Frame.splitExtendCSVs(format, trainData, testData,nLearnSamples, nIgnoreTestSamples, nTestSamples, shuffle)
      Right(learn ++ test)
    }

    object MyFormat extends DefaultCSVFormat { override val delimiter = ','} // ';'}
    val loader = T(splitExtendCSVsX _, MyFormat, trainData, testData, 0, 0, 0, false)


    // Pipeline
    //==================================================================================================================
    //val s3 = loadExtraINEGICSVs(MyFormat, filesp.map(_.toString()), tmp3.toList) ->:
    val s3 = loader ->:
          renameColumns("Time(s)" -> "t")  ->:            //Rename column
          toDouble("t", "X", "Y", "Z") ->:
          valMapStep( Map("b" -> 1.0 , "norm" -> 0.0 ), "damage" ) ->:
          //plotStreamStep(20000,"X", "Y", "Z") ->:
          plotAlarmInterfaceStep( 3000, "X","Y","Z","damage") ->:
          window(FFTSz,64) ->:                                                      // return vectors of frames 4x1x16(x,y,z,IsAnomaly)
          addWhiteGaussianNoiseStep("WgnAdd",noiseLevel,1,"X","Y","Z")  ->:     // Add white noise to each frame
          modeStep("Mode","damage")  ->:
          lfccStep("Lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"XWgnAdd","YWgnAdd","ZWgnAdd") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
          //emdFeatStep("emdEn","XWgnAdd","YWgnAdd","ZWgnAdd")  ->:
          concatenateStep("Lfcc","XWgnAddLfcc","YWgnAddLfcc","ZWgnAddLfcc") ->:
          //concatenateStep("EmdEn","XWgnAddEmdEn","YWgnAddEmdEn","ZWgnAddEmdEn") ->:
          window(FrmSz2,FrmStp2) ->:                          //return vectors of features 64x15(lfcc)
          medianStep("Median",NmbCpCoe*3,FrmSz2,"Lfcc") ->:   //1x15 (lfccmedian,damagemedian)
          modeStep("Mode","damageMode") ->:                                     // (isanomalymode)
          anomalyDetectionTrainStep("Det",NmbCpCoe*3,0.9,0.9,200,"LfccMedian") ->:
          anomDetectionEvalStep( "damageModeMode" , "Det" , "far" ) // ->:
          //plotStreamStep(20000,"XWgnAdd","YWgnAdd","ZWgnAdd")
          //plotAlarmInterfaceStep( 3000, "X","Y","Z","damage")

    //compile new pipes
    val c3 = compile(s3)

    // Execute the pipes
    val r3   = execDebug(c3)(Frame(("",List())))
    val res3: Frame = r3.head._1.right.get

    println(res3.colIDs)

    val prm="acc"
    val read1 = res3.project(prm).iterable.toIterator.toIndexedSeq
    println(read1.toArray)
    println(read1.size)
    // println(read1.mkString(","))


    read1.foreach(v => println(v))

    // For scalars
    val var1 = read1.toArray.map( r => unpack[Double](r(prm)).right.get)//.slice(0,1000)
    //println(var1.toVector)

    val fig = new JPlot("");   fig.figure();  fig.plot(var1)




  }

  def exp8():Unit={

    object TestTask {
      type I = Row                                // Input to the experimental pipe       // Output of the experimental pipe
      type O = (Either[ADWError,I], List[Vector[Double]] ,Executing)    // Select the best pipe

      def apply(alpha: Double) = {

        // TODO: how to automate this?
        val tsk = new Aggregator[I,O] {
          //override type Params = String
          override def name: String = "ResultsTable"
          override def params: Params = Map( "alpha" -> alpha)
          override def zero: O = ( Left(ADWError("zero")) , List(),  Executing() )

          // Criteria function
          def criteria(acc:Double, far:Double): Double = (alpha * acc) - (1.0 - alpha)*far


          val FuncNames:List[String] = List(
            "lfccStep.this.lfccStep$",
            "window.this.window$",
            "pt.inescn.search.stream.TasksFCC.addWhiteGaussianNoiseStep$",
            "anomalyDetectionTrainStep.this.anomalyDetectionTrainStep$",
          )


          //
          override def collect(acc: O, in: Either[ADWError,I], ta:Long, trk: Executing): O = {

            (acc, in) match {

              case (  (Right(ra),tbl,exea)  ,  Right(i) ) => //ra:  output-row  exea:

                // Get the accumulated accuracy and false alarm rate
                val accAcc: Either[ADWError, Double] = unpackErr[Double](ra("acc"))
                val accFar = unpackErr[Double](ra("far"))

                // Get a calculated accuracy and false alarm rate from an experiment
                val iAcc = unpackErr[Double](i("acc"))
                val iFar = unpackErr[Double](i("far"))



                val J = trk.history.map(_.name).zip(trk.history).toMap

                var K = J(FuncNames(2))
                var Line:Vector[Double] = Vector( K.params("Seed").toString.toDouble ,
                                                  K.params("SNR").toString.toDouble ,
                                                  iAcc.right.get, iFar.right.get)


                var acctmp = acc._2 :+ Line
                println(acctmp)


                // Calculate the score using a common criteria
                val tmp: Either[ADWError, (Executing,I)] = for {
                  aAcc <- accAcc
                  aFar <- accFar
                  iacc <- iAcc
                  ifar <- iFar
                  aCriteria = criteria(aAcc,aFar)
                  iCriteria = criteria(iacc,ifar)
                } yield {
                  // We want to maximize this criteria
                  val tmp:O=(Right(i),acctmp,trk)
                  if (aCriteria > iCriteria) (exea,ra) else (trk,i)
                }


                // Report an error if it occurred otherwise
                // return information on the experiment
                val rr = tmp match {
                  case Left(e) =>
                    (Left(e),acctmp,trk)
                  case Right((e,o)) =>
                    (Right(o),acctmp,e)
                }
                rr


              case ((Right(_),_,_),_) =>
                // Experiment error, lets ignore it
                acc
              case (_,Right(i)) =>
                // First entry, or an error occurred
                // when calculating the criteria
                val ra=in.right.get
                val accAcc: Either[ADWError, Double] = unpackErr[Double](ra("acc"))
                val accFar = unpackErr[Double](ra("far"))

                println("----")
                println(trk.history.map(_.name))


                val J = trk.history.map(_.name).zip(trk.history).toMap
                val K = J(FuncNames(2))
                var Line:Vector[Double]= Vector(K.params("Seed").toString.toDouble,
                                                K.params("SNR").toString.toDouble,
                                                accAcc.right.get, accFar.right.get)



                var acctmp = acc._2 :+ Line

                (Right(i),acctmp, trk)  //(row,exec)
              case ((Left(_),_,_), Left(_)) =>
                // First entry and an error occurred
                // when calculating the criteria
                //val accAcc: Either[ADWError, Double] = unpackErr[Double](acc("acc"))
                acc
            }
          } // collect
        }
        Agg(tsk)
      }
    }






    //Experience parametrization
    //==============================================================================================
    //val dir: File  = cwd / "data/inegi/ensaios_rolamentos_3"
    //val dir = File("C:/ADW_2018.12.06/data/inegi/ensaios_rolamentos_3")
    val dir: File  = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    //val dir = cwd / "data/inegi/anomaly_detection_data"
    //val dir = cwd / "data/inegi/anomaly_detection_with_noise_levels/DB_JoelAntunes2_0dB"
    //val matchNames = "rol1_rpm1000_hp0_b_mm0_exp1"
    val matchNames = "rol1_rpm1000_hp0_*_mm0_exp1.csv"


    val FFTSz = 128
    val Fs = 5100
    val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val FrmSz2 = 128 // Median window
    val FrmStp2 = 1


    //==================================================================================================================
    val files = DataFiles.parseFileNames(dir, matchNames, Utils.parseFileName).toList
    val parserOk = files.filter( _.isRight ).map(_.right.get)
    var (filesp,features,units) = parserOk.unzip3

    filesp=filesp.reverse
    features=features.reverse

    println("\nFiles-------------------------")
    filesp.indices.foreach( i => filesp(i))

    println("\nFeatures----------------------")
    features.indices.foreach(i => println(features(i)))

    println("\nUnits-------------------------")
    units.indices.foreach(i => println(units(i)))

    val offset = 0.000195
    val tmp3 = DataFiles.generateTimeStampOffsets(offset, filesp)
    println(tmp3.toVector)
    //---------

    // Stream compose
    //------------------------------------------------------
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName).toList    //Parses a list file names looking for features that are separated by an underscore
    val features2 = DataFiles.ignoreParseErrors(features1.toIterator)


    // Creates train and test datasets
    //------------------------------------------------------
    type FileFilter = (String,String)
    def classes(r: Row) : FileFilter = {
      val clas = Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      (clas, bearing)
    }

    def learnFilter(r: FileFilter) : Boolean = { (r._1 == "norm") && (r._2 == "1") }
    def testFilter(r: FileFilter) : Boolean = { (r._1 != "norm") && (r._2 == "1") }


    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = false)


    def splitExtendCSVsX(format: CSVFormat,                // Specifications of the csv file
                         trainData:Iterator[(File, Row)],  // Train Dataset
                         testData:Iterator[(File, Row)],   // Test Dataset
                         nLearnSamples: Int,               //
                         nIgnoreTestSamples: Int,
                         nTestSamples: Int,
                         shuffle: Boolean = true,          // Shuffle  the examples
                         i:Frame ): Either[ADWError,Frame] = {

      val (learn, test) = Frame.splitExtendCSVs(format, trainData, testData,nLearnSamples, nIgnoreTestSamples, nTestSamples, shuffle)
      Right(learn ++ test)
    }

    object MyFormat extends DefaultCSVFormat { override val delimiter = ','} // ';'}



    //addWhiteGaussianNoiseStep
    val AddWGNExtend = Seq(ColumnName("WgnAdd"))
    val noiseLevels = Seq(-12.0,0.0,12.0) //db
    val Seeds = Seq(1)
    val AddWGNCols = Seq(ColumnNames(Seq("X","Y","Z")))
    var AllArg = noiseLevels  ##  Seeds ## AddWGNExtend ## AddWGNCols
    var AllArg2 = flatten(AllArg)
    println(AllArg2.toVector)

    //val NoiseLevelOps = (TaskMacro.nameFunc(addWhiteGaussianNoiseStep.addWhiteGaussianNoiseStep _) , AllArg2 ).T
    val NoiseLevelOps = T(addWhiteGaussianNoiseStep$ _ , AllArg2 )

    // Pipeline
    //==================================================================================================================
    val s3 = loadExtraINEGICSVs(MyFormat, filesp.map(_.toString()), tmp3.toList) *:
    //val s3 = loader *:
      renameColumns("Time(s)" -> "t")  *:            //Rename column
      toDouble("t", "X", "Y", "Z") *:
      valMapStep( Map("b" -> 1.0 , "norm" -> 0.0 ), "damage" ) *:
      //plotStreamStep(20000,"X", "Y", "Z") ->:
      window(FFTSz,64) *: // return vectors of frames 4x1x16(x,y,z,IsAnomaly)
      NoiseLevelOps *:
      //addWhiteGaussianNoiseStep("WgnAdd",12,3,"X","Y","Z") *:      // Add white noise to each frame
      modeStep("Mode","damage")  *:
      lfccStep("Lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"XWgnAdd","YWgnAdd","ZWgnAdd") *:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      //lfccStep("Lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      //emdFeatStep("emdEn","XWgnAdd","YWgnAdd","ZWgnAdd")  ->:
      concatenateStep("Lfcc","XWgnAddLfcc","YWgnAddLfcc","ZWgnAddLfcc") *:
      //concatenateStep("EmdEn","XWgnAddEmdEn","YWgnAddEmdEn","ZWgnAddEmdEn") ->:
      window(FrmSz2,FrmStp2) *:                          //return vectors of features 64x15(lfcc)
      medianStep("Median",NmbCpCoe*3,FrmSz2,"Lfcc") *:   //1x15 (lfccmedian,damagemedian)
      modeStep("Mode","damageMode") *:                                     // (isanomalymode)
      anomalyDetectionTrainStep("Det",NmbCpCoe*3,0.9,0.9,200,"LfccMedian") *:
      anomDetectionEvalStep( "damageModeMode" , "Det" , "far","acc" )*:
      getLastRow("Det", "damageModeMode", "acc", "far")
    //TestTask(0.5)
       //->:
      //plotStreamStep( 3000, "damageModeMode", "Det","acc","far") //->:


    println("s3--------------------")
    // println(s3); // Função completa  da pipe
    println("----------------------")

    val empty = Frame(("",List()))   //Creates an empty frame
    val acc: Executing = Executing() //Create an empty task


    // Compile and Execute the pipes
    val c3 = compile(s3);
    val c3List = c3.toList; //c3List.map(println(_)) ; println()  // println(c3List)

    /*val r3List = r3.toList
    println(c3.toArray.size)
    println(r3List.size)

    val res3:Frame = r3List(0)._1.right.get

    println(res3.colIDs)
    val prm="acc"
    val read1 = res3.project(prm).iterable.toIterator.toArray
    read1.map(v => println(v))

    //var var1 = read1.map( r => unpack[Double](r(prm)).right.get)//.slice(0,1000)//println(var1.toVector)
    //val fig = new JPlot("");   fig.figure();  fig.plot(var1)*/

    val resultTask = TestTask(0.5)
    /*println("resultTask.t--------------------")
    println(resultTask.t)
    println(resultTask.v)
    println("----------------------")*/

    // c3List - itera pipes
    val agg = Pipes.partialsPar(c3List.iterator, resultTask.t, Par.cores, Par.waitFor, Par.verbose)(Par.scheduler)
    val all = agg(acc, empty)


    println(all._1)

    val result_all = all._1.right.get
    val all_best = result_all._1.right.get
    val all_exec = result_all._2

    val r2all_det = unpack[Double](all_best("Det"))
    val r2all_isanomalymodemode = unpack[Double](all_best("damageModeMode"))
    val r2all_acc = unpack[Double](all_best("acc"))  // accuracy
    val r2all_far = unpack[Double](all_best("far"))  // false alarm rate
    val Table=result_all._2

    Table.map(println(_))
    println(r2all_far)
    println(r2all_acc)

    val header = Array("Seed","SNR","ACC","FAR")
    val entra: Array[String] = (1 to Table.length).toArray.map(_.toString)

    println(entra)

    val caption = "Caption"


    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    //val err = File.newTemporaryFile(prefix = errName, parent = Some(tmp))
    //val out = File(outName)

    val dataFile: File = tmp / "test.png"


    val TableValues = Table.toArray.map(_.toArray)

    val txt=Table2Latex(header,entra,TableValues, caption)

    println(txt)
    /*val pw = new PrintWriter(new File("C:\\Users\\RSousa1\\Desktop\\testedoscala.tex" ))
    pw.write(txt)
    pw.close()*/

    var Nms = Array( "Alg1","Alg2","Alg3","Alg4","Alg5")
    var NP= NemenyiPlot



    //Lines  - methods
    //Columns- experimentations
    var CD = NP.computeCD(TableValues.length,TableValues(0).length,0.05 )
    println(CD)
    var order= NP.computeOrder(TableValues)
    var fig= NP.Plot(order,entra,CD)
    fig.saveas(dataFile.path.toString, 1000, 500)

    println(dataFile.path.toString)









  }






  val experiments: Map[String, () => Unit] = Map(
    "1" -> exp1 _,
    "2" -> exp2 _,
    "3" -> exp3 _,
    "4" -> exp4 _,
    "5" -> exp5 _,
    "6" -> exp6 _,
    "7" -> exp7 _,
    "8" -> exp8 _,
  )

  def main(args: Array[String]): Unit ={
    for (id <- args){
      if (experiments.contains(id)) {
        println(s"Executing experiment $id.")
        experiments(id)()
      }
      else
        println(s"Experiment arg $id not found.")
    }
  }

}
