package inegi

import pt.inescn.detector.stream.CantelliFCCAnomalyDetector
import utils._
import pt.inescn.features.LFCC
import pt.inescn.plot.JPlot

/**
  * Scratch pad for testing the FCC code.
  * Uses the Cantelli algorithm to determine if a fault occurred.
  *
  * root/runMain pt.inescn.detector.FCCAnomalyDetectorTest
  */
object FCCAnomalyDetectorTest{

  //=======================================================================================
  def main(args: Array[String]): Unit = {

    // variaveis constantes
    val Path= "../data/inegi/anomaly_detection_data/rol1_rpm1000_hp0_b_mm0_exp1.csv"
    val FrmSz=256
    val FrmStp=128
    val NmbCpCoe=5
    val BffSz=64
    //val AnomThres=0.9

    // Reads the data set
    //---------------------
    val rdf = new Read_Data_File(Path, 1, 1)
    val Sgnl= new Array[Double](rdf.Table.size())
    for ( i <- 0 until rdf.Table.size() )
      Sgnl(i)=rdf.Table.get(i)(3)

    //Windowing
    val frm= new Frmng().FrmngFunc(Sgnl,FrmSz,FrmStp, Array.fill[Double](FrmSz)(1) )

    //LFCC initialization
    val LFCCclss = LFCC(5100,5,2550,10,5,FrmSz)
    val AnomalyDetectorClss = CantelliFCCAnomalyDetector(5,0.9,0.9)
    var Buffer = List[Array[Double]]()

    val aBuffer = new Array[Double](frm.length)
    val iBuffer = new Array[Double](frm.length)

    val fig = new JPlot("h")
    fig.grid("on", "on") // grid on;


    for( i<-0 until frm.length) {

      println(s"\nFrame $i---------------------------")
      //LFCC computation
      val lfcc = LFCCclss.LFCCcomp(frm(i))

      if (Buffer.size < BffSz)
        Buffer ++= List(lfcc)
      else{
        Buffer.drop(0)
        Buffer ++= List(lfcc)

        //Smoothing
        val smthLfcc= new Array[Double](NmbCpCoe)
        val tmp = new Array[Double](BffSz)
        for (k <- 0 until NmbCpCoe){
          for(j <- 0 until BffSz)
            tmp(j) = Buffer(j)(k)
          smthLfcc(k)=AnomalyDetectorClss.median(tmp)
        }
        print(s"LFCC ")
        println(smthLfcc.mkString(";"))

        // Anomaly detection(Prediction)
        val FD = AnomalyDetectorClss.predict(smthLfcc)
        AnomalyDetectorClss.train(smthLfcc)

        aBuffer(i)= FD.toDouble;iBuffer(i)= i.toDouble

        println(s"Prediction $FD")
      }
    }
    fig.plot(iBuffer,aBuffer,"-r",1.0f,"AAPL")
    println(aBuffer.mkString(";"))

  }
}




