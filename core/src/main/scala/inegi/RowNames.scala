package inegi

object RowNames {
  val DET        = "Det"           // Detected outlier 1 or 0
  val TIMESTAMP  = "timeStamps"    // Time of event detection
  val MACHINE_ID = "machineID"     // ID of machine
  val SENSOR_ID  = "sensorID"      // ID of sensor
  val X          = "X"             // Accelerometer X
  val Y          = "Y"             // Accelerometer Y
  val Z          = "Z"             // Accelerometer Z
}
