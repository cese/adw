package inegi.utils

import java.io.{File, PrintWriter}

object  Table {

  def LatexHeader():String= {
    var TextStr = "\\documentclass[10 pt, a4paper]{article}\n" +
    "\\usepackage{booktabs}\n" +
      "\\usepackage{algorithm}\n" +
      "\\usepackage{array}\n" +
      "\\newcolumntype{L}[1]{>{\\raggedright\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}p{#1}}\n" +
      "\\newcolumntype{C}[1]{>{\\centering\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}p{#1}}\n" +
      "\\newcolumntype{R}[1]{>{\\raggedleft\\let\\newline\\\\\\arraybackslash\\hspace{0pt}}p{#1}}\n" +
      "\\begin{document}\n"

    TextStr
  }

  def Table2Latex(header: Array[String], entra: Array[String], body: Array[Array[Double]], caption: String): String={

    var TextStr = LatexHeader() + "\\begin{table}[H]\n" +
      "\\centering\n" +
      "\\caption{" + caption + "}\n" +
      "\\label{tab:" + caption + "}\n" +
      "\\begin{tabular}{ L{3cm} "

    var j=0
    while( j< body(0).length){
      TextStr += "C{2cm} "
      j+=1
    }

    TextStr += "}\n" + "\\hline\n"

    //Create the header of the table
    //------------------------------
    var line = ""
    var h = 0
    while ( h < header.length-1) {
      line += header(h) + " & "
      h+=1
    }
    line+=header(header.length-1) + " \\\\ \\hline\n"
    TextStr += line

    h=0
    while ( h < body.length) {
      line=""
      line+= entra(h).toString + " & "

      var i=0
      while ( i < body.length-1) {
        line += body(h)(i).toString() + " & "
        i+=1
      }
      line += body(h)( body(h).length-1)
      line += "\\\\ [0.7ex]\n"

      TextStr += line
      h+=1
    }

    TextStr += 	" \\specialrule{.1em}{.1em}{.1em} %  \\hline  & \n" +
      "\\end{tabular}\n" +
      "\\end{table}\n" +
      "\\end{document}\n"

    TextStr

  }


  def main(args: Array[String]): Unit = {


    var header = new Array[String](3)
    header(0) = "Entries Colunm"
    header(1) = "Column 1"
    header(2) = "Column 2"

    var entra = new  Array[String](2)
    entra(0)="linha 1"
    entra(1)="linha 2"

    var body = Array.ofDim[Double](2,2)
    body(0)(0) = 1
    body(1)(0) = 2
    body(0)(1) = 3
    body(1)(1) = 4


    var caption=" This table is for.."

    val txt=Table2Latex(header,entra, body, caption)

    println(txt)

    val pw = new PrintWriter(new File("C:\\Users\\RSousa1\\Desktop\\testedoscala.tex" ))
    pw.write(txt)
    pw.close()

  }

}

