%sudo apt-get install liboctave-dev
%pkg install -forge control
%pkg install -forge signal
%pkg load signal

%Start from nothing!
clear;

% Set the sampling frequency used by our digital filtering system:
fs=8000;
fs2 = fs / 2.0

% Start designing a butterworth filter, passband. (In Hz)
pass_lo = 300;
pass_hi = 600;

% The order of the filter
order = 4;

% Determine the low and high frequencies of the passband as fractions of the
% sampling rate:
flo = pass_lo/fs2;
fhi = pass_hi/fs2;

% Use the butterworth filter design function to get the coefficients for a
% bandpass filter with the settings above:
[b,a] = butter(order, [flo fhi]);

% Determine the frequency response of the filter design above. Get the output
% in frequency rather than rad/s. Use 512 plot points.
[H,f] = freqz(b, a, 512, fs2);

% Plot the result so that we can see if it is correct:
figure(1);
plot(f, 20*log10(abs(H)));
xlabel('Frequency (Hz)');
ylabel('Magnitude (dB)');

data = sinetone(450,fs,1,1);
filtered = filter(b,a,data);
%filtered

clf
subplot ( 2, 1, 1)
plot(data,";450Hz sine;")
subplot ( 2, 1, 2)
plot(filtered,";450Hz response;")

clear;

sf = 800; sf2 = sf/2;
data=[[1;zeros(sf-1,1)],sinetone(25,sf,1,1),sinetone(50,sf,1,1),sinetone(100,sf,1,1)];
[b,a]=butter ( 1, 50 / sf2 );
filtered = filter(b,a,data);

clf
subplot ( columns ( filtered ), 1, 1)
plot(filtered(:,1),";Impulse response;")
subplot ( columns ( filtered ), 1, 2 )
plot(filtered(:,2),";25Hz response;")
subplot ( columns ( filtered ), 1, 3 )
plot(filtered(:,3),";50Hz response;")
subplot ( columns ( filtered ), 1, 4 )
plot(filtered(:,4),";100Hz response;")
 