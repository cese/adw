%     						INPUT 
%-----------------------------------------------------------
% Path  -
% PttrnPrsnt -  List of patterns present on the filename 
% PttrnNtPrsnt
% Mem   -


%                         OUTPUT 
%-----------------------------------------------------------
% DB.s       -
% DB.Fs      -
% DB.NSmpls  -


function [DB]=Read_DB(Path,PttrnPrsnt,PttrnNtPrsnt,Mem)
DB=[];
disp(Path);

files=DirSrch2(Path);
%files=dir(Path);
%if isempty(files)
%    files=dir([Path '/*.pcm'] );
%end

if(isempty(files))
    disp('Read_DB: Empty Directory or inexistent');
    DB=-1;
    return;
end

if(length(PttrnNtPrsnt)==0)
	PttrnNtPrsnt={''};
end
	
if(length(PttrnPrsnt)==0)
	PttrnPrsnt={''};
end

disp(' LIST OF THE FILES');
disp('----------------------------');
n=1;
for i=1:length(files)
	
    % Check file name condictions
    flag=1;
    for a=1:length(PttrnPrsnt)
		if( ~isempty( regexp(files(i).name, PttrnPrsnt{a} ) ) )
			for b=1:length(PttrnNtPrsnt)
				if( ~isempty( regexp(files(i).name, PttrnNtPrsnt{b}) ) )
					flag=0;break;
				end
            end
        else
            flag=0;break;
        end
    end
    
    if(flag)
		if Mem
			[s,Fs]=wavread( [ Path '/' files(i).name ] );
			DB(n).s=s;
			DB(n).Fs=Fs;
			DB(n).NSmpls=length(DB(n).s);
            DB(n).name=[files(i).name];
            DB(n).Path= files(i).Path;
            %DB(n).Path=[ Path '/' files(i).name ];
            
            disp(['   >' DB(n).name ' '  num2str(DB(n).NSmpls) ' '  num2str(DB(n).Fs)  ]);
        else
            idx=strfind(files(i).name,'.');
            if ~isempty(idx)
                idx=idx(length(idx));
                DB(n).name=files(i).name(1:idx-1);
            else
                DB(n).name=files(i).name;
            end
            
            DB(n).Path= files(i).Path;
            %DB(n).Path=[ Path '/' files(i).name ];
            disp(['   >' DB(n).name  ]);
        end
        
        n=n+1;
    end
    
end



if(length(DB)==0)
    disp('>>>Files not found');
end

disp('----------------------------')

end

function OUT=DirSrch2(Path)

    OUT(1).Path=[];files=dir(Path);k=1;
    for i=3:length(files)
        if( files(i).isdir )
             disp([' ' Path '/' files(i).name ])
             files2=dir([ Path '/' files(i).name ]);
             for j=3:length(files2)
                if(files2(j).isdir)
                    disp([' >' Path '/' files(i).name '/' files2(j).name ])
                    files3=dir([ Path '/' files(i).name '/' files2(j).name ]);
                    for m=3:length(files3)
                        if(files3(m).isdir)
                            disp([ ' >>' Path '/' files(i).name '/' files2(j).name '/' files3(m).name])
                            files4=dir([ Path '/' files(i).name '/' files2(j).name '/' files3(m).name]);
                            for n=3:length(files4)
                                if(files4(n).isdir)
                                    disp([ ' >>' Path '/' files(i).name '/' files2(j).name '/' files3(m).name])
                                    files4=dir([ Path '/' files(i).name '/' files2(j).name '/' files3(m).name]);
                                else
                                    OUT(k).Path = [ Path '/' files(i).name '/' files2(j).name '/' files3(m).name '/' files4(n).name ]
                                    OUT(k).name = files4(n).name ;k=k+1;
                                end
                            end
                        else
                            OUT(k).Path = [ Path '/' files(i).name '/' files2(j).name '/' files3(m).name ]
                            OUT(k).name = files3(m).name ;k=k+1; 
                        end
                    end 
                else
                    OUT(k).Path = [Path '/' files(i).name '/' files2(j).name ];
                    OUT(k).name = files2(j).name ;k=k+1; 
                end 
             end
        else
            OUT(k).Path = [Path '/' files(i).name];
            OUT(k).name = files(i).name;k=k+1;
        end
    end
    
%     for j=1:length(out)
%         h=out(j).name
%     end
end



%             files2=dir(Path);
%             for j=1:length(file2)
%                 if( isdir(files2(j).name) )
%                     files3=dir(Path);
%                 else
%                     
%                 end
%             end




% S=[];v=1;
% for a=1:length(RES)
%     disp(RES{a});
%     for n=1:length(SEQ)
%         if( ~isempty( regexp(SEQ(n).name, RES{a} ) ) )
%             disp(['   >' SEQ(n).name ])
%             s1=wavread(SEQ(n).Path);
%             S{v}=s1;
%             v=v+1;
%         end
%     end
% end




