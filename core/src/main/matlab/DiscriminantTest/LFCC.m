%					INTPUT
%----------------------------------------------------
%  LFCC- Linear Frequency Cepstral Coeficients
%PRM.NmbFltrs -
%PRM.Fs       -
%PRM.MaxFrq   -
%PRM.MinFrq   -
%PRM.NmbCpCoe -

%					OUTPUT
%----------------------------------------------------

function CC=LFCC(PRM)

Fs=PRM.Fs;
S=PRM.S;
NmbFltrs =PRM.NmbFltrs;	                        % numero filtros do banco-de-filtros
FFTSz=size(S,1);
MaxFrq= PRM.MaxFrq/FFTSz
MinFrq= PRM.MinFrq/FFTSz
NmbCpCoe=PRM.NmbCpCoe;
PrEmphssFctr= -0.99;

dctMatrix = cos(((1:NmbFltrs)'-.5)*(1:NmbCpCoe)*pi/NmbFltrs);

if size(size(S)) < 3
    St=S;
    S = filter([1 PrEmphssFctr], 1, S); % Pre-Emphasis
    
%     figure(1)
%     fftS=20*log(abs(fft(S(:,1))));
%     fftSt=20*log(abs(fft(St(:,1))));
%     plot(fftS);hold on;
%     plot(fftSt,'r');
    
    Fltrs=FltrBnk(PRM);        % Generates de filter bank
    Mag=abs(fft(S)).^2;        % DFT Power Spectrum
    E=log(Fltrs*Mag(1:end/2,:)); % log and filtering and energy
    CC=dctMatrix'*E;
else
    for i=1:size(S,1)
        Si=squeeze(S(i,:,:));Sti=Si;
        Si = filter([1 PrEmphssFctr], 1, Si); % Pre-Emphasis
        % figure(1)
        % fftS=20*log(abs(fft(S(:,1))));
        % fftSt=20*log(abs(fft(St(:,1))));
        % plot(fftS);hold on;
        % plot(fftSt,'r');
        % pause;
        PRM.S=Si;
        Fltrs=FltrBnk(PRM);         % Generates de filter bank
        Mag=abs(fft(Si)).^2;         % DFT
        E=log( max( Fltrs*Mag(1:end/2,:),1e-10) ); % log and filtering and energy
        CC(i,:,:)=dctMatrix'*E; 
    end
end

end


function Fltrs=FltrBnk(PRM)

Fs=PRM.Fs;
NmbFltrs=PRM.NmbFltrs;	                        % numero filtros do banco-de-filtros
FFTSz=size(PRM.S,1);
MaxFrq=PRM.MaxFrq*FFTSz/Fs;
MinFrq=PRM.MinFrq*FFTSz/Fs;


Lnght=2*(MaxFrq-MinFrq)/(NmbFltrs+1);
LowFrq=MinFrq:(Lnght/2):MaxFrq-Lnght;
HigFrq=LowFrq+Lnght;

for i=1:NmbFltrs
    Fltrs(i,:)=TrnglrMsk(LowFrq(i),HigFrq(i),Lnght,FFTSz/2);
end
if(0)
figure(567)
plot(sum(Fltrs));hold on;
plot(Fltrs');hold off;
end

end


function out=TrnglrMsk(lk,hk,ln,N)

for k=1:N
    
   if(k<=lk)
       out(k)=0;
   end
   if( k>lk && k<=lk+ln/2)
       out(k)=2/ln*(k-lk);
   end
   if(k>lk+ln/2 && k<=hk)
       out(k)=-2/ln*(k-hk);
   end
   if( k>hk )
       out(k)=0;
   end
    
end

end

function out=MelFreSc(lk,hk,ln,N)
lowestFrequency = 5000;
linearFilters = NmbFltrs;
linearSpacing = 1000;
freqs = lowestFrequency + (0:linearFilters+2)*linearSpacing;
fftFreqs = (0:fftSize-1)/fftSize*PRM.Fs;

lr = freqs(1:NmbFltrs);
cr = freqs(2:NmbFltrs+1);
ur = freqs(3:NmbFltrs+2);
th = 2./(ur-lr);                                           	% "alturas" filtros (triangulares) passa-banda

filterWeights = zeros(NmbFltrs,fftSize);
for chan=1:NmbFltrs
    filterWeights(chan,:) = ...
    (fftFreqs>lr(chan) & fftFreqs<=cr(chan)).*...
    th(chan).*(fftFreqs-lr(chan))/(cr(chan)-lr(chan))+...
    (fftFreqs>cr(chan) & fftFreqs<ur(chan)).*...
    th(chan).*(ur(chan)-fftFreqs)/(ur(chan)-cr(chan));
end

end