function CF=CrestFactor(S)

if size(size(S))<3
    CF=0.5*(max(S)-min(S))./std(S);
else
	for i=1:size(S,1)
        Si=squeeze(S(i,:,:));
        CF(i,:)=0.5*(max(Si)-min(Si))./std(Si);
    end

end