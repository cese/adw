clear all;close all;clc;warning off;
rmpath('E:\1_WORKSPACE\3_PUBLICACOES\WORKSPACE_VOICE\3_ALGORITHMS');
addpath(genpath('E:\1_WORKSPACE\3_PUBLICACOES\WORKSPACE_VOICE\3_ALGORITHMS'));

Exp=1;

switch Exp
    case 0
%==========================================================================
DPath='E:\1_WORKSPACE\2_MAESTRA\5_ASSUNTOS\CESE\1_DATASETS\DB_SmDnCmDn_Ftrs';  
DB=Read_DB( DPath , {'arff'} , {'3dB','-3dB','-9dB'} , 0 ); %pause;
K=10;NmbrRuns=10; Map=Map3;
T_DR_lda={};T_DR_qda={};T_DR_logda={};
T_FP_lda={};T_FP_qda={};T_FP_logda={};
T_Ac_lda={};T_Ac_qda={};T_Ac_logda={};
Line_drlda={};Line_drqda={};Line_drlogda={};
Line_fplda={};Line_fpqda={};Line_fplogda={};
Line_aclda={};Line_acqda={};Line_aclogda={};
for m=1:length(DB)

    DB(m).Path
    OUT=Read_Data_File(DB(m).Path,[],[]); DtSt=OUT.Dt;  
    %wiggle(DtSt);pause;

    X = DtSt(:,2:end); Y = DtSt(:,1)+1; DB(m).name
    if(~isempty(strfind(DB(m).name,'LFCC5'))) X(:,[5 10 15])=[];end
    if(~isempty(strfind(DB(m).name,'LFCC13'))) X(:,[13 26 39])=[];end
    
    
    %Create the K folders
    for r=1:NmbrRuns
        s = RandStream('mt19937ar','Seed',r);RandStream.setGlobalStream(s);
        new_i=randperm(size(X,1)); X=X(new_i,:); Y=Y(new_i,:); %Examples shuffle
        S=floor(size(X,1)/K);
        for k=1:K
            Xf(k)={X(((k-1)*S)+1:(k*S),:)};  Yf(k)={Y(((k-1)*S)+1:(k*S),:)}; %Cross Validation grouping
        end

        for i=1:K
            for j=1:K 
                f_lda=lda(Xf{j},Yf{j}); f_qda=qda(Xf{j},Yf{j}); f_logda=logda(Xf{j},Yf{j});
                Yk=Yf{i}-1;

                Yhat=classify(f_lda,Xf{i})-1;
                DetRate_lda(r,j) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
                FPRate_lda(r,j) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
                Acc_lda(r,j)= length(find(Yk==Yhat))/length(Yk)*100;

                Yhat=classify(f_qda,Xf{i})-1;
                DetRate_qda(r,j) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
                FPRate_qda(r,j) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
                Acc_qda(r,j)= length(find(Yk==Yhat))/length(Yk)*100;

                Yhat=classify(f_logda,Xf{i})-1;
                DetRate_logda(r,j) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
                FPRate_logda(r,j) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
                Acc_logda(r,j)= length(find(Yk==Yhat))/length(Yk)*100;
            end
        end
    end
    
    
    if( ~isempty(strfind(DB(m).name,'-12dB')) )
        mtdNm=RnmFiles(DB(m).name,Map);
        T_DR_lda=[ T_DR_lda ; Line_drlda ]; Line_drlda={ mtdNm mean(mean(DetRate_lda))};
        T_DR_qda=[ T_DR_qda ; Line_drqda ]; Line_drqda={ mtdNm mean(mean(DetRate_qda))};
        T_DR_logda=[ T_DR_logda ; Line_drlogda ]; Line_drlogda={mtdNm mean(mean(DetRate_logda))};
        
        T_FP_lda=[ T_FP_lda ; Line_fplda ]; Line_fplda={mtdNm mean(mean(FPRate_lda))};
        T_FP_qda=[ T_FP_qda ; Line_fpqda ]; Line_fpqda={mtdNm mean(mean(FPRate_qda))};
        T_FP_logda=[ T_FP_logda ; Line_fplogda ]; Line_fplogda={mtdNm mean(mean(FPRate_logda))};
        
        T_Ac_lda=[ T_Ac_lda ; Line_aclda ]; Line_aclda={mtdNm mean(mean(Acc_lda))};
        T_Ac_qda=[ T_Ac_qda ; Line_acqda ]; Line_acqda={mtdNm mean(mean(Acc_qda))};
        T_Ac_logda=[ T_Ac_logda ; Line_aclogda ]; Line_aclogda={mtdNm mean(mean(Acc_logda))};
    else
        Line_drlda=[ Line_drlda  { mean(mean(DetRate_lda))} ];
        Line_drqda=[ Line_drqda  { mean(mean(DetRate_qda))} ];
        Line_drlogda=[ Line_drlogda  {mean(mean(DetRate_logda))} ];
        
        Line_fplda=[ Line_fplda  { mean(mean(FPRate_lda))} ];
        Line_fpqda=[ Line_fpqda  { mean(mean(FPRate_qda))} ];
        Line_fplogda=[ Line_fplogda  {mean(mean(FPRate_logda))} ];
        
        Line_aclda=[ Line_aclda  { mean(mean(Acc_lda))} ];
        Line_acqda=[ Line_acqda  { mean(mean(Acc_qda))} ];
        Line_aclogda=[ Line_aclogda  {mean(mean(Acc_logda))} ];
        
    end
    
    if m==length(DB)
        T_DR_lda=[ T_DR_lda ; Line_drlda ];
        T_DR_qda=[ T_DR_qda ; Line_drqda ];
        T_DR_logda=[ T_DR_logda ; Line_drlogda ]; 
        
        T_FP_lda=[ T_FP_lda ; Line_fplda ];
        T_FP_qda=[ T_FP_qda ; Line_fpqda ];
        T_FP_logda=[ T_FP_logda ; Line_fplogda ];
        
        T_Ac_lda=[ T_Ac_lda ; Line_aclda ];
        T_Ac_qda=[ T_Ac_qda ; Line_acqda ];
        T_Ac_logda=[ T_Ac_logda ; Line_aclogda ];
    end
        
    %pause;                  
end

IN.Hdr={'Method' 'wn' '0' '-6' '-12'};swidx=[ 1 5 4 3 2];
IN.Dt=T_DR_lda(:,swidx);Write_Data_File([ DPath '\results_DR_lda.xls'],IN);
IN.Dt=T_DR_qda(:,swidx);Write_Data_File([ DPath '\results_DR_qda.xls'],IN);
IN.Dt=T_DR_logda(:,swidx);Write_Data_File([ DPath '\results_DR_logda.xls'],IN);
IN.Dt=T_FP_lda(:,swidx);Write_Data_File([ DPath '\results_FP_lda.xls'],IN);
IN.Dt=T_FP_qda(:,swidx);Write_Data_File([ DPath '\results_FP_qda.xls'],IN);
IN.Dt=T_FP_logda(:,swidx);Write_Data_File([ DPath '\results_FP_logda.xls'],IN);
IN.Dt=T_Ac_lda(:,swidx);Write_Data_File([ DPath '\results_Ac_lda.xls'],IN);
IN.Dt=T_Ac_qda(:,swidx);Write_Data_File([ DPath '\results_Ac_qda.xls'],IN);
IN.Dt=T_Ac_logda(:,swidx);Write_Data_File([ DPath '\results_Ac_logda.xls'],IN);

    case 1
%==========================================================================        
DPath='E:\1_WORKSPACE\2_MAESTRA\5_ASSUNTOS\CESE\1_DATASETS\DB_Results';  
DB=Read_DB( DPath , {'arff'} , {'3dB','-3dB','-9dB'} , 0 ); pause;

K=10;
T_DR_lda={};T_DR_qda={};T_DR_logda={};
T_FP_lda={};T_FP_qda={};T_FP_logda={};
T_Ac_lda={};T_Ac_qda={};T_Ac_logda={};
Line_drlda={};Line_drqda={};Line_drlogda={};
Line_fplda={};Line_fpqda={};Line_fplogda={};
Line_aclda={};Line_acqda={};Line_aclogda={};
for m=1:length(DB)
    DB(m).Path
    OUT=Read_Data_File(DB(m).Path,[],[]); DtSt=OUT.Dt;  
    %wiggle(DtSt);pause;

    X = DtSt(:,2:end); Y = DtSt(:,1)+1; DB(m).name
    if(~isempty(strfind(DB(m).name,'LFCC5'))) X(:,[5 10 15])=[];end
    if(~isempty(strfind(DB(m).name,'LFCC13'))) X(:,[13 26 39])=[];end
    
    %Create the K folders
    s = RandStream('mt19937ar','Seed',1);RandStream.setGlobalStream(s);
    new_i=randperm(size(X,1)); X=X(new_i,:); Y=Y(new_i,:); %Examples shuffle
    
    S=floor(size(X,1)/K);
    for k=1:K
        Xf(k)={X(((k-1)*S)+1:(k*S),:)};  Yf(k)={Y(((k-1)*S)+1:(k*S),:)}; %Cross Validation grouping
    end
    
    for i=1:K
        for j=1:K 
            f_lda=lda(Xf{j},Yf{j}); f_qda=qda(Xf{j},Yf{j}); f_logda=logda(Xf{j},Yf{j});
            Yk=Yf{i}-1;

            Yhat=classify(f_lda,Xf{i})-1;
            DetRate_lda(j) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
            FPRate_lda(j) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
            Acc_lda(j)= length(find(Yk==Yhat))/length(Yk)*100;

            Yhat=classify(f_qda,Xf{i})-1;
            DetRate_qda(j) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
            FPRate_qda(j) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
            Acc_qda(j)= length(find(Yk==Yhat))/length(Yk)*100;

            Yhat=classify(f_logda,Xf{i})-1;
            DetRate_logda(j) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
            FPRate_logda(j) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
            Acc_logda(j)= length(find(Yk==Yhat))/length(Yk)*100;
        end
    end
    
    mtdNm=DB(m).name;
    T_DR_lda=[ T_DR_lda ; Line_drlda ]; Line_drlda={ mtdNm mean(DetRate_lda)};
    T_DR_qda=[ T_DR_qda ; Line_drqda ]; Line_drqda={ mtdNm mean(DetRate_qda)};
    T_DR_logda=[ T_DR_logda ; Line_drlogda ]; Line_drlogda={mtdNm mean(DetRate_logda)};
        
    T_FP_lda=[ T_FP_lda ; Line_fplda ]; Line_fplda={mtdNm mean(FPRate_lda)};
    T_FP_qda=[ T_FP_qda ; Line_fpqda ]; Line_fpqda={mtdNm mean(FPRate_qda)};
    T_FP_logda=[ T_FP_logda ; Line_fplogda ]; Line_fplogda={mtdNm mean(FPRate_logda)};
        
    T_Ac_lda=[ T_Ac_lda ; Line_aclda ]; Line_aclda={mtdNm mean(Acc_lda)};
    T_Ac_qda=[ T_Ac_qda ; Line_acqda ]; Line_acqda={mtdNm mean(Acc_qda)};
    T_Ac_logda=[ T_Ac_logda ; Line_aclogda ]; Line_aclogda={mtdNm mean(Acc_logda)};
    
    if m==length(DB)
        T_DR_lda=[ T_DR_lda ; Line_drlda ];
        T_DR_qda=[ T_DR_qda ; Line_drqda ];
        T_DR_logda=[ T_DR_logda ; Line_drlogda ]; 
        
        T_FP_lda=[ T_FP_lda ; Line_fplda ];
        T_FP_qda=[ T_FP_qda ; Line_fpqda ];
        T_FP_logda=[ T_FP_logda ; Line_fplogda ];
        
        T_Ac_lda=[ T_Ac_lda ; Line_aclda ];
        T_Ac_qda=[ T_Ac_qda ; Line_acqda ];
        T_Ac_logda=[ T_Ac_logda ; Line_aclogda ];
    end
    
end


IN.Hdr={'Experiment' 'Value' };
IN.Dt=T_DR_lda;Write_Data_File([ DPath '\results_DR_lda.xls'],IN);
IN.Dt=T_DR_qda;Write_Data_File([ DPath '\results_DR_qda.xls'],IN);
IN.Dt=T_DR_logda;Write_Data_File([ DPath '\results_DR_logda.xls'],IN);
IN.Dt=T_FP_lda;Write_Data_File([ DPath '\results_FP_lda.xls'],IN);
IN.Dt=T_FP_qda;Write_Data_File([ DPath '\results_FP_qda.xls'],IN);
IN.Dt=T_FP_logda;Write_Data_File([ DPath '\results_FP_logda.xls'],IN);
IN.Dt=T_Ac_lda;Write_Data_File([ DPath '\results_Ac_lda.xls'],IN);
IN.Dt=T_Ac_qda;Write_Data_File([ DPath '\results_Ac_qda.xls'],IN);
IN.Dt=T_Ac_logda;Write_Data_File([ DPath '\results_Ac_logda.xls'],IN);
              
end








