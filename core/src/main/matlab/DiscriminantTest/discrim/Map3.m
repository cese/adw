function Map=Map3

% Mapa para renomear
%------------------------
n=1;l=1;   % Bearing index
Map(n).M{l,1}='EMDeng_';Map(n).M{l,2}='EMD(Eng)';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='EMDEnt_';Map(n).M{l,2}='EMD(Ent)';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='EMDSV_';Map(n).M{l,2}='EMD(SV)';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='EMDengw';Map(n).M{l,2}='EMDw(Eng)';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='EMDentw';Map(n).M{l,2}='EMDw(Ent)';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='EMDSVw';Map(n).M{l,2}='EMDw(SV)';Map(n).M{l,3}=1;l=l+1;

Map(n).M{l,1}='LFCC13_';Map(n).M{l,2}='LFCC';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='LFCC5_';Map(n).M{l,2}='LFCC5';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='LFCC13w';Map(n).M{l,2}='LFCCw';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='LFCC5w';Map(n).M{l,2}='LFCC5w';Map(n).M{l,3}=1;l=l+1;

Map(n).M{l,1}='TimeF';Map(n).M{l,2}='Time';Map(n).M{l,3}=1;l=l+1;
Map(n).M{l,1}='Time_w';Map(n).M{l,2}='Timew';Map(n).M{l,3}=1;l=l+1;

