function IF=ImpulseFactor(S) 

if size(size(S))<3
     IF=0.5*(max(S)-min(S))./mean(abs(S));
else
	for i=1:size(S,1)
        Si=squeeze(S(i,:,:));
        IF(i,:)=0.5*(max(Si)-min(Si))./mean(abs(Si));
    end
end