clear all;close all;clc;warning off;
addpath('./discrim')
   

% Production of data set of features
%==========================================================================

DPath='E:\1_WORKSPACE\2_MAESTRA\5_ASSUNTOS\CESE\1_DATASETS\DB_JoelAntunes2_-3dB';
DPath='E:\1_WORKSPACE\2_MAESTRA\5_ASSUNTOS\CESE\1_DATASETS\DB_JoelAntunes2_-12dB';
%OutDir = 'E:\1_WORKSPACE\2_MAESTRA\5_ASSUNTOS\CESE\1_DATASETS\DB_SmDnCmDn_Ftrs';
%OutDir = 'E:\1_WORKSPACE\2_MAESTRA\5_ASSUNTOS\CESE\1_DATASETS\DB_Results';
OutDir = 'E:\1_WORKSPACE\2_MAESTRA\5_ASSUNTOS\CESE\1_DATASETS\Tmp';

mthd ='EMDSV';
%mthd ='Time';
%mthd ='EMDEnt';
%mthd ='LFCC5';
%mthd ='LFCC';
%mthd ='EMDEng';

Fs=5100; FrmSz=256; FrmStp=FrmSz/2;         % Global parameters
PRM.Fs=Fs;PRM.MaxFrq=Fs/2; PRM.MinFrq=10;   % LFCC parameters
PRM.MaxCmps=4;                              % EMD parameters

tacc={};
  
DB = Read_DB(DPath,{'SmDn' 'csv'},[],0); DtSt= []; t=0;tcntr=0;
for j=1:length(DB)
    
	OUT = Read_Data_File(DB(j).Path,[],[]); signal = OUT.Dt(:,2:4)';

    if (strcmp(mthd,'LFCC') || strcmp(mthd,'LFCC_w'))
        PRM.S = Frmng2(signal,FrmSz,FrmStp,hanning(FrmSz));
        PRM.NmbCpCoe=13;PRM.NmbFltrs=13; 
        strt=tic; Ftrs = LFCC(PRM); t=t+toc(strt);tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    if (strcmp(mthd,'LFCC5') || strcmp(mthd,'LFCC5_w'))
        PRM.S = Frmng2(signal,FrmSz,FrmStp,hanning(FrmSz));
        PRM.NmbCpCoe=5;PRM.NmbFltrs=5; 
        strt=tic; Ftrs = LFCC(PRM); t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    if (strcmp(mthd,'Time') || strcmp(mthd,'Time_w'))
        frm = Frmng2(signal,FrmSz,FrmStp,ones(1,FrmSz));
        strt=tic;
        Ftrs = [ CrestFactor(frm)'  KurtosisTime(frm)'  ClearanceFactor(frm)'  ImpulseFactor(frm)' ShapeFactor(frm)' ];
        t=t+toc(strt);tcntr=tcntr+1;
    end
    if (strcmp(mthd,'EMDEnt') || strcmp(mthd,'EMDEnt_w')) 
        PRM.S = Frmng2(signal,FrmSz,FrmStp,ones(1,FrmSz));
        PRM.msr='Entropy';  
        strt=tic; Ftrs = emd2(PRM);t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    if (strcmp(mthd,'EMDSV') || strcmp(mthd,'EMDSV_w')) 
        PRM.S = Frmng2(signal,FrmSz,FrmStp,ones(1,FrmSz));
        PRM.msr='SingValue'; 
        strt=tic; Ftrs = emd2(PRM);t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    if (strcmp(mthd,'EMDEng') || strcmp(mthd,'EMDEng_w')) 
        PRM.S = Frmng2(signal,FrmSz,FrmStp,ones(1,FrmSz));
        PRM.msr='Energy';  
        strt=tic; Ftrs = emd2(PRM);t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    
    %Median filtering
    if (isempty( strfind( DB(j).name ,'_w' )))
        disp('f')
       Ftrs = squeeze(median(Frmng2(Ftrs',64,1,ones(64,1)),2))'; 
    end

    DtSt = [ DtSt ; zeros(size(Ftrs,1),1) Ftrs]; %Anotation 
    %pause;
end

DB = Read_DB(DPath,{ 'CmDn' 'csv' },[],0);
for j=1:length(DB)
	OUT=Read_Data_File(DB(j).Path,[],[]);signal = OUT.Dt(:,2:4)'; 
    
    if (strcmp(mthd,'LFCC') || strcmp(mthd,'LFCC_w'))
        PRM.S = Frmng2(signal,FrmSz,FrmStp,hanning(FrmSz));
        PRM.NmbCpCoe=13;PRM.NmbFltrs=13; 
        strt=tic;Ftrs = LFCC(PRM);t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    if (strcmp(mthd,'LFCC5') || strcmp(mthd,'LFCC5_w'))
        PRM.S = Frmng2(signal,FrmSz,FrmStp,hanning(FrmSz));
        PRM.NmbCpCoe=5;PRM.NmbFltrs=5; 
        strt=tic;Ftrs = LFCC(PRM);t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    if (strcmp(mthd,'Time') || strcmp(mthd,'Time_w'))
        frm = Frmng2(signal,FrmSz,FrmStp,ones(1,FrmSz));
        strt=tic;
        Ftrs = [ CrestFactor(frm)'  KurtosisTime(frm)'  ClearanceFactor(frm)'  ImpulseFactor(frm)' ShapeFactor(frm)' ];
        t=t+toc(strt); tcntr=tcntr+1;
    end
    if (strcmp(mthd,'EMDEnt') || strcmp(mthd,'EMDEnt_w')) 
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
        PRM.S = Frmng2(signal,FrmSz,FrmStp,ones(1,FrmSz));
        PRM.msr='Entropy'; 
        strt=tic;Ftrs = emd2(PRM);t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    if (strcmp(mthd,'EMDSV') || strcmp(mthd,'EMDSV_w')) 
        PRM.S = Frmng2(signal,FrmSz,FrmStp,ones(1,FrmSz));
        PRM.msr='SingValue'; 
        strt=tic;Ftrs=emd2(PRM); t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    if (strcmp(mthd,'EMDEng') || strcmp(mthd,'EMDEng_w')) 
        PRM.S = Frmng2(signal,FrmSz,FrmStp,ones(1,FrmSz));
        PRM.msr='Energy';  
        strt=tic; Ftrs = emd2(PRM);t=t+toc(strt); tcntr=tcntr+1;
        Ftrs = [ squeeze(Ftrs(1,:,:))'  squeeze(Ftrs(2,:,:))' squeeze(Ftrs(3,:,:))' ];
    end
    

    %Median filtering
    if (isempty( strfind( DB(j).name ,'_w' )))
       Ftrs = squeeze(median(Frmng2(Ftrs',64,1,ones(64,1)),2))'; 
    end
    
    DtSt = [ DtSt ; ones(size(Ftrs,1),1) Ftrs]; %Anotation 
end

tacc=t/tcntr  %Run time per frame

%Discriminant analysis
X = DtSt(:,2:end-1); Y = DtSt(:,1);
wiggle(X);

% Linear Discriminant Analysis (aka. Fisher Discriminant Analysis) linear quadratic
X = DtSt(:,2:end); Y = DtSt(:,1)+1;
if(~isempty(strfind(mthd,'LFCC5'))) X(:,[5 10 15])=[];end
if(~isempty(strfind(mthd,'LFCC13'))) X(:,[13 26 39])=[];end
    
%s = RandStream('mt19937ar','Seed',1);RandStream.setGlobalStream(s);
new_i=randperm(size(X,1)); X=X(new_i,:); Y=Y(new_i,:); %Examples shuffle

%Create the K folders
K=10  % 10-fold cross validation
S=floor(size(X,1)/K);
for k=1:K
   Xf(k)={X(((k-1)*S)+1:(k*S),:)};  Yf(k)={Y(((k-1)*S)+1:(k*S),:)}; %Cross Validation grouping
end
    
for i=1:K 
    Xtr=[];Ytr=[];
    for j=1:K
        if i~=j
            Xtr=[Xtr ; Xf{j}]; Ytr=[Ytr ; Yf{j}];
        end
    end
    
    f_lda=lda(Xtr,Ytr); f_qda=qda(Xtr,Ytr); f_logda=logda(Xtr,Ytr);
    Yk=Yf{i}-1;
    
    Yhat=classify(f_lda,Xf{i})-1;
    DetRate_lda(i) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
    FPRate_lda(i) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
    Acc_lda(i)= length(find(Yk==Yhat))/length(Yk)*100;

    Yhat=classify(f_qda,Xf{i})-1;
    DetRate_qda(i) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
    FPRate_qda(i) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
    Acc_qda(i)= length(find(Yk==Yhat))/length(Yk)*100;

    Yhat=classify(f_logda,Xf{i})-1;
    DetRate_logda(i) = length(find(Yk==1 & Yhat==1))/length(find(Yk==1))*100;
    FPRate_logda(i) = length(find(Yk==0 & Yhat==1))/length(find(Yk==0))*100;
    Acc_logda(i)= length(find(Yk==Yhat))/length(Yk)*100;
end

disp('Results ------------------')
disp(['LDA Detection Rate  :' num2str( mean(DetRate_lda) )])
disp(['LDA Accuracy        :' num2str(mean(Acc_lda))])
disp(['LDA False Alarm Rate:' num2str(mean(FPRate_lda))])
disp('----')
disp(['QDA Detection Rate  :' num2str(mean(DetRate_qda))])
disp(['QDA Accuracy        :' num2str(mean(Acc_qda))])
disp(['QDA False Alarm Rate:' num2str(mean(FPRate_qda))])
disp('----')
disp(['LogDA Detection Rate  :' num2str(mean(DetRate_logda))])
disp(['LogDA Accuracy        :' num2str(mean(Acc_logda))])
disp(['LogDA False Alarm Rate:' num2str(mean(FPRate_logda))])


tacc

















