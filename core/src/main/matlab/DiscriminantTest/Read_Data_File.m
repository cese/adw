%                       INPUT
%----------------------------------------------------
% Path  - File  path
% type  - Type of data 
% 

%                       OUTPUT
%----------------------------------------------------
%OUT.Dt     - File data content
%OUT.MtHdr  - Multi-track header

% For wav
%--------
%OUT.Fs     - Sampling frequency 

%For arff 
%-------
%OUT.AttrbtsNm
%OUT.AttrbtsTps
%OUT.orng  - List of output index (automatically decteted)
%OUT.irng  - List of input index (automatically decteted)


function OUT=Read_Data_File(Path,ntrck,dttp)

disp(Path);
OUT=0;

%1-Try to extract file information from DBInfo.txt file
%-----------------------------------------------------
Path2=Path;
Found=1;
while Found && ~isempty(Path2)
    Path2=Path2(1:max(strfind(Path2,'\'))-1);
    FlLst=dir(Path2);
    for i=1:length(FlLst)
        if(strcmp(FlLst(i).name,'DBInfo.txt'))
            Found=0;
            break;
        end 
    end
end
info=DBInfo(Path2);

if isempty(info)
    %disp('Read_Data_File: DBInfo.txt not found');
else
    dttp=info.dttp;
    ntrk=info.ntrk;
    type=info.data;
end

%Finds the file format
%-----------------------------------------------------
idx=strfind(Path,'.');
if (length(idx)==0)
    type='csv'
else
    idx=idx(length(idx));
    type=Path(idx+1:end);
end



%-----------------------------------------------------
switch type

   
case 'xlsx'
    
    [num,txt,raw] = xlsread(Path);
    raw=raw(2:end,:);
    OUT.MtHdr=txt(1,:);
    OUT.Dt=raw;
    
    OUT.Dt=cell2mat(raw); idx=[];
    for i=1:size(OUT.Dt,1)
        if(sum(isnan(OUT.Dt(i,:)))>0)
            idx=[idx i];
        end
    end
    
    OUT.Dt(idx,:)=[];
    
    
case 'xls'
%--------------------------------------------------------------------------    
    [num,txt,raw] = xlsread(Path);
    raw=raw(2:end,:);
    OUT.MtHdr=txt(1,:);
    OUT.Dt=raw;
    %OUT.Dt=cell2mat(raw);
    
case 'wav'
%--------------------------------------------------------------------------    
	[OUT.Dt,OUT.Fs]=wavread(Path);
case 'dat'
%--------------------------------------------------------------------------     
	fid=fopen(Path,'r');
	OUT.Dt=fread(fid, [ ntrk inf], dttp)'; fclose(fid);
case 'edf'
%--------------------------------------------------------------------------    
    fid=fopen(Path,'r');
	OUT.Dt=fread(fid, [ ntrk inf], dttp)'; fclose(fid);
case 'arff'
%--------------------------------------------------------------------------
    %[OUT.DtStNm,OUT.AttrbtNm, OUT.AttrbtTp,OUT.Dt,OUT.OutptIdx]= arffread(Path);
	
    
	OUT=arffread2(Path);
	
case 'csv'
%--------------------------------------------------------------------------
    OUT=csvread2(Path);
otherwise
disp([ 'Read_Data_File: type unknown type= ' type ]);
end
end
%::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


%==========================================================================
% Path - 

%==========================================================================

    %TODO: detect nominal columns
    %tline=fgetl(fid);
    %fclose(fid);
    %s1 = regexp(tline, '[A-Z]');
    %s1 = regexp(tline, '[a-z]');
    %-------------------

    %Read Data
    %-------------------
    %OUT.Dt = csvread(Path,1,0);
%     OUT.Dt = dlmread(Path,fch);
%     
% end

%==========================================================================
% Path- Path of the csv file 

function OUT=csvread2(Path)

%Open file
%-------------------
fid=fopen(Path,'r');
if(fid==-1) ; disp('Read_Data_File: Could not open file...'); return; end

%Line counting
%-------------------
tline=fgetl(fid);
LnCntr=1;
while ischar(tline)
    tline=fgetl(fid); LnCntr=LnCntr+1;
end
LnCntr=LnCntr-1;
frewind(fid);

%Detect separator character
%-------------------
tline=fgetl(fid);
chars=',; ';fch='a';
for i=1:length(chars)
    if(~isempty(strfind(tline,chars(i))))
         fch=chars(i);break;
    end
end
if(fch=='a'); disp('Read_Data_File: Separator not recognized...');return;
else disp(['Read_Data_File: separator= ' fch ]); end

% Read Header/Column counter
%-------------------
idk=strfind(tline,fch);
header(1)={tline(1:idk(1)-1 )}; ClCntr=1;
for i=1:length(idk)-1
    header(i+1)={ tline( idk(i)+1:idk(i+1)-1 ) };
    ClCntr=ClCntr+1;
end
header(i+2) = {tline( idk(i+1)+1:end )};
ClCntr=ClCntr+1;
OUT.MtHdr=header;

% Data type detection/transformation
%----------------------
tline=fgetl(fid);
idk=strfind(tline,fch);ClCntr=1;
Type(1)=ContnsNmbrOnly(tline(1:idk(1)-1));
for i=1:length(idk)-1
    Type(i+1)=ContnsNmbrOnly(tline( idk(i)+1:idk(i+1)-1 ));
    ClCntr=ClCntr+1; 
end
Type(i+2)=ContnsNmbrOnly( tline( idk(i+1)+1:end ) );


% Read the data 
%----------------------
% Non numeric data - stored into a cell
if ~isempty(find(Type==0)) 

    Dt=cell(LnCntr-1,length(idk)+1); cntr=1;
    for k=1:LnCntr-1

        %Process one line
        tline=fgetl(fid);
        if(~ischar(tline)); break;end

        idk=strfind(tline,fch);

        %First colunm
        strng=tline(1:idk(1)-1); 
        if isempty(strng);  Dt{k,1}='<Empty>';
        else Dt{k,1}=strng; end

        %Intermediate colunms
        for i=1:length(idk)-1
             strng=tline( idk(i)+1:idk(i+1)-1 );
             if isempty(strng);  Dt{k,i+1}='<Empty>';
             else  Dt{k,i+1}=strng;   end
        end

        %Last colunm
        strng=tline(idk(end)+1:end); 
        if isempty(strng); Dt{k,end}='<Empty>';
        else    Dt{k,end}=strng;  end

        %Instances counter
        if( mod(cntr,1000)==0);  disp(cntr); end
        cntr=cntr+1;
    end
    fclose(fid);
else % Only numeric data- stored into a matrix
%-------------------    
    Dt = csvread(Path,1,0);
end

fclose(fid);

OUT.Type=Type; OUT.Dt=Dt;

end


function out=ContnsNmbrOnly(str)
out=1;
NmbrStr='-.0123456789';
for i=1:length(str)
    if isempty( strfind(NmbrStr,str(i)) )
       out=0; return
    end
end
end



%==========================================================================
function out=DBInfo(Path)

out=0;
fid=fopen([Path '/DBInfo.txt']);
if(fid==-1)
    out=[];return;
end

ln=fgetl(fid);
dtcnt=1;atcnt=1;
while ischar(ln)
    k=strfind(ln,':');
    fld=ln(1:k-1);
    
    switch fld
        case 'data'
            out.data=ln(k+1:end);
            dtcnt=dtcnt+1;
        case 'atri'
            out.atri=ln(k+1:end);
            atcnt=atcnt+1;
        case 'dttp'
            out.dttp=ln(k+1:end);
        case 'ntrk'
            out.ntrk=str2double( ln(k+1:end) );
    end
    
    ln=fgetl(fid);
end
fclose(fid);


if(0)
    disp('           DB Info  ');
    disp('------------------------------');
    disp([ 'File format     : ' out.data ]);
    disp([ 'Atribure file   : ' out.atri ]);
    disp([ 'Data type       : ' out.dttp ]);
    disp([ 'Number of tracks: ' num2str(out.ntrk) ]);
    disp('------------------------------');
end

end



%--------------------------------------------------------------------------
function OUT=arffread2(FlNm)

OUT=[];
fid=fopen(FlNm);
                    
if (fid==-1)
	return;
end

% Le o cabeçalho
%--------------------------------------------------------------------------
tline=[];
OUT.AttrbtsNm={};
OUT.AttrbtsTps={};

%Le o nome do data set e a localização dos outputs
tline=[];
while 1
	tline= fgetl(fid); 
    if ( ~isempty(strfind(tline,'@relation')) || ...
         ~isempty(strfind(tline,'@RELATION') ) )
        break
    end
end
relation=tline;


% Le os atributos e suas caracteristicas
tline=[];
while 1
	tline= fgetl(fid); 
    if ( ~isempty(strfind(tline,'@attribute')) || ... 
         ~isempty(strfind(tline,'@ATTRIBUTE')) )
        break
    end
end

k=strfind(tline,' ');
OUT.AttrbtsNm=[ OUT.AttrbtsNm { tline( k(1)+1:k(2)-1) }];
OUT.AttrbtsTps=[ OUT.AttrbtsTps { tline( k(2)+1:end) }];

while 1
	tline= fgetl(fid);
    if( feof(fid))break; end;
    if (  isempty(strfind(tline,'@attribute')) && ...
          isempty(strfind(tline,'@ATTRIBUTE')) )
        break 
    end
    
    k=strfind(tline,' ');
    OUT.AttrbtsNm=[ OUT.AttrbtsNm { tline( k(1)+1:k(2)-1) }];
    OUT.AttrbtsTps=[ OUT.AttrbtsTps { tline( k(2)+1:end) }];
end

% for i=1:length(OUT.AttrbtsNm)
%     disp([ OUT.AttrbtsNm(i) OUT.AttrbtsTps(i)] );  
% end

k=strfind(relation,'-');
if k~=0
    val=str2num(relation(k+2:end));
    if ( strcmp(relation(k+1),'C') == 1 )
        disp('Outputs at the begining');
        OUT.irng=(val+1):length(OUT.AttrbtsNm);
        OUT.orng=[1:val];
    else
        disp('Outputs at the end');
        OUT.orng=length(OUT.AttrbtsNm)-val+1:length(OUT.AttrbtsNm);
        OUT.irng=[1:length(OUT.AttrbtsNm)-val]; 
    end
end
   
% Encontra o campo data
while 1
    if( feof(fid))break; end;
    if ( ~isempty(strfind(tline,'@data')) || ...     
         ~isempty(strfind(tline,'@DATA')) )
        break; 
    end
    tline= fgetl(fid);
end

%Verifica se é um dataset esparso
tline= fgetl(fid);
tline= fgetl(fid);
SprsFlg=~isempty(strfind(tline,'{'));

% Conta o numero de exemplos
i=1;counter=2;
while 1
    tline= fgetl(fid);
    tline(strfind(tline,' '))=[];
    if( feof(fid))break; end;
    i=i+1;
    counter=counter+1;
end
Dt=zeros(counter,length(OUT.AttrbtsNm));
frewind(fid);

% Volta atrs no ficheiro e vai directamente para o campo data
% Finds the data field
while 1
    if( feof(fid))break; end;
    if ( ~isempty(strfind(tline,'@data')) || ~isempty(strfind(tline,'@DATA')) )
        break; 
    end
    tline= fgetl(fid);
end

% Reads data lines
disp('arffread2: Reading examples')


tline= fgetl(fid); % read enpty line 

if SprsFlg==0
    i=1;
    while 1
        tline= fgetl(fid);
        % Line format transformation
        %tline(strfind(tline,' '))=',';
        %tline(strfind(tline,' '))=[]
        tline(strfind(tline,','))=' ';
    
        if( feof(fid))break; end;
        
        if( ~isempty(str2num(tline)) )
            Dt(i,:)=str2num(tline);
        else
            disp('uuu')
        end
    
        if(mod(i,10000)==0)
            disp(i);
        end
        i=i+1;
    end
else
    i=1;
    while 1
        tline= fgetl(fid);
        if( feof(fid))break; end;
        
        Dt(i,:)=Uncmprss(tline,length(OUT.AttrbtsNm));
        
        if(mod(i,10000)==0)
            disp(i);
        end
        i=i+1;
    end 
end

OUT.Dt=Dt;
fclose(fid);

end

function Ln=Uncmprss(str,lnght)

if length(str)<=2
    Ln=zeros(1,lnght);
    return;
end

k=strfind(str,',');
NmbrOfVls=length(k)+1;
LstPr=zeros(NmbrOfVls,2);

if NmbrOfVls==1
  LstPr(1,:)=str2num(str(2:end-1));
  Ln=zeros(1,lnght);
  Ln(LstPr(1,1)+1)=LstPr(1,2);
  return;
end

tststrg=str(2:k(1)-1);
LstPr(1,:)=str2num(tststrg);

for i=1:NmbrOfVls-2
    tststrg=str(k(i)+1:k(i+1)-1);
    LstPr(i+1,:)=str2num(tststrg);
end

tststrg=str(k(end):end-1);
LstPr(end,:)=str2num(tststrg);

Ln=zeros(1,lnght);

for i=1:NmbrOfVls
    Ln(LstPr(i,1)+1)=LstPr(i,2);
end

end


%=========================================================================
function [dataName,attributeName, attributeType, data, OutptIdx ]= arffread(fileName)
% ARFFREAD  Reads arff formatted file.
%          
% USAGE:
%       [dataName,attributeName, attributeType, data] = arffRead(fileName)
%
% INPUT:    
%       fileName:       file name to be read
%       
%                           OUTPUT:
%--------------------------------------------------------------------------
%dataName:       relation name of the arff file
%       attributeName:       attribute name of attribute as cell array
%                       { 1 by nAttr }
%       attributeType:       attribute type of attribute as cell array
%                       { 1 by nAttr}
%       data:           data (nInstan by nAttr)
%    
 
% ***********************************************************************
if nargin < 1,
	error('No input arguments!');
end
if nargin > 1,
	error('Too many input arguments!');
end

% read whole string
wholeData = textread(fileName,'%s','delimiter','\n','whitespace','','bufsized',10000);
            %NOTA: o textread está limitado pelo buffer de memoria
atRelation = '@relation';
atAttribute = '@attribute';
atData = '@data';
noOfLines = size(wholeData,1);
k=0;

% Finding data name
%************************************************************************

for i=1:noOfLines
	k = findstr(wholeData{i},atRelation);
	if k ~= 0;
		lineAtRelation = i;
		[token,dataName] = strtok(wholeData{lineAtRelation});
		break
	end
end

%Output index
k=findstr(dataName,'-C ');
if k(1)~=0
    OutptIdx=str2num( dataName(k(1)+3:end) );
else
    OutptIdx=0;
end

% Check whether dataName has whitespaces
tf = isspace(dataName);
tf = find(tf ==1);
if size(tf,2)>1
	dataName = dataName(2:tf(2)-1);
else
	dataName = dataName(2:size(dataName,2));
end

% Check whether dataName has semicolons or others
% First convert to ascii code and note that quotation mark is 39 is ascii
ascDataName = double(dataName);
if ascDataName(1) == 39 
   ascDataName = ascDataName(2:end);
end
if ascDataName(end) == 39 
   ascDataName = ascDataName(1:end-1);
end

dataName = char(ascDataName);   % Convert back to characters

%Finding attribute name
%************************************************************************
lineAtAttribute =[];
k=0;l=0;j=0;
for i=lineAtRelation+1:noOfLines
	k = findstr(wholeData{i},atAttribute);
	if k ~= 0;
		lineAtAttribute =[lineAtAttribute i];
		[chopped,remainder] = strtok(wholeData{i});
		[attrName,remAttrType] = strtok(remainder);
		[attrType,rem] = strtok(remAttrType);
		j=j+1;
		attrVector{j} = attrName;
    	attrTypeVector{j} = attrType;
	end
	l = findstr(wholeData{i},atData);
	if l ~= 0;
		lineAtData = i;
		break
	end
end


% Finding whether data is tab formatted or csv and the position of data
%************************************************************************
k = [];
for i=lineAtData+1:noOfLines
	str = wholeData{i};
	if ~isempty(str) & ~strcmp(str,'%')
		k = findstr(wholeData{i},',');
		if ~isempty(k);
			dataFormat ='comma' ;
			lineData = i;
			break
		else
			dataFormat ='tabOrSpace' ;
			lineData = i;
			break
		end
	end
end

%Reading formatted data
%************************************************************************

%nRowSkip=lineData-1;
nColSkip = 0;
%dataName = dataName;
attributeName = attrVector ;
attributeType = attrTypeVector;
%
% % You have to convert '' marks for each var to write in arff file
% if strcmp(dataFormat,'comma')
% 	data = csvread(fileName,nRowSkip);
% elseif strcmp(dataFormat,'tabOrSpace') | strcmp(dataFormat,'tab')...
% 		| strcmp(dataFormat,'Space')
%     data = dlmread(fileName,'\t',nRowSkip,nColSkip);		% Space delimiter
% 	dataFormat = 'space';
% 	if size(data,2)~=size(attributeName,2)
%     	data = dlmread(fileName,'\t' ,nRowSkip,nColSkip);	% tab delimiter
% 		dataFormat = 'tab';
% 	end
% 	if size(data,2)~=size(attributeName,2)
% 		error('arff file is not tab or comma delemited!');
% 	end
% end
%
strData = wholeData(lineData:end);

% reads the first line 
nf = length( strfind(strData{1},',')  )+1;
if nf==1
   nf = length( strfind(strData{1},' ')  );
end
data=zeros(size(strData,1),nf);

size(data)

for i = 1:size(strData,1)  
    data(i,:) = str2num(strData{i});
   if(mod(i,100)==0)
       disp(num2str(i));
   end
end

end





