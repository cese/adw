function OUT=DA(X,Y,type)

OUT=0;

[Yhat,err,POSTERIOR,logp,coeff] = classify(X,X,Y,type);

% [f, iter, dev, hess] = logda(X,Y+1);
% plotobs(X,Y+1); hold on, plotdr(f);
% c = X*f.coefs(end:-1:2)' + f.coefs(1);
% figure(45)
% plot(c)
% pause;
% Yhat=(sign( 1/(1+exp(X*f.coefs(end:-1:2)'))  + f.coefs(1) ) + 1 )/2;
% f.coefs
% figure(45)
% plot(Yhat)
% pause;

OUT.DetRate = length(find(Y==1 & Yhat==1))/length(find(Y==1))*100;
OUT.FPRate = length(find(Y==0 & Yhat==1))/length(find(Y==0))*100;
OUT.err = err*100;

%K = coeff(1,2).const
%L = coeff(1,2).linear
%Q = coeff(1,2).quadratic
%f = @(x,y) K + [x y]*L + sum(([x y]*Q) .* [x y], 2);


end


%logitp = @(b,x) exp(b(1)+b(2).*x)./(1+exp(b(1)+b(2).*x));