function SF=ShapeFactor(S) 

if size(size(S))<3
    SF=std(S)./mean(abs(S));
else
	for i=1:size(S,1)
        Si=squeeze(S(i,:,:));
        SF(i,:)=std(Si)./mean(abs(Si));
    end
end