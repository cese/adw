function ClF=ClearanceFactor(S) 

N=size(S,2);
if size(size(S))<3
    ClF=N*0.5*(max(S)-min(S))./sum(abs(S).^0.5).^2;
else
	for i=1:size(S,1)
        Si=squeeze(S(i,:,:));
        ClF(i,:)=N*0.5*(max(Si)-min(Si))./sum(abs(Si).^0.5).^2;
    end
end