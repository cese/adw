function K=KurtosisTime(S) 

if size(size(S))<3
    K=kurtosis(S,1,1);
else
	for i=1:size(S,1)
        Si=squeeze(S(i,:,:));
        K(i,:)=kurtosis(Si,1,1);
    end

end