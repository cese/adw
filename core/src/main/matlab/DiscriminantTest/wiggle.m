function Res=wiggle(M)


if ( ~iscell(M) )
      
    for i=1:size(M,2)
        Res(:,i)= M(:,i)-mean(M(:,i));
        if ( max(abs(Res(:,i))) ~= 0 )
            Res(:,i)= Res(:,i)./max(abs(Res(:,i)) ) ;
        end
        Res(:,i)= Res(:,i)*0.4 + i;
    end

else  % M is a cell
  
    disp('Wiggle: it is a cell')
    
    %Classify data type
    for i=1:size(M,2)
        if( isnumeric(M{1,i}))
            Type(i)=1;
        else
            Type(i)=0;
        end
        Type(i);
    end
   
    for i=1:size(M,2)   
        if (Type(i)==1) % Numeric columns processing
            Res(:,i)= Cell2Mat( M(:,i)) - mean( Cell2Mat(M(:,i)) );
            if ( max(abs(Res(:,i))) ~= 0 )
                Res(:,i)= Res(:,i)./max(abs(Res(:,i)) ) ;
            end
            Res(:,i)= Res(:,i)*0.4 + i;
         
        else   %Nominal columns processing
            
         values=unique(M(:,i));
         for k=1:size(M,1)
             for j=1:length(values)
                 if( strcmp(values{j},M(j,i)) )
                     Res(k,i)=j;
                     break;
                 end
             end
         end
         
         if ( max(abs(Res(:,i))) ~= 0 )
             Res(:,i)= Res(:,i)./max(abs(Res(:,i)) ) ;
         end
         Res(:,i)= Res(:,i)*0.4 + i;
         
     end 
   end  
end

plot(Res);

end


%--------------------------------------------------------------------------
function out=Cell2Mat(M)
[r,c]=size(M);
for i=1:r
    for j=1:c
        out(i,j)=M{i,j};
    end
end
end 

%--------------------------------------------------------------------------
function out=Encode(M)


end