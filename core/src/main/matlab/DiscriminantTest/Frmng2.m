%                            INPUTS
%--------------------------------------------------------------------------
%Sgnl-   time signal (NmbrOfTracksxSz)
%FrmSz-  Frame size (1x1)  (number of samples)
%FrmStp  Frame step (1x1) (number of samples)
%wndw    Window  (1xFrmSz)

%                            OUTPUTS
%--------------------------------------------------------------------------
%OUT-  Matrix of frames ( NmbrOfTracks x FrmSz x floor[(Sz-FrmSz)/FrmStp] )

function out=Frmng2(Sgnl,FrmSz,FrmStp,wndw)


% Input verification
%---------------------
if size(Sgnl,1)>size(Sgnl,2)
    Sgnl=Sgnl';
end

if size(wndw,1)>1
    wndw=wndw';
end

out=-1;
if(size(Sgnl,2)<FrmSz)
    disp('Frmg: Signal length less than frame size');
    return;
end
NmbrOfTracks=size(Sgnl,1);
NmbrFrms=floor((size(Sgnl,2)-FrmSz)/FrmStp );
out=zeros(NmbrOfTracks,FrmSz,NmbrFrms);% Pre-allocation works faster

for j=1:NmbrOfTracks
    FrmCntr=1;
    for k=1:FrmStp:size(Sgnl,2)-FrmSz
        out(j,:,FrmCntr)=Sgnl(j,k:k+FrmSz-1).*wndw;
        FrmCntr=FrmCntr+1;
    end
end





