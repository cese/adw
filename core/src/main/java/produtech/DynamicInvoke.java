package produtech;

import java.util.*;

import pt.inescn.app.RabbitConfigs;
import pt.inescn.utils.ADWError;

import pt.inescn.etl.stream.Load;
import pt.inescn.utils.JEither;
import produtech.STasks.*;
import pt.inescn.utils.JRight;

/**
 * This is a test class to experiment with the interfacing of Java and Scala.
 * Here we access the `produtech.STask` that serves as Java wrappers. Here we
 * also try out the pipelines to be used in several projects. Unlike the
 * platforms' pipelines, these can be constructed dynamically. However type
 * consistency is not guaranteed and execution may fail during run-time.
 *
 * Here we also provide an example of a machine learning pipeline used to
 * detect failures in ball bearings based on an accelerometer signal (3
 * axis). Note that pile construction is lazy. Only when we consume data
 * does it execute.
 *
 * To execute the code, do:
 * sbt "root/ runMain produtech.DynamicInvoke"
 */
public class DynamicInvoke {

    // Where to load the configuration (Block Vars)
//    String rfilename = "core/src/main/java/produtech/ReceiveConfig_Tear1";    //for debugging purposes use the path core/...
    // String rfilename = "src/main/java/produtech/ReceiveConfig_Tear1";
    //String rfilename = "src/main/java/produtech/ReceiveConfig_Tear2";
    //String rfilename = "src/main/java/produtech/ReceiveConfig_Tear3";
    //String rfilename = "src/main/java/produtech/ReceiveConfig_Tear4";
    String rfilename = "src/main/java/produtech/ReceiveConfig_NewMachine";

    RabbitConfigs.ReceiveAppConfig rconfig = RabbitConfigs.ReceiveAppConfig$.MODULE$.apply(rfilename);
    String receiveConfig = rconfig.toJason();


    // Where to load the configuration (Ind Vars)
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear1_varMeters";
//    String rfilenameind = "core/src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1";    //for debugging purposes use the path core/...
    String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear1_varTempS2";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear1_varTempS3";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear2_varMeters";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear2_varTempS1";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear2_varTempS2";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear2_varTempS3";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear3_varMeters";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear3_varTempS1";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear3_varTempS2";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear3_varTempS3";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear4_varMeters";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear4_varTempS1";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear4_varTempS2";
    //String rfilenameind = "src/main/scala/produtech/ReceiveConfig2_tear4_varTempS3";

    //String rfilenameind = "core/src/main/scala/produtech/ReceiveConfig2_tear1_varTempS1";

    RabbitConfigs.ReceiveAppConfig rconfigind = RabbitConfigs.ReceiveAppConfig$.MODULE$.apply(rfilenameind);
    String receiveConfigInd = rconfigind.toJason();


    /**
     * Examples of basic use of the DynamicCompile API
     *
     * @see produtech.DynamicCompile
     * @see produtech.STasks
     */
    private void basicUsage() {
        // Get task 1
        STAdd$ tsk1 = STAdd$.MODULE$;
        Map<String,String> psk1 = new HashMap<>();
        psk1.put("p1", "1");

        // Use a task
        System.out.println("Get the function");
        System.out.println(tsk1.name());
        System.out.println("Invoke the function");
        Object otsk1 = tsk1.exec(psk1,2);
        System.out.println("Show the result");
        System.out.println(otsk1.getClass().toString()+":"+otsk1);


        // Get task 2
        STSubtract$ tsk2 = STSubtract$.MODULE$;
        Map<String,String> psk2 = new HashMap<>();
        psk2.put("p1", "2.0");
        Object otsk2 = tsk2.exec(psk2,otsk1);
        System.out.println("Show the result tsk1 compose task2");
        System.out.println(otsk2.getClass().toString()+":"+otsk2);


        // Check if a Task's parameter values can be converted
        Map<String,String> prms1 = new HashMap<>();
        prms1.put("p1", "1");
        JEither<List<String>, List<?>> tsk1pOk1 = DynamicCompile.taskParamsOk(tsk1, prms1);
        System.out.println(tsk1pOk1);
        prms1.put("p1", "a");
        JEither<List<String>, List<?>> tsk1pOk2 = DynamicCompile.taskParamsOk(tsk1, prms1);
        System.out.println(tsk1pOk2);


        // Check if a pipe's parameter values can be converted
        List<STask> pipe1 = Arrays.asList(tsk1, tsk2);
        List<Map<String, String>> params1 = Arrays.asList(psk1, psk2);
        JEither<List<String>, List<List<?>>> pipe1Ok = DynamicCompile.paramsOk(pipe1, params1);
        System.out.println(pipe1Ok);

        // Check if a pipe can be execute (parameters values and in/out types ok)
        JEither<List<String>, List<?>> canCompose1 = DynamicCompile.canCompose(pipe1, params1);
        System.out.println(canCompose1);
        // This will fail
        Map<String,String> prms2 = new HashMap<>();
        prms2.put("p1", "?");
        List<Map<String, String>> params2 = Arrays.asList(psk1, prms2);
        JEither<List<String>, List<?>> canCompose = DynamicCompile.canCompose(pipe1, params2);
        System.out.println(canCompose);


        // Execute the pipe.  First check if a pipe can be execute (see above)
        JEither<String,Object> result = DynamicCompile.composeExec(pipe1, params1, "100");
        System.out.println(result);
        System.out.println(result.right());

        /*
         * The next 2 tasks (3 and 4) are kept as reference;
         * however, due to changes in the infrastructure, you no longer need to send data (task3) from a file, as
         * the message broker is continually being fed with data now.
         * You only need to establish the connection and consume messages.
         * */
        /*// Get task 3
        String dataFile = "data/inegi/ensaios_rolamentos_2/rol1_1000_cmdn_ens1.csv";
        STLoadFrame$ tsk3 = STLoadFrame$.MODULE$;
        Map<String,String> psk3 = new HashMap<>();
        psk3.put("file", dataFile);
        Object otsk3 = tsk3.exec(psk3,"");
        System.out.println("Show the result task3");
        // Uses overridden toString
        // System.out.println(otsk3.getClass().toString(5)+":"+otsk3);
        // Always prints the contents !!
        //System.out.println(otsk3.getClass().toString()+":"+otsk3);
        // Always prints the contents !!
        //System.out.println(otsk3.getClass()+":"+otsk3);
        System.out.println(otsk3.getClass());
        Load.Frame frame3 = (Load.Frame)otsk3;
        scala.collection.immutable.List<Load.Row> rows3 = frame3.iterable().take(10).toList();
        System.out.println(rows3.mkString(";\n"));

        // Get task 4
        STMaybeLoadFrame$ tsk4 = STMaybeLoadFrame$.MODULE$;
        Map<String,String> psk4 = new HashMap<>();
        psk4.put("file", dataFile);
        Object otsk4 = tsk4.exec(psk4,"");
        System.out.println("Show the result task4");
        System.out.println(otsk4.getClass());
        @SuppressWarnings (value="unchecked")
        JRight<ADWError, Load.Frame> right4 = (JRight<ADWError, Load.Frame>)otsk4;
        Load.Frame frame4 = right4.right();
        scala.collection.immutable.List<Load.Row> rows4 = frame4.iterable().take(10).toList();
        System.out.println(rows4.mkString(";\n"));*/
    }

    /**
     * Example of demo pipeline: receive data from the message broker.
     */
    private void produtechReceive() {

        /*
         * The next task (1) is kept as reference;
         * however, due to changes in the infrastructure, you no longer need to send data from a file, as
         * the message broker is continually being fed with data now.
         * You only need to establish the connection and consume messages.
         * */

        // The file generates many records, sending all these takes too long
        // So we only send a limited amount of data for this example
        // IMPORTANT: we send enough records for 10 predictions only using the parameters below
        // This will ensure that a run of this methods will not leave messages pending
        /*RabbitConfigs.SendAppConfig sconfig = tmp.setNumMessages("1");
        String sendConfig = sconfig.toJason();

        // Use Send Task to simulate sensor data
        STSend$ tsk1 = STSend$.MODULE$;
        Map<String, String> psk1 = new HashMap<>();  //create a map that will be used on exec
        psk1.put("configs", sendConfig);
        psk1.put("dir", "data/inegi/ensaios_rolamentos_3");
        psk1.put("filter", "*.csv");
        //System.out.println(psk1.get("configs").getClass());
        //System.out.println(psk1.get("configs"));
        Object otsk1 = tsk1.exec(psk1, "");
        System.out.println(otsk1.getClass().toString()+":"+otsk1);

        System.out.println("-----------------------------------");*/


        //STReceive$ tsk5 = STReceive$.MODULE$;
        STReceive2$ tsk5 = STReceive2$.MODULE$;
        Map<String, String> psk5 = new HashMap<>();  //create a map that will be used on exec
        psk5.put("configs", receiveConfig);
        System.out.println(psk5.get("configs").getClass());
        Object otsk5 = tsk5.exec(psk5, "");
        @SuppressWarnings(value = "unchecked")
        JRight<ADWError, Load.Frame> right5 = (JRight<ADWError, Load.Frame>) otsk5;
        Load.Frame frame5 = right5.right();

        // Consume a certain number of records and then stop
        scala.collection.immutable.List<Load.Row> rows5 = frame5.iterable().take(8).toList();
        System.out.println(rows5.mkString(";\n"));
        tsk5.stop();
        System.out.println("Finished Read");

        // Server running forever
        //Iterator<Load.Row> iter = FrameUtils.toIterator(frame5);
        //iter.forEachRemaining((Load.Row e) -> System.out.println(e.toString()));
    }


    /**
     * check the compatibility between tasks inputs and outputs
     */
    private void checkCompatibility() {
        // Def task 1
        STAdd$ tsk1 = STAdd$.MODULE$;
        Map<String,String> psk1 = new HashMap<>();
        psk1.put("p1", "1");

        // This task performs the addition operation
        // Perform the operation 2 + 1
        // notice: 1 is a parameter in psk1, 2 is input in the exec function of the task
        Object otsk1 = tsk1.exec(psk1,2);
        System.out.println("Show the result");
        System.out.println(otsk1.getClass().toString()+":"+otsk1);

        //notice tsk1 has input and output Integer

        // Def task 2
        STSubtract$ tsk2 = STSubtract$.MODULE$;
        Map<String,String> psk2 = new HashMap<>();
        psk2.put("p1", "5.0");

        // This task performs the subtraction operation
        // Perform the operation 3 - 5.0
        // notice: 5.0 is a parameter in psk2, 3 is input in the exec function of the task (and the output of task 1)
        Object otsk2 = tsk2.exec(psk2,otsk1);
        System.out.println("Show the result tsk1 compose task2");
        System.out.println(otsk2.getClass().toString()+":"+otsk2);

        //notice tsk2 has input Integer and output Double
        //this operation was possible because otsk1 has type integer

        // Def task 3
        STMultiply$ tsk3 = STMultiply$.MODULE$;
        Map<String,String> psk3 = new HashMap<>();
        psk3.put("p1", "10.0");

        // This task performs the multiplication operation
        // Perform the operation 3 * 10.0
        // notice: 10.0 is a parameter in psk3, 3 is input in the exec function of the task (and the output of task 1)
        Object otsk3 = tsk3.exec(psk3,otsk1);
        System.out.println("Show the result tsk1 compose task3");
        System.out.println(otsk2.getClass().toString()+":"+otsk3);

        //notice tsk3 has input Integer and output Double
        //this operation was possible because otsk1 has type integer

        // Operation that fails
        // Perform the operation -2.0 * 10.0
        // notice: 10.0 is a parameter in psk3, -2.0 is input in the exec function of the task (and the output of task 2)
        /*Object otsk4 = tsk3.exec(psk3,otsk2);
        System.out.println("Show the result tsk1 compose task3");
        System.out.println(otsk4.getClass().toString()+":" + otsk4);*/

        //notice tsk3 has input Integer and output Double
        //this operation was not possible because otsk2 has type double (output of tsk2),
        //however, the task multiplication has input of type integer

        //in case you want to check compatibility beforehand you can use the properties of trait STasks:
        // inClass and outClass
        if (tsk1.outClass().equals(tsk2.inClass())) {
            System.out.println("****************************compatible****************************");
            System.out.println("Tasks input and output match with class: " + tsk1.outClass());
            System.out.println("****************************compatible****************************");
        } else {
            System.out.println("****************************not_compatible****************************");
            System.out.println("Tasks input and output do not match." + System.lineSeparator() +
                    "Task1 has output type: " + tsk1.outClass() + System.lineSeparator() +
                    "and Task2 has input type: " + tsk2.inClass());
            System.out.println("****************************not_compatible****************************");
        }

        //yields compatible - you can use the output of task1 as input to task2 (same is applies for task1 and task3)
        //however, if you run the object otsk4 (which feeds the output of task2 as input to task3 it fails):

        if (tsk2.outClass().equals(tsk3.inClass())) {
            System.out.println("****************************compatible****************************");
            System.out.println("Tasks input and output match with class: " + tsk2.outClass());
            System.out.println("****************************compatible****************************");
        } else {
            System.out.println("****************************not_compatible****************************");
            System.out.println("Tasks input and output do not match." + System.lineSeparator() +
                    "Task2 has output type: " + tsk2.outClass() + System.lineSeparator() +
                    "and Task3 has input type: " + tsk3.inClass());
            System.out.println("****************************not_compatible****************************");
        }

    }

    /**
     * Example of demo pipeline. Execute all tasks of the example pipe manually.
     * Tasks:
     *   1. Receive data from the message broker
     *   2. Change the column names so that the next ML steps can be used
     *   3. Converts variables type to double
     *   4. Generate windows of length FFTSz and stride FFTStride
     *   5. Calculate the linear Cepstral coefficients
     *   6. Concatenates the set of LFCCs of each sensor into a single LFCC feature vector
     *   7. Generate windows of a given size and stride
     *       NB: the windows are large. If not enough data available for the
     *       total number of frames we request in the take, the system will wait
     *       The small test file used has no more than 4 windows
     *    8. Once again we smooth the features by calculating the median of the LFCCs
     *    9. Now assign a label to each window - we use the mode
     *        We could set a failure as an average, median or simply true
     *        if any label was true (max)
     *    10. Trains the anomaly detector (on-line). Uses a simple *Cantelli+ statistic
     *    11. Uses the model learned in STAnomalyDetectionTrainStep to detect failures.
     *        Prediction placed in the `det` column
     *
     * @see inegi.ADIRAI40Experiments
     */
    private void produtechPipeExample2() {

        /*
         * This Pipe is based on AdiraI40Experiments 1 pipe
         */
        // RICARDO'S PIPE
        int FFTSz = 256;
        int FFTStride = 64;
        int Fs = 5100;
        int NmbFltrs = 5;
        double MaxFrq = 2550.0;
        double MinFrq = 10.0;
        int NmbCpCoe = 5;
        int FrmSz2 = 128;
        int FrmStp2 = 1;

        // Read data
        //STReceive$ tsk5 = STReceive$.MODULE$;
        STReceive2$ tsk5 = STReceive2$.MODULE$;
        Map<String, String> psk5 = new HashMap<>();  //create a map that will be used on exec
        psk5.put("configs", receiveConfig);
        System.out.println(psk5.get("configs").getClass());

        // Rename columns to expected name - STRenameColumns - explicitly identifying current column name and target name - hard-coded for machine "tear1"
        /*STRenameColumns$ tsk6 = STRenameColumns$.MODULE$;
        Map<String, String> psk6 = new HashMap<>();  //create a map that will be used on exec
        psk6.put("ColumnRenames", "84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration X.Timestamp -> t, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration Z -> Z, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration Y -> Y, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration X -> X");*/

        // Rename columns to expected name - STGetIDRenameColumns - automatically identify column ID and target name - works for every machine, provided it follows the convention for variable id
        STGetIDRenameColumns$ tsk6 = STGetIDRenameColumns$.MODULE$;
        Map<String, String> psk6 = new HashMap<>();  //create a map that will be used on exec
        psk6.put("ColumnRenames", "");

        // Converts the relative timestamp (in nanoseconds) to double values
        STToDouble$ tsk9 = STToDouble$.MODULE$;
        Map<String, String> psk9= new HashMap<>();
        psk9.put("ColumnNames", "X, Y, Z");


        // Generate windows of length FFTSz and stride FFTStride
        STWindow$ tsk10 = STWindow$.MODULE$;
        Map<String, String> psk10= new HashMap<>();
        psk10.put("size", String.valueOf(FFTSz));
        psk10.put("stride", String.valueOf(FFTStride));


        // Calculate the linear Cepstral coefficients
        STLfccStep$ tsk12 = STLfccStep$.MODULE$;
        Map<String, String> psk12= new HashMap<>();
        psk12.put("Fs", String.valueOf(Fs));
        psk12.put("NmbFltrs", String.valueOf(NmbFltrs));
        psk12.put("MaxFrq", String.valueOf(MaxFrq));
        psk12.put("MinFrq", String.valueOf(MinFrq));
        psk12.put("NmbCpCoe", String.valueOf(NmbCpCoe));
        psk12.put("ColumnNames", "X, Y, Z");
        psk12.put("ColumnName", "lfcc");


        // Concatenates the set of LFCCs of each sensor into a single LFCC feature vector
        STConcatenateStep$ tsk13 = STConcatenateStep$.MODULE$;
        Map<String, String> psk13= new HashMap<>();
        psk13.put("ColumnNames", "Xlfcc, Ylfcc, Zlfcc");
        psk13.put("ColumnName", "lfcc");

        // Generate windows of a given size and stride
        // NB: the windows are large. If not enough data available for the
        // total number of frames we request in the take, the system will wait
        // The small test file used has no more than 4 windows
        STWindow$ tsk14 = STWindow$.MODULE$;
        Map<String, String> psk14= new HashMap<>();
        psk14.put("size", String.valueOf(FrmSz2));
        psk14.put("stride", String.valueOf(FrmStp2));


        // Once again we smooth the features by calculating the median of the LFCCs
        STMedianStep$ tsk15 = STMedianStep$.MODULE$;
        Map<String, String> psk15= new HashMap<>();
        psk15.put("NmbOfColumns", String.valueOf(NmbCpCoe*3));
        psk15.put("Stride", String.valueOf(FrmSz2));
        psk15.put("cols", "lfcc");
        psk15.put("col", "median");


        // Trains the anomaly detector (on-line). Uses a simple "Cantelli" statistic
        STAnomalyDetectionTrainStep$ tsk17 = STAnomalyDetectionTrainStep$.MODULE$;
        Map<String, String> psk17= new HashMap<>();
        psk17.put("FtrsNmbr", String.valueOf(NmbCpCoe*3));
        psk17.put("AnomThres1", "0.9");
        psk17.put("AnomThres2", "0.9");
        psk17.put("NTrainFaseExmpl", "200");
        psk17.put("cols", "lfccmedian");
        psk17.put("col", "det");


        /*
         * Pipe creation.
         *
         * IMPORTANT: we need to stop() the receive task if we want to
         * shutdown the client thread. If we don't do this the application
         * will not terminate. If however we execute the receive task and
         * then use it again in the pipe, data will be consumed by two clients.
         * This means not enough messages may exist in the stream to complete
         * the windows. The client will then wait indefinitely for more data.
         */


        List<STask> pipe = Arrays.asList(
                tsk5, tsk6, tsk9, tsk10,
                tsk12, tsk13, tsk14, tsk15,
                tsk17);
        List<Map<String, String>> params = Arrays.asList(
                psk5, psk6, psk9, psk10,
                psk12, psk13, psk14, psk15,
                psk17);


        JEither<List<String>, List<List<?>>> pipeOk = DynamicCompile.paramsOk(pipe, params);
        //System.out.println("PipeOK");
        JEither<List<String>, List<?>> canCompose1 = DynamicCompile.canCompose(pipe, params);
        JEither<String,Object> result = DynamicCompile.composeExec(pipe, params, "");
        System.out.println("Pipe composed");
        @SuppressWarnings (value="unchecked")
        JRight<String, Load.Frame> result_1 = (JRight<String, Load.Frame>)result.right();
        System.out.println("Frame ready");

        System.out.println("Pipe starting... (about 10 sec)");


        //Check if failure is detected and publish messages
        /* Notice that queueName is a fix parameter - there is only this queue for publishing;
        The publishAll variable is used to control the publishing type:
        true - publish all messages (detected anomaly and not),
        false - publish messages only if anomaly is detected.
        */
        STDetectCheck$ tsk18 = STDetectCheck$.MODULE$;
        Map<String, String> psk18= new HashMap<>();
        psk18.put("configs", receiveConfig);
        psk18.put("queueName", "outbox/ba6a6169-8fe1-4b23-8e4b-bfc65da81549/alarms");
        psk18.put("publishAll", String.valueOf(true));

        //Load.Frame pipeFrame = result_1.right();
        JRight<String,Load.Frame> pipeFrame = result_1;//.right();

        Object otsk18 = tsk18.exec(psk18, pipeFrame);

        @SuppressWarnings(value = "unchecked")
        JRight<ADWError, Load.Frame> right5 = (JRight<ADWError, Load.Frame>) otsk18;
        Load.Frame frame5 = right5.right();

        // Consume a certain number of records and then stop
        frame5.iterable().take(8).foreach( x -> null );

        /*// Consume a certain number of records and then stop
        // - use this option if you need to print the frame for testing purposes
        scala.collection.immutable.List<Load.Row> rows5 = frame5.iterable().take(8).toList();
        System.out.println(rows5.mkString(";\n"));*/


        // close connection so JVM can terminate
        tsk18.stop();
        tsk5.stop();
        System.out.println("... finished PipeExample2");

    }

    private void produtechPipeExample2b() {

        /*
         * This Pipe is based on AdiraI40Experiments 1 pipe
         */
        // RICARDO'S PIPE
        int FFTSz = 256;
        int FFTStride = 64;
        int Fs = 5100;
        int NmbFltrs = 5;
        double MaxFrq = 2550.0;
        double MinFrq = 10.0;
        int NmbCpCoe = 5;
        int FrmSz2 = 128;
        int FrmStp2 = 1;

        // Read data
        //STReceive$ tsk5 = STReceive$.MODULE$;
        STReceive2$ tsk5 = STReceive2$.MODULE$;
        Map<String, String> psk5 = new HashMap<>();  //create a map that will be used on exec
        psk5.put("configs", receiveConfig);
        System.out.println(psk5.get("configs").getClass());

        // Rename columns to expected name - STRenameColumns - explicitly identifying current column name and target name - hard-coded for machine "tear1"
        /*STRenameColumns$ tsk6 = STRenameColumns$.MODULE$;
        Map<String, String> psk6 = new HashMap<>();  //create a map that will be used on exec
        psk6.put("ColumnRenames", "84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration X.Timestamp -> t, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration Z -> Z, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration Y -> Y, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration X -> X");*/

        // Rename columns to expected name - STGetIDRenameColumns - automatically identify column ID and target name - works for every machine, provided it follows the convention for variable id
        STGetIDRenameColumns$ tsk6 = STGetIDRenameColumns$.MODULE$;
        Map<String, String> psk6 = new HashMap<>();  //create a map that will be used on exec
        psk6.put("ColumnRenames", "");

        // Converts the relative timestamp (in nanoseconds) to double values
        STToDouble$ tsk9 = STToDouble$.MODULE$;
        Map<String, String> psk9= new HashMap<>();
        psk9.put("ColumnNames", "X, Y, Z");

        // Generate windows of length FFTSz and stride FFTStride
        STWindow$ tsk10 = STWindow$.MODULE$;
        Map<String, String> psk10= new HashMap<>();
        psk10.put("size", String.valueOf(FFTSz));
        psk10.put("stride", String.valueOf(FFTStride));

        // Calculate the linear Cepstral coefficients
        STLfccStep$ tsk12 = STLfccStep$.MODULE$;
        Map<String, String> psk12= new HashMap<>();
        psk12.put("Fs", String.valueOf(Fs));
        psk12.put("NmbFltrs", String.valueOf(NmbFltrs));
        psk12.put("MaxFrq", String.valueOf(MaxFrq));
        psk12.put("MinFrq", String.valueOf(MinFrq));
        psk12.put("NmbCpCoe", String.valueOf(NmbCpCoe));
        psk12.put("ColumnNames", "X, Y, Z");
        psk12.put("ColumnName", "lfcc");

        // Concatenates the set of LFCCs of each sensor into a single LFCC feature vector
        STConcatenateStep$ tsk13 = STConcatenateStep$.MODULE$;
        Map<String, String> psk13= new HashMap<>();
        psk13.put("ColumnNames", "Xlfcc, Ylfcc, Zlfcc");
        psk13.put("ColumnName", "lfcc");

        // Generate windows of a given size and stride
        // NB: the windows are large. If not enough data available for the
        // total number of frames we request in the take, the system will wait
        // The small test file used has no more than 4 windows
        STWindow$ tsk14 = STWindow$.MODULE$;
        Map<String, String> psk14= new HashMap<>();
        psk14.put("size", String.valueOf(FrmSz2));
        psk14.put("stride", String.valueOf(FrmStp2));

        // Once again we smooth the features by calculating the median of the LFCCs
        STMedianStep$ tsk15 = STMedianStep$.MODULE$;
        Map<String, String> psk15= new HashMap<>();
        psk15.put("NmbOfColumns", String.valueOf(NmbCpCoe*3));
        psk15.put("Stride", String.valueOf(FrmSz2));
        psk15.put("cols", "lfcc");
        psk15.put("col", "median");

        // Trains the anomaly detector (on-line). Uses a simple "Cantelli" statistic
        STAnomalyDetectionTrainStep$ tsk17 = STAnomalyDetectionTrainStep$.MODULE$;
        Map<String, String> psk17= new HashMap<>();
        psk17.put("FtrsNmbr", String.valueOf(NmbCpCoe*3));
        psk17.put("AnomThres1", "0.9");
        psk17.put("AnomThres2", "0.9");
        psk17.put("NTrainFaseExmpl", "200");
        psk17.put("cols", "lfccmedian");
        psk17.put("col", "det");

        //Check if failure is detected and publish messages
        /* Notice that queueName is a fix parameter - there is only this queue for publishing;
        The publishAll variable is used to control the publishing type:
        true - publish all messages (detected anomaly and not),
        false - publish messages only if anomaly is detected.
        */
        STDetectCheck$ tsk18 = STDetectCheck$.MODULE$;
        Map<String, String> psk18= new HashMap<>();
        psk18.put("configs", receiveConfig);
        psk18.put("queueName", "outbox/ba6a6169-8fe1-4b23-8e4b-bfc65da81549/alarms");
        psk18.put("publishAll", String.valueOf(true));


        /*
         * Pipe creation.
         *
         * IMPORTANT: we need to stop() the receive task if we want to
         * shutdown the client thread. If we don't do this the application
         * will not terminate. If however we execute the receive task and
         * then use it again in the pipe, data will be consumed by two clients.
         * This means not enough messages may exist in the stream to complete
         * the windows. The client will then wait indefinitely for more data.
         */


        List<STask> pipe = Arrays.asList(
                tsk5, tsk6, tsk9, tsk10,
                tsk12, tsk13, tsk14, tsk15,
                tsk17,
                tsk18);
        List<Map<String, String>> params = Arrays.asList(
                psk5, psk6, psk9, psk10,
                psk12, psk13, psk14, psk15,
                psk17,
                psk18);


        JEither<List<String>, List<List<?>>> pipeOk = DynamicCompile.paramsOk(pipe, params);
        //System.out.println("PipeOK");
        JEither<List<String>, List<?>> canCompose1 = DynamicCompile.canCompose(pipe, params);
        JEither<String,Object> result = DynamicCompile.composeExec(pipe, params, "");
        System.out.println("Pipe composed");
        @SuppressWarnings (value="unchecked")
        JRight<String, Load.Frame> result_1 = (JRight<String, Load.Frame>)result.right();
        System.out.println("Frame ready");

        System.out.println("Pipe starting... (about 10 sec)");


        //Load.Frame pipeFrame = result_1.right();
        JRight<String,Load.Frame> pipeFrame = result_1;//.right();

        Load.Frame frame5 = result_1.right();

        // Consume a certain number of records and then stop
        frame5.iterable().take(8).foreach( x -> null );

        /*// Consume a certain number of records and then stop
        // - use this option if you need to print the frame for testing purposes
        scala.collection.immutable.List<Load.Row> rows5 = frame5.iterable().take(8).toList();
        System.out.println(rows5.mkString(";\n"));*/


        // close connection so JVM can terminate
        tsk18.stop();
        tsk5.stop();
        System.out.println("... finished PipeExample2b");

    }

    private void produtechPipeExample3() {

        // Read data
        STReceive2$ tsk1 = STReceive2$.MODULE$;
        Map<String, String> psk1 = new HashMap<>();  //create a map that will be used on exec
        psk1.put("configs", receiveConfigInd);

        // Get the column IDs that are to be converted to double
        // Converts the value the independent variable takes to double
        STToDouble_GetIDInd$ tsk11 = STToDouble_GetIDInd$.MODULE$;
        Map<String, String> psk11 = new HashMap<>();  //create a map that will be used on exec
        psk11.put("configs", receiveConfigInd);

        //Task to check if values fall within range
        STRangeCheck$ tsk2 = STRangeCheck$.MODULE$;
        Map<String, String> psk2 = new HashMap<>();  //create a map that will be used on exec
        psk2.put("configs", receiveConfigInd);
        psk2.put("queueName", "outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts");
        psk2.put("publishAll", String.valueOf(false));

        /*
         * Pipe creation.
         *
         * IMPORTANT: we need to stop() the receive task if we want to
         * shutdown the client thread. If we don't do this the application
         * will not terminate. If however we execute the receive task and
         * then use it again in the pipe, data will be consumed by two clients.
         * This means not enough messages may exist in the stream to complete
         * the windows. The client will then wait indefinitely for more data.
         */


        List<STask> pipe = Arrays.asList(
                tsk1, tsk11, tsk2);
        List<Map<String, String>> params = Arrays.asList(
                psk1, psk11, psk2);


        JEither<List<String>, List<List<?>>> pipeOk = DynamicCompile.paramsOk(pipe, params);
        //System.out.println("PipeOK");
        JEither<List<String>, List<?>> canCompose1 = DynamicCompile.canCompose(pipe, params);
        JEither<String,Object> result = DynamicCompile.composeExec(pipe, params, "");
        System.out.println("Pipe composed");
        @SuppressWarnings (value="unchecked")
        JRight<String, Load.Frame> result_1 = (JRight<String, Load.Frame>)result.right();
        System.out.println("Frame ready");

        System.out.println("Pipe starting... (about 10 sec)");

        //Load.Frame pipeFrame = result_1.right();
        JRight<String,Load.Frame> pipeFrame = result_1;//.right();


        Load.Frame r_frame = result_1.right();
        r_frame.iterable().foreach( x -> null );


        /*// Consume a certain number of records and then stop
        // - use this option if you need to print the frame for testing purposes
        scala.collection.immutable.List<Load.Row> rows5 = frame5.iterable().take(8).toList();
        System.out.println(rows5.mkString(";\n"));*/


        // close connection so JVM can terminate
        tsk1.stop();
        tsk11.stop();
        tsk2.stop();
        System.out.println("... finished PipeExample3");

    }

    private void produtechPipeExample4() {

        // Read data
        STReceive2$ tsk1 = STReceive2$.MODULE$;
        Map<String, String> psk1 = new HashMap<>();  //create a map that will be used on exec
        psk1.put("configs", receiveConfigInd);


        // Filter frame if values are empty
        STFilterFormat$ tsk2 = STFilterFormat$.MODULE$;
        Map<String, String> psk2 = new HashMap<>();  //create a map that will be used on exec
//        psk2.put("varType", "ind");
        //config might be required to get var IDs - check - yes; added
//        psk2.put("configs", receiveConfigInd);

        // Get the column IDs that are to be converted to double
        // Converts the value the independent variable takes to double
        STToDouble_GetIDInd$ tsk3 = STToDouble_GetIDInd$.MODULE$;
        Map<String, String> psk3 = new HashMap<>();  //create a map that will be used on exec
        psk3.put("configs", receiveConfigInd);

        //Task to check if values fall within range
        STRangeCheck$ tsk4 = STRangeCheck$.MODULE$;
        Map<String, String> psk4 = new HashMap<>();  //create a map that will be used on exec
        psk4.put("configs", receiveConfigInd);
        psk4.put("queueName", "outbox/bee97f7d-43bb-442d-b040-2a54098f0f4b/alerts");
        psk4.put("publishAll", String.valueOf(false));

        /*
         * Pipe creation.
         *
         * IMPORTANT: we need to stop() the receive task if we want to
         * shutdown the client thread. If we don't do this the application
         * will not terminate. If however we execute the receive task and
         * then use it again in the pipe, data will be consumed by two clients.
         * This means not enough messages may exist in the stream to complete
         * the windows. The client will then wait indefinitely for more data.
         */


        List<STask> pipe = Arrays.asList(
                tsk1, tsk2, tsk3, tsk4);
        List<Map<String, String>> params = Arrays.asList(
                psk1, psk2, psk3, psk4);


        JEither<List<String>, List<List<?>>> pipeOk = DynamicCompile.paramsOk(pipe, params);
        //System.out.println("PipeOK");
        JEither<List<String>, List<?>> canCompose1 = DynamicCompile.canCompose(pipe, params);
        JEither<String,Object> result = DynamicCompile.composeExec(pipe, params, "");
        System.out.println("Pipe composed");
        @SuppressWarnings (value="unchecked")
        JRight<String, Load.Frame> result_1 = (JRight<String, Load.Frame>)result.right();
        System.out.println("Frame ready");

        System.out.println("Pipe starting... (about 10 sec)");

        //Load.Frame pipeFrame = result_1.right();
        JRight<String,Load.Frame> pipeFrame = result_1;//.right();


        Load.Frame r_frame = result_1.right();
        r_frame.iterable().foreach( x -> null );


        /*// Consume a certain number of records and then stop
        // - use this option if you need to print the frame for testing purposes
        scala.collection.immutable.List<Load.Row> rows5 = frame5.iterable().take(8).toList();
        System.out.println(rows5.mkString(";\n"));*/


        // close connection so JVM can terminate
        tsk1.stop();
        tsk3.stop();
        tsk4.stop();
        System.out.println("... finished PipeExample4");

    }

    /**
     * Example of demo pipeline - learning pipeline.
     * Tasks:
     * 1. Receive data from the message broker
     * 2. Filter formatting errors (exclude messages with empty fields - should not occur)
     * 3. Renames columns so that the next ML steps can be used
     * 4. Converts accelerometer variables type to double
     * 5. Generate windows of length FFTSz and stride FFTStride
     * 6. Calculate the linear Cepstral coefficients
     * 7. Concatenates the set of LFCCs of each sensor into a single LFCC feature vector
     * 8. Generate windows of a given size and stride
     *    NB: the windows are large. If not enough data available for the
     *    total number of frames we request in the take, the system will wait
     *    The small test file used has no more than 4 windows
     * 9. Once again we smooth the features by calculating the median of the LFCCs
     * 10. Trains the anomaly detector (on-line). Uses a simple *Cantelli+ statistic
     * 11. Uses the model learned in STAnomalyDetectionTrainStep to detect failures.
     *      Prediction placed in the `det` column
     * 12. Publish results (according to PublishAll might publish all predictions or only
     * those to which anomaly is detected - variable equals zero or one)
     */

    private void produtechPipeExample5() {

        /*
         * This Pipe is based on AdiraI40Experiments 1 pipe
         */
        // RICARDO'S PIPE
        int FFTSz = 256;
        int FFTStride = 64;
        int Fs = 5100;
        int NmbFltrs = 5;
        double MaxFrq = 2550.0;
        double MinFrq = 10.0;
        int NmbCpCoe = 5;
        int FrmSz2 = 128;
        int FrmStp2 = 1;

        // Read data
        STReceive2$ tsk1 = STReceive2$.MODULE$;
        Map<String, String> psk1 = new HashMap<>();  //create a map that will be used on exec
        psk1.put("configs", receiveConfig);
        System.out.println(psk1.get("configs").getClass());

        // Filter frame if values are empty
        STFilterFormat$ tsk2 = STFilterFormat$.MODULE$;
        Map<String, String> psk2 = new HashMap<>();  //create a map that will be used on exec

        // Rename columns to expected name - STRenameColumns - explicitly identifying current column name and target name - hard-coded for machine "tear1"
        /*STRenameColumns$ tsk6 = STRenameColumns$.MODULE$;
        Map<String, String> psk6 = new HashMap<>();  //create a map that will be used on exec
        psk6.put("ColumnRenames", "84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration X.Timestamp -> t, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration Z -> Z, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration Y -> Y, 84ce2b03-90b9-11e8-9eb6-529269fb1459:Variables:Vibration X -> X");*/

        // Rename columns to expected name - STGetIDRenameColumns - automatically identify column ID and target name - works for every machine, provided it follows the convention for variable id
        STGetIDRenameColumns$ tsk3 = STGetIDRenameColumns$.MODULE$;
        Map<String, String> psk3 = new HashMap<>();  //create a map that will be used on exec
        psk3.put("ColumnRenames", "");

        // Converts the relative timestamp (in nanoseconds) to double values
        STToDouble$ tsk4 = STToDouble$.MODULE$;
        Map<String, String> psk4= new HashMap<>();
        psk4.put("ColumnNames", "X, Y, Z");

        // Generate windows of length FFTSz and stride FFTStride
        STWindow$ tsk5 = STWindow$.MODULE$;
        Map<String, String> psk5= new HashMap<>();
        psk5.put("size", String.valueOf(FFTSz));
        psk5.put("stride", String.valueOf(FFTStride));

        // Calculate the linear Cepstral coefficients
        STLfccStep$ tsk6 = STLfccStep$.MODULE$;
        Map<String, String> psk6= new HashMap<>();
        psk6.put("Fs", String.valueOf(Fs));
        psk6.put("NmbFltrs", String.valueOf(NmbFltrs));
        psk6.put("MaxFrq", String.valueOf(MaxFrq));
        psk6.put("MinFrq", String.valueOf(MinFrq));
        psk6.put("NmbCpCoe", String.valueOf(NmbCpCoe));
        psk6.put("ColumnNames", "X, Y, Z");
        psk6.put("ColumnName", "lfcc");

        // Concatenates the set of LFCCs of each sensor into a single LFCC feature vector
        STConcatenateStep$ tsk7 = STConcatenateStep$.MODULE$;
        Map<String, String> psk7= new HashMap<>();
        psk7.put("ColumnNames", "Xlfcc, Ylfcc, Zlfcc");
        psk7.put("ColumnName", "lfcc");

        // Generate windows of a given size and stride
        // NB: the windows are large. If not enough data available for the
        // total number of frames we request in the take, the system will wait
        // The small test file used has no more than 4 windows
        STWindow$ tsk8 = STWindow$.MODULE$;
        Map<String, String> psk8= new HashMap<>();
        psk8.put("size", String.valueOf(FrmSz2));
        psk8.put("stride", String.valueOf(FrmStp2));

        // Once again we smooth the features by calculating the median of the LFCCs
        STMedianStep$ tsk9 = STMedianStep$.MODULE$;
        Map<String, String> psk9= new HashMap<>();
        psk9.put("NmbOfColumns", String.valueOf(NmbCpCoe*3));
        psk9.put("Stride", String.valueOf(FrmSz2));
        psk9.put("cols", "lfcc");
        psk9.put("col", "median");

        // Trains the anomaly detector (on-line). Uses a simple "Cantelli" statistic
        STAnomalyDetectionTrainStep$ tsk10 = STAnomalyDetectionTrainStep$.MODULE$;
        Map<String, String> psk10= new HashMap<>();
        psk10.put("FtrsNmbr", String.valueOf(NmbCpCoe*3));
        psk10.put("AnomThres1", "0.9");
        psk10.put("AnomThres2", "0.9");
        psk10.put("NTrainFaseExmpl", "200");
        psk10.put("cols", "lfccmedian");
        psk10.put("col", "det");

        //Check if failure is detected and publish messages
        /* Notice that queueName is a fix parameter - there is only this queue for publishing;
        The publishAll variable is used to control the publishing type:
        true - publish all messages (detected anomaly and not),
        false - publish messages only if anomaly is detected.
        */
        STDetectCheck$ tsk11 = STDetectCheck$.MODULE$;
        Map<String, String> psk11= new HashMap<>();
        psk11.put("configs", receiveConfig);
        //psk11.put("queueName", "outbox/ba6a6169-8fe1-4b23-8e4b-bfc65da81549/alarms");
        psk11.put("queueName", "outbox/tests");
        psk11.put("publishAll", String.valueOf(true));


        /*
         * Pipe creation.
         *
         * IMPORTANT: we need to stop() the receive task if we want to
         * shutdown the client thread. If we don't do this the application
         * will not terminate. If however we execute the receive task and
         * then use it again in the pipe, data will be consumed by two clients.
         * This means not enough messages may exist in the stream to complete
         * the windows. The client will then wait indefinitely for more data.
         */


        List<STask> pipe = Arrays.asList(
                tsk1, tsk2, tsk3, tsk4, tsk5,
                tsk6, tsk7, tsk8, tsk9, tsk10,
                tsk11);
        List<Map<String, String>> params = Arrays.asList(
                psk1, psk2, psk3, psk4, psk5,
                psk6, psk7, psk8, psk9, psk10,
                psk11);


        JEither<List<String>, List<List<?>>> pipeOk = DynamicCompile.paramsOk(pipe, params);
        //System.out.println("PipeOK");
        JEither<List<String>, List<?>> canCompose1 = DynamicCompile.canCompose(pipe, params);
        JEither<String,Object> result = DynamicCompile.composeExec(pipe, params, "");
        System.out.println("Pipe composed");
        @SuppressWarnings (value="unchecked")
        JRight<String, Load.Frame> result_1 = (JRight<String, Load.Frame>)result.right();
        System.out.println("Frame ready");

        System.out.println("Pipe starting... (about 10 sec)");


        //Load.Frame pipeFrame = result_1.right();
        JRight<String,Load.Frame> pipeFrame = result_1;//.right();

        Load.Frame frame5 = result_1.right();

        // Consume a certain number of records and then stop
        // frame5.iterable().take(8).foreach( x -> null );
        frame5.iterable().take(1).foreach( x -> null );

        /*// Consume a certain number of records and then stop
        // - use this option if you need to print the frame for testing purposes
        scala.collection.immutable.List<Load.Row> rows5 = frame5.iterable().take(8).toList();
        System.out.println(rows5.mkString(";\n"));*/


        // close connection so JVM can terminate
        tsk11.stop();
        tsk1.stop();
        System.out.println("... finished PipeExample5");

    }

    public static void main(String[] args) {

        DynamicInvoke di = new DynamicInvoke();

        // Basic use of STasks
        //di.basicUsage();
        // How to manually set-up a receive and a process task
        //di.produtechReceive();

        // Check compatibility between tasks input and output
        //di.checkCompatibility();

        //How to automatically set-up 14 tasks:
        //Pipeline demonstrator
        //Learning pipe (block vars processing) + publish task
        //di.produtechPipeExample2();

        //How to automatically set-up 14 tasks:
        //Pipeline demonstrator
        //Learning pipe (block vars processing) + publish task
        //Integrates publish task in pipe, unlike produtechPipeExample2 (pipe + publish task)
        //di.produtechPipeExample2b();

        //Pipeline demonstrator
        //Range detect pipe (ind. vars processing) (publish task within pipe)
        // di.produtechPipeExample3();

        //Pipeline demonstrator
        //Range detect pipe (ind. vars processing) (publish task within pipe) (filtering task)
        //di.produtechPipeExample4();

        //Pipeline demonstrator
        //Learning pipe (real data)
        di.produtechPipeExample5();
    }
}