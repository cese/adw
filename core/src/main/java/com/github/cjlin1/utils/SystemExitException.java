package com.github.cjlin1.utils;

/**
 * Created by hmf on 21-07-2017.
 */
public class SystemExitException extends RuntimeException {
    public SystemExitException() {
        super();
    }

    public SystemExitException(String message) {
        super(message);
    }

    public SystemExitException(String message, Throwable cause) {
        super(message, cause);
    }

    public SystemExitException(Throwable cause) {
        super(cause);
    }
}
