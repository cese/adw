package com.github.cjlin1.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;

/**
 *
 * This class was created to "replace" the <code>java.lang.System</code> class. This is because the libSVM code
 * calls the <code>System.exit()</code> when an error is detected. This in turn terminates the scripts
 * that should fail with an exception or error status.
 *
 * Issue 1: the code in this package tha does need to access the <code>java.lang.System</code> class
 * cannot hide this class. The solution is to import the <code>java.lang.System</code> class with
 * a different name (for example <code>JSystem</code>.
 *
 * Issue 2: Because the original <code>System</code> is a static class, this one should also be. We would
 * have liked to simply add the <code>import com.github.cjlin1.utils.System;</code> in order to access
 * and use it without any more changes. Unfortunately when the standard in, out and error streams are
 * changed, the equivalent static variables of this class are not updated. To avoid any more plumbing
 * and making too many changes to the code, we opted to make these streams functions. The original code must
 * now use <code>out()</code> and <code>err()</code> with the parenthesis.
 *
 * Created by hmf on 21-07-2017.
 * @see https://stackoverflow.com/questions/31743760/system-out-is-declared-as-static-final-and-initialized-with-null
 * @see [[java.lang.System.out]], [[java.lang.System.err]]
 */
public class System {
    private InputStream in;
    private PrintStream out;
    private PrintStream err;

    public System(InputStream in, PrintStream out, PrintStream err){
        this.in = in;
        this.out = out;
        this.err = err;
    }

    public InputStream in() {
        return in;
    }
    public PrintStream out() {
        return out;
    }
    public PrintStream err() { return err; }
    public void close() {
        try {
            this.in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.out.close();
        this.err.close();
    }

    public void exit(int status) {
        throw new SystemExitException("Exit code: " + status);
    }

    public void arraycopy(Object src, int srcPos,
                                 Object dest, int destPos,
                                 int length) {
        java.lang.System.arraycopy(src, srcPos, dest, destPos, length);
    }
}