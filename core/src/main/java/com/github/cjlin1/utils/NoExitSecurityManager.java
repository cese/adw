package com.github.cjlin1.utils;

import java.security.Permission;

/**
 *
 * Created by hmf on 21-07-2017.
 *
 * <code>
 * import com.github.cjlin1.utils.NoExitSecurityManager
 *
 * //Before running the external Command
 * val secManager = new NoExitSecurityManager
 * val old = JSystem.getSecurityManager
 * JSystem.setSecurityManager(secManager)
 * ...
 * JSystem.setSecurityManager(old)
 * <code/>
 *
 * @see https://stackoverflow.com/questions/309396/java-how-to-test-methods-that-call-system-exit
 * @see https://stackoverflow.com/questions/5549720/how-to-prevent-calls-to-system-exit-from-terminating-the-jvm
 */

class ExitException extends SecurityException
{
    public final int status;
    public ExitException(int status)
    {
        super("Bypass System.exit(" + status + ")");
        this.status = status;
    }
}

public class NoExitSecurityManager extends SecurityManager
{
    @Override
    public void checkPermission(Permission perm)
    {
        // allow anything.
    }
    @Override
    public void checkPermission(Permission perm, Object context)
    {
        // allow anything.
    }
    @Override
    public void checkExit(int status)
    {
        super.checkExit(status);
        throw new ExitException(status);
    }
}