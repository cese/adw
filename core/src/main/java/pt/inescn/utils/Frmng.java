package pt.inescn.utils;//                            INPUTS
//--------------------------------------------------------------------------
//Sgnl-   time signal (1xSz)
//FrmSz-  Frame size (1x1)  (number of samples)
//FrmStp  Frame step (1x1) (number of samples)
//wndw    Window  (1xFrmSz)

//                            OUTPUTS
//--------------------------------------------------------------------------
//OUT-  Matrix of frames ( FrmSz x floor[(Sz-FrmSz)/FrmStp] )

public class Frmng {
    public Frmng(){}
    public double [][] FrmngFunc(double [] Sgnl, int FrmSz, int FrmStp, double [] wndw){
        int NmbrFrms = (int) Math.floor( (Sgnl.length-FrmSz)/FrmStp );  //Pre-allocation works faster
        double [][] out = new double[NmbrFrms][FrmSz];

        for(int k=0,FrmCntr=0; k<(Sgnl.length-FrmSz); k+=FrmStp,FrmCntr++  )
            for(int n=k,i=0; n<=k+FrmSz-1; n++,i++)
                out[FrmCntr][i]=Sgnl[n]*wndw[i];

        return  out;
    }
}
