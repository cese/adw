package pt.inescn.utils;


/**
 * This class is used to return results of any kind and indicate
 * a possible failure. It is to be used in a right-side biased manner.
 * If the execution of an expression is successful a result is returned
 * as 'right' otherwise it is returned as 'left'.
 *
 * Trying to access a left or right that does not exist will raise a
 * runt-time exception.
 *
 * @param <L>
 * @param <R>
 */
public class JEither<L,R> {
    L left;
    R right;

    JEither(L l, R r) {
        left = l;
        right = r;
    }

    public static <L,R> JEither<L,R> Left(L value) {
        return new JLeft<L,R>(value);
    }

    public static <L,R> JEither<L,R> Right(R value) {
        return new JRight<L,R>(value);
    }

    public boolean isLeft() { return (left != null); }
    public L left() {
        if (isLeft())
            return left;
        else
            throw new RuntimeException("No left value available.");
    }

    public boolean isRight() { return (right != null); }
    public R right() {
        if (isRight())
            return right;
        else
            throw new RuntimeException("No right value available.");
    }

    @Override
    public String toString() {
        if (isRight())
            return "Right("+ right.toString() +")";
        if (isLeft())
            return "Left("+ left.toString() +")";
        else
            return "Either(?,?)";
    }
}
