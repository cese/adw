package pt.inescn.plot;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Stroke;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.*;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.*;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.Range;
import org.jfree.data.general.DefaultValueDataset;
import org.jfree.data.general.ValueDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.RefineryUtilities;
import org.jfree.util.ShapeUtilities;
import java.util.List;
import org.jfree.chart.axis.TickUnits;
import org.jfree.chart.axis.NumberTickUnit;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;



public class JPlotAlarmInterface extends ApplicationFrame {

    XYSeriesCollection dataset;
    ArrayList<Color> colors;
    ArrayList<Stroke> strokes;
    ArrayList<Shape> shapes;
    JFreeChart chart;
    JFrame Mnfrm;
    LegendTitle legend;
    Font font;
    int serieskey=0;
    boolean holdon=false;
    JPanel pnlAlrmLmp;
    DefaultValueDataset dataset1,dataset2,dataset3,dataset4;
    Label labelValue1, labelValue2, labelValue3, labelValue4;

    //Gauge ranges
    double range1=20;
    double range2=20;
    double range3=20;
    double range4=20;

    public class CustomMeterPlot extends MeterPlot {
        private static final long serialVersionUID = 8619280152355795883L;
        public CustomMeterPlot(ValueDataset dataset) {
            super(dataset);
        }

        @Override
        protected void drawTick(Graphics2D g2, Rectangle2D meterArea, double value) {
            drawTick(g2, meterArea, value, true);
        }
    }




    public JPlotAlarmInterface(final String title) {
        super(title);

        int d=10;
        int H=880;
        int W=1500;
        int ch=480;
        int cw=1200;
        int aw=240;
        int ah=ch;
        int mw=100;
        int mh=400;


        //Frame
        Mnfrm = new JFrame();
        Mnfrm.setLayout(null);
        Mnfrm.setBounds(10, 10, W, H);
        Mnfrm.getContentPane().setBackground(Color.black);

        //Signals container
        JPanel SgnlCntnr = new JPanel();
        SgnlCntnr.setBorder(BorderFactory.createLineBorder(Color.YELLOW));
        SgnlCntnr.setBounds(d, d, cw, ch);
        SgnlCntnr.setLayout(null);
        SgnlCntnr.setBackground(Color.BLACK);
        dataset = new XYSeriesCollection();
        colors = new ArrayList<Color>();
        strokes = new ArrayList<Stroke>();
        shapes = new ArrayList<Shape>();
        chart = createChart(dataset,title);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setBackground(Color.BLACK);
        chartPanel.setLayout(null);
        chartPanel.setBounds(0, 0, cw, ch);
        chartPanel.setBorder(BorderFactory.createLineBorder(Color.YELLOW));
        SgnlCntnr.add(chartPanel);
        chart.setBackgroundPaint(Color.BLACK);
        font("Arial",20);
        chart.getXYPlot().getRangeAxis().setLabelPaint(Color.yellow);
        chart.getXYPlot().getRangeAxis().setTickMarkPaint(Color.yellow);

        // Alarm Lamp container
        JPanel AlrmLmpCntnr = new JPanel();
        AlrmLmpCntnr.setBorder(BorderFactory.createLineBorder(Color.YELLOW));
        AlrmLmpCntnr.setBounds(2*d+cw, d, aw, ah);
        AlrmLmpCntnr.setLayout(null);
        AlrmLmpCntnr.setBackground(Color.BLACK);
        pnlAlrmLmp= new JPanel();
        pnlAlrmLmp.setBackground(Color.green);
        pnlAlrmLmp.setLayout(null);
        pnlAlrmLmp.setLocation(10,10);
        pnlAlrmLmp.setBounds(20,20,200,200);
        AlrmLmpCntnr.add(pnlAlrmLmp);


        //Metrics Container
        JPanel MtrcsCntnr = new JPanel();
        MtrcsCntnr.setBorder(BorderFactory.createLineBorder(Color.YELLOW));
        MtrcsCntnr.setBounds(2*d+cw, 2*d+ch,aw,H-ch-8*d);
        MtrcsCntnr.setLayout(null);
        MtrcsCntnr.setBackground(Color.BLACK);


        Label labelName1 = new java.awt.Label("Crest Factor",Label.LEFT);
        labelName1.setFont(new Font("Dialog", Font.BOLD, 16));
        labelName1.setForeground (Color.CYAN); labelName1.setBackground(Color.BLACK);
        labelName1.setBounds(10,10,130,20);

        labelValue1 = new java.awt.Label("10",Label.LEFT);
        labelValue1.setFont( new Font("Dialog", Font.BOLD, 16));
        labelValue1.setForeground (Color.CYAN); labelValue1.setBackground(Color.BLACK);
        labelValue1.setBounds(150,10,42,20);

        Label labelName2 = new java.awt.Label("Clearance Factor",Label.LEFT);
        labelName2.setFont(new Font("Dialog", Font.BOLD, 16));
        labelName2.setForeground (Color.CYAN); labelName1.setBackground(Color.BLACK);
        labelName2.setBounds(10,50,130,20);

        labelValue2 = new java.awt.Label("10",Label.LEFT);
        labelValue2.setFont( new Font("Dialog", Font.BOLD, 16));
        labelValue2.setForeground (Color.CYAN); labelValue1.setBackground(Color.BLACK);
        labelValue2.setBounds(150,50,42,20);

        Label labelName3 = new java.awt.Label("Impulse Factor",Label.LEFT);
        labelName3.setFont(new Font("Dialog", Font.BOLD, 16));
        labelName3.setForeground (Color.CYAN); labelName3.setBackground(Color.BLACK);
        labelName3.setBounds(10,90,130,20);

        labelValue3 = new java.awt.Label("10",Label.LEFT);
        labelValue3.setFont( new Font("Dialog", Font.BOLD, 16));
        labelValue3.setForeground (Color.CYAN); labelValue3.setBackground(Color.BLACK);
        labelValue3.setBounds(150,90,42,20);

        Label labelName4 = new java.awt.Label("Shape Factor",Label.LEFT);
        labelName4.setFont(new Font("Dialog", Font.BOLD, 16));
        labelName4.setForeground (Color.CYAN); labelName4.setBackground(Color.BLACK);
        labelName4.setBounds(10,130,130,20);

        labelValue4 = new java.awt.Label("10",Label.LEFT);
        labelValue4.setFont( new Font("Dialog", Font.BOLD, 16));
        labelValue4.setForeground (Color.CYAN); labelValue4.setBackground(Color.BLACK);
        labelValue4.setBounds(150,130,42,20);


        MtrcsCntnr.add(labelName1); MtrcsCntnr.add(labelValue1);
        MtrcsCntnr.add(labelName2); MtrcsCntnr.add(labelValue2);
        MtrcsCntnr.add(labelName3); MtrcsCntnr.add(labelValue3);
        MtrcsCntnr.add(labelName4); MtrcsCntnr.add(labelValue4);

        //Gauge container
        JPanel GgCntnr = new JPanel(new BorderLayout());
        GgCntnr.setBorder(BorderFactory.createLineBorder(Color.YELLOW));
        GgCntnr.setBounds(d, 2*d+ch, cw,H-ch-8*d);
        GgCntnr.setLayout(null);
        GgCntnr.setBackground(Color.BLACK);

        // Gauge 1
        double L11=range1*0.5;
        double L12=range1*0.75;
        dataset1= new DefaultValueDataset(0.5);
        CustomMeterPlot plot1 = new CustomMeterPlot(dataset1);
        plot1.addInterval(new MeterInterval("Low", new Range(0.0, L11), Color.GREEN, new BasicStroke(1.0f), null));
        plot1.addInterval(new MeterInterval("Middle", new Range(L11, L12), Color.ORANGE, new BasicStroke(2.0f), null));
        plot1.addInterval(new MeterInterval("High", new Range(L12, range1), Color.RED, new BasicStroke(2.0f), null));
        plot1.setUnits("");  plot1.setTickLabelsVisible(true);
        plot1.setDialShape(DialShape.CHORD);  plot1.setValuePaint(Color.BLUE);
        plot1.setTickLabelsVisible(true);
        plot1.setRange(new Range(0, range1));
        plot1.setTickLabelPaint(Color.ORANGE);
        JFreeChart chart1 = new JFreeChart("Crest Factor", JFreeChart.DEFAULT_TITLE_FONT, plot1, false);
        chart1.setBackgroundPaint(Color.BLACK);
        chart1.getTitle().setPaint(Color.yellow);
        JPanel  tmp1= new ChartPanel(chart1);
        tmp1.setBounds(2,20,280,280);
        GgCntnr.add(tmp1);



        // Gauge 2
        double L21=range2*0.5;
        double L22=range2*0.75;
        dataset2 = new DefaultValueDataset(0.5);
        CustomMeterPlot plot2 = new CustomMeterPlot(dataset2);
        plot2.setDialOutlinePaint(Color.white);
        plot2.addInterval(new MeterInterval("Low", new Range(0.0, L21), Color.GREEN, new BasicStroke(1.0f), null));
        plot2.addInterval(new MeterInterval("Middle", new Range(L21, L22), Color.ORANGE, new BasicStroke(1.0f), null));
        plot2.addInterval(new MeterInterval("High", new Range(L22, range2), Color.RED, new BasicStroke(0.1f), null));
        plot2.setUnits("");
        plot2.setTickLabelsVisible(true);
        plot2.setDialShape(DialShape.CHORD);
        plot2.setValuePaint(Color.yellow);
        plot2.setTickLabelsVisible(true);
        plot2.setRange(new Range(0, range2));
        plot2.setTickLabelPaint(Color.ORANGE);
        JFreeChart chart2 = new JFreeChart("Clearance Factor", JFreeChart.DEFAULT_TITLE_FONT, plot2, false);
        chart2.setBackgroundPaint(Color.BLACK);
        chart2.getTitle().setPaint(Color.yellow);
        JPanel  tmp2= new ChartPanel(chart2);
        tmp2.setBackground(Color.BLACK);
        tmp2.setBounds(300,20,280,280);
        GgCntnr.add(tmp2);

        // Gauge 3
        double L31=range2*0.5;
        double L32=range2*0.75;
        dataset3 = new DefaultValueDataset(0.5);
        CustomMeterPlot plot3 = new CustomMeterPlot(dataset3);
        plot3.setDialOutlinePaint(Color.white);
        plot3.addInterval(new MeterInterval("Low", new Range(0.0, L31), Color.GREEN, new BasicStroke(1.0f), null));
        plot3.addInterval(new MeterInterval("Middle", new Range(L31, L32), Color.ORANGE, new BasicStroke(1.0f), null));
        plot3.addInterval(new MeterInterval("High", new Range(L32, range3), Color.RED, new BasicStroke(0.1f), null));
        plot3.setUnits("");
        plot3.setTickLabelsVisible(true);
        plot3.setDialShape(DialShape.CHORD);
        plot3.setValuePaint(Color.yellow);
        plot3.setTickLabelsVisible(true);
        plot3.setRange(new Range(0, range3));
        plot3.setTickLabelPaint(Color.ORANGE);
        JFreeChart chart3 = new JFreeChart("Impulse Factor", JFreeChart.DEFAULT_TITLE_FONT, plot3, false);
        chart3.setBackgroundPaint(Color.BLACK);
        chart3.getTitle().setPaint(Color.yellow);
        JPanel  tmp3= new ChartPanel(chart3);
        tmp3.setBackground(Color.BLACK);
        tmp3.setBounds(600,20,280,280);
        GgCntnr.add(tmp3);

        // Gauge 4
        double L41=range2*0.5;
        double L42=range2*0.75;
        dataset4 = new DefaultValueDataset(0.5);
        CustomMeterPlot plot4 = new CustomMeterPlot(dataset4);
        plot4.setDialOutlinePaint(Color.white);
        plot4.addInterval(new MeterInterval("Low", new Range(0.0, L41), Color.GREEN, new BasicStroke(1.0f), null));
        plot4.addInterval(new MeterInterval("Middle", new Range(L41, L42), Color.ORANGE, new BasicStroke(1.0f), null));
        plot4.addInterval(new MeterInterval("High", new Range(L42, range4), Color.RED, new BasicStroke(0.1f), null));
        plot4.setUnits("");
        plot4.setTickLabelsVisible(true);
        plot4.setDialShape(DialShape.CHORD);
        plot4.setValuePaint(Color.yellow);
        plot4.setTickLabelsVisible(true);
        plot4.setRange(new Range(0, range4));
        plot4.setTickLabelPaint(Color.ORANGE);
        JFreeChart chart4 = new JFreeChart("Shape Factor", JFreeChart.DEFAULT_TITLE_FONT, plot4, false);
        chart4.setBackgroundPaint(Color.BLACK);
        chart4.getTitle().setPaint(Color.yellow);
        JPanel  tmp4= new ChartPanel(chart4);
        tmp4.setBackground(Color.BLACK);
        tmp4.setBounds(900,20,280,280);
        GgCntnr.add(tmp4);


        Mnfrm.add(SgnlCntnr);
        Mnfrm.add(AlrmLmpCntnr);
        Mnfrm.add(MtrcsCntnr);
        Mnfrm.add(GgCntnr);

        // Set visible
        Mnfrm.setVisible(true);
        Mnfrm.setLocationRelativeTo(null); //centra a janela

    }


    public void updatePlot(double[] x, double[] y, int serieIndex){
        for (int i = 0; i < x.length; i++)
            dataset.getSeries(serieIndex).update(x[i],y[i]);
    }


    public void setState(int St){
        switch (St){
            case 0:
                pnlAlrmLmp.setBackground(Color.black);break;
            case 1:
                pnlAlrmLmp.setBackground(Color.green);break;
            case 2:
                pnlAlrmLmp.setBackground(Color.orange);break;
            case 3:
                pnlAlrmLmp.setBackground(Color.red);break;
        }
    }


    public void updateGaugesValue(double val1, double val2, double val3, double val4){

        if (val1<range1)
            dataset1.setValue(val1);
        else
            dataset1.setValue(range1);

        if (val2<range2)
            dataset2.setValue(val2);
        else
            dataset2.setValue(range2);

        if (val3<range3)
            dataset3.setValue(val3);
        else
            dataset3.setValue(range3);

        
        if (val4<range4)
            dataset4.setValue(val4);
        else
            dataset4.setValue(range4);

    }

    public void updateDisplayValues(double val1, double val2, double val3, double val4){
        labelValue1.setText(Double.toString(val1));
        labelValue2.setText(Double.toString(val2));
        labelValue3.setText(Double.toString(val3));
        labelValue4.setText(Double.toString(val4));
    }


    public void plot(double[] x, double[] y, String spec, float lineWidth, String title) {
        serieskey++;
        final XYSeries series = new XYSeries(serieskey);

        if (!holdon){
            dataset.removeAllSeries();
        }

        // if x is index vector
        if (x.length!=y.length){
            x = new double[y.length];
            for(int i=1; i<=x.length ; i++)
                x[i-1]=i;
        }
        for (int i = 0; i < x.length; i++)
            series.add(x[i],y[i]);
        dataset.addSeries(series);

        // Line style
        FindColor(spec,lineWidth);

        // Add customization options to chart
        XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        for (int i = 0; i < colors.size(); i++) {
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, true);
            renderer.setSeriesStroke(i, strokes.get(i));
            renderer.setSeriesPaint(i, colors.get(i));
            renderer.setSeriesShape(i, shapes.get(i));
        }
        plot.setRenderer(renderer);

    }

    public void figure(){
        this.pack();
        RefineryUtilities.centerFrameOnScreen(this);
        this.setVisible(true);
    }


    private JFreeChart createChart(final XYDataset dataset, String title) {

        // create the chart...
        final JFreeChart chart = ChartFactory.createXYLineChart(
                title,      // chart title
                "X",                      // x axis label
                "Y",                      // y axis label
                dataset,                  // data
                PlotOrientation.VERTICAL,
                true,                     // include legend
                true,                     // tooltips
                false                     // urls
        );

        // Add customization options to chart
        XYPlot plot = chart.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        for (int i = 0; i < colors.size(); i++) {
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, true);
        }
        plot.setRenderer(renderer);

        ((NumberAxis)plot.getDomainAxis()).setAutoRangeIncludesZero(false);
        ((NumberAxis)plot.getRangeAxis()).setAutoRangeIncludesZero(false);
        plot.getRangeAxis().setTickMarkPaint(Color.yellow);
        plot.setBackgroundPaint(Color.BLACK);
        legend = chart.getLegend();
        chart.removeLegend();
        this.chart = chart;


        return chart;

    }

    public void FindColor(String spec, float lineWidth) {
        float dash[] = {5.0f};
        float dot[] = {lineWidth};


        Stroke stroke = new BasicStroke(lineWidth); // Default stroke is line
        if (spec.contains("-"))
            stroke = new BasicStroke(lineWidth);
        else if (spec.contains(":"))
            stroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
        else if (spec.contains("."))
            stroke = new BasicStroke(lineWidth, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 2.0f, dot, 0.0f);
        //else if (spec.contains("*"))
            //stroke = null

        strokes.add(stroke);

        Color color = Color.RED; // Default color is red
        if (spec.contains("y"))
            color = Color.YELLOW;
        else if (spec.contains("m"))
            color = Color.MAGENTA;
        else if (spec.contains("c"))
            color = Color.CYAN;
        else if (spec.contains("r"))
            color = Color.RED;
        else if (spec.contains("g"))
            color = Color.GREEN;
        else if (spec.contains("b"))
            color = Color.BLUE;
        else if (spec.contains("k"))
            color = Color.BLACK;
        colors.add(color);


        Shape shape = new Rectangle(0,0); // Default none
        if ( spec.contains("Q") )
            shape = new Rectangle2D.Double(-2.0, -2.0, 4.0, 4.0);
        else if (spec.contains("C"))
            shape = new Ellipse2D.Double(0,0,5,5);
        else if (spec.contains("D"))
            shape =ShapeUtilities.createDiamond(5);
        else if (spec.contains("L"))
            shape = new Rectangle2D.Double(-0.25, -4.0, 0.5, 8.0);

        shapes.add(shape);

        //markers

    }


    public void CheckExists() {
        if (chart == null) {
            throw new IllegalArgumentException("First plot something in the chart before you modify it.");
        }
    }


    public void grid(String xAxis, String yAxis) {
        CheckExists();
        if (xAxis.equalsIgnoreCase("on")){
            chart.getXYPlot().setDomainGridlinesVisible(true);
            chart.getXYPlot().setDomainMinorGridlinesVisible(true);
            chart.getXYPlot().setDomainGridlinePaint(Color.GRAY);
        } else {
            chart.getXYPlot().setDomainGridlinesVisible(false);
            chart.getXYPlot().setDomainMinorGridlinesVisible(false);
        }

        if (yAxis.equalsIgnoreCase("on")){
            chart.getXYPlot().setRangeGridlinesVisible(true);
            chart.getXYPlot().setRangeMinorGridlinesVisible(true);
            chart.getXYPlot().setRangeGridlinePaint(Color.GRAY);
        } else {
            chart.getXYPlot().setRangeGridlinesVisible(false);
            chart.getXYPlot().setRangeMinorGridlinesVisible(false);
        }
    }


    public void font(String name, int fontSize) {
        CheckExists();
        font = new Font(name, Font.PLAIN, fontSize);
        chart.getTitle().setFont(font);
        chart.getXYPlot().getDomainAxis().setLabelFont(font);
        chart.getXYPlot().getDomainAxis().setTickLabelFont(font);
        chart.getXYPlot().getDomainAxis().setLabelPaint(Color.yellow);

        chart.getXYPlot().getRangeAxis().setLabelFont(font);
        chart.getXYPlot().getRangeAxis().setTickLabelFont(font);
        chart.getXYPlot().getRangeAxis().setLabelPaint(Color.yellow);
        legend.setItemFont(font);
    }

    public void title(String title) { CheckExists(); chart.setTitle(title); }
    public void xlim(double l, double u) { CheckExists(); chart.getXYPlot().getDomainAxis().setRange(l, u); }
    public void ylim(double l, double u) { CheckExists(); chart.getXYPlot().getRangeAxis().setRange(l, u); }
    public void xlabel(String label) { CheckExists();chart.getXYPlot().getDomainAxis().setLabel(label); }
    public void ylabel(String label) { CheckExists();chart.getXYPlot().getRangeAxis().setLabel(label); }
    public void clear () { dataset.removeAllSeries();}
    public void holdon() { holdon=true;}
    public void holdoff(){ holdon=false;}

    // Novo codigo
    //==================================================================================================================
    public void setX_visible(boolean flag) { chart.getXYPlot().getDomainAxis().setVisible(flag); }
    public void setY_visible(boolean flag) { chart.getXYPlot().getRangeAxis().setVisible(flag); }
    public void text(String label, double x, double y, int fsize, String font  ) {
        final XYTextAnnotation text= new XYTextAnnotation(label,x,y);
        text.setFont(new Font(font, Font.BOLD, fsize ));
        chart.getXYPlot().addAnnotation(text);
    }

    public void setY_Unit(double unit){
        TickUnits units = new TickUnits();
        units.add(new NumberTickUnit(unit));
        chart.getXYPlot().getRangeAxis().setStandardTickUnits(units);
    }


    //==================================================================================================================

    public void legend(String position) {
        CheckExists();
        legend.setItemFont(font);
        legend.setBackgroundPaint(Color.WHITE);
        legend.setFrame(new BlockBorder(Color.BLACK));
        if (position.toLowerCase().equals("northoutside")) {
            legend.setPosition(RectangleEdge.TOP);
            chart.addLegend(legend);
        } else if (position.toLowerCase().equals("eastoutside")) {
            legend.setPosition(RectangleEdge.RIGHT);
            chart.addLegend(legend);
        } else if (position.toLowerCase().equals("southoutside")) {
            legend.setPosition(RectangleEdge.BOTTOM);
            chart.addLegend(legend);
        } else if (position.toLowerCase().equals("westoutside")) {
            legend.setPosition(RectangleEdge.LEFT);
            chart.addLegend(legend);
        } else if (position.toLowerCase().equals("north")) {
            legend.setPosition(RectangleEdge.TOP);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.50,0.98,legend, RectangleAnchor.TOP);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("northeast")) {
            legend.setPosition(RectangleEdge.TOP);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.98,0.98,legend, RectangleAnchor.TOP_RIGHT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("east")) {
            legend.setPosition(RectangleEdge.RIGHT);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.98,0.50,legend, RectangleAnchor.RIGHT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("southeast")) {
            legend.setPosition(RectangleEdge.BOTTOM);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.98,0.02,legend, RectangleAnchor.BOTTOM_RIGHT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("south")) {
            legend.setPosition(RectangleEdge.BOTTOM);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.50,0.02,legend, RectangleAnchor.BOTTOM);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("southwest")) {
            legend.setPosition(RectangleEdge.BOTTOM);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.02,0.02,legend, RectangleAnchor.BOTTOM_LEFT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("west")) {
            legend.setPosition(RectangleEdge.LEFT);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.02,0.50,legend, RectangleAnchor.LEFT);
            chart.getXYPlot().addAnnotation(ta);
        } else if (position.toLowerCase().equals("northwest")) {
            legend.setPosition(RectangleEdge.TOP);
            XYTitleAnnotation ta = new XYTitleAnnotation(0.02,0.98,legend, RectangleAnchor.TOP_LEFT);
            chart.getXYPlot().addAnnotation(ta);
        }
    }



    public void wiggle(double [] x, double [][] yy,double [] xp, int bufferSz){

        //final JPlot fig = new JPlot("h");
        this.grid("on","on");                 // grid on;
        this.font("Arial",18);             // Set font
        this.ylim(0.1,4-0.1); this.setY_Unit(1);
        this.xlim(0,bufferSz);
        this.ylabel("Axis X            Axis Y              Axis Z");
        this.xlabel("Samples"); this.holdon();

        double [] yp= {0,4};
        double [] yfinal= new double[yy[0].length];
        this.clear();

        for(int j=0; j < yy.length ; j++){

            //compute mean and max
            double cum=0;
            for(int k=0 ; k < yy[0].length ; k++)
                cum+= yy[j][k];

            // Remove mean
            for(int k=0; k < yy[0].length; k++ )
                yfinal[k]= yy[j][k] - cum/yy[0].length ;

            // Normalize
            double max=0;
            for(int k=0; k <yy[0].length; k++)
                if (Math.abs(yfinal[k]) > max)
                    max = Math.abs(yfinal[k]);

            for(int k=0; k < yy[0].length; k++ )
                yfinal[k] = yfinal[k] / (2*max) + j + 1;

            this.plot(x, yfinal ,"-y", 1.0f, "h");
        }
        this.plot(xp,yp,"-r", 50.0f, "h");
    }

    // Method for getting the maximum value
    public static double getMax( double [] inputArray){
        double maxValue = inputArray[0];
        for(int i=1;i < inputArray.length;i++){
            if(inputArray[i] > maxValue){
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }

    // Method for getting the minimum value
    public static double getMin ( double [] inputArray){
        double minValue = inputArray[0];
        for(int i=1;i<inputArray.length;i++){
            if(inputArray[i] < minValue){
                minValue = inputArray[i];
            }
        }
        return minValue;
    }

    public static double calculateSD(double numArray[]) {
        double sum = 0.0, standardDeviation = 0.0;
        int length = numArray.length;

        for(double num : numArray) { sum += num; }

        double mean = sum/length;

        for(double num: numArray) { standardDeviation += Math.pow(num - mean, 2); }

        return Math.sqrt(standardDeviation/length);
    }


    public static double calculateMeanAbs(double numArray[]) {
        double sum = 0.0, standardDeviation = 0.0;

        for(double num : numArray) {
            sum += Math.abs(num);
        }

        return sum/numArray.length;
    }


    public static double SumSquared(double numArray[]){
        double Sum=0;
        for(int i=0; i< numArray.length ; i++){
            Sum+=Math.sqrt(Math.abs(numArray[i]));
        }
        return Sum/numArray.length;
    }

    public static double SumAbs(double numArray[]){
        double Sum=0;
        for(int i=0; i< numArray.length ; i++){
            Sum+=Math.abs(numArray[i]);
        }
        return Sum/numArray.length;
    }


    public  double meanCrestFactor(double [][] vals){
        double CF=0;
        for(int i=0; i< vals.length ; i++){
            CF+=0.5*(getMax(vals[i])-getMin(vals[i]))/calculateSD(vals[i])/3;
        }
        return CF;
    }


    public  double meanImpulseFactor(double [][] vals){
        double IF=0;
        for(int i=0; i< vals.length ; i++){
            IF+=0.5*(getMax(vals[i])-getMin(vals[i]))/calculateMeanAbs(vals[i])/3;
        }
        return IF;
    }

    public double meanClearanceFactor(double [][] vals){
        double ClF=0;
        for(int i=0; i< vals.length ; i++){
            ClF+=0.5*(getMax(vals[i])-getMin(vals[i]))/Math.pow(SumSquared(vals[i]),2)/3;
        }
        return ClF;
    }

    public double meanShapeFactor(double [][] vals){
        double SF=0;
        for(int i=0; i< vals.length ; i++){
            SF+=calculateSD(vals[i])/SumAbs(vals[i])/3;
        }
        return SF;
    }


    public static  void main(final String[] args) {

        // Create some sample data
        double [] ay1 = new double[1]; double [] ay2 = new double[1]; double [] ay3 = new double[1];
        List<Double> y1 = new LinkedList<Double>();
        List<Double> y2 = new LinkedList<Double>();
        List<Double> y3 = new LinkedList<Double>();

        int bufferSz=400;
        double[] x = new double[bufferSz];

        final JPlotAlarmInterface figw = new JPlotAlarmInterface("");
        double [][] yy = new double[3][];
        figw.ylim(-10,10); figw.holdon();


        // Utilização em tempo real
        //---------------------------------------------------------------------
        double a=10; int av = 0; double pt=-10; int blink=0;
        for(int i = 1; i < 20000; i++){

            if(i==550){ a=20; av=1;}

            if ( i < bufferSz ) {
                x[i] = i;
                y1.add( Math.random()*a - 0 ); y2.add( Math.random()*a - 10); y3.add( Math.random()*a - 10);
            }
            else {
                y1.remove( 0 );  y1.add( Math.random()*a - 0 );
                y2.remove( 0 );  y2.add( Math.random()*a - 10 );
                y3.remove( 0 );  y3.add( Math.random()*a - 10 );
            }

            if( (i % 1)==0 ){

                ay1 =  new double[y1.size()];  ay2 =  new double[y2.size()]; ay3 =  new double[y3.size()];

                for(int j=0; j < y1.size() ; j++ )
                    ay1[j]=y1.get(j);

                for(int j=0; j < y2.size() ; j++ )
                    ay2[j]=y2.get(j);

                for(int j=0; j < y3.size() ; j++ )
                    ay3[j]=y3.get(j);

                yy[0]=ay1;  yy[1]=ay2; yy[2]=ay3;

                if(av==1) {
                    if (pt == -10)
                        pt = bufferSz;
                    else
                        pt -= 1;

                    if (pt == 0) {
                        pt = -10;
                        av = 0;
                    }
                }

                double [] xp={pt,pt};
                figw.wiggle(x,yy,xp,bufferSz);
                if (av==0)
                    figw.setState(1);
                else {
                    if ( (i%5)==0 && blink==0) {
                        figw.setState(3);
                        blink = 1;
                    }
                    else {
                        figw.setState(0);
                        blink=0;
                    }
                }

                figw.updateGaugesValue(figw.meanCrestFactor(yy), figw.meanClearanceFactor(yy),figw.meanImpulseFactor(yy),figw.meanShapeFactor(yy));
                figw.updateDisplayValues(figw.meanCrestFactor(yy), figw.meanClearanceFactor(yy),figw.meanImpulseFactor(yy),figw.meanShapeFactor(yy));

                try {
                    Thread.currentThread().sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}