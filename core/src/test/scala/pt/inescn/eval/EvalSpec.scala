/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.eval

import org.scalatest._

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.eval.EvalSpec"
  *
  * Created by hmf on 05-06-2017.
  */
class EvalSpec extends FlatSpec with Matchers {

  "Boolean Metrics" should "be correctly calculated" in {

    val labels  = List(true,true,false,true,false,true,false,true,false,true)
    val results = List(false,true,false,true,false,true,false,true,true,true)
    val metrics = BinaryClassificationMetrics(labels, results)

    val tp = metrics.tp
    tp shouldBe 5
    val tn = metrics.tn
    tn shouldBe 3
    val fp = metrics.fp
    fp shouldBe 1
    val fn = metrics.fn
    fn shouldBe 1
    val recall = metrics.recall
    recall.value shouldBe 5.0/6
    val precision = metrics.precision
    precision.value shouldBe 5.0/6
    val accuracy = metrics.accuracy
    accuracy.value shouldBe 8.0/10
    val mcc = metrics.mcc
    mcc.value shouldBe 14.0/24
    val f1 = metrics.F1
    f1.beta shouldBe FBeta(1.0)
    f1.value shouldBe 5.0/6
    val f2 = metrics.F2
    f2.beta shouldBe FBeta(2.0)
    f2.value shouldBe 5.0/6
    val f0_5 = metrics.F0_5
    f0_5.beta shouldBe FBeta(0.5)
    f0_5.value shouldBe 5.0/6
  }

}
