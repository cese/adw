/*******************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  ******************************************************************************/
package pt.inescn.detector

import better.files.Dsl.cwd
import com.github.tototoshi.csv.DefaultCSVFormat
import pt.inescn.etl.stream.DataFiles
import org.scalatest._
import pt.inescn.search.stream.Pipes.{compile => scompile, _}
import pt.inescn.etl.stream.Load._
import pt.inescn.search.stream.Pipes
import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.TestTasks._
import pt.inescn.search.stream.UtilTasks._
import pt.inescn.search.stream.TasksFCC._
import pt.inescn.utils.{ADWError, Utils}
import pt.inescn.app.{Utils => AUtils}

// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._


// import scala.collection.JavaConverters._

object TestTask {

  type O = Row                                  // Output of the experimental pipe
  type Acc = (Either[ADWError,O], Executing)    // Select the best pipe


  /**
    * Task builder. Creates the task that will trigger the
    * execution of the experiments (get the lazy output)
    *
    * @param alpha importance assigned to accuracy
    *              (1-alpha) is assigned to the false alarm rate
    * @return the selected experiment
    */
  def apply(alpha: Double): Agg[O,Acc] = {

    // TODO: how to automate this?
    val tsk: Aggregator[O,Acc] = new Aggregator[O,Acc] {
      //override type Params = String
      override def name: String = "testTask"

      override def params: Params = Map("alpha" -> alpha)

      /**
        * Initial value that will be accumulated and returned.
        * @return
        */
      override def zero: Acc = (Left(ADWError("zero")), Executing())

      /**
        * Selection criteria of the experiment. We want to
        * maximize the accuracy and reduce the fal alarm rates.
        *
        * @param acc accuracy
        * @param far false alarm rate
        * @return score of criteria
        */
      def criteria(acc:Double, far:Double): Double = (alpha * acc) - (1.0 - alpha)*far

      /**
        * It collects the results of the possibly lazy/asynchronous execution of a source.
        * Make use you use scala's `blocking` or use a separate thread if you want
        * to do slow I/O operations.
        *
        * @param acc initial aggregation value set as an [[pt.inescn.utils.ADWError]]
        * @param in output generated from the preceding step
        * @param trk execution tracking, used to log the execution
        * @return output passed onto the next task
        */
      override def collect(acc: Acc, in: Either[ADWError,O], ta:Long, trk: Executing): Acc = {
        (acc, in) match {
          case ((Right(ra),exea),Right(i)) =>
            // Get the accumulated accuracy and false alarm rate
            val accAcc: Either[ADWError, Double] = unpackErr[Double](ra("acc"))
            val accFar = unpackErr[Double](ra("far"))
            // Get a calculated accuracy and false alarm rate from an experiment
            val iAcc = unpackErr[Double](i("acc"))
            val iFar = unpackErr[Double](i("far"))
            // Calculate the score using a common criteria
            val tmp: Either[ADWError, (Executing,O)] = for {
              aAcc <- accAcc
              aFar <- accFar
              iacc <- iAcc
              ifar <- iFar
              aCriteria = criteria(aAcc,aFar)
              iCriteria = criteria(iacc,ifar)
            } yield {
              // We want to maximize this criteria
              if (aCriteria > iCriteria) (exea,ra) else (trk,i)
            }
            // Report an error if it occurred otherwise
            // return information on the experiment
            val rr: Acc = tmp match {
              case Left(e) =>
                (Left(e),trk)
              case Right((e,o)) =>
                (Right(o),e)
            }
            rr
          case ((Right(_),_),_) =>
            // Experiment error, lets ignore it
            acc
          case (_,Right(i)) =>
            // First entry, or an error occurred
            // when calculating the criteria
            (Right(i), trk)
          case ((Left(_),_), Left(_)) =>
            // First entry and an error occurred
            // when calculating the criteria
            acc
        }
      } // collect
    }
    Agg(tsk)
  }
}




object MyFormat extends DefaultCSVFormat {
  override val delimiter = ',' // ';'
}

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.detector.FCCAnomalyDetectorSpec"
  * sbt "testOnly pt.inescn.detector.FCCAnomalyDetectorSpec -- -oF"
  *
  * https://stackoverflow.com/questions/41182727/measure-memory-usage-of-code-unit
  * https://github.com/jbellis/jamm
  */
class FCCAnomalyDetectorSpec extends FlatSpec with Matchers {


    "Anomaly detector" should "single pipe detector" in {

    // Open file
    val dir = cwd / ".." / "data/inegi/anomaly_detection_data"
    val matchNames = "*.csv"

    val files = DataFiles.parseFileNames(dir, matchNames, AUtils.parseFileName).toList
    assert( files.size == 1 )
    val parserOk = files.filter( _.isRight ).map(_.right.get)
    assert( parserOk.size == 1 )
    val (filesp,features,units) = parserOk.unzip3

      // Gets 2048 lines
      val offset = 0.000195
      val FFTSz = 256
      val Fs = 5100
      val NmbFltrs = 5
      val MaxFrq = 2550.0
      val MinFrq = 10.0
      val NmbCpCoe = 5
      val tmp3 = DataFiles.generateTimeStampOffsets(offset, filesp)
      val FrmSz2 = 128
      val FrmStp2 = 1

    val NmbrOfSmpls = 4048 //Number of samples taken from the file
      val s3 = loadExtraINEGICSVs(MyFormat, filesp.map(_.toString()), tmp3.toList) ->:
      renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly")  ->: // Rename column
      toDouble("t", "X", "Y", "Z", "isanomaly" )  ->:                 // convert types
      //plotStreamStep(500,"X", "Z", "Y" )->:
      window(FFTSz,64) ->:                                            // vectors of frames 4x1x16(x,y,z,IsAnomaly)
      modeStep("mode","isanomaly") ->:                                // TODO: comment (isanomalymode)
      lfccStep("lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") ->:  //vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc") ->:             // 1x15 (lfcc)
      //plotVecStreamStep(500,15,"lfcc")->:
      window(FrmSz2,FrmStp2) ->:                                      // vectors of features 64x15(lfcc)
      medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc") ->:               // 1x15 (lfccmedian,damagemedian)
      modeStep("mode","isanomalymode")  ->:                           // (isanomalymode) // TODO: ??, repeated?
      anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian") ->: // TODO: ??
      anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" ) //->:   // TODO comment + plot
      //plotStreamStep( 500, "isanomalymodemode", "det","acc","far")

      // compile new pipes
    val c3 = scompile(s3)
    val c3l = c3.toList
    c3l.size shouldBe 1
    c3l.head should be ('right)

      // Execute the pipes
      val r3 = execDebug(c3)(Frame(("",List())))


      // Get the results (not executed yet)
      val result: Frame = r3.head._1.right.get

    // This will cause an OOM exception - stream too large
    // The question is why. We have estimates that the 3020 long stream has
    // Rows of 3225288 bytes (using JAMM) resulting in a total of 9.2 GBytes
    // (See below). Increasing the maximum to 64G did not help. Setting the start
    // to 20GBytes allowed partial success.
    //println(result.iterable.toIndexedSeq)

    // This works and does not seems to require the foreseen 9.2 GBytes
    // --> val tmp = result.iterable.toIndexedSeq
    // This does not collect the data
    //val tmp = result.iterable.toArray
    // --> println(s"tmp(0) = ${tmp(0)}")

    // This fails with a java.io.EOFException in ObjectInputStream.java:2960
    // (peekByte). So why does this fail. In Java each String character holds
    // a max of Integer.MAX_VALUE characters (2 bytes) (implemented as an
    // Array). That means we can only get a 2GByte string. If we assumed one
    // character per Row byte we cannot fit 9.2 GBytes here. Moreover,
    // converting a Row to string will not map one byte per character (ex.
    // Double values are show wih several decimal places) In addition to this
    // Java uses StringBuffer internally to accumulate the results and so
    // requires more memory (double?).
    //val stmp = tmp.toString
    //println(stmp)

    // This will work if each Row when converted to a String has less than
    // 2G characters
    //tmp.foreach(println)
    // These also work with no OOM
    //val len = tmp.size
    //println(s"tmp.size = ${tmp.size}")
    //println(s"tmp(tmp.size - 1)= ${tmp(len-1)}")

    // This also works fine because we just iterate (no memory allocation)
    // result.iterable.foreach(println)
    // Here we count the number of Rows nd get 3070 records
    // println(s"nu. records = ${result.iterable.count(_ => true)}")

    // Each Row occupies 3225288 bytes
    // That's 3149,6953125 KBytes = 3,075874329 MBytes
    // For a stream with 3070 Rows we have 9442,934188843 MBytes = 9,221615419 GBytes

    /*
      For the next code you need to activate the JAMM library and agent
      Strange result. JAMM reports the Index sequence with 0.1 Giga. But
      if we measure each row amd add those sizes we get about 9.GBytes
      tmp size 0.10462866723537445
      tmp(0) size 3215088
      tmp size 9.192451983690262
      sum 9.221615418791771
     */
    /*import org.github.jamm.MemoryMeter
    val mm = new MemoryMeter()
    val giga = 1024*1024*1024.0
    //result.iterable.foreach( o => println(s"${mm.measureDeep(o)}") )
    println(s"tmp size ${mm.measureDeep(tmp) / giga}")
    val rowSize = mm.measureDeep(tmp(0))
    println(s"tmp(0) size $rowSize")
    println(s"tmp size ${(rowSize*tmp.size)/giga}")
    val sm = tmp.map(mm.measureDeep).sum
    println(s"sum ${sm/giga}")*/

    // Collect some of the results (executes now)
    val k = 3000 // Max is 3000
    val read3 = result.project("det","isanomalymodemode","acc","far").iterable.toIterator.take(k+1).toIndexedSeq

    // The accuracy varies substantially through the time-series (stream)
    // For this model it is a global evaluation (includes all past accuracy values)
    val r2 = read3(k)

    // prediction
    val r2_det: Either[String, Double] = unpack[Double](r2("det"))
    r2_det should be ('right)
    r2_det.right.get should be (1.0)

    // label
    val r2_isanomalymodemode: Either[String, Double] = unpack[Double](r2("isanomalymodemode"))
    r2_isanomalymodemode should be ('right)
    r2_isanomalymodemode.right.get should be (1.0)

    // accuracy
    val r2_acc: Either[String, Double] = unpack[Double](r2("acc"))
    r2_acc should be ('right)
    r2_acc.right.get should be >= 0.9

    // false alarm rate
    val r2_far: Either[String, Double] = unpack[Double](r2("far"))
    r2_far should be ('right)
    r2_far.right.get should be <= 0.15  // was 0.091
  }


  it should "multiple pipe detector" in {

    // Open file and read features and factors
    val dir = cwd / ".." / "data/inegi/anomaly_detection_data"
    val matchNames = "*.csv"
    val files = DataFiles.parseFileNames(dir, matchNames, AUtils.parseFileName).toList
    assert( files.size == 1 )
    val parserOk = files.filter( _.isRight ).map(_.right.get)
    assert( parserOk.size == 1 )
    val (filesp,features,units) = parserOk.unzip3


    // Gets 2048 lines
    val offset = 0.000195
    val FFTSz = 256
    val Fs = 5100.0
    //val NmbFltrs = 5
    val MaxFrq = 2550.0
    val MinFrq = 10.0
    val NmbCpCoe = 5
    val tmp3 = DataFiles.generateTimeStampOffsets(offset, filesp)
    val FrmSz2 = 128
    val FrmStp2 = 1

    import  pt.inescn.samplers.stream.Ops._
    import pt.inescn.search.stream.Tasks._

    // Multiple parameters used
    // Search space for first windows
    val winFFTSz = Seq(FFTSz)
    val winStride = Seq(32)
    val winPs = winFFTSz ## winStride
    val opsWin = T( window1 _,  winPs)

    // Search space for LFCC parameters
    val lfccFs = Seq(Fs)
    val lfccNmbFltrs = 1 to 1
    val lfccMaxFrq = Seq(MaxFrq)
    val lfccMinFrq = Seq(MinFrq)
    val lfccNmbCpCoe = Seq(NmbCpCoe)
    val lfccCols = Seq(ColumnNames(Seq("X","Y","Z")))
    val lfccExtend = Seq(ColumnName("lfcc"))

    // The orginal (non-flattened) search space has the correct types
    val original = lfccFs ## lfccNmbFltrs ## lfccMaxFrq ## lfccMinFrq ## lfccNmbCpCoe ## lfccCols ## lfccExtend
    val lfccPs = flatten( original )
    // So we use an alternate function to create the task
    val opslfcc = T( lfccStep$ _, lfccPs )


   //val s4 = loadExtraINEGICSVs(MyFormat, filesp.map(_.toString()), tmp3.toList)

    ///println(s4)

    // Set-up the experiment pipes
    val s3 = loadExtraINEGICSVs(MyFormat, filesp.map(_.toString()), tmp3.toList) *:
      renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly")  *:            //Rename column
      toDouble("t", "X", "Y", "Z", "isanomaly" )  *:
      //window(FFTSz,64) *:                                                    // return vectors of frames 4x1x16(x,y,z,IsAnomaly)
      opsWin  *:                                                               // return vectors of frames 4x1x16(x,y,z,IsAnomaly)
      modeStep("mode","isanomaly") *:  // (isanomalymode)
      //lfccStep("lfcc",Fs, NmbFltrs, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z") *:  //return vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      opslfcc *:                                                               //return vectors of features 3x1x5(xlfcc,ylfcc,zlfcc)
      concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc") *:                       //1x15 (lfcc)
      window(FrmSz2,FrmStp2) *:                                                //return vectors of features 64x15(lfcc)
      medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc") *:                         //1x15 (lfccmedian,damagemedian)
      modeStep("mode","isanomalymode")  *:                                     // (isanomalymode)
      anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian") *:
      anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" ) *:
      //plotStreamStep( 3000, "isanomalymodemode", "det","acc","far") ->:
      aggregMaxStep("det") ->:
      //aggregToTableStep(lfccNmbCpCoe,lfccNmbFltrs,"far")
      getLastRow("det", "isanomalymodemode", "acc", "far")

    //val expectedSize = 20
    val expectedSize = 1
    val empty = Frame(("",List()))
    val acc: Executing = Executing()

    // compile new pipes, takes a few micro-seconds
    val (c3, t0) = Utils.time( scompile(s3) )
    val c3l = c3.toList
    c3l.size shouldBe expectedSize
    //t0/1e9 shouldBe <= (0.05) // seconds

    // Lets just grab and execute one of the pipes
    val idx = 0 // 19
    // Here we just set-up s delayed operation on a stream,
    // setting up stream was taking 1.5 sec but now is negligible
    val pipe1: Either[ADWError, Exec[Frame,Row]] = c3l(idx)
    //pipe1 should be('right)
    val func1: PartialFunc[Frame,Row] = pipe1.right.get.fs
    val (sol1: (Either[ADWError,Row],Executing), t1) =  Utils.time( func1( acc, empty ) )
    //println(s"t1 = ${t1/1.0e9} sec")
    //sol1._1 should be ('right)
    //val result1 = sol1._1.right.get  //  vector de vectors
    //println(result1)
    //t1/1e9 shouldBe <= (0.05) // seconds

    // Execute the pipes, same as above all pipes are delayed for stream processing
    val (r3: Iterable[(Either[ADWError, Row], Executing)], t2) = Utils.time( execDebug(c3)( empty ) )
    //println(s"t2 = ${t2/1.0e9} sec")
    val r3l = r3.toList
    //r3l.size shouldBe expectedSize
    //t2/1e9 shouldBe <= (0.05) // seconds

    // Get the results (not executed yet)
    val r2: Row = r3l(idx)._1.right.get
    //println(s"t3 = ${t3/1.0e9} sec")
    //t3/1e9 shouldBe <= (300.0) // seconds

    // prediction
    val r2_det: Either[String, Double] = unpack[Double](r2("det"))
    //r2_det should be ('right)
    //r2_det.right.get should be (1.0)

    // label
    val r2_isanomalymodemode: Either[String, Double] = unpack[Double](r2("isanomalymodemode"))
    //r2_isanomalymodemode should be ('right)
   // r2_isanomalymodemode.right.get should be (1.0)

    // accuracy
    val r2_acc: Either[String, Double] = unpack[Double](r2("acc"))
    //r2_acc should be ('right)
   // r2_acc.right.get should be >= 0.9

    // false alarm rate
    val r2_far: Either[String, Double] = unpack[Double](r2("far"))
    //r2_far should be ('right)
    //r2_far.right.get should be <= 0.085


    // Lets automatically execute all the pipes in parallel (nothing is executed, stream processing delayed)
    val resultTask: Agg[Row,  ( Either[ADWError,Row] , Executing ) ] = TestTask(0.5)
    val (agg, t4) = Utils.time( Pipes.partialsPar(c3.iterator, resultTask.t, Par.cores, Par.waitFor, Par.verbose)(Par.scheduler) )
    //println(s"t4 = ${t4/1.0e9} sec")
    //t4/1e9 shouldBe <= (0.03) // seconds


    // Now we execute all the pipes in parallel (TestTask kicks off the stream processing)
    val (all: (Either[ADWError, (Either[ADWError, Row], Executing)], Executing), t5) = Utils.time( agg(acc, empty) )
    //println(s"t5 = ${t5/1.0e9} sec")
    //all._1 should be ('right)
    val result_all = all._1.right.get
    //result_all._1 should be ('right)
    val all_best = result_all._1.right.get
    val all_exec = result_all._2

    // prediction
    val r2all_det: Either[String, Double] = unpack[Double](all_best("det"))
    //r2all_det should be ('right)
    r2all_det.right.get should be (1.0)

    // label
    val r2all_isanomalymodemode: Either[String, Double] = unpack[Double](all_best("isanomalymodemode"))
    r2all_isanomalymodemode should be ('right)
    r2all_isanomalymodemode.right.get should be (1.0)

    // accuracy
    val r2all_acc: Either[String, Double] = unpack[Double](all_best("acc"))
    r2all_acc should be ('right)
    r2all_acc.right.get should be >= 0.9

    // false alarm rate
    val r2all_far: Either[String, Double] = unpack[Double](all_best("far"))
    r2all_far should be ('right)
    r2all_far.right.get should be <= 0.085

    // TODO: ask - Number of filters of the filter bank( equal to NmbCpCoe), but here they are different
    /*
    all_exec.history should contain theSameElementsInOrderAs List(
      loadExtraINEGICSVs(MyFormat, filesp.map(_.toString()), tmp3.toList).t,
        renameColumns("Time(s)" -> "t","IsAnomaly" -> "isanomaly").t,
        toDouble("t", "X", "Y", "Z", "isanomaly" ).t,
        getTask(opsWin,0).right.get, // window(FFTSz,32).t,  // !!
        modeStep("mode","isanomaly").t,
        getTask(opslfcc,0).right.get, // lfccStep("lfcc",Fs, 1, MaxFrq, MinFrq,NmbCpCoe,"X","Y","Z").t, // !!
        concatenateStep("lfcc","Xlfcc","Ylfcc","Zlfcc").t,
        window(FrmSz2,FrmStp2).t,
        medianStep("median",NmbCpCoe*3,FrmSz2,"lfcc").t,
        modeStep("mode","isanomalymode").t,
        anomalyDetectionTrainStep("det",NmbCpCoe*3,0.9,0.9,200,"lfccmedian").t,
        anomDetectionEvalStep( "isanomalymodemode" , "det" , "far" ).t
    ).reverse
    */

    //t5/1e9 shouldBe <= (1200.0) // seconds aprox. 17 minutes

  }


    /*
    it should "multiple pipe detector2" in {
        // Open file
        val dir = cwd / "data/inegi/ensaios_rolamentos_3"
        val matchNames = "rol1_rpm1000_hp0_b_mm0_exp1.csv"

        val file = DataFiles.parseFileNames(dir, matchNames, Utils.parseFileName).toList
        assert( file.size == 1 )
        val parserOk = file.filter( _.isRight ).map(_.right.get)
        assert( parserOk.size == 1 )
        val (filep,feature,unit) = parserOk.unzip3

        filep.size shouldBe 1

        val offset = 0.000195
        val tmp1 = DataFiles.generateTimeStampOffsets(offset, filep)

        val s1 = loadExtraINEGICSVs(MyFormat, filep.map(_.toString()), tmp1.toList)

        // compile new pipes
        val c1 = scompile(s1)
        val c1l = c1.toList
        c1l.size shouldBe 1
        c1l.head should be ('right)
/*
        // Execute the pipes
        val r1 = execDebug(c1)(Frame(("",List())))

        // Get the results (not executed yet)
        val result1: Frame = r1.head._1.right.get

        // Collect some of the results (executes now)
        val read1 = result1.project("X","Y","Z").iterable.toIterator.toIndexedSeq
        read1.size shouldBe 20480

        val matchNames1 = "rol1_rpm1000_hp0_b_mm0_exp*.csv"

        val files = DataFiles.parseFileNames(dir, matchNames1, Utils.parseFileName).toList
        assert( files.size == 5 )
        val parserOks = files.filter( _.isRight ).map(_.right.get)
        assert( parserOks.size == 5 )
        val (filesp,features,units) = parserOks.unzip3

        filesp.size shouldBe 5

        val tmp2 = DataFiles.generateTimeStampOffsets(offset, filesp)
        val s2 = loadExtraINEGICSVs(MyFormat, filesp.map(_.toString()), tmp2.toList)

        // compile new pipes
        val c2 = scompile(s2)
        val c2l = c2.toList
        c2l.size shouldBe 1
        c2l.head should be ('right)

        // Execute the pipes
        val r2 = execDebug(c2)(Frame(("",List())))

        // Get the results (not executed yet)
        val result2: Frame = r2.head._1.right.get

        // Collect some of the results (executes now)
        val read2 = result2.project("X","Y","Z").iterable.toIterator.toIndexedSeq
        read2.size shouldBe (5*20480)*/

    }
*/


}
