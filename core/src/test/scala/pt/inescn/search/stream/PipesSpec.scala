package pt.inescn.search.stream

import java.time.LocalDateTime

import scala.concurrent.duration.Duration
import org.scalatest.{FlatSpec, Inside, Matchers}
import better.files.Dsl.cwd
import better.files.File
import com.github.tototoshi.csv.DefaultCSVFormat
import pt.inescn.etl.Load.format
import pt.inescn.etl.stream.Load._
import pt.inescn.eval.stream.BinaryClassMetrics
import pt.inescn.eval.{MCC, Precision, Recall}
import pt.inescn.samplers.stream.Enumeration
import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.TestTasks._
import pt.inescn.search.stream.UtilTasks._
import pt.inescn.search.stream.OpCheck._
import pt.inescn.search.stream.Pipes.{compile => scompile, _}
import pt.inescn.search.stream.TestTasks.maxSum.Acc
import pt.inescn.utils.{ADWError, Utils}

// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._

/**
  *
  * TODO: https://stackoverflow.com/questions/47806670/how-to-serialize-functions-in-scala
  * serialization
  * http://scalacenter.github.io/spores/spores.html
  * https://github.com/scalacenter/spores
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "root/testOnly pt.inescn.search.stream.PipesSpec"
  */
class PipesSpec extends FlatSpec with Matchers with Inside {
  object MyFormat extends DefaultCSVFormat {
    override val delimiter = ',' // ';'
  }

  "Pipes algebra" should "allow pipe construction with ->: and all(*) " in {

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

    // ->:

    val s1 = concat("0", "x", "y", "z") ->:
      concat("1", "x") ->: concat("2", "y") ->: concat("3", "z")

    val c1 = scompile(s1)
    val ex1 = c1.toList
    //println(ex1)
    // only one sequential pipe
    ex1.size shouldBe 1

    // 4 operations
    ex1.head should be ('right)
    val ex1h = ex1.head.right.get.track
    ex1h.history.size shouldBe 4
    ex1h.history.head shouldBe concat("0", "x", "y", "z").t
    ex1h.history(1) shouldBe concat("1", "x").t
    ex1h.history(2) shouldBe concat("2", "y").t
    ex1h.history(3) shouldBe concat("3", "z").t

    // execute, recreate iterator that has been spent
    val r1 = execDebug(c1)(Frame(testData: _*))
    val r1l = r1.toList
    // one partial function result per pipe
    r1l.size shouldBe 1

    // get the results
    val r1r = r1l.head
    r1r._1 should be('right)
    // check execution logs
    val r1r_traces = r1r._2.history
    //println(r1r_traces)
    val log1 = List(
      concat("3", "z").t,
      concat("2", "y").t,
      concat("1", "x").t,
      concat("0", "x", "y", "z").t)
    r1r_traces should contain theSameElementsInOrderAs log1

    r1r._1 should be('right)
    val r1r_result = r1r._1.right.get
    val read1 = r1r_result.iterable.toIterator.take(25).toIndexedSeq
    //println(read1.mkString(";\n"))
    val result1 = List(
      Row(Map("x" -> Val("101"), "y" -> Val("202"), "z" -> Val("303"))),
      Row(Map("x" -> Val("101"), "y" -> Val("202"), "z" -> Val("303"))),
      Row(Map("x" -> Val("101"), "y" -> Val("202"), "z" -> Val("303")))
    )
    read1 should contain theSameElementsInOrderAs result1

    // All
    val s2 = load(testData) *: All(concat("1", "x"), concat("2", "y"), concat("3", "z"))

    val c2 = scompile(s2)
    val c2l = c2.toList
    //println(c2l)
    // 3 sequential pipes
    c2l.size shouldBe 3

    c2l.head should be('right)
    c2l(1) should be('right)
    c2l(1) should be('right)

    val ex2: Exec[Any, Frame] = c2l.head.right.get
    val ex2h: Compiling = ex2.track
    // 4 operations
    ex2h.history.size shouldBe 2
    ex2h.history should contain theSameElementsInOrderAs List(load(testData).t, concat("1", "x").t)
    c2l(1).right.get.track.history should contain theSameElementsInOrderAs List(load(testData).t, concat("2", "y").t)
    c2l(2).right.get.track.history should contain theSameElementsInOrderAs List(load(testData).t, concat("3", "z").t)

    // execute
    val r2 = execDebug(c2)(empty)
    val r2l = r2.toList
    // 3 partial function result for single pipe
    r2l.size shouldBe 3
    r2l.head._1 should be('right)
    r2l(1)._1 should be('right)
    r2l(2)._1 should be('right)
    //println(r2)

    val r2r_traces = r2l.flatMap {
      case (_, Executing(history)) => List(history);
      case _ => List()
    }
    val r2r_result = r2l.flatMap {
      case (Right(result), _) =>
        result match {
          case _: Frame => List(result)
          case _ => List()
        }
      case (Left(_), _) => List()
    }

    // Check log of execution (inverse order)
    //println(r2r_traces)
    val log2 = List(
      List(concat("3", "z").t, load(testData).t),
      List(concat("2", "y").t, load(testData).t),
      List(concat("1", "x").t, load(testData).t)
    )
    r2r_traces should contain theSameElementsAs log2

    // Check results of the 3 pipes
    val read2_0 = r2r_result.head.iterable.toIterator.take(25).toIndexedSeq
    //println(read2_0.mkString(";\n"))
    val result2_0 = List(
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("3")))
    )
    read2_0 should contain theSameElementsInOrderAs result2_0

    val read2_1 = r2r_result(1).iterable.toIterator.take(25).toIndexedSeq
    //println(read2_1.mkString(";\n"))
    val result2_1 = List(
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("3")))
    )
    read2_1 should contain theSameElementsInOrderAs result2_1

    val read2_2 = r2r_result(2).iterable.toIterator.take(25).toIndexedSeq
    //println(read2.mkString(";\n"))
    val result2_2 = List(
      Row(Map("x" -> Val("1"), "y" -> Val("2"), "z" -> Val("33"))),
      Row(Map("x" -> Val("1"), "y" -> Val("2"), "z" -> Val("33"))),
      Row(Map("x" -> Val("1"), "y" -> Val("2"), "z" -> Val("33")))
    )
    read2_2 should contain theSameElementsInOrderAs result2_2


    // all ->: all

    val s3 = load(testData) *:
      All(concat("1", "x"), concat("2", "y")) ->:
      All(concat("3", "z"), concat("4", "z"))

    val c3 = scompile(s3)
    val c3l = c3.toList
    //println(c3)
    c3l.size shouldBe 2
    c3l.head should be('right)
    c3l(1) should be('right)

    val ex3: Exec[Any, Frame] = c3l.head.right.get
    // 2 sequential pipes
    ex3.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1", "x").t, concat("3", "z").t)
    c3l(1).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("2", "y").t, concat("4", "z").t)

    // execute
    val r3 = execDebug(c3)(empty)
    val r3l = r3.toList
    //println(r3)
    // 2 partial function result for single pipe
    r3l.size shouldBe 2
    r3l.head._1 should be('right)
    r3l(1)._1 should be('right)
    //println(r3l)

    val r3r_traces = r3l.flatMap {
      case (_, Executing(history)) => List(history);
      case _ => List()
    }
    val r3r_result = r3l.flatMap {
      case (Right(result), _) =>
        result match {
          case _: Frame => List(result)
          case _ => List()
        }
      case _ => List()
    }

    // Check log of execution (inverse order)
    //println(r3r_traces)
    val log3 = List(
      List(concat("3", "z").t, concat("1", "x").t, load(testData).t),
      List(concat("4", "z").t, concat("2", "y").t, load(testData).t)
    )
    r3r_traces should contain theSameElementsAs log3

    // Check results of the 2 pipes
    val read3_0 = r3r_result.head.iterable.toIterator.take(25).toIndexedSeq
    //println(read3_0.mkString(";\n"))
    val result3_0 = List(
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("33"))),
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("33"))),
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("33")))
    )
    read3_0 should contain theSameElementsInOrderAs result3_0

    val read3_1 = r3r_result(1).iterable.toIterator.take(25).toIndexedSeq
    //println(read3_1.mkString(";\n"))
    val result3_1 = List(
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("34"))),
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("34"))),
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("34")))
    )
    read3_1 should contain theSameElementsInOrderAs result3_1

  }

  it should "allow pipe construction with *: and all(*) " in {

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

    // all *: all

    val s3 = load(testData) *:
      All(concat("1", "x"), concat("2", "y")) *:
        All(concat("3", "z"), concat("4", "z"))

    val c3 = scompile(s3)
    val c3l = c3.toList
    c3l.size shouldBe 4
    //println(c3)
    c3l.head should be('right)
    c3l(1) should be('right)
    c3l(2) should be('right)
    c3l(3) should be('right)

    val ex3: Exec[Any, Frame] = c3l.head.right.get
    // 4 sequential pipes - 2 x 2
    ex3.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1", "x").t, concat("3", "z").t)
    c3l(1).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1", "x").t, concat("4", "z").t)
    c3l(2).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("2", "y").t, concat("3", "z").t)
    c3l(3).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("2", "y").t, concat("4", "z").t)

    // execute
    val r3 = execDebug(c3)(empty)
    val r3l = r3.toList
    r3l.size shouldBe 4
    //println(r3)
    // 4 partial function
    r3l.head._1 should be('right)
    r3l(1)._1 should be('right)
    r3l(2)._1 should be('right)
    r3l(3)._1 should be('right)
    //println(r3)

    val r3r_traces = r3l.flatMap {
      case (_, Executing(history)) => List(history);
      case _ => List()
    }
    val r3r_result = r3l.flatMap {
      case (Right(result), _) =>
        result match {
          case _: Frame => List(result)
          case _ => List()
        }
      case _ => List()
    }

    // Check log of execution (inverse order)
    //println(r3r_traces)
    val log3 = List(
      List(concat("3", "z").t, concat("1", "x").t, load(testData).t),
      List(concat("4", "z").t, concat("1", "x").t, load(testData).t),
      List(concat("3", "z").t, concat("2", "y").t, load(testData).t),
      List(concat("4", "z").t, concat("2", "y").t, load(testData).t)
    )
    r3r_traces should contain theSameElementsAs log3

    // Check results of the 2 pipes
    val read3_0 = r3r_result.head.iterable.toIterator.take(25).toIndexedSeq
    //println(read3_0.mkString(";\n"))
    val result3_0 = List(
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("33"))),
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("33"))),
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("33")))
    )
    read3_0 should contain theSameElementsInOrderAs result3_0

    val read3_1 = r3r_result(1).iterable.toIterator.take(25).toIndexedSeq
    //println(read3_1.mkString(";\n"))
    val result3_1 = List(
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("34"))),
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("34"))),
      Row(Map("x" -> Val("11"), "y" -> Val("2"), "z" -> Val("34")))
    )
    read3_1 should contain theSameElementsInOrderAs result3_1

    val read3_2 = r3r_result(2).iterable.toIterator.take(25).toIndexedSeq
    //println(read3_2.mkString(";\n"))
    val result3_2 = List(
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("33"))),
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("33"))),
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("33")))
    )
    read3_2 should contain theSameElementsInOrderAs result3_2

    val read3_3 = r3r_result(3).iterable.toIterator.take(25).toIndexedSeq
    //println(read3_3.mkString(";\n"))
    val result3_3 = List(
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("34"))),
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("34"))),
      Row(Map("x" -> Val("1"), "y" -> Val("22"), "z" -> Val("34")))
    )
    read3_3 should contain theSameElementsInOrderAs result3_3

    // all *: all *: all

    val s4 = load(testData) *:
      All(concat("1", "x"), concat("2", "y")) *:
      All(concat("3", "z"), concat("4", "z")) *:
      All(concat("5", "x"), concat("6", "x"))

    val c4 = scompile(s4)
    val c4l = c4.toList
    // 8 sequential pipes - 2 x 2 x 2
    c4l.size shouldBe 8
    c4l.head should be('right)
    c4l(1) should be('right)
    c4l(2) should be('right)
    c4l(3) should be('right)
    c4l(4) should be('right)
    c4l(5) should be('right)
    c4l(6) should be('right)
    c4l(7) should be('right)

    val ex4: Exec[Any, Frame] = c4l.head.right.get
    ex4.track.history should contain theSameElementsInOrderAs
      List(load(testData).t,
        concat("1", "x").t, concat("3", "z").t, concat("5", "x").t)
    c4l(1).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t,
        concat("1", "x").t, concat("3", "z").t, concat("6", "x").t)
    c4l(2).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t,
        concat("1", "x").t, concat("4", "z").t, concat("5", "x").t)
    c4l(3).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t,
        concat("1", "x").t, concat("4", "z").t, concat("6", "x").t)
    c4l(4).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t,
        concat("2", "y").t, concat("3", "z").t, concat("5", "x").t)
    c4l(5).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t,
        concat("2", "y").t, concat("3", "z").t, concat("6", "x").t)
    c4l(6).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t,
        concat("2", "y").t, concat("4", "z").t, concat("5", "x").t)
    c4l(7).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t,
        concat("2", "y").t, concat("4", "z").t, concat("6", "x").t)

    // execute
    //val r4 = exec(fs4)(testData)
    val r4: Iterable[(Either[ADWError, Frame], Executing)] = execDebug(c4)(empty)
    // 8 partial function
    val r4l = r4.toList
    r4l.size shouldBe 8
    r4l.head._1 should be('right)
    r4l(1)._1 should be('right)
    r4l(2)._1 should be('right)
    r4l(3)._1 should be('right)
    r4l(4)._1 should be('right)
    r4l(5)._1 should be('right)
    r4l(6)._1 should be('right)
    r4l(7)._1 should be('right)
    //println(r4)
  }

  it should "allow detection of incorrect pipe construction with ->:" in {

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

    // one ->: all *: all
    // one cannot be paired with the m (all *: all) pipes
    // the -> operator will pair with a single  of the m pipes only
    // missed paring will be flagged with an error

    val s1 = load(testData) ->:
      All(concat("1", "x"), concat("2", "y")) *:
      All(concat("3", "z"), concat("4", "z"))

    val c1 = scompile(s1)
    val c1l = c1.toList
    c1l.size shouldBe 4
    c1l.head should be('right)
    c1l(1) should be('left)
    c1l(2) should be('left)
    c1l(3) should be('left)

    // Note that we can still pair with the -> operator
    // That paring will result in 2 pipes

    val s2 = load(testData) *:
      All(concat("1", "x"), concat("2", "y")) ->:
      All(concat("3", "z"), concat("4", "z"))

    val c2 = scompile(s2)
    val c2l = c2.toList
    c2l.size shouldBe 2
    c2l.head should be('right)
    c2l(1) should be('right)

  }

  it should "allow pipe construction with When(*) " in {


    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )


    // all *: all

    // Important note: we only get access to the complete tracking
    // during compilation after all sub-expressions have been compiled
    // So we can only apply the check to the sub-expressions' tracking
    val combinations = All(concat("1x", "x"), concat("1y", "y")) *:
      All(concat("2x", "x"), concat("2y", "y"))
    // make sure we do not repeatedly apply the same function to the same columns
    val s3 = load(testData) *: When(NoRepeatsW, combinations)

    val c3 = scompile(s3)
    val c3l = c3.toList
    // 4 sequential pipes - 2 x 2, but we remove repeats so only 2
    c3l.size shouldBe 2
    c3l.head should be('right)
    c3l(1) should be('right)

    val ex3: Exec[Any, Frame] = c3l.head.right.get
    ex3.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1x", "x").t, concat("2y", "y").t)
    c3l(1).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1y", "y").t, concat("2x", "x").t)

    // execute
    val r3 = execDebug(c3)(empty)
    val r3l = r3.toList
    // 4 partial function
    r3l.size shouldBe 2
    r3l.head._1 should be('right)
    r3l(1)._1 should be('right)

    val r3r_traces = r3l.flatMap {
      case (_, Executing(history)) => List(history)
      case _ => List()
    }
    val r3r_result = r3l.flatMap {
      case (Right(result), _) =>
        result match {
          case _: Frame => List(result)
          case _ => List()
        }
      case _ => List()
    }

    // Check log of execution (inverse order)
    //println(r3r_traces)
    val log3 = List(
      List(concat("2y", "y").t, concat("1x", "x").t, load(testData).t),
      List(concat("2x", "x").t, concat("1y", "y").t, load(testData).t)
    )
    r3r_traces should contain theSameElementsAs log3

    // Check results of the 2 pipes
    val read3_0 = r3r_result.head.iterable.toIterator.take(25).toIndexedSeq
    //println(read3_0.mkString(";\n"))
    val result3_0 = List(
      Row(Map("x" -> Val("11x"), "y" -> Val("22y"), "z" -> Val("3"))),
      Row(Map("x" -> Val("11x"), "y" -> Val("22y"), "z" -> Val("3"))),
      Row(Map("x" -> Val("11x"), "y" -> Val("22y"), "z" -> Val("3")))
    )
    read3_0 should contain theSameElementsInOrderAs result3_0

    val read3_1 = r3r_result(1).iterable.toIterator.take(25).toIndexedSeq
    //println(read3_1.mkString(";\n"))
    val result3_1 = List(
      Row(Map("x" -> Val("12x"), "y" -> Val("21y"), "z" -> Val("3"))),
      Row(Map("x" -> Val("12x"), "y" -> Val("21y"), "z" -> Val("3"))),
      Row(Map("x" -> Val("12x"), "y" -> Val("21y"), "z" -> Val("3")))
    )
    read3_1 should contain theSameElementsInOrderAs result3_1

  }

  it should "allow pipe construction with Iff(*) " in {


    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

    // all *: all

    // make sure we do not repeatedly apply the same function to the same columns
    // in this case the tracking is executed in order. We simply
    val s3c = load(testData) *:
      All(concat("1x", "x"), concat("1y", "y")) *: All(
        Iff(NoRepeats, concat("2x", "x")),
        Iff(NoRepeats, concat("2y", "y")))

    val c3c = scompile(s3c)
    val c3cl = c3c.toList
    c3cl.size shouldBe 4
    c3cl.forall{ case Right(_) => true ; case _ => false } shouldBe true

    val s3 = load(testData) *:
      All(concat("1x", "x"), concat("1y", "y")) *:
        Iff(NoRepeats, concat("2x", "x"), concat("2y", "y"))

    val c3 = scompile(s3)
    val c3l = c3.toList
    // 4 sequential pipes - 2 x 2
    c3l.size shouldBe 4
    c3l.head should be('right)
    c3l(1) should be('right)
    c3l(2) should be('right)
    c3l(3) should be('right)

    val ex3: Exec[Any, Frame] = c3l.head.right.get
    ex3.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1x", "x").t, concat("2x", "x").t)
    c3l(1).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1x", "x").t, concat("2y", "y").t)
    c3l(2).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1y", "y").t, concat("2x", "x").t)
    c3l(3).right.get.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concat("1y", "y").t, concat("2y", "y").t)

    // execute
    val r3 = execDebug(c3)(empty)
    val r3l = r3.toList
    // 4 partial function, ony 2 will execute to the end
    r3l.size shouldBe 4
    r3l.head._1 should be('left)
    r3l(1)._1 should be('right)
    r3l(2)._1 should be('right)
    r3l(3)._1 should be('left)

    // Get rid of errors/non-execution
    val r3r_traces = r3l.flatMap {
      case (Right(_), Executing(history)) => List(history)
      case _ => List()
    }
    val r3r_result = r3l.flatMap {
      case (Right(result), _) =>
        result match {
          case _: Frame => List(result)
          case _ => List()
        }
      case _ => List()
    }

    // Check log of execution (inverse order)
    //println(r3r_traces)
    val log3 = List(
      List(concat("2y", "y").t, concat("1x", "x").t, load(testData).t),
      List(concat("2x", "x").t, concat("1y", "y").t, load(testData).t)
    )
    r3r_traces should contain theSameElementsAs log3

    // Check results of the 2 pipes, 2 were "removed" during run-time
    val read3_0 = r3r_result.head.iterable.toIterator.take(25).toIndexedSeq
    //println(read3_0.mkString(";\n"))
    val result3_0 = List(
      Row(Map("x" -> Val("11x"), "y" -> Val("22y"), "z" -> Val("3"))),
      Row(Map("x" -> Val("11x"), "y" -> Val("22y"), "z" -> Val("3"))),
      Row(Map("x" -> Val("11x"), "y" -> Val("22y"), "z" -> Val("3")))
    )
    read3_0 should contain theSameElementsInOrderAs result3_0

    val read3_1 = r3r_result(1).iterable.toIterator.take(25).toIndexedSeq
    //println(read3_1.mkString(";\n"))
    val result3_1 = List(
      Row(Map("x" -> Val("12x"), "y" -> Val("21y"), "z" -> Val("3"))),
      Row(Map("x" -> Val("12x"), "y" -> Val("21y"), "z" -> Val("3"))),
      Row(Map("x" -> Val("12x"), "y" -> Val("21y"), "z" -> Val("3")))
    )
    read3_1 should contain theSameElementsInOrderAs result3_1

  }

  it should "automatically deal with exceptions" in {

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

    // second task should throw an exception
    val s3 = load(testData) ->: concatThrow("1", "x") ->: concat("2", "y")

    val c3 = scompile(s3)
    val c3l = c3.toList
    c3l.forall{ case Right(_) => true ; case _ => false } shouldBe true
    // 1 sequential pipes
    c3l.size shouldBe 1

    val ex3: Exec[Any, Frame] = c3l.head.right.get
    ex3.track.history should contain theSameElementsInOrderAs
      List(load(testData).t, concatThrow("1", "x").t, concat("2", "y").t)

    // execute
    val r3 = execDebug(c3)(empty)
    val r3l = r3.toList
    // 4 partial function, ony 2 will execute to the end
    r3l.size shouldBe 1
    r3l.head._1 should be('left)
    //println(r3)

    // Get rid of errors/non-execution
    val r3r_traces = r3l.flatMap {
      case (Right(_), Executing(history)) => List(history);
      case _ => List()
    }
    val r3r_result = r3l.flatMap {
      case (Right(result), _) => List(result);
      case (Left(_), _) => List[Val[_]]()
    }

    r3r_traces.isEmpty shouldBe true
    r3r_result.isEmpty shouldBe true

    // Get rid of errors/non-execution
    val r3r_result_e = r3l.flatMap {
      case (Right(_), _) => List();
      case (Left(ADWError(e)), _) => List(e)
    }
    r3r_result_e.size shouldBe 1

    val expected = "Trace(TestTasks.this.concatThrow$(Map(a -> StringP(1), cols -> ColumnNames(WrappedArray(x))))):\njava.lang.RuntimeException: Column('X') exception"
    r3r_result_e.head.substring(0, expected.length) shouldBe expected
  }


  it should "allow propagating cross-product operation via sequence" in {

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )


    // second task should throw an exception
    val s3 = load(testData) *:
      All(concat("1", "x"), concat("2", "x")) *:
      concat("3", "x") *:
      All(concat("4", "x"), concat("5", "x"))

    val c3 = scompile(s3)
    val c3l = c3.toList
    c3l.forall{ case Right(_) => true ; case _ => false } shouldBe true
    c3l.head should be('right)
    c3l(1) should be('right)
    c3l(2) should be('right)
    c3l(3) should be('right)

    // execute
    val r3: Iterable[(Either[ADWError, Frame], Executing)] = execDebug(c3)(empty)
    val r3l = r3.toList
    r3l.size shouldBe 4
    r3l.head._1 should be('right)

    val result_1 = r3.head._1.right.get
    val read3_1 = result_1.iterable.toIterator.toIndexedSeq
    //println(read3_1.mkString(";\n"))
    val result3_1 = List(
      Row(Map("x" -> Val("1134"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1134"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1134"), "y" -> Val("2"), "z" -> Val("3")))
    )
    read3_1 should contain theSameElementsInOrderAs result3_1

    val result_2 = r3l(1)._1.right.get
    val read3_2 = result_2.iterable.toIterator.toIndexedSeq
    //println(read3_2.mkString(";\n"))
    val result3_2 = List(
      Row(Map("x" -> Val("1135"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1135"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1135"), "y" -> Val("2"), "z" -> Val("3")))
    )
    read3_2 should contain theSameElementsInOrderAs result3_2

    val result_3 = r3l(2)._1.right.get
    val read3_3 = result_3.iterable.toIterator.toIndexedSeq
    //println(read3_3.mkString(";\n"))
    val result3_3 = List(
      Row(Map("x" -> Val("1234"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1234"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1234"), "y" -> Val("2"), "z" -> Val("3")))
    )
    read3_3 should contain theSameElementsInOrderAs result3_3

    val result_4 = r3l(3)._1.right.get
    val read3_4 = result_4.iterable.toIterator.toIndexedSeq
    //println(read3_4.mkString(";\n"))
    val result3_4 = List(
      Row(Map("x" -> Val("1235"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1235"), "y" -> Val("2"), "z" -> Val("3"))),
      Row(Map("x" -> Val("1235"), "y" -> Val("2"), "z" -> Val("3")))
    )
    read3_4 should contain theSameElementsInOrderAs result3_4

  }

  "Adding parameter search space" should "create one pipe per parameter combination" in {

    val empty = Frame(("Bo00000o", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

    import  pt.inescn.samplers.stream.Ops._

    // This is an example of how to create a task with a single parameter

    // Lets set-up a single parameter
    val p1s = BigDecimal(1.0) to 10 by 1.0 map(_.toDouble)

    // We now create a task that is parameterized with this single parameter
    val func1s: All[Double, Double] = T((p1:Double, i:Double) => Right(p1 * i), p1s)
    // If we compile this we get a single task pipe for each parameter
    val ops1: Iterable[Either[ADWError, Exec[Double, Double]]] = scompile(func1s)
    // We check that all compiled pipes have succeeded for each parameter
    ops1.forall{ case Right(_) => true ; case _ => false } shouldBe true
    ops1.size shouldBe 10

    // Lets execute each of these pipes and extract the trace of the execution trace
    val result1: Iterable[(Executing, Double)] = exec(ops1)(1.0)
    val history1: Iterable[List[Task[_, _]]] = result1.map{ case (Executing(h), _) => h}

    // We now compare to make sure that each if the parameterized functions were execute
    // Note that the parameterized tasks are only available as an Op, so we
    // need to look for the Func and extract the tasks from those
    val tasks1 = func1s.f.collect{ case Func(t) => List(t)}
    // Did we execute them all ?
    history1 should contain theSameElementsAs tasks1

    // This is an example of how we create a two parameter task

    // This is the second parameter
    val p2s = BigDecimal(1.0) to 100 by 10.0 map(_.toDouble)

    // Now we have to define the search space of the parameters
    val psFunc2: Iterable[(Double, Double)] = p1s && p2s

    // Create a task with two parameters using the defined search space
    val func2s: All[Double, Double] = T((p1:Double, p2:Double, i:Double) => Right(p1 * p2 * i), psFunc2)
    // If we compile this we get a single task pipe for each parameter combination (search space)
    val ops2: Iterable[Either[ADWError, Exec[Double, Double]]] = scompile(func2s)
    // We check that all compiled pipes have succeeded for each parameter
    ops2.forall{ case Right(_) => true ; case _ => false } shouldBe true
    ops2.size shouldBe 10

    // Lets execute each of these pipes and extract the trace of the execution trace
    val result2: Iterable[(Executing, Double)] = exec(ops2)(1.0)
    val history2: Iterable[List[Task[_, _]]] = result2.map{ case (Executing(h), _) => h}

    // We now compare to make sure that each if the parameterized functions were execute
    // Note that the parameterized tasks are only available as an Op, so we
    // need to look for the Func and extract the tasks from those
    val tasks2 = func2s.f.collect{ case Func(t) => List(t)}
    // Did we execute them all ?
    history2 should contain theSameElementsAs tasks2


    // This is an example of how we create a two parameter task

    // This is the second parameter
    val p3s = 'a' to 'j' by 1

    // Now we have to define the search space of the parameters
    val tmp = p1s && p2s && p3s
    // NOTE: now we have to flatten the search space so that implicit
    // task creation is possible. Note that the cse.Result type is
    // the same type as the tmp elements ((Double, Double), Char)
    val psFunc3 = flatten(tmp)
    // Alternate forms of flattening that should be avoided
    //val psFunc3X = tmp.map( Flatten(_) )               // uses Scala B«builder pattern, may not produce lazy Iterable
    //val psFunc3Z = Ops.map(tmp)( e => Flatten(e) )     // uses Ops lazy map, but extended requires coding
    //val psFunc3Y = flattenByMAp(tmp)                   // alternate implementation that uses Ops.map

    // Create a task with two parameters using the defined search space
    val funcs3 = T(
            (p1:Double, p2:Double, p3:Char, i:Double) => {
        val tmp = p1 * p2 * i
        Right(tmp.toString + p3)
      }, psFunc3)

    // If we compile this we get a single task pipe for each parameter combination (search space)
    val ops3 = scompile(funcs3)
    // We check that all compiled pipes have succeeded for each parameter
    ops3.forall{ case Right(_) => true ; case _ => false } shouldBe true
    ops3.size shouldBe 10

    // Lets execute each of these pipes and extract the results
    val result3 = exec(ops3)(1.0)
    val results3 = result3.map{ case (Executing(_), r) => r}
    // We now check that we get the expected results
    results3.toList should contain theSameElementsInOrderAs
      List("1.0a","22.0b","63.0c","124.0d","205.0e","306.0f","427.0g","568.0h","729.0i","910.0j")


    // We now show how to use search spaces in a pipe

    // Task 1: applies a function to the input of a Frame's column with 2 parameterized values
    val p11 = Enumeration.range(1.0, 3.0, 1.0)
    val p12 = Enumeration.range(1.0, 2.0, 1.0)
    val p13 = Iterable("x","y")
    val ptsk1 = flatten( p11 ## p12 ## p13)
    val tsk1: All[Frame,Frame] = T(
        (a: Double, b:Double, col:String, i:Frame) => {
          // convert the col to double and calculate the output
          val tmp:Frame = i((v:String) => a*v.toDouble + b, col)
          Right(tmp)
        },
    //1.0, 1.0, "x" )
    ptsk1)

    // Task 2: converts a Frame's column from Double to a string an prepends a parameterized string
    val p21 = Enumeration.continually("xCol:").take(6) +++ Enumeration.continually("yCol:").take(6)
    val p22 = Enumeration.continually("x").take(6) +++ Enumeration.continually("y").take(6)
    val ptsk2 = flatten( p21 && p22)
    val tsk2: All[Frame,Frame] = T(
        (a: String, col:String, i:Frame) => {
          // covert the column to a string and prepend the 'a'
          val tmp:Frame = i((v:Double) => a+v.toString, col)
          Right(tmp)
        }
      ,
    //"xCol:", "x" )
    ptsk2)

    // now combine the tasks
    val s2 = load(testData) *: tsk1 ->: tsk2

    // Prepare the execution
    val c2 = scompile(s2)
    // We check that all compiled pipes have succeeded for each parameter
    c2.forall{ case Right(_) => true ; case _ => false } shouldBe true
    c2.size shouldBe 12 // 2 * 3 * (x,y)

    // Lets execute each of these pipes and extract the results
    val result4 = exec(c2)(empty)
    val results4: Iterable[Frame] = result4.map{ case (Executing(_), r) => r}
    // We execute the pipes and get a result for each
    val result4s = results4.toIndexedSeq
    result4s.size shouldBe 12 // 2 * 3 * (x,y)

    // Frames are ephemeral lazy streams, we can only access them once, so keep a copy
    // And use iterable to execute pending operations and get result
    val r4row1 = result4s(0).iterable.toList
    // Just in case you are
    // println(result4s(0).colTypes)
    // println(r4row1)
    // We now check that we get the expected results
    r4row1 should contain theSameElementsInOrderAs
      List(
        Row(Map("x" -> Val("xCol:2.0"), "y" -> Val("2"), "z" -> Val("3"))),
        Row(Map("x" -> Val("xCol:2.0"), "y" -> Val("2"), "z" -> Val("3"))),
        Row(Map("x" -> Val("xCol:2.0"), "y" -> Val("2"), "z" -> Val("3")))
      )

    // Same as above but now we get the first result for the p13 = "y" column

    val r4row6 = result4s(6).iterable.toList
    // Just in case you are
    // println(result4s(6).colTypes)
    // println(r4row6)
    // We now check that we get the expected results
    r4row6 should contain theSameElementsInOrderAs
      List(
        Row(Map("x" -> Val("1"), "y" -> Val("yCol:3.0"), "z" -> Val("3"))),
        Row(Map("x" -> Val("1"), "y" -> Val("yCol:3.0"), "z" -> Val("3"))),
        Row(Map("x" -> Val("1"), "y" -> Val("yCol:3.0"), "z" -> Val("3")))
      )

    // We assume the other results are correct and don't test threm
  }

  "Pipes" should "be serializable as JSON" in {

    //val empty = Frame(("",List()))
    /*
    val testData = List(
      "x" -> Seq("1","1","1"),
      "y" -> Seq("2","2","2"),
      "z" -> Seq("3","3","3")
    )*/

    import io.circe.{Decoder, Encoder}, io.circe.generic.auto._
    import io.circe.parser.decode, io.circe.syntax._

    // NOTE: the files are read in the order they are found when we use glob searching.
    // Here we enumerate the files so that the non-failure data is read in first
    val file1: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_watt0_norm_mm0_exp1.csv"
    val file2: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_watt0_or_mm0_exp1.csv"
    val files = List(file1, file2).map(_.toString)
    FileNames(files).asJson.noSpaces

    // Circe handles date and time, but only for JDK8 and above
    import java.time.LocalDateTime

    val now = LocalDateTime.now
    val dt = Encoder[LocalDateTime].apply(now)
    val dtc1 = Decoder[LocalDateTime].apply(dt.hcursor)
    dtc1 should be('right)
    dtc1.right.get shouldBe now

    val wrapper = DateTime(now)
    val jdt = wrapper.asJson.noSpaces
    val dtc2 = decode[DateTime](jdt)
    dtc2 should be('right)
    dtc2.right.get shouldBe wrapper

    /*
    //val sx = load(testData) ->: concat("1", "x") ->: concat("2", "y")
    //val sx = NoOp("A")
    //val sx:Param = testData       // Ok
    //val sx = load(testData)
    //val sx = Row()
    val sx: Val = IntVal(100)  // Vals are not sealed
    val sxi: Task = load(testData)     // Tasks includes a task that uses Row that uses Val
    //val sx = Func(load(testData))
    //val sx = load(testData)
    // Encode it
    val jsonx = sx.asJson.noSpaces
    println(jsonx)
    val sx =  fftP(10000, pad = true)
    val jsonx = sx.asJson.noSpaces
    println(jsonx)
    */

    /*
    // Circe only handles one level of hierarchy
    // If HannFilterP extends filtersP, we only get HannFilterP
    /*val p1: Param = new filtersP(0, 1)
    // Encode it
    val jsonx = p1.asJson.noSpaces
    println(jsonx)*/
    val p2: Param = HannFilterP(0, 1)
    // Encode it
    val jsony = p2.asJson.noSpaces
    println(jsony)
    /*val p1d = decode[Param](jsonx)
    println(p1d)*/
    val p2d = decode[Param](jsony)
    println(p2d)
    */

    /* TODO:
    // This is a simple pipe
    val s3: Op[Any, Frame, Frame] = load(testData) ->: concat("1", "x") ->: concat("2", "y")
    // Encode it
    val json = s3.asJson.noSpaces
    //println(json)
    // Decode it
    val dtc3: Either[circe.Error, Op[Any, Frame, Frame]] = decode[Op[Any,Frame,Frame]](json)
    dtc3 should be ('right)
    dtc3.right.get shouldBe s3
    */

  }


  it should "be able to express a ModelSpec test pipe" in {
    // NOTE: the files are read in the order they are found when we use glob searching.
    // Here we enumerate the files so that the non-failure data is read in first
    val file1: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_watt0_norm_mm0_exp1.csv"
    val file2: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_watt0_or_mm0_exp1.csv"
    val files = List(file1, file2).map(_.toString)

    val s0 = loadINEGICSVs(MyFormat, files)
    s0 shouldBe loadINEGICSVs(MyFormat, files)
    val c0 = scompile(s0)
    val c0l = c0.toList
    c0l.size shouldBe 1
    c0l.head should be('right)
    val ex0 = c0.head.right.get
    ex0.track.history.size shouldBe 1
    ex0.track.history.head shouldBe loadINEGICSVs(MyFormat, files).t

    val s1 = loadINEGICSVs(MyFormat, files) ->: toDouble("x", "t")
    s1 shouldBe SeqOp(List(loadINEGICSVs(MyFormat,files)), List(toDouble("x", "t")))
    val c1 = scompile(s1)
    val c1l = c1.toList
    c1l.head should be('right)
    val ex1 = c1l.head.right.get
    ex1.track.history.head shouldBe loadINEGICSVs(MyFormat, files).t
    ex1.track.history(1) shouldBe toDouble("x", "t").t

    // Create pipes
    val s2 = loadINEGICSVs(MyFormat, files) ->:
      toDouble("x", "t") ->:
      xDouble(1e9, "t") ->:
      doubleToLong("t")
    //println(s2)
    s2 shouldBe SeqOp(
      List(loadINEGICSVs(MyFormat,files)),
      List(SeqOp(List(toDouble("x", "t")),
        List(SeqOp(List(xDouble(1.0E9, "t")),
          List(doubleToLong("t")))))))

    // compile the pipes into executable code
    val c2 = scompile(s2)
    val c2l = c2.toList
    c2l.head should be('right)
    c2l.size shouldBe 1

    // execute the code
    //val r2 = exec(fs2)(Frame(("",List())))
    val r2 = execDebug(c2)(Frame(("", List())))
    val r2l = r2.toList
    r2l.size shouldBe 1
    r2l.head._1 should be('right)

    // get the results
    val r2r: (Either[ADWError, Frame], Executing) = r2.head
    val r2r_traces = r2r._2.history.map(_.trace)
    //println(r2r)
    // this is what was execute
    val expected_r2r = List(
      Trace("UtilTasks.this.doubleToLong$(Map(cols -> ColumnNames(WrappedArray(t))))"),
      Trace("UtilTasks.this.xDouble$(Map(v -> DoubleP(1.0E9), cols -> ColumnNames(WrappedArray(t))))"),
      Trace("UtilTasks.this.toDouble$(Map(cols -> ColumnNames(WrappedArray(x, t))))"),
      Trace(s"TestTasks.this.loadINEGICSVs$$(Map(format -> $MyFormat, fileNames -> FileNames(List(${file1.toString}, ${file2.toString}))))")
     )
    r2r_traces should contain theSameElementsAs expected_r2r
    r2r._1 should be('right)
    val read2 = r2r._1.right.get.iterable.toIterator.take(25).toIndexedSeq
    //println(read2.mkString(";\n"))
    read2(1) shouldBe Row(
      Map(
        "x" -> Val(0.8470701131176883),
        "t" -> Val(195000L),
        "load" -> Val("0"),
        "damage" -> Val("norm"),
        "exp" -> Val("1"),
        "damage_size" -> Val("0"),
        "bearing" -> Val("1"),
        "speed" -> Val("1000")))

    // User specified task

    val stamp = LocalDateTime.parse("2018-05-14 10:49:58.701", format)
    //val data4 = data3((x:Duration) => stamp.plus(x), "t" )
    val s3 = loadINEGICSVs(MyFormat, files) ->:
      toDouble("x", "t") ->:
      xDouble(1e9, "t") ->:
      doubleToLong("t") ->:
      longToDuration("t") ->:
      addDateTimeToDuration(stamp, "t") ->:
      convertLabelValue("damage") ->:
      addCounterColumn("counter") ->:
      renameColumns("damage" -> "label")
    //println(s3)

    // compile new pipes
    val c3 = scompile(s3)
    val c3l = c3.toList
    c3l.head should be('right)
    c3l.size shouldBe 1

    // execute the pipes
    val r3 = execDebug(c3)(Frame(("", List())))

    // get the results
    val r3r: Frame = r3.head._1.right.get
    val read3 = r3r.iterable.toIterator.take(25).toIndexedSeq
    //println(read3.mkString(";\n"))
    read3(1) shouldBe Row(
      Map(
        "x" -> Val(0.8470701131176883),
        "t" -> Val(LocalDateTime.parse("2018-05-14 10:49:58.701195", format)),
        "load" -> Val("0"),
        "label" -> Val(false),
        "counter" -> Val(2L),
        "exp" -> Val("1"),
        "damage_size" -> Val("0"),
        "bearing" -> Val("1"),
        "speed" -> Val("1000")))

    /* Use prediction model */

    // Number of samples to fit model
    val maxSamples = 100
    // sliding window
    val winSize = 20
    val stride = 10
    // Don't evaluate first records used to learn model
    val dropSize = maxSamples / (winSize - stride)
    dropSize shouldBe 10

    val s4 = loadINEGICSVs(MyFormat, files) ->:
      toDouble("x", "t") ->:
      xDouble(1e9, "t") ->:
      doubleToLong("t") ->:
      longToDuration("t") ->:
      addDateTimeToDuration(stamp, "t") ->:
      convertLabelValue("damage") ->:
      addCounterColumn("counter") ->:
      renameColumns("damage" -> "label") ->:
      fitWSD(maxSamples, "x") ->:
      window(winSize, stride) ->: // 39 records
      predictWSD(2.0, "x") ->:
      evaluateWSD(dropSize, "x") // drop 10, so 39 - 10 = 28
    //println(s4)

    // compile new pipes
    val c4 = scompile(s4)
    val c4l = c4.toList
    c4l.head should be('right)
    c4l.size shouldBe 1

    // execute the pipes
    val r4 = execDebug(c4)(Frame(("", List())))

    // get the results
    val r4r: (Either[ADWError, Frame], Executing) = r4.head
    val result: Frame = r4r._1.right.get

    // Same check as in ModelSpec ("Weighted mean + SD model example")
    // We have two files with 200 records each. The first 100 are used to learn
    // the model. We are left with 300 records. Records are grouped by window
    // so get last window's result
    val eval = result.drop(28) // total windows = 39, learning for first 10, so left with 29 windows
    //val read = result("counter", "label", "x_prediction", "x_eval").iterable.iterator.toIndexedSeq
    //val read = eval.iterable.iterator.toIndexedSeq
    //val read = eval("counter", "label", "x_prediction", "x_eval").iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))
    //println(read.size) // 10 - 11

    // Get the last remaining (total) metrics
    implicit val defBinaryMetrics: BinaryClassMetrics = BinaryClassMetrics()
    //val metrics  = eval( MeanWSD.getBinaryClassMetrics _, "x_eval" ).toIndexedSeq
    val metrics = eval((e: BinaryClassMetrics) => e, "x_eval").iterable.toIndexedSeq
    //println(metrics.mkString(";\n"))
    // Lets see if we have the expected performance
    val metric = unpack[BinaryClassMetrics](metrics(0)("x_eval"))
    val precision = metric.fold(_ => Precision(0.0), e => e.precision)
    precision.value should be >= 1.0
    val recall = metric.fold(_ => Recall(0.0), _.recall)
    recall.value should be >= 1.0
    val mcc = metric.fold(_ => MCC(0.0), _.mcc)
    mcc.value should be >= 1.0
    val fp = metric.fold(_ => Double.NaN, e => e.fp)
    fp shouldBe 0.0
    val fn = metric.fold(_ => Double.NaN, e => e.fn)
    fn shouldBe 0.0
    val tp = metric.fold(_ => Double.NaN, e => e.tp)
    tp shouldBe 20.0
    val tn = metric.fold(_ => Double.NaN, e => e.tn)
    tn shouldBe 9.0

    // what was actually executed
    //println(r4r._1)
    val log = List(
      evaluateWSD(10, "x").t,
      predictWSD(2.0, "x").t,
      window(20, 10).t,
      fitWSD(maxSamples, "x").t,
      renameColumns("damage" -> "label").t,
      addCounterColumn("counter").t,
      convertLabelValue("damage").t,
      addDateTimeToDuration(LocalDateTime.parse("2018-05-14 10:49:58.701", format), "t").t,
      longToDuration("t").t,
      doubleToLong("t").t,
      xDouble(1.0E9, "t").t,
      toDouble("x", "t").t,
      loadINEGICSVs(MyFormat, files).t
    )
    r4r._2.history should contain theSameElementsInOrderAs log

  }


  "Data Aggregation" should "execute and collect all results of a pipe definition (linear)" in {
    import scala.language.existentials

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("1", "2", "2"),
      "z" -> Seq("1", "3", "3")
    )

    // Need to reload data on each experiment, otherwise stream is consumed on first execution
    // we generate 4 sub-pipes that set the all values of column "x" to be either 3, 4, 6 or 8
    // we then sum all the values of column x and get 4 DoubleVal results: 9, 12, 18 or 24
    // finally we aggregate all these values and select the one that is maximum: 24
    /*val s0: Op[Any, Val[Map[String, Double]]] = load(testData) ->: toDouble("x", "y", "z") ->:
      all(xDouble(1.0,"x"), xDouble(2.0,"x")) *: all(xDouble(3.0,"x"), xDouble(4.0,"x")) ->:
      sum("x")
    */

    val op: Op[Any, Val[Map[String, Double]]] = load(testData) *:
      toDouble("x", "y", "z") *:
      All(xDouble(1.0, "x"), xDouble(2.0, "x")) *:
      All(xDouble(3.0, "x"), xDouble(4.0, "x")) *:
      sum("x")
    val func: Agg[Val[Map[String, Double]], Either[ADWError, (Executing, Val[_])]] = maxSum("x")

    // Debugging the operator |:
    val s1 = Aggregate(op, func)
    // Example of adding verbose output to execution
    //val p = Par(Func(func.t), parallelism = 0, verbose = 1)(Par.scheduler)
    //val s1 = Aggregate(op, p)

    // compile new pipes
    val c1 = scompile(s1)
    val c1l = c1.toList
    c1l.head should be('right)
    // aggregator takes the 4 result and returns 1
    c1l.size shouldBe 1

    // execute the pipes (one result)
    val r1 = execDebug(c1)(empty)
    val r1l = r1.toList
    r1l.size shouldBe 1
    r1l.head._1 should be('right)

    // get the result
    val r1r: (Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing) = r1l.head
    //println(r1r._2)
    r1r._2.history should contain theSameElementsInOrderAs List(
      maxSum("x").t
    )

    // this is maximum metric value obtained
    r1r._1 should be('right)
    val Right((r1r_1, r1r_2)) = r1r._1.right.get
    r1r_2 shouldBe Val(24.0)
    r1r_1.history should contain theSameElementsInOrderAs List(
      sum("x").t,
      xDouble(4.0, "x").t,
      xDouble(2.0, "x").t,
      toDouble("x", "y", "z").t,
      load(testData).t
    )
  }


  it should "allow use of operator for executing and collecting results of pipes (linear)" in {
    import scala.language.existentials

    val empty = Frame(("",List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("1", "2", "2"),
      "z" -> Seq("1", "3", "3")
    )

    // Need to reload data on each experiment, otherwise stream is consumed on first execution
    // we generate 4 sub-pipes that set the all values of column "x" to be either 3, 4, 6 or 8
    // we then sum all the values of column x and get 4 DoubleVal results: 9, 12, 18 or 24
    // finally we aggregate all these values and select the one that is maximum: 24
    val op: Op[Any, Val[Map[String, Double]]] = load(testData) *:
      toDouble("x", "y", "z") *:
      All(xDouble(1.0, "x"), xDouble(2.0, "x")) *:
      All(xDouble(3.0, "x"), xDouble(4.0, "x")) *:
      sum("x")
    val func: Agg[Val[Map[String, Double]], Either[ADWError, (Executing, Val[_])]] = maxSum("x")

    // Debugging the operator |:
    val s1: Op[Any, Either[ADWError, (Executing, pt.inescn.etl.stream.Load.Val[_])]] = op |: func

    // compile new pipes
    val c1 = scompile(s1)
    val c1l = c1.toList

    // aggregator takes the 4 result and returns 1
    c1l.size shouldBe 1
    c1l.head should be ('right)

    // execute the pipes (one result)
    val r1: Iterable[(Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing)] = execDebug(c1)(empty)
    val r1l = r1.toList
    r1l.size shouldBe 1
    r1l.head._1 should be ('right)

    // get the result
    val r1r: (Either[ADWError, Acc], Executing) = r1l.head
    r1r._2.history should contain theSameElementsInOrderAs List(
      maxSum("x").t
    )

    // this is maximum metric value obtained
    r1r._1 should be ('right)
    val Right((r1r_1,r1r_2)) = r1r._1.right.get
    r1r_2 shouldBe Val(24.0)
    r1r_1.history should contain theSameElementsInOrderAs List(
      sum("x").t,
      xDouble(4.0,"x").t,
      xDouble(2.0,"x").t,
      toDouble("x", "y", "z").t,
      load(testData).t
    )
  }


  it should "execute and collect all results of a pipe definition (parallel)" in {
    import scala.language.existentials

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("1", "2", "2"),
      "z" -> Seq("1", "3", "3")
    )

    // Need to reload data on each experiment, otherwise stream is consumed on first execution
    // we generate 4 sub-pipes that set the all values of column "x" to be either 3, 4, 6 or 8
    // we then sum all the values of column x and get 4 DoubleVal results: 9, 12, 18 or 24
    // finally we aggregate all these values and select the one that is maximum: 24
    /*val s0: Op[Any, Val[Map[String, Double]]] = load(testData) ->: toDouble("x", "y", "z") ->:
      all(xDouble(1.0,"x"), xDouble(2.0,"x")) *: all(xDouble(3.0,"x"), xDouble(4.0,"x")) ->:
      sum("x")
    */

    val op: Op[Any, Val[Map[String, Double]]] = load(testData) *:
      toDouble("x", "y", "z") *:
      All(xDouble(1.0, "x"), xDouble(2.0, "x")) *:
      All(xDouble(3.0, "x"), xDouble(4.0, "x")) *:
      sum("x")
    val func: Agg[Val[Map[String, Double]], Either[ADWError, (Executing, Val[_])]] = maxSum("x")

    val waitFor = Duration.Inf // 5000 millis
    val verbose = 0  // > 0 to print output
    val cores = Runtime.getRuntime.availableProcessors
    val scheduler = monix.execution.Scheduler.Implicits.global  // could be used implicitly

    /* Notes: look at Monix 3.x documentation in
         https://monix.io/docs/3x/execution/scheduler.html
         https://monix.io/docs/3x/execution/scheduler.html#execution-model

       see: https://github.com/atnos-org/eff/issues/93

       The default execution mode is: BatchedExecution. This execution mode promotes
       some sort of cooperative multi-threading: at each batch end, CPU is released
       for other processing. All batched operations are assigned and executed by
       the live-threads. Only at the end of the batch does context switching occur.
       We can set a BatchedExecution with a given batch size: `BatchedExecution(128)`.

       `AlwaysAsyncExecution` behaves like Scala's Future, because each operation
       creates and uses a new Runnable in the thread-pool (context switching always
       happens).

       `SynchronousExecution` behaves like Scalaz's Task, so execution will stay on the
       current thread as long as possible. THis means the thread can be used indefinitely
       and cannot be assigned/used for another operation or task.

       `BatchedExecution` seems to be a good compromise, so we will use that (balance
       between fairness and throughput).

       We can explicitly execute a task in a given execution model so:

       `task.executeWithModel(BatchedExecution(32))`

       Note that Monix allows synchronization. So in batched mode when we end the
       batch synchronization provides a way to collect and return a set of results
       in order (for example via map). In order maximize concurrency we use Monix's
       `mapParallelUnordered` because aggregation does not assume ordering (instead
       of `mapTask`).

     */


    // Debugging the operator |:
    val p1 = Par(func.t, cores, waitFor, verbose, scheduler)
    val s1 = Aggregate(op, p1)


    // compile new pipes
    val c1: Iterable[Either[ADWError, Exec[Any, Either[ADWError, (Executing, Val[_])]]]] = scompile(s1)
    val c1l = c1.toList
    c1l.head should be('right)
    // aggregator takes the 4 result and returns 1
    c1l.size shouldBe 1

    // execute the pipes (one result)
    val r1: Iterable[(Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing)] =
      execDebug(c1)(empty)
    val r1l = r1.toList
    r1l.size shouldBe 1
    r1l.head._1 should be('right)

    // get the result
    val r1r: (Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing) = r1l.head
    r1r._2.history should contain theSameElementsInOrderAs List(
      maxSum("x").t
    )

    // this is maximum metric value obtained
    r1r._1 should be('right)
    val Right((r1r_1, r1r_2)) = r1r._1.right.get
    r1r_2 shouldBe Val(24.0)
    r1r_1.history should contain theSameElementsInOrderAs List(
      sum("x").t,
      xDouble(4.0, "x").t,
      xDouble(2.0, "x").t,
      toDouble("x", "y", "z").t,
      load(testData).t
    )
  }


  it should "allow use of operator for executing and collecting results of pipes (parallel)" in {
    import scala.language.existentials

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("1", "2", "2"),
      "z" -> Seq("1", "3", "3")
    )

    // Need to reload data on each experiment, otherwise stream is consumed on first execution
    // we generate 4 sub-pipes that set the all values of column "x" to be either 3, 4, 6 or 8
    // we then sum all the values of column x and get 4 DoubleVal results: 9, 12, 18 or 24
    // finally we aggregate all these values and select the one that is maximum: 24
    val op: Op[Any, Val[Map[String, Double]]] = load(testData) *:
      toDouble("x", "y", "z") *:
      All(xDouble(1.0, "x"), xDouble(2.0, "x")) *:
      All(xDouble(3.0, "x"), xDouble(4.0, "x")) *:
      sum("x")
    val func: Agg[Val[Map[String, Double]], Either[ADWError, (Executing, Val[_])]] = maxSum("x")

    val waitFor = Duration.Inf // 5000 millis
    val verbose = 0  // > 0 to print output
    val cores = Runtime.getRuntime.availableProcessors
    val scheduler = monix.execution.Scheduler.Implicits.global  // could be used implicitly
    scheduler shouldNot be(null) // just to get rid of the compiler warning not used, but used in compile test below

    import monix.execution.Scheduler.Implicits.global  // used implicitly
    // Parallel execution
    "val p1 = Par(func)" should compile
    "val p2 = Par(func)(scheduler)" shouldNot compile
    "val p3 = Par(func, verbose = verbose)" should compile
    "val agg = Par(func, verbose = verbose, duration = waitFor, parallelism = cores)(scheduler)" should compile
    // Implicit scheduler
    val agg = Par(func, verbose = verbose, duration = waitFor, parallelism = cores)

    // Debugging the operator ||:
    val s1 = op ||: agg

    // compile new pipes
    val c1: Iterable[Either[ADWError, Exec[Any, Either[ADWError, (Executing, Val[_])]]]] = scompile(s1)
    val c1l = c1.toList
    // aggregator takes the 4 result and returns 1
    c1l.size shouldBe 1
    c1.head should be('right)

    // execute the pipes (one result)
    val r1: Iterable[(Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing)] =
      execDebug(c1)(empty)
    val r1l = r1.toList
    r1l.size shouldBe 1
    //println(r1)
    r1l.head._1 should be('right)

    // get the result
    val r1r: (Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing) = r1l.head
    //println(r1r._2)
    r1r._2.history should contain theSameElementsInOrderAs List(
      maxSum("x").t
    )

    // this is maximum metric value obtained
    r1r._1 should be('right)
    val Right((r1r_1, r1r_2)) = r1r._1.right.get
    r1r_2 shouldBe Val(24.0)
    r1r_1.history should contain theSameElementsInOrderAs List(
      sum("x").t,
      xDouble(4.0, "x").t,
      xDouble(2.0, "x").t,
      toDouble("x", "y", "z").t,
      load(testData).t
    )
  }

  "Repeat operator for Cross-Validation" should "allow one to repeat multiple pipelines" in {

    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("1", "2", "2"),
      "z" -> Seq("1", "3", "3")
    )

    val op: Op[Any, Row] = load(testData) *:
      toDouble("x", "y", "z") *:
      All(xDouble(1.0, "x"), xDouble(2.0, "x")) *:
      All(xDouble(3.0, "x"), xDouble(4.0, "x")) *:
      getLastRow("x")

    /*
       IMPORTANT NOTE: the aggregator must not do the CPU intensive processing
       This must be left to the pipeline. More specifically be sure to get the
       last element in the pipe. See the `getLastRow` task.
     */
    def testRepeater(col: String): Agg[Row,Map[String,Double]] = {

      type Acc = Map[String,Double]

      val tsk = new Aggregator[Row,Acc] {
        override def name: String = "sumLastRowColumn"
        override def params: Params = Map("col" -> col)
        override def zero: Acc = Map()

        override def collect(acc: Acc, in: Either[ADWError,Row], ta:Long, trk: Executing): Acc = {
          val tmp = for {
            row <- in
            // _ = println(s"Row($col) = ${row(col)}")
            //_ = println(s"Unpacked Row($col) = ${unpackErr[Double](row(col))}")
            tmp <- unpackErr[Double](row(col))
            id = trk.toString
            sum = acc.get(id).fold(tmp)(v => v +  tmp)
            //_ = println(s"Adding $tmp to accumulator")
            //_ = println(s"(id, sum) = ($id, $sum)")
          } yield acc.updated(id, sum)
          tmp.fold(_ => acc, m => m)
        }
      }

      Agg(tsk)
    }

    val rep = testRepeater("x")
    // Explicit set-up for sequential execution. parallelism <= 1
    val p1 = Par(rep, parallelism = 1, verbose = 0)(Par.scheduler)
    val s1 = Repeat(3, op, p1)

    // compile new pipes
    val c1: Iterable[Either[ADWError, Exec[Any, Map[String, Double]]]] = scompile(s1)
    val c1l = c1.toList
    // repeat takes 1 and returns 1 that repeats 3 times
    c1l.size shouldBe 1
    c1.head should be('right)

    // execute the pipes (one result)
    val r1: Iterable[(Either[ADWError, Map[String, Double]], Executing)] = execDebug(c1)(empty)
    val r1l = r1.toList
    r1l.size shouldBe 1
    //println(r1l)
    r1l.head._1 should be('right)

    // get the result
    val r1r: (Either[ADWError, Map[String, Double]], Executing) = r1l.head
    //println(r1r._2)
    r1r._2.history should contain theSameElementsInOrderAs List(
      rep.t
    )
    r1r._1 should be('right)
    val resultsPerRepeat1 = r1r._1.right.get
    val id = "Executing(List(UtilTasks.this.getLastRow$(Map(cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.xDouble$(Map(v -> DoubleP(3.0), cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.xDouble$(Map(v -> DoubleP(1.0), cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.toDouble$(Map(cols -> ColumnNames(WrappedArray(x, y, z)))), UtilTasks.this.load$(Map(data -> DataFrame(List((x,List(1, 1, 1)), (y,List(1, 2, 2)), (z,List(1, 3, 3))))))))"
    resultsPerRepeat1(id) shouldBe 9 // 3 x 3
    val maxId = "Executing(List(UtilTasks.this.getLastRow$(Map(cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.xDouble$(Map(v -> DoubleP(4.0), cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.xDouble$(Map(v -> DoubleP(2.0), cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.toDouble$(Map(cols -> ColumnNames(WrappedArray(x, y, z)))), UtilTasks.this.load$(Map(data -> DataFrame(List((x,List(1, 1, 1)), (y,List(1, 2, 2)), (z,List(1, 3, 3))))))))"
    resultsPerRepeat1(maxId) shouldBe 24 // 3 x 8


    // Implicit set-up for sequential execution. parallelism <= 1
    val s2 = Repeat(3, op, rep)

    // compile new pipes
    val c2: Iterable[Either[ADWError, Exec[Any, Map[String, Double]]]] = scompile(s2)
    val c2l = c2.toList
    // repeat takes 1 and returns 1 that repeats 3 times
    c2l.size shouldBe 1
    c2.head should be('right)

    // execute the pipes (one result)
    val r2: Iterable[(Either[ADWError, Map[String, Double]], Executing)] = execDebug(c2)(empty)
    val r2l = r2.toList
    r2l.size shouldBe 1
    //println(r2l)
    r2l.head._1 should be('right)

    // get the result
    val r2r: (Either[ADWError, Map[String, Double]], Executing) = r2l.head
    //println(r1r._2)
    r2r._2.history should contain theSameElementsInOrderAs List(
      rep.t
    )
    r2r._1 should be('right)
    val resultsPerRepeat2 = r1r._1.right.get
    resultsPerRepeat2(id) shouldBe 9 // 3 x 3
    resultsPerRepeat2(maxId) shouldBe 24 // 3 x 8

    // parallel execution
    val waitFor = Duration.Inf // 5000 millis
    val verbose = 0  // > 0 to print output
    val cores = Runtime.getRuntime.availableProcessors

    import monix.execution.Scheduler.Implicits.global  // used implicitly
    // Parallel execution
    // Implicit scheduler
    val reps = Par(rep, verbose = verbose, duration = waitFor, parallelism = cores)

    // Implicit set-up for parallel execution.
    // If you uncomment the print line in the collect method, you can see the execution is unordered
    val s3: Op[Any, Map[String, Double]] = Repeat(30, op, reps)

    // compile new pipes
    val c3t: (Iterable[Either[ADWError, Exec[Any, Map[String, Double]]]], Long) = Utils.time(scompile(s3))
    //println(s"compile time = ${c3t._2}")
    val c3 = c3t._1
    val c3l = c3.toList
    // repeat takes 1 and returns 1 that repeats 3 times
    c3l.size shouldBe 1
    c3.head should be('right)

    // execute the pipes (one result)
    val r3t: (Iterable[(Either[ADWError, Map[String, Double]], Executing)], Long) = Utils.time(execDebug(c3)(empty))
    //println(s"execution time = ${r3t._2}")
    val r3 = r3t._1
    val r3l = r3.toList
    r3l.size shouldBe 1
    //println(r2l)
    r3l.head._1 should be('right)
    // Ration runtime versus compile time. Make sure we only execute the pipes after compilation
    val tmp: Double = (r3t._2 * 1.0) / c3t._2
    tmp should be >= 10.0

    // get the result
    val r3r: (Either[ADWError, Map[String, Double]], Executing) = r3l.head
    //println(r1r._2)
    r3r._2.history should contain theSameElementsInOrderAs List(
      rep.t
    )
    r3r._1 should be('right)
    val resultsPerRepeat3 = r3r._1.right.get
    resultsPerRepeat3(id) shouldBe 90 // 30 x 3
    resultsPerRepeat3(maxId) shouldBe 240 // 30 x 8


    // An example of an ML experiment: we repeat a given pipeline 3 times and
    // add the result (usually we would collect stats). Then add the max
    // aggregator to select the best mean result (bst expected result).
    // Note that we use the tracking information to ID the pipelines

    def updateExpResults(in: Map[String,Double], acc: Map[String,Double]): Map[String, Double] = {
      in.foldLeft(acc){ case (acum,(k,v)) =>
        val max = acum.get(k).fold( v )( d => Math.max(v,d) )
        acum.updated(k, max)
      }
    }

    /*
       IMPORTANT NOTE: the aggregator must not do the CPU intensive processing
       This must be left to the pipeline. More specifically be sure to get the
       last element in the pipe. See the `getLastRow` task.
     */
    def testAggregator(): Agg[Map[String,Double],Map[String,Double]] = {

      type Acc = Map[String,Double]

      val tsk = new Aggregator[Map[String,Double],Acc] {
        override def name: String = "maxLastRowColumn"
        override def params: Params = Map()
        override def zero: Acc = Map[String,Double]()

        override def collect(max: Acc, in: Either[ADWError,Map[String,Double]], ta:Long, trk: Executing): Acc = {
          val tmp = for {
            inMap <- in
            newMax = updateExpResults(inMap, max)
          } yield newMax
          tmp.right.get
        }
      }

      Agg(tsk)
    }

    val aggx = Par(testAggregator(), verbose = verbose, duration = waitFor, parallelism = cores)
    val s4 = s3 ||: aggx

    // compile new pipes
    val c4t: (Iterable[Either[ADWError, Exec[Any, Map[String, Double]]]], Long) = Utils.time(scompile(s4))
    //println(s"compile time = ${c3t._2}")
    val c4 = c4t._1
    val c4l = c4.toList
    // agg takes single repeat(30), so 1
    c4l.size shouldBe 1
    c4.head should be('right)

    // execute the pipes (collects all into one result)
    val r4t: (Iterable[(Either[ADWError, Map[String, Double]], Executing)], Long) = Utils.time(execDebug(c4)(empty))
    //println(s"execution time = ${r3t._2}")
    val r4 = r4t._1
    val r4l = r4.toList
    r4l.size shouldBe 1
    //println(r2l)
    r4l.head._1 should be('right)
    // Ration runtime versus compile time. Make sure we only execute the pipes after compilation
    val tmp4: Double = (r3t._2 * 1.0) / c3t._2
    tmp4 should be >= 10.0

    // the original pipe generates 4 experiments, each was repeated 30 times
    // so we collect all 4 experimental results as summarized by the repeat
    r4l.head._1.right.get.size shouldBe 4
    val map4 = r4l.head._1.right.get
    val minId4 = "Executing(List(UtilTasks.this.getLastRow$(Map(cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.xDouble$(Map(v -> DoubleP(3.0), cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.xDouble$(Map(v -> DoubleP(1.0), cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.toDouble$(Map(cols -> ColumnNames(WrappedArray(x, y, z)))), UtilTasks.this.load$(Map(data -> DataFrame(List((x,List(1, 1, 1)), (y,List(1, 2, 2)), (z,List(1, 3, 3))))))))"
    map4(minId4) shouldBe 90 // 30 x 3
    val maxId4 = "Executing(List(UtilTasks.this.getLastRow$(Map(cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.xDouble$(Map(v -> DoubleP(4.0), cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.xDouble$(Map(v -> DoubleP(2.0), cols -> ColumnNames(WrappedArray(x)))), UtilTasks.this.toDouble$(Map(cols -> ColumnNames(WrappedArray(x, y, z)))), UtilTasks.this.load$(Map(data -> DataFrame(List((x,List(1, 1, 1)), (y,List(1, 2, 2)), (z,List(1, 3, 3))))))))"
    map4(maxId4) shouldBe 240 // 30 x 8

  }


  /* TODO:
  it should "deploy a linear pipe definition" in {
    import scala.language.existentials

    val empty = Frame(("",List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("1", "2", "2"),
      "z" -> Seq("1", "3", "3")
    )

    // Need to reload data on each experiment, otherwise stream is consumed on first execution
    // we generate 4 sub-pipes that set the all values of column "x" to be either 3, 4, 6 or 8
    // we then sum all the values of column x and get 4 DoubleVal results: 9, 12, 18 or 24
    // finally we aggregate all these values and select the one that is maximum: 24
    val s1 = load(testData) ->: toDouble("x", "y", "z") ->:
      All(xDouble(1.0,"x"), xDouble(2.0,"x")) *:
      All(xDouble(3.0,"x"), xDouble(4.0,"x")) ->:
      sum("x") |: maxSum("x")

    val ss1: Op[Any, Val[Map[String, Double]], Unit] = load(testData) ->: toDouble("x", "y", "z") ->:
      All(xDouble(1.0,"x"), xDouble(2.0,"x")) *:
        All(xDouble(3.0,"x"), xDouble(4.0,"x")) ->:
      sum("x")

    // compile new pipes
    val c1 = scompile(s1)
    c1 should be ('right)

    // Get pipes (only one)
    // aggregator takes the 4 result and returns 1
    val fs1 = c1.right.get
    fs1.fs.size shouldBe 1

    // execute the pipes (one result)
    val r1: List[(Either[ADWError, Acc], Executing)] = execDebug(fs1)(empty)
    r1.size shouldBe 1
    //println(r1)
    r1.head._1 should be ('right)

    // get the result
    val r1r: (Either[ADWError, Acc], Executing) = r1.head
    //println(r1r._2)
    r1r._2.history should contain theSameElementsInOrderAs List(
      maxSum("x").t
    )

    // this is maximum metric value obtained
    r1r._1 should be ('right)
    val Right((r1r_1,r1r_2)) = r1r._1.right.get
    r1r_2 shouldBe Val(24.0)
    r1r_1.history should contain theSameElementsInOrderAs List(
      maxSum("x").t,
      sum("x").t,
      xDouble(4.0,"x").t,
      xDouble(2.0,"x").t,
      toDouble("x", "y", "z").t,
      load(testData).t
    )

    /* TODO:
    // Now deploy the best pipe
    val s2 = deploy(r1r._1.history.drop(1))
    //println(s2)

    // compile new pipes
    val c2 = scompile(s2)
    c2 should be ('right)

    // Get pipes (only one)
    // aggregator selects only one pipe
    val fs2 = c2.right.get
    fs2.fs.size shouldBe 1
    //println(fs2.tracks)
    fs2.tracks.head.history should contain theSameElementsInOrderAs List(
      load(testData),
      toDouble(ColumnNames(Seq("x", "y", "z"))),
      xDouble(2.0,ColumnNames(Seq("x"))),
      xDouble(4.0,ColumnNames(Seq("x"))),
      sum(ColumnNames(Seq("x")))
    )

    // execute the pipes (one result)
    val r2: List[Either[ADWError, (Executing, Val[_])]] = execDebug(fs2)(empty)
    r2.size shouldBe 1
    //println(r2)
    r2.head should be ('right)

    // get the result
    val r2r: (Pipes.Executing, Val[_]) = r2.head.right.get
    //println(r2r._2)
    r2r._1.history should contain theSameElementsInOrderAs List(
      sum(ColumnNames(Seq("x"))),
      xDouble(4.0,ColumnNames(Seq("x"))),
      xDouble(2.0,ColumnNames(Seq("x"))),
      toDouble(ColumnNames(Seq("x", "y", "z"))),
      load(testData)
    )

    // this is maximum metric value obtained
    r2r._2 shouldBe Val(Map("x" -> 24.0))
    */
  }
*/

}
