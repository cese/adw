package pt.inescn.search.stream

//import java.io.{FileOutputStream, ObjectOutputStream, Serializable}
import java.io.Serializable

/*import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.Pipes._
import pt.inescn.search.stream.OpCheck._
*/

/**
  * // Object serialization
  * @see https://gist.github.com/ramn/5566596
  * @see https://alvinalexander.com/scala/how-to-use-serialization-in-scala-serializable-trait
  * @see https://gist.github.com/laughedelic/634f1a1e5333d58085603fcff317f6b4
  * @see https://stackoverflow.com/questions/39369319/convert-any-type-in-scala-to-arraybyte-and-back/39371571#39371571
  * // Scala pickling ?
  *
  * @see https://medium.com/@dkomanov/scala-serialization-419d175c888a
  * @see http://lampwww.epfl.ch/~hmiller/pickling/
  *
  * // Checking serialization dependencies
  * @see https://github.com/scalacenter/spores
  * @see http://scalacenter.github.io/spores/
  * @see https://www.scala-lang.org/blog/2016/11/30/spores-release.html
  *
  * // JSON
  * @see https://index.scala-lang.org/suzaku-io/boopickle/boopickle-shapeless/1.3.0?target=_2.12
  * @see https://github.com/suzaku-io/boopickle
  * @see https://github.com/lihaoyi/upickle
  * @see https://github.com/benhutchison/prickle
  * see also Circe
  *
  * // Automatically wrap functions?
  * https://scalameta.org/
  * https://scalameta.org/tutorial/
  * https://github.com/scalameta/scalameta
  * https://github.com/scalameta
  */
object testSerialize {
  sealed trait Event extends Serializable

  case class Foo(i: Int) extends Event
  case class Bar(s: String) extends Event
  case class Baz(c: Char) extends Event
  case class Qux(values: List[String]) extends Event
  //case class Funky(f: Int => Int) extends Event // cannot serialize to JSON, need objec serializtion (pickle)

  case class Person(name: String, age: Int)

  import better.files._
  //import better.files.File
  import java.lang.{System => JSystem}
  //import java.io.{File => JFile}
  //import File._

  val tmp = File(JSystem.getProperty("java.io.tmpdir"))
  val file: File = tmp / "pipe1.bin"

  //val fos = new FileOutputStream(file.path.toString)
  //val oos = new ObjectOutputStream(fos)
  val foo100 = Foo(100)
  //val funk2x = Funky( i => 2*i)
  val person = Person("Chris", 24)

  // we can serialize functions
  // See: https://github.com/pathikrit/better-files#java-serialization-utils
  //file.newOutputStream.asObjectOutputStream.serialize(funk2x).flush()
  //file.newOutputStream.asObjectOutputStream.serialize(foo100).flush()
  //file.newOutputStream.asObjectOutputStream.serialize(person).flush()
  //oos.serialize(person).flush()

  // Ee can deserialize functions
  //val funk = file.newInputStream.asObjectInputStream.deserialize[Funky]
  //println(funk.f(100))
  import io.circe.parser.decode, io.circe.syntax._

  import io.circe.{ Decoder, Encoder }, io.circe.generic.auto._

  object ShapesDerivation {
    //import io.circe.shapes
    import shapeless.{ Coproduct, Generic }

    implicit def encodeAdtNoDiscr[A, Repr <: Coproduct](implicit
                                                        gen: Generic.Aux[A, Repr],
                                                        encodeRepr: Encoder[Repr]
                                                       ): Encoder[A] = encodeRepr.contramap(gen.to)

    implicit def decodeAdtNoDiscr[A, Repr <: Coproduct](implicit
                                                        gen: Generic.Aux[A, Repr],
                                                        decodeRepr: Decoder[Repr]
                                                       ): Decoder[A] = decodeRepr.map(gen.from)

  }

  import ShapesDerivation._

  println((Foo(100): Event).asJson.noSpaces)
  println(Foo(100).asJson.noSpaces)
  decode[Event]("""{ "i": 1000 }""")

  sealed trait Taski
  case class Nopi() extends Taski

  /*
  // This breaks
  object Nopi {
    def apply(): Taski = new Nopi()
  }*/

  (new Nopi():Taski).asJson

  /*
  object Nopi {
    def apply(): Option[Taski] = Some(new Nopi())
  }

  (Nopi():Option[Taski]).asJson
  */

}
