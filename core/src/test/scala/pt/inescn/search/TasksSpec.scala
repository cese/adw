package pt.inescn.search

import org.scalatest._
import pt.inescn.search.Pipes.{All, Exec, Runtime, exec, all => allP, compile => compileP}
import pt.inescn.search.Tasks._
import pt.inescn.utils.ADWError

/**
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.search.TasksSpec"
  */
class TasksSpec extends FlatSpec with Matchers with Inside {

  "Tasks" should "allow compilation and execution of a single normalize function" in {

    // This is a normalization task. It is designed for use with the Pipes
    // This is a normalization task. It is designed for use with the Pipes
    // The results are automatically accumulated and recorded as Json strings
    // By default, when the data normalizer receive no parameters (for
    // example, during the learning phase) it calculates the parameters mean and
    // standard deviation. These are collected and recorded for later use (for
    // example during deployment)
    val t1 = Normalize()

    // We could specifically set and store the task parameters
    val pr1 = Some((100.0,0.3))

    // Lets say we want to normalize this vector
    val input = Array(1.0,2,3,4,5.0)

    // However, because the base Task trait uses generic parameters, we
    // have to be able to convert the Task to Jason explicitly. Note that
    // we can also set the parameters for later use
    val t1j2 = t1.defaultOf(pr1, input)
    //println(t1j2)
    // We now have access to the calculated parameters
    t1j2 should be ('defined)
    t1j2 should be (pr1)

    val t1j3 = t1.defaultOf(None, input)
    //println(t1j3)
    // We now have access to the calculated parameters
    t1j3 should be ('defined)
    t1j3.get should be ((3.0,1.5811388300841898))

    // We can get the function that does the actual normalization
    // Note that we do not provide the parameters, so in this case the mean
    // and standard deviation will be calculated and used during normalization
    val t1o: Either[ADWError, Vec] = t1.exec(t1j3,input)
    //println(t1o)
    //println(t1o._2.mkString(","))
    // We also have access to the normalized data
    t1o should be ('right)
    t1o.right.get should contain theSameElementsInOrderAs Array(
      -1.2649110640673518,-0.6324555320336759,0.0,0.6324555320336759,1.2649110640673518)

    // Now lets prepare the task for use in a pipeline
    // First get a pipe description using the function description
    val t1f: All[Vec, Nothing, Vec] = t1.toFunc
    // Compile the pipe into a function ready for execution
    val t1c: Either[ADWError, Exec[Vec, Vec]] = compileP(t1f)
    //println(t1c)
    t1c should be ('right)
    t1c.right.get.fs.size shouldBe 1
    // Now execute the function if no error occurred
    val t1r1: Either[ADWError, Seq[(Pipes.Runtime, Vec)]] = t1c.flatMap(f => Right( exec(List(input),f) ))
    //println(t1r1)
    t1r1 should be ('right)
    t1r1.right.get.length shouldBe 1
    val t1r1s: (Pipes.Runtime, Vec) = t1r1.right.get.head
    // Now that the pipe is executed lets check the output
    t1r1s._2 should contain theSameElementsInOrderAs Array(
      -1.2649110640673518,-0.6324555320336759,0.0,0.6324555320336759,1.2649110640673518)
  }


  it should "allow compilation and execution of a function that returns multiple parameters/results" in {

    // This is a scaling task. It is designed for use with the Pipes
    // The results are automatically accumulated and recorded as Json strings
    // By default, when the data scaler receives no parameters (for
    // example, during the learning phase) it uses the default upper (1.0)
    // and lower (-1.0) ranges. These are collected and recorded for later
    // use (for example during deployment)
    val t1 = Scale((0.0,1.0),(-1.0,0.0),(-1.0,1.0))

    // Lets say we want to scale this vector
    val input = Array(1.0,2,3,4,5.0)

    // Lets first prepare the task for use in a pipeline
    // First get a pipe description using the function description
    val t1f: Pipes.All[Vec, Nothing, Vec] = t1.toFunc
    // Compile the pipe into a function ready for execution
    val t1c: Either[ADWError, Exec[Vec, Vec]] = compileP(t1f)
    //println(t1c)
    t1c should be ('right)
    //println(t1c.right.get.fs.mkString(","))
    t1c.right.get.fs.size shouldBe 3
    // Now execute the function if no error occurred
    val t1r1: Either[ADWError, Seq[(Pipes.Runtime, Vec)]] = t1c.flatMap(f => Right( exec(List(input),f) ))
    //println(t1r1)
    t1r1 should be ('right)
    // e get three results, one for each parameter
    t1r1.right.get.length shouldBe 3
    // Now that the pipe is executed lets check the output
    val t1r1s: (Pipes.Runtime, Vec) = t1r1.right.get.head
    t1r1s._2 should contain theSameElementsInOrderAs Array(-1.0, -0.5, 0.0, 0.5, 1.0)
    val t1r2s: (Pipes.Runtime, Vec) = t1r1.right.get(1)
    t1r2s._2 should contain theSameElementsInOrderAs  Array(-1.0, -0.75, -0.5, -0.25, 0.0)
    val t1r3s: (Pipes.Runtime, Vec) = t1r1.right.get(2)
    t1r3s._2 should contain theSameElementsInOrderAs  Array(0.0, 0.25, 0.5, 0.75, 1.0)

    // This is a unit task. It is designed for use with the Pipes
    // The results are automatically accumulated and recorded as Json strings
    // The unit length data function receives no parameters. If any are provided
    // they will be ignored. Only one result is returned.
    val t2 = UnitLen()

    val jsont2p1: String = t2.toString
    //println(jsont2p1)
    jsont2p1 shouldBe """UnitLen(List(None),Unit)"""

    // Now lets prepare the task for use in a pipeline
    // First get a pipe description using the function description
    val t2f: All[Vec, Nothing, Vec] = t2.toFunc
    // Compile the pipe into a function ready for execution
    val t2c: Either[ADWError, Exec[Vec, Vec]] = compileP(t2f)
    //println(t2c)
    t2c should be ('right)
    t2c.right.get.fs.size shouldBe 1
    // Now execute the function if no error occurred
    val t2r1: Either[ADWError, Seq[(Pipes.Runtime, Vec)]] = t2c.flatMap(f => Right( exec(List(input),f) ))
    //println(t2r1)
    t2r1 should be ('right)
    t2r1.right.get.length shouldBe 1
    val t2r1s: (Pipes.Runtime, Vec) = t2r1.right.get.head
    // Now that the pipe is executed lets check the output
    t2r1s._2 should contain theSameElementsInOrderAs Array(0.13483997249264842,
      0.26967994498529685, 0.40451991747794525, 0.5393598899705937, 0.674199862463242)


    // This is a softmax task. It is designed for use with the Pipes
    // The results are automatically accumulated and recorded as Json strings
    // The softmax function receives no parameters. If any are provided
    // they will be ignored. Only one result is returned.
    val t3 = Softmax()

    val jsont3p1: String = t3.toString
    //println(jsont3p1)
    jsont3p1 shouldBe """Softmax(List(None),Softmax)"""

    // Now lets prepare the task for use in a pipeline
    // First get a pipe description using the function description
    val t3f: All[Vec, Nothing, Vec] = t3.toFunc
    // Compile the pipe into a function ready for execution
    val t3c: Either[ADWError, Exec[Vec, Vec]] = compileP(t3f)
    //println(t3c)
    t3c should be ('right)
    t3c.right.get.fs.size shouldBe 1
    // Now execute the function if no error occurred
    val t3r1: Either[ADWError, Seq[(Pipes.Runtime, Vec)]] = t3c.flatMap(f => Right( exec(List(input),f) ))
    //println(t3r1)
    t3r1 should be ('right)
    t3r1.right.get.length shouldBe 1
    val t3r1s: (Pipes.Runtime, Vec) = t3r1.right.get.head
    // Now that the pipe is executed lets check the output
    t3r1s._2 should contain theSameElementsInOrderAs Array(0.011656230956039607,
      0.03168492079612427, 0.0861285444362687, 0.23412165725273662, 0.6364086465588308)


    // This is a softLambdamax task. It is designed for use with the Pipes
    // The results are automatically accumulated and recorded as Json strings
    // The softmax function receives the following parameters: mean, standard
    // deviation and lambda value. If we only supply a single value, it is
    // the lambda value and the mean and standard deviation are calculated.
    // If all values are provided they are all used. If at least the lambda
    // value of the mean and standard deviation are not provided, all three
    // values are returned
    val t4 = SoftLambdaMax(0.5,1.0,2.0)

    val jsont4p1: String = t4.toString
    //println(jsont4p1)
    jsont4p1 shouldBe """SoftLambdaMax(List(Some((NaN,NaN,0.5)), Some((NaN,NaN,1.0)), Some((NaN,NaN,2.0))),SoftLambdaMax)"""

    // Now lets prepare the task for use in a pipeline
    // First get a pipe description using the function description
    val t4f: All[Vec, Nothing, Vec] = t4.toFunc
    // Compile the pipe into a function ready for execution
    val t4c: Either[ADWError, Exec[Vec, Vec]] = compileP(t4f)
    //println(t4c)
    t4c should be ('right)
    t4c.right.get.fs.size shouldBe 3
    // Now execute the function if no error occurred
    val t4r1: Either[ADWError, Seq[(Pipes.Runtime, Vec)]] = t4c.flatMap(f => Right( exec(List(input),f) ))
    //println(t4r1)
    t4r1 should be ('right)
    t4r1.right.get.length shouldBe 3
    // Now that the pipe is executed lets check the output
    val t4r1s: (Pipes.Runtime, Vec) = t4r1.right.get.head
    t4r1s._2 should contain theSameElementsInOrderAs Array(0.01845422504617577,
      0.12058334169804585, 0.5, 0.879416658301954, 0.9815457749538243)
    val t4r2s: (Pipes.Runtime, Vec) = t4r1.right.get(1)
    t4r2s._2 should contain theSameElementsInOrderAs Array(3.533597017258217E-4,
      0.01845422504617577, 0.5, 0.9815457749538243, 0.9996466402982741)
    val t4r3s: (Pipes.Runtime, Vec) = t4r1.right.get(2)
    t4r3s._2 should contain theSameElementsInOrderAs Array(1.249513531458961E-7,
      3.533597017258217E-4, 0.5, 0.9996466402982741, 0.9999998750486468)

    // This is a softLambdamax task. It is designed for use with the Pipes
    // The results are automatically accumulated and recorded as Json strings
    // The softmax function receives the following parameters: mean, standard
    // deviation and lambda value. If we only supply a single value, it is
    // the lambda value and the mean and standard deviation are calculated.
    // If all values are provided they are all used. If at least the lambda
    // value of the mean and standard deviation are not provided, all three
    // values are returned
    val t5 = Logit()

    val jsont5p1: String = t5.toString
    //println(jsont5p1)
    jsont5p1 shouldBe """Logit(List(None),Logit)"""

    // Now lets prepare the task for use in a pipeline
    // First get a pipe description using the function description
    val t5f: All[Vec, Nothing, Vec] = t5.toFunc
    // Compile the pipe into a function ready for execution
    val t5c: Either[ADWError, Exec[Vec, Vec]] = compileP(t5f)
    //println(t5c)
    t5c should be ('right)
    t5c.right.get.fs.size shouldBe 1
    // Now execute the function if no error occurred
    val t5r1: Either[ADWError, Seq[(Pipes.Runtime, Vec)]] = t5c.flatMap(f => Right( exec(List(input),f) ))
    //println(t5r1)
    t5r1 should be ('right)
    t5r1.right.get.length shouldBe 1
    // Now that the pipe is executed lets check the output
    val t5r1s: (Pipes.Runtime, Vec) = t5r1.right.get.head
    t5r1s._2 should contain theSameElementsInOrderAs Array(0.7310585786300049,
      0.8807970779778823, 0.9525741268224334, 0.9820137900379085, 0.9933071490757153)

  }

  it should "allow compilation and execution of a set of functions" in {

    // Lets assume we have loaded a data set (data-frame): (X,Y)
    val mat = IndexedSeq(Array(1.0,2,3,4,5), Array(10.0, 20.0, 30.0, 40.0, 50.0))

    // We want to normalize the Y values
    val t1: PipeTask[Param2, Vec, Vec] = Normalize()
    val t2: PipeTask[Param2, Vec, Vec] = Scale((0.0,1.0),(-1.0,0.0),(-1.0,1.0))
    val t3: PipeTask[Param0, Vec, Vec] = UnitLen()
    val t4: PipeTask[Param0, Vec, Vec] = Softmax()
    val t5: PipeTask[Param3, Vec, Vec] = SoftLambdaMax(0.5,1.0,2.0)
    val t6: PipeTask[Parami2, Vec2, Vec2] = RegressionDetrend(x=0,y=1)
    val t7: PipeTask[Parami2, Vec2, Vec2] = DifferenceDetrend(x=0,y=1)

    // Indicate that we want to apply Normalize to column 1 only
    val t1a: PipeTask[List[Param2], Vec2, Vec2] = ApplyColumns1(t1, 1)

    // Generate and compile the function
    val t1v = t1a.toFunc
    val t1c = compileP(t1v)
    //println(t1c)
    t1c should be ('right)
    t1c.right.get.fs.size shouldBe 1
    // Now execute the function if no error occurred
    val t1r1: Either[ADWError, Seq[(Pipes.Runtime, Vec2)]] = t1c.flatMap(f => Right( exec(List(mat),f) ))
    //println(t1r1)
    t1r1 should be ('right)
    t1r1.right.get.length shouldBe 1
    val t1r1s: (Pipes.Runtime, Vec2) = t1r1.right.get.head
    // Now that the pipe is executed lets check the output
    t1r1s._2.head should contain theSameElementsInOrderAs Array(1,2,3,4,5)
    t1r1s._2(1) should contain theSameElementsInOrderAs Array(
      -1.2649110640673518,-0.6324555320336759,0.0,0.6324555320336759,1.2649110640673518)

    // We could also apply any of the normalization functions to the matrix
    // Because these functions can only be applied per column, we have to
    // indicate which columns one must apply these functions
    val t2v: All[Vec2, Nothing, Vec2] = ApplyColumns1(t2, 1).toFunc
    val t3v: All[Vec2, Nothing, Vec2] = ApplyColumns1(t3, 1).toFunc
    val t4v: All[Vec2, Nothing, Vec2] = ApplyColumns1(t4, 1).toFunc
    val t5v: All[Vec2, Nothing, Vec2] = ApplyColumns1(t5, 1).toFunc

    val phase1: All[Vec2, Nothing, Vec2] = allP(t1v,t2v,t3v,t4v,t5v)
    val ph1c = compileP(phase1)
    //println(ph1c)
    ph1c should be ('right)
    //println(ph1c.right.get.fs.mkString(","))
    ph1c.right.get.fs.size shouldBe 9
    // Now execute the function if no error occurred
    val ph1r1: Either[ADWError, Seq[(Pipes.Runtime, Vec2)]] = ph1c.flatMap(f => Right( exec(List(mat),f) ))
    //println(ph1r1)
    ph1r1 should be ('right)
    ph1r1.right.get.length shouldBe 9 // 3 softmax lambda and 3 scale + unit + normalize + softmax
    //println(ph1r1.right.get.mkString(";\n"))
    val ph1r1s: (Pipes.Runtime, Vec2) = ph1r1.right.get.head
    // Now that the pipe is executed lets check the output
    //println(ph1r1s._1)
    ph1r1s._2.head should contain theSameElementsInOrderAs Array(1,2,3,4,5)
    ph1r1s._2(1) should contain theSameElementsInOrderAs Array(0.018454225046175762,
      0.12058334169804585, 0.5, 0.879416658301954, 0.9815457749538243)
    val ph1r2s: (Pipes.Runtime, Vec2) = ph1r1.right.get(1)
    ph1r2s._2.head should contain theSameElementsInOrderAs Array(1,2,3,4,5)
    ph1r2s._2(1) should contain theSameElementsInOrderAs Array(3.533597017258214E-4,
      0.018454225046175762, 0.5, 0.9815457749538243, 0.9996466402982741)
    val ph1r3s: (Pipes.Runtime, Vec2) = ph1r1.right.get(2)
    ph1r3s._2.head should contain theSameElementsInOrderAs Array(1,2,3,4,5)
    ph1r3s._2(1) should contain theSameElementsInOrderAs Array(1.249513531458959E-7,
      3.533597017258214E-4, 0.5, 0.9996466402982741, 0.9999998750486468)
    val ph1r8s: (Pipes.Runtime, Vec2) = ph1r1.right.get(8)
    ph1r8s._2.head should contain theSameElementsInOrderAs Array(1,2,3,4,5)
    ph1r8s._2(1) should contain theSameElementsInOrderAs Array(-1.2649110640673518,
      -0.6324555320336759, 0.0, 0.6324555320336759, 1.2649110640673518)


    // These functions work directly on the matrix, so no apply is necessary
    val t6v: All[Vec2, Nothing, Vec2] = t6.toFunc
    val t7v: All[Vec2, Nothing, Vec2] = t7.toFunc
    val phase2: All[Vec2, Nothing, Vec2] = allP(t6v,t7v)

    // Perform all combinations of pre-processing and then detrending
    val f01 = phase1 *: phase2
    val cf01: Either[ADWError, Exec[Vec2, Vec2]] = compileP(f01)
    //println(cf01)
    cf01.isRight shouldBe true
    val cf01e: Exec[Vec2, Vec2] = cf01.right.get
    val cf01r: Seq[(Pipes.Runtime, Vec2)] = exec(List(mat),cf01e)
    cf01r.length shouldBe (9*2) // 3 softmax lambda and 3 scale + unit + normalize + softmax
    //println(cf01r)
    val traces = cf01r.map( _._1 )
    //println(traces)
    // theSameElementsInOrderAs
    // Note that we now have access to the updated parameters
    traces.map(_.lstIDs) should contain theSameElementsAs List(
      List("ApplyColumns1.SoftLambdaMax(Some(List((30.0,15.811388300841896,2.0))))", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.SoftLambdaMax(Some(List((30.0,15.811388300841896,2.0))))", "RegressionDetrend(Some((0,1)))"),
      List("ApplyColumns1.SoftLambdaMax(Some(List((30.0,15.811388300841896,1.0))))", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.SoftLambdaMax(Some(List((30.0,15.811388300841896,1.0))))", "RegressionDetrend(Some((0,1)))"),
      List("ApplyColumns1.SoftLambdaMax(Some(List((30.0,15.811388300841896,0.5))))", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.SoftLambdaMax(Some(List((30.0,15.811388300841896,0.5))))", "RegressionDetrend(Some((0,1)))"),
      List("ApplyColumns1.Softmax(None)", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.Softmax(None)", "RegressionDetrend(Some((0,1)))"),
      List("ApplyColumns1.Unit(None)", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.Unit(None)", "RegressionDetrend(Some((0,1)))"),
      List("ApplyColumns1.Scale(Some(List((-1.0,1.0))))", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.Scale(Some(List((-1.0,1.0))))", "RegressionDetrend(Some((0,1)))"),
      List("ApplyColumns1.Scale(Some(List((-1.0,0.0))))", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.Scale(Some(List((-1.0,0.0))))", "RegressionDetrend(Some((0,1)))"),
      List("ApplyColumns1.Scale(Some(List((0.0,1.0))))", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.Scale(Some(List((0.0,1.0))))", "RegressionDetrend(Some((0,1)))"),
      List("ApplyColumns1.Normalize(Some(List((30.0,15.811388300841896))))", "DifferenceDetrend(Some((0,1)))"),
      List("ApplyColumns1.Normalize(Some(List((30.0,15.811388300841896))))", "RegressionDetrend(Some((0,1)))")
    )

    // Now lets select one of the the pipes generated above. We copy that trace
    //println(traces.head)
    val track2 = Runtime(traces.head)
    // Create that single pipe from the Pipes descriptor.
    val cc4: Either[ADWError, Exec[Vec2, Vec2]] = compileP(f01, track2)
    //println(s"cc4 = $cc4")
    cc4 should be ('right)
    val cc4e: Exec[Vec2, Vec2] = cc4.right.get
    //println(s"cc4e.track.length = ${cc4e.track.length}")
    // Make sure we get only one pipe back
    cc4e.track.length shouldBe 1
    // Since we deployed a trace we get (nearly) the same Runtime trace
    val track0: Pipes.Tracking = cc4e.track.head
    //println(track0)
    // We get this
    track0 shouldBe a[Runtime]
    val run0 = track0.asInstanceOf[Runtime]
    // Traces are not quite the same, when we deploy we reuse the
    // previously updated parameters because no input is available.
    // This input is now missing (replaced with a ?).
    //println(s"run0.all = ${run0.all}")
    //println(s"track2.all = ${track2.all}")
    // We cannot use diff or intersect to check and test the differences
    // because the vector output shows addresses that contain characters.
    // These vary between calls and sometimes match the base case
    //println(s"run0.all intersect track2.all = ${run0.all intersect track2.all}")
    // But the trace with the default parameters match (no input shown)
    //println(s"run0.lstIDs = ${run0.lstIDs}")
    //println(s"track2.lstIDs = ${track2.lstIDs}")
    run0.lstIDs should contain theSameElementsAs track2.lstIDs
    // And the trace with the updated parameters match (use the ones set previously)
    //println(s"run0.lstDefaultIDs = ${run0.lstDefaultIDs}")
    //println(s"track2.lstDefaultIDs = ${track2.lstDefaultIDs}")
    run0.lstDefaultIDs should contain theSameElementsAs track2.lstDefaultIDs

    // Now we re-execute the deployed pipe that should be the same as the original one
    val cc4er: Seq[(Pipes.Runtime, Vec2)] = exec(List(mat),cc4e)
    cc4er.length shouldBe 1 // One only, the pipe we selected
    //println(cc4er)
    val trace1 = cc4er.head._1
    // We have the same trace
    trace1.lstIDs should contain theSameElementsAs track2.lstIDs
    trace1.lstDefaultIDs should contain theSameElementsAs track2.lstDefaultIDs
    val traceOriginalResult = cf01r.head._2
    val traceDeployResult = cc4er.head._2
    // And the same results
    //println(traceOriginalResult.map( _.mkString(",") + ";\n").foldLeft("")(_+_))
    //println(traceDeployResult.map( _.mkString(",") + ";\n").foldLeft("")(_+_))
    traceDeployResult should contain theSameElementsInOrderAs  traceOriginalResult
  }

}
