/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.search

import java.time

import org.hipparchus.random.RandomDataGenerator
import org.scalatest._
import pt.inescn.eval.{BinaryClassificationMetrics, _}
import pt.inescn.eval.Base.BinaryClassificationEvalDouble
import pt.inescn.features.Stats.DescriptiveStats.Values
import pt.inescn.features.Stats._
import pt.inescn.models.Algorithms
import pt.inescn.models.Base._
import pt.inescn.models.Data.{DenseMatrixData, MatrixData}
import pt.inescn.search.ParamSearch._
import pt.inescn.samplers.Base._
import pt.inescn.samplers.Distribution
import pt.inescn.samplers.Distribution._
import pt.inescn.samplers.Enumeration.{EmptyRange, ListRange, Single, ValidRange}
import pt.inescn.utils.{ADWError, Utils}

import scala.collection.mutable
import scala.util.Random

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.search.ParamSearchSpec"
  *
  * Testing issue: we have random objects tha are not case values and cannot
  * be compared. We cannot compare instantiate objects that because they cannot
  * be unapply and recursively compared. This occurs in the random
  * distributions that use a [[org.hipparchus.random.RandomDataGenerator]]. We
  * can however parse the elements we need to using Scalatest "inside". For
  * examples of use "inside":
  * @see http://www.scalatest.org/user_guide/other_goodies
  *
  * @see http://www.scalatest.org/user_guide/using_matchers#checkingThatCodeDoesNotCompile
  *
  * Created by hmf on 05-06-2017.
  */
class ParamSearchSpec extends FlatSpec with Matchers with Inside {

  "Simple grid search strategy" should "generate typed values" in {
    val p1: Param[Int] = IntRange(1, 10)
    val p2: Param[Char] = Items(List('a', 'b', 'c'))
    val p3: Composite[Int, Char] = Composite(p1, p2)

    val q1 = Constant("kernel 1")
    val q2 = Constant("kernel 2")
    val q3 = Constant("kernel 3")
    val q4 = Items(List("k1p1", "k1p2", "p3"))
    val q5 = Items(List("k2p1", "k2p2", "p3"))
    val q6 = Items(List("k3p1", "k3p2", "k3p4"))
    val q7: Seq[(Constant[String], Items[String])] = List((q1,q4), (q2,q5), (q3,q6))
    val q8: PairThese[String, String] = PairThese(q7)

    val numSamples = 9

    val s1: Sampler[Finite, Int] = Strategy.grid(numSamples)(p1)
    val s2: Sampler[Finite, Char] = Strategy.grid(numSamples)(p2)
    val s3: Sampler[Finite, (Int, Char)] = Strategy.grid(numSamples)(p3)
    val s4: Sampler[Finite, (Int, (Int, Char))] = Strategy.grid(numSamples)(Composite(p1,p3))
    val s5: Sampler[Finite, (String, String)] = Strategy.grid(numSamples)(q8)
    /*
    println(s1)
    println(s2)
    println(s3)
    println(s4)
    println(s5)
    */
    s1 shouldBe ValidRange(1,10,1,0)
    s2 shouldBe ListRange(List('a','b','c'),List('a','b','c'))
    s3 shouldBe Cartesian(ValidRange(1,10,1,0),ListRange(List('a','b','c'),List('a','b','c')))
    s4 shouldBe Cartesian(ValidRange(1,10,1,0),
                          Cartesian(ValidRange(1,10,1,0),ListRange(List('a','b','c'),List('a','b','c'))))
    s5 shouldBe AppendB(
      Vector(
        Cartesian(Single("kernel 1"),ListRange(List("k1p1", "k1p2", "p3"),List("k1p1", "k1p2", "p3"))),
        Cartesian(Single("kernel 2"),ListRange(List("k2p1", "k2p2", "p3"),List("k2p1", "k2p2", "p3"))),
        Cartesian(Single("kernel 3"),ListRange(List("k3p1", "k3p2", "k3p4"),List("k3p1", "k3p2", "k3p4")))))

    val v1 = s1.toList
    //println(v1.mkString(","))
    v1 should contain theSameElementsInOrderAs List(10,9,8,7,6,5,4,3,2,1)

    val v2 = s2.toList
    //println(v2.mkString(","))
    v2 should contain theSameElementsInOrderAs List('c','b','a')

    val v3 = s3.toList
    //println(v3.mkString(","))
    v3 should contain theSameElementsInOrderAs List(
      (10,'c'),
      (10,'b'),
      (10,'a'),
      (9,'c'),
      (9,'b'),
      (9,'a'),
      (8,'c'),
      (8,'b'),
      (8,'a'),
      (7,'c'),
      (7,'b'),
      (7,'a'),
      (6,'c'),
      (6,'b'),
      (6,'a'),
      (5,'c'),
      (5,'b'),
      (5,'a'),
      (4,'c'),
      (4,'b'),
      (4,'a'),
      (3,'c'),
      (3,'b'),
      (3,'a'),
      (2,'c'),
      (2,'b'),
      (2,'a'),
      (1,'c'),
      (1,'b'),
      (1,'a'))

    val v4 = s4.toList
    //println(v4.mkString(","))
    v4 should contain theSameElementsInOrderAs List(
      (10,(10,'c')),(10,(10,'b')),(10,(10,'a')),
      (10,(9,'c')),(10,(9,'b')),(10,(9,'a')),
      (10,(8,'c')),(10,(8,'b')),(10,(8,'a')),
      (10,(7,'c')),(10,(7,'b')),(10,(7,'a')),
      (10,(6,'c')),(10,(6,'b')),(10,(6,'a')),
      (10,(5,'c')),(10,(5,'b')),(10,(5,'a')),
      (10,(4,'c')),(10,(4,'b')),(10,(4,'a')),
      (10,(3,'c')),(10,(3,'b')),(10,(3,'a')),
      (10,(2,'c')),(10,(2,'b')),(10,(2,'a')),
      (10,(1,'c')),(10,(1,'b')),(10,(1,'a')),
      (9,(10,'c')),(9,(10,'b')),(9,(10,'a')),
      (9,(9,'c')),(9,(9,'b')),(9,(9,'a')),
      (9,(8,'c')),(9,(8,'b')),(9,(8,'a')),
      (9,(7,'c')),(9,(7,'b')),(9,(7,'a')),
      (9,(6,'c')),(9,(6,'b')),(9,(6,'a')),
      (9,(5,'c')),(9,(5,'b')),(9,(5,'a')),
      (9,(4,'c')),(9,(4,'b')),(9,(4,'a')),
      (9,(3,'c')),(9,(3,'b')),(9,(3,'a')),
      (9,(2,'c')),(9,(2,'b')),(9,(2,'a')),
      (9,(1,'c')),(9,(1,'b')),(9,(1,'a')),
      (8,(10,'c')),(8,(10,'b')),(8,(10,'a')),
      (8,(9,'c')),(8,(9,'b')),(8,(9,'a')),
      (8,(8,'c')),(8,(8,'b')),(8,(8,'a')),
      (8,(7,'c')),(8,(7,'b')),(8,(7,'a')),
      (8,(6,'c')),(8,(6,'b')),(8,(6,'a')),
      (8,(5,'c')),(8,(5,'b')),(8,(5,'a')),
      (8,(4,'c')),(8,(4,'b')),(8,(4,'a')),
      (8,(3,'c')),(8,(3,'b')),(8,(3,'a')),
      (8,(2,'c')),(8,(2,'b')),(8,(2,'a')),
      (8,(1,'c')),(8,(1,'b')),(8,(1,'a')),
      (7,(10,'c')),(7,(10,'b')),(7,(10,'a')),
      (7,(9,'c')),(7,(9,'b')),(7,(9,'a')),
      (7,(8,'c')),(7,(8,'b')),(7,(8,'a')),
      (7,(7,'c')),(7,(7,'b')),(7,(7,'a')),
      (7,(6,'c')),(7,(6,'b')),(7,(6,'a')),
      (7,(5,'c')),(7,(5,'b')),(7,(5,'a')),
      (7,(4,'c')),(7,(4,'b')),(7,(4,'a')),
      (7,(3,'c')),(7,(3,'b')),(7,(3,'a')),
      (7,(2,'c')),(7,(2,'b')),(7,(2,'a')),
      (7,(1,'c')),(7,(1,'b')),(7,(1,'a')),
      (6,(10,'c')),(6,(10,'b')),(6,(10,'a')),
      (6,(9,'c')),(6,(9,'b')),(6,(9,'a')),
      (6,(8,'c')),(6,(8,'b')),(6,(8,'a')),
      (6,(7,'c')),(6,(7,'b')),(6,(7,'a')),
      (6,(6,'c')),(6,(6,'b')),(6,(6,'a')),
      (6,(5,'c')),(6,(5,'b')),(6,(5,'a')),
      (6,(4,'c')),(6,(4,'b')),(6,(4,'a')),
      (6,(3,'c')),(6,(3,'b')),(6,(3,'a')),
      (6,(2,'c')),(6,(2,'b')),(6,(2,'a')),
      (6,(1,'c')),(6,(1,'b')),(6,(1,'a')),
      (5,(10,'c')),(5,(10,'b')),(5,(10,'a')),
      (5,(9,'c')),(5,(9,'b')),(5,(9,'a')),
      (5,(8,'c')),(5,(8,'b')),(5,(8,'a')),
      (5,(7,'c')),(5,(7,'b')),(5,(7,'a')),
      (5,(6,'c')),(5,(6,'b')),(5,(6,'a')),
      (5,(5,'c')),(5,(5,'b')),(5,(5,'a')),
      (5,(4,'c')),(5,(4,'b')),(5,(4,'a')),
      (5,(3,'c')),(5,(3,'b')),(5,(3,'a')),
      (5,(2,'c')),(5,(2,'b')),(5,(2,'a')),
      (5,(1,'c')),(5,(1,'b')),(5,(1,'a')),
      (4,(10,'c')),(4,(10,'b')),(4,(10,'a')),
      (4,(9,'c')),(4,(9,'b')),(4,(9,'a')),
      (4,(8,'c')),(4,(8,'b')),(4,(8,'a')),
      (4,(7,'c')),(4,(7,'b')),(4,(7,'a')),
      (4,(6,'c')),(4,(6,'b')),(4,(6,'a')),
      (4,(5,'c')),(4,(5,'b')),(4,(5,'a')),
      (4,(4,'c')),(4,(4,'b')),(4,(4,'a')),
      (4,(3,'c')),(4,(3,'b')),(4,(3,'a')),
      (4,(2,'c')),(4,(2,'b')),(4,(2,'a')),
      (4,(1,'c')),(4,(1,'b')),(4,(1,'a')),
      (3,(10,'c')),(3,(10,'b')),(3,(10,'a')),
      (3,(9,'c')),(3,(9,'b')),(3,(9,'a')),
      (3,(8,'c')),(3,(8,'b')),(3,(8,'a')),
      (3,(7,'c')),(3,(7,'b')),(3,(7,'a')),
      (3,(6,'c')),(3,(6,'b')),(3,(6,'a')),
      (3,(5,'c')),(3,(5,'b')),(3,(5,'a')),
      (3,(4,'c')),(3,(4,'b')),(3,(4,'a')),
      (3,(3,'c')),(3,(3,'b')),(3,(3,'a')),
      (3,(2,'c')),(3,(2,'b')),(3,(2,'a')),
      (3,(1,'c')),(3,(1,'b')),(3,(1,'a')),
      (2,(10,'c')),(2,(10,'b')),(2,(10,'a')),
      (2,(9,'c')),(2,(9,'b')),(2,(9,'a')),
      (2,(8,'c')),(2,(8,'b')),(2,(8,'a')),
      (2,(7,'c')),(2,(7,'b')),(2,(7,'a')),
      (2,(6,'c')),(2,(6,'b')),(2,(6,'a')),
      (2,(5,'c')),(2,(5,'b')),(2,(5,'a')),
      (2,(4,'c')),(2,(4,'b')),(2,(4,'a')),
      (2,(3,'c')),(2,(3,'b')),(2,(3,'a')),
      (2,(2,'c')),(2,(2,'b')),(2,(2,'a')),
      (2,(1,'c')),(2,(1,'b')),(2,(1,'a')),
      (1,(10,'c')),(1,(10,'b')),(1,(10,'a')),
      (1,(9,'c')),(1,(9,'b')),(1,(9,'a')),
      (1,(8,'c')),(1,(8,'b')),(1,(8,'a')),
      (1,(7,'c')),(1,(7,'b')),(1,(7,'a')),
      (1,(6,'c')),(1,(6,'b')),(1,(6,'a')),
      (1,(5,'c')),(1,(5,'b')),(1,(5,'a')),
      (1,(4,'c')),(1,(4,'b')),(1,(4,'a')),
      (1,(3,'c')),(1,(3,'b')),(1,(3,'a')),
      (1,(2,'c')),(1,(2,'b')),(1,(2,'a')),
      (1,(1,'c')),(1,(1,'b')),(1,(1,'a'))
    )

    val v5 = s5.toList
    //println(v5.mkString(","))
    v5 should contain theSameElementsInOrderAs List(
      ("kernel 3","k3p4"),
      ("kernel 3","k3p2"),
      ("kernel 3","k3p1"),
      ("kernel 2","p3"),
      ("kernel 2","k2p2"),
      ("kernel 2","k2p1"),
      ("kernel 1","p3"),
      ("kernel 1","k1p2"),
      ("kernel 1","k1p1")
    )
  }

  "Simple grid search strategy" should "detect invalid ranges" in {
    val p1: Param[Int] = IntRange(1, 10)
    val p2: Param[Char] = Items(List('a', 'b', 'c'))
    val p3: Composite[Int, Char] = Composite(p1, p2)

    // Range of integers from 1 to 10 can only be divided into a
    // maximum of 9 elements, so it is invalid
    val numSamples = 10

    val s1: Sampler[Finite, Int] = Strategy.grid(numSamples)(p1)
    val s2: Sampler[Finite, Char] = Strategy.grid(numSamples)(p2)
    val s3: Sampler[Finite, (Int, Char)] = Strategy.grid(numSamples)(p3)
    /*
    println(s1)
    println(s2)
    println(s3)
    */
    //s1 shouldBe ValidRange(1,10,1,0)
    s1 shouldBe a[EmptyRange[_]]
    s1 shouldBe EmptyRange(0)
    s2 shouldBe ListRange(List('a','b','c'),List('a','b','c'))
    s3 shouldBe Empty2(EmptyRange(0),ListRange(List('a', 'b', 'c'),List('a', 'b', 'c')))

    isEmpty(s1) shouldBe true
    isEmpty(s3) shouldBe true

    // But real based ranges can be sampled to an arbitrary level (up to machine precision)
    val p4: Param[Double] = DoubleRange(1, 10)
    val p5: Param[Char] = Items(List('a', 'b', 'c'))
    val p6: Composite[Double, Char] = Composite(p4, p5)

    // Range of integers from 1 to 10 can only be divided into a
    // maximum of 9 elements, so it is invalid
    //val numSamples = 10

    val s4: Sampler[Finite, Double] = Strategy.grid(numSamples)(p4)
    val s5: Sampler[Finite, Char] = Strategy.grid(numSamples)(p5)
    val s6: Sampler[Finite, (Double, Char)] = Strategy.grid(numSamples)(p6)
    /*
    println(s4)
    println(s5)
    println(s6)
    */
    s4 shouldBe ValidRange(1.0,10.0,0.9,0.09999999999999998)
    s5 shouldBe ListRange(List('a','b','c'),List('a','b','c'))
    s6 shouldBe Cartesian(ValidRange(1.0,10.0,0.9,0.09999999999999998),ListRange(List('a', 'b', 'c'),List('a', 'b', 'c')))
  }

  "Simple random search strategy" should "generate typed values" in {
    val p1: Param[Int] = IntRange(1, 10)
    val p2: Param[Char] = Items(List('a', 'b', 'c'))
    val p3: Composite[Int, Char] = Composite(p1, p2)

    val q1 = Constant("kernel 1")
    val q2 = Constant("kernel 2")
    val q3 = Constant("kernel 3")
    val q4 = Items(List("k1p1", "k1p2", "p3"))
    val q5 = Items(List("k2p1", "k2p2", "p3"))
    val q6 = Items(List("k3p1", "k3p2", "k3p4"))
    val q7: Seq[(Constant[String], Items[String])] = List((q1,q4), (q2,q5), (q3,q6))
    val q8: PairThese[String, String] = PairThese(q7)

    // Always generate same random sequences
    val rnd = Distribution.getGenerator(910018L)

    val s6: Sampler[Infinite, Int] = Strategy.random(p1,rnd)
    val s7: Sampler[Infinite, Char] = Strategy.random(p2,rnd)
    val s8: Sampler[Infinite, (Int, Char)] = Strategy.random(p3,rnd)
    val s9: Sampler[Infinite, (Int, (Int, Char))] = Strategy.random(Composite(p1,p3),rnd)
    val s10: Sampler[Infinite, (String, String)] = Strategy.random(q8,rnd)

    //println(s6)
    //println(s7)
    //println(s8)
    //println(s9)
    //println(s10)

    // Testing issue: we have random objects tha are not case values and cannot
    // be compared. We cannot compare instantiate objects that because they cannot be
    // unapply and recursively compared.
    // http://www.scalatest.org/user_guide/other_goodies
    s6 shouldBe a[IntUniform]
    inside (s6) { case IntUniform(1,10,rnd0) =>
      rnd0 shouldBe a[RandomDataGenerator]
    }
    inside (s7) { case DiscreteUniform(List('a', 'b', 'c'),3,rnd0) =>
      rnd0 shouldBe a[RandomDataGenerator]
    }
    inside (s8) { case And(IntUniform(1,10,rnd1),DiscreteUniform(List('a', 'b', 'c'),3,rnd0)) =>
      rnd1 shouldBe a[RandomDataGenerator]
      rnd0 shouldBe a[RandomDataGenerator]
    }
    inside (s9) { case And(IntUniform(1,10,rnd1),
                          And(IntUniform(1,10,rnd2),DiscreteUniform(List('a', 'b', 'c'),3,rnd3))) =>
      rnd1 shouldBe a[RandomDataGenerator]
      rnd2 shouldBe a[RandomDataGenerator]
      rnd3 shouldBe a[RandomDataGenerator]
    }
    inside (s10) { case AppendU(a1,rnd0,0) =>
      rnd0 shouldBe a[RandomDataGenerator]
      a1 shouldBe a[Vector[_]]
      a1.length shouldBe 3
      val v1 = a1(0)
      val v2 = a1(1)
      val v3 = a1(2)
      inside (v1) { case And(Const("kernel 1"),DiscreteUniform(List("k1p1", "k1p2", "p3"),3,rnd1)) =>
        rnd1 shouldBe a[RandomDataGenerator]
      }
      inside (v2) { case And(Const("kernel 2"),DiscreteUniform(List("k2p1", "k2p2", "p3"),3,rnd1)) =>
        rnd1 shouldBe a[RandomDataGenerator]
      }
      inside (v3) { case And(Const("kernel 3"),DiscreteUniform(List("k3p1", "k3p2", "k3p4"),3,rnd1)) =>
        rnd1 shouldBe a[RandomDataGenerator]
      }

    }

    val v6 = s6.toStream.take(20)
    //println(v6.mkString(","))
    v6 should contain theSameElementsInOrderAs List(9,8,2,10,7,2,9,9,9,5,5,2,1,8,5,9,1,9,2,8)

    val v7 = s7.toStream.take(30)
    //println(v7.mkString(","))
    v7 should contain theSameElementsInOrderAs List(
      'c','b','a','c','c','c','b','a','b','b',
      'c','b','b','b','c','c','b','a','b','b',
      'b','b','b','b','c','a','c','c','a','b')

    val v8 = s8.toStream.take(10)
    //println(v8.mkString(","))
    v8 should contain theSameElementsInOrderAs List((2,'b'),(9,'c'),(1,'c'),(7,'c'),(10,'b'),(4,'b'),(3,'a'),(4,'c'),(1,'b'),(3,'a'))

    val v9 = s9.toStream.take(5)
    //println(v9.mkString(","))
    v9 should contain theSameElementsInOrderAs List((7,(8,'a')),(6,(9,'a')),(9,(7,'c')),(10,(10,'c')),(6,(6,'b')))

    val v10 = s10.toStream.take(5)
    //println(v10.mkString(","))
    v10 should contain theSameElementsInOrderAs List(
      ("kernel 2","p3"),("kernel 3","k3p1"),("kernel 1","k1p2"),("kernel 1","k1p2"),("kernel 3","k3p1"))
  }

  "Applying a grid search strategy" should "should allow calls to a specific algorithm only" in {
    // The data we are going to use
    val labels1 = Array( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0)
    val mat0 : Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(11.0, 21.0, 31.0, 41.0, 51.0, 61.0, 71.0, 81.0, 91.0),
      Array( 1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)
    )
    val data1 : Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose
    val mat1 = MatrixData.dense(data1, new Random(5587) )
    val sz1 = mat1.size
    sz1._1 shouldBe data1.length       // no. of lines
    sz1._2 shouldBe data1(0).length    // no. columns

    // Use a 2-k cross validation data set:
    val kfolds = mat1.splitStratifiedFolds(2)
    kfolds.isRight shouldBe true
    val kfolds1 = kfolds.right.get
    // 10 samples divided into 2 folds
    kfolds1.length shouldBe 2
    // First fold
    val fold1 = kfolds1.head
    // Train and test
    val fold1_train: DenseMatrixData = fold1._1
    val fold1_test: DenseMatrixData = fold1._2

    // Test algorithm (stubs)
    val a1 = Algorithms.TestAlgorithm1()
    val a2 = Algorithms.TestAlgorithm1()

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 9
    val ps1: Sampler[Finite, (String, String)] = Strategy.grid(numSamples)(a1.params)
    val pa1: List[(String, String)] = ps1.toList

    // Train the models. Note that the models are specific to the algorithm instance
    // Use the arguments tha give "good" results
    val m1: Either[ADWError, a1.Model] = a1.fit(fold1_train,("kernel 1","k1p1"))
    m1.isRight shouldBe true
    // Use the arguments tha give "bad" results
    val m2: Either[ADWError, a2.Model] = a2.fit(fold1_train,pa1.head)
    m2.isRight shouldBe true

    // Make prediction based on the model
    val p1: Either[ADWError, Prediction[a1.Label]] = a1.predict(fold1_test,m1.right.get)
    //println(p1)
    p1.isRight shouldBe true
    p1.right.get.toArray should contain theSameElementsInOrderAs Array(1.0, 0.31805634905334673, 1.0, 0.9077818835613637)
    val p2: Either[ADWError, Prediction[a2.Label]] = a2.predict(fold1_test,m2.right.get)
    //println(p2)
    p2.isRight shouldBe true
    p2.right.get.toArray should contain theSameElementsInOrderAs Array(0.8720619427016808, 0.8322079256354361, 0.6490945899047893, 0.051446370433858024)

    // you can only use models in appropriate algorithm
    //val p3: Double = a2.predict(0,m1)
    "val p3: Double = a2.predict(0,m1)" shouldNot typeCheck
  }

  it should "should allow evaluation of a function" in {
    // The data we are going to use
    val labels1 = Array(1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0, 0.0)
    val mat0: Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(11.0, 21.0, 31.0, 41.0, 51.0, 61.0, 71.0, 81.0, 91.0),
      Array(1.1, 2.1, 3.1, 4.2, 5.1, 6.1, 7.1, 8.1, 9.1)
    )
    val data1: Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose
    val mat1 = MatrixData.dense(data1, new Random(5587))
    val sz1 = mat1.size
    sz1._1 shouldBe data1.length // no. of lines
    sz1._2 shouldBe data1(0).length // no. columns

    // Test algorithm (stubs)
    val a1 = Algorithms.TestAlgorithm1()

    // Generate the full set of all possible parameter combinations
    // Note: if request more samples than are possible, the error will be
    // detected and returned as an empty type. Make sure this does not
    // happen.
    val numSamples = 9
    val ps1: Sampler[Finite, (String, String)] = Strategy.grid(numSamples)(a1.params)
    val pa1: Seq[(String, String)] = ps1.toList
    val args = pa1.head

    // k-fold Split data into training and test sets
    val nrFold = 3
    val folds: Either[ADWError, Stream[(DenseMatrixData, DenseMatrixData)]] = mat1.splitStratifiedFolds(nrFold)
    folds.isRight shouldBe true

    // Get the first train and test sets
    val data = folds.right.get.toList
    val train: DataIn[Double,Double] = data.head._1
    val test: DataIn[Double,Double] = data.head._2

    // Fit a model and predict the output
    val m1: Either[ADWError, a1.Model] = a1.fit(train, args)
    m1.isRight shouldBe true
    // Predict output
    val p1: Either[ADWError, Prediction[a1.Label]] = a1.predict(test, m1.right.get)
    p1.isRight shouldBe true

    // Start up a Binary Classification Evaluator. Note tha it will convert the real number
    // predictions to true/false predictions (true if prediction >= 0.5)
    //val metrics: BinaryClassificationEvalDouble = BinaryClassificationEvalDouble(0.5)
    val metrics: BinaryClassificationEvalDouble = BinaryClassificationEvalDouble(0.5)
    // Now get the concrete metric evaluation
    val e1: BinaryClassificationMetrics = metrics(test, p1.right.get)
    val me1: Either[ADWError, BinaryClassificationMetrics#All] = e1.calculate
    // We used the "good" parameters so we get the maximum metric values
    //println((e1.tp, e1.tn, e1.fp, e1.fn))
    me1 shouldBe Right(
      BinaryClassifications(
        Recall(1.0),Precision(0.5),Accuracy(0.5),MCC(0.0),
        FScore(FBeta(1.0),0.6666666666666666),FScore(FBeta(2.0),0.8333333333333334),
        FScore(FBeta(0.5),0.5555555555555556),GMeasure(0.7071067811865476)))

    // We can do the above three steps in a single call
    val eval1: Either[ADWError, BinaryClassificationMetrics] = ParamSelect.trainAndEval(a1)(args, train, test, metrics)
    eval1.isRight shouldBe true
    // Same result as above
    //println(eval1.right.get)
    //val tt = eval1.right.get
    //println((tt.tp, tt.tn, tt.fp, tt.fn))
    eval1.right.get.calculate shouldBe Right(
      BinaryClassifications(
        Recall(1.0),Precision(1.0),Accuracy(1.0),MCC(1.0),
        FScore(FBeta(1.0),1.0),FScore(FBeta(2.0),1.0),FScore(FBeta(0.5),1.0),
        GMeasure(1.0)))

    // Start a stream that will fit and evaluate the model several times.
    // Note the the execution is delayed. Only the first one is executed
    val cv1: Stream[Either[ADWError, BinaryClassificationMetrics]] = ParamSelect.prepareCrossValidation(a1)(args,metrics)(folds.right.get)
    //println(cv1.toList)
    cv1 should contain theSameElementsInOrderAs List(
      Right(BinaryClassificationMetrics(Array(true, false),mutable.ArraySeq(false, false))),
      Right(BinaryClassificationMetrics(Array(false, true),mutable.ArraySeq(false, true))),
      Right(BinaryClassificationMetrics(Array(true, false),mutable.ArraySeq(true, true))))


    // Once we have fitted the models, we can evaluate the performance of the fit.
    // At this point the fitting and prediction are executed and the metric evaluation is started (only the first)
    val met1: Either[ADWError, Stream[BinaryClassificationMetrics#All]] = metrics.collect(cv1)
    met1.isRight shouldBe true
    met1.right.get.length shouldBe 3
    //println(met1.right.get.toList.mkString(","))
    met1.right.get.length shouldBe 3
    val m11 = met1.right.get.head
    val m12 = met1.right.get(1)
    val m13 = met1.right.get(2)
    m11 shouldBe
      BinaryClassifications(
        Recall(1.0),Precision(0.5),Accuracy(0.5),MCC(0.0),
        FScore(FBeta(1.0),0.6666666666666666),FScore(FBeta(2.0),0.8333333333333334),FScore(FBeta(0.5),0.5555555555555556),
        GMeasure(0.7071067811865476))
    m12 shouldBe
      BinaryClassifications(
        Recall(1.0),Precision(1.0),Accuracy(1.0),MCC(1.0),
        FScore(FBeta(1.0),1.0),FScore(FBeta(2.0),1.0),FScore(FBeta(0.5),1.0),
        GMeasure(1.0))
    inside(m13) { case BinaryClassifications(Recall(0.0),Precision(prec),Accuracy(0.5),MCC(0.0),
                                             FScore(FBeta(1.0),0.0),FScore(FBeta(2.0),0.0),FScore(FBeta(0.5),0.0),
                                             GMeasure(g)) =>
      prec.isNaN shouldBe true
      g.isNaN shouldBe true
    }


    // Here we collect a specific metric only
    val met2: Either[ADWError, Stream[MCC[Double]]] = metrics.collectOnly((m:BinaryClassificationMetrics#All) => m.mcc)(cv1)
    met2.isRight shouldBe true
    met2.right.get.length shouldBe 3
    //println(met2.right.get.toList.mkString(","))
    met2.right.get should contain theSameElementsInOrderAs Array(MCC(0.0), MCC(1.0), MCC(0.0))

    val metricsEvals: Stream[BinaryClassificationMetrics#All] = Stream(
      BinaryClassifications(Recall(0.1),Precision(0.3),Accuracy(0.5),MCC(1.0),
                            FScore(FBeta(1.0),1.0),FScore(FBeta(2.0),1.0),FScore(FBeta(0.5),1.0),GMeasure(1.0)),
      BinaryClassifications(Recall(0.2),Precision(0.2),Accuracy(0.6),MCC(1.0),
                            FScore(FBeta(1.0),1.0),FScore(FBeta(2.0),1.0),FScore(FBeta(0.5),1.0),GMeasure(1.0)),
      BinaryClassifications(Recall(0.4),Precision(0.1),Accuracy(0.7),MCC(1.0),
                            FScore(FBeta(1.0),1.0),FScore(FBeta(2.0),1.0),FScore(FBeta(0.5),1.0),GMeasure(1.0)))


    // Once we have collected the full set of metrics, we can use them in various ways
    // Select the best combination of metrics
    val maxMetric: BinaryClassificationMetrics#All = metrics.max((m: BinaryClassificationMetrics#All) =>
                                                             m.recall.value*0.5 + m.precision.value*.05 , metricsEvals)
    //println(maxMetric)
    maxMetric shouldBe
      BinaryClassifications(
        Recall(0.4),Precision(0.1),Accuracy(0.7),MCC(1.0),
        FScore(FBeta(1.0),1.0),FScore(FBeta(2.0),1.0),FScore(FBeta(0.5),1.0),
        GMeasure(1.0))
    val minMetric: BinaryClassificationMetrics#All = metrics.min((m: BinaryClassificationMetrics#All) =>
                                                              m.recall.value*0.5 + m.precision.value*.05 , metricsEvals)
    //println(minMetric)
    // Select the worst combination of metrics
    minMetric shouldBe
      BinaryClassifications(
        Recall(0.1),Precision(0.3),Accuracy(0.5),MCC(1.0),
        FScore(FBeta(1.0),1.0),FScore(FBeta(2.0),1.0),FScore(FBeta(0.5),1.0),
        GMeasure(1.0))

    // Calculate the statistics of each metric
    val stats2 = metrics.stats(cv1)
    stats2.isRight shouldBe true
    stats2.right.get shouldBe a[BinaryClassifications[_]] // 8 binary metrics
    //println(stats2)
    inside(stats2) { case Right(
    BinaryClassifications(
      Recall(Values(GeoMean(0.0),Kurtosis(v11),Max(1.0),Mean(0.6666666666666666),Min(0.0),N(3.0),Quartile1(0.0),
          Quartile2(1.0),Quartile3(1.0),PopulationVariance(0.22222222222222224),QuadraticMean(0.816496580927726),
          Skewness(-1.7320508075688765),StandardDeviation(0.5773502691896258),SumTotal(2.0),SumSquares(2.0),
          Variance(0.33333333333333337))),
      Precision(Values(GeoMean(v21),Kurtosis(v22),Max(1.0),Mean(v23),Min(0.5),N(3.0),Quartile1(0.5),
          Quartile2(0.75),Quartile3(1.0),PopulationVariance(v24),QuadraticMean(v25),
          Skewness(v26),StandardDeviation(v27),SumTotal(v28),SumSquares(v29),
          Variance(v210))),
      Accuracy(Values(GeoMean(0.6299605249474366),Kurtosis(v31),Max(1.0),Mean(0.6666666666666666),Min(0.5),N(3.0),Quartile1(0.5),
          Quartile2(0.5),Quartile3(1.0),PopulationVariance(0.05555555555555555),QuadraticMean(0.7071067811865476),
          Skewness(1.7320508075688785),StandardDeviation(0.28867513459481287),SumTotal(2.0),SumSquares(1.5),
          Variance(0.08333333333333333))),
      MCC(Values(GeoMean(0.0),Kurtosis(v41),Max(1.0),Mean(0.33333333333333337),Min(0.0),N(3.0),Quartile1(0.0),
          Quartile2(0.0),Quartile3(1.0),PopulationVariance(0.22222222222222224),QuadraticMean(0.5773502691896257),
          Skewness(1.7320508075688767),StandardDeviation(0.5773502691896258),SumTotal(1.0),SumSquares(1.0),
          Variance(0.33333333333333337))),
      FScore(FBeta(1.0),Values(GeoMean(0.0),Kurtosis(v51),Max(1.0),Mean(0.5555555555555556),Min(0.0),N(3.0),Quartile1(0.0),
          Quartile2(0.6666666666666666),Quartile3(1.0),PopulationVariance(0.17283950617283952),QuadraticMean(0.693888666488711),
          Skewness(-0.9352195295828248),StandardDeviation(0.5091750772173156),SumTotal(1.6666666666666665),SumSquares(1.4444444444444444),
          Variance(0.2592592592592593))),
      FScore(FBeta(2.0),Values(GeoMean(0.0),Kurtosis(v61),Max(1.0),Mean(0.6111111111111112),Min(0.0),N(3.0),Quartile1(0.0),
          Quartile2(0.8333333333333334),Quartile3(1.0),PopulationVariance(0.19135802469135801),QuadraticMean(0.7515416254704824),
          Skewness(-1.5453925256950218),StandardDeviation(0.5357583756107197),SumTotal(1.8333333333333335),SumSquares(1.6944444444444446),
          Variance(0.28703703703703703))),
      FScore(FBeta(0.5),Values(GeoMean(0.0),Kurtosis(v71),Max(1.0),Mean(0.5185185185185185),Min(0.0),N(3.0),Quartile1(0.0),
          Quartile2(0.5555555555555556),Quartile3(1.0),PopulationVariance(0.16735253772290806),QuadraticMean(0.6604649814861855),
          Skewness(-0.3308318147058984),StandardDeviation(0.5010277503136549),SumTotal(1.5555555555555556),SumSquares(1.308641975308642),
          Variance(0.2510288065843621))),
      GMeasure(Values(GeoMean(v81),Kurtosis(v82),Max(1.0),Mean(v83),Min(0.7071067811865476),N(3.0),Quartile1(0.7071067811865476),
          Quartile2(0.8535533905932737),Quartile3(1.0),PopulationVariance(v84),QuadraticMean(v85),
          Skewness(v86),StandardDeviation(v87),SumTotal(v88),SumSquares(v89),
          Variance(v810)))
      ) ) =>

      v11.isNaN shouldBe true
      v21.isNaN shouldBe true
      v22.isNaN shouldBe true
      v23.isNaN shouldBe true
      v24.isNaN shouldBe true
      v25.isNaN shouldBe true
      v26.isNaN shouldBe true
      v27.isNaN shouldBe true
      v28.isNaN shouldBe true
      v29.isNaN shouldBe true
      v210.isNaN shouldBe true
      v31.isNaN shouldBe true
      v41.isNaN shouldBe true
      v51.isNaN shouldBe true
      v61.isNaN shouldBe true
      v71.isNaN shouldBe true
      v81.isNaN shouldBe true
      v82.isNaN shouldBe true
      v83.isNaN shouldBe true
      v84.isNaN shouldBe true
      v85.isNaN shouldBe true
      v86.isNaN shouldBe true
      v87.isNaN shouldBe true
      v88.isNaN shouldBe true
      v89.isNaN shouldBe true
      v810.isNaN shouldBe true
    }

    // Take the 3 cross validation results and place all 8 metrics
    // of each experiment in it own column
    //val cols1 = metrics.toColumns(metrics.values)(cv1)
    val cols1 = metrics.collectCols(cv1)
    //println(metrics.collect(cv1).right.get.toList )
    //println(cols1)
    cols1.isRight shouldBe true
    cols1.right.get shouldBe a[BinaryClassifications[_]] // 8 metrics in the boolean metrics class
    inside(cols1) { case Right(
      BinaryClassifications(
        Recall(List(1.0, 1.0, 0.0)),
        Precision(List(0.5, 1.0, v1)),
        Accuracy(List(0.5, 1.0, 0.5)),
        MCC(List(0.0, 1.0, 0.0)),
        FScore(FBeta(1.0), List(0.6666666666666666, 1.0, 0.0)),
        FScore(FBeta(2.0), List(0.8333333333333334, 1.0, 0.0)),
        FScore(FBeta(0.5), List(0.5555555555555556, 1.0, 0.0)),
        GMeasure(List(0.7071067811865476, 1.0, v2))
        )) =>

      v1.isNaN shouldBe true
      v2.isNaN shouldBe true
    }

    //Select parameters
    // If we print out, we see that it is not fully calculate. Only the first element of stream stand ready
    val srch1: Stream[Either[ADWError,(BinaryClassificationMetrics#AllColumnsStats, a1.Args)]] =
      ParamSelect.testParams(a1)(ps1, metrics, mat1, nrFolds = Some(2), argsSampleSize = Some(1000) )

    // Select the best parameter according to some criteria based on the metrics
    // Here we select the best mean MCC that has the least standard deviation
    def selCriteria(eval: Either[ADWError,(BinaryClassificationMetrics#AllColumnsStats, a1.Args)]): (Double, Double) = {
      def ok(metrics: (BinaryClassificationMetrics#AllColumnsStats, a1.Args)) = {
        val mcc: MCC[Values] = metrics._1.mcc
        val mean = mcc.value.mean
        val std = mcc.value.standard_deviation
        (mean.value,-std.value)
      }
      def failed(e: ADWError) = (-1.0, Double.MaxValue)

      eval.fold(failed, ok)
    }
    // Execute the calculations
    val sel2: Either[ADWError,(BinaryClassificationMetrics#AllColumnsStats, a1.Args)] = srch1.maxBy( selCriteria )
    inside(sel2) { case Right
      ((
        BinaryClassifications(
         Recall(Values(GeoMean(0.7071067811865476),Kurtosis(_),Max(1.0),
                Mean(0.75),Min(0.5),N(2.0),Quartile1(0.5),Quartile2(0.75),
                Quartile3(1.0),PopulationVariance(0.0625),QuadraticMean(0.7905694150420949),
                Skewness(_),StandardDeviation(0.3535533905932738),SumTotal(1.5),SumSquares(1.25),Variance(0.125))),
         Precision(Values(GeoMean(0.816496580927726),Kurtosis(_),Max(1.0),
                Mean(0.8333333333333333),Min(0.6666666666666666),N(2.0),Quartile1(0.6666666666666666),
                Quartile2(0.8333333333333333),Quartile3(1.0),PopulationVariance(0.027777777777777783),
                QuadraticMean(0.8498365855987975),Skewness(_),StandardDeviation(0.23570226039551587),
                SumTotal(1.6666666666666665),SumSquares(1.4444444444444444),Variance(0.055555555555555566))),
         Accuracy(Values(GeoMean(0.75),Kurtosis(_),Max(0.75),
                  Mean(0.75),Min(0.75),N(2.0),Quartile1(0.75),
                  Quartile2(0.75),Quartile3(0.75),PopulationVariance(0.0),
                  QuadraticMean(0.75),Skewness(_),StandardDeviation(0.0),
                  SumTotal(1.5),SumSquares(1.125),Variance(0.0))),
         MCC(Values(GeoMean(0.5773502691896258),Kurtosis(_),Max(0.5773502691896258),
                    Mean(0.5773502691896258),Min(0.5773502691896258),N(2.0),Quartile1(0.5773502691896258),
                    Quartile2(0.5773502691896258),Quartile3(0.5773502691896258),PopulationVariance(0.0),
                    QuadraticMean(0.5773502691896258),Skewness(_),StandardDeviation(0.0),
                    SumTotal(1.1547005383792517),SumSquares(0.6666666666666669),Variance(0.0))),
         FScore(FBeta(1.0),Values(GeoMean(0.7302967433402215),Kurtosis(_),Max(0.8),
                Mean(0.7333333333333334),Min(0.6666666666666666),N(2.0),Quartile1(0.6666666666666666),
                Quartile2(0.7333333333333334),Quartile3(0.8),PopulationVariance(0.0044444444444444505),
                QuadraticMean(0.7363574011458174),Skewness(_),StandardDeviation(0.0942809041582064),
                SumTotal(1.4666666666666668),SumSquares(1.0844444444444445),Variance(0.008888888888888901))),
         FScore(FBeta(2.0),Values(GeoMean(0.7106690545187014),Kurtosis(_),Max(0.9090909090909091),
                Mean(0.7323232323232323),Min(0.5555555555555556),N(2.0),Quartile1(0.5555555555555556),
                Quartile2(0.7323232323232323),Quartile3(0.9090909090909091),PopulationVariance(0.031246811549841845),
                QuadraticMean(0.7533552469786009),Skewness(_),StandardDeviation(0.24998724587403193),
                SumTotal(1.4646464646464645),SumSquares(1.1350882563003775),Variance(0.06249362309968369))),
         FScore(FBeta(0.5),Values(GeoMean(0.7715167498104596),Kurtosis(_),Max(0.8333333333333334),
                Mean(0.7738095238095238),Min(0.7142857142857143),N(2.0),Quartile1(0.7142857142857143),
                Quartile2(0.7738095238095238),Quartile3(0.8333333333333334),PopulationVariance(0.0035430839002267584),
                QuadraticMean(0.7760955244288867),Skewness(_),StandardDeviation(0.08417937871268424),
                SumTotal(1.5476190476190477),SumSquares(1.2046485260770976),Variance(0.007086167800453517))),
        GMeasure(Values(GeoMean(0.7598356856515925),Kurtosis(_),Max(0.816496580927726),
                 Mean(0.7618016810571369),Min(0.7071067811865476),N(2.0),Quartile1(0.7071067811865476),
                 Quartile2(0.7618016810571369),Quartile3(0.816496580927726),PopulationVariance(0.002991532071853782),
                 QuadraticMean(0.7637626158259734),Skewness(_),StandardDeviation(0.07735026918962573),
                 SumTotal(1.5236033621142737),SumSquares(1.1666666666666667),Variance(0.005983064143707564)))),
        ("kernel 1","k1p2")
      ))
      =>
    }
  }

  "Experiment set-up" should "execute tasks in parallel (concurrency)" in {
    import scala.concurrent.duration._

    //val NANOS = 1000000000
    val NANOS = 1000000000L
    NANOS shouldBe 1e9
    val duration: Long =
      (49 * 60 * 60 * NANOS) +
      (30 * 60 * NANOS) +
      (20 * NANOS) +
      123000000
    //println(duration)
    duration shouldBe 178220123000000L
    val tmpT: time.Duration = java.time.Duration.ofNanos(duration)
    //println(tmpT)
    tmpT.toString shouldBe "PT49H30M20.123S"
    val totalNanos = duration.nanoseconds
    //println(totalNanos.toString)
    totalNanos.toString shouldBe "178220123000000 nanoseconds"
    totalNanos.toNanos shouldBe duration
    val (days, hours, minutes, seconds, millis, nanos) = Utils.decompose(totalNanos)
    days shouldBe 2
    hours shouldBe 1
    minutes shouldBe 30
    seconds shouldBe 20
    millis shouldBe 123
    nanos shouldBe 0
    val timeString = Utils.decomposeToString(totalNanos)
    //println(timeString)
    timeString shouldBe "2 days + 1 hours + 30 minutes + 20 seconds + 123 millis + 0 nanos"

    import java.lang.{System => JSystem}
    import better.files.File
    //import better.files.Dsl._

    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    val resultFile = tmp / ("thread_test" + suiteName + ".txt")

    import pt.inescn.search.ParamSearch.ParamSelect
    import java.io.PrintWriter
    import java.nio.channels.FileLock

    val logger: Either[ADWError, (FileLock, PrintWriter)] = ParamSelect.createResultsFile(resultFile)
    logger.isRight shouldBe true
    val (logFileLoc, logFile) = logger.right.get

    /* Simulate a task: does nothing */
    def func(i:Long): Long = {
      //println(i)
      i
    }

    val very_large = 100 // 99999 // 999999999
    def params = Stream.from(0).map(_.toLong).take(very_large)

    // Lets use the default scheduler: ForkJoin pool
    import monix.execution.Scheduler.Implicits.global
    import scala.concurrent.duration._
    val numThreads = java.lang.Runtime.getRuntime.availableProcessors()
    //println(s"numThreads = $numThreads")
    val timeOut = 7.seconds
    val separator = ";"
    val task = ParamSelect.makeTaski(separator,logFile, func) _
    val experiment = ParamSelect.execFunci(numThreads, timeOut, logFile, task ) _

    // Execute the func on each value in search
    val exp = experiment(params)
    exp.isRight shouldBe true
    resultFile.lineIterator.toList.length shouldBe very_large + 1 // extra line contains elapsed time

    // Now check if all tasks were executed
    // We simple check that all ids are present the result file
    val results = resultFile.lineIterator
      .filter( !_.startsWith(ParamSelect.TotalElapsedTime)) // ignore last line that records total processing time
      .map( ParamSelect.parseResulti(separator) ) // parse the fields of the result file
      .toList // convert into something that can be sorted
      .sorted // sort for checking
    //println(results.mkString(","))
    // All the task IDs that must be present
    val orderedContinuous = (0L to very_large).zip(results).forall{ p =>
      val id: Long = p._1
      val recordedId: Long = p._2._1
      //println(s"id($id) == recordedId($recordedId)")
      id == recordedId
    }
    //println(s"orderedContinuous = $orderedContinuous")
    orderedContinuous shouldBe true
  }

  it should "deal with concurrent tasks that fail (throw exception)" in {
    import scala.concurrent.duration._

    import java.lang.{System => JSystem}
    import better.files.File
    //import better.files.Dsl._

    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    val resultFile = tmp / ("short_thread_test" + suiteName + ".txt")

    import pt.inescn.search.ParamSearch.ParamSelect
    import java.io.PrintWriter
    import java.nio.channels.FileLock

    val logger: Either[ADWError, (FileLock, PrintWriter)] = ParamSelect.createResultsFile(resultFile)
    logger.isRight shouldBe true
    val (logFileLoc, logFile) = logger.right.get

    val missing = 10
    /*
      Simulate a task that fails.
      Note that the other tasks may continue being processed, but
      as soon as Monix detects a failure, all other pending processes
      are terminated and the Observer stops.
    */
    def func(i:Long): Long = {
      //println(i)
      if (i == missing)
        throw new RuntimeException(s"Testing Monix Task failure (task ID = $i)")
      else
        i
    }

    val very_large = 100 // 99999 // 999999999
    def params = Stream.from(0).map(_.toLong).take(very_large)

    // Lets use the default scheduler: ForkJoin pool
    import monix.execution.Scheduler.Implicits.global
    import scala.concurrent.duration._
    val numThreads = java.lang.Runtime.getRuntime.availableProcessors()
    //println(s"numThreads = $numThreads")
    val timeOut = 6.seconds
    val separator = ","
    val task = ParamSelect.makeTaski(separator, logFile, func) _
    val experiment = ParamSelect.execFunci(numThreads, timeOut, logFile, task ) _

    // Execute the func on each value in search
    val exp = experiment(params)
    //println(exp)
    exp shouldBe Left(ADWError("Concurrent processing failed: Testing Monix Task failure (task ID = 10)"))
    resultFile.lineIterator.toList.length should be < very_large // +1 extra line contains elapsed time

    // Now check to see if the task was executed
    // We simple check that the task id missing is not present in the result file
    val results = resultFile.lineIterator
      .filter( !_.startsWith(ParamSelect.TotalElapsedTime)) // ignore last line that records total processing time
      .map( ParamSelect.parseResulti(separator) ) // parse the fields of the result file
      .toList // convert into something that can be sorted
      .filter(_._1 == missing)
    results.isEmpty shouldBe true
  }

}
