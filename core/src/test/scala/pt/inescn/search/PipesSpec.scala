package pt.inescn.search

import org.scalatest._
import pt.inescn.search.Pipes.{Compile, Runtime, exec, execDebug, f, when, iff, all => allP, compile => compileP}
import pt.inescn.utils.ADWError

/**
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.search.PipesSpec"
  */
class PipesSpec extends FlatSpec with Matchers with Inside {

  "Pipes" should "allow splits" in {

    // These are the functions we want to compose
    // They have a specific structure.
    // They must access two parameters: one is the parameter the other is the input
    // They must return a tuple:
    // - The first element is tracking data that will be recorded
    // - The second element is the output which will be passed on to the next function
    /*def func1(p:Int, v:Int): (String, Int) = (s"func1($p+$v)", p + v)
    def func2(p:Double, v:Int): (String, Double) = (s"func2($p+$v)", v.toDouble + 1.0)
    def func3_1(p:Double, v:Double): (String, Double) = (s"func3_1($p+$v)", v.toDouble + 1.0)
    def func3_2(p:Double, v:Double): (String, Double) = (s"func3_2($p+$v)", v.toDouble + 1.0)
    */

    def func0(p:Int, v:Int): Int = v+p
    def func1(p:Double, v:Int): Double = v.toDouble + p
    def func2(p:String, v:Int): Double = -v.toDouble
    def func3(p:Int, v:Double): Double = v.toDouble - p
    def func4(p:Int, v:Double): Double = p*v.toDouble
    def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    val f0 = f("func0", 1,   func0)
    val f1 = f("func1", 1.0, func1)
    val f2 = f("func2", "",  func2)
    val f3 = f("func3", 2,   func3)
    val f4 = f("func4", 3,   func4)
    val f5 = f("func5", 2,   func5)

    val cf1: Either[ADWError, Pipes.Exec[Int, Int]] = compileP(f0)
    cf1.isRight shouldBe true
    val cf1e = cf1.right.get
    val cf1r = exec(List(1,2,3,4),cf1e)
    //println(cf1r)
    cf1r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func0(1,1)",2),
      ("func0(1,2)",3),
      ("func0(1,3)",4),
      ("func0(1,4)",5))

    // Sequences

    // Functions

    val f01 = f0 ->: f1
    val cf01: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f01)
    cf01.isRight shouldBe true
    val cf01e = cf01.right.get
    val cf01r = exec(List(1,2,3,4),cf01e)
    //println(cf01r)
    cf01r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func0(1,1),func1(1.0,2)",3.0), ("func0(1,2),func1(1.0,3)",4.0),
      ("func0(1,3),func1(1.0,4)",5.0), ("func0(1,4),func1(1.0,5)",6.0))

    // Sets of functions in sequence

    val fa12 = allP(f1,f2)
    val cfa12: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(fa12)
    cfa12.isRight shouldBe true
    val cfa12e = cfa12.right.get
    val cfa12r = exec(List(1,2,3,4),cfa12e)
    //println(cfa12r)
    // theSameElementsInOrderAs
    cfa12r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func2(,1)",-1.0), ("func1(1.0,1)",2.0),
      ("func2(,2)",-2.0), ("func1(1.0,2)",3.0),
      ("func2(,3)",-3.0), ("func1(1.0,3)",4.0),
      ("func2(,4)",-4.0), ("func1(1.0,4)",5.0))

    val f0a12 = f0 ->: allP(f1,f2)
    //println(f0a12)
    val cf0a12: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f0a12)
    //println(cf0a12)
    cf0a12.isRight shouldBe true
    val cf0a12e = cf0a12.right.get
    val cf0a12r = exec(List(1,2,3,4),cf0a12e)
    //println(cfa12r)
    cf0a12r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func0(1,1),func2(,2)",-2.0), ("func0(1,1),func1(1.0,2)",3.0),
      ("func0(1,2),func2(,3)",-3.0), ("func0(1,2),func1(1.0,3)",4.0),
      ("func0(1,3),func2(,4)",-4.0), ("func0(1,3),func1(1.0,4)",5.0),
      ("func0(1,4),func2(,5)",-5.0), ("func0(1,4),func1(1.0,5)",6.0))

    val f12a3 = allP(f1,f2) ->: f3
    //println(f12a3)
    val cf12a3: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f12a3)
    //println(cf12a3)
    cf12a3.isRight shouldBe true
    val cf12a3e = cf12a3.right.get
    val cf12a3r = exec(List(1,2,3,4),cf12a3e)
    //println(cf12a3r)
    cf12a3r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func2(,1),func3(2,-1.0)",-3.0), ("func1(1.0,1),func3(2,2.0)",0.0),
      ("func2(,2),func3(2,-2.0)",-4.0), ("func1(1.0,2),func3(2,3.0)",1.0),
      ("func2(,3),func3(2,-3.0)",-5.0), ("func1(1.0,3),func3(2,4.0)",2.0),
      ("func2(,4),func3(2,-4.0)",-6.0), ("func1(1.0,4),func3(2,5.0)",3.0))

    val f12a34 = allP(f1,f2) ->: allP(f3,f4)
    //println(f12a34)
    val cf12a34: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f12a34)
    //println(cf12a34)
    cf12a34.isRight shouldBe true
    val cf12a34e = cf12a34.right.get
    val cf12a34r = exec(List(1,2,3,4),cf12a34e)
    //println(cf12a34r)
    cf12a34r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func2(,1),func4(3,-1.0)",-3.0),  ("func1(1.0,1),func3(2,2.0)",0.0),
      ("func2(,2),func4(3,-2.0)",-6.0),  ("func1(1.0,2),func3(2,3.0)",1.0),
      ("func2(,3),func4(3,-3.0)",-9.0),  ("func1(1.0,3),func3(2,4.0)",2.0),
      ("func2(,4),func4(3,-4.0)",-12.0), ("func1(1.0,4),func3(2,5.0)",3.0))

    //val f0f12a34 = f0 ->: allP(f1,f2) ->: allP(f3,f4) ->: f5
    val f12a34f5 = allP(f1,f2) ->: allP(f3,f4) ->: f5
    //println(f12a34f5)
    val cf12a34f5: Either[ADWError, Pipes.Exec[Int, Boolean]] = compileP(f12a34f5)
    //println(cf12a34f5)
    cf12a34f5.isRight shouldBe true
    val cf12a34f5e = cf12a34f5.right.get
    val cf12a34f5r = exec(List(1,2,3,4),cf12a34f5e)
    //println(cf12a34f5r)
    cf12a34f5r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func2(,1),func4(3,-1.0),func5(2,-3.0)",false), ("func1(1.0,1),func3(2,2.0),func5(2,0.0)",true),
      ("func2(,2),func4(3,-2.0),func5(2,-6.0)",true),  ("func1(1.0,2),func3(2,3.0),func5(2,1.0)",false),
      ("func2(,3),func4(3,-3.0),func5(2,-9.0)",false), ("func1(1.0,3),func3(2,4.0),func5(2,2.0)",true),
      ("func2(,4),func4(3,-4.0),func5(2,-12.0)",true), ("func1(1.0,4),func3(2,5.0),func5(2,3.0)",false))

    val f0f12a34 = f0 ->: allP(f1,f2) ->: allP(f3,f4) ->: f5
    //println(f0f12a34)
    val cf0f12a34: Either[ADWError, Pipes.Exec[Int, Boolean]] = compileP(f0f12a34)
    //println(cf0f12a34)
    cf0f12a34.isRight shouldBe true
    val cf0f12a34e = cf0f12a34.right.get
    val cf0f12a34r = exec(List(1,2,3,4),cf0f12a34e)
    //println(cf0f12a34r)
    cf0f12a34r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func0(1,1),func2(,2),func4(3,-2.0),func5(2,-6.0)",true),
      ("func0(1,1),func1(1.0,2),func3(2,3.0),func5(2,1.0)",false),
      ("func0(1,2),func2(,3),func4(3,-3.0),func5(2,-9.0)",false),
      ("func0(1,2),func1(1.0,3),func3(2,4.0),func5(2,2.0)",true),
      ("func0(1,3),func2(,4),func4(3,-4.0),func5(2,-12.0)",true),
      ("func0(1,3),func1(1.0,4),func3(2,5.0),func5(2,3.0)",false),
      ("func0(1,4),func2(,5),func4(3,-5.0),func5(2,-15.0)",false),
      ("func0(1,4),func1(1.0,5),func3(2,6.0),func5(2,4.0)",true))
  }

  it should "allow cross-products" in {

    // These are the functions we want to compose
    // They have a specific structure.
    // They must access two parameters: one is the parameter the other is the input
    // They must return a tuple:
    // - The first element is tracking data that will be recorded
    // - The second element is the output which will be passed on to the next function

    def func0(p:Int, v:Int): Int = v+p
    def func1(p:Double, v:Int): Double = v.toDouble + p
    def func2(p:String, v:Int): Double = -v.toDouble
    def func3(p:Int, v:Double): Double = v.toDouble - p
    def func4(p:Int, v:Double): Double = p*v.toDouble
    def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    val f0 = f("func0", 1,   func0)
    val f1 = f("func1", 1.0, func1)
    val f2 = f("func2", "",  func2)
    val f3 = f("func3", 2,   func3)
    val f4 = f("func4", 3,   func4)
    val f5 = f("func5", 2,   func5)

    // Sets of functions in cross-product

    // Cross products of sets of functions
    val f12c34 = allP(f1,f2) *: allP(f3,f4)
    //println(f12c34)
    val cf12c34: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f12c34)
    //println(cf12c34)
    cf12c34.isRight shouldBe true
    val cf12c34e = cf12c34.right.get
    val cf12c34r = exec(List(1,2,3,4),cf12c34e)
    //println(cf12c34r)
    cf12c34r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func2(,1),func4(3,-1.0)",  -3.0), ("func2(,1),func3(2,-1.0)", -3.0),
      ("func1(1.0,1),func4(3,2.0)", 6.0), ("func1(1.0,1),func3(2,2.0)",0.0),
      ("func2(,2),func4(3,-2.0)",  -6.0), ("func2(,2),func3(2,-2.0)", -4.0),
      ("func1(1.0,2),func4(3,3.0)", 9.0), ("func1(1.0,2),func3(2,3.0)",1.0),
      ("func2(,3),func4(3,-3.0)",  -9.0), ("func2(,3),func3(2,-3.0)", -5.0),
      ("func1(1.0,3),func4(3,4.0)",12.0), ("func1(1.0,3),func3(2,4.0)",2.0),
      ("func2(,4),func4(3,-4.0)", -12.0), ("func2(,4),func3(2,-4.0)", -6.0),
      ("func1(1.0,4),func4(3,5.0)",15.0), ("func1(1.0,4),func3(2,5.0)",3.0))

    // sequence followed by cross-product
    val f0a12c34 = f0 ->: allP(f1,f2) *: allP(f3,f4)
    //println(f0a12c34)
    val cf0a12c34: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f0a12c34)
    //println(cf0a12c34)
    cf0a12c34.isRight shouldBe true
    val cf0a12c34e = cf0a12c34.right.get
    val cf0a12c34r = exec(List(1,2,3,4),cf0a12c34e)
    //println(cf0a12c34r)
    cf0a12c34r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func0(1,1),func2(,2),func4(3,-2.0)",  -6.0), ("func0(1,1),func2(,2),func3(2,-2.0)",  -4.0),
      ("func0(1,1),func1(1.0,2),func4(3,3.0)", 9.0), ("func0(1,1),func1(1.0,2),func3(2,3.0)", 1.0),
      ("func0(1,2),func2(,3),func4(3,-3.0)",  -9.0), ("func0(1,2),func2(,3),func3(2,-3.0)",  -5.0),
      ("func0(1,2),func1(1.0,3),func4(3,4.0)",12.0), ("func0(1,2),func1(1.0,3),func3(2,4.0)", 2.0),
      ("func0(1,3),func2(,4),func4(3,-4.0)", -12.0), ("func0(1,3),func2(,4),func3(2,-4.0)",  -6.0),
      ("func0(1,3),func1(1.0,4),func4(3,5.0)",15.0), ("func0(1,3),func1(1.0,4),func3(2,5.0)", 3.0),
      ("func0(1,4),func2(,5),func4(3,-5.0)", -15.0), ("func0(1,4),func2(,5),func3(2,-5.0)",  -7.0),
      ("func0(1,4),func1(1.0,5),func4(3,6.0)",18.0), ("func0(1,4),func1(1.0,5),func3(2,6.0)", 4.0))

    // cross-product followed by sequence

    val f12c34a0 = allP(f1,f2) *: allP(f3,f4) ->: f5
    //println(f12c34a0)
    val cf12c34a0: Either[ADWError, Pipes.Exec[Int, Boolean]] = compileP(f12c34a0)
    //println(cf12c34a0)
    cf12c34a0.isRight shouldBe true
    val cf12c34a0e = cf12c34a0.right.get
    val cf12c34a0r = exec(List(1,2,3,4),cf12c34a0e)
    //println(cf12c34a0r)
    cf12c34a0r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func2(,1),func4(3,-1.0),func5(2,-3.0)",false),  ("func2(,1),func3(2,-1.0),func5(2,-3.0)",false),
      ("func1(1.0,1),func4(3,2.0),func5(2,6.0)",true),  ("func1(1.0,1),func3(2,2.0),func5(2,0.0)",true),
      ("func2(,2),func4(3,-2.0),func5(2,-6.0)", true),  ("func2(,2),func3(2,-2.0),func5(2,-4.0)", true),
      ("func1(1.0,2),func4(3,3.0),func5(2,9.0)",false), ("func1(1.0,2),func3(2,3.0),func5(2,1.0)",false),
      ("func2(,3),func4(3,-3.0),func5(2,-9.0)", false), ("func2(,3),func3(2,-3.0),func5(2,-5.0)", false),
      ("func1(1.0,3),func4(3,4.0),func5(2,12.0)",true), ("func1(1.0,3),func3(2,4.0),func5(2,2.0)", true),
      ("func2(,4),func4(3,-4.0),func5(2,-12.0)", true), ("func2(,4),func3(2,-4.0),func5(2,-6.0)",  true),
      ("func1(1.0,4),func4(3,5.0),func5(2,15.0)",false),("func1(1.0,4),func3(2,5.0),func5(2,3.0)",false))

    val f0a12c34a5 = f0 ->: allP(f1,f2) *: allP(f3,f4) ->: f5
    //println(f0a12c34a5)
    val cf0a12c34a5: Either[ADWError, Pipes.Exec[Int, Boolean]] = compileP(f0a12c34a5)
    //println(cf0a12c34a5)
    cf0a12c34a5.isRight shouldBe true
    val cf0a12c34a5e = cf0a12c34a5.right.get
    val cf0a12c34a5r = exec(List(1,2,3,4),cf0a12c34a5e)
    //println(cf0a12c34a5r)
    cf0a12c34a5r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func0(1,1),func2(,2),func4(3,-2.0),func5(2,-6.0)",true),("func0(1,1),func2(,2),func3(2,-2.0),func5(2,-4.0)",true),
      ("func0(1,1),func1(1.0,2),func4(3,3.0),func5(2,9.0)",false),("func0(1,1),func1(1.0,2),func3(2,3.0),func5(2,1.0)",false),
      ("func0(1,2),func2(,3),func4(3,-3.0),func5(2,-9.0)",false), ("func0(1,2),func2(,3),func3(2,-3.0),func5(2,-5.0)",false),
      ("func0(1,2),func1(1.0,3),func4(3,4.0),func5(2,12.0)",true), ("func0(1,2),func1(1.0,3),func3(2,4.0),func5(2,2.0)",true),
      ("func0(1,3),func2(,4),func4(3,-4.0),func5(2,-12.0)",true), ("func0(1,3),func2(,4),func3(2,-4.0),func5(2,-6.0)",true),
      ("func0(1,3),func1(1.0,4),func4(3,5.0),func5(2,15.0)",false), ("func0(1,3),func1(1.0,4),func3(2,5.0),func5(2,3.0)",false),
      ("func0(1,4),func2(,5),func4(3,-5.0),func5(2,-15.0)",false), ("func0(1,4),func2(,5),func3(2,-5.0),func5(2,-7.0)",false),
      ("func0(1,4),func1(1.0,5),func4(3,6.0),func5(2,18.0)",true), ("func0(1,4),func1(1.0,5),func3(2,6.0),func5(2,4.0)",true))

  }

  it should "allow filtering" in {

    // These are the functions we want to compose
    // They have a specific structure.
    // They must access two parameters: one is the parameter the other is the input
    // They must return a tuple:
    // - The first element is tracking data that will be recorded
    // - The second element is the output which will be passed on to the next function

    //def func0(p:Int, v:Int): Int = v+p
    def func1(p:Double, v:Int): Double = v.toDouble + p
    def func2(p:String, v:Int): Double = -v.toDouble
    def func3(p:Int, v:Double): Double = v.toDouble - p
    def func4(p:Int, v:Double): Double = p*v.toDouble
    //def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    //val f0 = f("func0", 1,   func0)
    val f1 = f("func1", 1.0, func1)
    val f2 = f("func2", "",  func2)
    val f3 = f("func3", 2,   func3)
    val f4 = f("func4", 3,   func4)
    //val f5 = f("func5", 2,   func5)

    // Sets of filters to terminate early

    def none[P,I](a:Runtime, i:I, id: Option[Runtime]) : Boolean = false
    def everything[P,I](a:Runtime, i:I, id: Option[Runtime]) : Boolean = true
    def even[P,I](a:Runtime, i:Int, id: Option[Runtime]) : Boolean = (i % 2.0) == 0
    def positive[P,I](a:Runtime, i:Double, id: Option[Runtime]) : Boolean = i >= 0
    def evenD[P,I](a:Runtime, i:Double, id: Option[Runtime]) : Boolean = (i % 2.0) == 0

    // Remove everything
    val fs1 = iff(none, f1)
    //println(fs1)
    val cfs1: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(fs1)
    //println(cfs1)
    cfs1.isRight shouldBe true
    val cfs1e = cfs1.right.get
    val cfs1r = exec(List(1, 2, 3, 4),cfs1e)
    //println(cfs1r)
    cfs1r should contain theSameElementsInOrderAs List()

    // Remove nothing
    val f2s1 = iff(everything, f1)
    //println(f2s1)
    val cf2s1: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f2s1)
    //println(cf2s1)
    cf2s1.isRight shouldBe true
    val cf2s1e = cf2s1.right.get
    val cf2s1r = exec(List(1, 2, 3, 4),cf2s1e)
    //println(cf2s1r)
    cf2s1r.map( e => (e._1.all, e._2)) should contain theSameElementsInOrderAs List(
      ("func1(1.0,1)",2.0), ("func1(1.0,2)",3.0),
      ("func1(1.0,3)",4.0), ("func1(1.0,4)",5.0))

    // Just allow inputs that are even
    val f1_f2 = allP(f1,f2)
    val fs12 = iff(even, f1_f2)
    //println(fs12)
    val cfs12: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(fs12)
    //println(cf2s1)
    cfs12.isRight shouldBe true
    val cfs12e = cfs12.right.get
    val cfs12r = exec(List(1, 2, 3, 4),cfs12e)
    //println(cfs12r)
    // theSameElementsInOrderAs
    cfs12r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func2(,2)",-2.0), ("func1(1.0,2)",3.0),
      ("func2(,4)",-4.0), ("func1(1.0,4)",5.0))

    // Cross products of sets of functions
    //val f12c34 = allP(f1,f2) *: iff(positive,allP(f3,f4))
    val f12c34 = allP(f1,f2) *: iff(positive,f3,f4)
    //println(f12c34)
    val cf12c34: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f12c34)
    //println(cf12c34)
    cf12c34.isRight shouldBe true
    val cf12c34e = cf12c34.right.get
    val cf12c34r = exec(List(1,2,3,4),cf12c34e)
    //println(cf12c34r)
    // theSameElementsInOrderAs
    cf12c34r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,1),func4(3,2.0)",6.0),  ("func1(1.0,1),func3(2,2.0)",0.0),
      ("func1(1.0,2),func4(3,3.0)",9.0),  ("func1(1.0,2),func3(2,3.0)",1.0),
      ("func1(1.0,3),func4(3,4.0)",12.0), ("func1(1.0,3),func3(2,4.0)",2.0),
      ("func1(1.0,4),func4(3,5.0)",15.0), ("func1(1.0,4),func3(2,5.0)",3.0))

    // Cross products of sets of functions
    import pt.inescn.search.Pipes.IffOps

    val nf = positive _ | evenD
    val f12c34b = allP(f1,f2) *: iff(nf,allP(f3,f4))
    //println(f12c34b)
    val c12c34b: Either[ADWError, Pipes.Exec[Int, Double]] = compileP(f12c34b)
    //println(c12c34b)
    c12c34b.isRight shouldBe true
    val c12c34be = c12c34b.right.get
    val c12c34br = exec(List(1,2,3,4),c12c34be)
    //println(c12c34br)
    // theSameElementsInOrderAs
    c12c34br.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,1),func4(3,2.0)",6.0),  ("func1(1.0,1),func3(2,2.0)",0.0),
      ("func2(,2),func4(3,-2.0)",-6.0),   ("func2(,2),func3(2,-2.0)",-4.0),
      ("func1(1.0,2),func4(3,3.0)",9.0),  ("func1(1.0,2),func3(2,3.0)",1.0),
      ("func1(1.0,3),func4(3,4.0)",12.0), ("func1(1.0,3),func3(2,4.0)",2.0),
      ("func2(,4),func4(3,-4.0)",-12.0),  ("func2(,4),func3(2,-4.0)",-6.0),
      ("func1(1.0,4),func4(3,5.0)",15.0), ("func1(1.0,4),func3(2,5.0)",3.0))

  }

  it should "allow gracefully handling exceptions thrown by functions" in {

    // Create a function that will fail
    def func0(p:Int, v:Double): Double = v+p
    def func1(p:Double, v:Double): Double = v.toDouble + p
    def func2(p:String, v:Double): Double = if (v==3) throw new RuntimeException("func2(v==3)") else -v.toDouble
    def func3(p:Int, v:Double): Double = v.toDouble - p
    //def func4(p:Int, v:Double): Double = p*v.toDouble
    //def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    val f0: Pipes.Func[Int, Double, Double] = f("func0", 1, func0)
    val f1 = f("func1", 1.0, func1)
    val f2 = f("func2", "",  func2)
    val f3 = f("func3", 2,   func3)
    //val f4 = f("func4", 3,   func4)
    //val f5 = f("func5", 2,   func5)

    // Compile and execute
    val c1 = allP(f0, f1) ->: allP(f2, f3)
    //println(c1)
    val cc1: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c1)
    //println(cc1)
    cc1.isRight shouldBe true
    val cc1e = cc1.right.get
    val cc1r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc1e)
    //println(cc1r)
    // theSameElementsInOrderAs
    // One result is missing, failed silently
    cc1r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,1.0),func3(2,2.0)",0.0),
      ("func1(1.0,2.0),func3(2,3.0)",1.0),
      ("func1(1.0,3.0),func3(2,4.0)",2.0),
      ("func1(1.0,4.0),func3(2,5.0)",3.0),
      ("func0(1,1.0),func2(,2.0)",-2.0),
      // ("func0(1,2.0),func2(,3.0)",-3.0), exception was thrown
      ("func0(1,3.0),func2(,4.0)",-4.0),
      ("func0(1,4.0),func2(,5.0)",-5.0))

    // But we can check for failures because they are tracked
    val cc2r: Seq[Either[ADWError, (Runtime, Double)]] = execDebug(List(1.0, 2, 3, 4), cc1e)
    //println(cc2r)
    val failures = cc2r.filter( _.isLeft )
    failures.length shouldBe 1
    val failed = failures.head
    failed should be ('left)
    // We can print the stack trace
    //println(failed.left.get)
  }

  "Using Iff" should "allow selecting an operation that was not used yet in a single line" in {

    def func0(p:Int, v:Double): Double = v+p
    def func1(p:Double, v:Double): Double = v.toDouble + p
    def func2(p:String, v:Double): Double = -v.toDouble
    def func3(p:Int, v:Double): Double = v.toDouble - p
    //def func4(p:Int, v:Double): Double = p*v.toDouble
    //def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    val f0 = f("func0", 1,   func0)
    val f1 = f("func1", 1.0, func1)
    val f2 = f("func2", "",  func2)
    val f3 = f("func3", 2,   func3)
    //val f4 = f("func4", 3,   func4)
    //val f5 = f("func5", 2,   func5)

    def notDone[I](a:Runtime, i:I, p: Option[Runtime]) : Boolean = p match {
      case None => true
      case Some(track) =>
        // lstIDs contains last call id, so its the current call p that will follow
        // Make sure we execute this function only if it has not been executed
        // previously with the same parameters. If you just want the name, use
        // track.setNames : !a.setNames.contains(track.setNames.head)
        !a.setIDs.contains(track.lstIDs.head)
    }

    val f0f = iff(notDone,f0)
    val f1f = iff(notDone,f1)
    val f2f = iff(notDone,f2)
    val f3f = iff(notDone,f3)
    /*val f4f = iff(notDone,f4)*/
    //val c1 = allP(f0,f1,f2,f3,f4) *: allP(f0f,f1f,f2f,f3f,f4f)
    val c1 = allP(f0,f1) *: allP(f0f,f1f)
    //println(c1)
    val cc1: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c1)
    //println(cc1)
    cc1.isRight shouldBe true
    val cc1e = cc1.right.get
    val cc1r: Seq[(Runtime, Double)] = exec(List(1.0,2,3,4),cc1e)
    //println(cc1r)
    // theSameElementsInOrderAs
    cc1r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,1.0),func0(1,2.0)",3.0),
      ("func1(1.0,2.0),func0(1,3.0)",4.0),
      ("func1(1.0,3.0),func0(1,4.0)",5.0),
      ("func1(1.0,4.0),func0(1,5.0)",6.0),
      ("func0(1,1.0),func1(1.0,2.0)",3.0),
      ("func0(1,2.0),func1(1.0,3.0)",4.0),
      ("func0(1,3.0),func1(1.0,4.0)",5.0),
      ("func0(1,4.0),func1(1.0,5.0)",6.0))

    val c2 = allP(f0,f1,f2) *: allP(f0f,f1f,f3) *: allP(f2f,f3f)
    //println(c2)
    val cc2: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c2)
    //println(cc2)
    cc2.isRight shouldBe true
    val cc2e = cc2.right.get
    val cc2r: Seq[(Runtime, Double)] = exec(List(1.0,2,3,4),cc2e)
    //println(cc2r)
    cc2r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func0(1,1.0),func1(1.0,2.0),func2(,3.0)",-3.0),
      ("func0(1,2.0),func1(1.0,3.0),func2(,4.0)",-4.0),
      ("func0(1,3.0),func1(1.0,4.0),func2(,5.0)",-5.0),
      ("func0(1,4.0),func1(1.0,5.0),func2(,6.0)",-6.0),

      ("func0(1,1.0),func1(1.0,2.0),func3(2,3.0)",1.0),
      ("func0(1,2.0),func1(1.0,3.0),func3(2,4.0)",2.0),
      ("func0(1,3.0),func1(1.0,4.0),func3(2,5.0)",3.0),
      ("func0(1,4.0),func1(1.0,5.0),func3(2,6.0)",4.0),

      ("func0(1,1.0),func3(2,2.0),func2(,0.0)",-0.0),
      ("func0(1,2.0),func3(2,3.0),func2(,1.0)",-1.0),
      ("func0(1,3.0),func3(2,4.0),func2(,2.0)",-2.0),
      ("func0(1,4.0),func3(2,5.0),func2(,3.0)",-3.0),

      ("func1(1.0,1.0),func0(1,2.0),func2(,3.0)",-3.0),
      ("func1(1.0,2.0),func0(1,3.0),func2(,4.0)",-4.0),
      ("func1(1.0,3.0),func0(1,4.0),func2(,5.0)",-5.0),
      ("func1(1.0,4.0),func0(1,5.0),func2(,6.0)",-6.0),

      ("func1(1.0,1.0),func0(1,2.0),func3(2,3.0)",1.0),
      ("func1(1.0,2.0),func0(1,3.0),func3(2,4.0)",2.0),
      ("func1(1.0,3.0),func0(1,4.0),func3(2,5.0)",3.0),
      ("func1(1.0,4.0),func0(1,5.0),func3(2,6.0)",4.0),

      ("func1(1.0,1.0),func3(2,2.0),func2(,0.0)",-0.0),
      ("func1(1.0,2.0),func3(2,3.0),func2(,1.0)",-1.0),
      ("func1(1.0,3.0),func3(2,4.0),func2(,2.0)",-2.0),
      ("func1(1.0,4.0),func3(2,5.0),func2(,3.0)",-3.0),

      ("func2(,1.0),func0(1,-1.0),func3(2,0.0)",-2.0),
      ("func2(,2.0),func0(1,-2.0),func3(2,-1.0)",-3.0),
      ("func2(,3.0),func0(1,-3.0),func3(2,-2.0)",-4.0),
      ("func2(,4.0),func0(1,-4.0),func3(2,-3.0)",-5.0),

      ("func2(,1.0),func1(1.0,-1.0),func3(2,0.0)",-2.0),
      ("func2(,2.0),func1(1.0,-2.0),func3(2,-1.0)",-3.0),
      ("func2(,3.0),func1(1.0,-3.0),func3(2,-2.0)",-4.0),
      ("func2(,4.0),func1(1.0,-4.0),func3(2,-3.0)",-5.0))

  }

  it should "allow selecting the next operation according to a runtime condition" in {

    def func0(p:Int, v:Double): Double = v+p
    def func1(p:Double, v:Double): Double = v.toDouble + p
    //def func2(p:String, v:Double): Double = -v.toDouble
    //def func3(p:Int, v:Double): Double = v.toDouble - p
    //def func4(p:Int, v:Double): Double = p*v.toDouble
    //def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    val f0 = f("func0", 1,   func0)
    val f1 = f("func1", 1.0, func1)
    //val f2 = f("func2", "",  func2)
    //val f3 = f("func3", 2,   func3)
    //val f4 = f("func4", 3,   func4)
    //val f5 = f("func5", 2,   func5)

    def notDone(a: Runtime, i: Double, p: Option[Runtime]): Boolean = p match {
      case None => true
      case Some(track) => // !a.used.contains(name) && (i%2 == 0)
        // lstIDs contains last call id, so its the current call p that will follow
        // Make sure we execute this function only if it has not been executed
        // previously with the same parameters. If you just want the name, use
        // track.setNames : !a.setNames.contains(track.setNames.head)
        !a.setIDs.contains(track.lstIDs.head) && (i%2 == 0)
    }

    val f0f = iff(notDone, f0)
    val f1f = iff(notDone, f1)
    /*val f2f = iff(notDone, f2)
    val f3f = iff(notDone, f3)
    val f4f = iff(notDone,f4)*/
    //val c1 = allP(f0,f1,f2,f3,f4) *: allP(f0f,f1f,f2f,f3f,f4f)
    val c1 = allP(f0, f1) *: allP(f0f, f1f)
    //println(c1)
    val cc1: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c1)
    //println(cc1)
    cc1.isRight shouldBe true
    val cc1e = cc1.right.get
    val cc1r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc1e)
    //println(cc1r)
    // theSameElementsInOrderAs
    cc1r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,1.0),func0(1,2.0)",3.0),
      ("func1(1.0,3.0),func0(1,4.0)",5.0),
      ("func0(1,1.0),func1(1.0,2.0)",3.0),
      ("func0(1,3.0),func1(1.0,4.0)",5.0))
  }


  it should "allow filtering the output" in {


    def func0(p:Int, v:Double): Double = v+p
    def func1(p:Double, v:Double): Double = v.toDouble + p
    //def func2(p:String, v:Double): Double = -v.toDouble
    //def func3(p:Int, v:Double): Double = v.toDouble - p
    //def func4(p:Int, v:Double): Double = p*v.toDouble
    //def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0
    def funcId(p: String, v: Double): Double = v

    val f0 = f("func0", 1,   func0)
    val f1 = f("func1", 1.0, func1)
    //val f2 = f("func2", "",  func2)
    //val f3 = f("func3", 2,   func3)
    //val f4 = f("func4", 3,   func4)
    //val f5 = f("func5", 2,   func5)
    val fid = f("fid", "", funcId)

    def notDone(a: Runtime, i: Double, p: Option[Runtime]): Boolean = p match {
      case None => true
      case Some(track) => // !a.used.contains(name) && (i%2 == 0)
        // lstIDs contains last call id, so its the current call p that will follow
        // Make sure we execute this function only if it has not been executed
        // previously with the same parameters. If you just want the name, use
        // track.setNames : !a.setNames.contains(track.setNames.head)
        !a.setIDs.contains(track.lstIDs.head) && (i%2 == 0)
    }

    def myFilter(a: Runtime, i: Double, p: Option[Runtime]): Boolean = i > 4.0

    val f0f = iff(notDone, f0)
    val f1f = iff(notDone, f1)
    val filter = iff(myFilter, fid)
    /*val f2f = iff(notDone, f2)
    val f3f = iff(notDone, f3)
    val f4f = iff(notDone,f4)*/
    //val c1 = allP(f0,f1,f2,f3,f4) *: allP(f0f,f1f,f2f,f3f,f4f)
    val c1 = allP(f0, f1) *: allP(f0f, f1f) ->: filter
    //println(c1)
    val cc1: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c1)
    //println(cc1)
    cc1.isRight shouldBe true
    val cc1e = cc1.right.get
    val cc1r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc1e)
    //println(cc1r)
    // theSameElementsInOrderAs
    cc1r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,3.0),func0(1,4.0),fid(,5.0)",5.0),
      ("func0(1,3.0),func1(1.0,4.0),fid(,5.0)",5.0))
  }

  it should "allow using All construct on functions with different parameters" in {

    def func0(p: String, v: Double): Double = v + p.toDouble
    def func1(p: Int, v: Double): Double = v + p

    val f0 = f("f0", "1.0", func0)
    val f1 = f("f1", 1, func1)

    val f01 = allP(f0,f1)
    val cfa01: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(f01)
    cfa01.isRight shouldBe true
    val cfa01e = cfa01.right.get
    val cfa01r = exec(List(1.0,2,3,4),cfa01e)
    //println(cfa01r)
    // theSameElementsInOrderAs
    cfa01r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("f1(1,1.0)",2.0), ("f0(1.0,1.0)",2.0),
      ("f1(1,2.0)",3.0), ("f0(1.0,2.0)",3.0),
      ("f1(1,3.0)",4.0), ("f0(1.0,3.0)",4.0),
      ("f1(1,4.0)",5.0), ("f0(1.0,4.0)",5.0))
  }

  "RunTime tracking" should "allow deploying a single pipe specified by its ids" in {

    def func0(p:Int, v:Double): Double = v+p
    def func1(p:Double, v:Double): Double = v.toDouble + p
    def func2(p:String, v:Double): Double = -v.toDouble
    def func3(p:Int, v:Double): Double = v.toDouble - p
    //def func4(p:Int, v:Double): Double = p*v.toDouble
    //def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    val f0: Pipes.Func[Int, Double, Double] = f("func0", 1, func0)
    val f1 = f("func1", 1.0, func1)
    val f2 = f("func2", "",  func2)
    val f3 = f("func3", 2,   func3)
    //val f4 = f("func4", 3,   func4)
    //val f5 = f("func5", 2,   func5)

    // We do standard set-up, compilation and execution
    val c1 = allP(f0, f1) ->: allP(f2, f3)
    //println(c1)
    val cc1: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c1)
    //println(cc1)
    cc1.isRight shouldBe true
    val cc1e = cc1.right.get
    val cc1r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc1e)
    //println(cc1r)
    // theSameElementsInOrderAs
    cc1r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,1.0),func3(2,2.0)",0.0),
      ("func1(1.0,2.0),func3(2,3.0)",1.0),
      ("func1(1.0,3.0),func3(2,4.0)",2.0),
      ("func1(1.0,4.0),func3(2,5.0)",3.0),
      ("func0(1,1.0),func2(,2.0)",-2.0),
      ("func0(1,2.0),func2(,3.0)",-3.0),
      ("func0(1,3.0),func2(,4.0)",-4.0),
      ("func0(1,4.0),func2(,5.0)",-5.0))

    // We can check for failures because they are tracked
    //val cc2r: Seq[Either[ADWError, (Runtime, Double)]] = execDebug(List(1.0, 2, 3, 4), cc1e)
    //println(cc2r)

    // Now lets deploy a single pipe
    // First select the pipe to deploy
    val track1 = cc1r(3)._1
    //println(s"track1.all = ${track1.all}")
    //println(s"track1.lstIDs = ${track1.lstIDs.mkString(",")}")
    track1.lstIDs shouldBe List("func0(1)", "func2()")

    // Create that single pipe. Note that in reality we will load and use a Runtime
    // instance that has been saved to a file or simply have a terminal function
    // record the list of Ids and construct a Runtime from that.
    val cc2: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c1, track1)
    //println(s"cc2 = $cc2")
    cc2 should be ('right)
    val cc2e = cc2.right.get
    val cc2r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc2e)
    //println(cc2r)
    // theSameElementsInOrderAs
    cc2r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func0(1,1.0),func2(,2.0)",-2.0),
      ("func0(1,2.0),func2(,3.0)",-3.0),
      ("func0(1,3.0),func2(,4.0)",-4.0),
      ("func0(1,4.0),func2(,5.0)",-5.0))

    // Lets see this at work with an Iff
    // We only want to use inputs that are even
    def even[P,I](a:Runtime, i:Double, id: Option[Runtime]) : Boolean = (i % 2.0) == 0

    val f0e = iff(even, f0)
    val f1e = iff(even, f1)
    val c3 = allP(f0e, f1e) ->: allP(f2, f3)
    //println(c3)
    val cc3: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c3)
    //println(cc3)
    cc3.isRight shouldBe true
    val cc3e = cc3.right.get
    val cc3r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc3e)
    //println(cc3r)
    // theSameElementsInOrderAs
    cc3r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,2.0),func3(2,3.0)",1.0),
      ("func1(1.0,4.0),func3(2,5.0)",3.0),
      ("func0(1,2.0),func2(,3.0)",-3.0),
      ("func0(1,4.0),func2(,5.0)",-5.0))

    // Now lets select the same pipe as above, but manually construct it
    // We ony need the Runtime.lstIDs member filled in correctly
    val funcIDs = List("func0(1)", "func2()")
    val track2 = Runtime("",funcIDs,Set(),Set(),funcIDs)
    // Create that single pipe from the Pipes descriptor.
    val cc4: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c3, track2)
    //println(s"cc4 = $cc4")
    cc4 should be ('right)
    val cc4e = cc4.right.get
    val cc4r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc4e)
    //println(cc4r)
    // theSameElementsInOrderAs
    // And we get the same results but only for the selected pipe
    cc4r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func0(1,2.0),func2(,3.0)",-3.0),
      ("func0(1,4.0),func2(,5.0)",-5.0))

    // But what happens if we create an invalid pipe descriptor for the track?
    // Note that we only change the parameter of the first function call
    val funcIDs3 = List("func0(11)", "func2()")
    val track3 = Runtime("",funcIDs3,Set(),Set(),funcIDs3)
    // Create that single pipe from the Pipes descriptor.
    val cc5: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c3, track3)
    //println(s"cc5 = $cc5")
    // We have a compilation error
    cc5 should be ('left)
  }


  "Using When" should "allow selecting the next operation according to a compile-time condition" in {

    def func0(p:Int, v:Double): Double = v+p
    def func1(p:Double, v:Double): Double = v.toDouble + p
    //def func2(p:String, v:Double): Double = -v.toDouble
    //def func3(p:Int, v:Double): Double = v.toDouble - p
    //def func4(p:Int, v:Double): Double = p*v.toDouble
    //def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    val f0 = f("func0", 1,   func0)
    val f1 = f("func1", 1.0, func1)
    //val f2 = f("func2", "",  func2)
    //val f3 = f("func3", 2,   func3)
    //val f4 = f("func4", 3,   func4)
    //val f5 = f("func5", 2,   func5)

    // The following function is used to ensure that we do not use pipes in
    // which a function is execute more than once. For example we can try
    // and apply a set of pre-processing steps but not repeat the same step
    // more than once.
    // After each compilation step, which is done in a recursively bottom up,
    // we get a compilation track in the form of a Compile. Here we record the
    // combination of functions in sequence (list) and th set of all functions
    // called (function name and parameters are used). If the set of function
    // IDs are not equal to the list of ID's then we know that a at least one
    // function has been called more than once with the same parameters.
    // If we apply the When on an operator (combination of functions), then
    // the parameter p is None. If however we call a When on a Function, then
    // we also provide that functions ID.
    def noRepeats(a: Compile, p: Option[Compile]): Boolean = p match {
      case None =>
        a.setIDs.size == a.lstIDs.length
      case Some(track) =>
        a.setIDs.size == a.lstIDs.length
    }

    // We will create all combinations of (f0,f1) * (f0,f1)
    val af0_1 = allP(f0, f1)
    val c0 = af0_1 *: af0_1
    // But we check at compile time that we do not repeat a call to
    // the same function in the pipe we generate
    val c1 = when(noRepeats,c0)
    //println(c1)
    val cc1: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c1)
    //println(cc1)
    cc1.isRight shouldBe true
    val cc1e = cc1.right.get
    val cc1r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc1e)
    //println(cc1r)
    // theSameElementsInOrderAs
    // We can see that no combinations of func_i  occur were "_i" s repeated
    cc1r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,1.0),func0(1,2.0)",3.0),
      ("func1(1.0,2.0),func0(1,3.0)",4.0),
      ("func1(1.0,3.0),func0(1,4.0)",5.0),
      ("func1(1.0,4.0),func0(1,5.0)",6.0),
      ("func0(1,1.0),func1(1.0,2.0)",3.0),
      ("func0(1,2.0),func1(1.0,3.0)",4.0),
      ("func0(1,3.0),func1(1.0,4.0)",5.0),
      ("func0(1,4.0),func1(1.0,5.0)",6.0))
  }

  it should "allow selecting a single function according to some criterion" in {

    def func0(p:Int, v:Double): Double = v+p
    def func1(p:Double, v:Double): Double = v.toDouble + p
    //def func2(p:String, v:Double): Double = -v.toDouble
    //def func3(p:Int, v:Double): Double = v.toDouble - p
    //def func4(p:Int, v:Double): Double = p*v.toDouble
    //def func5(p:Int, v: Double): Boolean = v.toDouble % p == 0

    val f0 = f("func0", 1,   func0)
    val f1 = f("func1", 1.0, func1)
    val f02 = f("func0", 2,   func0)
    //val f2 = f("func2", "",  func2)
    //val f3 = f("func3", 2,   func3)
    //val f4 = f("func4", 3,   func4)
    //val f5 = f("func5", 2,   func5)

    // The following function is used to ensure that we do not use pipes in
    // which a function is execute more than once. For example we can try
    // and apply a set of pre-processing steps but not repeat the same step
    // more than once.
    // After each compilation step, which is done in a recursively bottom up,
    // we get a compilation track in the form of a Compile. Here we record the
    // combination of functions in sequence (list) and th set of all functions
    // called (function name and parameters are used). If the set of function
    // IDs are not equal to the list of ID's then we now that a at least one
    // function has been called more than once with the same parameters.
    // If we apply the When on an operator (combination of functions), then
    // the parameter p is None. If however we call a When on a Function, then
    // we also provide that functions ID.
    def noRepeats(a: Compile, p: Option[Compile]): Boolean = p match {
      case None =>
        a.setIDs.size == a.lstIDs.length
      case Some(track) =>
        a.setIDs.size == a.lstIDs.length
    }

    // Same as above, but here we have only access to the leaf node that is a Function
    // We only let the compiler generate pipes when the funtion's parameter is "1"
    def iffc(a: Compile, p: Option[Compile]) : Boolean = p match {
      case None => true
      case Some(c) =>
        //val call = c.lstIDs.head
        c.lstParams.head == "1"
    }

    // We will create all combinations of (f0,f1) * (f0,f1)
    val af0_1 = allP(f0, f1)
    // But only if these functions have a parameter 1
    val wf0_1 = when(iffc,f0)
    val wf0_2 = when(iffc,f02)
    //val af0_2 = allP(f0, f02, f1)
    val af0_2 = allP(wf0_1, wf0_2, f1)
    val c0 = af0_1 *: af0_2
    val c1 = when(noRepeats,c0)
    //println(c1)
    val cc1: Either[ADWError, Pipes.Exec[Double, Double]] = compileP(c1)
    //println(cc1)
    cc1.isRight shouldBe true
    val cc1e = cc1.right.get
    val cc1r: Seq[(Runtime, Double)] = exec(List(1.0, 2, 3, 4), cc1e)
    //println(cc1r)
    // theSameElementsInOrderAs
    // We can see that no combinations of func_i  occur were "_i" s repeated
    // and no func0(2) appears in the composition (to check this use the commented af0_2)
    cc1r.map( e => (e._1.all, e._2)) should contain theSameElementsAs List(
      ("func1(1.0,1.0),func0(1,2.0)",3.0),
      ("func1(1.0,2.0),func0(1,3.0)",4.0),
      ("func1(1.0,3.0),func0(1,4.0)",5.0),
      ("func1(1.0,4.0),func0(1,5.0)",6.0),
      ("func0(1,1.0),func1(1.0,2.0)",3.0),
      ("func0(1,2.0),func1(1.0,3.0)",4.0),
      ("func0(1,3.0),func1(1.0,4.0)",5.0),
      ("func0(1,4.0),func1(1.0,5.0)",6.0))
  }

}
