package pt.inescn.model.stream

import java.time.{Duration, LocalDateTime}

import org.scalatest.{FlatSpec, Matchers}
import com.github.tototoshi.csv.DefaultCSVFormat
import pt.inescn.etl.Load.format
import pt.inescn.etl.stream.DataFiles
import pt.inescn.etl.stream.Load._
import pt.inescn.eval.stream.BinaryClassMetrics
import pt.inescn.eval.{MCC, Precision, Recall}
import pt.inescn.models.stream.{MeanSD, MeanWSD}
import pt.inescn.app.Utils

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.model.stream.ModelSpec"
  *
  * Created by hmf on 07-06-2017.
  */
class ModelSpec extends FlatSpec with Matchers {

  "Simple mean + SD model example" should "produce boolean evaluation metrics" in {


    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    import better.files._
    //import File._
    //import java.io.{File => JFile}
    import better.files.Dsl._

    /*
    // Used to generate the data
    import org.hipparchus.random.{JDKRandomGenerator, RandomDataGenerator}

    val rnd = RandomDataGenerator.of( new JDKRandomGenerator() )
    val randomDataGenerator: RandomDataGenerator = RandomDataGenerator.of(rnd)
    val mean = 3 // bad, 1 // good
    val sd = 1 // bad, 0.5 // good

    val rand1 = 1 to 200 map (_ => randomDataGenerator.nextNormal(mean,sd))
    println(rand1.mkString("\n"))
    */

    // NOTE: the files are read in the order they are found when we use glob searching.
    // Here we enumerate the files so that the non-failure data is read in first
    val file1:File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_watt0_norm_mm0_exp1.csv"
    val file2:File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_watt0_or_mm0_exp1.csv"
    val files = Iterator(file1, file2)

    // Parse the file name to get the features
    //val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_2"
    //val matchNames = "*watt*.csv"
    //val features1 = fileNamesfeatures(dir, matchNames) // Don't use globbing due to the order
    val features1 = DataFiles.fileNamesfeatures(files, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1)
    //println(features2.toList)

    // Load the file as a stream and extend exisiting features
    // with the ones parsed from the file names. We assume the
    // data fields are correct (no checks)
    val data0 = Frame.extendCSVs(MyFormat, features2)
    //val read = data0.iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // convert t,x to double
    val data1 = data0((x:String) => x.toDouble, "t", "x")

    // Create a time stamp from a given local date
    val data2 = data1((x:Double) => (x * 1e9).toLong, "t")
    val data3 = data2((x: Long) => Duration.ofNanos(x), "t")
    val stamp = LocalDateTime.parse( "2018-05-14 10:49:58.701", format)
    val data4 = data3((x:Duration) => stamp.plus(x), "t" )
    //val read = data4.iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // convert the damage column to integers (0 - no failure /1 -  anomaly present)
    val data5 = data4((x:String) => if (x.toLowerCase == "norm") false else true, "damage" )
    //val read = data5.iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // rename damage to label
    val data6 = data5.renameCols("damage" -> "label")
    //val read = data6("label").iterable.iterator.toIndexedSeq
    //val read = data6.iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // Add a column Long counter (Stream from)
    //val counter = Stream.from(1).toIterator
    //val count = Frame("counter" -> counter)
    def from(c:Long):Stream[Long] = c #:: from(c+1)
    val count = Frame("counter" -> from(1L).toIterator)
    //println(count.colIDs)
    //println(count.colTypes)
    //val read = count.iterable.iterator.take(25).toIndexedSeq
    //val read = count.iterable.toIterator.take(25).toIndexedSeq
    //println(read.mkString(";\n"))

    val data7 = data6 || count
    //val read = data7.iterable.iterator.toIndexedSeq
    //println(read.take(25).mkString(";\n"))
    // should have 400 lines (220 each)
    //read.length shouldBe 400

    // slice to windows (with overlap)
    //val data8 = data7.window(20,10)
    /*val read1: immutable.IndexedSeq[Row] = data8.iterable.iterator.toIndexedSeq
    println(read1.take(1).mkString(";\n"))
    println(s"length = ${read1.length}")*/

    // fit: generate the model - simple running mean and standard deviation model
    //  We only fir during the first maxSamples = 100
    val maxSamples = 100
    val data9 = MeanSD.fit(data7, List("x"), maxSamples)
    //val read = data9.iterable.iterator.take(105).toIndexedSeq
    //println(read.mkString(";\n"))

    // predict: use the model to check if an anomaly occurred
    // If the value is outside the expected range of mean +/- 1 standard
    // deviation, then tag as a true (failure) else false
    val data10 = MeanSD.predict(data9, List("x"), 2.0)
    //val read = data10.iterable.iterator.take(205).toIndexedSeq
    //println(read.mkString(";\n"))

    // evaluate: use the "label" and "predict" fields to update a new "metric" field
    // We assume that a "label" column exists. Drop the records used for learning
    val data11 = MeanSD.evaluate(data10.drop(maxSamples), List("x"))
    //val read = data11.iterable.iterator.take(105).toIndexedSeq
    //val read = data11("counter", "label", "x_prediction", "x_eval").iterable.iterator.toIndexedSeq
    //val read = data11("counter", "label", "x_prediction").iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // We have two files with 200 records each. The first 100 are used to learn
    // the model. We are left with 300 records.
    val data12 = data11.drop(299)
    //val read = data12("counter", "label", "x_prediction", "x_eval").iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))
    // Get the last remaining (total) metrics
    //val metrics  = data12( ("x_eval", MeanSD.getBinaryClassMetrics _) ).toIndexedSeq
    implicit val defBinaryMetrics: BinaryClassMetrics = BinaryClassMetrics()
    val metrics = data12((e:BinaryClassMetrics) => e, "x_eval" ).iterable.toIndexedSeq
    //println(metrics.mkString(";\n"))
    // Lets see if we have the expected performance
    val metric = unpack[BinaryClassMetrics](metrics(0)("x_eval"))
    val precision = metric.fold(_ => Precision(0.0),_.precision)
    precision.value should be >= 0.97
    precision.value should be <= 0.98
    val recall = metric.fold(_ => Recall(0.0), _.recall)
    recall.value should be >= 0.86
    recall.value should be <= 0.87
    val mcc = metric.fold(_ => MCC(0.0), _.mcc)
    mcc.value should be >= 0.78
    mcc.value should be <= 0.79
  }

  "Weighted mean + SD model example" should "produce boolean evaluation metrics" in {

    /*
    val v1 = Vector(1,2,3,4,5)
    println(v1)
    val v2 = v1 :+ 6
    println(v2)
    val v3 = v2.drop(1)
    println(v3)
    println(s"v3(0)} = ${v3(0)}")
    println(s"v3(4)} = ${v3(3)}")
    */

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    import better.files._
    //import File._
    //import java.io.{File => JFile}
    import better.files.Dsl._


    // NOTE: the files are read in the order they are found when we use glob searching.
    // Here we enumerate the files so that the non-failure data is read in first
    val file1:File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_watt0_norm_mm0_exp1.csv"
    val file2:File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_watt0_or_mm0_exp1.csv"
    val files = Iterator(file1, file2)

    // Parse the file name to get the features
    //val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_2"
    //val matchNames = "*watt*.csv"
    //val features1 = fileNamesfeatures(dir, matchNames) // Don't use globbing due to the order
    val features1 = DataFiles.fileNamesfeatures(files, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1)
    //println(features2.toList)

    // Load the file as a stream and extend existing features
    // with the ones parsed from the file names. We assume the
    // data fields are correct (no checks)
    val data0 = Frame.extendCSVs(MyFormat, features2)
    //val read = data0.iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // convert t,x to double
    val data1 = data0((x:String) => x.toDouble, "t", "x")

    // Create a time stamp from a given local date
    val data2 = data1((x:Double) => (x * 1e9).toLong, "t")
    val data3 = data2((x: Long) => Duration.ofNanos(x), "t")
    val stamp = LocalDateTime.parse( "2018-05-14 10:49:58.701", format)
    val data4 = data3((x:Duration) => stamp.plus(x), "t" )
    //val read = data4.iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // convert the damage column to integers (0 - no failure /1 -  anomaly present)
    val data5 = data4((x:String) => if (x.toLowerCase == "norm") false else true, "damage" )
    //val read = data5.iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // rename damage to label
    val data6 = data5.renameCols("damage" -> "label")
    //val read = data6("label").iterable.iterator.toIndexedSeq
    //val read = data6.iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))

    // Add a column Long counter (Stream from)
    //val counter = Stream.from(1).toIterator
    //val count = Frame("counter" -> counter)
    def from(c:Long):Stream[Long] = c #:: from(c+1)
    val count = Frame("counter" -> from(1L).toIterator)
    //println(count.colIDs)
    //println(count.colTypes)
    //val read = count.iterable.iterator.take(25).toIndexedSeq
    //val read = count.iterable.toIterator.take(25).toIndexedSeq
    //println(read.mkString(";\n"))

    val data7 = data6 || count
    //val read = data7.iterable.iterator.toIndexedSeq
    //println(read.take(25).mkString(";\n"))
    // should have 400 lines (220 each)
    //read.length shouldBe 400

    // fit: generate the model - simple running mean and standard deviation model
    //  We only fit during the first maxSamples = 100
    val maxSamples = 100
    val data9 = MeanWSD.fit(data7, List("x"), maxSamples)
    //val read = data9.iterable.iterator.take(105).toIndexedSeq
    //println(read.mkString(";\n"))

    // slice to windows (with overlap)
    val winSize = 20
    val stride = 10
    val data8 = data9.window(winSize, stride)
    //val read = data8.iterable.iterator.toIndexedSeq
    //println(read.take(1).mkString(";\n"))
    //println(s"length = ${read.length}")

    // predict: use the model to check if an anomaly occurred
    // We use the last model of the window to perform the prediction for
    // that window. If the window's mean is outside the expected range
    // of mean +/- 1 standard deviation, then tag as a true (failure) else false
    val data10 = MeanWSD.predict(data8, List("x"), 2.0)
    //val read = data10.iterable.iterator.take(205).toIndexedSeq
    //println(read.mkString(";\n"))
    //println(read.size) // 39 - last 10 samples will be missing
    //read.size shouldBe 39


    // evaluate: use the "label" and "predict" fields to update a new "metric" field
    // We assume that a "label" column exists. The window's label is assigned as anomaly
    // if at least one anomaly has been labelled in that window. The windows prediction
    // will be a single value that can be compared to the window label.
    //Drop the records used for learning
    val dropSize = maxSamples/(winSize - stride) // = 10 for slide(size=20,step=10)
    val data11 = MeanWSD.evaluate(data10.drop(dropSize), List("x"))
    //val read = data11.iterable.iterator.take(28).toIndexedSeq
    //val read = data11("counter", "label", "x_prediction", "x_eval").iterable.iterator.take(1).toIndexedSeq
    //val read = data11("counter", "label", "x_prediction").iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))
    //println(dropSize)

    // We have two files with 200 records each. The first 100 are used to learn
    // the model. We are left with 300 records. Records are grouped by window
    // so get last window's result
    val data12 = data11.drop(28) // total windows = 39, learning for first 10, so left with 29 windows
    //val read = data12("counter", "label", "x_prediction", "x_eval").iterable.iterator.toIndexedSeq
    //println(read.mkString(";\n"))
    // Get the last remaining (total) metrics
    implicit val defBinaryMetrics: BinaryClassMetrics = BinaryClassMetrics()
    val metrics = data12((e:BinaryClassMetrics) => e, "x_eval" ).iterable.toIndexedSeq
    //println(metrics.mkString(";\n"))
    // Lets see if we have the expected performance
    val metric = unpack[BinaryClassMetrics](metrics(0)("x_eval"))
    val precision = metric.fold(_ => Precision(0.0),_.precision)
    precision.value should be >= 1.0
    val recall = metric.fold(_ => Recall(0.0),_.recall)
    recall.value should be >= 1.0
    val mcc = metric.fold(_ => MCC(0.0),_.mcc)
    mcc.value should be >= 1.0
  }

}
