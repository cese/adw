/*******************************************************************************
 * Copyright (C) 2017 INESC-TEC
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 ******************************************************************************/
 package pt.inescn.utils

 import java.io.{InputStream, PrintStream}
 import java.lang.{System => JSystem}

 import better.files.File
 import com.github.cjlin1.utils.System


 object TestUtils {

  import org.scalatest.Matchers._

  /**
    * Used for timing a single call.
    */
  def time[R](block: => R): R = {
    val (result, _) = Utils.time(block)
    result
  }

  val precision = 1e-5

  /**
    * Compare two real values with a given precision. The `eps` parameter
    * determines the precision with which the comparison is done.
    */
  def chk(a: Double, b: Double, eps: Double = precision): Unit = {
    if (a.isNaN) b.isNaN should be(true)
    else if (a.isInfinity) b.isInfinity should be(true)
    else if (b.isNaN) a.isNaN should be(true)
    else if (b.isInfinity) a.isInfinity should be(true)
    else a should be(b +- eps)
  }

   /**
     * Creates a user-define System object that "replaces" the JVM wide
     * `System`. This is necessary to run code in several threads that use
     * Java System object. The Java System is not thread safe.
     *
     * @param inName - thread input data
     * @param outName - thread output data
     * @param errName -
     * @return
     */
   def createIOSys(inName: File, outName: File, errName:File): System = this.synchronized{
     val in: InputStream = inName.newInputStream
     val out: PrintStream = new java.io.PrintStream(outName.toJava)
     val err: PrintStream = new java.io.PrintStream(errName.toJava)
     new System(in, out, err)
   }

   def IOSys(inName: String, outName: String, errName:String): System = this.synchronized{
     val tmp = File(JSystem.getProperty("java.io.tmpdir"))
     val in = File(inName)
     val err = File.newTemporaryFile(prefix = errName, parent = Some(tmp))
     val out = File(outName)
     createIOSys(in,out,err)
   }

   def testIOSys(inName: String, outName: String, errName:String, suiteName:String): System = this.synchronized{
     IOSys(inName, outName+ "." + suiteName,errName)
   }


}