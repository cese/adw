/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.utils

import org.scalatest._
import pt.inescn.detector.perfect.{HashFound, UseData}


// import scala.collection.JavaConverters._

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.utils.NABUtilsSpec"
  *
  *
  */
class NABUtilsSpec extends FlatSpec with Matchers {

  import pt.inescn.utils.NABUtils._

  "Listing files" should "find the NAB data files" in {
    //import better.files._
    import better.files.Dsl._

    val data = cwd / ".." / "data/nab/data"
    val dataFiles = allDataFiles(data)
    dataFiles shouldBe 'defined
    // May change in the future
    dataFiles.get should have size 58
    //println(  dataFiles.mkString( "," ) )
    //println(  dataFiles.size )
  }

  it should "find the NAB label files" in {
    //import better.files._
    import better.files.Dsl._

    val labels = cwd / ".." / "data/nab/labels"
    val labelFiles = allLabelFiles(labels)
    labelFiles shouldBe 'defined
    // May change in the future
    labelFiles.get should have size 10
    //println( labelFiles.mkString( "," ) )
    //println(  labelFiles.size )
  }

  import pt.inescn.utils.NABUtils._
  // JSON parsing
  import org.json4s._
  import org.json4s.native.JsonMethods._
  // Dates (JDK < 8)
  //import java.util.Calendar
  import pt.inescn.utils.Utils._

  /**
    * Important note: if we use the hh:mm instead of HH:mm we get a
    * `Unable to obtain LocalTime from TemporalAccessor: {NanoOfSecond=0, MinuteOfHour=31,
    * MicroOfSecond=0, MilliOfSecond=0, HourOfAmPm=11, SecondOfMinute=13},ISO resolved to 2017-09-29 of
    * type java.time.format.Parsed`
    *
    * Problem is the parser does no know it is `am` or `pm` if this is  not indicated
    */
  "Basic date-time parsing" should "parse milliseconds" in {
    val datePattern = "yyyy-MM-dd HH:mm:ss.SSS"

    val str = "2017-09-29 11:31:13.123"
    val formatter = java.time.format.DateTimeFormatter.ofPattern(datePattern)
    val dateTime = java.time.LocalDateTime.parse(str, formatter)
    //println(dateTime)
    val r = java.time.LocalDateTime.of(2017, 9, 29, 11, 31, 13, 123 * 1000000)
    dateTime shouldBe r
  }

  it should "parse microseconds" in {
    val datePattern = "yyyy-MM-dd HH:mm:ss.SSSSSS"

    val str = "2017-09-29 11:31:13.123456"
    val formatter = java.time.format.DateTimeFormatter.ofPattern(datePattern)
    val dateTime = java.time.LocalDateTime.parse(str, formatter)
    //println(dateTime)
    val r = java.time.LocalDateTime.of(2017, 9, 29, 11, 31, 13, 123456 * 1000)
    dateTime shouldBe r
  }

  it should "parse nanoseconds" in {
    val datePattern = "yyyy-MM-dd HH:mm:ss.SSSSSSSSS"

    val str = "2017-09-29 11:31:13.123456789"
    val formatter = java.time.format.DateTimeFormatter.ofPattern(datePattern)
    val dateTime = java.time.LocalDateTime.parse(str, formatter)
    //println(dateTime)
    val r = java.time.LocalDateTime.of(2017, 9, 29, 11, 31, 13, 123456789)
    dateTime shouldBe r
  }

  it should "parse nanoseconds (2)" in {
    val datePattern = "yyyy-MM-dd HH:mm:ss.nnnnnnnnn"

    val str = "2017-09-29 11:31:13.123456789"
    val formatter = java.time.format.DateTimeFormatter.ofPattern(datePattern)
    val dateTime = java.time.LocalDateTime.parse(str, formatter)
    //println(dateTime)
    val r = java.time.LocalDateTime.of(2017, 9, 29, 11, 31, 13, 123456789)
    dateTime shouldBe r
  }

  case class Event(domain: String, filePath: String, timestamp: Long)

  /**
    * From: https://gist.github.com/djamelz/f963cab45933d05b2e10d8680e4213b6
    */
  "JSON Custom serializer" should "parse dates with microseconds" in {
    import org.joda.time.DateTime
    import org.joda.time.LocalDateTime
    import org.joda.time.format.DateTimeFormat
    import org.joda.time.DateTimeZone
    import org.json4s._
    import org.json4s.native.JsonMethods._

    val datePattern = "yyyy-MM-dd HH:mm:ss.SSSSSS"
    val s = """{"timestamp": "2016-09-29 11:31:13.247772", "domain": "d1", "filePath": "s3://..."}"""
    /*
    object StringToLong extends CustomSerializer[ Long ]( format => (
      { case JString( x ) => DateTime.parse( x, DateTimeFormat.forPattern( datePattern ) )
                                     .withZone( DateTimeZone.UTC )
                                     .getMillis },
      { case x: Long => JInt( x ) } ) )
    */
    /*
    object StringToLong extends CustomSerializer[ Long ]( format => (
      { case JString( x ) => LocalDateTime.parse(x, DateTimeFormat.forPattern( datePattern ))
        .toDateTime( DateTimeZone.UTC )
        .getMillis },
      { case x: Long => JInt( x ) } ) )
    */
    object StringToLong extends CustomSerializer[Long](format => ( {
      case JString(x) =>
        val ldt = LocalDateTime.parse(x, DateTimeFormat.forPattern(datePattern))
        val dt = ldt.toDateTime(DateTimeZone.UTC)
        val ms = dt.getMillis
        // println(dt) // same as input
        // println(ms) // ms we want
        val rd = new DateTime(ms)
        // println(rd) // adds daylight saving from time-zone
        // println(rd.withZone(DateTimeZone.UTC)) // back to original
        // println(rd.withZone(DateTimeZone.UTC).getMillis) // milli we want
        //println(rd.withZoneRetainFields(DateTimeZone.UTC)) // shows original but is not, see milli
        // println(rd.withZoneRetainFields(DateTimeZone.UTC).getMillis) // milli we don't want
        rd.withZone(DateTimeZone.UTC).getMillis
    }, {
      case x: Long => JInt(x)
    }))

    implicit val formats = DefaultFormats + StringToLong

    val event = parse(s).extract[Event]
    //println( event )

    // http://stackoverflow.com/questions/19002978/in-joda-time-how-to-convert-time-zone-without-changing-time
    // CI test fails: 1475148673247 was not equal to 1475145073247
    //                      ^^                             ^^
    // event.timestamp shouldBe 1475145073247L
    /*
        Joda DateTime treats any time in millis like "millis since 1970y in current time zone". So, when you create
        DateTime instance, it is created with current time zone.

        Use a local date time (no zone) and then convert to a given time zone
        val ldt = LocalDateTime.parse(s, DateTimeFormat.forPattern( datePattern )).toDateTime( DateTimeZone.UTC )
    */

    // We use this instead - note that we have only millisecond precision!
    val t = new DateTime(event.timestamp) withZone DateTimeZone.UTC
    t.toString shouldBe "2016-09-29T11:31:13.247Z"
    event.timestamp shouldBe 1475148673247L
  }

  case class Event1(domain: String, filePath: String, timestamp: java.time.LocalDateTime)

  /**
    * From: https://gist.github.com/djamelz/f963cab45933d05b2e10d8680e4213b6
    */
  it should "parse dates with microseconds to JDK's java.time.LocalDateTime" in {
    import org.json4s._
    import org.json4s.native.JsonMethods._

    //val datePattern = "yyyy-MM-dd HH:mm:ss.SSSSSS"
    val s =
      """{"timestamp": "2016-09-29 11:31:13.247772", "domain": "d1", "filePath": "s3://..."}"""

    implicit val formats = DefaultFormats + StringToJDKLocalDateTime

    val event = parse(s).extract[Event1]
    //println( event )
    val r = java.time.LocalDateTime.of(2016, 9, 29, 11, 31, 13, 247772 * 1000)
    event.timestamp shouldBe r
  }

  case class Event2(domain: String, filePath: String, timestamp: java.time.Instant)

  it should "parse dates with microseconds to JDK's Instant" in {
    import org.json4s._
    import org.json4s.native.JsonMethods._

    //val datePattern = "yyyy-MM-dd HH:mm:ss.SSSSSS"
    val s =
      """{"timestamp": "2016-09-29 11:31:13.247772", "domain": "d1", "filePath": "s3://..."}"""

    implicit val formats = DefaultFormats + StringToJDKInstant

    val event = parse(s).extract[Event2]
    //println( event )
    val r = java.time.LocalDateTime.of(2016, 9, 29, 11, 31, 13, 247772 * 1000)
    val off = java.time.ZoneId.of("UTC")
    // parsedDate.atStartOfDay(off).toInstant() // for java.time.Local
    val r1 = r.atZone(off).toInstant
    event.timestamp shouldBe r1
  }

  /*
  // https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
  // http://stackoverflow.com/questions/37672012/how-to-create-java-time-instant-from-pattern
  // http://stackoverflow.com/questions/33477695/why-does-the-new-java-8-date-time-api-not-have-nanosecond-precision
  // http://stackoverflow.com/questions/27454025/unable-to-obtain-localdatetime-from-temporalaccessor-when-parsing-localdatetime
  */

  // Cannot be defined in method body - JSON4S barfs complaining about this. 
  case class OneDate(t1: java.time.Instant)

  "Parsing the JSON window file " should "parse the date with microsecond precision" in {

    val json0 = parse(
      """{
            "t1" : "2014-04-10 16:15:00.000001"
            }
      """)

    implicit val formats = DefaultFormats + StringToJDKInstant
    val w0 = json0.extract[OneDate]
    //println( json0 )
    //println( w0 )
    json0.toString shouldBe "JObject(List((t1,JString(2014-04-10 16:15:00.000001))))"

    val r = java.time.LocalDateTime.of(2014, 4, 10, 16, 15, 0, 1 * 1000)
    val off = java.time.ZoneId.of("UTC")
    // parsedDate.atStartOfDay(off).toInstant() // for java.time.Local
    val r1 = r.atZone(off).toInstant
    w0.t1.compareTo(r1) shouldBe 0
  }

  it should "parse the date format with millisecond precision" in {

    val json0 = parse(
      """{
            "t1" : "2014-04-10 16:15:00.001000"
            }
      """)
    implicit val formats = DefaultFormats + StringToJDKInstant
    val w0 = json0.extract[OneDate]
    //println( json0 )
    //println( w0 )
    json0.toString shouldBe "JObject(List((t1,JString(2014-04-10 16:15:00.001000))))"

    val r = java.time.LocalDateTime.of(2014, 4, 10, 16, 15, 0, 1 * 1000000)
    val off = java.time.ZoneId.of("UTC")
    // parsedDate.atStartOfDay(off).toInstant() // for java.time.Local
    val r1 = r.atZone(off).toInstant
    w0.t1.compareTo(r1) shouldBe 0
  }

  it should "parse a list of dates correctly" in {
    val json1 = parse(
      """
        [
            "2014-04-10 16:15:00.000002",
            "2014-04-12 01:45:00.000003"
        ]
      """)
    implicit val formats = DefaultFormats + StringToJDKInstant
    //println( json1 )
    val w1 = json1.extract[List[java.time.Instant]]
    //println( w1.mkString( "," ) )
    w1.size shouldBe 2
    val r1 = java.time.LocalDateTime.of(2014, 4, 10, 16, 15, 0, 2 * 1000)
    val r2 = java.time.LocalDateTime.of(2014, 4, 12, 1, 45, 0, 3 * 1000)
    val off = java.time.ZoneId.of("UTC")
    // parsedDate.atStartOfDay(off).toInstant() // for java.time.Local
    val ri1 = r1.atZone(off).toInstant
    val ri2 = r2.atZone(off).toInstant
    //println(ri1)
    //println(ri2)
    w1.head.compareTo(ri1) shouldBe 0
    w1(1).compareTo(ri2) shouldBe 0
  }

  it should "parse a list of list of dates correctly" in {
    val json2 = parse(
      """[
        [
            "2014-02-19 10:50:00.000000",
            "2014-02-20 03:30:00.000000"
        ],
        [
            "2014-02-23 11:45:00.000000",
            "2014-02-24 04:25:00.000000"
        ]
      ]
      """)
    //println( json2 )
    implicit val formats = DefaultFormats + StringToJDKInstant
    val w2 = json2.extract[List[List[java.time.Instant]]]
    //println( w2.mkString( ";" ) )
    w2.size shouldBe 2

    w2.head.size shouldBe 2
    val d1 = makeInstance(year = 2014, month = 2, day = 19, hrs = 10, min = 50)
    val d2 = makeInstance(year = 2014, month = 2, day = 20, hrs = 3, min = 30)
    w2.head.head.compareTo(d1) shouldBe 0
    w2.head(1).compareTo(d2) shouldBe 0

    w2(1).size shouldBe 2
    val d3 = makeInstance(year = 2014, month = 2, day = 23, hrs = 11, min = 45)
    val d4 = makeInstance(year = 2014, month = 2, day = 24, hrs = 4, min = 25)
    w2(1).head.compareTo(d3) shouldBe 0
    w2(1)(1).compareTo(d4) shouldBe 0
  }

  it should "map the data-sets windows to list of lists of dates correctly" in {
    val json3 = parse(
      """
            {
                "artificialNoAnomaly/art_daily_no_noise.csv": [],
                "artificialNoAnomaly/art_daily_perfect_square_wave.csv": [],
                "artificialNoAnomaly/art_daily_small_noise.csv": [],
                "artificialNoAnomaly/art_flatline.csv": [],
                "artificialNoAnomaly/art_noisy.csv": [],
                "artificialWithAnomaly/art_daily_flatmiddle.csv": [
                    [
                        "2014-04-10 07:15:00.000000",
                        "2014-04-11 16:45:00.000000"
                    ]
                ]
            }
            """)
    //println( json3 )
    implicit val formats = DefaultFormats + StringToJDKInstant
    val w3 = json3.extract[Map[String, List[List[java.time.Instant]]]]
    //println( w3.mkString( ";\n" ) )

    val d1 = w3("artificialNoAnomaly/art_daily_no_noise.csv")
    d1.size shouldBe 0
    val d2 = w3("artificialNoAnomaly/art_daily_perfect_square_wave.csv")
    d2.size shouldBe 0
    val d3 = w3("artificialNoAnomaly/art_daily_small_noise.csv")
    d3.size shouldBe 0
    val d4 = w3("artificialNoAnomaly/art_flatline.csv")
    d4.size shouldBe 0
    val d5 = w3("artificialNoAnomaly/art_noisy.csv")
    d5.size shouldBe 0
    val d6 = w3("artificialWithAnomaly/art_daily_flatmiddle.csv")
    d6.size shouldBe 1
    d6.head.size shouldBe 2
    val dt1 = makeInstance(year = 2014, month = 4, day = 10, hrs = 7, min = 15)
    val dt2 = makeInstance(year = 2014, month = 4, day = 11, hrs = 16, min = 45)
    d6.head.head.compareTo(dt1) shouldBe 0
    d6.head(1).compareTo(dt2) shouldBe 0
  }

  /*
  import java.time.LocalDateTime
  import java.time.format.DateTimeFormatter
  import java.time.ZoneId
  import java.time.ZonedDateTime
  import java.time.ZoneOffset
  import java.time.Instant
  import java.time.Clock
  */

  "Labelling the JSON Window file" should "generate the labels for a time-stamp list for a given data-set windows (exclusive)" in {
    val json3 = parse(
      """
            {
                "artificialNoAnomaly/art_daily_no_noise.csv": [],
                "artificialNoAnomaly/art_daily_perfect_square_wave.csv": [],
                "artificialNoAnomaly/art_daily_small_noise.csv": [],
                "artificialNoAnomaly/art_flatline.csv": [],
                "artificialNoAnomaly/art_noisy.csv": [],
                "artificialWithAnomaly/art_daily_flatmiddle.csv": [
                    [
                        "2014-04-10 07:15:00.000010",
                        "2014-04-10 07:15:00.000020",
                    ]
                ]
            }
            """)
    //println( json3 )
    implicit val formats = DefaultFormats + StringToJDKInstant
    val w3 = json3.extract[Map[String, List[List[java.time.Instant]]]]
    //println( w3.mkString( ";\n" ) )

    val w4 = windowToIntervals(w3)
    //println( w4 )

    val delta = 5 * 1000 // 5 us 
    val start = makeInstance(2014, 4, 10, 7, 15)
    val dts = 0 to (10 * delta) by delta map { n => start.plusNanos(n) }
    val dates = dts.toList
    //println( dates.mkString(", ++.. \n") )

    val labels = labelInstanceExclusive(dates, w4("artificialWithAnomaly/art_daily_flatmiddle.csv"))
    //println( labels.mkString(",\n") )

    labels should have size 11
    labels should be(List(0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0))
  }

  it should "generate the labels for a time-stamp list for a given data-set windows (inclusive)" in {
    val json3 = parse(
      """
            {
                "artificialNoAnomaly/art_daily_no_noise.csv": [],
                "artificialNoAnomaly/art_daily_perfect_square_wave.csv": [],
                "artificialNoAnomaly/art_daily_small_noise.csv": [],
                "artificialNoAnomaly/art_flatline.csv": [],
                "artificialNoAnomaly/art_noisy.csv": [],
                "artificialWithAnomaly/art_daily_flatmiddle.csv": [
                    [
                        "2014-04-10 07:15:00.000010",
                        "2014-04-10 07:15:00.000020",
                    ]
                ]
            }
            """)
    //println( json3 )
    implicit val formats = DefaultFormats + StringToJDKInstant
    val w3 = json3.extract[Map[String, List[List[java.time.Instant]]]]
    //println( w3.mkString( ";\n" ) )

    val w4 = windowToIntervals(w3)
    //println( w4 )

    val delta = 5 * 1000 // 5 us 
    val start = makeInstance(2014, 4, 10, 7, 15)
    val dts = 0 to (10 * delta) by delta map { n => start.plusNanos(n) }
    val dates = dts.toList
    //println( dates.mkString(", ++.. \n") )

    val labels = labelInstanceInclusive(dates, w4("artificialWithAnomaly/art_daily_flatmiddle.csv"))
    //println( labels.mkString(",\n") )

    labels should have size 11
    labels should be(List(0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0))
  }

  it should "generate the labels for a time-stamp list for multiple windows (inclusive)" in {
    val json3 = parse(
      """
            {
                "artificialNoAnomaly/art_daily_no_noise.csv": [],
                "artificialNoAnomaly/art_daily_perfect_square_wave.csv": [],
                "artificialNoAnomaly/art_daily_small_noise.csv": [],
                "artificialNoAnomaly/art_flatline.csv": [],
                "artificialNoAnomaly/art_noisy.csv": [],
                "artificialWithAnomaly/art_daily_flatmiddle.csv": [
                    [
                        "2014-04-10 07:15:00.000010",
                        "2014-04-10 07:15:00.000020",
                    ]
                    [
                        "2014-04-10 07:15:00.000030",
                        "2014-04-10 07:15:00.000040",
                    ]
                ]
            }
            """)
    //println( json3 )
    implicit val formats = DefaultFormats + StringToJDKInstant
    val w3 = json3.extract[Map[String, List[List[java.time.Instant]]]]
    //println( w3.mkString( ";\n" ) )

    val w4 = windowToIntervals(w3)
    //println( w4 )

    val delta = 5 * 1000 // 5 us 
    val start = makeInstance(2014, 4, 10, 7, 15)
    val dts = 0 to (10 * delta) by delta map { n => start.plusNanos(n) }
    val dates = dts.toList
    //println( dates.mkString(", ++.. \n") )

    val labels = labelInstanceInclusive(dates, w4("artificialWithAnomaly/art_daily_flatmiddle.csv"))
    //println( labels.mkString(",\n") )

    labels should have size 11
    labels should be(List(0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0))
  }


  "Labelling the DataFrame" should "load and parse the NAB data file correctly" in {
    //import better.files._
    import better.files.Dsl._

    // Import the NAB data file parser implicitly
    import pt.inescn.utils.NABUtils._

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    //import kantan.csv._
    //import kantan.csv.ops._
    import kantan.csv.generic._

    // Data file to process
    val fileName = "art_increase_spike_density.csv"
    //println( pwd )
    val data = cwd / ".." / "data/nab/data/artificialWithAnomaly" / fileName // cwd = pwd
    val r = loadData(data)
    //println(r)
    r.isRight should be(true)

    val d0 = r.getOrElse(NABUtils.NABFrame(List[java.time.Instant](), List[Double]()))
    d0.dt.head should be(parseInstantUTC("2014-04-01 00:00:00"))
    d0.value.head should be(20.0)

    d0.dt(1) should be(parseInstantUTC("2014-04-01 00:05:00"))
    d0.value(1) should be(0.0)

    d0.dt(108) should be(parseInstantUTC("2014-04-01 09:00:00"))
    d0.value(108) should be(0.0)

    d0.dt(4031) should be(parseInstantUTC("2014-04-14 23:55:00"))
    d0.value(4031) should be(0.0)
  }

  it should "should be able to load the result file (for testing)" in {
    //import better.files._
    import better.files.Dsl._

    import NABUtils._
    import NABUtils.NABDataRow._

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    //import kantan.csv._
    //import kantan.csv.ops._
    import kantan.csv.generic._

    val dataDirectory = "data/nab/results/numentaTM/artificialWithAnomaly"
    val dataFileName = "art_increase_spike_density.csv"
    val algorithm_name = "numentaTM"

    val labelledp = cwd / ".." / dataDirectory / (algorithm_name + "_" + dataFileName)
    //val expected = loadResults( labelledp )
    val expected: Either[List[Throwable], NABResultAll] = loadResults(labelledp)
    //println(expected)
    expected.isRight should be(true)
    val d0 = expected.getOrElse(emptyNABResultAll)
    val line0 = 0
    //println( d0.dt( line0) )   
    d0.dt(line0) should be(parseInstantUTC("2014-04-01 00:00:00"))
    //d0.value( line0 ) should be (20.0 +- 0.0)
    d0.value(line0) should be(20.0)
    d0.anomaly_score(line0) should be(0.0301029996659)
    d0.raw_score(line0) should be(1.0)
    d0.label(line0) should be(0)
    d0.reward_low_FP_rate(line0) should be(0.0)
    d0.reward_low_FN_rate(line0) should be(0.0)

    val line1 = 1422
    d0.dt(line1) should be(parseInstantUTC("2014-04-05 22:30:00"))
    d0.value(line1) should be(0.0)
    d0.anomaly_score(line1) should be(0.0034982625592)
    d0.raw_score(line1) should be(0.0250000003725)
    d0.label(line1) should be(0)
    d0.reward_low_FP_rate(line1) should be(0.0)
    d0.reward_low_FN_rate(line1) should be(0.0)

    val line2 = 4031
    d0.dt(line2) should be(parseInstantUTC("2014-04-14 23:55:00"))
    d0.value(line2) should be(0.0)
    d0.anomaly_score(line2) should be(0.0115668894529)
    d0.raw_score(line2) should be(0.0)
    d0.label(line2) should be(0)
    d0.reward_low_FP_rate(line2) should be(0.0)
    d0.reward_low_FN_rate(line2) should be(0.0)

  }

  it should "should be able to load the failure labels (windows)" in {
    //import better.files._
    import better.files.Dsl._

    import NABUtils._

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    //import kantan.csv._
    //import kantan.csv.ops._
    import kantan.csv.generic._

    val labelsDirectory = "data/nab/labels"
    val labelsFileName = "combined_windows.json"
    //val algorithm_name = "numentaTM"

    val labels = cwd / ".." / labelsDirectory / labelsFileName
    val wins = loadJSONLabels(labels)
    //println(wins.size)
    wins.size should be(58)

    val file1 = "artificialNoAnomaly/art_daily_no_noise.csv"
    wins.contains(file1) should be(true)
    val empty1 = wins(file1)
    empty1.length should be(0)

    val file2 = "artificialNoAnomaly/art_daily_perfect_square_wave.csv"
    wins.contains(file2) should be(true)
    val empty2 = wins(file2)
    empty2.length should be(0)

    val file3 = "artificialNoAnomaly/art_daily_small_noise.csv"
    wins.contains(file3) should be(true)
    val empty3 = wins(file3)
    empty3.length should be(0)

    val file4 = "artificialNoAnomaly/art_flatline.csv"
    wins.contains(file4) should be(true)
    val empty4 = wins(file4)
    empty4.length should be(0)

    val file5 = "artificialNoAnomaly/art_noisy.csv"
    wins.contains(file5) should be(true)
    val empty5 = wins(file5)
    empty5.length should be(0)

    val file6 = "artificialWithAnomaly/art_daily_flatmiddle.csv"
    wins.contains(file6) should be(true)
    val wins6 = wins(file6)
    wins6.length should be(1)
    val i6 = makeInterval(parseInstantUTC("2014-04-10 07:15:00.000000"), parseInstantUTC("2014-04-11 16:45:00.000000"))
    wins6.head should be(i6.get)

    val file7 = "realAWSCloudwatch/ec2_network_in_5abac7.csv"
    wins.contains(file7) should be(true)
    val wins7 = wins(file7)
    wins7.length should be(2)
    val i71 = makeInterval(parseInstantUTC("2014-03-10 09:06:00.000000"), parseInstantUTC("2014-03-11 04:46:00.000000"))
    wins7.head should be(i71.get)
    val i72 = makeInterval(parseInstantUTC("2014-03-12 11:11:00.000000"), parseInstantUTC("2014-03-13 06:51:00.000000"))
    wins7(1) should be(i72.get)

    val file8 = "realTweets/Twitter_volume_UPS.csv"
    wins.contains(file8) should be(true)
    val wins8 = wins(file8)
    wins8.length should be(5)
    val i81 = makeInterval(parseInstantUTC("2015-03-02 11:17:53.000000"), parseInstantUTC("2015-03-03 13:37:53.000000"))
    wins8.head should be(i81.get)
    val i85 = makeInterval(parseInstantUTC("2015-03-29 03:17:53.000000"), parseInstantUTC("2015-03-30 05:37:53.000000"))
    wins8(4) should be(i85.get)

  }

  it should "generate the labels for a time-stamp list for a given data-set windows (inclusive)" in {
    //import better.files._
    import better.files.Dsl._

    import NABUtils._
    import NABUtils.NABDataRow._

    // Files to process
    val datasetId = "artificialWithAnomaly"
    val dataFileName = "art_increase_spike_density.csv"
    val dataset = datasetId + "/" + dataFileName
    val labelsFileName = "combined_windows.json"

    val labelsp = cwd / ".." / "data/nab/labels" / labelsFileName
    val labels = loadJSONLabels(labelsp)
    labels.isEmpty should be(false)

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    //import kantan.csv._
    //import kantan.csv.ops._
    import kantan.csv.generic._

    // This is the original data
    val datap = cwd / ".." / "data/nab/data" / datasetId / dataFileName
    val data = loadData(datap)
    data.isRight should be(true)

    // This is the result data that has already been labeled and scored
    val labelledp = cwd / ".." / "data/nab/results/numentaTM" / datasetId / ("numentaTM_" + dataFileName)
    val expected: Either[List[Throwable], NABResultAll] = loadResults(labelledp)
    expected.isRight should be(true)

    // We now label the original data
    val wins = labels.getOrElse(dataset, List[org.threeten.extra.Interval]())
    val new_labels = addLabels(labelInstanceInclusive)(data.getOrElse(emptyNABFrame), wins)
    // And check that it is the same as the result data labels
    val old_labels = expected.getOrElse(emptyNABResultAll)
    new_labels.dt should contain theSameElementsInOrderAs old_labels.dt
    new_labels.dt should be(sorted)
    new_labels.label should contain theSameElementsInOrderAs old_labels.label
  }

  "The perfect detector" should "be able to record the expected scores from the NAB result data" in {

    val ts1 = parseInstantUTC("2014-04-05 22:30:00")
    val bytes1 = toBytes(ts1)
    val hex1 = Hex.valueOf(bytes1)
    bytes1.length shouldBe 20
    hex1.length shouldBe (2 * bytes1.length)
    hex1 shouldBe "323031342D30342D30355432323A33303A30305A"

    import java.security.MessageDigest
    val digest1 = MessageDigest.getInstance("SHA-256")
    updateHash(digest1, ts1)
    val hash1 = digest1.digest
    val hexh1 = Hex.valueOf(hash1)
    // 256 bits -> / 8 bytes -> * 2 Hex digits (4 bits)
    hexh1.length shouldBe ((256 / 8) * 2)

    val bytes2 = toBytes(20.00001)
    val hex2 = Hex.valueOf(bytes2)
    bytes2.length shouldBe 8
    hex2.length shouldBe (2 * bytes2.length)

    val digest2 = MessageDigest.getInstance("SHA-256")
    updateHash(digest2, 20.00001)
    val hash2 = digest2.digest
    val hexh2 = Hex.valueOf(hash2)
    // 256 bits -> / 8 bytes -> * 2 Hex digits (4 bits)
    hexh2.length shouldBe ((256 / 8) * 2)


    val digest3 = MessageDigest.getInstance("SHA-256")
    updateHash(digest3, 20.000000000000001)
    val hash3 = digest3.digest
    val hexh3 = Hex.valueOf(hash3)
    // 256 bits -> / 8 bytes -> * 2 Hex digits (4 bits)
    val hashlen_256 = (256 / 8) * 2
    hexh3.length shouldBe hashlen_256
    hexh2 should not equal hexh3

    //import better.files._
    //import better.files._
    import better.files.Dsl._
    import NABUtils._
    //import NABUtils.NABDataRow._

    // Files to process
    val datasetId = "artificialWithAnomaly"
    val dataFileName1 = "art_increase_spike_density.csv"
    val dataFileName2 = "art_daily_jumpsdown.csv"

    // This is the result data that has already been labeled and scored
    val labelledp1 = cwd / ".." / "data/nab/results/numentaTM" / datasetId / ("numentaTM_" + dataFileName1)
    val labelledp2 = cwd / ".." / "data/nab/results/numentaTM" / datasetId / ("numentaTM_" + dataFileName2)

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    //import kantan.csv._
    //import kantan.csv.ops._
    import kantan.csv.generic._

    import scala.collection.mutable.ArrayBuffer
    //import java.security.MessageDigest
    val digest = MessageDigest.getInstance("SHA-256")
    val hashf1e: Either[List[Throwable], ArrayBuffer[Byte]] = tagData(labelledp1, sample_size = 0.15, digest)
    hashf1e.isRight should be(true)
    val hashf1 = hashf1e.right.get
    val hexf1 = Hex.valueOf(hashf1)
    hexf1.length shouldBe hashlen_256

    // Restart or get a new one
    digest.reset()

    val hashf2e: Either[List[Throwable], ArrayBuffer[Byte]] = tagData(labelledp2, sample_size = 0.15, digest)
    hashf2e.isRight should be(true)
    val hashf2 = hashf2e.right.get
    val hexf2 = Hex.valueOf(hashf2)
    hexf2.length shouldBe hashlen_256
  }

  it should "deal with the NAB result files with non raw scores" in {
    //import better.files._
    import better.files.Dsl._

    import NABUtils._

    // Files to process
    val datasetId = "realAWSCloudwatch"
    val dataFileName1 = "iio_us-east-1_i-a2eb1cd9_NetworkIn.csv"

    // This is the result data that has already been labeled and scored
    val labelledp1 = cwd / ".." / "data/nab/results/windowedGaussian" / datasetId / ("windowedGaussian_" + dataFileName1)

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    //import kantan.csv._
    //import kantan.csv.ops._
    import kantan.csv.generic._

    import java.security.MessageDigest
    val digest = MessageDigest.getInstance("SHA-256")

    // 256 bits -> / 8 bytes -> * 2 Hex digits (4 bits)
    val hashlen_256 = (256 / 8) * 2

    import scala.collection.mutable.ArrayBuffer
    val hashf1e: Either[List[Throwable], ArrayBuffer[Byte]] = tagData(labelledp1, sample_size = 0.15, digest)
    hashf1e.isRight should be(true)
    val hashf1 = hashf1e.right.get
    val hexf1 = Hex.valueOf(hashf1)
    hexf1.length shouldBe hashlen_256
  }

  it should "be able to generate the hashes for NAB result files based on theire time-stamp and values" in {

    import better.files._
    import better.files.Dsl._

    import NABUtils._

    val data = cwd / ".." / "data/nab/results"
    val dataFiles = allDataFiles(data)
    dataFiles shouldBe 'defined
    //println( dataFiles.mkString(",") )

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    import kantan.csv.generic._

    import java.security.MessageDigest
    val digest = MessageDigest.getInstance("SHA-256")

    import pt.inescn.utils.TestUtils.time
    // Generate the hashes to find the labels
    val fileHash = dataFiles.map { x =>
      time(tagFiles(x, sample_size = 0.1, digest))
    }
    fileHash shouldBe 'defined
    fileHash.get.isRight should be(true)
    fileHash.get.right.get.size should be > 1

    // Are ArrayBuffer good for keys
    import scala.collection.mutable.ArrayBuffer
    val a1 = ArrayBuffer(-62, 103, -57, 6, 121, 31, 15, 1, 8, 57, -111, 116, 31, 70, -105, -123, -54, -110, -61, 28, 114, -118, 50, -38, 92, -36, 86, -79, 51, 30, 44, -7)
    val a2 = ArrayBuffer(-62, 103, -57, 6, 121, 31, 15, 1, 8, 57, -111, 116, 31, 70, -105, -123, -54, -110, -61, 28, 114, -118, 50, -38, 92, -36, 86, -79, 51, 30, 44, -7)
    a1 shouldBe a2

    // NOTE: IMPORTANT we list and process all result files because they are the only ones with the labels.
    // However, we only use the time-stamp and values to generate the hash keys. This means that because the
    // same data is used by various algorithms, we will generate the same hash several time over. So the 
    // Map will not contain all the result files (last files overwrite the initial ones). 

    // So pick a file at random
    val dataMap = fileHash.get.right.get
    val y = dataMap.toList
    val tesFile = scala.util.Random.shuffle(y).take(1).head
    val labelledp1 = File(tesFile._2)
    val w = tesFile._1

    // Now calculate the hash for that file and see if we can find it
    // Make sure we restart the hash generator
    digest.reset()

    // 256 bits -> / 8 bytes -> * 2 Hex digits (4 bits)
    val hashlen_256 = (256 / 8) * 2

    val hashf1e: Either[List[Throwable], ArrayBuffer[Byte]] = tagData(labelledp1, sample_size = 0.1, digest)
    hashf1e.isRight should be(true)
    val hashf1 = hashf1e.right.get
    val hexf1 = Hex.valueOf(hashf1)
    hexf1.length shouldBe hashlen_256

    // Same hash?
    Hex.valueOf(w) shouldBe hexf1
    w should be(hashf1)

    // In the Map?
    dataMap.get(hashf1) shouldBe 'defined
    dataMap(hashf1) shouldBe labelledp1.toString
  }

  it should "add the detection correctly" in {
    //import better.files._
    import better.files.Dsl._
    import NABUtils._

    // Get only the labelled data for a single algorithm/detector
    val dataDir = cwd / ".." / "data/nab/results/null"
    val dataFiles = allDataFiles(dataDir)
    dataFiles shouldBe 'defined

    import pt.inescn.utils.TestUtils.time

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    import kantan.csv.generic._

    // Generate the hashes to find the labels
    import java.security.MessageDigest
    val digest = MessageDigest.getInstance("SHA-256")
    val fileHash = dataFiles.map { x =>
      time(tagFiles(x, sample_size = 0.1, digest))
    }
    fileHash shouldBe 'defined
    fileHash.get.isRight should be(true)
    fileHash.get.right.get.size should be(58)
    //println(fileHash.get.right.get.size)

    val map = fileHash.get.right.get

    import pt.inescn.detector.perfect.PerfectDetector
    // "realAdExchange/exchange-4_cpm_results.csv" 4
    // "realTweets/Twitter_volume_UPS.csv" 5
    val dataFile = dataDir / "realTweets/null_Twitter_volume_UPS.csv"

    /*
    val safeDigest0 = digest.clone().asInstanceOf[MessageDigest]
    safeDigest0.reset()
    val hashf1e: Either[List[Throwable], ArrayBuffer[Byte]] = tagData(dataFile, sample_size = 0.1, safeDigest0)
    hashf1e.isRight should be(true)
    val hashf1 = hashf1e.right.get*/

    // Set up the algorithm
    val safeDigest = digest.clone().asInstanceOf[MessageDigest]
    // Important, reset
    safeDigest.reset()
    val perfect = new PerfectDetector(safeDigest, map)

    // Check if we can oad the file
    val data = perfect.loadFile(dataFile, 0)
    //print(data)
    data.dt.length should be > 0
    data.label.length should be > 0

    // Assume we used 10% to hash the file, can we find the hash in the map?
    // Note tha the data has a header, so subtract 1
    val numRecords = dataFile.lines.size - 1
    val numHashRecords = (0.1 * numRecords).toInt
    val d = data.dt zip data.value
    val d0 = perfect.scanForData(d)
    d0 shouldBe a[HashFound] // or an[]
    d0.gfile shouldBe Some(dataFile)
    d0.gread shouldBe numHashRecords

    // Assume we used 10% to hash the file, can we load the data via the map's file name ?
    // Important, reset
    safeDigest.reset()
    val (s1, d1) = perfect.identifyData(d)
    d1 shouldBe a[UseData] // or an[]
    s1.length shouldBe numRecords

    // We have a part of the data used for hash generation and not labelled
    // We have another part of the data not used for hash generation and can be labelled
    // Because we read all the data, we have no more to consume
    // Did we label it correctly?
    // Lets compare the data labels to the "predicted" scores
    val tmp = data.label.drop(numHashRecords) zip s1.drop(numHashRecords)
    // Converts labels to score
    val tmp1 = tmp.map { case (a, b) => a.toDouble == b }
    val tmp2 = tmp1.map({ x => if (x == false) 1 else 0 }).sum
    tmp2 shouldBe 0

  }

  it should "add the correct detection incrementally" in {
    //import better.files._
    import better.files.Dsl._
    import NABUtils._


    val dataDir = cwd / ".." / "data/nab/results/null"
    val dataFiles = allDataFiles(dataDir)
    dataFiles shouldBe 'defined

    // We need to bring in shapeless "compile time reflection"
    // https://nrinaudo.github.io/kantan.csv/tut/shapeless.html
    import kantan.csv.generic._

    // Generate the hashes to find the labels
    import java.security.MessageDigest
    val digest = MessageDigest.getInstance("SHA-256")
    val fileHash = dataFiles.map { x => tagFiles(x, sample_size = 0.1, digest) }
    fileHash shouldBe 'defined
    fileHash.get.isRight should be(true)
    fileHash.get.right.get.size should be(58)
    //println(fileHash.get.right.get.size)

    val map = fileHash.get.right.get

    import pt.inescn.detector.perfect.PerfectDetector
    // "realAdExchange/exchange-4_cpm_results.csv" 4
    // "realTweets/Twitter_volume_UPS.csv" 5
    val dataFile = dataDir / "realTweets/null_Twitter_volume_UPS.csv"

    // Assume we used 10% to hash the file, can we find the hash in the map?
    // Note tha the data has a header, so subtract 1
    val numRecords = dataFile.lines.size - 1
    val numHashRecords = (0.1 * numRecords).toInt
    val ld = loadResults(dataFile)
    ld.isRight should be(true)
    val ds = ld.right.get
    val d = ds.dt zip ds.value

    // Set up the algorithm
    val safeDigest = digest.clone().asInstanceOf[MessageDigest]
    // Important, reset
    safeDigest.reset()
    val perfect = new PerfectDetector(safeDigest, map)

    // No hash found yet
    val d0 = d.take(numHashRecords / 2)
    val (r1, a1) = perfect.detect(d0)
    a1.state shouldBe a[pt.inescn.detector.perfect.NoHash] // or an[]
    a1.state.gread shouldBe d0.length
    r1.forall(_ == 0.0) shouldBe true
    r1.length shouldBe d0.length
    a1.fileIsLoaded shouldBe false

    // Found the hash and labelled some data
    //val d1 = d.drop(numHashRecords / 2).take(numHashRecords + 10 )
    val d1 = d.slice(numHashRecords / 2, (numHashRecords / 2) + numHashRecords + 10)
    //val dt1 = ds.label.drop(numHashRecords / 2).take(numHashRecords + 10 )
    //val dt1 = ds.label.slice(numHashRecords / 2, (numHashRecords / 2) + numHashRecords + 10)
    val (r2, a2) = a1.detect(d1)
    r2.length shouldBe d1.length
    a2.state shouldBe a[pt.inescn.detector.perfect.UseData] // or an[]
    a2.state.gread shouldBe (d0.length + d1.length)

    // The first part prior to the hash is set to the default value
    r2.take(numHashRecords / 2).forall(_ == 0.0) shouldBe true

    // The second part holds the correct predictions
    // start predictions from the record were the hash was determined
    //val preds = ds.label.drop(numHashRecords).take((numHashRecords/2) + 10 )
    val preds = ds.label.slice(numHashRecords, numHashRecords + (numHashRecords / 2) + 10)
    // Take the second part that was correctly predicted
    //val tmpr2 = r2.drop(numHashRecords / 2).take(numHashRecords + 10 )
    val tmpr2 = r2.slice(numHashRecords / 2, (numHashRecords / 2) + numHashRecords + 10)
    val tmp = tmpr2 zip preds
    val tmp1 = tmp.map { case (a, b) => a.toDouble == b }
    val tmp2 = tmp1.map({ x => if (!x) 1 else 0 }).sum
    tmp2 shouldBe 0

    // Now predict for the rest of the data
    val d2 = d.drop((numHashRecords / 2) + numHashRecords + 10)
    //val dt2 = ds.label.drop((numHashRecords / 2) + numHashRecords + 10)
    val (r3, a3) = a2.detect(d2)
    r3.length shouldBe d2.length
    a3.state shouldBe a[pt.inescn.detector.perfect.UseData] // or an[]
    a3.state.gread shouldBe (d0.length + d1.length + d2.length)

  }

}