/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp


import breeze.linalg.{DenseMatrix, DenseVector, max, min}
import org.scalatest._
import java.lang.{System => JSystem}

import Fourier._
import better.files.File
import pt.inescn.samplers.{Base, Distribution, Function}
import org.hipparchus.random.JDKRandomGenerator
import org.hipparchus.transform.DftNormalization
import pt.inescn.etl.Transform


/**
  * Contains tests for the DSP related functions. Also used to test some basic DSP library routines.
  *
  * Created by hmf on 27-06-2017.
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.dsp.FourierSpec"
  *
  */
class FourierSpec extends FlatSpec with Matchers {

  val eps = 0.3

  "Utility functions for FFT" should "produce the correct solutions" in {

    val po2_1 = isPowerOf2(16)
    po2_1 shouldBe true
    val po2_2 = isPowerOf2(6)
    po2_2 shouldBe false
    val po2_3 = isPowerOf2( Math.pow(2,30).toInt )
    po2_3 shouldBe true
    val po2_4 = isPowerOf2( Math.pow(2,31).toInt ) // integer overflows at 31 bits, should be true
    po2_4 shouldBe false // (should be true)
    val po2_5 = isPowerOf2( Math.pow(2,31).toLong ) // integer overflows at 31 bits, should be true
    po2_5 shouldBe true
    val po2_6 = isPowerOf2( 55956 )
    po2_6 shouldBe false


    val tpo2_1 = floorPowerOf2(6)
    tpo2_1 shouldBe 3
    val tpo2_2 = floorPowerOf2(8)
    tpo2_2 shouldBe 3
    val tpo2_3 = floorPowerOf2(1025)
    tpo2_3 shouldBe 11
    val tpo2_4 = floorPowerOf2(Math.pow(2,31).toLong)
    tpo2_4 shouldBe 31

    val to2_1 = truncatePowerOf2(6)
    to2_1 shouldBe 4
    val to2_2 = truncatePowerOf2(8)
    to2_2 shouldBe 8
    val to2_3 = truncatePowerOf2(1025)
    to2_3 shouldBe 1024
    val to2_4 = truncatePowerOf2(Math.pow(2,31).toLong)
    to2_4 shouldBe 2147483648L
    val to2_5 = truncatePowerOf2(55956)
    to2_5 shouldBe 32768

    val to3_1 = cielPowerOf2(6)
    to3_1 shouldBe 8
    val to3_2 = cielPowerOf2(8)
    to3_2 shouldBe 8
    val to3_3 = cielPowerOf2(1025)
    to3_3 shouldBe 2048
    val to3_4 = cielPowerOf2(Math.pow(2,31).toLong)
    to3_4 shouldBe 2147483648L
    val to3_5 = cielPowerOf2(55956)
    to3_5 shouldBe 65536
    val to3_6 = cielPowerOf2(2048)
    to3_6 shouldBe 2048

    // samplesTo at 8Hz and samplesTo for 2.2 seconds
    val fs = 8
    val dur = 2.2
    val (_,xx) = Fourier.samplingSpecification(fs,dur)
    val len = truncatePowerOf2(xx.length)
    //println(s"xx.length = ${xx.length} ~> len= $len")
    xx.length shouldBe 17
    len shouldBe 16
  }

  /**
    * NOTE: we have used a specific class loader to circumvent the problem of SBT
    * altering the class loader used. For a standard run we need only fork the process
    * (see the build.sbt file). However, when testing, either forking is not working or
    * the SBT class loader is still use. To solve tis we use the system's class loader.
    *
    * @see https://stackoverflow.com/questions/31727385/sbt-scala-audiosystem
    * @see http://www.scala-sbt.org/0.13/docs/Testing.html
    */
  "FFT and DFT" should "produce the same results" in {
    import better.files.Dsl._

    // codec: Uncompressed 16-bit PCM audio
    // channels: Stereo
    // sampling rate: 44100 Hz
    // Bitrate : N/A

    // /home/hmf/git/adw/data/audio
    val audio = cwd / ".." / "data/audio"
    val sample5 = audio / "canary" / "Canary_singing_master_selec_01_sub_sel_2.wav"

    val cl = classOf[javax.sound.sampled.AudioSystem].getClassLoader
    val old = Thread.currentThread.getContextClassLoader

    try {
      Thread.currentThread.setContextClassLoader(cl)
      val audioData = Audio.open(sample5)
      audioData.isDefined shouldBe true

      val data = audioData.get._2
      val Some((channels, idx)) = Audio.load(sample5)
      channels.length shouldBe 2

      // No padding, to compare both transformed samples must have the same length
      val leftChannel1 = channels(0)
      val numSamples = 2500 // DFT takes a long time, so use a few samples only

      val time = Fourier.sampledTime(data.getFormat.getFrameRate, data.getFrameLength)
      time shouldBe 1.16575 +- eps // total = 24.38297

      val fft1 = Fourier.FFT(data.getFormat.getFrameRate, numSamples, leftChannel1) // no padding
      fft1._1 shouldBe 2048
      val dft1 = Fourier.DFT(data.getFormat.getFrameRate, fft1._1, leftChannel1) // can use all all samples, so no padding
      dft1._1 shouldBe fft1._1
      //fft1._3 should contain theSameElementsInOrderAs dft1._3.slice(0, fft1._1)
      fft1._3.zip( dft1._3.slice(0, fft1._1) ).foreach( c => c._1.getReal shouldBe c._2.getReal +- eps )
      fft1._3.zip( dft1._3.slice(0, fft1._1) ).foreach( c => c._1.getImaginary shouldBe c._2.getImaginary +- eps )

      // With padding, to compare both transformed samples must have the same length
      val leftChannel2 = channels(0).slice(0,1124)
      val numSamples2 = 2048

      val fft2 = Fourier.FFT(data.getFormat.getFrameRate, numSamples2, leftChannel2, pad = true) // with padding
      fft2._1 shouldBe 2048

      val paddedLeftChannel2 = Fourier.resizeLength(leftChannel2, numSamples2)
      paddedLeftChannel2.slice(0,1124) should contain theSameElementsInOrderAs leftChannel2
      paddedLeftChannel2.slice(1124,paddedLeftChannel2.length) should contain only 0.0

      val dft2 = Fourier.DFT(data.getFormat.getFrameRate, numSamples2, paddedLeftChannel2) // can use all all samples, so no padding
      dft2._1 shouldBe fft2._1
      //fft2._3 should contain theSameElementsInOrderAs dft2._3.slice(0, fft2._1)
      fft2._3.zip( dft2._3.slice(0, fft2._1) ).foreach( c => c._1.getReal shouldBe c._2.getReal +- eps )
      fft2._3.zip( dft2._3.slice(0, fft2._1) ).foreach( c => c._1.getImaginary shouldBe c._2.getImaginary +- eps )

    } finally
      Thread.currentThread.setContextClassLoader(old)

  }

  "FFT" should "produce the correct absolute values and arguments for a single oscillator (10*Nyquist)" in {
    val f1 = 10 /* Hz */ ; val a1 = 1   ;  val phase1 = Math.toRadians(0) // (-45)
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1)
    samplingFreq shouldBe (2.0*f1)
    // samplesTo at 10*Nyquist and samplesTo for 2.2 seconds
    val fs = samplingFreq * 10 // IMPORTANT: if you do it at Nyquist's frequency, plotting very unclear
    val dur = 2.2
    val (dd,xx) = Fourier.samplingSpecification(fs,dur)
    val sig1 = Function.Sin(f1, a1, phase1, dd)

    // Make sure it is fast - change in len changes scaling, sampling frequency the same
    val len = Fourier.truncatePowerOf2(xx.length)
    //println(s"xx.length = ${xx.length} ~> len= $len")
    xx.length shouldBe 440
    len shouldBe 256

    // https://sites.google.com/site/piotrwendykier/software/jtransforms
    // https://github.com/wendykierp/JTransforms
    // Breeze uses JTransforms, see breeze.signal.support.JTransformsSupport
    // see edu.emory.mathcs.jtransforms.fft.{DoubleFFT_1D, DoubleFFT_2D}
    // // https://mvnrepository.com/artifact/com.github.wendykierp/JTransforms
    //libraryDependencies += "com.github.wendykierp" % "JTransforms" % "3.1"
    // http://carsomyr.github.io/shared/
    import org.hipparchus.transform.FastFourierTransformer
    import org.hipparchus.transform.DftNormalization
    import org.hipparchus.transform.TransformType

    val fft = new FastFourierTransformer(DftNormalization.STANDARD)

    // Perform FFT and ge components
    val tsig1: Array[Double] = sig1(len).toArray
    val complexTsig1= fft.transform(tsig1, TransformType.FORWARD)
    val real_s1 = complexTsig1.map(_.getReal)
    val imaj_s1 = complexTsig1.map(_.getImaginary)
    val abs_s1 = complexTsig1.map(_.abs())
    val arg_s1 = complexTsig1.map(_.getArgument)

    // Check absolute value
    val rte1 = real_s1.zip(imaj_s1).map(p => Math.sqrt((p._1*p._1)+(p._2*p._2)) )
    rte1.zip(abs_s1).foreach( p => p._1 should be (p._2 +- eps) )

    // Check phase
    val angle1 = real_s1.zip(imaj_s1).map(p => Math.atan2(p._2, p._1) )
    angle1.zip(arg_s1).foreach( p => p._1 should be (p._2 +- eps) )

    //println(xx.take(10).mkString(","))
    val freq = fs / 2                          // Don't show all the frequencies
    val deltaFreq = (fs*1.0)/len               // Show them at the correct sampling rate
    val freqs = BigDecimal(0.0) to freq by deltaFreq map(_.toDouble)
    freqs.take(10) should contain theSameElementsInOrderAs List(0.0, 0.78125, 1.5625, 2.34375, 3.125, 3.90625, 4.6875, 5.46875, 6.25, 7.03125)
    //println(abs_s1.take(10).mkString(","))  // all zero except at fs
    val maxAbs1 = abs_s1.zipWithIndex.maxBy(_._1)._2
    abs_s1.foreach( _ should be <= abs_s1(maxAbs1) ) // all zero except at fs

    val xa : Array[Double] = freqs.toArray
    val ya : Array[Double] = abs_s1.take(freqs.length)
    xa.length shouldBe 129 // Includes DC component at 0Hz - sampling/2 + 1 DC component
    ya.length shouldBe 129 // Includes DC component at 0Hz

    val maxAbs = ya.zipWithIndex.maxBy(_._1)._2
    xa(maxAbs) should be (10.0 +- eps) // Hz
    //ya(maxAbs) should be (a1*(len-1.0) +- eps) // why not (A * N) ? Do we have to remove the DC component (TODO)
    deltaFreq shouldBe 0.78125

    val yb : Array[Double] = arg_s1.take(freqs.length).map(_ / Math.PI)
    // println(Math.toDegrees(yb(maxAbs))) // -39.918518077611765
    //Math.toDegrees(yb(maxAbs)) should be (-39.918518077611765 +- eps)// should be 0 but get this value because frequency multiples ?
    // We know theire is a 0 phase at abu 10Hz, so lets look for it
    val maxPhase = yb.zipWithIndex.maxBy(_._1)._2
    val minPhase = yb.zipWithIndex.minBy(_._1)._2
    // Get the frequency at the crossing of 0
    val maxFreq = xa(maxPhase)
    val minFreq = xa(minPhase)
    // Simple linear interpolation
    val approximateFreq = minFreq + ((maxFreq - minFreq)/2.0)
    // close enough
    approximateFreq should be (10.0 +- eps)

    // JPlot seems to be ok.
    /*
    import breeze.plot._
    val fig = Figure("FFT: Single Oscillator")
    val p1 = fig.subplot(0)
    p1 += plot(xx.take(len), tsig1)
    p1.title = s"Signal (t) @ $f1"
    p1.refresh

    val p2 = fig.subplot(2,1,1)
    p2 += plot(xa, ya)
    p2.title = "FFT (Abs)"
    p2.refresh

    // FFT phase is noisy (not usable)
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    val p3 = fig.subplot(3,1,2)
    p3 += plot(xa, yb)
    p3.title = "FFT (Theta)"
    p3.refresh*/
  }

  /**
    * This is an example of how sampling at the Nyquist frequency is not enough. If we samplesTo a pure sine
    * (or cosine wave) exactly at the zero crossing, then all samples will be zero. In this all FFT
    * coefficients will also be 0 and hence not useful. To "correct"  this one oly need to shift the samples
    * either by starting at a `t > 0` and `t` not at every (1/2T) or simply adding a non-zero phase.
    *
    * Note also that in these conditions ratios of near zero values may produce very large vales. The result
    * is a phase diagram that exhibits noise.
    *
    * See Sampling: What Nyquist Didn’t Say, and What to Do About It, by Tim Wescott, of Wescott Design Services
    * June 20, 2016, Figure 10, page 13
    *
    * @see http://www.wescottdesign.com/articles/Sampling/sampling.pdf
    */
  it should "produce the IN-correct absolute values and arguments for a single oscillator (Nyquist)" in {
    val f1 = 10 /* Hz */ ; val a1 = 1   ;  val phase1 = Math.toRadians(0) // (-45)
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1)
    samplingFreq shouldBe (2.0*f1)
    // samplesTo at 10*Nyquist and samplesTo for 2.2 seconds
    val fs = samplingFreq // IMPORTANT: if you do it at Nyquist's frequency, plotting very unclear
    val dur = 2.2
    val (dd,xx) = Fourier.samplingSpecification(fs,dur)
    val sig1 = Function.Sin(f1, a1, phase1, dd)

    // Make sure it is fast - change in len changes scaling, sampling frequency the same
    val len = Fourier.truncatePowerOf2(xx.length)
    //println(s"xx.length = ${xx.length} ~> len= $len")
    xx.length shouldBe 44
    len shouldBe 32

    // https://sites.google.com/site/piotrwendykier/software/jtransforms
    // https://github.com/wendykierp/JTransforms
    // Breeze uses JTransforms, see breeze.signal.support.JTransformsSupport
    // see edu.emory.mathcs.jtransforms.fft.{DoubleFFT_1D, DoubleFFT_2D}
    // // https://mvnrepository.com/artifact/com.github.wendykierp/JTransforms
    //libraryDependencies += "com.github.wendykierp" % "JTransforms" % "3.1"
    // http://carsomyr.github.io/shared/
    import org.hipparchus.transform.FastFourierTransformer
    import org.hipparchus.transform.DftNormalization
    import org.hipparchus.transform.TransformType

    val fft = new FastFourierTransformer(DftNormalization.STANDARD)

    // Perform FFT and ge components
    val tsig1: Array[Double] = sig1(len).toArray
    val complexTsig1= fft.transform(tsig1, TransformType.FORWARD)
    val real_s1 = complexTsig1.map(_.getReal)
    val imaj_s1 = complexTsig1.map(_.getImaginary)
    val abs_s1 = complexTsig1.map(_.abs())
    val arg_s1 = complexTsig1.map(_.getArgument)

    // Check absolute value
    val rte1 = real_s1.zip(imaj_s1).map(p => Math.sqrt((p._1*p._1)+(p._2*p._2)) )
    rte1.zip(abs_s1).foreach( p => p._1 should be (p._2 +- eps) )

    // Check phase
    val angle1 = real_s1.zip(imaj_s1).map(p => Math.atan2(p._2, p._1) )
    angle1.zip(arg_s1).foreach( p => p._1 should be (p._2 +- eps) )

    val freq = fs / 2                          // Don't show all the frequencies
    val deltaFreq = (fs*1.0)/len               // Show them at the correct sampling rate
    val freqs = BigDecimal(0.0) to freq by deltaFreq map(_.toDouble)
    freqs.take(10) should contain theSameElementsInOrderAs List(0.0, 0.625, 1.25, 1.875, 2.5, 3.125, 3.75, 4.375, 5.0, 5.625)

    val xa : Array[Double] = freqs.toArray
    val ya : Array[Double] = abs_s1.take(freqs.length)
    xa.length shouldBe 17 // Includes DC component at 0Hz - sampling/2 + 1 DC component
    ya.length shouldBe 17 // Includes DC component at 0Hz

    val maxAbs = ya.zipWithIndex.maxBy(_._1)._2
    xa(maxAbs) should be (10.0 +- eps) // Hz, we still find the component
    ya(maxAbs) should be (0.0 +- eps) // Hz, but its 0
    deltaFreq shouldBe 0.625
    ya.foreach( p => p should be (0.0 +- eps) )     // Oops, sampling wrong

    //val yb : Array[Double] = arg_s1.take(freqs.length).map(_ / Math.PI)
    //yb.foreach( p => p should be (0.0 +- eps) )     // Cannot test, random numbers
    // We know there is a 0 phase at abu 10Hz, so lets look for it

    // JPlot seems to be ok.
    /*
    import breeze.plot._
    val fig = Figure("FFT: Single Oscillator")
    val p1 = fig.subplot(0)
    p1 += plot(xx.take(len), tsig1)
    p1.title = s"Signal (t) @ $f1"
    p1.refresh

    val p2 = fig.subplot(2,1,1)
    p2 += plot(xa, ya)
    p2.title = "FFT (Abs)"
    p2.refresh

    // FFT phase is noisy (not usable)
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    val p3 = fig.subplot(3,1,2)
    p3 += plot(xa, yb)
    p3.title = "FFT (Theta)"
    p3.refresh*/
  }


  /**
    * A note on testing the amplitude and frequencies of the FFT as compared to the basic sine waves.
    * The FFT generates a pair of components for a single (infinite) oscillator (cosine or sine wave).
    * One component is in the positive frequency and its mirror is in the negative frequency domain
    * (because we are dealing with a real signal). When we compare the amplitude of the FFT component
    * we are measuring the energy. So we must take into account that the amplitude of both components
    * (in the positive and negative frequency domain) must be added. In addition to this, the FFT
    * parameter [[org.hipparchus.transform.DftNormalization.STANDARD]] means that the time domain
    * amplitude must be scaled by `1/N` were N is the number of samples used by the FFT.
    *
    * So to compare the amplitude `A` of the sine (or cosine wave) at the oscillating frequency to the
    * FFT component `F` we simply check that `A = (2*F)/N` or equivalently `A = F / (N/2)` (seeing
    * as N is a power of two, you need only shift left once).
    *
    * Note that this is only true is the oscillators were samples with infinite number of samples. Because
    * we only have a finite number of samples it is as if we applied (convolved) a square wave filter in the
    * time domain. This means that an equivalent `sinc` filter is applied (multiplied) in the frequency domain.
    * This causes the appearance of components a other frequencies that represent part of the original
    * signal's energy - so those components also have to be added to get back the precise original amplitude of
    * the oscillator. For large `N`, he difference is small.
    *
    * The FFT absolute values are proportional to the oscillator's amplitude. We must consider how to calculate
    * and use power (energy per uit of time), which provides the correct amplitude. This implies the use of the
    * RMS of the wave. Note that the amplitude of a wave's FFT component F is equal to A/sqrt(2), were A is the
    * average (RMS) value of the peak value of the sine wave [2]. So, for the sine wave with a peak of A'=1, the
    * RMS = 0.707. The expected amplitude for a single sided FFT is therefore:
    *   A/sqrt(2) = RMS/sqrt(2) = 0.707/sqrt(2) = 0,499924494
    * Hence, for a 2 sided FFT:
    *   2*A/sqrt(2) = sqrt(2) * RMS = 0,999848989
    * Which is not quite the amplitude A=1.
    * As a result we have that:
    *   2 * RMS(A) = 2 * F / N
    *       RMS(A) = F / N
    *
    * Note: for A=1, the value F = 0,999848989, so for the next tests we use A instead of RMS(A) which is very close.
    *
    * @see https://dsp.stackexchange.com/questions/3466/amplitude-of-the-signal-in-frequency-domain-different-from-time-domain
    * @see https://dsp.stackexchange.com/questions/8604/fft-of-sine-wave-not-coming-as-expected-i-e-single-point
    * @see https://www.dataq.com/data-acquisition/general-education-tutorials/fft-fast-fourier-transform-waveform-analysis.html
    * @see http://www.ni.com/white-paper/4278/en/  [2]
    */
  it should "produce the correct absolute values and arguments for various summed oscillator" in {
    val f1 = 1.0 /* Hz */ ; val a1 = 1.0  ; val phase1 = Math.toRadians(-45)
    val f2 = 2.0 /* Hz */ ; val a2 = 1.0  ; val phase2 = Math.toRadians(0)
    val f3 = 4.0 /* Hz */ ; val a3 = 0.6; val phase3 = Math.toRadians(20)
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
    // samplesTo at twice Nyquist (avoid aliasing) and samplesTo for 2.2 seconds
    val fs = samplingFreq * 2
    val dur = 2.2
    val (dd, xx) = Fourier.samplingSpecification(fs, dur)

    val sig1 = Function.Sin(f1, a1, phase1, dd)
    val sig2 = Function.Sin(f2, a2, phase2, dd)
    val sig3 = Function.Cos(f3, a3, phase3, dd)
    val sig4 = sig1 - sig2 + sig3

    val (len,xa,fftSig) = Fourier.FFT(fs, xx.length, sig4, pad = false, DftNormalization.STANDARD)
    val (sya, syb) = Fourier.topAbsoluteComponents(5, fftSig) // for testing puposes only, could have use ya, yb
    // Absolute
    // f1
    xa(sya(0)._2) should be (f1 +- eps)
    sya(0)._1 / (len/2.0) should be (a1 +- eps)
    // f2
    xa(sya(1)._2) should be (f2 +- eps)
    sya(1)._1 / (len/2.0) should be (a2 +- eps)
    // f3
    xa(sya(2)._2) should be (f3 +- eps)
    sya(2)._1 / (len/2.0) should be (a3 +- eps)
    // No more components
    sya(3)._1 should be (0.0 +- eps)
    sya(4)._1 should be (0.0 +- eps)
    // Argument
    // f1
    xa(syb(0)._2) should be (f1 +- eps)
    Math.cos(syb(0)._1) should be (Math.sin(phase1) +- eps)
    // f2
    xa(syb(1)._2) should be (f2 +- eps)
    Math.cos(syb(1)._1) should be (Math.sin(phase2) +- eps)
    // f3
    xa(syb(2)._2) should be (f3 +- eps)
    Math.cos(syb(2)._1) should be (Math.cos(phase3) +- eps)
    // No more components

    // JPlot seems to be ok.
    /*
    val ya = Utils.FFTPAbs(fftSig)
    val yb = Utils.FFTPhase(fftSig)

    import breeze.plot._
    val fig = Figure("FFT: Multiple Oscillators")
    val p1 = fig.subplot(0)
    p1 += plot(xx, sig1(xx.length), name = s"@ $f1 Hz")
    p1 += plot(xx, sig2(xx.length), name = s"@ $f2 Hz")
    p1 += plot(xx, sig3(xx.length), name = s"@ $f3 Hz")
    p1 += plot(xx, sig4(xx.length), name = "mixed f1 - f2 + f3")
    p1.title = s"Signals (t)"
    p1.legend = true
    //p1.refresh

    val p2 = fig.subplot(2,1,1)
    p2 += plot(xa, ya.take(xa.length))
    p2.title = "FFT (Abs)"
    //p2.refresh

    // FFT phase is noisy (not usable)
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    val p3 = fig.subplot(3,1,2)
    p3 += plot(xa, yb.take(xa.length))
    p3.title = "FFT (Theta)"
    p3.refresh*/
  }

  /**
    * This is an example of aliasing. in this example we generate two sine waves, one at 110Hz and the
    * other at 1110Hz. If we samplesTo below the Nyquist frequency, say at 1000Hz, and if the sine waves
    * are in sync (starting in te same phase), sampling will allways occur in the intersection of both
    * signals. This means, only the signal of lower frequency is correctly sampled - so the FFT will
    * only produce a single component.
    *
    * Note also that in these conditions ratios of near zero values may produce very large vales. The result
    * is a phase diagram that exhibits noise.
    *
    * See Sampling: What Nyquist Didn’t Say, and What to Do About It, by Tim Wescott, of Wescott Design Services
    * June 20, 2016, Figure 2, page 3
    *
    * @see http://www.wescottdesign.com/articles/Sampling/sampling.pdf
    */
  it should "produce the IN-correct absolute values and arguments for a summed oscillators (Nyquist)" in {
    val f1 = 110.0 /* Hz */ ; val a1 = 1.0  ; val phase1 = Math.toRadians(-45)
    val f2 = 1110.0 /* Hz */ ; val a2 = 1.0  ; val phase2 = Math.toRadians(0)

    // samplesTo at Nyquist (avoid aliasing) and samplesTo for 2.2 seconds
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2)
    val fs = samplingFreq
    val dur = 2.2
    val (dd, xx) = Fourier.samplingSpecification(fs, dur)

    val sig1 = Function.Sin(f1, a1, phase1, dd)
    val sig2 = Function.Sin(f2, a2, phase2, dd)
    val sig4 = sig1 + sig2

    val (len,xa,fftSig) = Fourier.FFT(fs, xx.length, sig4, pad = false, DftNormalization.STANDARD)

    val (sya, syb) = Fourier.topAbsoluteComponents(5, fftSig) // for testing puposes only, could have use ya, yb
    // Absolute
    // f1
    xa(sya(0)._2) should be (f1 +- eps)
    sya(0)._1 / (len/2.0) should be (a1 +- eps)
    // No more components
    (sya(1)._1 / (len/2.0)) should be (0.0 +- eps)
    (sya(3)._1 / (len/2.0)) should be (0.0 +- eps)
    (sya(4)._1  / (len/2.0)) should be (0.0 +- eps)
    // Argument
    // f1
    xa(syb(0)._2) should be (f1 +- eps)
    Math.cos(syb(0)._1) should be (Math.sin(phase1) +- eps)
    // No more components

    // JPlot seems to be ok.
    /*
    val ya = Utils.FFTPAbs(fftSig)
    val yb = Utils.FFTPhase(fftSig)

    import breeze.plot._
    val fig = Figure("FFT: Multiple Oscillators")
    val p1 = fig.subplot(0)
    p1 += plot(xx, sig1(xx.length), name = s"@ $f1 Hz")
    p1 += plot(xx, sig2(xx.length), name = s"@ $f2 Hz")
    p1 += plot(xx, sig4(xx.length), name = "mixed f1 - f2 + f3")
    p1.title = s"Signals (t)"
    p1.legend = true
    //p1.refresh

    val p2 = fig.subplot(2,1,1)
    p2 += plot(xa, ya.take(xa.length))
    p2.title = "FFT (Abs)"
    //p2.refresh

    // FFT phase is noisy (not usable)
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    val p3 = fig.subplot(3,1,2)
    p3 += plot(xa, yb.take(xa.length))
    p3.title = "FFT (Theta)"
    p3.refresh*/
  }

  /**
    * We now get many components that exceed the number of base oscillators. In addition to this, none of the top
    * frequency components have the ame frequency as any of the oscillators. So if one of those signals are an
    * indicator of a failure we will not be able to identify it without any (possibly non-linear) mapping. Note that
    * this is true when the signals are multiplied (sum and subtraction still work).
    */
  it should "produce the correct absolute values and arguments for various summed and multiplied oscillator" in {
    val f1 = 1 /* Hz */  ; val a1 = 1    ; val phase1 = Math.toRadians(-45)
    val f2 = 2 /* Hz */  ; val a2 = 1.5  ; val phase2 = Math.toRadians(0)
    val f3 = 4 /* Hz */  ; val a3 = 2    ; val phase3 = Math.toRadians(25)
    val f4 = 10 /* Hz */ ; val a4 = 0.8  ; val phase4 = Math.toRadians(0)
    val f5 = 20 /* Hz */ ; val a5 = 0.6  ; val phase5 = Math.toRadians(25)

    // samplesTo at twice the Nyquist frequency and samplesTo for 2.2 seconds
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3, f4, f5)
    val fs = 2 * samplingFreq
    val dur = 2.2
    val (dd, xx) = Fourier.samplingSpecification(fs, dur)

    val sig1 = Function.Sin(f1, a1, phase1, dd)
    val sig2 = Function.Sin(f2, a2, phase2, dd)
    val sig3 = Function.Sin(f3, a3, phase3, dd)
    val sig4 = Function.Sin(f4, a4, phase4, dd)
    val sig5 = Function.Sin(f5, a5, phase5, dd)
    //val sig4 = Distribution.Normal(0, 0.3)
    val sig6 = sig1 - (sig2 + sig4) + (sig3 * sig5)


    val (len,xa,fftSig) = Fourier.FFT(fs, xx.length, sig6, pad = false, DftNormalization.STANDARD)
    val ya = Fourier.abs(fftSig)
    val yb = Fourier.phase(fftSig)
    val (sya, _) = Fourier.topAbsoluteComponents(40, ya, yb)
    //def frequencyAt(fs: Double, len: Int)(i : Int) = i * ((fs*1.0)/len)
    val threshold = a5 * (len/2) * 0.6  // 60% of the smallest amplitude
    val signifcantAmp = sya.filter( p => p._1 > threshold)
    signifcantAmp.length shouldBe 6
    //println( signifcantAmp.map(p => (p._1, xa(p._2))).mkString )
    signifcantAmp.map(p => xa(p._2)) should not contain theSameElementsAs( List(f1, f2, f3, f4, f5).map(_.toDouble) )

    // JPlot seems to be ok.
    /*
    import breeze.plot._
    val fig = Figure("FFT: Multiple Oscillators")
    val p1 = fig.subplot(0)
    p1 += plot(xx, sig1(xx.length), name = s"@ $f1 Hz", style = '.')
    p1 += plot(xx, sig2(xx.length), name = s"@ $f2 Hz", style = '.')
    p1 += plot(xx, sig3(xx.length), name = s"@ $f3 Hz", style = '.')
    p1 += plot(xx, sig4(xx.length), name = s"@ $f4 Hz", style = '.')
    p1 += plot(xx, sig5(xx.length), name = s"@ $f5 Hz", style = '.')
    p1 += plot(xx, sig6(xx.length), name = "mixed f1 - (f2*f4) + (f3*f5)")
    p1.title = s"Signals (t)"
    p1.legend = true
    //p1.refresh

    val p2 = fig.subplot(2,1,1)
    p2 += plot(xa, ya.take(xa.length))
    p2.title = "FFT (Abs)"
    //p2.refresh

    import org.jfree.chart.annotations.XYTextAnnotation
    p2.plot.addAnnotation(new XYTextAnnotation("(1)",  xa(sya(0)._2), sya(0)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(2)",  xa(sya(1)._2), sya(1)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(3)",  xa(sya(2)._2), sya(2)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(4)",  xa(sya(3)._2), sya(3)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(5)",  xa(sya(4)._2), sya(4)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(6)",  xa(sya(5)._2), sya(5)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(7)",  xa(sya(6)._2), sya(6)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(8)",  xa(sya(7)._2), sya(7)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(9)",  xa(sya(8)._2), sya(8)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(10)", xa(sya(9)._2), sya(9)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(11)", xa(sya(10)._2), sya(10)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(12)", xa(sya(11)._2), sya(11)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(13)", xa(sya(12)._2), sya(12)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(14)", xa(sya(13)._2), sya(13)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(15)", xa(sya(14)._2), sya(14)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(16)", xa(sya(15)._2), sya(15)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(17)", xa(sya(16)._2), sya(16)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(18)", xa(sya(17)._2), sya(17)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(19)", xa(sya(18)._2), sya(18)._1))
    p2.plot.addAnnotation(new XYTextAnnotation("(20)", xa(sya(19)._2), sya(19)._1))

    // FFT phase is noisy (not usable)
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    val p3 = fig.subplot(3,1,2)
    p3 += plot(xa, yb.take(xa.length))
    p3.title = "FFT (Theta)"
    p3.refresh*/
  }

  it should "produce several absolute values and arguments for a linear chirp oscillator" in {
    val f1 = 1 /* Hz */ ; val a1 = 1 ; val phase1 = Math.toRadians(-45)

    // samplesTo at twice the Nyquist frequency and samplesTo for 2.2 seconds
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1)
    val amplitudeIncrease = 1.08
    val frequencyIncrease = 1.1
    val fs = 300 * samplingFreq  // we increase the
    val duration = 3
    val (dd, xx) = Fourier.samplingSpecification(fs, duration)

    val sig1 = Function.LinearChirp(f1, a1, phase1, amplitudeIncrease, frequencyIncrease, dd)

    val (len,xa,fftSig) = Fourier.FFT(fs, xx.length, sig1, pad = false, DftNormalization.STANDARD)
    val ya = Fourier.abs(fftSig)
    val yb = Fourier.phase(fftSig)
    xa.length shouldBe (len / 2)

    val (sya, _) = Fourier.topAbsoluteComponents(40, ya, yb)
    //def frequencyAt(fs: Double, len: Int)(i : Int) = i * ((fs*1.0)/len)
    val threshold = a1 * (len/2) * 0.6  // 60% of the smallest amplitude
    val signifcantAmp = sya.filter( p => p._1 > threshold)
    signifcantAmp.length shouldBe 4
    //println(sya.mkString(","))
    //println(signifcantAmp.mkString(","))

    // Seems to be ok
    /*
    import breeze.plot._
    val fig = Figure("FFT: Multiple Oscillators")
    val p1 = fig.subplot(0)
    p1 += plot(xx, sig1(xx.length), name = s"@ $f1 Hz", style = '-')
    p1.title = s"Signals (t)"
    p1.legend = true
    p1.refresh

    val p2 = fig.subplot(2,1,1)
    p2 += plot(xa, ya.take(xa.length))
    p2.title = "FFT (Abs)"

    //p2.refresh
    // FFT phase is noisy (not usable)
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    val p3 = fig.subplot(3,1,2)
    p3 += plot(xa, yb.take(xa.length))
    p3.title = "FFT (Theta)"
    p3.refresh*/
  }

  it should "produce several absolute values and arguments for an exponential chirp oscillator" in {
    val f1 = 1 /* Hz */ ; val a1 = 1 ; val phase1 = Math.toRadians(-45)

    // samplesTo at twice the Nyquist frequency and samplesTo for 2.2 seconds
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1)
    val amplitudeIncrease = 2
    val frequencyIncrease = 2
    val fs = 300 * samplingFreq  // we increase the
    val duration = 3
    val (dd, xx) = Fourier.samplingSpecification(fs, duration)

    val sig1 = Function.ExpChirp(f1, a1, phase1, amplitudeIncrease, frequencyIncrease, dd)

    val (len,xa,fftSig) = Fourier.FFT(fs, xx.length, sig1, pad = false, DftNormalization.STANDARD)
    val ya = Fourier.abs(fftSig)
    val yb = Fourier.phase(fftSig)

    val (sya, _) = Fourier.topAbsoluteComponents(40, ya, yb)
    //def frequencyAt(fs: Double, len: Int)(i : Int) = i * ((fs*1.0)/len)
    val threshold = a1 * (len/2) * 0.6  // 60% of the smallest amplitude
    val signifcantAmp = sya.filter( p => p._1 > threshold)
    signifcantAmp.length shouldBe 5
    //println(sya.mkString(","))
    //println(signifcantAmp.mkString(","))

    /*
    import breeze.plot._
    val fig = Figure("FFT: Multiple Oscillators")
    val p1 = fig.subplot(0)
    p1 += plot(xx, sig1(xx.length), name = s"@ $f1 Hz")
    p1.title = s"Signals (t)"
    p1.legend = true
    p1.refresh

    val p2 = fig.subplot(2,1,1)
    p2 += plot(xa, ya.take(xa.length))
    p2.title = "FFT (Abs)"

    //p2.refresh
    // FFT phase is noisy (not usable)
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    val p3 = fig.subplot(3,1,2)
    p3 += plot(xa, yb.take(xa.length))
    p3.title = "FFT (Theta)"
    p3.refresh
    */
  }


  /**
    * For a normal distribution with mean at 0 the FFT is uniformly erratic at all frequencies
    * For a normal distribution with mean at 1 the FFT is uniformly erratic at all frequencies but has a clear DC
    * component. The higher the mean value, the bigger the DC component. rest of the components seem less erratic
    * due to scaling.
    * Increasing the standard deviation increases the non DC components. So for a large `mean/sd` ratios we get less
    * interference in the rest of the FFT components. For `sd > mean` the signal seems to be erratic again. If large
    * enough, the DC component is washed out.
    *
    * Conclusion: if the S/N rations is high we can still see clear FFT components, otherwise of the true signal
    * otherwise they are not visible.
    */
  it should "produce many absolute values and arguments for random values " in {
    val f1 = 1 /* Hz */ ; val a1 = 1 //; val phase1 = Math.toRadians(-45)

    // samplesTo at twice the Nyquist frequency and samplesTo for 2.2 seconds
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1)
    val fs = 50 * samplingFreq  // we increase the
    val duration = 3
    val (_, xx) = Fourier.samplingSpecification(fs, duration)

    val sig1 = Distribution.Normal(1, 1, new JDKRandomGenerator(98765))

    val (len,xa,fftSig) = Fourier.FFT(fs, xx.length, sig1, pad = false, DftNormalization.STANDARD)
    val ya = Fourier.abs(fftSig)
    val yb = Fourier.phase(fftSig)

    val (sya, _) = Fourier.topAbsoluteComponents(40, ya, yb)
    //def frequencyAt(fs: Double, len: Int)(i : Int) = i * ((fs*1.0)/len)
    val threshold = a1 * (len/2) * 0.6  // 60% of the smallest amplitude
    val signifcantAmp = sya.filter( p => p._1 > threshold)
    signifcantAmp.length shouldBe 1
    //println(sya.mkString(","))
    //println(signifcantAmp.mkString(","))

    /*
    import breeze.plot._
    val fig = Figure("FFT: Multiple Oscillators")
    val p1 = fig.subplot(0)
    p1 += plot(xx, sig1(xx.length), name = s"@ $f1 Hz")
    p1.title = s"Signals (t)"
    p1.legend = true
    p1.refresh

    val p2 = fig.subplot(2,1,1)
    p2 += plot(xa, ya.take(xa.length))
    p2.title = "FFT (Abs)"

    //p2.refresh
    // FFT phase is noisy (not usable)
    // https://dsp.stackexchange.com/questions/13844/baffled-by-fft-phase-spectrum
    // http://www.gaussianwaves.com/2015/11/interpreting-fft-results-obtaining-magnitude-and-phase-information/
    val p3 = fig.subplot(3,1,2)
    p3 += plot(xa, yb.take(xa.length))
    p3.title = "FFT (Theta)"
    p3.refresh*/

  }

  "Plotting Fourier transform" should "produce image correctly" in {
    val f1 = 1 /* Hz */  ; val a1 = 1    ; val phase1 = Math.toRadians(-45)
    val f2 = 2 /* Hz */  ; val a2 = 1.5  ; val phase2 = Math.toRadians(0)
    val f3 = 4 /* Hz */  ; val a3 = 2    ; val phase3 = Math.toRadians(25)
    val f4 = 10 /* Hz */ ; val a4 = 0.8  ; val phase4 = Math.toRadians(0)
    val f5 = 20 /* Hz */ ; val a5 = 0.6  ; val phase5 = Math.toRadians(25)

    // samplesTo at twice the Nyquist frequency and samplesTo for 2.2 seconds
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3, f4, f5)
    val fs = 2 * samplingFreq
    val dur = 2.2
    val (dd, _) = Fourier.samplingSpecification(fs, dur)

    val sig1 = Function.Sin(f1, a1, phase1, dd)
    val sig2 = Function.Sin(f2, a2, phase2, dd)
    val sig3 = Function.Sin(f3, a3, phase3, dd)
    val sig4 = Function.Sin(f4, a4, phase4, dd)
    val sig5 = Function.Sin(f5, a5, phase5, dd)
    val sig6: Base.Sum[Base.Infinite, Double] = sig1 - (sig2 + sig4) + (sig3 * sig5)

    //import better.files._
    //import java.io.{File => JFile}
    import better.files.File.root

    // Headless environment causes plotting routines to throw exception
    if (!java.awt.GraphicsEnvironment.isHeadless) {
      val tmp = File(JSystem.getProperty("java.io.tmpdir"))
      val file_1 = tmp/"testsig6.png"
      Fourier.plotFourierTransform(Fourier.FFT[Base.Infinite] _, fs, dur, sig6, "Save as PNG", file_1.toString, pad=false, DftNormalization.STANDARD)
      file_1.exists shouldBe true
      file_1.delete(true)

      val file_2 = tmp/"testsig6.svg"
      Fourier.plotFourierTransform(Fourier.DFT[Base.Infinite] _, fs, dur, sig6, "Save as SVG", file_2.toString, pad = false, DftNormalization.STANDARD)
      file_2.exists shouldBe true
      file_2.delete(true)
    }
  }

  "STFT functions" should "split windows correctly (no overlap)" in {
    val sig1 = BigDecimal(1.0) to 12.0 by 1.0 map(_.toDouble)
    val samplingFrequency = 1 // Hz

    // No boundary extension needed, no padding needed
    val (step1, boundary1, wins1, _, cola1) = Fourier.splitSignalS(sig1,samplingFrequency,4)
    //println(s"step1 = $step1")
    //println(s"wins1 = ${wins1.mkString(",")}")

    step1 shouldBe 4
    boundary1 shouldBe 0
    cola1 shouldBe true
    wins1.toList should contain theSameElementsAs List(
      Vector(1.0, 2.0, 3.0, 4.0),
      Vector(5.0, 6.0, 7.0, 8.0),
      Vector(9.0, 10.0, 11.0, 12.0))

    // Must extend last window for constant length windows, padding required
    val (step2, boundary2, wins2, _, cola2) = Fourier.splitSignalS(sig1,samplingFrequency,5)

    step2 shouldBe 5 // original step
    boundary2 shouldBe 3 // original boundary extension
    cola2 shouldBe true
    wins2.toList should contain theSameElementsAs List(
      Array(0.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 0.0),
      Array(0.0, 0.0, 6.0, 7.0, 8.0, 9.0, 10.0, 0.0),
      Array(0.0, 0.0, 11.0, 12.0, 0.0, 0.0, 0.0, 0.0))

    // Must extend last window for constant length windows, padding required for 8 but we truncate to 4
    val (step3, boundary3, wins3, _, cola3) = Fourier.splitSignalS(sig1,samplingFrequency,5,pad=false)
    //println(s"step3 = $step3")
    //println(s"wins3 = ${wins3.mkString(",")}")

    step3 shouldBe 4
    boundary3 shouldBe 0
    cola3 shouldBe true
    wins3.toList should contain theSameElementsAs List(
      Array(1.0, 2.0, 3.0, 4.0),
      Array(5.0, 6.0, 7.0, 8.0),
      Array(9.0, 10.0, 11.0, 12.0))

    // Leave window length unchanged, padding and extending would have been required
    val (step4, boundary4, wins4, _, cola4) = Fourier.splitSignalS(sig1,samplingFrequency,3,extendBoundary=false,pad=false)
    //println(s"step4 = $step4")
    //println(s"wins4 = ${wins4.mkString(",")}")

    step4 shouldBe 2
    boundary4 shouldBe 0
    cola4 shouldBe true
    wins4.toList should contain theSameElementsAs List(
      Array(1.0, 2.0),
      Array(3.0, 4.0),
      Array(5.0, 6.0),
      Array(7.0, 8.0),
      Array(9.0, 10.0),
      Array(11.0, 12.0))
  }

  it should "split windows correctly (overlap)" in {
    val sig1 = BigDecimal(1.0) to 12.0 by 1.0 map(_.toDouble)
    val samplingFrequency = 1 // Hz

    // No boundary extension needed, no padding needed
    val (step1, boundary1, wins1, _, cola1) = Fourier.splitSignalS(sig1,samplingFrequency,4,overlap=2)
    //println(s"step1 = $step1")
    //println(s"wins1 = ${wins1.map(_.mkString(",")).mkString(";\n")}")

    step1 shouldBe 2
    boundary1 shouldBe 0
    cola1 shouldBe true
    wins1.toList should contain theSameElementsAs List(
      Array(1.0, 2.0, 3.0, 4.0),
      Array(3.0, 4.0, 5.0, 6.0),
      Array(5.0, 6.0, 7.0, 8.0),
      Array(7.0, 8.0, 9.0, 10.0),
      Array(9.0, 10.0, 11.0, 12.0))

    // Must extend last window for constant length windows, padding required
    val (step2, boundary2, wins2, _, cola2) = Fourier.splitSignalS(sig1,samplingFrequency,5,overlap=2)
    //println(s"wins2 = ${wins2.map(_.mkString(",")).mkString(";\n")}")

    step2 shouldBe 3 // 5 - 2
    boundary2 shouldBe 2 // rest of window
    cola2 shouldBe true
    wins2.toList should contain theSameElementsAs List(
      Array(0.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 0.0),
      Array(0.0, 0.0, 4.0, 5.0, 6.0, 7.0, 8.0, 0.0),
      Array(0.0, 0.0, 7.0, 8.0, 9.0, 10.0, 11.0, 0.0),
      Array(0.0, 0.0, 10.0, 11.0, 12.0, 0.0, 0.0, 0.0))

    // Must extend last window for constant length windows, padding required for 8 but we truncate to 4
    val (step3, boundary3, wins3, _, cola3) = Fourier.splitSignalS(sig1,samplingFrequency,5,overlap=2,pad=false)
    //println(s"step3 = $step3")
    //println(s"wins3 = ${wins3.mkString(",")}")

    step3 shouldBe 2
    boundary3 shouldBe 0
    //cola3 shouldBe false // No padding, so cannot reconstruct
    wins3.toList should contain theSameElementsAs List(
      Array(1.0, 2.0, 3.0, 4.0),
      Array(3.0, 4.0, 5.0, 6.0),
      Array(5.0, 6.0, 7.0, 8.0),
      Array(7.0, 8.0, 9.0, 10.0),
      Array(9.0, 10.0, 11.0, 12.0))

    // Leave window length unchanged, padding and extending would have been required
    val (step4, boundary4, wins4, _, cola4) = Fourier.splitSignalS(sig1,samplingFrequency,3,overlap=1,extendBoundary=false,pad=false)
    //println(s"step4 = $step4")
    //println(s"wins4 = ${wins4.mkString(",")}")

    step4 shouldBe 1
    boundary4 shouldBe 0
    cola4 shouldBe true
    wins4.toList should contain theSameElementsAs List(
      Array(1.0, 2.0), Array(2.0, 3.0),
      Array(3.0, 4.0), Array(4.0, 5.0),
      Array(5.0, 6.0), Array(6.0, 7.0),
      Array(7.0, 8.0), Array(8.0, 9.0),
      Array(9.0, 10.0), Array(10.0, 11.0),
      Array(11.0, 12.0))
  }

  it should "correctly calculate the STFT (no overlap)" in {
    val f1 = 10.0 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(0)
    val f2 = 20.0 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val samplingFreq = 10 * Fourier.nyquistNamplingFrequency(f1, f2)
    val duration = 2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,duration)

    val good = Function.Sin(f1, a1, phase1, dd)  // good
    val bad = Function.Sin(f2, a2, phase2, dd)  // bad

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, good(xx.length))
    plt += plot(xx, bad(xx.length))
    plt.title = s"DSL Ops @ $samplingFreq"
    plt.refresh*/

    val (_,xaGood,fftSigGood) = Fourier.FFT(samplingFreq, xx.length, good, pad = false, DftNormalization.STANDARD)
    //Fourier.plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaGood, fftSigGood, title="Good")

    val (_,xaBad,fftSigBad) = Fourier.FFT(samplingFreq, xx.length, bad, pad = false, DftNormalization.STANDARD)
    //Fourier.plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xaBad, fftSigBad, title="Bad")

    // Signal at 20 Hz means that we have to samplesTo at least 1/20 seconds to get a full period (0.05 seconds)
    // So how many samples do we need to get using the selected sampling rate?
    val f2Period = 1.0 / f2
    val delta = 1.0 / samplingFreq
    val lenBad = Math.ceil(f2Period / delta).toInt

    // Alternatively use the utility function to get this info
    val samples1T = Fourier.numberOfSamplesFor1Period(samplingFreq, f2)
    samples1T shouldBe lenBad

    // Now calculate the STFT with windows larger than that 1T (aliasing causes reduced precision)
    // Should be a least for 2 or more periods (good resolution at 10x)
    val (stftsGood , xaSGood, colaGood)= Fourier.stft(good(xx.length), samplingFreq, 10*lenBad)
    xaSGood.length shouldBe 128 // 128 frequencies (2^7)
    colaGood shouldBe true

    // We can plot this to confirm that the STFT is correct (all of them should be the same)
    //Fourier.plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xa, stfts(0), title="Good_i")
    /*stfts.zipWithIndex.map { e =>
      val (s,i) = e
      Fourier.plotFourier(samplingFreq, duration, xx, good(xx.length).toArray, xa, s, title=s"Good $i") }
    */

    // We know the good signal has a single (top) component at f1 = 10Hz
    // So get the top component
    val (maxAbsGood, maxPhaseGood) = Fourier.topAbsoluteComponents(1, stftsGood(0))
    // Now get the frequency domain scale (delta frequency per element)
    val (_,deltaFreqGood) = Fourier.deltaFreq(samplingFreq, stftsGood(0).length)
    // So at that component we have this frequency
    val topFreqGood = maxAbsGood(0)._2 * deltaFreqGood
    // Which should be about what we want
    // For greater precision, increase the number of samples, ex: 10xlenBad
    topFreqGood shouldBe f1 +- 0.7

    // Now calculate the STFT with windows larger than that 1T (aliasing causes reduced precision)
    // Should be a least for 2 or more periods (good resolution at 10x)
    val (stftsBad , xaSBad, colaBad)= Fourier.stft(bad(xx.length), samplingFreq, 10*lenBad)
    xaSBad.length shouldBe 128 // 128 frequencies (2^7)
    colaBad shouldBe true

    // We can plot this to confirm that the STFT is correct (all of them should be the same)
    //Fourier.plotFourier(samplingFreq, duration, xx, bad(xx.length).toArray, xaSBad, stftsBad(0), title="Bad_i")

    // We know the good signal has a single (top) component at f1 = 10Hz
    // So get the top component
    val (maxAbsBad, maxPhaseBad) = Fourier.topAbsoluteComponents(1, stftsBad(0))
    // Now get the frequency domain scale (delta frequency per element)
    val (_,deltaFreqBad) = Fourier.deltaFreq(samplingFreq, stftsBad(0).length)
    // So at that component we have this frequency
    val topFreqBad = maxAbsBad(0)._2 * deltaFreqBad
    // Which should be about what we want
    // For greater precision, increase the number of samples, ex: 10xlenBad
    topFreqBad shouldBe f2 +- 0.4

    // Real signal so only take half the frequencies + DC
    val freqDomainLen = xaSBad.length
    val freqAbs = stftsBad.map(_.take(freqDomainLen))

    // Lest normalize the data (not required)
    val zeros = DenseMatrix.zeros[Double](freqDomainLen, stftsBad.length)
    val abs = freqAbs.map( Fourier.abs )
    abs.zipWithIndex.foreach( e => zeros(::,e._2) += DenseVector(e._1) )
    val maxAbs = max(zeros)
    val minAbs = min(zeros)
    val range = maxAbs - minAbs
    // //val normalized = ones - (zeros /:/ range)
    val normalized = zeros /:/ range

    // JPlot the STFT
    // IMPORTANT: we can see the aliasing that occurs due to the STFT windowing
    import breeze.plot._

    val colors = GradientPaintScale(lower=0, upper=1.0, gradient = PaintScale.BlueToRed)
    "plotSTFTMat(normalized, colors)" should compile
    //plotSTFTMat(normalized, colors)

    val colors1 = GradientPaintScale(lower=0, upper=maxAbs, gradient = PaintScale.BlueToRed)
    "plotSTFTMat(zeros, colors)" should compile
    //plotSTFTMat(zeros, colors1)
  }

  it should "allow us to plot the correct spectrogram (window padding, no overlap, no filters, no detrend)" in {
    val seed = 17399225432L
    val rg = new JDKRandomGenerator(seed)

    // see https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.stft.html

    /*
      Generate a test signal, a 2 Vrms sine wave whose frequency is slowly
      modulated around 3kHz, corrupted by white noise of exponentially
      decreasing magnitude sampled at 10 kHz.
     */
    val fs = 10.0e3
    val fmod = 0.25
    val fcarrier = 3e3
    val N = 1e5
    // amp = 2 * np.sqrt(2)
    val amp = 2 * Math.sqrt(2)
    // noise_power = 0.01 * fs / 2
    val noise_power = 0.01 * fs / 2
    // time = np.arange(N) / float(fs)
    val time = (BigDecimal(0.0) until N by 1.0) map ( _.toDouble / fs )
    // mod = 500*np.cos(2*np.pi*0.25*time)
    val mod = time map { t => 500*Math.cos(2*Math.PI*fmod*t) }
    // carrier = amp * np.sin(2*np.pi*3e3*time + mod)
    val carrier = time.map { t => amp * Math.sin(2*Math.PI*fcarrier*t) }
      .zip(mod)
      .map( p => p._1 + p._2)
    // noise = np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
    val norm = Distribution.Normal(0, Math.sqrt(noise_power), rg)
    val rand = norm.toStream.take(N.toInt).toArray

    //  noise *= np.exp(-time/5)
    val exp = time.map( t => math.exp(-t/5.0) )
    val noise = rand.zip(exp).map( e => e._1 * e._2)
    // x = carrier + noise
    val x = carrier.zip(noise).map( e => e._1 + e._2)


    // JPlot the signals above to check that all is ok
    // We should see a large sinusoid. When zooming into
    // this signal we see the carrier signal with noise
    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(time, mod)
    plt += plot(time, carrier)
    plt += plot(time, noise)
    plt += plot(time, x)
    plt.title = s"STFT Example"
    plt.refresh*/

    // JPlot the traditional FFT for all of the signal and see that it is
    // what is expected. We should see an initial set of very high amplitude
    // frequencies (highest at the modulation frequency of 0.25 Hz) and a
    // small yet visible "bump" of the carrier at 3kHz (use zoom).
    /*def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) = Fourier.FFT(fs, signal_length, sig)
    val duration = time.last
    Fourier.plotFourierTransform(fft, fs, duration, x.toArray, "Example")
    */

    /* Compute and plot the STFT’s magnitude. */
    // How large should be the windows?
    val samples1T = Fourier.numberOfSamplesFor1Period(fs, fcarrier)
    samples1T shouldBe 4

    // Calculate thr STFT
    // Lest use the value from the original example
    // f, t, Zxx = signal.stft(x, fs, nperseg=1000)
    val (stfts , xa, cola) = Fourier.stft(x, fs, 1000)
    xa.length shouldBe 512 // 512 frequencies (2^9)
    stfts(0).length shouldBe 1024
    stfts.length shouldBe 100
    cola shouldBe true

    // We can plot this to confirm that the STFT is correct (all of them should be the same)
    // JPlot the traditional FFT for all of the signal and see that it is
    // what is expected. We should see an initial set of very high amplitude
    // frequencies (highest at the modulation frequency of 0.25 Hz) and a
    // small yet visible "bump" of the carrier at 3kHz (use zoom).
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(0), title="Example")
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(10), title="Example")
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(50), title="Example")


    // Real signal so only take half the frequencies + DC
    val freqDomainLen = xa.length
    //println(s"freqDomainLen=$freqDomainLen")
    // Get only half of th required frequencies
    val freqs = stfts.map(_.take(freqDomainLen))
    // Calculate the absolute value of the frequency components
    val absFreq = freqs.map( Fourier.abs )

    // Now convert the SFTF to a Matrix that can be plotted (See Breeze)
    val zeros = DenseMatrix.zeros[Double](freqDomainLen, stfts.length)
    absFreq.zipWithIndex.foreach( e => zeros(::,e._2) += DenseVector(e._1) )
    /* // We could normalize if we wanted, but plot already does that
    val maxAbs = max(zeros)
    val minAbs = min(zeros)
    println(s"maxAbs = $maxAbs")
    println(s"minAbs = $minAbs")
    println(s"amp = $amp")
    val range = maxAbs - minAbs
    val normalized = zeros /:/ range */

    // If we look at the FFT plots above, we see the that carrier amplitude
    // (amp = 2*sqrt(2)= 2.828) is very small compared to the modulating signal (500)
    // So if we plot the STFT , we will not see those frequency bands clearly.

    // Find index of the carrier frequency
    val index = Fourier.indexOfFrequency(fs, stfts(0).length, fcarrier)
    index shouldBe 307 // this shows up correctly in the plot below
    // Lets find the top frequency at about is point
    val initial = 300
    val rest = absFreq(0).drop(initial)
    val topFreq = rest.max
    // Frequency at the carrier index should be the same as the maximum we found here
    absFreq(0)(index) shouldBe topFreq
    // Now go the other way, find the index at which we have the maximum frequency
    val maxI = rest.zipWithIndex.maxBy(_._1)._2 + initial
    // Should be the same as the initial one we expect
    index shouldBe maxI

    // JPlot the STFT (indirectly)
    import breeze.plot._

    // Pick a gradient colour
    // Note that we set the minimum and maximum values of the gradient. If
    // we did not do this the carrier component at 3kHz is so small compared
    // to the 0.25Hz modulation that it would not be visible. USe the FFT
    // plots to determine a good value. Note that any value above the maximum
    // will have the same colour.
    val colors = GradientPaintScale(lower=0, upper=5000.0, gradient = PaintScale.BlueToRed)
    //val colors = GradientPaintScale(lower=minAbs, upper=maxAbs, gradient = PaintScale.BlueToRed)
    //val colors = GradientPaintScale(lower=minAbs, upper=maxAbs, gradient = PaintScale.Rainbow)
    //val colors = GradientPaintScale(lower=minAbs, upper=maxAbs, gradient = PaintScale.RedToGreen)
    //val colors = GradientPaintScale(lower=minAbs, upper=maxAbs, gradient = PaintScale.RedOrangeYellow)
    //val colors = GradientPaintScale(lower=maxAbs, upper=minAbs, gradient = PaintScale.GreenYelloOrangeRed)
    //val colors = GradientPaintScale(lower=minAbs, upper=maxAbs, gradient = PaintScale.BlackToWhite)
    //val colors = GradientPaintScale(lower=minAbs, upper=maxAbs, gradient = PaintScale.Heat)
    //val colors = GradientPaintScale(lower=minAbs, upper=maxAbs, gradient = PaintScale.Hot)
    //val colors = GradientPaintScale(lower=minAbs, upper=maxAbs, gradient = PaintScale.MaroonToGold)

    "plotSTFTMat(zeros, colors)" should compile
    //plotSTFTMat(zeros, colors)

    // JPlot the STFT directly
    "plotSTFT(stfts, colors)" should compile
    //plotSTFT(stfts, colors)

  }

  it should "allow us to plot the correct spectrogram (window padding, overlap, no filters, no detrend)" in {
    val seed = 17399225432L
    val rg = new JDKRandomGenerator(seed)

    // see https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.stft.html

    /*
      Generate a test signal, a 2 Vrms sine wave whose frequency is slowly
      modulated around 3kHz, corrupted by white noise of exponentially
      decreasing magnitude sampled at 10 kHz.
     */
    val fs = 10.0e3
    val fmod = 0.25
    val fcarrier = 3e3
    val N = 1e5
    // amp = 2 * np.sqrt(2)
    val amp = 2 * Math.sqrt(2)
    // noise_power = 0.01 * fs / 2
    val noise_power = 0.01 * fs / 2
    // time = np.arange(N) / float(fs)
    val time = (BigDecimal(0.0) until N by 1.0) map ( _.toDouble / fs )
    // mod = 500*np.cos(2*np.pi*0.25*time)
    val mod = time map { t => 500*Math.cos(2*Math.PI*fmod*t) }
    // carrier = amp * np.sin(2*np.pi*3e3*time + mod)
    val carrier = time.map { t => amp * Math.sin(2*Math.PI*fcarrier*t) }
      .zip(mod)
      .map( p => p._1 + p._2)
    // noise = np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
    val norm = Distribution.Normal(0, Math.sqrt(noise_power), rg)
    val rand = norm.toStream.take(carrier.length).toArray
    //  noise *= np.exp(-time/5)
    val exp = time.map( t => math.exp(-t/5.0) )
    val noise = rand.zip(exp).map( e => e._1 * e._2)
    // x = carrier + noise
    val x = carrier.zip(noise).map( e => e._1 + e._2)

    // JPlot the signals above to check that all is ok
    // We should see a large sinusoid. When zooming into
    // this signal we see the carrier signal with noise
    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(time, mod)
    plt += plot(time, carrier)
    plt += plot(time, noise)
    plt += plot(time, x)
    plt.title = s"STFT Example"
    plt.refresh*/

    // JPlot the traditional FFT for all of the signal and see that it is
    // what is expected. We should see an initial set of very high amplitude
    // frequencies (highest at the modulation frequency of 0.25 Hz) and a
    // small yet visible "bump" of the carrier at 3kHz (use zoom).
    /*def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) = Fourier.FFT(fs, signal_length, sig)
    val duration = time.last
    Fourier.plotFourierTransform(fft, fs, duration, x.toArray, "Example")
    */

    /* Compute and plot the STFT’s magnitude. */
    // How large should be the windows?
    val samples1T = Fourier.numberOfSamplesFor1Period(fs, fcarrier)
    samples1T shouldBe 4

    // Calculate the STFT
    // Lest use the value from the original example
    // f, t, Zxx = signal.stft(x, fs, nperseg=1000)
    val (stfts , xa, cola) = Fourier.stft(x, fs, 1000, overlap=200)
    xa.length shouldBe 512 // 512 frequencies (2^9)
    stfts(0).length shouldBe 1024
    stfts.length shouldBe 125
    cola shouldBe false // ???

    // We can plot this to confirm that the STFT is correct (all of them should be the same)
    // JPlot the traditional FFT for all of the signal and see that it is
    // what is expected. We should see an initial set of very high amplitude
    // frequencies (highest at the modulation frequency of 0.25 Hz) and a
    // small yet visible "bump" of the carrier at 3kHz (use zoom).
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(0), title="Example")
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(10), title="Example")
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(50), title="Example")


    // Real signal so only take half the frequencies + DC
    val freqDomainLen = xa.length
    //println(s"freqDomainLen=$freqDomainLen")
    // Get only half of th required frequencies
    val freqs = stfts.map(_.take(freqDomainLen))
    // Calculate the absolute value of the frequency components
    val absFreq = freqs.map( Fourier.abs )

    // Now convert the SFTF to a Matrix that can be plotted (See Breeze)
    val zeros = DenseMatrix.zeros[Double](freqDomainLen, stfts.length)
    absFreq.zipWithIndex.foreach( e => zeros(::,e._2) += DenseVector(e._1) )

    // Find index of the carrier frequency
    val index = Fourier.indexOfFrequency(fs, stfts(0).length, fcarrier)
    index shouldBe 307 // this shows up correctly in the plot below

    // JPlot the STFT (indirectly)
    import breeze.plot._

    // Pick a gradient colour
    // Note that we set the minimum and maximum values of the gradient. If
    // we did not do this the carrier component at 3kHz is so small compared
    // to the 0.25Hz modulation that it would not be visible. USe the FFT
    // plots to determine a good value. Note that any value above the maximum
    // will have the same colour.
    val colors = GradientPaintScale(lower=0, upper=5000.0, gradient = PaintScale.BlueToRed)

    "plotSTFTMat(zeros, colors)" should compile
    //plotSTFTMat(zeros, colors, title = "window padding, overlap, no filters, no detrend")
  }


  it should "allow us to plot the correct spectrogram (window padding, overlap, Hann filter, no detrend)" in {
    val seed = 17399225432L
    val rg = new JDKRandomGenerator(seed)

    // see https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.stft.html

    /*
      Generate a test signal, a 2 Vrms sine wave whose frequency is slowly
      modulated around 3kHz, corrupted by white noise of exponentially
      decreasing magnitude sampled at 10 kHz.
     */
    val fs = 10.0e3
    val fmod = 0.25
    val fcarrier = 3e3
    val N = 1e5
    // amp = 2 * np.sqrt(2)
    val amp = 2 * Math.sqrt(2)
    // noise_power = 0.01 * fs / 2
    val noise_power = 0.01 * fs / 2
    // time = np.arange(N) / float(fs)
    val time = (BigDecimal(0.0) until N by 1.0) map ( _.toDouble / fs )
    // mod = 500*np.cos(2*np.pi*0.25*time)
    val mod = time map { t => 500*Math.cos(2*Math.PI*fmod*t) }
    // carrier = amp * np.sin(2*np.pi*3e3*time + mod)
    val carrier = time.map { t => amp * Math.sin(2*Math.PI*fcarrier*t) }
      .zip(mod)
      .map( p => p._1 + p._2)
    // noise = np.random.normal(scale=np.sqrt(noise_power), size=time.shape)
    val norm = Distribution.Normal(0, Math.sqrt(noise_power), rg)
    val rand = norm.toStream.take(carrier.length).toArray

    //  noise *= np.exp(-time/5)
    val exp = time.map( t => math.exp(-t/5.0) )
    val noise = rand.zip(exp).map( e => e._1 * e._2)
    // x = carrier + noise
    val x = carrier.zip(noise).map( e => e._1 + e._2)

    // JPlot the signals above to check that all is ok
    // We should see a large sinusoid. When zooming into
    // this signal we see the carrier signal with noise
    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(time, mod)
    plt += plot(time, carrier)
    plt += plot(time, noise)
    plt += plot(time, x)
    plt.title = s"STFT Example"
    plt.refresh*/

    // JPlot the traditional FFT for all of the signal and see that it is
    // what is expected. We should see an initial set of very high amplitude
    // frequencies (highest at the modulation frequency of 0.25 Hz) and a
    // small yet visible "bump" of the carrier at 3kHz (use zoom).
    /*def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) = Fourier.FFT(fs, signal_length, sig)
    val duration = time.last
    Fourier.plotFourierTransform(fft, fs, duration, x.toArray, "Example")
    */

    /* Compute and plot the STFT’s magnitude. */
    // How large should be the windows?
    val samples1T = Fourier.numberOfSamplesFor1Period(fs, fcarrier)
    samples1T shouldBe 4

    // Calculate the STFT
    // In the experiments above we see aliasing occurring due the window splits
    // (these introduce frequencies related to the start and end of the window)
    // To reduce this aliasing substantially we apply a Hann filter. The plot
    // clearly shows a significant improvement
    // Lest use the value from the original example
    // f, t, Zxx = signal.stft(x, fs, nperseg=1000)
    val (stfts , xa, cola) = Fourier.stft(x, fs, 1000, overlap=200, filter = Filter.initHann)
    xa.length shouldBe 512 // 512 frequencies (2^9)
    stfts(0).length shouldBe 1024
    stfts.length shouldBe 125
    cola shouldBe false // ???

    // We can plot this to confirm that the STFT is correct (all of them should be the same)
    // JPlot the traditional FFT for all of the signal and see that it is
    // what is expected. We should see an initial set of very high amplitude
    // frequencies (highest at the modulation frequency of 0.25 Hz) and a
    // small yet visible "bump" of the carrier at 3kHz (use zoom).
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(0), title="Example")
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(10), title="Example")
    //Fourier.plotFourier(fs, duration, time, x.toArray, xa, stfts(50), title="Example")


    // Real signal so only take half the frequencies + DC
    val freqDomainLen = xa.length
    //println(s"freqDomainLen=$freqDomainLen")
    // Get only half of th required frequencies
    val freqs = stfts.map(_.take(freqDomainLen))
    // Calculate the absolute value of the frequency components
    val absFreq = freqs.map( Fourier.abs )

    // Now convert the SFTF to a Matrix that can be plotted (See Breeze)
    val zeros = DenseMatrix.zeros[Double](freqDomainLen, stfts.length)
    absFreq.zipWithIndex.foreach( e => zeros(::,e._2) += DenseVector(e._1) )

    // Find index of the carrier frequency
    val index = Fourier.indexOfFrequency(fs, stfts(0).length, fcarrier)
    index shouldBe 307 // this shows up correctly in the plot below

    // JPlot the STFT (indirectly)
    import breeze.plot._

    // Pick a gradient colour
    // Note that we set the minimum and maximum values of the gradient. If
    // we did not do this the carrier component at 3kHz is so small compared
    // to the 0.25Hz modulation that it would not be visible. USe the FFT
    // plots to determine a good value. Note that any value above the maximum
    // will have the same colour.
    val colors = GradientPaintScale(lower=0, upper=5000.0, gradient = PaintScale.BlueToRed)

    "plotSTFTMat(zeros, colors)" should compile
    //plotSTFTMat(zeros, colors, title = "title = window padding, overlap, Hann filters, no detrend")
  }

  "Detrending" should "show how regression detrending allows correct frequency component identification" in {

    // Generate a sine wave
    val fs = 100.0e3 // 100 kHz sampling, just t make plots neat
    val frequency = 3.0e3 // carrier signal period
    val amplitude = 0.1
    val phase = Math.toRadians(-45)
    // Get domain for N periods of the sampled signal
    val N = 15
    val (d,time) = Fourier.samplingSpecification(fs,(1.0/frequency)*N)

    // Modulation signal (near linear trend)
    val last = time.last
    val mod = time map { i => Math.exp(i/last)}

    // Modulate carrier
    //  amplitude * Math.sin((2 * Math.PI * frequency * t) + phase)
    val carrier = time.map { t => amplitude * Math.sin((2*Math.PI*frequency*t) + phase) }
      .zip(mod)
      .map( p => p._1 + p._2)
    // Add noise
    val noiseAmp = amplitude / 10.0
    val norm = Distribution.Normal(0, noiseAmp, new JDKRandomGenerator(98765))
    val noise = norm.toStream.take(carrier.length)
    val y = carrier.zip(noise).map( p => p._1 + p._2)

    // Detrend the signal
    val yy = Transform.regressionDetrend(time.toArray, y.toArray)

    /*
    // JPlot the signals above to check that all is ok
    // We should see a large sinusoid. When zooming into
    // this signal we see the carrier signal with noise
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(time, mod)
    plt += plot(time, carrier)
    //plt += plot(time, noise)
    plt += plot(time, y)
    plt += plot(time, yy)
    plt.title = s"Detrend Example"
    plt.refresh*/

    // JPlot the traditional FFT for all of the signal and see that it is
    // what is expected. We should see an initial set of very high amplitude
    // frequencies (highest at the modulation frequency of 0.25 Hz) and a
    // small yet visible "bump" of the carrier at 3kHz (use zoom).
    def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) = Fourier.FFT(fs, signal_length, sig)
    //val duration = time.last
    //Fourier.plotFourierTransform(fft, fs, duration, y.toArray, "Example Y")
    //Fourier.plotFourierTransform(fft, fs, duration, yy.toArray, "Example YY")

    // Lets get the maximum frequency component of YY (the detrended signal)
    // Calculate the FFT
    val (len2, xat, sig2) = fft(fs, yy.length, yy.toArray, pad = false)
    // Get the maximum component
    val (sig2Abs, _) = Fourier.topAbsoluteComponents(1, sig2)
    //println(s"sig2Abs = ${sig2Abs.mkString(",")}")
    // What is the frequency of the component?
    val freqSig2 = Fourier.frequencyOfIndex(fs, len2, sig2Abs(0)._2)
    // Close enough
    freqSig2 shouldBe (frequency +- 125)

    // What about signal that has not been detrended?
    val (_, xas, sig1) = fft(fs, y.length, y.toArray, pad = false)
    // Get the maximum component
    val (sig1Abs, _) = Fourier.topAbsoluteComponents(3, sig1)
    //println(s"sig1Abs = ${sig1Abs.mkString(",")}")
    // What is the frequency of the component?
    val freqSig1 = Fourier.frequencyOfIndex(fs, xat.length, sig1Abs(0)._2)
    // Not close enough
    freqSig1 should not be (frequency +- 125)
    val freqSig13 = Fourier.frequencyOfIndex(fs, xat.length, sig1Abs(2)._2)
    // Close enough
    freqSig13 should not be (frequency +- 125)


      // How large should be the windows?
    val samples1T = Fourier.numberOfSamplesFor1Period(fs, frequency)
    samples1T shouldBe 34

    // Calculate the STFT
    // pad so that > 34 windows become 128 windows
    val (stfts , xa, cola) = Fourier.stft(yy, fs, 100) //, overlap=200, filter = Filter.initHann)
    xa.length shouldBe 64 // 128 frequencies (2^8) - (real signal, half the samples)
    stfts(0).length shouldBe 128
    stfts.length shouldBe 5
    cola shouldBe true

    // Real signal so only take half the frequencies + DC
    val freqDomainLen = xa.length
    //println(s"freqDomainLen=$freqDomainLen")
    // Get only half of th required frequencies
    val freqs = stfts.map(_.take(freqDomainLen))
    // Calculate the absolute value of the frequency components
    val absFreq = freqs.map( Fourier.abs )

    // Find index of the carrier frequency
    val index = Fourier.indexOfFrequency(fs, stfts(0).length, frequency)
    index shouldBe 4 // this shows up correctly in the plot below
    // Lets find the top frequency at about is point
    val initial = 2
    val rest = absFreq(0).drop(initial)
    val topFreq = rest.max
    // Frequency at the carrier index should be the same as the maximum we found here
    absFreq(0)(index) shouldBe topFreq
    // Now go the other way, find the index at which we have the maximum frequency
    val maxI = rest.zipWithIndex.maxBy(_._1)._2 + initial
    // Should be the same as the initial one we expect
    index shouldBe maxI

    // Now convert the SFTF to a Matrix that can be plotted (See Breeze)
    val zeros = DenseMatrix.zeros[Double](freqDomainLen, stfts.length)
    absFreq.zipWithIndex.foreach( e => zeros(::,e._2) += DenseVector(e._1) )
    //val maxAbs = max(zeros)

    // JPlot the STFT (indirectly)
    import breeze.plot._

    // Pick a gradient colour
    // Note that we set the minimum and maximum values of the gradient. If
    // we did not do this the carrier component at 3kHz is so small compared
    // to the 0.25Hz modulation that it would not be visible. USe the FFT
    // plots to determine a good value. Note that any value above the maximum
    // will have the same colour.
    //val colors = GradientPaintScale(lower=0, upper=maxAbs, gradient = PaintScale.BlueToRed)
    "val maxAbs = max(zeros) \n" +
    "val colors = GradientPaintScale(lower=0, upper=maxAbs, gradient = PaintScale.BlueToRed) \n" +
      "plotSTFTMat(zeros, colors)" should compile
    //plotSTFTMat(zeros, colors, title = "title = window padding, overlap, Hann filters, no detrend")

  }


  "Detrending" should "show how difference detrending allows correct frequency component identification" in {

    // Generate a sine wave
    val fs = 100.0e3 // 100 kHz sampling, just t make plots neat
    val frequency = 3.0e3 // carrier signal period
    val amplitude = 0.1
    val phase = Math.toRadians(-45)
    // Get domain for N periods of the sampled signal
    val N = 15
    val (d,time) = Fourier.samplingSpecification(fs,(1.0/frequency)*N)

    // Modulation signal (near linear trend)
    val last = time.last
    val mod = time map { i => Math.exp(i/last)}

    // Modulate carrier
    //  amplitude * Math.sin((2 * Math.PI * frequency * t) + phase)
    val carrier = time.map { t => amplitude * Math.sin((2*Math.PI*frequency*t) + phase) }
      .zip(mod)
      .map( p => p._1 + p._2)
    // Add noise
    val noiseAmp = amplitude / 10.0
    val norm = Distribution.Normal(0, noiseAmp, new JDKRandomGenerator(98765))
    val noise = norm.toStream.take(carrier.length)
    val y = carrier.zip(noise).map( p => p._1 + p._2)

    // Detrend the signal
    // NOTE: this form of detrending accentuates the noise related components
    val yy = Transform.differenceDetrend(time.toArray, y.toArray) :+ 0.0

    /*
    // JPlot the signals above to check that all is ok
    // We should see a large sinusoid. When zooming into
    // this signal we see the carrier signal with noise
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(time, mod)
    plt += plot(time, carrier)
    //plt += plot(time, noise)
    plt += plot(time, y)
    plt += plot(time, yy)
    plt.title = s"Detrend Example"
    plt.refresh*/

    // JPlot the traditional FFT for all of the signal and see that it is
    // what is expected. We should see an initial set of very high amplitude
    // frequencies (highest at the modulation frequency of 0.25 Hz) and a
    // small yet visible "bump" of the carrier at 3kHz (use zoom).
    def fft(fs: Double, signal_length: Int, sig: Array[Double], pad: Boolean) = Fourier.FFT(fs, signal_length, sig)
    //val duration = time.last
    //Fourier.plotFourierTransform(fft, fs, duration, y.toArray, "Example Y")
    //Fourier.plotFourierTransform(fft, fs, duration, yy.toArray, "Example YY")

    // Lets get the maximum frequency component of YY (the detrended signal)
    // Calculate the FFT
    val (len2, xat, sig2) = fft(fs, yy.length, yy.toArray, pad = false)
    // Get the maximum component
    val (sig2Abs, _) = Fourier.topAbsoluteComponents(1, sig2)
    //println(s"sig2Abs = ${sig2Abs.mkString(",")}")
    // What is the frequency of the component?
    val freqSig2 = Fourier.frequencyOfIndex(fs, len2, sig2Abs(0)._2)
    // Close enough
    freqSig2 shouldBe (frequency +- 125)

    // What about signal that has not been detrended?
    val (_, xas, sig1) = fft(fs, y.length, y.toArray, pad = false)
    // Get the maximum component
    val (sig1Abs, _) = Fourier.topAbsoluteComponents(3, sig1)
    //println(s"sig1Abs = ${sig1Abs.mkString(",")}")
    // What is the frequency of the component?
    val freqSig1 = Fourier.frequencyOfIndex(fs, xat.length, sig1Abs(0)._2)
    // Not close enough
    freqSig1 should not be (frequency +- 125)
    val freqSig13 = Fourier.frequencyOfIndex(fs, xat.length, sig1Abs(2)._2)
    // Close enough
    freqSig13 should not be (frequency +- 125)


    // How large should be the windows?
    val samples1T = Fourier.numberOfSamplesFor1Period(fs, frequency)
    samples1T shouldBe 34

    // Calculate the STFT
    // pad so that > 34 windows become 128 windows
    val (stfts , xa, cola) = Fourier.stft(yy, fs, 100) //, overlap=200, filter = Filter.initHann)
    xa.length shouldBe 64 // 128 frequencies (2^8) - (real signal, half the samples)
    stfts(0).length shouldBe 128
    stfts.length shouldBe 5
    cola shouldBe true

    // Real signal so only take half the frequencies + DC
    val freqDomainLen = xa.length
    //println(s"freqDomainLen=$freqDomainLen")
    // Get only half of th required frequencies
    val freqs = stfts.map(_.take(freqDomainLen))
    // Calculate the absolute value of the frequency components
    val absFreq = freqs.map( Fourier.abs )

    // Find index of the carrier frequency
    val index = Fourier.indexOfFrequency(fs, stfts(0).length, frequency)
    index shouldBe 4 // this shows up correctly in the plot below
    // Lets find the top frequency at about is point
    val initial = 2
    val rest = absFreq(0).drop(initial)
    val topFreq = rest.max
    // Frequency at the carrier index should be the same as the maximum we found here
    absFreq(0)(index) shouldBe topFreq
    // Now go the other way, find the index at which we have the maximum frequency
    val maxI = rest.zipWithIndex.maxBy(_._1)._2 + initial
    // Should be the same as the initial one we expect
    index shouldBe maxI

    // Now convert the SFTF to a Matrix that can be plotted (See Breeze)
    val zeros = DenseMatrix.zeros[Double](freqDomainLen, stfts.length)
    absFreq.zipWithIndex.foreach( e => zeros(::,e._2) += DenseVector(e._1) )
    //val maxAbs = max(zeros)

    // JPlot the STFT (indirectly)
    import breeze.plot._

    // Pick a gradient colour
    // Note that we set the minimum and maximum values of the gradient. If
    // we did not do this the carrier component at 3kHz is so small compared
    // to the 0.25Hz modulation that it would not be visible. USe the FFT
    // plots to determine a good value. Note that any value above the maximum
    // will have the same colour.
    //val colors = GradientPaintScale(lower=0, upper=maxAbs, gradient = PaintScale.BlueToRed)
    "val maxAbs = max(zeros) \n" +
    "val colors = GradientPaintScale(lower=0, upper=maxAbs, gradient = PaintScale.BlueToRed) \n" +
    "plotSTFTMat(zeros, colors)" should compile
    //plotSTFTMat(zeros, colors, title = "title = window padding, overlap, Hann filters, no detrend")

  }

}
