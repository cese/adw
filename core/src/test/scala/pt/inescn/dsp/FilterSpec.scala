/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.dsp

import java.lang.{System => JSystem}
import org.scalatest._

import better.files.File
import org.hipparchus.complex.Complex
import pt.inescn.dsp.Filter.{applyFilterBank, filterBankIndexes, getFilterBank, linearToLinear}
import pt.inescn.samplers.Function

/**
  * Contains tests for the DSP related functions. Also used to test some basic DSP library routines.
  *
  * Created by hmf on 12-07-2017.
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.dsp.FilterSpec"
  *
  */
class FilterSpec extends FlatSpec with Matchers {

  val eps = 0.3

  /**
    * Sanity checking // https://github.com/berndporr/iirj
    */
  "Envelope filters" should "uses correct Butterworth output" in {
    import better.files.Dsl._


    val cl = classOf[javax.sound.sampled.AudioSystem].getClassLoader
    val old = Thread.currentThread.getContextClassLoader

    try {
      Thread.currentThread.setContextClassLoader(cl)
      // codec: Uncompressed 16-bit PCM audio
      // channels: Stereo
      // sampling rate: 44100 Hz
      // Bitrate : N/A

      // /home/hmf/git/adw/data/audio
      val audio = cwd / ".." / "data/audio"
      val sample5 = audio / "canary" / "Canary_singing_master_selec_01_sub_sel_2.wav"

      val audioData = Audio.open(sample5)
      //Audio.printFileFormat(audioData)
      audioData.isDefined shouldBe true

      //val data = audioData.get._2
      val Some((channels, idx)) = Audio.load(sample5)
      channels.length shouldBe 2

      // Test filter via Octave (see main/src/octave/filter1.m)
      // Also with https://www-users.cs.york.ac.uk/~fisher/cgi-bin/mkfscript
      // None produced the same filter coefficients! In fact the above testes used less coefficients
      // So test another way
      // Set the sampling frequency used by our digital filtering system:
      val ofs = 8000.0
      val ofs2 = ofs / 2.0

      // Start designing a butterworth filter, passband. (In Hz)
      val pass_lo = 300.0
      val pass_hi = 600.0

      // The order of the filter
      val order = 4

      // Determine the low and high frequencies of the pass-band as fractions of the sampling rate:
      //val flo = pass_lo/ofs
      //val fhi = pass_hi/ofs

      // Use the butterworth filter design function to get the coefficients for a bandpass filter with the settings above:
      // [b,a] = butter(order, [flo fhi])
      import uk.me.berndporr.iirj.Butterworth
      val butterworth = new Butterworth
      val notch = pass_hi - pass_lo
      val centerFs = (pass_lo / 2.0) + notch
      centerFs shouldBe 450
      notch shouldBe 300
      butterworth.bandPass(order, ofs, centerFs, notch)
      butterworth.getNumBiquads shouldBe 4
      butterworth.getBiquad(0).getA0 shouldBe 1.0
      butterworth.getBiquad(0).getA1 shouldBe (-1.790 +- eps)
      butterworth.getBiquad(0).getA2 shouldBe (0.9496 +- eps)

      // Push various sines waves through and confirm the expected gain
      val f1 = 200 /* Hz */ ; val a1 = 1.0 ; val phase1 = Math.toRadians(0)
      val f2 = 300 /* Hz */ ; val a2 = 1.0 ; val phase2 = Math.toRadians(0)
      val f3 = 450 /* Hz */ ; val a3 = 1.0 ; val phase3 = Math.toRadians(0)
      val f4 = 600 /* Hz */ ; val a4 = 1.0 ; val phase4 = Math.toRadians(0)
      val f5 = 700 /* Hz */ ; val a5 = 1.0 ; val phase5 = Math.toRadians(0)

      val (dd,xx) = Fourier.samplingSpecification(ofs,2)

      val sig1 = Function.Sin(f1, a1, phase1, dd)
      val sig2 = Function.Sin(f2, a2, phase2, dd)
      val sig3 = Function.Sin(f3, a3, phase3, dd)
      val sig4 = Function.Sin(f4, a4, phase4, dd)
      val sig5 = Function.Sin(f5, a5, phase5, dd)

      sig1(xx.length).max shouldBe 1.0
      sig2(xx.length).max shouldBe 1.0
      sig3(xx.length).max shouldBe 1.0
      sig4(xx.length).max shouldBe 1.0
      sig5(xx.length).max shouldBe 1.0

      val fs1 = sig1(xx.length).map( butterworth.filter )
      val fs2 = sig2(xx.length).map( butterworth.filter )
      val fs3 = sig3(xx.length).map( butterworth.filter )
      val fs4 = sig4(xx.length).map( butterworth.filter )
      val fs5 = sig5(xx.length).map( butterworth.filter )

      // IMPORTANT: initial values are incorrect due to transient states, so drop that
      val mfs1 = fs1.drop(fs1.length/4).max
      val mfs2 = fs2.drop(fs1.length/4).max
      val mfs3 = fs3.drop(fs1.length/4).max
      val mfs4 = fs4.drop(fs1.length/4).max
      val mfs5 = fs5.drop(fs1.length/4).max

      Fourier.todB(mfs1) shouldBe (-29.25 +-eps)  // Ok, less than -3dB, note that frequency response is NOT symmetric
      Fourier.todB(mfs2) shouldBe (-3.0 +- eps)  // Ok, band start
      Fourier.todB(mfs3) shouldBe (0.0 +- eps)   // Ok, band
      Fourier.todB(mfs4) shouldBe (-3.01 +- eps)  // Ok, band stop
      Fourier.todB(mfs5) shouldBe (-14.01 +- eps) // Ok, expected less than -3 dB

      // Use the response plot to get gain
      // Note: plot normalized to sampling frequency
      val freqs = BigDecimal(0.0) to 1 by 1.0/ofs map( _.toDouble )
      val response = freqs.map { e =>
        val tmp = butterworth.response(e)
        new Complex(tmp.getReal, tmp.getImaginary)
      }
      // Get 3 samples - band limits plus bandpass center
      val realRespnse = response.slice(0, response.length / 2)
      val n = realRespnse.length
      val idx_lo = ((pass_lo  * n) / ofs2 ).toInt
      val idx_center = ((centerFs * n)  / ofs2).toInt
      val idx_hi = ((pass_hi  * n) / ofs2).toInt

      // Get gain from  these 3 samples
      val startBand = Fourier.todB( realRespnse( idx_lo ).abs )
      val centerBand = Fourier.todB( realRespnse( idx_center ).abs )
      val stopBand = Fourier.todB( realRespnse( idx_hi ).abs )
      startBand shouldBe (-3.0 +- eps) // Ok, as above
      centerBand shouldBe (0.0 +- eps) // Ok, as above
      stopBand shouldBe (-3.0 +- eps) // Ok, as above

      // Make file name empty to see plot. Note that markers only appear in screen, not saved file
      if (!java.awt.GraphicsEnvironment.isHeadless) {
        val tmp = File(JSystem.getProperty("java.io.tmpdir"))
        val file = tmp/"testbutterworth1.png"
        val fig = Fourier.plotMagnitudePhase(ofs, realRespnse.toArray, "Butterworth response", file.toString)
        // Show bandwidth
        import org.jfree.chart.plot.ValueMarker
        val plt = fig.subplot(0)
        plt.plot.addDomainMarker(new ValueMarker(pass_lo / ofs))
        plt.plot.addDomainMarker(new ValueMarker(pass_hi / ofs))
        plt.plot.addRangeMarker(new ValueMarker(Fourier.dBtoLin(-3)))
        file.exists shouldBe true
        file.delete(true)
      }

    } finally
      Thread.currentThread.setContextClassLoader(old)
  }

  "Filter bank" should "generate the correct results" in {

    // sample at 4Hz, FFT positive side has 0Hz to 2Hz
    val sampleRate = 1.0 / 4
    // Make as FFT of 12 (should be 16, power of 2)
    val windowLen = 12
    // Want 3 filters to cover bandwidth from 0 to 2 Hz
    val numFilters = 3
    val minFreq = 0
    val maxFreq = 2
    val indexes = filterBankIndexes(linearToLinear)(windowLen, sampleRate, numFilters, minFreq, maxFreq)
    //println(indexes)
    indexes should contain theSameElementsInOrderAs Vector((0,2,5), (2,5,8), (5,8,11))


    val filterBank = getFilterBank(indexes, Filter.initHann)
    //println(filterBank.mkString(","))
    filterBank.size shouldBe 3
    filterBank(0)._3.size shouldBe 6
    filterBank(1)._3.size shouldBe 7
    filterBank(2)._3.size shouldBe 7

    val sample = Array(1.0,2,3,4,5,6,7,8,9,10,11,12)
    val sampleFeatures = applyFilterBank(filterBank)(sample)
    //println(sampleFeatures.mkString(";\n"))
    sampleFeatures.length shouldBe 3
    sampleFeatures(0) should contain theSameElementsInOrderAs Vector(0.0, 0.6909830056250525, 2.713525491562421, 3.618033988749895, 1.7274575140626318, 0.0)
    sampleFeatures(1) should contain theSameElementsInOrderAs Vector(0.0, 0.9999999999999998, 3.7499999999999996, 6.0, 5.250000000000002, 1.9999999999999996, 0.0)
    sampleFeatures(2) should contain theSameElementsInOrderAs Vector(0.0, 1.7499999999999996, 5.999999999999999, 9.0, 7.500000000000002, 2.7499999999999996, 0.0)
  }


}
