package pt.inescn.samplers.stream

import org.scalatest._
import java.time.{Duration, Instant}

import org.hipparchus.random.JDKRandomGenerator
import pt.inescn.dsp.Fourier
import pt.inescn.features.Stats
import pt.inescn.samplers.stream.Ops.{cartesian, _}

import scala.annotation.tailrec
import scala.util.Random


/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "root/testOnly pt.inescn.samplers.stream.OpsSpec"
  *
  * Created by hmf on 02-10-2018.
  */
class OpsSpec extends FlatSpec with Matchers {

  "Combination operators" should "generate sample iterators" in {

    // Cartesian product
    // Its is commutative but resulting tuple order varies

    val pi1s = 1 to 2 by 1
    val pi2s = 1 to 3 by 1
    val pis1 = cartesian(pi1s, pi2s)
    val p1s1l = pis1.toList
    val expected1 = List((1,1),(2,1),(1,2),(2,2),(1,3),(2,3))
    p1s1l should contain theSameElementsAs expected1

    // Cartesian product using operator
    val pis1op = pi1s ## pi2s
    pis1op should contain theSameElementsAs expected1

    // Combing an empty sampler with any other samples results in an empty sample
    val pi0s = Ops.empty[Double]
    val pis2 = cartesian(pi0s, pi2s)
    val p1s2l = pis2.toList
    p1s2l.size shouldBe 0

    // same as above but with operators
    val pis2op = pi0s ## pi1s ## pi2s
    pis2op.size shouldBe 0

    // We can combine samples of any type
    // We get arbitrarily nested tuples as a result
    val pi3s = 'a' to 'c' by 1
    val pis3 = cartesian(pis1, pi3s)
    val pis3l = pis3.toList
    val expected2 = List(
      ((1,1),'a'),((2,1),'a'),((1,2),'a'),((2,2),'a'),((1,3),'a'),((2,3),'a'),
      ((1,1),'b'),((2,1),'b'),((1,2),'b'),((2,2),'b'),((1,3),'b'),((2,3),'b'),
      ((1,1),'c'),((2,1),'c'),((1,2),'c'),((2,2),'c'),((1,3),'c'),((2,3),'c')
    )
    pis3l should contain theSameElementsAs expected2

    // Same as above but with an operator
    val pis3op = pis1 ## pi3s
    pis3op should contain theSameElementsAs expected2

    // And operator
    // Sampling in parallel (as long as the longest sampler)
    // [1,2] and [a,b,c] -> [(1,a),(2,b)]

    val pis4op = pi1s && pi3s
    pis4op should contain theSameElementsAs List((1,'a'),(2,'b'))

    // And operator is commutative resulting tuple order varies)

    val pis5op = pi3s && pi1s
    pis5op should contain theSameElementsAs List(('a',1),('b',2))

    // Or operator
    // Keeps combining the samplers in parallel as long as at
    // least one element in a sub-sampler has not been visited

    val pis6op = pi1s !! pi3s
    pis6op should contain theSameElementsAs List((1,'a'), (2,'b'), (1,'c'), (2,'a'), (1,'b'), (2,'c'))

    // Commutative (save for the tuple sizes)

    val pis7op = pi3s !! pi1s
    pis7op should contain theSameElementsAs List(('a',1), ('b',2), ('c',1), ('a',2), ('b',1), ('c',2))
  }

  "Enumeration" should "allow generating ranges" in {

    val r1 = Enumeration.range(0.0, 10, 1)
    r1 should contain theSameElementsAs List(0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0)

    val r2 = Enumeration.from(0.0, 1.0) take 11
    r2 should contain theSameElementsAs List(0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0)

    val r3 = Enumeration.continually(1.0) take 11
    r3 should contain theSameElementsAs List(1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1)

    val r4 = Enumeration.continually(1.0) map(i => i + 1.0) take 11
    r4 should contain theSameElementsAs List(2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2)

    val r5 = Enumeration.range(0, 10)
    r5 should contain theSameElementsAs List(0,1,2,3,4,5,6,7,8,9,10)

    val r6 = Enumeration.from(0) take 11
    r6 should contain theSameElementsAs List(0,1,2,3,4,5,6,7,8,9,10)

    val r7 = Enumeration.continually(1) take 11
    r7 should contain theSameElementsAs List(1,1,1,1,1,1,1,1,1,1,1)

    val r8 = Enumeration.continually(1) map(i => i + 1) take 11
    r8 should contain theSameElementsAs List(2,2,2,2,2,2,2,2,2,2,2)

  }

  it should "produce a series of timestamps" in {

    val s1 = Fourier.convertFractionSecondsToDuration(0.1)
    val r1 = Duration.ofMillis(100)
    s1 shouldBe r1
    val s2 = Fourier.convertFractionSecondsToDuration(0.9)
    val r2 = Duration.ofMillis(900)
    s2 shouldBe r2
    val s3 = Fourier.convertFractionSecondsToDuration(0.999)
    val r3 = Duration.ofMillis(999)
    s3 shouldBe r3
    val s4 = Fourier.convertFractionSecondsToDuration(0.099)
    val r4 = Duration.ofMillis(99)
    s4 shouldBe r4
    val s5 = Fourier.convertFractionSecondsToDuration(0.0001)
    val r5 = Duration.ofNanos(100*1000) // micro
    s5 shouldBe r5
    val s6 = Fourier.convertFractionSecondsToDuration(0.000999)
    val r6 = Duration.ofNanos(999*1000) // micro
    s6 shouldBe r6
    val s7 = Fourier.convertFractionSecondsToDuration(0.000000999)
    val r7 = Duration.ofNanos(999)
    s7 shouldBe r7

    val startInstant = "2017-06-16T11:35:54.100000Z"
    val start = Instant.parse(startInstant)
    val t1 = Instant.parse("2017-06-16T11:35:54.102Z")
    val t9 = Instant.parse("2017-06-16T11:35:54.118Z")
    val (dd,_) = Fourier.samplingSpecification(500,2)
    val delta = Fourier.convertFractionSecondsToDuration(dd)
    val len = 10
    val y = Enumeration.from(start, delta).take(len).toSeq
    y.length shouldBe len
    y.head should be(start)
    y(1) should be(t1)
    y.last should be (t9)

    // TODO: how do we plot time-series with timestamps?
    /*
    val z = Enumeration.continually(1.0).take(len).toSeq
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    //plt += plot(x.take(len), z)
    //plt += plot(y.toList, z.toList)
    plt += plot(y, z)
    plt.title = "Time stamp"
    plt.refresh*/
  }

  val eps = 0.17 // 0.17 // 0.12

  // Real distributions

  "Real distribution" should "produce a Gaussian samples" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val y1: Seq[Double] = Distribution.normal(0,1.0).take(500).toSeq
    y1.length shouldBe 500
    val stats = Stats.DescriptiveStats.batch(y1)
    stats.mean.value should be(0.0 +- eps)
    stats.standard_deviation.value should be(1.0 +- eps)
    stats.skewness.value should be(0.0 +- eps)
    //stats.kurtosis.value should be(0.0 +- eps)

    /*
    import pt.inescn.plot._
    val x = (1 to y1.length).map(_.toDouble)
    Plot.showPlot(x, y1)
    Plot.showHistogram(y1, 7)
    */
  }


  /**
    * https://en.wikipedia.org/wiki/Uniform_distribution_(continuous)
    */
  it should "produce a Uniform distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val a = 0
    val b = 1

    val y = Distribution.uniform(a, b).take(1000).toSeq.sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)

    //
    val mean = (a + b) / 2.0
    val median = (a + b) / 2.0
    val sd = Math.sqrt( Math.pow(b - a,2) / 12.0)
    val kurtosis = -6.0/5.0
    val skewness = 0.0
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be (median +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 7)
    */
  }

  /**
    * https://en.wikipedia.org/wiki/Gamma_distribution
    */
  it should "produce a Gamma distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val shape = 9.0
    val scale = 0.5
    // NOTE: using the JDKRandomGenerator generates different results
    val y = Distribution.gamma(shape, scale).take(10000).toSeq.sorted  // need to sort for median stats
    y.length shouldBe 10000
    val stats = Stats.DescriptiveStats.batch(y)
    val tmean = shape * scale
    val sd = Math.sqrt( shape * scale * scale )
    val skewness = 2.0 / Math.sqrt(shape)
    val kurtosis = 6.0 / shape
    stats.mean.value should be(tmean +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps) // ??
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }


  /**
    * https://en.wikipedia.org/wiki/Log-normal_distribution
    */
  it should "produce a LogNormal distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val shape = 0.25 // standard deviation
    val scale = 0.05 // mean parameter
    // (new JDKRandomGenerator(9876))
    val y = Distribution.logNormal(shape, scale).take(1000).toSeq.sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = Math.exp(scale + ((shape*shape)/2.0) )
    val median = Math.exp(scale)
    val sd = Math.sqrt(Math.exp(shape*shape)-1)*Math.exp((2*scale)+(shape*shape))
    val kurtosis = Math.exp(4*shape*shape) + 2*Math.exp(3*shape*shape) + 3*Math.exp(2*shape*shape) - 6
    val skewness = (Math.exp(shape*shape)+2)*Math.sqrt(Math.exp(shape*shape)-1)
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be(median +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps) // ??
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }


  /**
    * https://en.wikipedia.org/wiki/Exponential_distribution
    *
    * BUG: Kurtosis and skewness
    */
  it should "produce an Exponential distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val pmean = 0.5
    val y = Distribution.exponential(pmean).take(1000).toSeq.sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = pmean
    val median = pmean * Math.log(2)
    val sd = Math.sqrt(pmean*pmean)
    //val kurtosis = 6.0
    //val skewness = 2.0
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be(median +- eps) // ??
    stats.standard_deviation.value should be(sd +- eps)
    //stats.kurtosis.value should be(kurtosis +- eps)
    //stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }

  /**
    * https://en.wikipedia.org/wiki/Beta_distribution
    */
  it should "produce a Beta distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val alpha = 2.0
    val beta = 2.0
    val y = Distribution.beta(alpha, beta).take(1000).toSeq.sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = alpha / (alpha + beta)
    val median = (alpha - (1/3.0))/ (alpha + beta - (2/3.0) )   // alpha and beta > 1
    val sd = Math.sqrt((alpha*beta)/(Math.pow(alpha+beta,2)*(alpha+beta+1.0)))
    val kurtosis = 6*((Math.pow(alpha-beta,2)*(alpha+beta+1))-((alpha*beta)*(alpha+beta+2)))/((alpha*beta)*(alpha+beta+2)*(alpha+beta+3))
    val skewness = (2.0*(beta-alpha)*Math.sqrt(alpha+beta+1)) / ((alpha+beta+2)*Math.sqrt(alpha*beta))
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be(median +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }

  // Integer samples

  /**
    * Approximation of the Riemann Zeta Function
    * @param s - parameter of function
    * @return an approximation of the Riemann Zeta Function (not infinite sum but 100000 elements)
    */
  def riemannZetaFunction(s:Int) : Double = {
    @tailrec
    def loop(i: Int, acc: Double) : Double = {
      if (i==0)
        acc
      else
        loop(i-1, acc + (1.0/Math.pow(i,s)))
    }
    loop(100000,0.0)
  }

  /**
    * https://en.wikipedia.org/wiki/Zeta_distribution
    */
  "Integer Distributions" should "produce a Zipf distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val numberOfElements = 5 // K
    val exp = 4  // s
    val y = Distribution.zipf(numberOfElements, exp).take(1000).toSeq.sorted  // need to sort for median stats
    //val x = 1 to y.length
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y.map(_.toDouble))
    def gh = riemannZetaFunction _
    val mean = gh(exp-1) / gh(exp)  // for exp > 2
    val sd = Math.sqrt( (gh(exp)*gh(exp-2)) - Math.pow(gh(exp-1),2) ) / Math.pow(gh(exp),2) // for s > 3
    stats.mean.value should be(mean +- eps)
    stats.standard_deviation.value should be(sd +- eps)  // biggest error

    /*import pt.inescn.plot._
    Plot.showPlot(x.map(_.toDouble), y.map(_.toDouble))
    Plot.showHistogram(y.map(_.toDouble), 15)*/
  }

  /**
    * https://en.wikipedia.org/wiki/Poisson_distribution
    */
  it should "produce a Poisson distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val smean = 20.0
    val y = Distribution.poisson(smean).take(1000).toSeq.sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y.map(_.toDouble))
    //def gh = riemannZetaFunction _
    val mean = smean
    val sd = Math.sqrt(smean)
    val kurtosis = 1.0/smean
    val skewness = 1.0/Math.sqrt(smean)
    val median = Math.floor(smean + (1.0/3.0) - (0.02/smean))
    stats.quartile_2.value should be(median +- eps)
    stats.mean.value should be(mean +- eps)                  // second biggest error
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)

    val lower_median = mean - Math.log(2)
    val upper_median = mean + (1.0 / 1.3)
    stats.quartile_2.value should be >= lower_median
    stats.quartile_2.value should be < upper_median

    /*import pt.inescn.plot._
    val x = 1 to y.length
    Plot.showPlot(x.map(_.toDouble), y.map(_.toDouble))
    Plot.showHistogram(y.map(_.toDouble), 15)*/
  }

  /**
    * https://en.wikipedia.org/wiki/Uniform_distribution_(continuous)
    */
  it should "produce an Int Uniform distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val a = 0
    val b = 1

    val n = 1000
    val y = Distribution.intUniform(a, b).take(n).map(_.toDouble).toSeq.sorted  // need to sort for median stats
    y.length shouldBe n
    val stats = Stats.DescriptiveStats.batch(y)

    //
    val mean = (a + b) / 2.0
    val median = y(n/2) //(a + b) / 2.0
    val sd = Math.sqrt( (Math.pow(b - a + 1.0,2) - 1.0)/ 12.0)
    //val kurtosis = (-6.0*(n*n+1))/(5.0*(n*n-1))
    // https://www.randomservices.org/random/special/UniformDiscrete.html
    val kurtosis = (-3.0*((3*n*n)-7))/(5.0*((n*n)-1))
    val skewness = 0.0
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be (median +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- 0.21)  // ??
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 7)*/

  }

  /**
    * https://en.wikipedia.org/wiki/Uniform_distribution_(continuous)
    */
  it should "produce a Long Uniform distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val a = 0
    val b = 1

    val n = 1000
    val y = Distribution.longUniform(a, b).take(n).map(_.toDouble).toSeq.sorted  // need to sort for median stats
    y.length shouldBe n
    val stats = Stats.DescriptiveStats.batch(y)

    //
    val mean = (a + b) / 2.0
    val median = y(n/2) //(a + b) / 2.0
    val sd = Math.sqrt( (Math.pow(b - a + 1.0,2) - 1.0)/ 12.0)
    //val kurtosis = (-6.0*(n*n+1))/(5.0*(n*n-1))
    // https://www.randomservices.org/random/special/UniformDiscrete.html
    val kurtosis = (-3.0*((3*n*n)-7))/(5.0*((n*n)-1))
    val skewness = 0.0
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be (median +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- 0.21)  // ??
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 7)*/

  }


  "Discrete Proportional Distributions" should "produce a correct discrete proportional distribution" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val len = 10000
    val denom = len / 100.0
    val eps = 1.5 // %

    val mp = Map[Int,Double](1 -> 0.1, 2 -> 0.2, 3 -> 0.7)
    val y = Distribution.proportional(mp).take(len).toSeq

    val gen1_classes = y.groupBy( p => p ).map( p => (p._1, p._2.length) )
    //println(gen1_classes)
    val gen1_class1: Int = gen1_classes(1)
    val gen1_class2: Int = gen1_classes(2)
    val gen1_class3: Int = gen1_classes(3)
    assertThrows[java.util.NoSuchElementException] { gen1_classes(4) }

    /*
    println(s"1 : $gen1_class1 = ${gen1_class1 / denom}")
    println(s"2 : $gen1_class2 = ${gen1_class2 / denom}")
    println(s"3 : $gen1_class3 = ${gen1_class3 / denom}")
    */

    (gen1_class1 / denom) shouldBe (10.0 +- eps)
    (gen1_class2 / denom) shouldBe (20.0 +- eps)
    (gen1_class3 / denom) shouldBe (70.0 +- eps)
  }

  it should "produce a 50% discrete proportional distribution correctly" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val len = 10000
    val denom = len / 100.0
    val eps = 1.5 // %

    val mp = Map[Int, Double](1 -> 0.50, 2 -> 0.50)
    val y = Distribution.proportional(mp).take(len).toSeq

    val gen1_classes = y.groupBy( p => p ).map( p => (p._1, p._2.length) )
    //println(gen1_classes)
    val gen1_class1: Int = gen1_classes(1)
    val gen1_class2: Int = gen1_classes(2)
    assertThrows[java.util.NoSuchElementException] { gen1_classes(3) }

    /*
    println(s"1 : $gen1_class1 = ${gen1_class1 / denom}")
    println(s"2 : $gen1_class2 = ${gen1_class2 / denom}")
    */


    (gen1_class1 / denom) shouldBe (50.0 +- eps)
    (gen1_class2 / denom) shouldBe (50.0 +- eps)
  }

  // Some functions

  "Function sampler" should "produce a sine wave" in {
    import pt.inescn.features._

    // Period of 1 cycle = 0.5 sec
    val frequency = 2
    val amplitude = 1.0
    val phase = Math.toRadians(-45)

    // If one cycle is 0.5 sec, then in 1.0 sec we have 2 cycles
    // Lets sample at 10 times the signal frequency, increases sampling resolution only
    val (_,x) = Fourier.samplingSpecification(10*frequency,1.0)
    val sinF = Function.sin(frequency, amplitude, phase) _
    val y = x.map(sinF)

    // Lest check output
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = 0.0
    val sd = amplitude/Math.sqrt(2) // Approximately RMS of signal
    //val kurtosis = 0.0
    val skewness = 0.0
    val mx = amplitude
    val mn = -amplitude
    stats.mean.value should be(mean +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    //stats.kurtosis.value should be(kurtosis +- eps) // TODO get a negative value?
    stats.skewness.value should be(skewness +- eps)
    stats.max.value should be(mx +- eps)
    stats.min.value should be(mn +- eps)
    stats.quartile_2.value should be (mean +- eps)

    /*import pt.inescn.plot._
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }

  it should "produce a cosine wave" in {
    import pt.inescn.features._

    // Period of 1 cycle = 0.5 sec
    val frequency = 2
    val amplitude = 1.0
    val phase = Math.toRadians(-45)

    // If one cycle is 0.5 sec, then in 1.0 sec we have 2 cycles
    // Lets sample at 10 times the signal frequency, increases sampling resolution only
    val (_,x) = Fourier.samplingSpecification(10*frequency,1.0)
    val sinF = Function.cos(frequency, amplitude, phase) _
    val y = x.map(sinF)

    // Lest check output
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = 0.0
    val sd = amplitude/Math.sqrt(2) // Approximately RMS of signal
    //val kurtosis = 0.0
    val skewness = 0.0
    val mx = amplitude
    val mn = -amplitude
    stats.mean.value should be(mean +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    //stats.kurtosis.value should be(kurtosis +- eps) // TODO get a negative value?
    stats.skewness.value should be(skewness +- eps)
    stats.max.value should be(mx +- eps)
    stats.min.value should be(mn +- eps)
    stats.quartile_2.value should be (mean +- eps)

    /*import pt.inescn.plot._
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }


  it should "produce a linear chirp wave" in {
    import pt.inescn.features._

    // Period of 1 cycle = 0.5 sec
    val frequency = 2
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val deltaAmp = 1.2
    val deltaFreq = 0.9

    // If one cycle is 0.5 sec, then in 1.0 sec we have 2 cycles
    // Lets sample at 10 times the signal frequency, increases sampling resolution only
    val (_,x) = Fourier.samplingSpecification(30*frequency,2.0)
    val chirpF = Function.linearChirp(frequency, amplitude, phase, deltaAmp, deltaFreq) _
    val y = x.map(chirpF)

    // Lest check output
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = 0.0
    val sd = 1.64        // unchecked
    val kurtosis = -0.96 // unchecked
    val skewness = -0.17 // unchecked
    val mx = 3.158       // unchecked
    val mn = -3.23       // unchecked
    val median = 0.23    // unchecked

    stats.mean.value should be(mean +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)
    stats.max.value should be(mx +- eps)
    stats.min.value should be(mn +- eps)
    stats.quartile_2.value should be (median +- eps)

    /*import pt.inescn.plot._
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }


  it should "produce a exponential chirp wave" in {
    import pt.inescn.features._

    // Period of 1 cycle = 0.5 sec
    val frequency = 2
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val deltaAmp = 1.2
    val deltaFreq = 1.5

    // If one cycle is 0.5 sec, then in 1.0 sec we have 2 cycles
    // Lets sample at 10 times the signal frequency, increases sampling resolution only
    val (_,x) = Fourier.samplingSpecification(40*frequency,2.0)
    val chirpF = Function.expChirp(frequency, amplitude, phase, deltaAmp, deltaFreq) _
    val y = x.map(chirpF)

    // Lest check output
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = 0.0
    val sd = 0.83        // unchecked
    val kurtosis = -1.41 // unchecked
    val skewness = -0.17 // unchecked
    val mx = 1.28        // unchecked
    val mn = -1.36       // unchecked
    val median = -0.02   // unchecked

    stats.mean.value should be(mean +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)
    stats.max.value should be(mx +- eps)
    stats.min.value should be(mn +- eps)
    stats.quartile_2.value should be (median +- eps)

    /*import pt.inescn.plot._
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }



  "Numeric operator" should "produce a single series of added values" in {
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (_,xx) = Fourier.samplingSpecification(500,2)
    val y1 = xx.map( Function.sin(frequency, amplitude, phase) )
    val y2 = Enumeration.continually(0.5)
    val y3 = y1 + y2

    val y1t = y1.take(xx.length)
    val y2t = y2.take(xx.length)
    val y3t = y3.take(xx.length)
    val y3r = y1t.zip(y2t).map( p => p._1 + p._2)
    y3t should contain theSameElementsAs y3r

    /*import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, y1t)
    plt += plot(xx, y2t.toSeq)
    plt += plot(xx, y3t.toSeq)
    plt.title = "Add"
    plt.refresh*/
  }

  it should "produce a single series of subtracted values" in {
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (_,xx) = Fourier.samplingSpecification(500,2)
    val y1 = xx.map( Function.sin(frequency, amplitude, phase) )
    val y2 = Enumeration.continually(0.5)
    val y3 = y1 - y2

    val y1t = y1.take(xx.length)
    val y2t = y2.take(xx.length)
    val y3t = y3.take(xx.length)
    val y3r = y1t.zip(y2t).map( p => p._1 - p._2)
    y3t should contain theSameElementsAs y3r

    /*import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, y1t)
    plt += plot(xx, y2t.toSeq)
    plt += plot(xx, y3t.toSeq)
    plt.title = "Subtract"
    plt.refresh*/
  }

  it should "produce a single series of multiplied values" in {
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (_,xx) = Fourier.samplingSpecification(500,2)
    val y1 = xx.map( Function.sin(frequency, amplitude, phase) )
    val y2 = Enumeration.continually(0.5)
    val y3 = y1 * y2

    val y1t = y1.take(xx.length)
    val y2t = y2.take(xx.length)
    val y3t = y3.take(xx.length)
    val y3r = y1t.zip(y2t).map( p => p._1 * p._2)
    y3t should contain theSameElementsAs y3r

    /*import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, y1t)
    plt += plot(xx, y2t.toSeq)
    plt += plot(xx, y3t.toSeq)
    plt.title = "Multiply"
    plt.refresh*/
  }

  it should "produce a single series of divided values" in {
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (_,xx) = Fourier.samplingSpecification(500,2)
    val y1 = xx.map( Function.sin(frequency, amplitude, phase) )
    val y2 = Enumeration.continually(0.5)
    val y3 = y1 / y2

    val y1t = y1.take(xx.length)
    val y2t = y2.take(xx.length)
    val y3t = y3.take(xx.length)
    val y3r = y1t.zip(y2t).map( p => p._1 / p._2)
    y3t should contain theSameElementsAs y3r

    /*import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, y1t)
    plt += plot(xx, y2t.toSeq)
    plt += plot(xx, y3t.toSeq)
    plt.title = "Divide"
    plt.refresh*/
  }

  "Using a DSL to operate on samplers" should "allow one to simulate signals" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val f1 = 1 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(-45)
    val f2 = 2 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val f3 = 4 /* Hz */ ; val a3 = 0.6 ; val phase3 = Math.toRadians(20)
    //val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
    // samplesTo at 500Hz and samplesTo for 2 seconds
    val (_,xx) = Fourier.samplingSpecification(500,2)

    val sig1 = xx.map( Function.sin(f1, a1, phase1) )
    val sig2 = xx.map( Function.sin(f2, a2, phase2) )
    val sig3 = xx.map( Function.cos(f3, a3, phase3) )
    val sig4 = Distribution.normal(0, 0.3)
    val sig5 = (sig1 - sig2 + sig3 + sig4).toSeq

    sig5.length shouldBe xx.length
    sig5 should not contain theSameElementsAs(sig1)

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, sig1(xx.length))
    plt += plot(xx, sig2(xx.length))
    plt += plot(xx, sig3(xx.length))
    plt += plot(xx, sig4(xx.length))
    plt += plot(xx, sig5(xx.length))
    plt.title = "DSL Ops"
    plt.refresh*/
  }

  "Combining samplers" should "allow the creation of a discrete search space" in {

    // An example of an exponential learning rate parameter
    // 0.001, 0.01, 0.1, 1, 10, 100, .....
    //val learningRate = Enumeration.from(-3).map( p => Math.pow(10,p))
    val power = Function.power(10) _
    val learningRate = Enumeration.from(-3.0, 1.0).map( power )
    learningRate.take(6).toList should contain theSameElementsInOrderAs List(0.001, 0.01, 0.1, 1, 10, 100)

    val mlAlg = Iterable( "alg1", "alg2", "alg3")

    // Algorithm 1 has 2 parameters, one for kernels and another general one
    val param1_Alg1 = Iterable("k1", "k2", "k3")
    // parameter1 : But it each kernel has its own set of parameters
    val alg1_k1_p1 = Iterable("k1p1", "k1p2", "p3")
    val alg1_k2_p1 = Iterable("k2p1", "k2p2", "p3")
    val alg1_k3_p1 = Iterable("k3p1", "k3p2", "k3p3")
    // parameter 2 : applies to any of the kernels
    val param2_Alg1 = Iterable("a1p2_1", "a1p2_2")

    // we now want to pair (left) the kernels with each of its parameters
    val sParam1_Alg1 = param1_Alg1 #& Iterable(alg1_k1_p1, alg1_k2_p1, alg1_k3_p1)
    // See how each kernel is assigned only the required/valid parameters
    sParam1_Alg1.toSeq should contain theSameElementsAs List(
      ("k1","k1p1"),("k1","k1p2"),("k1","p3"),
      ("k2","k2p1"),("k2","k2p2"),("k2","p3"),
      ("k3","k3p1"),("k3","k3p2"),("k3","k3p3"))

    // Now we can combine the paired parameters with any of the general parameters
    val s_Alg1 = sParam1_Alg1 ## param2_Alg1
    // See how each kernel combination is combined with all of the general parameters
    s_Alg1.toList should contain theSameElementsAs List(
      (("k1","k1p1"),"a1p2_1"),(("k1","k1p2"),"a1p2_1"),(("k1","p3"),  "a1p2_1"),
      (("k2","k2p1"),"a1p2_1"),(("k2","k2p2"),"a1p2_1"),(("k2","p3"),  "a1p2_1"),
      (("k3","k3p1"),"a1p2_1"),(("k3","k3p2"),"a1p2_1"),(("k3","k3p3"),"a1p2_1"),

      (("k1","k1p1"),"a1p2_2"),(("k1","k1p2"),"a1p2_2"),(("k1","p3"),  "a1p2_2"),
      (("k2","k2p1"),"a1p2_2"),(("k2","k2p2"),"a1p2_2"),(("k2","p3"),  "a1p2_2"),
      (("k3","k3p1"),"a1p2_2"),(("k3","k3p2"),"a1p2_2"),(("k3","k3p3"),"a1p2_2")
    )

    // We may want to apply these to a function. To make this easier we can
    // use some Shapeless macro magic to flatten the the tuples.
    // NOTE: the tuple type must be the same for all iterable's elements
    val fs_Alg1 = flatten(s_Alg1)
    fs_Alg1.toList should contain theSameElementsAs List(
      ("k1","k1p1","a1p2_1"),("k1","k1p2","a1p2_1"),("k1","p3",  "a1p2_1"),
      ("k2","k2p1","a1p2_1"),("k2","k2p2","a1p2_1"),("k2","p3",  "a1p2_1"),
      ("k3","k3p1","a1p2_1"),("k3","k3p2","a1p2_1"),("k3","k3p3","a1p2_1"),

      ("k1","k1p1","a1p2_2"),("k1","k1p2","a1p2_2"),("k1","p3",  "a1p2_2"),
      ("k2","k2p1","a1p2_2"),("k2","k2p2","a1p2_2"),("k2","p3",  "a1p2_2"),
      ("k3","k3p1","a1p2_2"),("k3","k3p2","a1p2_2"),("k3","k3p3","a1p2_2")
    )


    // We can prepare the search parameters for each algorithm separately
    // and then pair them too as we done before
    // So lets set up algorithm 2 and 3's parameters (assume we have only one)
    // IMPORTANT NOTE: this is just a test to check that composition is correct
    // note however that we should NOT combine parameters of different algorithms
    // if the types are not the same because type information will be lost (in
    // this case the second tuple element will be inferred as serializable because
    // they have different types
    val s_Alg2 = Iterable("a21", "a22", "a23")
    val s_Alg3 = Iterable("a31", "a32", "a33")
    val s_all_algs = mlAlg #& Iterable(s_Alg1, s_Alg2, s_Alg3)
    s_all_algs.toList should contain theSameElementsAs List(

      // Alg1
      ("alg1",(("k1","k1p1"),"a1p2_1")),("alg1",(("k1","k1p2"),"a1p2_1")),("alg1",(("k1","p3"),  "a1p2_1")),
      ("alg1",(("k2","k2p1"),"a1p2_1")),("alg1",(("k2","k2p2"),"a1p2_1")),("alg1",(("k2","p3"),  "a1p2_1")),
      ("alg1",(("k3","k3p1"),"a1p2_1")),("alg1",(("k3","k3p2"),"a1p2_1")),("alg1",(("k3","k3p3"),"a1p2_1")),

      ("alg1",(("k1","k1p1"),"a1p2_2")),("alg1",(("k1","k1p2"),"a1p2_2")),("alg1",(("k1","p3"),  "a1p2_2")),
      ("alg1",(("k2","k2p1"),"a1p2_2")),("alg1",(("k2","k2p2"),"a1p2_2")),("alg1",(("k2","p3"),  "a1p2_2")),
      ("alg1",(("k3","k3p1"),"a1p2_2")),("alg1",(("k3","k3p2"),"a1p2_2")),("alg1",(("k3","k3p3"),"a1p2_2")),

      // Alg 2
      ("alg2","a21"),("alg2","a22"),("alg2","a23"),

      // Alg 3
      ("alg3","a31"),("alg3","a32"),("alg3","a33")
    )

    // We cannot flatten these mixed types
    // This compiles but will not flatten
    /*val fs_all_algs = flatten(s_all_algs)
    println(fs_all_algs.mkString(","))*/

    // We can also append various search parameters and combine those if need be
    // Say we have want to test some specific learning rates that were used in a
    // previous article. Notice that the reference is a finite list and the other
    // learning rates are not (infinite). In this case make sure that the
    // non-terminating list appears last wit no other non-terminating samplers
    // Also do NOT use the Iterator's append (++) operator
    val referenceLearningRate = Iterable(0.002, 0.5)
    val allLearningRates = referenceLearningRate +++ learningRate
    allLearningRates.take(8).toList should contain theSameElementsInOrderAs List(0.002,0.5,0.001,0.01,0.1,1.0,10.0,100.0)

  }

  it should "allow the creation of a random search space" in {
    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    // We can combine various distributions to construct our own tailor made PDFs
    // Lets say we want to produce a univariate bi-modal distribution: one node at
    // mean = 0, sd = 0.25 and another at mean = 1.5 and sd = 0.25
    val n_samples1 = 10000
    val x1 = Distribution.normal(0.0, 0.25)
    val x2 = Distribution.normal(1.5, 0.5)
    val mp = Map(x1 -> 0.5, x2 -> 0.5)
    val x1x2 = Distribution.mixed( mp )
    val y = x1x2.take(n_samples1).toSeq.sorted

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 30)*/

    // Plot above shoes that the mixed distribution seems to be correct
    // Lets add some numerical tests: we will collect about 95 (2*sigma) of
    // the data points generated by each normal distribution. We assume that
    // very data points will that fall beyond 2*sigma are shared by the
    // distributions. So if we count the number of data points of both
    // distributions within 2*sigma, then we should account for most of
    // the data (about 95%)

    // Count data points within 2*sigma of x1
    val mode1 = y.filter( yi => (yi >= 0 - (2*0.25)) && (yi <= 0 + (2*0.25)) )
    val c_model1 = mode1.size

    // Count data points within 2*sigma of x2
    val mode2 = y.filter( yi => (yi >= 1.5 - (2*0.5)) && (yi <= 1.5 + (2*0.5)) )
    val c_model2 = mode2.size

    val portion = (c_model1 + c_model2)/(1.0 * n_samples1)
    portion should be >= 0.95 // excess are due to shared data points counted twice

    // We now show how to set-up a random search consisting of random
    // and non-random parameters. Care must be taken when setting up
    // the number of elements to be sampled

    // Lets set-up the search for the learning rate an exponential learning rate parameter
    // Lets use this 0.001, 0.01, 0.1, 1, 10, 100, .....
    val power = Function.power(10) _
    val learningRateExp = Enumeration.from(-3.0, 1.0).map( power )
    learningRateExp.take(6).toList should contain theSameElementsInOrderAs List(0.001, 0.01, 0.1, 1, 10, 100)

    // But we also want to explore random values for this parameter
    val x3 = Distribution.uniform(0.001, 1e3)

    // We also have a discrete and finite parameter that we want to combine
    // with both of the parameters above.
    val x4 = Iterable("k1", "k2")

    // We will use the !! operator that will rotate each sampler until all
    // both have had all samples taken. Note that 'x4' is finite so we
    // need not indicate the number of samples we want. 'learningRateExp'
    // could be sampled infinitely but learning rates above 1e3 are not
    // usable, so we use the first 7 learning rates. 'x3' is a random
    // distribution, so we can sample any and all values.

    // NOTE: if at least one of the samplers are infinite, then the
    // composed sampler is also infinite. See commented examples below

    //val xs1 = x4 !! (learningRateExp  take 10) !! (x3 take 12)  // finite
    //val xs1 = x4 !! learningRateExp !! (x3 take 12)             // infinite
    //val xs1 = x4 !! learningRateExp !! x3                       // infinite
    val xs1 = x4 !! (learningRateExp take 6) !! x3              // infinite (solution)
    //val y1 = flatten( xs1.take(12) ).toList
    val y1 = flatten( xs1 ).take(12).toList
    y1 should contain theSameElementsInOrderAs List(
      ("k1",0.001,123.75698896178626),
      ("k2",0.01,711.8539214999806),
      ("k1",0.1,117.30317010860202),
      ("k2",1.0,927.9039299482237),
      ("k1",10.0,759.2720295640999),
      ("k2",100.0,415.8791522648494),
      ("k1",0.001,431.2707698225681),
      ("k2",0.01,984.6793808312218),
      ("k1",0.1,749.8773468900456),
      ("k2",1.0,16.360092063255674),
      ("k1",10.0,310.532501000267),
      ("k2",100.0,708.971197103782))

    // We repeat the same experiment above, but limit the learning rate with the until operator

    val xs2 = x4 !! learningRateExp.until(0) { (z:Int, i:Double) => (z, i > 100.0)} !! x3
    //val y2 = flatten( xs2.take(12) ).toList
    val y2 = flatten( xs2 ).take(12).toList
    y2 should contain theSameElementsInOrderAs List(
      ("k1",0.001,123.75698896178626),
      ("k2",0.01,711.8539214999806),
      ("k1",0.1,117.30317010860202),
      ("k2",1.0,927.9039299482237),
      ("k1",10.0,759.2720295640999),
      ("k2",100.0,415.8791522648494),
      ("k1",0.001,431.2707698225681),
      ("k2",0.01,984.6793808312218),
      ("k1",0.1,749.8773468900456),
      ("k2",1.0,16.360092063255674),
      ("k1",10.0,310.532501000267),
      ("k2",100.0,708.971197103782))


    // We repeat the same experiment above, but randomize the learning rate (not necessary)

    Random.setSeed(9876)
    val xs3 = x4 !! randomize(learningRateExp take 6) !! x3
    val y3 = flatten( xs3.take(12) ).toList
    y3 should contain theSameElementsInOrderAs List(
      ("k1",100.0,123.75698896178626),
      ("k2",0.01,711.8539214999806),
      ("k1",10.0,117.30317010860202),
      ("k2",1.0,927.9039299482237),
      ("k1",0.1,759.2720295640999),
      ("k2",0.001,415.8791522648494),
      ("k1",100.0,431.2707698225681),
      ("k2",0.01,984.6793808312218),
      ("k1",10.0,749.8773468900456),
      ("k2",1.0,16.360092063255674),
      ("k1",0.1,310.532501000267),
      ("k2",0.001,708.971197103782))
  }

  it should "allow repeating sampling of combined samplers" in {
    val s1 = 1 to 6
    val s2 = 'a' to 'c'

    // This is the sampling we want to repeat
    val c1 = s1 !! s2
    c1.toList should contain theSameElementsInOrderAs List((1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c'))


    // We now repeat the samples above (ad-infinitum)
    val s3 = repeat(c1)
    // Lets sample c1 twice
    s3.take(12).toList should contain theSameElementsInOrderAs List(
      (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c'),
      (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c')
    )

    // We now repeat the samples above twice
    val s4 = repeatFor(2)(c1)
    // Lets sample c1 twice
    s4.toList should contain theSameElementsInOrderAs List(
      (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c'),
      (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c')
    )
  }

}
