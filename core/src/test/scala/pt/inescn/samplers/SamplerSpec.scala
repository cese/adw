/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package pt.inescn.samplers

import java.time.{Duration, Instant}

import org.hipparchus.random.{JDKRandomGenerator, RandomDataGenerator}
import org.scalatest._
import pt.inescn.dsp.Fourier
import pt.inescn.features.Stats
import pt.inescn.samplers.Base.{SamplerOps, _}
import pt.inescn.samplers.Enumeration._
import pt.inescn.samplers.Distribution.Proportional
import pt.inescn.utils.RandomCollection

import scala.annotation.tailrec

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.samplers.SamplerSpec"
  *
  * Created by hmf on 22-05-2017.
  */
class SamplerSpec extends FlatSpec with Matchers {

  val eps = 0.17 // 0.17 // 0.12

  "Simple numeric ranges" should "generate a list of samples" in {
    val sampler1 = RangeOf(0, 2, 1)
    val l1 = sampler1.toList
    //println(l1.reverse.mkString(","))
    l1 should have length 3
    val cl1 = List(0,1,2)
    l1.reverse should contain theSameElementsInOrderAs cl1
    val sampler2 = RangeOf(1.0, 1.5, 0.5)
    val l2 = sampler2.toList
    //println(l2.mkString(","))
    l2 should have length 2
    val cl2 = List(1.0,1.5)
    l2.reverse should contain theSameElementsInOrderAs cl2
  }

  it should "generate a stream of samples" in {
    val sampler1 = RangeOf(0, 2, 1)
    val l1 = sampler1.toStream
    //println(l1.toList.mkString(","))
    l1 should have length 3

    val sampler2 = RangeOf(1.0, 1.5, 0.5)
    val l2 = sampler2.toStream
    //println(l2.toList.mkString(","))
    l2 should have length 2
  }

  "Negative delta ranges " should "produce finite monotonically decreasing samples" in {
    val sampler1 = RangeOf(2, 0, -1)
    val l1 = sampler1.toList
    //println(l1.reverse.mkString(","))
    l1 should have length 3
    val cl1 = List(2,1,0)
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler2 = RangeOf(1.5, 1.0, -0.5)
    val l2 = sampler2.toStream
    //println(l2.toList.mkString(","))
    l2 should have length 2
    val cl2 = List(1.5, 1.0)
    l2 should contain theSameElementsInOrderAs cl2

    // Invalid range
    val sampler3 = RangeOf(0, 2, -1)
    val l3 = sampler3.toList
    //println(l3.reverse.mkString(","))
    l3 should have length 1
    val cl3 = List(0)
    l3.reverse should contain theSameElementsInOrderAs cl3
    //println(sampler3)
    sampler3 shouldBe an[EmptyRange[Int]]
  }

  "Simple list ranges" should "generate a list of samples" in {
    val cl1 = List(1,2,3,4,5,6)
    val sampler1 = ListRange(cl1)
    val l1 = sampler1.toList
    //println(l1.reverse.mkString(","))
    l1 should have length 6
    l1.reverse should contain theSameElementsInOrderAs cl1

    val cl2 = List('a', 'b', 'c', 'd')
    val sampler2 = ListRange(cl2)
    val l2 = sampler2.toList
    //println(l2.mkString(","))
    l2 should have length 4
    l2.reverse should contain theSameElementsInOrderAs cl2
  }

  "Simple numeric ranges" should "be combined via a cartesian product" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(0, 3, 1)
    val sampler3 = RangeOf (1.0, 1.5, 0.5)

    val sampler_p1_2 = Cartesian(sampler1, sampler2)
    val l1 = sampler_p1_2.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 12
    val cl1 = List((0,0), (0,1), (0,2), (0,3), (1,0), (1,1), (1,2), (1,3), (2,0), (2,1), (2,2), (2,3))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sample_p1_2_3 = Cartesian(sampler_p1_2, sampler3)
    val l2 = sample_p1_2_3.toList.map( SamplerOps.flatten3 )
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2 should have length 24
    val cl2 = List(
      (0,0,1.0), (0,0,1.5),
      (0,1,1.0), (0,1,1.5),
      (0,2,1.0), (0,2,1.5),
      (0,3,1.0), (0,3,1.5),
      (1,0,1.0), (1,0,1.5),
      (1,1,1.0), (1,1,1.5),
      (1,2,1.0), (1,2,1.5),
      (1,3,1.0), (1,3,1.5),
      (2,0,1.0), (2,0,1.5),
      (2,1,1.0), (2,1,1.5),
      (2,2,1.0), (2,2,1.5),
      (2,3,1.0), (2,3,1.5))
    l2.reverse should contain theSameElementsInOrderAs cl2

    sample_p1_2_3.reset
    val l3 = sample_p1_2_3.toList.map( SamplerOps.flatten3 )
    //println(l3.reverse.mkString("\n"))
    //println(l3.length)
    l3 should have length 24
    l3.reverse should contain theSameElementsInOrderAs cl2
  }

  "Complex numeric ranges" should "be combined via a cartesian product" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(0, 3, 1)
    val sampler3 = RangeOf(1.0, 1.5, 0.5)
    val sampler4 = RangeOf(1.5, 2.0, 0.5)

    val sampler_p1_2 = Cartesian(sampler1, sampler2)
    val l1 = sampler_p1_2.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 12
    val cl1 = List((0,0), (0,1), (0,2), (0,3), (1,0), (1,1), (1,2), (1,3), (2,0), (2,1), (2,2), (2,3))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler_p3_4 = Cartesian(sampler3, sampler4)
    val l2 = sampler_p3_4.toList
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2 should have length 4
    val cl2 = List((1.0,1.5), (1.0,2.0), (1.5,1.5), (1.5,2.0))
    l2.reverse should contain theSameElementsInOrderAs cl2

    val sampler_p1_2_3_4 = Cartesian(sampler_p1_2, sampler_p3_4)
    val l3 = sampler_p1_2_3_4.toList
    //println(l3.reverse.mkString("\n"))
    //println(l3.length)
    l3 should have length 48
    val cl3 = List(
      ((0,0),(1.0,1.5)),
      ((0,0),(1.0,2.0)),
      ((0,0),(1.5,1.5)),
      ((0,0),(1.5,2.0)),
      ((0,1),(1.0,1.5)),
      ((0,1),(1.0,2.0)),
      ((0,1),(1.5,1.5)),
      ((0,1),(1.5,2.0)),
      ((0,2),(1.0,1.5)),
      ((0,2),(1.0,2.0)),
      ((0,2),(1.5,1.5)),
      ((0,2),(1.5,2.0)),
      ((0,3),(1.0,1.5)),
      ((0,3),(1.0,2.0)),
      ((0,3),(1.5,1.5)),
      ((0,3),(1.5,2.0)),
      ((1,0),(1.0,1.5)),
      ((1,0),(1.0,2.0)),
      ((1,0),(1.5,1.5)),
      ((1,0),(1.5,2.0)),
      ((1,1),(1.0,1.5)),
      ((1,1),(1.0,2.0)),
      ((1,1),(1.5,1.5)),
      ((1,1),(1.5,2.0)),
      ((1,2),(1.0,1.5)),
      ((1,2),(1.0,2.0)),
      ((1,2),(1.5,1.5)),
      ((1,2),(1.5,2.0)),
      ((1,3),(1.0,1.5)),
      ((1,3),(1.0,2.0)),
      ((1,3),(1.5,1.5)),
      ((1,3),(1.5,2.0)),
      ((2,0),(1.0,1.5)),
      ((2,0),(1.0,2.0)),
      ((2,0),(1.5,1.5)),
      ((2,0),(1.5,2.0)),
      ((2,1),(1.0,1.5)),
      ((2,1),(1.0,2.0)),
      ((2,1),(1.5,1.5)),
      ((2,1),(1.5,2.0)),
      ((2,2),(1.0,1.5)),
      ((2,2),(1.0,2.0)),
      ((2,2),(1.5,1.5)),
      ((2,2),(1.5,2.0)),
      ((2,3),(1.0,1.5)),
      ((2,3),(1.0,2.0)),
      ((2,3),(1.5,1.5)),
      ((2,3),(1.5,2.0)))
    l3.reverse should contain theSameElementsInOrderAs cl3
  }
  it should "be combined via a non-commutative cartesian product" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(0, 3, 1)

    val sampler_p1_2 = Cartesian(sampler1, sampler2)
    val l1 = sampler_p1_2.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 12
    val cl1 = List((0, 0), (0, 1), (0, 2), (0, 3), (1, 0), (1, 1), (1, 2), (1, 3), (2, 0), (2, 1), (2, 2), (2, 3))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler_p2_1 = Cartesian(sampler2, sampler1)
    val l2 = sampler_p2_1.toList
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2 should have length 12
    val cl2 = List(
      (0,0),
      (0,1),
      (0,2),
      (1,0),
      (1,1),
      (1,2),
      (2,0),
      (2,1),
      (2,2),
      (3,0),
      (3,1),
      (3,2))
    l2.reverse should contain theSameElementsInOrderAs cl2
  }
  it should "be combined via a associative cartesian product" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(0, 3, 1)
    val sampler3 = RangeOf(1.0, 1.5, 0.5)
    val sampler4 = RangeOf(1.5, 2.0, 0.5)

    val sampler_p1_2_3_4a = Cartesian( Cartesian(Cartesian(sampler1, sampler2), sampler3), sampler4)
    val l1 = sampler_p1_2_3_4a.toList.map( SamplerOps.flatten4 )
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 48
    val cl1 = List(
      (0,0,1.0,1.5),
      (0,0,1.0,2.0),
      (0,0,1.5,1.5),
      (0,0,1.5,2.0),
      (0,1,1.0,1.5),
      (0,1,1.0,2.0),
      (0,1,1.5,1.5),
      (0,1,1.5,2.0),
      (0,2,1.0,1.5),
      (0,2,1.0,2.0),
      (0,2,1.5,1.5),
      (0,2,1.5,2.0),
      (0,3,1.0,1.5),
      (0,3,1.0,2.0),
      (0,3,1.5,1.5),
      (0,3,1.5,2.0),
      (1,0,1.0,1.5),
      (1,0,1.0,2.0),
      (1,0,1.5,1.5),
      (1,0,1.5,2.0),
      (1,1,1.0,1.5),
      (1,1,1.0,2.0),
      (1,1,1.5,1.5),
      (1,1,1.5,2.0),
      (1,2,1.0,1.5),
      (1,2,1.0,2.0),
      (1,2,1.5,1.5),
      (1,2,1.5,2.0),
      (1,3,1.0,1.5),
      (1,3,1.0,2.0),
      (1,3,1.5,1.5),
      (1,3,1.5,2.0),
      (2,0,1.0,1.5),
      (2,0,1.0,2.0),
      (2,0,1.5,1.5),
      (2,0,1.5,2.0),
      (2,1,1.0,1.5),
      (2,1,1.0,2.0),
      (2,1,1.5,1.5),
      (2,1,1.5,2.0),
      (2,2,1.0,1.5),
      (2,2,1.0,2.0),
      (2,2,1.5,1.5),
      (2,2,1.5,2.0),
      (2,3,1.0,1.5),
      (2,3,1.0,2.0),
      (2,3,1.5,1.5),
      (2,3,1.5,2.0))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler_p1_2_3_4b = Cartesian(
                                    Cartesian(sampler1, sampler2),
                                    Cartesian(sampler3, sampler4))
    val l2 = sampler_p1_2_3_4b.toList.map( SamplerOps.flatten2_2 )
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2.reverse should contain theSameElementsInOrderAs cl1
  }

  "Simple numeric ranges" should "be combined via an AND sum operator" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(4, 8, 1)
    //val sampler3 = RangeSampler(1.0, 1.5, 0.5)
    //val sampler4 = RangeSampler(1.5, 2.0, 0.5)

    val sampler_sa_1_2 = And(sampler1, sampler2)
    val l1 = sampler_sa_1_2.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 3
    val cl1 = List( (0,4), (1,5), (2,6))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler_sa_2_1 = And(sampler2, sampler1)
    val l2 = sampler_sa_2_1.toList
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2 should have length 3
    val cl2 = List( (4,0), (5,1), (6,2))
    l2.reverse should contain theSameElementsInOrderAs cl2
  }

  it should "be combined via an OR sum operator" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(4, 8, 1)
    //val sampler3 = RangeSampler(1.0, 1.5, 0.5)
    //val sampler4 = RangeSampler(1.5, 2.0, 0.5)

    val sampler_sa_1_2 = Or(sampler1, sampler2)
    val l1 = sampler_sa_1_2.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 15
    val cl1 = List(
      (0,4),
      (1,5),
      (2,6),
      (0,7),
      (1,8),
      (2,4),
      (0,5),
      (1,6),
      (2,7),
      (0,8),
      (1,4),
      (2,5),
      (0,6),
      (1,7),
      (2,8))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler_sa_2_1 = Or(sampler2, sampler1)
    val l2 = sampler_sa_2_1.toList
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2 should have length 15
    val cl2 = List(
      (4,0),
      (5,1),
      (6,2),
      (7,0),
      (8,1),
      (4,2),
      (5,0),
      (6,1),
      (7,2),
      (8,0),
      (4,1),
      (5,2),
      (6,0),
      (7,1),
      (8,2))
    l2.reverse should contain theSameElementsInOrderAs cl2
  }

  "Complex numeric ranges" should "be combined via all product operators" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(0, 3, 1)
    val sampler3 = RangeOf(1.0, 1.5, 0.5)
    val sampler4 = RangeOf(1.5, 2.0, 0.5)

    val sampler_sa_1_2 = Or(sampler1, sampler2)
    val l1 = sampler_sa_1_2.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 12
    val cl1 = List(
      (0,0),
      (1,1),
      (2,2),
      (0,3),
      (1,0),
      (2,1),
      (0,2),
      (1,3),
      (2,0),
      (0,1),
      (1,2),
      (2,3))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler_sa_3_4 = And(sampler3, sampler4)
    val l2 = sampler_sa_3_4.toList
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2 should have length 2
    val cl2 = List((1.0,1.5), (1.5,2.0))
    l2.reverse should contain theSameElementsInOrderAs cl2

    val sampler_pb_1_2_3_4 = Cartesian(sampler_sa_1_2, sampler_sa_3_4)
    val l3 = sampler_pb_1_2_3_4.toList
    //println(l3.reverse.mkString("\n"))
    //println(l3.length)
    l3 should have length 24
    val cl3 = List(
      ((0,0),(1.0,1.5)),
      ((0,0),(1.5,2.0)),
      ((1,1),(1.0,1.5)),
      ((1,1),(1.5,2.0)),
      ((2,2),(1.0,1.5)),
      ((2,2),(1.5,2.0)),
      ((0,3),(1.0,1.5)),
      ((0,3),(1.5,2.0)),
      ((1,0),(1.0,1.5)),
      ((1,0),(1.5,2.0)),
      ((2,1),(1.0,1.5)),
      ((2,1),(1.5,2.0)),
      ((0,2),(1.0,1.5)),
      ((0,2),(1.5,2.0)),
      ((1,3),(1.0,1.5)),
      ((1,3),(1.5,2.0)),
      ((2,0),(1.0,1.5)),
      ((2,0),(1.5,2.0)),
      ((0,1),(1.0,1.5)),
      ((0,1),(1.5,2.0)),
      ((1,2),(1.0,1.5)),
      ((1,2),(1.5,2.0)),
      ((2,3),(1.0,1.5)),
      ((2,3),(1.5,2.0)))
    l3.reverse should contain theSameElementsInOrderAs cl3

    // test combining with itself several times BUG
  }
  it should "be able to combined the same combinators results" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(0, 3, 1)
    val sampler3 = RangeOf(1.0, 1.5, 0.5)

    val sampler_sa_1_1 = Cartesian(sampler1, sampler1)
    val l1 = sampler_sa_1_1.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 9
    val cl1 = List(
      (0,0),
      (0,1),
      (0,2),
      (1,0),
      (1,1),
      (1,2),
      (2,0),
      (2,1),
      (2,2))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler_sa_2_2 = And(sampler2, sampler2)
    val l2 = sampler_sa_2_2.toList
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2 should have length 4
    val cl2 = List(
      (0,0),
      (1,1),
      (2,2),
      (3,3))
    l2.reverse should contain theSameElementsInOrderAs cl2

    val sampler_sa_2_2_3 = Or(sampler_sa_2_2, sampler3)
    val l3 = sampler_sa_2_2_3.toList
    //println(l3.reverse.mkString("\n"))
    //println(l3.length)
    l3 should have length 4
    val cl3 = List(
      ((0,0),1.0),
      ((1,1),1.5),
      ((2,2),1.0),
      ((3,3),1.5))
    l3.reverse should contain theSameElementsInOrderAs cl3

    val sampler_sb_2_2_3 = Cartesian(sampler_sa_2_2, sampler3)
    val l4 = sampler_sb_2_2_3.toList
    //println(l4.reverse.mkString("\n"))
    //println(l4.length)
    l4 should have length 8
    val cl4 = List(
      ((0,0),1.0),
      ((0,0),1.5),
      ((1,1),1.0),
      ((1,1),1.5),
      ((2,2),1.0),
      ((2,2),1.5),
      ((3,3),1.0),
      ((3,3),1.5))
    l4.reverse should contain theSameElementsInOrderAs cl4
  }

  "Applying commuted samplers" should "produce equivalent results" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(0, 3, 1)
    val sampler3 = RangeOf(1.0, 1.5, 0.5)

    val sampler_sa_1_2 = Cartesian(sampler1, sampler2)
    val l1 = sampler_sa_1_2.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 12
    val cl1 = List(
      (0,0),
      (0,1),
      (0,2),
      (0,3),
      (1,0),
      (1,1),
      (1,2),
      (1,3),
      (2,0),
      (2,1),
      (2,2),
      (2,3))
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler_sa_2_1 = Cartesian(sampler2, sampler1)
    val l2 = sampler_sa_2_1.toList
    //println(l2.reverse.mkString("\n"))
    //println(l2.length)
    l2 should have length 12
    val cl2 = List(
      (0,0),
      (0,1),
      (0,2),
      (1,0),
      (1,1),
      (1,2),
      (2,0),
      (2,1),
      (2,2),
      (3,0),
      (3,1),
      (3,2))
    l2.reverse should contain theSameElementsInOrderAs cl2

    val sampler_sa_2_2_3 = Or(sampler_sa_1_2, sampler3)
    val l3 = sampler_sa_2_2_3.toList
    //println(l3.reverse.mkString("\n"))
    //println(l3.length)
    l3 should have length 12
    val cl3 = List(
      ((0,0),1.0),
      ((0,1),1.5),
      ((0,2),1.0),
      ((0,3),1.5),
      ((1,0),1.0),
      ((1,1),1.5),
      ((1,2),1.0),
      ((1,3),1.5),
      ((2,0),1.0),
      ((2,1),1.5),
      ((2,2),1.0),
      ((2,3),1.5))
    l3.reverse should contain theSameElementsInOrderAs cl3

    val sampler_sa_3_2_2 = Or(sampler3, sampler_sa_1_2)
    val l4 = sampler_sa_3_2_2.toList
    //println(l4.reverse.mkString("\n"))
    //println(l4.length)
    l4 should have length 12
    val cl4 = List(
      (1.0,(0,0)),
      (1.5,(0,1)),
      (1.0,(0,2)),
      (1.5,(0,3)),
      (1.0,(1,0)),
      (1.5,(1,1)),
      (1.0,(1,2)),
      (1.5,(1,3)),
      (1.0,(2,0)),
      (1.5,(2,1)),
      (1.0,(2,2)),
      (1.5,(2,3)))
    l4.reverse should contain theSameElementsInOrderAs cl4
  }

  "Using the operators" should "produce equivalent results" in {
    val sampler1 = RangeOf(0, 2, 1)
    val sampler2 = RangeOf(0, 3, 1)
    val sampler3 = RangeOf(1.0, 1.5, 0.5)

    //val sampler_sa_1_2 = SamplerOps.cartesian(sampler1, sampler2)
    val sampler_sa_1_2 = sampler1 ## sampler2
    val l1 = sampler_sa_1_2.toList
    //println(l1.reverse.mkString("\n"))
    //println(l1.length)
    l1 should have length 12
    val cl1 = List(
      (0,0),
      (0,1),
      (0,2),
      (0,3),
      (1,0),
      (1,1),
      (1,2),
      (1,3),
      (2,0),
      (2,1),
      (2,2),
      (2,3))
    l1.reverse should contain theSameElementsInOrderAs cl1

    //val sampler_sa_2_2_3 = SamplerOps.or(sampler_sa_1_2, sampler3)
    val sampler_sa_2_2_3 = sampler_sa_1_2 || sampler3
    val l3 = sampler_sa_2_2_3.toList
    //println(l3.reverse.mkString("\n"))
    //println(l3.length)
    l3 should have length 12
    val cl3 = List(
      ((0,0),1.0),
      ((0,1),1.5),
      ((0,2),1.0),
      ((0,3),1.5),
      ((1,0),1.0),
      ((1,1),1.5),
      ((1,2),1.0),
      ((1,3),1.5),
      ((2,0),1.0),
      ((2,1),1.5),
      ((2,2),1.0),
      ((2,3),1.5))
    l3.reverse should contain theSameElementsInOrderAs cl3

    val sampler_sa_1_2_3 = (sampler1 ## sampler2) || sampler3
    val l4 = sampler_sa_1_2_3.toList
    //println(l4.reverse.mkString("\n"))
    //println(l4.length)
    l4 should have length 12
    l4.reverse should contain theSameElementsInOrderAs cl3
  }

  /**
    * https://en.wikipedia.org/wiki/Normal_distribution
    */
  "Using a normal sampler" should "produce a normal distribution" in {
    import pt.inescn.features._
    val y= Distribution.Normal(0, 1, new JDKRandomGenerator(9876)).toStream.take(500)
    y.length shouldBe 500
    val stats = Stats.DescriptiveStats.batch(y)
    stats.mean.value should be(0.0 +- eps)
    stats.standard_deviation.value should be(1.0 +- eps)
    stats.skewness.value should be(0.0 +- eps)
    //stats.kurtosis.value should be(0.0 +- eps)

    //import pt.inescn.plot._
    //val x = (1 to y.length).map(_.toDouble)
    //Plot.showPlot(x, y)
    //Plot.showHistogram(y, 7)
  }

  /**
    * https://en.wikipedia.org/wiki/Uniform_distribution_(continuous)
    */
  "Using a uniform sampler" should "produce a uniform distribution" in {
    import pt.inescn.features._
    val a = 0
    val b = 1
    val y = Distribution.Uniform(a, b, new JDKRandomGenerator(9876)).toStream.take(1000).sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)

    val mean = (a + b) / 2.0
    val median = (a + b) / 2.0
    val sd = Math.sqrt( Math.pow(b - a,2) / 12.0)
    val kurtosis = -6.0/5.0
    val skewness = 0.0
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be (median +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 7)*/
  }

  /**
    * https://en.wikipedia.org/wiki/Gamma_distribution
    */
  "Using a gamma sampler" should "produce a gamma distribution" in {
    import pt.inescn.features._
    val shape = 9.0
    val scale = 0.5
    // NOTE: using the JDKRandomGenerator generates different results
    val y = Distribution.Gamma(shape, scale, new RandomDataGenerator(9876)).toStream.take(1000).sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)
    val tmean = shape * scale
    val sd = Math.sqrt( shape * scale * scale )
    val kurtosis = 2.0 / Math.sqrt(shape)
    val skewness = 6.0 / shape
    stats.mean.value should be(tmean +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }

  /**
    * https://en.wikipedia.org/wiki/Log-normal_distribution
    */
  "Using a LogNorma sampler" should "produce a LogNorma distribution" in {
    import pt.inescn.features._
    val shape = 0.25 // standard deviation
    val scale = 0.05 // mean parameter
    val y = Distribution.LogNormal(shape, scale, new JDKRandomGenerator(9876)).toStream.take(1000).sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = Math.exp(scale + ((shape*shape)/2.0) )
    val median = Math.exp(scale)
    val sd = Math.sqrt(Math.exp(shape*shape)-1)*Math.exp((2*scale)+(shape*shape))
    val kurtosis = Math.exp(4*shape*shape) + 2*Math.exp(3*shape*shape) + 3*Math.exp(2*shape*shape) - 6
    val skewness = (Math.exp(shape*shape)+2)*Math.sqrt(Math.exp(shape*shape)-1)
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be(median +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps) // ??
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }


  /**
    * https://en.wikipedia.org/wiki/Exponential_distribution
    *
    * BUG: Kurtosis and skewness
    */
  "Using a Exponential sampler" should "produce a exponential distribution" in {
    import pt.inescn.features._
    val pmean = 0.5
    val y = Distribution.Exponential(pmean, new JDKRandomGenerator(9876)).toStream.take(1000).sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = pmean
    val median = pmean * Math.log(2)
    val sd = Math.sqrt(pmean*pmean)
    //val kurtosis = 6.0
    //val skewness = 2.0
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be(median +- eps) // ??
    stats.standard_deviation.value should be(sd +- eps)
    //stats.kurtosis.value should be(kurtosis +- eps)
    //stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }

  /**
    * https://en.wikipedia.org/wiki/Beta_distribution
    */
  "Using a Beta sampler" should "produce a beta distribution" in {
    import pt.inescn.features._
    val alpha = 2.0
    val beta = 2.0
    val y = Distribution.Beta(alpha, beta, new JDKRandomGenerator(9876)).toStream.take(1000).sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = alpha / (alpha + beta)
    val median = (alpha - (1/3.0))/ (alpha + beta - (2/3.0) )   // alpha and beta > 1
    val sd = Math.sqrt((alpha*beta)/(Math.pow(alpha+beta,2)*(alpha+beta+1.0)))
    val kurtosis = 6*((Math.pow(alpha-beta,2)*(alpha+beta+1))-((alpha*beta)*(alpha+beta+2)))/((alpha*beta)*(alpha+beta+2)*(alpha+beta+3))
    val skewness = (2.0*(beta-alpha)*Math.sqrt(alpha+beta+1)) / ((alpha+beta+2)*Math.sqrt(alpha*beta))
    stats.mean.value should be(mean +- eps)
    stats.quartile_2.value should be(median +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)

    /*import pt.inescn.plot._
    val x = (1 to y.length).map(_.toDouble)
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }

  // Integer samples

  /**
    * Approximation of the Riemann Zeta Function
    * @param s - parameter of function
    * @return an approximation of the Riemann Zeta Function (not infinite sum but 100000 elements)
    */
  def riemannZetaFunction(s:Int) : Double = {
    @tailrec
    def loop(i: Int, acc: Double) : Double = {
      if (i==0)
        acc
      else
        loop(i-1, acc + (1.0/Math.pow(i,s)))
    }
    loop(100000,0.0)
  }

  /**
    * https://en.wikipedia.org/wiki/Zeta_distribution
    */
  "Using a Zipf sampler" should "produce a beta distribution" in {
    import pt.inescn.features._
    val numberOfElements = 5 // K
    val exp = 4  // s
    val y = Distribution.Zipf(numberOfElements, exp, new JDKRandomGenerator(9876)).toStream.take(1000).sorted  // need to sort for median stats
    //val x = 1 to y.length
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y.map(_.toDouble))
    def gh = riemannZetaFunction _
    val mean = gh(exp-1) / gh(exp)  // for exp > 2
    val sd = Math.sqrt( (gh(exp)*gh(exp-2)) - Math.pow(gh(exp-1),2) ) / Math.pow(gh(exp),2) // for s > 3
    stats.mean.value should be(mean +- eps)
    stats.standard_deviation.value should be(sd +- eps)  // biggest error

    /*import pt.inescn.plot._
    Plot.showPlot(x.map(_.toDouble), y.map(_.toDouble))
    Plot.showHistogram(y.map(_.toDouble), 15)*/
  }

  /**
    * https://en.wikipedia.org/wiki/Poisson_distribution
    */
  "Using a Poisson sampler" should "produce a poisson distribution" in {
    import pt.inescn.features._
    val smean = 20.0
    val y = Distribution.Poisson(smean, new JDKRandomGenerator(9876)).toStream.take(1000).sorted  // need to sort for median stats
    y.length shouldBe 1000
    val stats = Stats.DescriptiveStats.batch(y.map(_.toDouble))
    //def gh = riemannZetaFunction _
    val mean = smean
    val sd = Math.sqrt(smean)
    val kurtosis = 1.0/smean
    val skewness = 1.0/Math.sqrt(smean)
    val median = Math.floor(smean + (1.0/3.0) - (0.02/smean))
    stats.quartile_2.value should be(median +- eps)
    stats.mean.value should be(mean +- eps)                  // second biggest error
    stats.standard_deviation.value should be(sd +- eps)
    stats.kurtosis.value should be(kurtosis +- eps)
    stats.skewness.value should be(skewness +- eps)

    val lower_median = mean - Math.log(2)
    val upper_median = mean + (1.0 / 1.3)
    stats.quartile_2.value should be >= lower_median
    stats.quartile_2.value shouldBe >=(lower_median)
    stats.quartile_2.value should be < upper_median

    /*import pt.inescn.plot._
    val x = 1 to y.length
    Plot.showPlot(x.map(_.toDouble), y.map(_.toDouble))
    Plot.showHistogram(y.map(_.toDouble), 15)*/
  }

  "Using a Proportional sampler" should "produce a correct discrete proportional distribution" in {
    val len = 10000
    val denom = len / 100.0
    val eps = 1.5 // %

    val mp = Map[Int, Double](1 -> 0.1, 2 -> 0.2, 3 -> 0.7)
    val y = Proportional(new JDKRandomGenerator(9876), mp).toStream.take(len)

    val gen1_classes = y.groupBy( p => p ).map( p => (p._1, p._2.length) )
    //println(gen1_classes)
    val gen1_class1: Int = gen1_classes(1)
    val gen1_class2: Int = gen1_classes(2)
    val gen1_class3: Int = gen1_classes(3)
    assertThrows[java.util.NoSuchElementException] { gen1_classes(4) }

    /*
    println(s"1 : $gen1_class1 = ${gen1_class1 / denom}")
    println(s"2 : $gen1_class2 = ${gen1_class2 / denom}")
    println(s"3 : $gen1_class3 = ${gen1_class3 / denom}")
    */

    (gen1_class1 / denom) shouldBe (10.0 +- eps)
    (gen1_class2 / denom) shouldBe (20.0 +- eps)
    (gen1_class3 / denom) shouldBe (70.0 +- eps)
  }

  it should "produce a correct discrete proportional distribution adding weights" in {
    val len = 10000
    val denom = len / 100.0
    val eps = 1.5 // %

    //val mp = TreeMap[Double, Int](0.1 -> 1, 0.2 -> 2, 0.7 -> 3)
    val yx = Proportional(new JDKRandomGenerator(9876))
      .add(0.1,1)
      .add(0.2,2)
      .add(0.7,3)
      .toStream.take(len)
    yx.nonEmpty shouldBe true

    val y0 = Proportional[Int](new JDKRandomGenerator(9876))
    val y1 = y0.add(0.1,1)
    val y2 = y1.add(0.2,2)
    val y3 = y2.add(0.7,3)
    val y = y3.toStream.take(len)


    val gen1_classes = y.groupBy( p => p ).map( p => (p._1, p._2.length) )
    //println(gen1_classes)
    val gen1_class1: Int = gen1_classes(1)
    val gen1_class2: Int = gen1_classes(2)
    val gen1_class3: Int = gen1_classes(3)
    assertThrows[java.util.NoSuchElementException] { gen1_classes(4) }

    /*
    println(s"1 : $gen1_class1 = ${gen1_class1 / denom}")
    println(s"2 : $gen1_class2 = ${gen1_class2 / denom}")
    println(s"3 : $gen1_class3 = ${gen1_class3 / denom}")
    */

    (gen1_class1 / denom) shouldBe (10.0 +- eps)
    (gen1_class2 / denom) shouldBe (20.0 +- eps)
    (gen1_class3 / denom) shouldBe (70.0 +- eps)
  }


  "Using a Sine sampler" should "produce a sine wave" in {
    import pt.inescn.features._
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (d,x) = Fourier.samplingSpecification(500,2)
    val y = Function.Sin(frequency, amplitude, phase, d).toStream.take(x.length) //.sorted  // need to sort for median stats
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = 0.0
    val sd = amplitude/Math.sqrt(2) // Approximately RMS of signal
    //val kurtosis = 0.0
    val skewness = 0.0
    val mx = amplitude
    val mn = -amplitude
    stats.mean.value should be(mean +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    //stats.kurtosis.value should be(kurtosis +- eps) // TODO get a negative value?
    stats.skewness.value should be(skewness +- eps)
    stats.max.value should be(mx +- eps)
    stats.min.value should be(mn +- eps)
    stats.quartile_2.value should be (mean +- eps)

    /*import pt.inescn.plot._
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }

  "Using a Cosine sampler" should "produce a cosine wave" in {
    import pt.inescn.features._
    val frequency = 1
    val amplitude = 2.0
    val phase = Math.toRadians(-45)
    val (d,x) = Fourier.samplingSpecification(500,2)
    val y = Function.Cos(frequency, amplitude, phase, d).toStream.take(x.length) //.sorted  // need to sort for median stats
    val stats = Stats.DescriptiveStats.batch(y)
    val mean = 0.0
    val sd = amplitude / Math.sqrt(2)  // Approximately RMS of signal
    //val kurtosis = 0.0
    val skewness = 0.0
    val mx = amplitude
    val mn = -amplitude
    stats.mean.value should be(mean +- eps)
    stats.standard_deviation.value should be(sd +- eps)
    //stats.kurtosis.value should be(kurtosis +- eps) // TODO get a negative value?
    stats.skewness.value should be(skewness +- eps)
    stats.max.value should be (mx +- eps)
    stats.min.value should be (mn +- eps)
    stats.quartile_2.value should be (mean +- eps)

    /*import pt.inescn.plot._
    Plot.showPlot(x, y)
    Plot.showHistogram(y, 15)*/
  }

  "Using a TimeStamp sampler" should "produce a series of timestamps" in {

    val s1 = Fourier.convertFractionSecondsToDuration(0.1)
    val r1 = Duration.ofMillis(100)
    s1 shouldBe r1
    val s2 = Fourier.convertFractionSecondsToDuration(0.9)
    val r2 = Duration.ofMillis(900)
    s2 shouldBe r2
    val s3 = Fourier.convertFractionSecondsToDuration(0.999)
    val r3 = Duration.ofMillis(999)
    s3 shouldBe r3
    val s4 = Fourier.convertFractionSecondsToDuration(0.099)
    val r4 = Duration.ofMillis(99)
    s4 shouldBe r4
    val s5 = Fourier.convertFractionSecondsToDuration(0.0001)
    val r5 = Duration.ofNanos(100*1000) // micro
    s5 shouldBe r5
    val s6 = Fourier.convertFractionSecondsToDuration(0.000999)
    val r6 = Duration.ofNanos(999*1000) // micro
    s6 shouldBe r6
    val s7 = Fourier.convertFractionSecondsToDuration(0.000000999)
    val r7 = Duration.ofNanos(999)
    s7 shouldBe r7

    val startInstant = "2017-06-16T11:35:54.100000Z"
    val start = Instant.parse(startInstant)
    val t1 = Instant.parse("2017-06-16T11:35:54.102Z")
    val t9 = Instant.parse("2017-06-16T11:35:54.118Z")
    val (dd,_) = Fourier.samplingSpecification(500,2)
    val delta = Fourier.convertFractionSecondsToDuration(dd)
    val len = 10
    val y = Function.TimeStamp(start, delta, start).toStream.take(len)
    y.length shouldBe len
    y.head should be(start)
    y(1) should be(t1)
    y.last should be (t9)

    // TODO: how do we plot time-series with timestamps?
    /*
    val z = Function.Const(1.0,dd).toStream.take(len)
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    //plt += plot(x.take(len), z)
    //plt += plot(y.toList, z.toList)
    plt += plot(y, z)
    plt.title = "Time stamp"
    plt.refresh*/
  }

  "Adding two sampler" should "produce a single series of added values" in {
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (dd,xx) = Fourier.samplingSpecification(500,2)
    val y1 = Function.Sin(frequency, amplitude, phase, dd)
    val y2 = Function.Const(0.5, dd)
    val y3 = Base.Sum(y1,y2)

    val y1t = y1.toStream.take(xx.length)
    val y2t = y2.toStream.take(xx.length)
    val y3t = y3.toStream.take(xx.length)
    val y3r = y1t.zip(y2t).map( p => p._1 + p._2)
    y3t should contain theSameElementsAs y3r

    /*import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, y1t)
    plt += plot(xx, y2t)
    plt += plot(xx, y3t)
    plt.title = "Add"
    plt.refresh*/
  }

  "Subtracting two sampler" should "produce a single series of subtracted values" in {
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (dd,xx) = Fourier.samplingSpecification(500,2)
    val y1 = Function.Sin(frequency, amplitude, phase, dd)
    val y2 = Function.Const(0.5, dd)
    val y3 = Base.Subtract(y1,y2)

    val y1t = y1.toStream.take(xx.length)
    val y2t = y2.toStream.take(xx.length)
    val y3t = y3.toStream.take(xx.length)
    val y3r = y1t.zip(y2t).map( p => p._1 - p._2)
    y3t should contain theSameElementsAs y3r

    /*import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, y1t)
    plt += plot(xx, y2t)
    plt += plot(xx, y3t)
    plt.title = "Subtract"
    plt.refresh*/
  }

  "Multiplying two sampler" should "produce a single series of multiplied values" in {
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (dd,xx) = Fourier.samplingSpecification(500,2)
    val y1 = Function.Sin(frequency, amplitude, phase, dd)
    val y2 = Function.Const(0.5, dd)
    val y3 = Base.Multiply(y1,y2)

    val y1t = y1.toStream.take(xx.length)
    val y2t = y2.toStream.take(xx.length)
    val y3t = y3.toStream.take(xx.length)
    val y3r = y1t.zip(y2t).map( p => p._1 * p._2)
    y3t should contain theSameElementsAs y3r

    /*import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, y1t)
    plt += plot(xx, y2t)
    plt += plot(xx, y3t)
    plt.title = "Multiply"
    plt.refresh*/
  }

  "Dividing two sampler" should "produce a single series of divided values" in {
    val frequency = 1
    val amplitude = 1.0
    val phase = Math.toRadians(-45)
    val (dd,xx) = Fourier.samplingSpecification(500,2)
    val y1 = Function.Sin(frequency, amplitude, phase, dd)
    val y2 = Function.Const(0.5, dd)
    val y3 = Base.Divide(y1,y2)

    val y1t = y1.toStream.take(xx.length)
    val y2t = y2.toStream.take(xx.length)
    val y3t = y3.toStream.take(xx.length)
    val y3r = y1t.zip(y2t).map( p => p._1 / p._2)
    y3t should contain theSameElementsAs y3r

    /*import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, y1t)
    plt += plot(xx, y2t)
    plt += plot(xx, y3t)
    plt.title = "Divide"
    plt.refresh*/
  }

  "Using a DSL to operate on samplers" should "produce a single series of changed values" in {
    val f1 = 1 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(-45)
    val f2 = 2 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val f3 = 4 /* Hz */ ; val a3 = 0.6 ; val phase3 = Math.toRadians(20)
    //val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
    // samplesTo at 500Hz and samplesTo for 2 seconds
    val (dd,xx) = Fourier.samplingSpecification(500,2)

    val sig1 = Function.Sin(f1, a1, phase1, dd)
    val sig2 = Function.Sin(f2, a2, phase2, dd)
    val sig3 = Function.Cos(f3, a3, phase3, dd)
    val sig4 = Distribution.Normal(0, 0.3)
    val sig5 = sig1 - sig2 + sig3 + sig4

    sig5(xx.length).length shouldBe xx.length
    sig5(xx.length) should not contain theSameElementsAs(sig1(xx.length))

    /*
    import breeze.plot._
    val fig = Figure()
    val plt = fig.subplot(0)
    plt += plot(xx, sig1(xx.length))
    plt += plot(xx, sig2(xx.length))
    plt += plot(xx, sig3(xx.length))
    plt += plot(xx, sig4(xx.length))
    plt += plot(xx, sig5(xx.length))
    plt.title = "DSL Ops"
    plt.refresh*/
  }

  "Java discrete multi-class generator" should "generate correct multi-class distribution" in {
    import pt.inescn.utils.JRandomCollection

    val len = 10000
    val denom = len / 100.0
    val eps = 1.8 // %

    val gen1 = new JRandomCollection[Int]()
    gen1.add(0.1, 1)
    gen1.add(0.2, 2)
    gen1.add(0.7, 3)

    val sample1: Seq[Int] = (0 to len).map(_ => gen1.next() )
    val gen1_classes = sample1.groupBy( p => p ).map( p => (p._1, p._2.length) )
    val gen1_class1: Int = gen1_classes(1)
    val gen1_class2: Int = gen1_classes(2)
    val gen1_class3: Int = gen1_classes(3)
    assertThrows[java.util.NoSuchElementException] { gen1_classes(4) }

    /*
    println(s"1 : $gen1_class1 = ${gen1_class1 / denom}")
    println(s"2 : $gen1_class2 = ${gen1_class2 / denom}")
    println(s"3 : $gen1_class3 = ${gen1_class3 / denom}")
    */

    (gen1_class1 / denom) shouldBe (10.0 +- eps)
    (gen1_class2 / denom) shouldBe (20.0 +- eps)
    (gen1_class3 / denom) shouldBe (70.0 +- eps)
  }

  "Scala discrete multi-class generator" should "generate correct multi-class distribution" in {

    val len = 10000
    val denom = len / 100.0
    val eps = 1.5 // %

    val mp = RandomCollection[Int]()

    val mp1 = mp.add(0.1, 1)
    val mp2 = mp1.add(0.2, 2)
    val mp3 = mp2.add(0.7, 3)

    val sample1: Seq[Int] = (0 to len).map(_ => mp3.next )
    val gen1_classes = sample1.groupBy( p => p ).map( p => (p._1, p._2.length) )
    val gen1_class1: Int = gen1_classes(1)
    val gen1_class2: Int = gen1_classes(2)
    val gen1_class3: Int = gen1_classes(3)
    assertThrows[java.util.NoSuchElementException] { gen1_classes(4) }

    /*
    println(s"1 : $gen1_class1 = ${gen1_class1 / denom}")
    println(s"2 : $gen1_class2 = ${gen1_class2 / denom}")
    println(s"3 : $gen1_class3 = ${gen1_class3 / denom}")
    */

    (gen1_class1 / denom) shouldBe (10.0 +- eps)
    (gen1_class2 / denom) shouldBe (20.0 +- eps)
    (gen1_class3 / denom) shouldBe (70.0 +- eps)
  }

  it should "generate correct multi-class distribution dynamically" in {

    val len = 10000
    val denom = len / 100.0
    val eps = 1.5 // %


    val mpt = Map[Int,Double](1 -> 0.1, 2 -> 0.2)
    val mp = RandomCollection[Int](mpt)
    val mp3 = mp.add(0.7, 3)

    val sample1: Seq[Int] = (0 to len).map(_ => mp3.next )
    val gen1_classes = sample1.groupBy( p => p ).map( p => (p._1, p._2.length) )
    val gen1_class1: Int = gen1_classes(1)
    val gen1_class2: Int = gen1_classes(2)
    val gen1_class3: Int = gen1_classes(3)
    assertThrows[java.util.NoSuchElementException] { gen1_classes(4) }

    /*
    println(s"1 : $gen1_class1 = ${gen1_class1 / denom}")
    println(s"2 : $gen1_class2 = ${gen1_class2 / denom}")
    println(s"3 : $gen1_class3 = ${gen1_class3 / denom}")
    */

    (gen1_class1 / denom) shouldBe (10.0 +- eps)
    (gen1_class2 / denom) shouldBe (20.0 +- eps)
    (gen1_class3 / denom) shouldBe (70.0 +- eps)
  }

  "AppendB operations of samplers" should "generate a single discrete sample" in {
    val sampler1 = RangeOf(0, 2, 1)
    val l1 = sampler1.toList
    //println(l1.reverse.mkString(","))
    l1 should have length 3
    val cl1 = List(0,1,2)
    l1.reverse should contain theSameElementsInOrderAs cl1

    val sampler2 = RangeOf(3, 5, 1)
    val l2 = sampler2.toList
    //println(l2.mkString(","))
    l2 should have length 3
    val cl2 = List(3,4,5)
    l2.reverse should contain theSameElementsInOrderAs cl2

    val sampler3: AppendB[Int] = AppendB(Vector(sampler1, sampler2))
    val l3 = sampler3.toList
    //println(l3.mkString(","))
    l3 should have length 6
    val cl3 = List(0,1,2,3,4,5)
    l3.reverse should contain theSameElementsInOrderAs cl3
  }

  it should "generate a single continuous sample" in {
    val rnd = new JDKRandomGenerator(9876)
    val a1 = 0.0
    val b1 = 1.0
    val r1: Distribution.Uniform = Distribution.Uniform(a1, b1, rnd)
    val a2 = 2.0
    val b2 = 4.0
    val r2: Distribution.Uniform = Distribution.Uniform(a2, b2, rnd)

    val sampler1 = AppendU(Vector(r1, r2),rnd)
    val l3 = sampler1.toStream.take(1000).toList
    //println(l3.mkString(","))

    l3.length shouldBe 1000

    val l1 = l3.filter( _ <= 1.0 )
    l1.length shouldBe 500 +- 40
    val stats1 = Stats.DescriptiveStats.batch(l1)
    val mean1 = (a1 + b1) / 2.0
    val median1 = (a1 + b1) / 2.0
    val sd1 = Math.sqrt( Math.pow(b1 - a1,2) / 12.0)
    val kurtosis1 = -6.0/5.0
    val skewness1 = 0.0
    stats1.mean.value should be(mean1 +- eps)
    stats1.quartile_2.value should be (median1 +- eps)
    stats1.standard_deviation.value should be(sd1 +- eps)
    stats1.kurtosis.value should be(kurtosis1 +- eps)
    stats1.skewness.value should be(skewness1 +- eps)

    val l2 = l3.filter( _ > 1.0 )
    l2.length shouldBe 500 +- 40
    val stats2 = Stats.DescriptiveStats.batch(l2)
    val mean2 = (a2 + b2) / 2.0
    val median2 = (a2 + b2) / 2.0
    val sd2 = Math.sqrt( Math.pow(b2 - a2,2) / 12.0)
    val kurtosis2 = -6.0/5.0
    val skewness2 = 0.0
    stats2.mean.value should be(mean2 +- eps)
    stats2.quartile_2.value should be (median2 +- eps)
    stats2.standard_deviation.value should be(sd2 +- eps)
    stats2.kurtosis.value should be(kurtosis2 +- eps)
    stats2.skewness.value should be(skewness2 +- eps)
  }

// TODO: JFX
// TODO: https://stackoverflow.com/questions/13064406/javafx-real-time-linechart-with-time-axis
// TODO: https://gist.github.com/jewelsea/2129306
// TODO: https://github.com/mhrimaz/AwesomeJavaFX
// TODO: https://www.youtube.com/watch?v=VVUE0T9UJ2g
// TODO: https://www.youtube.com/watch?v=L9xtOhdSx6k
// TODO: https://www.youtube.com/watch?v=xk-YfIdH0_Y

// TODO: pipe https://hackernoon.com/operator-in-scala-cbca7b939fc0

// TODO: https://stackoverflow.com/questions/7745959/how-to-simulate-keyboard-presses-in-java
// TODO: http://alvinalexander.com/java/java-robot-class-example-mouse-keystroke
// TODO: https://github.com/kwhat/jnativehook
// TODO: https://github.com/kristian/system-hook
}
