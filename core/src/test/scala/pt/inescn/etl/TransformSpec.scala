package pt.inescn.etl

import java.lang.{System => JSystem}

import better.files.Dsl._
import better.files.File
import org.scalatest._
import pt.inescn.etl.LightSVM.{SparseFold, ValidateDepVar}
import pt.inescn.etl.Load.Source
import pt.inescn.etl.Transform.denseToSparse
import pt.inescn.models.Data.{MatrixData, SparseMatrixData}
import pt.inescn.utils.ADWError

import scala.util.Random

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.etl.TransformSpec"
  *
  * Created by cfo on 20-09-2017.
  */
class TransformSpec extends FlatSpec with Matchers {

  "Scaling functions" should "get correct query results for DenseMatrix" in {

    val labels1 = Array( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0)
    val mat0 : Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(-11.0, -21.0, -31.0, -41.0, -51.0, -61.0, -71.0, -81.0, -91.0),
      Array( -1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)
    )
    val data1 : Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose
    val mat1 = MatrixData.dense(data1, new Random(5587) )
    val sz1 = mat1.size
    sz1._1 shouldBe data1.length       // no. of lines
    sz1._2 shouldBe data1(0).length    // no. columns

    val (nrows,ncols) = mat1.size
    val cols = 1 until ncols
    val zero = (Double.MaxValue, Double.MinValue)
    val min_max: IndexedSeq[(Double, Double)] = cols.map( mat1.apply(_,zero,Transform.minMaxOfCol _))
    min_max should contain theSameElementsInOrderAs Vector((10.0,90.0), (-91.0,-11.0), (-1.1,9.1))

    // Set the limits of the scaling functions
    val x_min = -1.0
    val x_max = +1.0
    val range_scale = Transform.scale(x_min,x_max) _
    // Generate a scaling function for each column (except te first one)
    val scale: IndexedSeq[Double => Double] = min_max.map { range =>
      val (min,max) = range
      range_scale(min,max)
    }
    // Now apply the transformation for each columns
    cols.foreach{ col => mat1.apply(col, scale(col-1) ) }
    mat1.getColumn(0) should contain theSameElementsInOrderAs labels1
    mat1.getColumn(1) should contain theSameElementsInOrderAs Vector(-1.0,-0.75,-0.5,-0.25,0.0,0.25,0.5,0.75,1.0)
    mat1.getColumn(2) should contain theSameElementsInOrderAs Vector(1.0,0.75,0.5,0.25,0.0,-0.25,-0.5,-0.75,-1.0)
    mat1.getColumn(3) should contain theSameElementsInOrderAs
      Vector(-1.0,-0.37254901960784303,-0.17647058823529405,0.03921568627451011,0.21568627450980382,0.4117647058823528,0.607843137254902,0.803921568627451,1.0)

  }

  it should "get correct query results for SparseMatrix" in {

    val labels1 = Array( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0)
    val mat0 : Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(-11.0, -21.0, -31.0, -41.0, -51.0, -61.0, -71.0, -81.0, -91.0),
      Array( -1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)
    )
    val data0 : Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose

    val data1 = data0.map{ row =>
      val full = row.zipWithIndex.map( p => (p._2,p._1)).toMap
      val yVar: Double = full(0)
      val rest: Map[Int, Double] = full - 0
      (yVar, rest)
    }
    val mat1 = MatrixData.sparse(data1, new Random(5587) )
    val sz1 = mat1.size
    sz1._1 shouldBe data0.length       // no. of lines
    sz1._2 shouldBe data0(0).length    // no. columns

    val (nrows,ncols) = mat1.size
    val cols = 1 until ncols
    val zero = (Double.MaxValue, Double.MinValue)
    val min_max: IndexedSeq[(Double, Double)] = cols.map( mat1.apply(_,zero,Transform.minMaxOfCol _))
    min_max should contain theSameElementsInOrderAs Vector((10.0,90.0), (-91.0,-11.0), (-1.1,9.1))

    // Set the limits of the scaling functions
    val x_min = -1.0
    val x_max = +1.0
    val range_scale = Transform.scale(x_min,x_max) _
    // Generate a scaling function for each column (except te first one)
    val scale: IndexedSeq[Double => Double] = min_max.map { range =>
      val (min,max) = range
      range_scale(min,max)
    }
    // Now apply the transformation for each columns
    cols.foreach{ col => mat1.apply(col, scale(col-1) ) }
    mat1.getColumn(0) should contain theSameElementsInOrderAs labels1
    mat1.getColumn(1) should contain theSameElementsInOrderAs Vector(-1.0,-0.75,-0.5,-0.25,0.0,0.25,0.5,0.75,1.0)
    mat1.getColumn(2) should contain theSameElementsInOrderAs Vector(1.0,0.75,0.5,0.25,0.0,-0.25,-0.5,-0.75,-1.0)
    mat1.getColumn(3) should contain theSameElementsInOrderAs
      Vector(-1.0,-0.37254901960784303,-0.17647058823529405,0.03921568627451011,0.21568627450980382,0.4117647058823528,0.607843137254902,0.803921568627451,1.0)

  }

  "Normalization functions" should "get correct query results for DenseMatrix" in {

    val labels1 = Array( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0)
    val mat0 : Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(-11.0, -21.0, -31.0, -41.0, -51.0, -61.0, -71.0, -81.0, -91.0),
      Array(-1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)
    )
    val data1 : Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose
    val mat1 = MatrixData.dense(data1, new Random(5587) )
    val sz1 = mat1.size
    sz1._1 shouldBe data1.length       // no. of lines
    sz1._2 shouldBe data1(0).length    // no. columns

    // Mean
    val (nrows,ncols) = mat1.size
    val cols = 0 until ncols
    val means = cols.map( col => Transform.mean(col,mat1) )
    means should contain theSameElementsInOrderAs Vector(1.0, 50.0, -51.0, 4.866666666666666)

    // Variance
    val vars = cols.map( col => Transform.variance(col,mat1,isBiasCorrected = true) )
    vars should contain theSameElementsInOrderAs Vector(0.5, 750.0, 750.0, 10.22)

    // Variance
    val sds = cols.map( col => Transform.standardDeviation(col,mat1) )
    sds should contain theSameElementsInOrderAs Vector(0.7071067811865476, 27.386127875258307, 27.386127875258307, 3.1968734726291563)

    Transform.normalize(mat1, scaleY = true)
    // println(mat1.toString)
    /*
      # http://stat.ethz.ch/R-manual/R-devel/library/base/html/scale.html

      m = matrix(c(
        c( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0),
        c(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
        c(-11.0, -21.0, -31.0, -41.0, -51.0, -61.0, -71.0, -81.0, -91.0),
        c(-1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)),
        9,4
      )
      m

      scale(m)

                [,1]       [,2]       [,3]        [,4]
       [1,]  0.000000 -1.4605935  1.4605935 -1.86640689
       [2,] -1.414214 -1.0954451  1.0954451 -0.86542889
       [3,]  0.000000 -0.7302967  0.7302967 -0.55262327
       [4,]  1.414214 -0.3651484  0.3651484 -0.20853708
       [5,]  0.000000  0.0000000  0.0000000  0.07298798
       [6,]  0.000000  0.3651484 -0.3651484  0.38579360
       [7,]  1.414214  0.7302967 -0.7302967  0.69859923
       [8,]  0.000000  1.0954451 -1.0954451  1.01140485
       [9,] -1.414214  1.4605935 -1.4605935  1.32421047
    */
    val col0 = mat1.getColumn(0)
    col0 should contain theSameElementsInOrderAs Array( 0.0, -1.414213562373095, 0.0, 1.414213562373095, 0.0, 0.0, 1.414213562373095, 0.0, -1.414213562373095 )
    val col1 = mat1.getColumn(1)
    col1 should contain theSameElementsInOrderAs Array(-1.4605934866804429, -1.0954451150103321, -0.7302967433402214, -0.3651483716701107, 0.0, 0.3651483716701107, 0.7302967433402214, 1.0954451150103321, 1.4605934866804429)
    val col2 = mat1.getColumn(2)
    col2 should contain theSameElementsInOrderAs Array(1.4605934866804429, 1.0954451150103321, 0.7302967433402214, 0.3651483716701107, 0.0, -0.3651483716701107, -0.7302967433402214, -1.0954451150103321, -1.4605934866804429)
    val col3 = mat1.getColumn(3)
    col3 should contain theSameElementsInOrderAs Array(-1.866406887151399, -0.8654288918076317, -0.5526232682627046, -0.2085370823632846, 0.0729879788271497, 0.3857936023720769, 0.6985992259170041, 1.0114048494619312, 1.3242104730068585)
  }

  it should "get correct query results for SparseMatrix" in {

    val labels1 = Array( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0)
    val mat0 : Array[Array[Double]] = Array(
      Array(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
      Array(-11.0, -21.0, -31.0, -41.0, -51.0, -61.0, -71.0, -81.0, -91.0),
      Array(-1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)
    )
    val data0 : Array[Array[Double]] = Array(
      labels1,
      mat0(0),
      mat0(1),
      mat0(2)
    ).transpose

    val data1 = data0.map{ row =>
      val full = row.zipWithIndex.map( p => (p._2,p._1)).toMap
      val yVar: Double = full(0)
      val rest: Map[Int, Double] = full - 0
      (yVar, rest)
    }
    val mat1 = MatrixData.sparse(data1, new Random(5587) )
    val sz1 = mat1.size
    sz1._1 shouldBe data0.length       // no. of lines
    sz1._2 shouldBe data0(0).length    // no. columns

    // Mean
    val (nrows,ncols) = mat1.size
    val cols = 0 until ncols
    val means = cols.map( col => Transform.mean(col,mat1) )
    means should contain theSameElementsInOrderAs Vector(1.0, 50.0, -51.0, 4.866666666666666)

    // Variance
    val vars = cols.map( col => Transform.variance(col,mat1,isBiasCorrected = true) )
    vars should contain theSameElementsInOrderAs Vector(0.5, 750.0, 750.0, 10.22)

    // Variance
    val sds = cols.map( col => Transform.standardDeviation(col,mat1) )
    sds should contain theSameElementsInOrderAs Vector(0.7071067811865476, 27.386127875258307, 27.386127875258307, 3.1968734726291563)

    Transform.normalize(mat1, scaleY = true)
    // println(mat1.toString)
    /*
      # http://stat.ethz.ch/R-manual/R-devel/library/base/html/scale.html

      m = matrix(c(
        c( 1.0,  0.0,  1.0,  2.0,  1.0,  1.0,  2.0,  1.0,  0.0),
        c(10.0, 20.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0),
        c(-11.0, -21.0, -31.0, -41.0, -51.0, -61.0, -71.0, -81.0, -91.0),
        c(-1.1,  2.1,  3.1,  4.2,  5.1,  6.1,  7.1,  8.1,  9.1)),
        9,4
      )
      m

      scale(m)

                [,1]       [,2]       [,3]        [,4]
       [1,]  0.000000 -1.4605935  1.4605935 -1.86640689
       [2,] -1.414214 -1.0954451  1.0954451 -0.86542889
       [3,]  0.000000 -0.7302967  0.7302967 -0.55262327
       [4,]  1.414214 -0.3651484  0.3651484 -0.20853708
       [5,]  0.000000  0.0000000  0.0000000  0.07298798
       [6,]  0.000000  0.3651484 -0.3651484  0.38579360
       [7,]  1.414214  0.7302967 -0.7302967  0.69859923
       [8,]  0.000000  1.0954451 -1.0954451  1.01140485
       [9,] -1.414214  1.4605935 -1.4605935  1.32421047
    */
    val col0 = mat1.getColumn(0)
    col0 should contain theSameElementsInOrderAs Array( 0.0, -1.414213562373095, 0.0, 1.414213562373095, 0.0, 0.0, 1.414213562373095, 0.0, -1.414213562373095 )
    val col1 = mat1.getColumn(1)
    col1 should contain theSameElementsInOrderAs Array(-1.4605934866804429, -1.0954451150103321, -0.7302967433402214, -0.3651483716701107, 0.0, 0.3651483716701107, 0.7302967433402214, 1.0954451150103321, 1.4605934866804429)
    val col2 = mat1.getColumn(2)
    col2 should contain theSameElementsInOrderAs Array(1.4605934866804429, 1.0954451150103321, 0.7302967433402214, 0.3651483716701107, 0.0, -0.3651483716701107, -0.7302967433402214, -1.0954451150103321, -1.4605934866804429)
    val col3 = mat1.getColumn(3)
    col3 should contain theSameElementsInOrderAs Array(-1.866406887151399, -0.8654288918076317, -0.5526232682627046, -0.2085370823632846, 0.0729879788271497, 0.3857936023720769, 0.6985992259170041, 1.0114048494619312, 1.3242104730068585)
  }

  "vector normalization" should "produce same results as R" in {
    val data = Array(1.0,2,3,4,5,6,7,8,9,10)

    val n1 = Transform.normalize(data)
    /* normalize
    mydata = c(1,2,3,4,5,6,7,8,9,10)
    scale(mydata)*/
    //               (-1.4863011,          -1.1560120,          -0.8257228,          -0.4954337,           -0.1651446,
    n1 shouldBe Array(-1.4863010829205867, -1.1560119533826787, -0.8257228238447705, -0.49543369430686224, -0.1651445647689541,
    // 0.1651446,          0.4954337,           0.8257228,          1.1560120,          1.4863011)
       0.1651445647689541, 0.49543369430686224, 0.8257228238447705, 1.1560119533826787, 1.4863010829205867)

    /* scale
    lapply(mydata, function(x)(x-min(x))/(max(x)-min(x)))
    n <- function(x) { (x - min(x)) / (max(x)- min(x)) }
    n(mydata)*/
    val n2 = Transform.scale(0.0, 1.0, data)
    //                0.0000000 0.1111111           0.2222222           0.3333333           0.4444444
    n2 shouldBe Array(0.0,      0.1111111111111111, 0.2222222222222222, 0.3333333333333333, 0.4444444444444444,
      // 0.5555556        0.6666667           0.7777778           0.8888889           1.0000000
      0.5555555555555556, 0.6666666666666666, 0.7777777777777778, 0.8888888888888888, 1.0)

    /* unit vector
    scalar1 <- function(x) {x / sqrt(sum(x^2))}
    scalar1(mydata) */
    val n3 = Transform.unit(data)
    //                0.05096472            0.10192944           0.15289416           0.20385888
    n3 shouldBe Array(0.050964719143762556, 0.10192943828752511, 0.15289415743128767, 0.20385887657505022,
    // 0.25482360          0.30578831           0.35675303          0.40771775           0.45868247         0.50964719
       0.2548235957188128, 0.30578831486257535, 0.3567530340063379, 0.40771775315010045, 0.458682472293863, 0.5096471914376256)

    val data1 = Array(1.0,2,3,4,1,2,3)
    val n4 = Transform.softmax(data1)
    //                0.024,                0.064,               0.175,               0.475,
    n4 shouldBe Array(0.023640543021591385, 0.06426165851049616, 0.17468129859572226, 0.4748329997443803,
    // 0.024,                0.064,               0.175
       0.023640543021591385, 0.06426165851049616, 0.17468129859572226)

    /*
    Book "Data Mining with R, learning with case studies" by Luis Torgo, CRC Press 2010.
    library("DMwR2")
    SoftMax(mydata, lambda = 2)(1) */
    val n5 = Transform.softLambdaMax(data)
    //                0.009291203,         0.025788025,          0.069520087,         0.174156516
    n5 shouldBe Array(0.00929120338542677, 0.025788025410358316, 0.06952008692520315, 0.1741565163416285,
    //0.373128912,         0.626871088,        0.825843484,        0.930479913,        0.974211975         0.990708797
      0.37312891158966693, 0.6268710884103331, 0.8258434836583716, 0.9304799130747967, 0.9742119745896417, 0.9907087966145732)

    /*
      library("psych")
      logistic(mydata,d=0, a=1,c=0, z=1)
     */
    val n6 = Transform.logit(data)
    //                0.7310586           0.8807971           0.9525741           0.9820138           0.9933071
    n6 shouldBe Array(0.7310585786300049, 0.8807970779778823, 0.9525741268224334, 0.9820137900379085, 0.9933071490757153,
    // 0.9975274           0.9990889           0.9996646           0.9998766           0.9999546
       0.9975273768433653, 0.9990889488055994, 0.9996646498695336, 0.9998766054240137, 0.9999546021312976)

  }

  "Conversion function" should "convert dense to sparse" in {
    val dense_1 = Array(
      Array(0.0, 1.0, 2.0),
      Array(3.0, 4.0, 5.0),
      Array(6.0, 7.0, 8.0)
    )

    val sparse = denseToSparse(dense_1)

    import pt.inescn.etl.Save._
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    val trainData = tmp / "sparse2.csv"
    saveSparse(sparse, trainData.toString)

    val tmp1 = Source.libSVMSparse(ValidateDepVar.binaryClassification, SparseFold.binaryClassFail)

    val a1ar1: Either[ADWError, SparseMatrixData] = tmp1.map(trainData)
    a1ar1.isLeft shouldBe true
    // println(a1ar1)
    // TODO: a1ar1.isRight shouldBe true
    /*val a1a: SparseMatrixData = a1ar1.right.get
    // Data order has been reversed -> math.abs(i-len)-1 instead of i

    val len = sparse.length
    for(i <- sparse.indices){
      val a1a_r: (Double, a1a.SparseFeature) = a1a(math.abs(i-len)-1)
      a1a_r shouldBe sparse(i)
    }*/
  }
}
