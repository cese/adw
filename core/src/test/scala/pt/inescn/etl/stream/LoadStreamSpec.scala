package pt.inescn.etl.stream

import java.time.{Duration, LocalDateTime}
import scala.collection.AbstractIterator
import scala.collection.mutable.ArraySeq
import scala.language.existentials

import org.scalatest.{FlatSpec, Matchers}

import com.github.tototoshi.csv.DefaultCSVFormat
import pt.inescn.app.Utils
import pt.inescn.etl.Load.format
import pt.inescn.etl.stream.Load._


object MyFormat extends DefaultCSVFormat {
  override val delimiter = ',' // ';'
}

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "root/testOnly pt.inescn.etl.stream.LoadStreamSpec"
  *
  * For full stack trace:
  * "~testOnly pt.inescn.etl.stream.LoadStreamSpec -- -oF"
  * @see http://www.scalatest.org/user_guide/using_scalatest_with_sbt
  *
  * Created by hmf on 07-06-2017.
  */
class LoadStreamSpec extends FlatSpec with Matchers {


  "Row Vals" should "be automatically created via implicits" in {

    // We can wrap the values automatically
    // Note however that we mus make the Val type explicit
    val r0: Val[String] = "100"
    //println(s"r0: Val(${r0.v}) : ${r0.v.getClass}")
    r0 shouldBe an[Val[String]]
    r0.v shouldBe an[String]

    val r1: Val[Double] = 200.0
    //println(s"r1: Val(${r1.v}) : ${r1.v.getClass}")
    r1 shouldBe an[Val[java.lang.Double]]
    r1.v shouldBe an[java.lang.Double]

    val r2: Val[Array[Double]] = Array(300.0, 301, 302)
    //println(s"r2: Val(${r2.v.mkString(",")}) : ${r2.v.getClass}")
    r2 shouldBe an[Val[Array[Double]]]
    r2.v shouldBe an[Array[Double]]

    // We can generate vector versions automatically
    // Note that we need to type the Val, but we need not add all typing information
    val r3: Val[_] = r0.arr(5)
    //println(s"r3: $r3) : ${r3.getClass}")
    //println(s"r3: Val(${r3.v}) : ${r3.v.getClass}")
    r3.v shouldBe an[Array[String]]

    // If don't add the Val explicilty, it will still be used implicitly
    val r4: Array[Double] = r1.arr(5)
    //println(s"r4: Val(${r4.v.mkString(",")}) : ${r4.v.getClass}")
    r4.v shouldBe an[Array[Double]]

    val r5 = r2.arr(5)
    //println(s"r7: Val(${r7.v.mkString(",")}) : ${r7.v.getClass}")
    r5 shouldBe an[Array[Array[Double]]]

    // Untyped Val's
    val rs = Map("r1" -> r1, "r2" -> r2, "r3" -> r3)
    val r1m = rs("r1")
    //println(s"rs: ${r1m.v} : ${r1m.v.getClass}")
    r1m shouldBe an[Val[java.lang.Double]]
    r1m.v shouldBe an[java.lang.Double]

    val r2m = rs("r2")
    r2m shouldBe an[Val[Array[Double]]]
    r2m.v shouldBe an[Array[Double]]
  }

  it should "allow implicit conversions" in {
    val v1: Val[String] = "100.0"
    val v2: Val[Double] = 200.0
    val v3: Val[Int] = 300

    val s1: Either[String, String] = v1
    val s2: Either[String, Double] = v2
    val s3: Either[String, Int] = v3

    val r: Either[String, Double] = for {
      t <- s1
      t1 = t.toDouble
      t2 <- s2
      t3 <- s3
      ts = t1 + t2 + t3
    } yield ts
    r shouldBe 'right
    r.right.get shouldBe 600.0

    val ri: Val[Double] = for {
      v1t:String <- v1
      v1d = v1t.toDouble
      v2d:Double <- v2
      v3d:Int <- v3
      ts: Double = v1d + v2d + v3d
    } yield ts
    ri shouldBe a[Val[_]]
    ri should not be a[Empty[_]]
    ri shouldBe Val(600.0)

    /*
    val re: Val[Double] = for {
      v1t:String <- v1
      v1d = v1t.toDouble
      v2d:Double <- v2
      v3d:Double <- v3 // Its an Int, won't compile
      ts: Double = v1d + v2d + v3d
    } yield ts
    re shouldBe a[Val[_]]
    re should not be a[Empty[_]]
    re shouldBe Val(600.0)
    */

    /*Doesn't work, unknown type
    val hidden: Val[_] = v3
    val rr: Val[Double] = for {
      v1t:String <- v1
      v1d = v1t.toDouble
      v2d:Double <- v2
      v3d:Double <- hidden // Its an Int, won't compile
      ts: Double = v1d + v2d + v3d
    } yield ts
    rr shouldBe a[Val[_]]
    rr should not be a[Empty[_]]
    rr shouldBe Val(600.0)
    */

  }

  "Rows" should "should allow manipulation of Val" in {

    // Initialize with full typing
    val r0: Val[String] = "100"
    val r1: Val[Double] = 200.0
    val r2: Val[_] = "300"
    val r4: Val[_] = "400"
    val r5: Val[_] = 400

    // Store and check that typing is still visible
    val row1 = Row("r0" -> r0, "r1" -> r1, "r2" -> r2)
    row1.size shouldBe 3
    row1.v.colNames should contain theSameElementsAs List("r0", "r1", "r2")
    row1.colTypes should contain theSameElementsAs Map(
      "r0" -> classOf[java.lang.String],
      "r1" -> classOf[java.lang.Double],
      "r2" -> classOf[java.lang.String])
    //println(row1.colTypes.mkString(","))

    // apply single parameter and replace field
    // Lets convert several columns to Double
    val row3 = row1((e: String) => e.toDouble, "r0", "r2") //uses implicits (unpack, pack)
    row3.colTypes should contain theSameElementsAs Map(
      "r0" -> classOf[java.lang.Double],
      "r1" -> classOf[java.lang.Double],
      "r2" -> classOf[java.lang.Double])
    //println(row3.r.mkString(","))

    // apply single parameter and add field
    val row4 = row1((e: String) => e.toDouble, "r0" -> "r0d", "r2" -> "r2d") //uses implicits (unpack, pack)
    row4.colTypes should contain theSameElementsAs Map(
      "r0" -> classOf[String],
      "r0d" -> classOf[java.lang.Double],
      "r1" -> classOf[java.lang.Double],
      "r2" -> classOf[String],
      "r2d" -> classOf[java.lang.Double])

    // Convert all these columns to an array of double
    //val row3Arr = row3.arr((_:Double) => Double)
    val row3Arr = row3.arr((e: Double) => e)
    //println(row3Arr.mkString(","))
    row3Arr should contain theSameElementsAs Array(300.0, 200.0, 100.0)

    // apply 2 parameters
    // adds new fields
    val row5 = row3((a: Double, b: Double) => a + b, ("r0", "r1", "r0_1"), ("r0", "r2", "r0_2"))
    //println(row4.r.mkString(","))
    val row5Arr = row5.arr((e: Double) => e)
    //println(row4Arr.mkString(","))
    row5Arr should contain theSameElementsAs Array(400.0, 300.0, 200.0, 300.0, 100.0)


    val row2 = Row("r4" -> r4, "r7" -> r5)
    val row6 = row2 ++ row4
    //println(row6.r.mkString(","))
    row6("r0") shouldBe r0
    row6.colNames should contain theSameElementsAs List("r0", "r0d", "r1", "r2", "r2d", "r4", "r7")

    val nr0: Val[_] = "0"
    val row7 = row6.updated("r0", nr0)
    row7("r0") shouldBe nr0

    val row8 = row7 - "r0"
    row8.get("r0") shouldBe empty

    val row9 = row8 + ("r0" -> r0)
    row9.get("r0") shouldBe Some(r0)

    val row10 = row9.project("r0", "r1", "r2").apply((e: String) => e.toDouble, "r0", "r2")
    //println(row10.colTypes.mkString(","))
    val row10Arr = row10.arr((e: Double) => e)
    //println(row10Arr.mkString(","))
    row10Arr should contain theSameElementsAs Array(300.0, 200.0, 100.0)

    val SUM = "sum"
    val row11 = row10.foldLeft(Row(SUM -> pack(0.0))) { case (accRow, (k, v)) =>
      val t = applyArgs((a1: Double, a2: Double) => a1 + a2, accRow(SUM), v)
      Row(SUM -> t)
    }
    //println(row10.r.mkString(","))
    //println(row11.r.mkString(","))
    val v600: Val[_] = 600.0
    //println(v600.v.getClass)
    //println(row11(SUM).v.getClass)
    // Check that Val's equal is correct
    row11.get(SUM) shouldBe Some(v600)
    row11.get(SUM).get shouldBe Some(v600).get
    row11.get(SUM).get shouldBe v600
    row11.get(SUM).get.v shouldBe v600.v


  }

  "Frame" should "allow collection into vectors and arrays" in {

    // Create a fully typed array and access it elements
    val av: Val[String] = "100"
    val a: Val[Array[String]] = av.arr(5)
    //println(s"a = ${a.v.mkString(",")}")
    //paramInfo(a)
    //paramInfoX(a)
    val au: Either[String, Array[String]] = unpack[Array[String]](a)
    //println(au)
    au shouldBe 'right
    au.right.get shouldBe Array("100", "100", "100", "100", "100")
    au.right.get shouldBe an[Array[String]]

    // Create and use a wrapper with an opaque value (dependent types)
    //val avt:Val[String] = "100"
    val avt: Val[_] = "100"
    //print("pack Val[_]: "); paramInfo(avt.v)
    //val tavt: Either[String, Val[_]] = unpack(avt)
    //print("unpack Val[_]: "); paramInfo(tavt.right.get)
    // Now duplicate elements and check that we still have access to the inner
    // value. Note however that we cannot indicate the correct inner type due
    // to erasure (its an object)
    val at: Val[Array[_]] = avt.arr(5)
    //paramInfo(at)
    //paramInfoX(at)
    at.v shouldBe Array("100", "100", "100", "100", "100")
    // So we also cannot check it type
    at.v should not be an[Array[String]]


    // Lets use an opaque Vector instead
    val vat: Vector[_] = avt.vec(5)
    //print(s"Vector(vat=$vat) ") ; paramInfo(vat)
    // We still have access to the inner type
    //val vat0 = vat(0).asInstanceOf[String]
    vat(0) shouldBe an[String]
    //println(s"vat0 = $vat0 ${vat0.getClass}")
    // Lets pack it into a Val
    val vvat: Val[Vector[_]] = avt.vec(5)
    //print(s"Vector(vvat=$vvat) ") ; paramInfo(vvat)
    // We still have access to the inner  type
    val vat1 = vvat.v
    //println(s"vat1 = $vat1 ${vat1.getClass}")
    val str1: String = vat1(0).asInstanceOf[String]
    str1 shouldBe "100"
    // We can unpack without casting to the correct type
    //println(s"str1 = $str1 ${str1.getClass}")
    val vut = unpack(vvat) // Either[String, Nothing] ??
    //println(vut)
    vut shouldBe 'right
    // But we still can cast to the correct type
    val vec: Vector[String] = vut.right.get
    //println(s"vec(0) = ${vec(0)} @ ${vec(0).getClass}")
    // value shouldBe is not a member of Nothing
    //vut.right.get shouldBe an[Vector[String]]

    // And we can convert it to a Java generic for easy integration
    val arrVut: Array[String] = vec.toArray
    //println(s"arrVut = ${arrVut.mkString(",")}")
    arrVut shouldBe Array("100", "100", "100", "100", "100")

    // But if we unpack it to the correct type, we can use it
    val vut2 = unpack[Vector[String]](vvat)
    //println(vut2)
    vut2 shouldBe 'right
    val vec2: Vector[String] = vut2.right.get
    //println(s"vec2(0) = ${vec2(0)} @ ${vec2(0).getClass}")
    vec2 shouldBe an[Vector[String]]
    vec2 shouldBe Vector("100", "100", "100", "100", "100")

    // If we unpack it to an opaque Vector type, we can still use it
    val vut3 = unpack[Vector[_]](vvat)
    //println(vut3)
    vut3 shouldBe 'right
    //val vec3:Vector[String] = vut2.right.get  // why does this compile?
    val vec3: Vector[_] = vut2.right.get
    //println(s"vec3(0) = ${vec3(0)} @ ${vec3(0).getClass}")
    vec3 shouldBe an[Vector[String]]
    vec3 shouldBe Vector("100", "100", "100", "100", "100")


    // The use of n array is problematic due to erasure.
    // Use of class and type tags did not help
    // Here we will use an opaque type (cannot type it to string)
    val wvat: Array[_] = avt.arr(5)
    //print(s"Array(vat=$wvat) ") ; paramInfo(wvat)
    wvat(0) shouldBe an[String]
    val wvat0 = wvat(0).asInstanceOf[String]
    //println(s"wvat0 = $wvat0 ${wvat0.getClass}")
    wvat0 shouldBe "100"
    // Lets pack the data and see if we can still access the
    // contained element in the array.
    val wvvat: Val[Array[_]] = avt.arr(5)
    //print(s"Array(wvvat=$wvvat) ") ; paramInfo(wvvat)
    val wvat1 = wvvat.v
    //println(s"wvat1 = $wvat1 ${wvat1.getClass}")
    wvat1(0) shouldBe an[String]
    val wstr1: String = wvat1(0).asInstanceOf[String]
    //println(s"wstr1 = $wstr1 ${wstr1.getClass}")
    wstr1 shouldBe "100"
    // Can we unpack to the correct (inner) type, it seems like it
    //val wvut = unpack(wvvat) // Either[String, Nothing] ??
    val wvut = unpack[Array[String]](wvvat) // Either[String, Nothing] ??
    //println(wvut)
    wvut shouldBe 'right
    // But this won't work
    wvut.right.get should not be an[Array[String]]
    // However this will
    wvut.right.get shouldBe an[Array[_]]
    // No casting helps, so keep it opaque and it works !?
    //val wvec:Array[String] = wvut.right.get
    //val wvec:Array[String] = wvut.right.get.asInstanceOf[Array[String]]
    val wvec: Array[_] = wvut.right.get
    //println(s"wvec(0) = ${wvec(0)} @ ${wvec(0).getClass}")
    wvec should not be an[Array[String]]
    //wvec.isInstanceOf[Array[String]] shouldBe true
    // But this work ?
    wvec shouldBe Array("100", "100", "100", "100", "100")
    wvec(0) shouldBe an[String]


    // It is important to note that when we unpack from an opaque type
    // array, even though we cannot cast to check the inner String type we
    // can still access the data correctly. Simply have to unpack to an
    // unknown type
    val aut: Either[String, Array[String]] = unpack[Array[String]](at)
    //println(aut)
    aut shouldBe 'right
    aut.right.get shouldBe Array("100", "100", "100", "100", "100")
    // Will fail if the initial Val is untyped, only have info of Array[Object]
    //val arr:Array[String] = aut.right.get
    // fails with an error when casting object to String
    //val arr = aut.right.get
    val arr: Array[_] = aut.right.get
    //println(arr(0).getClass)
    arr(0) shouldBe an[String]
    // fails because it is an Array[Object] and not an Array[String]
    //aut.right.get shouldBe an[Array[String]]
    aut.right.get shouldBe an[Array[_]]

  }

  /*
  it should "handle typed rows" in {
    import shapeless._

    // https://github.com/milessabin/shapeless/wiki/Feature-overview:-shapeless-2.0.0
    import shapeless._ ; import syntax.singleton._ ; import record._

    val row1 =
        ("x" ->> "100") ::
        ("y" ->> "200") ::
        ("z" ->>  300.0) ::
        HNil

    val x1:String = row1.get("x")

    //println(s"x1 = $x1")

  }*/
  it should "allow dropping rows " in {
    val colx1 = ("x", Seq("1", "1", "1", "1", "1", "3", "3", "3", "3", "3", "5", "5", "5", "5", "5", "1", "1", "1", "1", "1"))
    val coly1 = ("y", Seq("11", "11", "11", "11", "11", "13", "13", "13", "13", "13", "15", "15", "15", "15", "15", "11", "11", "11", "11", "11"))
    val colz1 = ("z", Seq("21", "21", "21", "21", "21", "23", "23", "23", "23", "23", "25", "25", "25", "25", "25", "21", "21", "21", "21", "21"))
    val cols1 = Frame(colx1, coly1, colz1)
    //println(cols1.colSize)
    cols1.colSize shouldBe 3
    cols1.colIDs should contain theSameElementsAs List("x", "y", "z")
    cols1.v.colNames should contain theSameElementsAs List("x", "y", "z")
    cols1.colTypes should contain theSameElementsAs Map(
      "x" -> classOf[String],
      "y" -> classOf[String],
      "z" -> classOf[String])
    //val read1: IndexedSeq[Row] = cols1.iterable.iterator.toIndexedSeq
    //println(read1.mkString(";\n"))
    //println(s"read1 size : ${read1.size}")

    val cols2 = cols1.drop(5)
    val read2: IndexedSeq[Row] = cols2.iterable.iterator.toIndexedSeq
    //println(read2.mkString(";\n"))
    //println(s"read2 size : ${read2.size}")
    read2.size shouldBe 15
  }

  it should "allow manual creation of columns" in {
    val colx1 = ("x", Seq("1", "1", "1", "1", "1", "3", "3", "3", "3", "3", "5", "5", "5", "5", "5", "1", "1", "1", "1", "1"))
    val coly1 = ("y", Seq("11", "11", "11", "11", "11", "13", "13", "13", "13", "13", "15", "15", "15", "15", "15", "11", "11", "11", "11", "11"))
    val colz1 = ("z", Seq("21", "21", "21", "21", "21", "23", "23", "23", "23", "23", "25", "25", "25", "25", "25", "21", "21", "21", "21", "21"))
    val cols1 = Frame(colx1, coly1, colz1)
    //println(cols1.colSize)
    cols1.colSize shouldBe 3
    cols1.colIDs should contain theSameElementsAs List("x", "y", "z")
    cols1.v.colNames should contain theSameElementsAs List("x", "y", "z")
    cols1.colTypes should contain theSameElementsAs Map(
      "x" -> classOf[String],
      "y" -> classOf[String],
      "z" -> classOf[String])
    //val cols0: IndexedSeq[Row] = cols1.iterable.iterator.toIndexedSeq
    //println(cols0.mkString(";\n"))


    val conv0 = cols1.window(4, 2)
    val conv0d: IndexedSeq[Row] = conv0.iterable.iterator.toIndexedSeq
    //println(conv0.colTypes)
    conv0d.length shouldBe 9
    val expectedR = Vector(
      Map("x" -> Val(ArraySeq("1", "1", "1", "1")), "y" -> Val(ArraySeq("11", "11", "11", "11")), "z" -> Val(ArraySeq("21", "21", "21", "21"))),
      Map("x" -> Val(ArraySeq("1", "1", "1", "3")), "y" -> Val(ArraySeq("11", "11", "11", "13")), "z" -> Val(ArraySeq("21", "21", "21", "23"))),
      Map("x" -> Val(ArraySeq("1", "3", "3", "3")), "y" -> Val(ArraySeq("11", "13", "13", "13")), "z" -> Val(ArraySeq("21", "23", "23", "23"))),
      Map("x" -> Val(ArraySeq("3", "3", "3", "3")), "y" -> Val(ArraySeq("13", "13", "13", "13")), "z" -> Val(ArraySeq("23", "23", "23", "23"))),
      Map("x" -> Val(ArraySeq("3", "3", "5", "5")), "y" -> Val(ArraySeq("13", "13", "15", "15")), "z" -> Val(ArraySeq("23", "23", "25", "25"))),
      Map("x" -> Val(ArraySeq("5", "5", "5", "5")), "y" -> Val(ArraySeq("15", "15", "15", "15")), "z" -> Val(ArraySeq("25", "25", "25", "25"))),
      Map("x" -> Val(ArraySeq("5", "5", "5", "1")), "y" -> Val(ArraySeq("15", "15", "15", "11")), "z" -> Val(ArraySeq("25", "25", "25", "21"))),
      Map("x" -> Val(ArraySeq("5", "1", "1", "1")), "y" -> Val(ArraySeq("15", "11", "11", "11")), "z" -> Val(ArraySeq("25", "21", "21", "21"))),
      Map("x" -> Val(ArraySeq("1", "1", "1", "1")), "y" -> Val(ArraySeq("11", "11", "11", "11")), "z" -> Val(ArraySeq("21", "21", "21", "21")))
    )
    conv0d.map(_.r) should contain theSameElementsInOrderAs expectedR

    // From val dataFile: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_1000_cmdn_ens1.csv"

    val ts = List(
      0.000000, 0.000195, 0.000391, 0.000586, 0.000781,
      0.000977, 0.001172, 0.001367, 0.001563, 0.001758,
      0.001953, 0.002148, 0.002344, 0.002539, 0.002734,
      0.002930, 0.003125, 0.003320, 0.003516, 0.003711)
    val colt2 = ("t", List(
      "0.000000", "0.000195", "0.000391", "0.000586", "0.000781",
      "0.000977", "0.001172", "0.001367", "0.001563", "0.001758",
      "0.001953", "0.002148", "0.002344", "0.002539", "0.002734",
      "0.002930", "0.003125", "0.003320", "0.003516", "0.003711"))

    val xs = List(
      -0.101918, 0.886976, 0.207878, -0.968121, -0.386563,
      0.598651, 0.531784, 0.902926, 0.536692, 0.596197,
      0.039791, 0.112179, 0.004210, 0.212172, 0.010958,
      0.307872, -0.408034, 1.088191, 0.628710, -0.481035)
    val colx2 = ("x", List(
      "-0.101918", "0.886976", "0.207878", "-0.968121", "-0.386563",
      "0.598651", "0.531784", "0.902926", "0.536692", "0.596197",
      "0.039791", "0.112179", "0.004210", "0.212172", "0.010958",
      "0.307872", "-0.408034", "1.088191", "0.628710", "-0.481035"))

    val ys = List(
      1.346609, 0.931745, -0.090765, -1.355280, -2.908090,
      0.164716, 0.519811, 1.399932, 0.485239, -1.158396,
      -1.644161, -0.383748, -0.441173, 1.435676, 0.576064,
      1.034289, -1.108588, -0.049748, -0.744704, 1.091714)
    val coly2 = ("y", List(
      "1.346609", "0.931745", "-0.090765", "-1.355280", "-2.908090",
      "0.164716", "0.519811", "1.399932", "0.485239", "-1.158396",
      "-1.644161", "-0.383748", "-0.441173", "1.435676", "0.576064",
      "1.034289", "-1.108588", "-0.049748", "-0.744704", "1.091714"))

    val zs = List(
      -2.201459, -0.621733, 1.079872, -0.161299, -0.523405,
      2.409951, 1.240612, 0.320921, -1.868204, -1.377741,
      1.041012, 2.372268, 0.475184, 0.157826, -0.932026,
      -0.492199, -1.072158, 2.406418, 1.043367, 0.217882)
    val colz2 = ("z", List(
      "-2.201459", "-0.621733", "1.079872", "-0.161299", "-0.523405",
      "2.409951", "1.240612", "0.320921", "-1.868204", "-1.377741",
      "1.041012", "2.372268", "0.475184", "0.157826", "-0.932026",
      "-0.492199", "-1.072158", "2.406418", "1.043367", "0.217882"))

    val lenCols = 20
    // NOTE: we can only read the iterator once, so
    // projections cannot be done multiple times
    val cols3 = Frame(colt2, colx2, coly2, colz2)
    val values3 = cols3.iterable.toIterator.toIndexedSeq
    //println(values3)
    values3.length shouldBe lenCols
    values3(0).r("t").v shouldBe a[String]
    values3(0).r("x").v shouldBe a[String]
    values3(0).r("y").v shouldBe a[String]
    values3(0).r("z").v shouldBe a[String]
    val tda: Array[String] = values3.map(m => m("t").cast[String]).toArray
    val xda: Array[String] = values3.map(m => m("x").cast[String]).toArray
    val yda: Array[String] = values3.map(m => m("y").cast[String]).toArray
    val zda: Array[String] = values3.map(m => m("z").cast[String]).toArray
    tda should contain theSameElementsInOrderAs colt2._2
    xda should contain theSameElementsInOrderAs colx2._2
    yda should contain theSameElementsInOrderAs coly2._2
    zda should contain theSameElementsInOrderAs colz2._2

    // Reload/restart iterator
    val cols4 = Frame(colt2, colx2, coly2, colz2)
    val tdd: Frame = cols4((x: String) => x.toDouble, "t")
    val tdds = tdd.iterable.iterator.toIndexedSeq
    tdds(0)("t").v shouldBe a[java.lang.Double]
    val tdda: Array[Double] = tdds.map(m => m("t").cast[Double]).toArray
    tdda should contain theSameElementsInOrderAs ts


    // Reload/restart iterator
    val cols5 = Frame(colt2, colx2, coly2, colz2)
    val conv1 = cols5((x: String) => x.toDouble, "t", "x", "y", "z")
    val conv1d = conv1.iterable.iterator.toIndexedSeq
    conv1d(0)("t").v shouldBe a[java.lang.Double]
    conv1d(0)("x").v shouldBe a[java.lang.Double]
    conv1d(0)("y").v shouldBe a[java.lang.Double]
    conv1d(0)("z").v shouldBe a[java.lang.Double]
    val conv1t: Array[Double] = conv1d.map(m => m("t").cast[Double]).toArray
    val conv1x: Array[Double] = conv1d.map(m => m("x").cast[Double]).toArray
    val conv1y: Array[Double] = conv1d.map(m => m("y").cast[Double]).toArray
    val conv1z: Array[Double] = conv1d.map(m => m("z").cast[Double]).toArray
    conv1t should contain theSameElementsInOrderAs ts
    conv1x should contain theSameElementsInOrderAs xs
    conv1y should contain theSameElementsInOrderAs ys
    conv1z should contain theSameElementsInOrderAs zs

    // Reload/restart iterator
    val cols6 = Frame(colt2, colx2, coly2, colz2)
    val conv2_0 = cols6((x: String) => x.toDouble, "t", "x", "y", "z")
    val conv2 = conv2_0((x: Double) => (x * 1e9).toLong, "t")
    val conv2d = conv2.iterable.iterator.toIndexedSeq
    //println(conv2d)
    conv2d(0)("t").v shouldBe a[java.lang.Long]
    val ttnanos = ts map (_ * 1e9) map (_.toLong)
    val conv2t: Array[Long] = conv2d.map(m => m("t").cast[Long].v).toArray
    conv2t should contain theSameElementsInOrderAs ttnanos

    // Reload/restart iterator
    val cols7 = Frame(colt2, colx2, coly2, colz2)
    val conv3_0 = cols7((x: String) => x.toDouble, "t", "x", "y", "z")
    val conv3_1 = conv3_0((x: Double) => (x * 1e9).toLong, "t")
    val conv3 = conv3_1((x: Long) => Duration.ofNanos(x), "t")
    val conv3d = conv3.iterable.iterator.toIndexedSeq
    //println(conv3d)
    conv3d(0)("t").v shouldBe a[Duration]
    val ttnanoss = List(
      "PT0S", "PT0.000195S", "PT0.000391S", "PT0.000586S", "PT0.000781S",
      "PT0.000977S", "PT0.001172S", "PT0.001367S", "PT0.001563S", "PT0.001758S",
      "PT0.001953S", "PT0.002148S", "PT0.002344S", "PT0.002539S", "PT0.002734S",
      "PT0.00293S", "PT0.003125S", "PT0.00332S", "PT0.003516S", "PT0.003711S")
    val conv3t: Array[Duration] = conv3d.map(m => m("t").cast[Duration].v).toArray
    conv3t should contain theSameElementsInOrderAs ttnanoss.map(Duration.parse)

    val stamp = LocalDateTime.parse("2018-05-04 13:43:52.101", format)
    //println(stamp)
    stamp.toString shouldBe "2018-05-04T13:43:52.101"
    val cols8 = Frame(colt2, colx2, coly2, colz2)
    val conv4_0 = cols8((x: String) => x.toDouble, "t", "x", "y", "z")
    val conv4_1 = conv4_0((x: Double) => (x * 1e9).toLong, "t")
    val conv4_2 = conv4_1((x: Long) => Duration.ofNanos(x), "t")
    val conv4 = conv4_2((x: Duration) => stamp.plus(x), "t")
    val conv4d = conv4.iterable.iterator.toIndexedSeq
    //println(conv4d)
    val conv4t: Array[LocalDateTime] = conv4d.map(m => m("t").cast[LocalDateTime].v).toArray
    conv4t map (_.toString) should contain theSameElementsInOrderAs List(
      "2018-05-04T13:43:52.101", "2018-05-04T13:43:52.101195", "2018-05-04T13:43:52.101391",
      "2018-05-04T13:43:52.101586", "2018-05-04T13:43:52.101781", "2018-05-04T13:43:52.101977",
      "2018-05-04T13:43:52.102172", "2018-05-04T13:43:52.102367", "2018-05-04T13:43:52.102563",
      "2018-05-04T13:43:52.102758", "2018-05-04T13:43:52.102953", "2018-05-04T13:43:52.103148",
      "2018-05-04T13:43:52.103344", "2018-05-04T13:43:52.103539", "2018-05-04T13:43:52.103734",
      "2018-05-04T13:43:52.103930", "2018-05-04T13:43:52.104125", "2018-05-04T13:43:52.104320",
      "2018-05-04T13:43:52.104516", "2018-05-04T13:43:52.104711"
    )

    val cols9 = Frame(colt2, colx2, coly2, colz2)
    val conv5_0 = cols9((x: String) => x.toDouble, "t", "x", "y", "z")
    val conv5_1 = conv5_0((x: Double) => (x * 1e9).toLong, "t")
    val conv5_2 = conv5_1((x: Long) => Duration.ofNanos(x), "t")
    val conv5_3 = conv5_2((x: Duration) => stamp.plus(x), "t")
    val conv5_4 = conv5_3((x: LocalDateTime) => x.toString, "t")
    //val conv5d1 = conv5_3.iterable.iterator.toIndexedSeq
    //println(conv5d1.mkString(";\n"))
    val conv5 = conv5_4.window(4, 2)
    val conv5d = conv5.iterable.iterator.toIndexedSeq
    //println(conv5d.mkString(";\n"))
    conv5d(0) shouldBe
      Row(Map(
        "t" -> Val(Vector("2018-05-04T13:43:52.101", "2018-05-04T13:43:52.101195", "2018-05-04T13:43:52.101391", "2018-05-04T13:43:52.101586")),
        "x" -> Val(Vector(-0.101918, 0.886976, 0.207878, -0.968121)),
        "y" -> Val(Vector(1.346609, 0.931745, -0.090765, -1.35528)),
        "z" -> Val(Vector(-2.201459, -0.621733, 1.079872, -0.161299))))
  }

  it should "allow creating streaming iterators from file" in {
    // From val dataFile: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_1000_cmdn_ens1.csv"

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    import better.files._
    //import File._
    //import java.io.{File => JFile}
    import better.files.Dsl._

    val dataFile: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_1000_cmdn_ens1.csv"
    val data: Frame = Frame.csv(dataFile)
    val read = data.iterable.iterator.take(10).toIndexedSeq
    //println(read.mkString(";\n"))
    read should contain theSameElementsInOrderAs Array(
      Row(Map("Time(s)" -> Val("0.000000"), "X" -> Val("-0.101918"), "Y" -> Val("1.346609"),  "Z" -> Val("-2.201459"))),
      Row(Map("Time(s)" -> Val("0.000195"), "X" -> Val("0.886976"),  "Y" -> Val("0.931745"),  "Z" -> Val("-0.621733"))),
      Row(Map("Time(s)" -> Val("0.000391"), "X" -> Val("0.207878"),  "Y" -> Val("-0.090765"), "Z" -> Val("1.079872"))),
      Row(Map("Time(s)" -> Val("0.000586"), "X" -> Val("-0.968121"), "Y" -> Val("-1.355280"), "Z" -> Val("-0.161299"))),
      Row(Map("Time(s)" -> Val("0.000781"), "X" -> Val("-0.386563"), "Y" -> Val("-2.908090"), "Z" -> Val("-0.523405"))),
      Row(Map("Time(s)" -> Val("0.000977"), "X" -> Val("0.598651"),  "Y" -> Val("0.164716"),  "Z" -> Val("2.409951"))),
      Row(Map("Time(s)" -> Val("0.001172"), "X" -> Val("0.531784"),  "Y" -> Val("0.519811"),  "Z" -> Val("1.240612"))),
      Row(Map("Time(s)" -> Val("0.001367"), "X" -> Val("0.902926"),  "Y" -> Val("1.399932"),  "Z" -> Val("0.320921"))),
      Row(Map("Time(s)" -> Val("0.001563"), "X" -> Val("0.536692"),  "Y" -> Val("0.485239"),  "Z" -> Val("-1.868204"))),
      Row(Map("Time(s)" -> Val("0.001758"), "X" -> Val("0.596197"),  "Y" -> Val("-1.158396"), "Z" -> Val("-1.377741")))
    )

    val norm_exp1 = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_norm_mm0_exp1.csv"
    val ir_exp1 = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_ir_mm1_exp1.csv"
    val or_exp1 = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_or_mm1_exp1.csv"
    val b_exp1 = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_b_mm1_exp1.csv"
    val data1: Frame = Frame.csvs(MyFormat, Seq(norm_exp1, ir_exp1, b_exp1, or_exp1))
    //val read1 = data1.iterable.iterator.toIndexedSeq
    //println(read1.map(_.mkString(",")).mkString(";\n"))

    val data1_1 = data1((x: String) => LocalDateTime.parse(x, format), "t")
    //val read1_1 = data1_1.iterable.iterator.toIndexedSeq
    //println(read1_1.map(_.mkString(",")).mkString(";\n"))
    val read1_2 = data1_1.iterable.iterator.map(m => m("x"))
    //println(read1_2.mkString(","))
    read1_2.toSeq should contain theSameElementsInOrderAs List(
      Val("-0.000000"), Val("-1.000000"), Val("-2.000000"), Val("-3.000000"),
      Val("-4.000000"), Val("-5.000000"), Val("-6.000000"), Val("-7.000000"),
      Val("-8.000000"), Val("-9.000000"), Val("-10.000000"), Val("-11.000000"),
      Val("-12.000000"), Val("-13.000000"), Val("-14.000000"), Val("-15.000000"),
      Val("-16.000000"), Val("-17.000000"), Val("-18.000000"), Val("-19.000000"),
      Val("-20.000000"), Val("-21.000000"), Val("-22.000000"), Val("-23.000000"),
      Val("-24.000000"), Val("-25.000000"), Val("-26.000000"), Val("-27.000000"),
      Val("-28.000000"), Val("-29.000000"), Val("-30.000000"), Val("-31.000000"),
      Val("-32.000000"), Val("-33.000000"), Val("-34.000000"), Val("-35.000000"),
      Val("-36.000000"), Val("-37.000000"), Val("-38.000000"), Val("-39.000000")
    )

    val data2: Frame = Frame.csvs(MyFormat, Seq(norm_exp1, ir_exp1, b_exp1, or_exp1))
    val data2_1 = data2((x: String) => LocalDateTime.parse(x, format), "t")
    val data2_2 = data2_1((x: String) => x.toDouble, "x", "y", "z")
    //val read2_2 = data2_2.iterable.iterator.toIndexedSeq
    //println(read2_2.map(_.mkString(",")).mkString(";\n"))
    data2_2.colSize shouldBe 4
    val read2_2 = data2_2.iterable.iterator.map(m => m("t"))
    //println(read2_2.mkString(","))
    read2_2.toSeq should contain theSameElementsInOrderAs List(
      Val(LocalDateTime.parse("2018-05-09T13:06:45")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000001")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000002")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000003")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000004")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000005")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000006")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000007")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000008")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000009")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000010")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000011")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000012")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000013")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000014")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000015")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000016")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000017")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000018")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000019")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000031")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000032")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000033")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000034")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000035")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000036")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000037")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000038")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000039")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000040")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000020")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000021")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000022")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000023")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000024")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000025")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000026")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000027")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000028")),
      Val(LocalDateTime.parse("2018-05-09T13:06:45.000029"))
    )

  }

  it should "allow adding columns to the CSV stream" in {

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    import better.files._
    //import File._
    //import java.io.{File => JFile}
    import better.files.Dsl._

    val dataFile: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp2_norm_mm3_exp4.csv"

    // parse file name to get extra features (add to Inegi utils)

    val data: Frame = Frame.csv(dataFile)
    val data1 = data.ignore("t")
    val read = data1.iterable.iterator.take(10).toIndexedSeq
    //println(read.mkString(";\n"))
    read should contain theSameElementsInOrderAs Array(
      Row(Map("x" -> Val("-0.101918"), "y" -> Val("1.346609"), "z" -> Val("-2.201459"))),
      Row(Map("x" -> Val("0.886976"), "y" -> Val("0.931745"), "z" -> Val("-0.621733"))),
      Row(Map("x" -> Val("0.207878"), "y" -> Val("-0.090765"), "z" -> Val("1.079872"))),
      Row(Map("x" -> Val("-0.968121"), "y" -> Val("-1.355280"), "z" -> Val("-0.161299"))),
      Row(Map("x" -> Val("-0.386563"), "y" -> Val("-2.908090"), "z" -> Val("-0.523405"))),
      Row(Map("x" -> Val("0.598651"), "y" -> Val("0.164716"), "z" -> Val("2.409951"))),
      Row(Map("x" -> Val("0.531784"), "y" -> Val("0.519811"), "z" -> Val("1.240612"))),
      Row(Map("x" -> Val("0.902926"), "y" -> Val("1.399932"), "z" -> Val("0.320921"))),
      Row(Map("x" -> Val("0.536692"), "y" -> Val("0.485239"), "z" -> Val("-1.868204"))),
      Row(Map("x" -> Val("0.596197"), "y" -> Val("-1.158396"), "z" -> Val("-1.377741")))
    )

    val data2: Frame = Frame.csv(dataFile).ignore("t")
    //val data3 = data2.project("y","x")
    val data3 = data2("y", "x")
    val read3 = data3.iterable.iterator.take(10).toIndexedSeq
    //println(read3.mkString(";\n"))
    val expected3 = List(
      Row(Map("y" -> Val("1.346609"), "x" -> Val("-0.101918"))),
      Row(Map("y" -> Val("0.931745"), "x" -> Val("0.886976"))),
      Row(Map("y" -> Val("-0.090765"), "x" -> Val("0.207878"))),
      Row(Map("y" -> Val("-1.355280"), "x" -> Val("-0.968121"))),
      Row(Map("y" -> Val("-2.908090"), "x" -> Val("-0.386563"))),
      Row(Map("y" -> Val("0.164716"), "x" -> Val("0.598651"))),
      Row(Map("y" -> Val("0.519811"), "x" -> Val("0.531784"))),
      Row(Map("y" -> Val("1.399932"), "x" -> Val("0.902926"))),
      Row(Map("y" -> Val("0.485239"), "x" -> Val("0.536692"))),
      Row(Map("y" -> Val("-1.158396"), "x" -> Val("0.596197")))
    )
    read3.toList should contain theSameElementsInOrderAs expected3

    //val dataFile: File = cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp2_norm_mm3_exp4.csv"
    val features1 = Utils.parseFileName(dataFile)
    //println(features1)
    features1 should be('right)
    features1.right.get._1 shouldBe Map(
      "load" -> "2", "damage" -> "norm", "exp" -> "4", "damage_size" -> "3",
      "bearing" -> "1", "speed" -> "1000"
    )

    features1.right.get._2 shouldBe Map(
      "load" -> "hp", "damage" -> "norm", "exp" -> "exp", "damage_size" -> "mm",
      "bearing" -> "rol", "speed" -> "rpm")

    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_2"
    val matchNames = "*hp*.csv"
    val files = DataFiles.parseFileNames(dir, matchNames, Utils.parseFileName).toList
    //println(files.mkString(";\n"))
    val parserOk = files.filter(_.isRight).map(_.right.get)
    //println(parserOk.mkString(";\n"))
    val (filesp, features, units) = parserOk.unzip3
    //println(filesp.mkString(";\n"))
    //println(features.mkString(";\n"))
    //println(units.mkString(";\n"))
    features should contain theSameElementsAs List(
      Map("load" -> "0", "damage" -> "ir", "exp" -> "1", "damage_size" -> "1", "bearing" -> "1", "speed" -> "1000"),
      Map("load" -> "0", "damage" -> "b", "exp" -> "1", "damage_size" -> "1", "bearing" -> "1", "speed" -> "1000"),
      Map("load" -> "2", "damage" -> "norm", "exp" -> "4", "damage_size" -> "3", "bearing" -> "1", "speed" -> "1000"),
      Map("load" -> "0", "damage" -> "norm", "exp" -> "1", "damage_size" -> "0", "bearing" -> "1", "speed" -> "1000"),
      Map("load" -> "0", "damage" -> "or", "exp" -> "1", "damage_size" -> "1", "bearing" -> "1", "speed" -> "1000")
    )
    units should contain theSameElementsAs List(
      Map("load" -> "hp", "damage" -> "ir", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm"),
      Map("load" -> "hp", "damage" -> "b", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm"),
      Map("load" -> "hp", "damage" -> "norm", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm"),
      Map("load" -> "hp", "damage" -> "norm", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm"),
      Map("load" -> "hp", "damage" -> "or", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm")
    )

    val features2 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val errCount = DataFiles.countParseErrors(features2)
    //println(s"errCount = $errCount")
    errCount shouldBe 0
    val features3 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features4 = DataFiles.ignoreParseErrors(features3).toList
    //println(features4.mkString(";\n"))
    val expectedParse = List(
      (cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_ir_mm1_exp1.csv",
        Row(Map("load" -> Val("0"), "damage" -> Val("ir"), "exp" -> Val("1"),
          "damage_size" -> Val("1"), "bearing" -> Val("1"), "speed" -> Val("1000")))),
      (cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_b_mm1_exp1.csv",
        Row(Map("load" -> Val("0"), "damage" -> Val("b"), "exp" -> Val("1"),
          "damage_size" -> Val("1"), "bearing" -> Val("1"), "speed" -> Val("1000")))),
      (cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp2_norm_mm3_exp4.csv",
        Row(Map("load" -> Val("2"), "damage" -> Val("norm"), "exp" -> Val("4"),
          "damage_size" -> Val("3"), "bearing" -> Val("1"), "speed" -> Val("1000")))),
      (cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_norm_mm0_exp1.csv",
        Row(Map("load" -> Val("0"), "damage" -> Val("norm"), "exp" -> Val("1"),
          "damage_size" -> Val("0"), "bearing" -> Val("1"), "speed" -> Val("1000")))),
      (cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_or_mm1_exp1.csv",
        Row(Map("load" -> Val("0"), "damage" -> Val("or"), "exp" -> Val("1"),
          "damage_size" -> Val("1"), "bearing" -> Val("1"), "speed" -> Val("1000"))))
    )
    features4 should contain theSameElementsAs expectedParse

    val loadParsed = List(
      (cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_b_mm1_exp1.csv",
        Row(Map("load" -> Val("0"), "damage" -> Val("b"), "exp" -> Val("1"),
          "damage_size" -> Val("1"), "bearing" -> Val("1"), "speed" -> Val("1000")))),
      (cwd / ".." / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp2_norm_mm3_exp4.csv",
        Row(Map("load" -> Val("2"), "damage" -> Val("norm"), "exp" -> Val("4"),
          "damage_size" -> Val("3"), "bearing" -> Val("1"), "speed" -> Val("1000"))))
    )
    val data4 = Frame.extendCSVs(MyFormat, loadParsed.toIterator)
    //val read4 = data4.iterable.iterator.toIndexedSeq
    //println(read4.mkString(";\n"))
    val data5 = data4("x", "z", "damage")
    val read5 = data5.iterable.iterator.toIndexedSeq
    //println(read5.mkString(",\n"))
    read5 should contain theSameElementsAs List(
      Row(Map("x" -> Val("-20.000000"), "z" -> Val("40.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-21.000000"), "z" -> Val("41.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-22.000000"), "z" -> Val("42.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-23.000000"), "z" -> Val("43.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-24.000000"), "z" -> Val("44.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-25.000000"), "z" -> Val("45.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-26.000000"), "z" -> Val("46.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-27.000000"), "z" -> Val("47.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-28.000000"), "z" -> Val("48.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-29.000000"), "z" -> Val("49.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-0.101918"), "z" -> Val("-2.201459"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.886976"), "z" -> Val("-0.621733"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.207878"), "z" -> Val("1.079872"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("-0.968121"), "z" -> Val("-0.161299"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("-0.386563"), "z" -> Val("-0.523405"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.598651"), "z" -> Val("2.409951"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.531784"), "z" -> Val("1.240612"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.902926"), "z" -> Val("0.320921"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.536692"), "z" -> Val("-1.868204"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.596197"), "z" -> Val("-1.377741"), "damage" -> Val("norm")))
    )

    // Can we specify a single file only ?
    val dir1 = cwd / ".." / "data/inegi/ensaios_rolamentos_2" / "rol1_rpm1000_hp0_b_mm1_exp1.csv"
    val matchNames1 = ""
    val features6 = DataFiles.fileNamesfeatures(dir1, matchNames1, Utils.parseFileName)
    val features7 = DataFiles.ignoreParseErrors(features6).toList
    features7.length shouldBe 1

  }

  it should "allow processing stream per row" in {
    val colx1 = ("x", Seq("1", "1", "1", "1", "1", "3", "3", "3", "3", "3", "5", "5", "5", "5", "5", "1", "1", "1", "1", "1"))
    val coly1 = ("y", Seq("11", "11", "11", "11", "11", "13", "13", "13", "13", "13", "15", "15", "15", "15", "15", "11", "11", "11", "11", "11"))
    val colz1 = ("z", Seq("21", "21", "21", "21", "21", "23", "23", "23", "23", "23", "25", "25", "25", "25", "25", "21", "21", "21", "21", "21"))
    val cols1 = Frame(colx1, coly1, colz1)

    // Handling column renames
    val cols2 = cols1.renameCols("x" -> "xx", "y" -> "yy")
    //println(s"cols2.v.r = ${cols2.v.r.mkString(",")}")
    //val read2 = cols2.iterable.iterator.toIndexedSeq
    //println(read2.mkString(",\n"))

    // We convert old columns to a new type
    val cols3 = cols2((s: String) => s.toDouble, "xx", "yy", "z")
    //println(s"cols3.v.r = ${cols3.v.r.mkString(",")}")
    //println(s"cols3.v.colTypes = ${cols3.v.colTypes.mkString(",")}")
    //println(s"cols3.colTypes = ${cols3.colTypes.mkString(",")}")
    cols3.v.colNames should contain theSameElementsAs List("z", "xx", "yy")
    cols3.v.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Double])
    cols3.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Double])

    // We convert old columns to new columns with new types
    val cols3a = cols2((s: String) => s.toDouble, "xD", "yD", "z")
    //println(s"cols3a.v.r = ${cols3a.v.r.mkString(",")}")
    //println(s"cols3a.v.colTypes = ${cols3a.v.colTypes.mkString(",")}")
    //println(s"cols3a.colTypes = ${cols3a.colTypes.mkString(",")}")
    cols3a.v.colNames should contain theSameElementsAs  List("z", "xx", "yy", "xD", "yD")
    cols3a.v.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.String],
      "yy" -> classOf[java.lang.String],
      "xD" -> classOf[java.lang.Double],
      "yD" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Double])
    cols3a.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.String],
      "yy" -> classOf[java.lang.String],
      "xD" -> classOf[java.lang.Double],
      "yD" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Double])

    // We convert old columns to new columns with new types
    val cols4a = cols2((s: String) => s.toDouble, "xx" -> "xD", "yy" -> "yD", "z" -> "z")
    //println(s"cols4a.v.r = ${cols4a.v.r.mkString(",")}")
    //println(s"cols4a.v.colTypes = ${cols4a.v.colTypes.mkString(",")}")
    //println(s"cols4a.colTypes = ${cols4a.colTypes.mkString(",")}")
    cols4a.v.colNames should contain theSameElementsAs List("z", "xx", "yy", "xD", "yD")
    cols4a.v.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.String],
      "yy" -> classOf[java.lang.String],
      "xD" -> classOf[java.lang.Double],
      "yD" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Double])
    cols4a.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.String],
      "yy" -> classOf[java.lang.String],
      "xD" -> classOf[java.lang.Double],
      "yD" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Double])

    // Here we create a single new variable with a new type
    val cols4c = cols3((a: Double, b: Double) => a + b, ("xx", "yy", "x+y"))
    //println(s"cols4c.v.r = ${cols4c.v.r.mkString(",")}")
    //println(s"cols4c.v.colTypes = ${cols4c.colTypes.mkString(",")}")
    //println(s"cols4c.colTypes = ${cols4c.colTypes.mkString(",")}")
    //val read3 = cols3.iterable.iterator.toIndexedSeq
    //println(read3.mkString(",\n"))
    cols4c.v.colNames should contain theSameElementsAs List("z", "xx", "yy", "x+y")
    cols4c.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Double],
      "x+y" -> classOf[java.lang.Double])

    // Here we overwrite a single variable with a new type
    val cols4d = cols3((a: Double, b: Double) => (a + b).toInt, ("xx", "yy", "z"))
    //println(s"cols4d.v.r = ${cols4d.v.r.mkString(",")}")
    //println(s"cols4d.v.colTypes = ${cols4d.colTypes.mkString(",")}")
    //println(s"cols4d.colTypes = ${cols4d.colTypes.mkString(",")}")
    //val read3 = cols3.iterable.iterator.toIndexedSeq
    //println(read3.mkString(",\n"))
    cols4d.v.colNames should contain theSameElementsAs List("z", "xx", "yy")
    cols4d.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Integer])

    val cols5 = cols4c.renameCols("z" -> "zz")
    //println(s"cols5.v.r = ${cols5.v.r.mkString(",")}")
    //println(s"cols5.v.colTypes = ${cols5.colTypes.mkString(",")}")
    //println(s"cols5.colTypes = ${cols5.colTypes.mkString(",")}")
    cols5.v.colNames should contain theSameElementsAs List("zz", "xx", "yy", "x+y")
    cols5.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "zz" -> classOf[java.lang.Double],
      "x+y" -> classOf[java.lang.Double])

    val cols5a = cols5.updated("zz", "Invalidated")
    //println(s"cols5a.v.r = ${cols5a.v.r.mkString(",")}")
    //println(s"cols5a.v.colTypes = ${cols5a.colTypes.mkString(",")}")
    //println(s"cols5a.colTypes = ${cols5a.colTypes.mkString(",")}")
    //val read5a = cols5a.iterable.iterator.toIndexedSeq
    //println(read5a.mkString(",\n"))
    cols5a.v.colNames should contain theSameElementsAs List("zz", "xx", "yy", "x+y")
    cols5a.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "zz" -> classOf[java.lang.String],
      "x+y" -> classOf[java.lang.Double])

    // Back to the (nearly) original stream
    val cols5b = cols5a("xx", "yy", "zz")
    //println(s"cols5b.v.r = ${cols5b.v.r.mkString(",")}")
    //println(s"cols5b.v.colTypes = ${cols5b.colTypes.mkString(",")}")
    //println(s"cols5b.colTypes = ${cols5b.colTypes.mkString(",")}")
    //val read5b = cols5b.iterable.iterator.toIndexedSeq
    //println(read5b.mkString(",\n"))
    cols5b.v.colNames should contain theSameElementsAs List("zz", "xx", "yy")
    cols5b.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "zz" -> classOf[java.lang.String])

    val cols5c = cols5a.ignore("x+y", "zz")
    //println(s"cols5c.v.r = ${cols5c.v.r.mkString(",")}")
    //println(s"cols5c.v.colTypes = ${cols5c.colTypes.mkString(",")}")
    //println(s"cols5c.colTypes = ${cols5c.colTypes.mkString(",")}")
    //val read5c = cols5c.iterable.iterator.toIndexedSeq
    //println(read5c.mkString(",\n"))
    cols5c.v.colNames should contain theSameElementsAs List("xx", "yy")
    cols5c.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double])

    val cols5d = cols5a.vec("v_x_y", (i: Double) => i, "xx", "yy", "x+y")
    //println(s"cols5d.v.r = ${cols5d.v.r.mkString(",")}")
    //println(s"cols5d.v.colTypes = ${cols5d.colTypes.mkString(",")}")
    //println(s"cols5d.colTypes = ${cols5d.colTypes.mkString(",")}")
    val read5d = cols5d.iterable.iterator.toIndexedSeq
    //println(read5d.mkString(",\n"))
    cols5d.v.colNames should contain theSameElementsAs List("xx", "zz", "yy", "x+y", "v_x_y")
    cols5d.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "zz" -> classOf[java.lang.String],
      "x+y" -> classOf[java.lang.Double],
      "v_x_y" -> classOf[Vector[java.lang.Double]])
    read5d.head shouldBe Row(Map(
      "xx" -> Val(1.0),
      "zz" -> Val("Invalidated"),
      "yy" -> Val(11.0),
      "x+y" -> Val(12.0),
      "v_x_y" -> Val(Vector(12.0, 11.0, 1.0))))

    // Don't use arrays due to erasure: need to unpack the array

    val cols5e = Frame(colx1, coly1, colz1)
      .renameCols("x" -> "xx", "y" -> "yy", "z" -> "zz")
      .apply((s: String) => s.toDouble, "xx", "yy")
      .apply((a: Double, b: Double) => a + b, ("xx", "yy", "x+y"))
      .arr("v_x_y", (i: Double) => i, "xx", "yy", "x+y")
    //println(s"cols5e.v.r = ${cols5e.v.r.mkString(",")}")
    //println(s"cols5e.v.colTypes = ${cols5e.colTypes.mkString(",")}")
    //println(s"cols5e.colTypes = ${cols5e.colTypes.mkString(",")}")
    val read5e = cols5e.iterable.iterator.toIndexedSeq
    //println(read5e.mkString(",\n"))
    cols5e.v.colNames should contain theSameElementsAs List("xx", "zz", "yy", "x+y", "v_x_y")
    cols5e.colTypes shouldBe Map(
      "xx" -> classOf[java.lang.Double],
      "yy" -> classOf[java.lang.Double],
      "zz" -> classOf[java.lang.String],
      "x+y" -> classOf[java.lang.Double],
      "v_x_y" -> classOf[Array[Double]])
    read5e.head("xx") shouldBe Val(1.0)
    read5e.head("zz") shouldBe Val("21")
    read5e.head("yy") shouldBe Val(11.0)
    read5e.head("x+y") shouldBe Val(12.0)
    val tt: Either[String, Array[Double]] = unpack[Array[Double]](read5e.head("v_x_y"))
    val ta = tt.right.get
    //println(ta.mkString(","))
    ta shouldBe Array(12.0, 11.0, 1.0)
    /* Won't work due to erasure
    read5e.head shouldBe Row(Map(
      "xx" -> Val(1.0),
      "zz" -> Val("21"),
      "yy" -> Val(11.0),
      "x+y" -> Val(12.0),
      "v_x_y" -> Val(Array(12.0, 11.0, 1.0))))
      */
  }

  it should "allow mapping rows of a stream" in {
    val colx1 = ("x", Seq("1", "1", "1", "1", "1", "3", "3", "3", "3", "3", "5", "5", "5", "5", "5", "1", "1", "1", "1", "1"))
    val coly1 = ("y", Seq("11", "11", "11", "11", "11", "13", "13", "13", "13", "13", "15", "15", "15", "15", "15", "11", "11", "11", "11", "11"))
    val colz1 = ("z", Seq("21", "21", "21", "21", "21", "23", "23", "23", "23", "23", "25", "25", "25", "25", "25", "21", "21", "21", "21", "21"))

    // TODO: map and mapExtend

    // This is an example of how we can operate on the full row
    // Note that we are crating additional columns, so it is
    // necessary provide a Row withe the column names and default
    // values (indicates the type used)
    val cols6 = Frame(colx1, coly1, colz1)
    val cols6a = cols6.map({ r: Row =>
      val nr = for {
        // TODO: comprehensions
        /*xx:String <- r("x")
        yy:String <- r("y")
        zz:String <- r("z")*/
        xx <- unpack[String](r("x"))
        yy <- unpack[String](r("y"))
        zz <- unpack[String](r("z"))
        xd = xx.toDouble
        yd = yy.toDouble
        zd = zz.toDouble
        x_y = xd + yd
        nrow = Row(Map[String, Val[_]]("xx" -> xd, "yy" -> yd, "zz" -> zd, "x+y" -> x_y))
      } yield nrow
      // If no error then add new columns (replace existing ones)
      nr.fold(err => r + ("x+y" -> Val[String](err)), (nrow: Row) => r ++ nrow)
    },
      "xx" -> 0.0, "yy" -> 0.0, "zz" -> 0.0, "x+y" -> 0.0
    )
    //println(s"cols6a.v.r = ${cols6a.v.r.mkString(",")}")
    //println(s"cols6a.v.colTypes = ${cols6a.colTypes.mkString(",")}")
    //println(s"cols6a.colTypes = ${cols6a.colTypes.mkString(",")}")
    val read6a = cols6a.iterable.iterator.toIndexedSeq
    //println(read6a.mkString(",\n"))
    cols6a.v.colNames should contain theSameElementsAs List("x", "xx", "y", "zz", "yy", "x+y", "z")
    cols6a.colTypes shouldBe Map(
      "x" -> classOf[java.lang.String],
      "xx" -> classOf[java.lang.Double],
      "y" -> classOf[java.lang.String],
      "yy" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.String],
      "zz" -> classOf[java.lang.Double],
      "x+y" -> classOf[java.lang.Double])
    read6a.head shouldBe Row(Map(
      "x" -> Val("1"), "xx" -> Val(1.0), "y" -> Val("11"), "zz" -> Val(21.0), "yy" -> Val(11.0),
      "x+y" -> Val(12.0), "z" -> Val("21")))

    // In this example we overwrite existing columns with a different
    // type so we need to provide the Row parameter. Here we replace
    // converted values x,y and replace z with a sum
    val cols6f = Frame(colx1, coly1, colz1)
    val cols6b = cols6f.map({ r: Row =>
      val nr = for {
        xx <- unpack[String](r("x"))
        yy <- unpack[String](r("y"))
        zz <- unpack[String](r("z"))
        xd = xx.toDouble
        yd = yy.toDouble
        //zd = zz.toDouble
        x_y = xd + yd
        nrow = Row(Map[String, Val[_]]("x" -> xd, "y" -> yd, "z" -> x_y))
      } yield nrow
      // If no error then add new columns (replace existing ones)
      nr.fold(err => r + ("x+y" -> Val[String](err)), (nrow: Row) => r ++ nrow)
    },
      "x" -> 0.0, "y" -> 0.0, "z" -> 0.0
    )
    //println(s"cols6b.v.r = ${cols6b.v.r.mkString(",")}")
    //println(s"cols6b.v.colTypes = ${cols6b.colTypes.mkString(",")}")
    //println(s"cols6b.colTypes = ${cols6b.colTypes.mkString(",")}")
    val read6b = cols6b.iterable.iterator.toIndexedSeq
    //println(read6b.mkString(",\n"))
    cols6b.v.colNames should contain theSameElementsAs List("x", "y", "z")
    cols6b.colTypes shouldBe Map(
      "x" -> classOf[java.lang.Double],
      "y" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Double])
    read6b.head shouldBe Row(Map("x" -> Val(1.0), "y" -> Val(11.0), "z" -> Val(12.0)))

    val cols6g = Frame(colx1, coly1, colz1)
    val cols6c = cols6g.filterWith { r =>
      val xv: Either[String, String] = r("x")
      xv.fold(_ => false, s => s.toDouble > 1.0)
    }
    //println(s"cols6c.v.r = ${cols6c.v.r.mkString(",")}")
    //println(s"cols6c.v.colTypes = ${cols6c.colTypes.mkString(",")}")
    //println(s"cols6c.colTypes = ${cols6c.colTypes.mkString(",")}")
    val read6c = cols6c.iterable.iterator.toIndexedSeq
    //println(read6c.mkString(",\n"))
    cols6c.v.colNames should contain theSameElementsAs List("x", "y", "z")
    cols6c.colTypes shouldBe Map(
      "x" -> classOf[java.lang.String],
      "y" -> classOf[java.lang.String],
      "z" -> classOf[java.lang.String])
    read6c.head shouldBe Row(Map("x" -> Val("3"), "y" -> Val("13"), "z" -> Val("23")))
    read6c.length shouldBe 10


    val cols6h = Frame(colx1, coly1, colz1)
    val cols6d = cols6h.filterWith { r =>
      val xv: Either[String, String] = r("x")
      xv.fold(_ => false, s => s.toDouble > 25.0)
    }
    //println(s"cols6c.v.r = ${cols6c.v.r.mkString(",")}")
    //println(s"cols6c.v.colTypes = ${cols6c.colTypes.mkString(",")}")
    //println(s"cols6c.colTypes = ${cols6c.colTypes.mkString(",")}")
    val read6d = cols6d.iterable.iterator.toIndexedSeq
    //println(read6c.mkString(",\n"))
    cols6d.v.colNames should contain theSameElementsAs List("x", "y", "z")
    cols6d.colTypes shouldBe Map(
      "x" -> classOf[java.lang.String],
      "y" -> classOf[java.lang.String],
      "z" -> classOf[java.lang.String])
    //read6d.head shouldBe None
    read6d.headOption shouldBe None
    read6d.length shouldBe 0

  }


  it should "allow fold over a stream by rows or columns" in {
    val colx1 = ("x", Seq("1","1","1","1","1","3","3","3","3","3","5","5","5","5","5","1","1","1","1","1"))
    val coly1 = ("y", Seq("11","11","11","11","11","13","13","13","13","13","15","15","15","15","15","11","11","11","11","11"))
    val colz1 = ("z", Seq("21","21","21","21","21","23","23","23","23","23","25","25","25","25","25","21","21","21","21","21"))

    // We can fold left over every row. Each fold starts with the same
    // accumulator. This example sums over the columns of the row
    val key = "sum"
    val acc = Row(key -> 0.0)
    val cols1 = Frame(colx1, coly1, colz1)
    val sum1 = cols1.foldLeft(acc){ case (acum,(k,e)) =>
      val el = unpack[String](e)
      el.fold( _ => acum, { vl =>
        val d = vl.toDouble
        val se:Either[_,Double] = acum(key)
        val ne = se.fold(_ => se, v => v + d)
        val tmp = acum.updated(key,ne)
        tmp
      })
    }
    //println(s"sum1.v.r = ${sum1.v.r.mkString(",")}")
    //println(s"sum1.v.colTypes = ${sum1.colTypes.mkString(",")}")
    //println(s"sum1.colTypes = ${sum1.colTypes.mkString(",")}")
    val readSum1 = sum1.iterable.iterator.toIndexedSeq
    //println(readSum1.mkString(",\n"))
    sum1.v.colNames should contain theSameElementsAs List("sum")
    sum1.colTypes shouldBe Map("sum" -> classOf[java.lang.Double])
    readSum1.head shouldBe Row(Map("sum" -> Val(33.0)))
    readSum1.length shouldBe 20

    // We can also fold over the entire frame, but this is less useful
    val cols2 = Frame(colx1, coly1, colz1).apply((s:String) => s.toDouble, "x", "y", "z")
    val acum = Row("xsum" -> 0.0, "ysum" -> 0.0, "zsum" -> 0.0 )
    val sum = cols2.iterable.foldLeft(acum) { (acc,row) =>
      val xe = unpack[Double](row("x"))
      val ye = unpack[Double](row("y"))
      val ze = unpack[Double](row("z"))

      val xs = unpack[Double](acc("xsum")).right.get
      val ys = unpack[Double](acc("ysum")).right.get
      val zs = unpack[Double](acc("zsum")).right.get

      val sx = xe.fold(_ => xs, v => v + xs)
      val sy = ye.fold(_ => ys, v => v + ys)
      val sz = ze.fold(_ => zs, v => v + zs)
      acc.updated("xsum",sx)
        .updated("ysum",sy)
        .updated("zsum",sz)
    }
    //println(sum)
    sum("xsum") shouldBe Val(50.0)

  }

  it should "allow extending stream by columns" in {

    val colx1 = ("x", Seq("1","1","1","1","1","3","3","3","3","3","5","5","5","5","5","1","1","1","1","1"))
    val coly1 = ("y", Seq("11","11","11","11","11","13","13","13","13","13","15","15","15","15","15","11","11","11","11","11"))
    val colz1 = ("z", Seq("21","21","21","21","21","23","23","23","23","23","25","25","25","25","25","21","21","21","21","21"))

    val cols1 = Frame(colx1, coly1).apply((s:String) => s.toDouble,"x", "y")
    val cols2 = Frame(colz1).apply((s:String) => s.toInt,"z")

    val cols = cols1 || cols2

    //println(s"cols.v.r = ${cols.v.r.mkString(",")}")
    //println(s"cols.v.colTypes = ${cols.colTypes.mkString(",")}")
    //println(s"cols.colTypes = ${cols.colTypes.mkString(",")}")
    val read = cols.iterable.iterator.toIndexedSeq
    //println(read.mkString(",\n"))
    cols.v.colNames should contain theSameElementsAs List("x", "y", "z")
    cols.colTypes shouldBe Map(
      "x" -> classOf[java.lang.Double],
      "y" -> classOf[java.lang.Double],
      "z" -> classOf[java.lang.Integer])
    read.head shouldBe Row(Map("x" -> Val(1.0), "y" -> Val(11.0), "z" -> Val(21)))
    read.length shouldBe 20

  }

  it should "allow appending frames by rows" in {

    // First frame
    val colx1 = ("x", Seq("1",  "2", "3", "4", "5"))
    val coly1 = ("y", Seq("11","12","13","14","15"))
    val cols1 = Frame(colx1, coly1).apply((s:String) => s.toDouble,"x").apply((s:String) => s.toInt,"y")

    // Second frame
    val colx2 = ("x", Seq(" 6"," 7", "8", "9","10"))
    val coly2 = ("y", Seq("16","17","18","19","20"))
    val cols2 = Frame(colx2, coly2).apply((s:String) => s.toDouble,"x").apply((s:String) => s.toInt,"y")

    // 'Stack' one after the other
    val cols = cols1 ++ cols2

    val read = cols.iterable.iterator.toIndexedSeq
    //println(read.mkString(",\n"))
    cols.v.colNames should contain theSameElementsAs List("x", "y")
    cols.colTypes shouldBe Map(
      "x" -> classOf[java.lang.Double],
      "y" -> classOf[java.lang.Integer])
    read.length shouldBe 10
    read should contain theSameElementsInOrderAs List(
      Row(Map("x" -> Val(1.0), "y" -> Val(11))),
      Row(Map("x" -> Val(2.0), "y" -> Val(12))),
      Row(Map("x" -> Val(3.0), "y" -> Val(13))),
      Row(Map("x" -> Val(4.0), "y" -> Val(14))),
      Row(Map("x" -> Val(5.0), "y" -> Val(15))),
      Row(Map("x" -> Val(6.0), "y" -> Val(16))),
      Row(Map("x" -> Val(7.0), "y" -> Val(17))),
      Row(Map("x" -> Val(8.0), "y" -> Val(18))),
      Row(Map("x" -> Val(9.0), "y" -> Val(19))),
      Row(Map("x" -> Val(10.0), "y" -> Val(20)))
    )
  }


  it should "allow post-processing appended frames" in {

    // First frame
    val colx1 = ("x", Seq("1",  "2", "3", "4", "5"))
    val coly1 = ("y", Seq("11","12","13","14","15"))
    val cols1 = Frame(colx1, coly1)

    // Second frame
    val colx2 = ("x", Seq( "6", "7", "8", "9","10"))
    val coly2 = ("y", Seq("16","17","18","19","20"))
    val cols2 = Frame(colx2, coly2)

    // 'Stack' one after the other
    val cols = cols1 ++ cols2
    val colsa = cols.renameCol("x" -> "x1")

    val read = colsa.iterable.iterator.toIndexedSeq
    //println(read.mkString(",\n"))
    colsa.v.colNames should contain theSameElementsAs List("x1", "y")
    colsa.colTypes shouldBe Map(
      "x1" -> classOf[java.lang.String],
      "y" -> classOf[java.lang.String])
    read.length shouldBe 10
    read should contain theSameElementsInOrderAs List(
      Row(Map("x1" -> Val("1"), "y" -> Val("11"))),
      Row(Map("x1" -> Val("2"), "y" -> Val("12"))),
      Row(Map("x1" -> Val("3"), "y" -> Val("13"))),
      Row(Map("x1" -> Val("4"), "y" -> Val("14"))),
      Row(Map("x1" -> Val("5"), "y" -> Val("15"))),
      Row(Map("x1" -> Val("6"), "y" -> Val("16"))),
      Row(Map("x1" -> Val("7"), "y" -> Val("17"))),
      Row(Map("x1" -> Val("8"), "y" -> Val("18"))),
      Row(Map("x1" -> Val("9"), "y" -> Val("19"))),
      Row(Map("x1" -> Val("10"), "y" -> Val("20")))
    )
  }

  "lazy iterator" should "handle infinite steams " in {

    // ISSUE: https://users.scala-lang.org/t/why-is-my-implementation-of-iterable-no-lazy/2744
    // This is test ode use to diagnose why our iterables are not lazy. Tests
    // show that we cannot map directly ion the Iterable, but must use the
    // underlying iterator. Isse has been posted in the scala-users mailing list.
    case class WrapperX(r:Iterable[Int])

    val counter = Stream.from(1).toIterator
    counter.map( _ * 2).take(25).toList // terminates
    //println(r)
    val counter1 = Stream.from(1).toIterable // No need to call toIterable
    counter1.map( _ * 2).take(25).toList // terminates
    //println(r1)
    val counter2 = WrapperX(Stream.from(1))
    counter2.r.map( _ * 2).take(25).toList // terminates
    //println(r2)
    val data = List(Stream.from(1).toIterator)
    val counter3 = new Iterable[List[Int]] {
      override def iterator: Iterator[List[Int]] = new AbstractIterator[List[Int]] {

        override def hasNext: Boolean = data.forall( p => p.hasNext)
        override def next(): List[Int] = data.map { v => v.next() }
      }
    }
    //val r3 = counter3.toIterable.map( _.map(_ * 2) ).take(25).toList // does not terminate
    //val r3 = counter3.map( _.map(_ * 2) ).take(25).toList // does not terminate
    counter3.toIterator.map( _.map(_ * 2) ).take(25).toList // terminates
    //val r3 = counter3.iterator.map( _.map(_ * 2) ).take(25).toList // terminates
    //println(r3)

    val counter4 = WrapperX(Stream.from(1))
    counter4.r.map( _ * 2).take(25).toList // terminates
    //println(r4)
    val data1 = WrapperX(Stream.from(1))
    val counter5 = new Iterable[Int] {
      override def iterator: Iterator[Int] = new Iterator[Int] {
        val iter: Iterator[Int] = data1.r.toIterator

        override def hasNext: Boolean = iter.hasNext
        override def next(): Int = iter.next()
        // Works only in 2.13 from M4 onwards
        //override def iterableFactory = Stream
      }
    }
    //val r7 = counter5.toIterable.map( _ * 2 ).take(25).toList // does not terminate
    //val r7 = counter5.map( _ * 2 ).take(25).toList // does not terminate
    counter5.toIterator.map( _ * 2 ).take(25).toList // terminates
    //val r7 = counter5.iterator.map( _ * 2 ).take(25).toList // terminates
    //println(r7)
    //fail()
  }


}
