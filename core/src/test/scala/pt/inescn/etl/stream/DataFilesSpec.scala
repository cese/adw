package pt.inescn.etl.stream

import java.lang.{System => JSystem}
import better.files.Dsl.cwd
import better.files.File
import com.github.tototoshi.csv.DefaultCSVFormat
import org.scalatest.{FlatSpec, Matchers}
import pt.inescn.etl.stream.Load.Frame._
import pt.inescn.etl.stream.Load._
import pt.inescn.app.Utils

/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly pt.inescn.etl.stream.DataFilesSpec"
  *
  * For full stack trace:
  * "~testOnly pt.inescn.etl.stream.DataFilesSpec -- -oF"
  * @see http://www.scalatest.org/user_guide/using_scalatest_with_sbt
  *
  * Created by hmf on 10-10-2018.
  */
class DataFilesSpec extends FlatSpec with Matchers {

  "Load files" should "partially load the start of data files" in {

    // Use files
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

/*
    val files = DataFiles.parseFileNames(dir, matchNames, Utils.parseFileName).toList
    assert( files.size == 90 )
    val parserOk: List[(File, Map[String, String], Map[String, String])] = files.filter( _.isRight ).map(_.right.get)
    assert( parserOk.size == 90 )
    val (filesp,features,units) = parserOk.unzip3

    println(filesp.size)
    features.foreach(e => println(e.mkString(",")))

    scala.util.Random.setSeed(101)
    val shuffled = scala.util.Random.shuffle(parserOk)
    val byClass: Map[String, List[(File, Map[String, String], Map[String, String])]] = shuffled.groupBy{ e => e._2("damage")}
    println(byClass.size) // 2
*/
    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1)
    // Sorting so that tests in different platforms won't fail
    val features3 = features2.toList.sortBy(_._1.path.getFileName.toString)
    //assert( features1.size == 90 )
    //assert( features2.size == 90 )

    val expectedNumberOfFiles = 45

    // No shuffling due to tests
    //val shuffled1 = scala.util.Random.shuffle(features2)

    // Possible better-files bug: we have to convert the iterator to some other collection to avoid the issue
    //val (goodTmp, badTmp) = shuffled1.partition{ e => e._2("damage") == Val("norm") }
    val (good, bad) = features3.partition{ e => e._2("damage") == Val("norm") }
    //println(s"good.size = ${good.size}")
    //println(s"bad.size = ${bad.size}")
    good.size shouldBe expectedNumberOfFiles
    bad.size shouldBe expectedNumberOfFiles

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    // Lets read `numberOfSamples` from all the files
    val numberOfSamples = 2048 //Number of samples taken from the file
    val goodLearn: Frame = Frame.extendCSVs(MyFormat, good.toIterator, SplitOptions.takeOnly(numberOfSamples))

    // Lets just check some of the columns
    val goodIdx = goodLearn("X", "Y", "speed", "damage").iterable.toIndexedSeq
    // Did we read the expected total number of records ?
    goodIdx.size shouldBe numberOfSamples * expectedNumberOfFiles

    // Lets check the data of the first file that was processed
    val tmp1 = good.head._1
    tmp1.path.getFileName.toString shouldBe "rol1_rpm1000_hp0_norm_mm0_exp1.csv"
    //Time(s),X,Y,Z
    // line 1
    goodIdx(0) shouldBe Row("X" -> "0.107271", "Y" -> "-0.202685", "speed" -> "1000", "damage" -> "norm")
    // line 2049 (count the header)
    goodIdx(numberOfSamples - 1) shouldBe Row("X" -> "0.104817", "Y" -> "0.775292", "speed" -> "1000", "damage" -> "norm")

    // Lets check the data of the second file that was processed
    val tmp2 = good(1)._1
    tmp2.path.getFileName.toString shouldBe "rol1_rpm1000_hp0_norm_mm0_exp2.csv"
    //Time(s),X,Y,Z
    goodIdx(numberOfSamples) shouldBe  Row("X" -> "0.578407", "Y" -> "-0.557780", "speed" -> "1000", "damage" -> "norm")
    // line 2049 (count the header)
    goodIdx(2*numberOfSamples-1) shouldBe Row("X" -> "0.717048", "Y" -> "0.670405", "speed" -> "1000", "damage" -> "norm")

    // Lets check the data of the last file that was processed
    val tmp3 = good.last._1
    tmp3.path.getFileName.toString shouldBe "rol3_rpm3000_hp0_norm_mm0_exp5.csv"
    // line 2049 (count the header) the last line
    // 0.369831,3.707467,5.219661
    goodIdx(expectedNumberOfFiles*numberOfSamples-1) shouldBe Row("X" -> "-0.724578", "Y" -> "1.698189", "speed" -> "3000", "damage" -> "norm")
  }

  it should "split data into train and tests streams" in {
    // Use files
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1).toList
    //assert( features1.size == 90 )
    //assert( features2.size == 90 )

    type FileFilter = (String,String)

    // What is used to tag the class
    def classes(r: Row) : FileFilter = {
      val clas = Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      (clas, bearing)
    }
    // learning data has ony 1 class: no failures (total of expectedNumberOfFiles files)
    // avoid data leakage, learn only with bearings 'rol1' and 'rol2'
    //def learnFilter(r: FileFilter) : Boolean = { r._1 == "norm" }
    def learnFilter(r: FileFilter) : Boolean = { (r._1 == "norm") && ((r._2 == "1") || (r._2 == "2")) }
    // test data has any class (total of 2*expectedNumberOfFiles files)
    // avoid data leakage, test only with bearings 'rol3'
    //def testFilter(r: FileFilter) : Boolean = { true }
    def testFilter(r: FileFilter) : Boolean = { r._2 == "3" }

    val (train,test) = DataFiles.splitFiles(features2.toIterator, classes, learnFilter, testFilter, shuffle = false)
    val train_files = train.map(_._1.path.getFileName.toString)
    train_files.toList should contain theSameElementsAs List(
      "rol2_rpm2000_hp0_norm_mm0_exp1.csv",
      "rol2_rpm2000_hp0_norm_mm0_exp3.csv",
      "rol2_rpm3000_hp0_norm_mm0_exp1.csv",
      "rol2_rpm2000_hp0_norm_mm0_exp5.csv",
      "rol2_rpm1000_hp0_norm_mm0_exp1.csv",
      "rol2_rpm3000_hp0_norm_mm0_exp4.csv",
      "rol2_rpm2000_hp0_norm_mm0_exp4.csv",
      "rol2_rpm1000_hp0_norm_mm0_exp5.csv",
      "rol2_rpm2000_hp0_norm_mm0_exp2.csv",
      "rol2_rpm3000_hp0_norm_mm0_exp3.csv",
      "rol2_rpm3000_hp0_norm_mm0_exp2.csv",
      "rol2_rpm3000_hp0_norm_mm0_exp5.csv",
      "rol2_rpm1000_hp0_norm_mm0_exp4.csv",
      "rol2_rpm1000_hp0_norm_mm0_exp2.csv",
      "rol2_rpm1000_hp0_norm_mm0_exp3.csv",
      "rol1_rpm3000_hp0_norm_mm0_exp3.csv",
      "rol1_rpm2000_hp0_norm_mm0_exp1.csv",
      "rol1_rpm2000_hp0_norm_mm0_exp5.csv",
      "rol1_rpm1000_hp0_norm_mm0_exp3.csv",
      "rol1_rpm3000_hp0_norm_mm0_exp4.csv",
      "rol1_rpm1000_hp0_norm_mm0_exp5.csv",
      "rol1_rpm1000_hp0_norm_mm0_exp2.csv",
      "rol1_rpm1000_hp0_norm_mm0_exp1.csv",
      "rol1_rpm2000_hp0_norm_mm0_exp4.csv",
      "rol1_rpm2000_hp0_norm_mm0_exp3.csv",
      "rol1_rpm3000_hp0_norm_mm0_exp1.csv",
      "rol1_rpm2000_hp0_norm_mm0_exp2.csv",
      "rol1_rpm3000_hp0_norm_mm0_exp2.csv",
      "rol1_rpm3000_hp0_norm_mm0_exp5.csv",
      "rol1_rpm1000_hp0_norm_mm0_exp4.csv"
    )

    val test_files = test.map(_._1.path.getFileName.toString)
    test_files.toList should contain theSameElementsAs List(
      "rol3_rpm2000_hp0_norm_mm0_exp5.csv",
      "rol3_rpm1000_hp0_norm_mm0_exp2.csv",
      "rol3_rpm3000_hp0_norm_mm0_exp5.csv",
      "rol3_rpm3000_hp0_norm_mm0_exp3.csv",
      "rol3_rpm2000_hp0_norm_mm0_exp1.csv",
      "rol3_rpm1000_hp0_norm_mm0_exp3.csv",
      "rol3_rpm3000_hp0_norm_mm0_exp2.csv",
      "rol3_rpm3000_hp0_norm_mm0_exp1.csv",
      "rol3_rpm1000_hp0_norm_mm0_exp4.csv",
      "rol3_rpm2000_hp0_norm_mm0_exp3.csv",
      "rol3_rpm1000_hp0_norm_mm0_exp5.csv",
      "rol3_rpm3000_hp0_norm_mm0_exp4.csv",
      "rol3_rpm2000_hp0_norm_mm0_exp2.csv",
      "rol3_rpm2000_hp0_norm_mm0_exp4.csv",
      "rol3_rpm1000_hp0_norm_mm0_exp1.csv",
      "rol3_rpm3000_hp0_b_mm0_exp2.csv",
      "rol3_rpm1000_hp0_b_mm0_exp4.csv",
      "rol3_rpm2000_hp0_b_mm0_exp2.csv",
      "rol3_rpm2000_hp0_b_mm0_exp4.csv",
      "rol3_rpm3000_hp0_b_mm0_exp1.csv",
      "rol3_rpm1000_hp0_b_mm0_exp2.csv",
      "rol3_rpm1000_hp0_b_mm0_exp1.csv",
      "rol3_rpm3000_hp0_b_mm0_exp5.csv",
      "rol3_rpm1000_hp0_b_mm0_exp5.csv",
      "rol3_rpm2000_hp0_b_mm0_exp5.csv",
      "rol3_rpm2000_hp0_b_mm0_exp3.csv",
      "rol3_rpm3000_hp0_b_mm0_exp3.csv",
      "rol3_rpm2000_hp0_b_mm0_exp1.csv",
      "rol3_rpm1000_hp0_b_mm0_exp3.csv",
      "rol3_rpm3000_hp0_b_mm0_exp4.csv"
    )

    val (traint,testt) = DataFiles.splitFiles(features2.toIterator, classes, learnFilter, testFilter, shuffle = false)
    // Need to sort to ensure results in multiple platforms are the same
    val trains = traint.toList.sortBy(_._1.path.getFileName.toString).toIterator
    val tests = testt.toList.sortBy(_._1.path.getFileName.toString).toIterator
    val learnFrame: Frame = Frame.extendCSVs(MyFormat, trains, SplitOptions.all)
    val testFrame: Frame = Frame.extendCSVs(MyFormat, tests, SplitOptions.all)
    val fullData = learnFrame ++ testFrame

    // Create a counter
    def from(c: Long): Stream[Long] = c #:: from(c + 1)
    val count = Frame("id" -> from(1L).toIterator)
    val indexedFullData = fullData || count


    val tmp = File(JSystem.getProperty("java.io.tmpdir")) / "fulldata.csv"
    Load.save(tmp, MyFormat, indexedFullData)

    val testTmp = File(JSystem.getProperty("java.io.tmpdir")) / "fulldata.csv"
    testTmp.exists shouldBe true

    val data = testTmp.lineIterator

    data.hasNext shouldBe true
    val header = data.next()
    header shouldBe "X,Time(s),Y,load,damage,exp,damage_size,id,bearing,speed,Z"

    data.hasNext shouldBe true
    val line1 = data.next()
    line1 shouldBe "0.107271,0.000000,-0.202685,0,norm,1,0,1,1,1000,-0.700631"

    data.hasNext shouldBe true
    val lastLine = data.toIterable.last
    lastLine shouldBe "-1.073023,3.999805,-1.688109,0,norm,5,0,1228800,3,3000,2.950461"
  }

  it should "partially load the end of data files" in {
    // Use files
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1)
    //assert( features1.size == 90 )
    //assert( features2.size == 90 )

    val expectedNumberOfFiles = 45 // for each good and bad, 90 total
    val numberOfSamplesPerFile = 20480 // excluding the header

    // No shuffling due to tests
    //val shuffled1 = scala.util.Random.shuffle(features2)

    // Possible better-files bug: we have to convert the iterator to some other collection to avoid the issue
    //val (goodTmp, badTmp) = shuffled1.partition{ e => e._2("damage") == Val("norm") }
    val (good, bad) = features2.toList.partition{ e => e._2("damage") == Val("norm") }
    //println(s"good.size = ${good.size}")
    //println(s"bad.size = ${bad.size}")
    good.size shouldBe expectedNumberOfFiles
    bad.size shouldBe expectedNumberOfFiles


    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    // Lets read `numberOfSamples` from all the files
    val numberOfSamples = 2048 //Start of file: number of samples to learn

    // Sample part of the 'no fail' signal for learning
    val goodLearn: Frame = Frame.extendCSVs(MyFormat, good.toIterator, SplitOptions.takeOnly(numberOfSamples))
    val goodTest: Frame = Frame.extendCSVs(MyFormat, good.toIterator, SplitOptions.dropOnly(numberOfSamples))
    // Sample all of the 'fail' signal for testing
    val badTest: Frame = Frame.extendCSVs(MyFormat, good.toIterator)

    // Lets just check some of the columns
    val goodLearnIdx = goodLearn("X", "Y", "speed", "damage").iterable.toIndexedSeq
    // Did we read the expected total number of records ?
    goodLearnIdx.size shouldBe numberOfSamples * expectedNumberOfFiles

    // Lets just check some of the columns
    val goodTestIdx = goodTest("X", "Y", "speed", "damage").iterable.toIndexedSeq
    // Did we read the expected total number of records ?
    goodTestIdx.size shouldBe (numberOfSamplesPerFile - numberOfSamples) * expectedNumberOfFiles

    // Lets just check some of the columns
    val badIdx = badTest("X", "Y", "speed", "damage").iterable.toIndexedSeq
    // Did we read the expected total number of records ?
    badIdx.size shouldBe numberOfSamplesPerFile * expectedNumberOfFiles
  }


  it should "intersperse data files with test protocol with basic types for keys" in {
    // Use files
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1)
    //assert( features1.size == 90 )
    //assert( features2.toList.size == 90 )

    // What is used to tag the class
    def classes(r: Row) : String= { Load.unpack[String](r("damage")).right.get }
    // learning data has ony 1 class: no failures (total of expectedNumberOfFiles files)
    def learnFilter(r: String) : Boolean = { r == "norm" }
    // test data has any class (total of 2*expectedNumberOfFiles files)
    def testFilter(r: String) : Boolean = { true }

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = false)
    //println(trainData.size)  // 45
    //println(testData.size)   // 90

    val expectedNumberOfTrainFiles = 45 // only good
    val expectedNumberOfTestFiles = 90 // good and bad
    val numberOfSamplesPerFile = 20480 // excluding the header

    // Lets read `numberOfSamples` from all the files for training
    val numberOfSamples = 2048 //Start of file: number of samples to learn

    // We generate training data so that each file uses only the first 'numberOfSamples'
    // The test files will ignore those first 'numberOfSamples' and use the rst of the data
    // for testing (test sample size = 0). Datafiles are shuffled by default
    val (learn, test) = Frame.splitExtendCSVs(MyFormat, trainData, testData, numberOfSamples, numberOfSamples, 0, shuffle = false)

    // Lets just check some of the columns
    val learnIdx = learn("X", "Y", "speed", "damage").iterable.toIndexedSeq
    // Did we read the expected total number of records ?
    learnIdx.size shouldBe numberOfSamples * expectedNumberOfTrainFiles

    // Lets just check some of the columns
    val testIdx = test("X", "Y", "speed", "damage").iterable.toIndexedSeq
    testIdx.size shouldBe (numberOfSamplesPerFile - numberOfSamples) * expectedNumberOfTestFiles
  }


  it should "intersperse data files with test protocol with Va[_] types for keys" in {
    // Use files
    val dir = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1)
    //assert( features1.size == 90 )
    //assert( features2.toList.size == 90 )

    // What is used to tag the class
    def classes(r: Row) : Val[_] = { r("damage") }
    // learning data has ony 1 class: no failures (total of expectedNumberOfFiles files)
    def learnFilter(r: Val[_]) : Boolean = { r == Val("norm") }
    // test data has any class (total of 2*expectedNumberOfFiles files)
    def testFilter(r: Val[_]) : Boolean = { true }

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    val (trainData,testData) = DataFiles.splitFiles(features2, classes, learnFilter, testFilter, shuffle = false)
    //println(trainData.size)  // 45
    //println(testData.size)   // 90

    val expectedNumberOfTrainFiles = 45 // only good
    val expectedNumberOfTestFiles = 90 // good and bad
    val numberOfSamplesPerFile = 20480 // excluding the header

    // Lets read `numberOfSamples` from all the files for training
    val numberOfSamples = 2048 //Start of file: number of samples to learn

    // We generate training data so that each file uses only the first 'numberOfSamples'
    // The test files will ignore those first 'numberOfSamples' and use the rst of the data
    // for testing (test sample size = 0). Datafiles are shuffled by default
    val (learn, test) = Frame.splitExtendCSVs(MyFormat, trainData, testData, numberOfSamples,numberOfSamples, 0, shuffle = false)

    // Lets just check some of the columns
    val learnIdx = learn("X", "Y", "speed", "damage").iterable.toIndexedSeq
    // Did we read the expected total number of records ?
    learnIdx.size shouldBe numberOfSamples * expectedNumberOfTrainFiles

    // Lets just check some of the columns
    val testIdx = test("X", "Y", "speed", "damage").iterable.toIndexedSeq
    testIdx.size shouldBe (numberOfSamplesPerFile - numberOfSamples) *  expectedNumberOfTestFiles
  }

  "Better-files" should "handle the partitioning of files" in {

    // https://github.com/pathikrit/better-files/issues/270
    // Fixed
    /*
[info]   java.lang.IllegalStateException:
[info]   at java.nio.file.FileTreeIterator.hasNext(FileTreeIterator.java:103)
[info]   at java.util.Spliterators$IteratorSpliterator.tryAdvance(Spliterators.java:1811)
[info]   at java.util.stream.StreamSpliterators$WrappingSpliterator.lambda$initPartialTraversalState$0(StreamSpliterators.java:294)
[info]   at java.util.stream.StreamSpliterators$AbstractWrappingSpliterator.fillBuffer(StreamSpliterators.java:206)
[info]   at java.util.stream.StreamSpliterators$AbstractWrappingSpliterator.doAdvance(StreamSpliterators.java:169)
[info]   at java.util.stream.StreamSpliterators$WrappingSpliterator.tryAdvance(StreamSpliterators.java:300)
[info]   at java.util.Spliterators$1Adapter.hasNext(Spliterators.java:681)
[info]   at scala.collection.convert.Wrappers$JIteratorWrapper.hasNext(Wrappers.scala:39)
[info]   at better.files.ManagedResource.$anonfun$flatMap$1(ManagedResource.scala:83)
[info]   at better.files.Implicits$IteratorExtensions$$anon$1.hasNext(Implicits.scala:52)
     */

    // Use files
    val dir: File = cwd / ".." / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    val matches_tmp: Iterator[File] = dir.glob(matchNames)
    //val (good, bad) = matches_tmp.partition{ e => e.path.getFileName.toString.contains("norm") } // fails
    val (good, bad) = matches_tmp.toList.partition{ e => e.path.getFileName.toString.contains("norm") } // Ok
    //println(s"good.size = ${good.size}")
    //println(s"bad.size = ${bad.size}")
    assert( good.size == 45 ) // breaks here
    assert( bad.size == 45 )

    //fail
  }

}
