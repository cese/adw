/** *****************************************************************************
 * Copyright (C) 2017 INESC-TEC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * *****************************************************************************/
package pt.inescn.etl

import org.scalatest._
import java.time.Instant

import pt.inescn.etl.LightSVM.{SparseFold, ValidateDepVar}

//import java.lang.{System => JSystem}

import better.files.Dsl._
import kantan.csv.{CsvReader, ReadResult, RowDecoder, rfc}
import pt.inescn.etl.Load._
import pt.inescn.features.Stats.DescriptiveStats._
import pt.inescn.models.Data.SparseMatrixData
import pt.inescn.utils.ADWError
import pt.inescn.utils.Utils._


/**
  *
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "root/testOnly pt.inescn.etl.LoadSpec"
  *
  * Created by hmf on 07-06-2017.
  */
class LoadSpec  extends FlatSpec with Matchers {

  "Loading Time-series from a file" should "be able to load the first 2 columns only" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData1[Double](data)
    val rr = TimeSeriesFrame1().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = r.getOrElse(emptyTimeSeriesFrame1)
    val d0 = rr.getOrElse(TimeSeriesFrame1())
    //val d0: FTT[(Instant, Double, Double)] = r.getOrElse(Alternative2F())
    d0.dt.head should be(parseInstantUTC("2017-01-01 00:00:00.001"))
    d0.value1.head should be(0)
  }

  it should "be able to load the first 3 columns only" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData2[Double,Double](data)
    val rr = TimeSeriesFrame2().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame2)
    val d0 = rr.getOrElse(TimeSeriesFrame2())
    d0.dt.head should be(parseInstantUTC("2017-01-01 00:00:00.001"))
    d0.value1.head should be(0)
    d0.value2.head should be(0)
  }

  it should "be able to load the first 4 columns only" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData3[Double,Double,Double](data)
    val rr = TimeSeriesFrame3().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame3)
    val d0 = rr.getOrElse(TimeSeriesFrame3())
    d0.dt.head should be(parseInstantUTC("2017-01-01 00:00:00.001"))
    d0.value1.head should be(0)
    d0.value2.head should be(0)
    d0.value3.head should be(-0.0062634552)
  }

  it should "be able to load the first 5 columns only" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame4().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame4)
    val d0 = rr.getOrElse(TimeSeriesFrame4())
    d0.dt.head should be(parseInstantUTC("2017-01-01 00:00:00.001"))
    d0.value1.head should be(0)
    d0.value2.head should be(0)
    d0.value3.head should be(-0.0062634552)
    d0.value4.head should be(-0.0062634552)

    val idx = 999
    d0.dt(idx) should be(parseInstantUTC("2017-01-01 00:00:01.000"))
    d0.value1(idx) should be(49.95)
    d0.value2(idx) should be(-0.3102751539)
    d0.value3(idx) should be(-0.0589078244)
    d0.value4(idx) should be(-0.3691829783)
  }

  val eps = 1e-6

  "Processing the time-series frame as a batch" should "generate the all descriptive statistics of Frame-1" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame1().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame4)
    val d0 = rr.getOrElse(TimeSeriesFrame1())
    val stats1: Values = batch(d0)
    //println(stats4)

    stats1.geo_mean.value should be(0.0 +- eps)
    stats1.kurtosis.value should be(-1.2 +- eps)
    stats1.max.value should be(49.95 +- eps)
    stats1.mean.value should be(24.975 +- eps)
    stats1.min.value should be(0.0 +- eps)
    stats1.n.value should be(1000.0 +- eps)
    stats1.quartile_1.value should be(12.4624999 +- eps)
    stats1.quartile_2.value should be(24.975 +- eps)
    stats1.quartile_3.value should be(37.4875 +- eps)
    stats1.population_variance.value should be(208.333125 +- eps)
    stats1.quadratic_mean.value should be(28.84586192159978 +- eps)
    stats1.skewness.value should be(-1.2116435509741383E-16 +- eps)
    stats1.standard_deviation.value should be(14.44097180478747 +- eps)
    stats1.sum.value should be(24975.0 +- eps)
    stats1.sum_squares.value should be(832083.7500000001 +- eps)
    stats1.variance.value should be(208.54166666666666 +- eps)
  }

  it should "generate the all descriptive statistics of Frame-2" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame2().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame4)
    val d0 = rr.getOrElse(TimeSeriesFrame2())
    val stats2: (Values, Values) = batch(d0)
    //println(stats4)

    stats2._1.geo_mean.value should be(0.0 +- eps)
    stats2._1.kurtosis.value should be(-1.2 +- eps)
    stats2._1.max.value should be(49.95 +- eps)
    stats2._1.mean.value should be(24.975 +- eps)
    stats2._1.min.value should be(0.0 +- eps)
    stats2._1.n.value should be(1000.0 +- eps)
    stats2._1.quartile_1.value should be(12.4624999 +- eps)
    stats2._1.quartile_2.value should be(24.975 +- eps)
    stats2._1.quartile_3.value should be(37.4875 +- eps)
    stats2._1.population_variance.value should be(208.333125 +- eps)
    stats2._1.quadratic_mean.value should be(28.84586192159978 +- eps)
    stats2._1.skewness.value should be(-1.2116435509741383E-16 +- eps)
    stats2._1.standard_deviation.value should be(14.44097180478747 +- eps)
    stats2._1.sum.value should be(24975.0 +- eps)
    stats2._1.sum_squares.value should be(832083.7500000001 +- eps)
    stats2._1.variance.value should be(208.54166666666666 +- eps)

    stats2._2.geo_mean.value.isNaN shouldBe true
    stats2._2.kurtosis.value should be(-1.508531793268359 +- eps)
    stats2._2.max.value should be(0.9999952152 +- eps)
    stats2._2.mean.value should be(8.317208750000194E-4 +- eps)
    stats2._2.min.value should be(-0.9999902066 +- eps)
    stats2._2.n.value should be(1000.0 +- eps)
    stats2._2.quartile_1.value should be(-0.7102240882 +- eps)
    stats2._2.quartile_2.value should be(0.007741151200000001 +- eps)
    stats2._2.quartile_3.value should be(0.711083155375 +- eps)
    stats2._2.population_variance.value should be(0.502494605956662 +- eps)
    stats2._2.quadratic_mean.value should be(0.7088690271949222 +- eps)
    stats2._2.skewness.value should be(-0.003429275010240599 +- eps)
    stats2._2.standard_deviation.value should be(0.7092232395799098 +- eps)
    stats2._2.sum.value should be(0.8317208750000289 +- eps)
    stats2._2.sum_squares.value should be(502.49529771627533 +- eps)
    stats2._2.variance.value should be(0.5029976035602222 +- eps)

  }


  it should "generate the all descriptive statistics of Frame-3" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame3().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame4)
    val d0 = rr.getOrElse(TimeSeriesFrame3())
    val stats3: (Values, Values, Values) = batch(d0)
    //println(stats4)

    stats3._1.geo_mean.value should be(0.0 +- eps)
    stats3._1.kurtosis.value should be(-1.2 +- eps)
    stats3._1.max.value should be(49.95 +- eps)
    stats3._1.mean.value should be(24.975 +- eps)
    stats3._1.min.value should be(0.0 +- eps)
    stats3._1.n.value should be(1000.0 +- eps)
    stats3._1.quartile_1.value should be(12.4624999 +- eps)
    stats3._1.quartile_2.value should be(24.975 +- eps)
    stats3._1.quartile_3.value should be(37.4875 +- eps)
    stats3._1.population_variance.value should be(208.333125 +- eps)
    stats3._1.quadratic_mean.value should be(28.84586192159978 +- eps)
    stats3._1.skewness.value should be(-1.2116435509741383E-16 +- eps)
    stats3._1.standard_deviation.value should be(14.44097180478747 +- eps)
    stats3._1.sum.value should be(24975.0 +- eps)
    stats3._1.sum_squares.value should be(832083.7500000001 +- eps)
    stats3._1.variance.value should be(208.54166666666666 +- eps)

    stats3._2.geo_mean.value.isNaN shouldBe true
    stats3._2.kurtosis.value should be(-1.508531793268359 +- eps)
    stats3._2.max.value should be(0.9999952152 +- eps)
    stats3._2.mean.value should be(8.317208750000194E-4 +- eps)
    stats3._2.min.value should be(-0.9999902066 +- eps)
    stats3._2.n.value should be(1000.0 +- eps)
    stats3._2.quartile_1.value should be(-0.7102240882 +- eps)
    stats3._2.quartile_2.value should be(0.007741151200000001 +- eps)
    stats3._2.quartile_3.value should be(0.711083155375 +- eps)
    stats3._2.population_variance.value should be(0.502494605956662 +- eps)
    stats3._2.quadratic_mean.value should be(0.7088690271949222 +- eps)
    stats3._2.skewness.value should be(-0.003429275010240599 +- eps)
    stats3._2.standard_deviation.value should be(0.7092232395799098 +- eps)
    stats3._2.sum.value should be(0.8317208750000289 +- eps)
    stats3._2.sum_squares.value should be(502.49529771627533 +- eps)
    stats3._2.variance.value should be(0.5029976035602222 +- eps)

    stats3._3.geo_mean.value.isNaN shouldBe true
    stats3._3.kurtosis.value should be(-0.1925767043586446 +- eps)
    stats3._3.max.value should be(0.0883366126 +- eps)
    stats3._3.mean.value should be(1.6997754644980518E-5 +- eps)
    stats3._3.min.value should be(-0.0889319136 +- eps)
    stats3._3.n.value should be(1000.0 +- eps)
    stats3._3.quartile_1.value should be(-0.0204631927 +- eps)
    stats3._3.quartile_2.value should be(0.0003078098 +- eps)
    stats3._3.quartile_3.value should be(0.02088272235 +- eps)
    stats3._3.population_variance.value should be(8.939012708021035E-4 +- eps)
    stats3._3.quadratic_mean.value should be(0.029898186562495148 +- eps)
    stats3._3.skewness.value should be(-0.013457266568580248 +- eps)
    stats3._3.standard_deviation.value should be(0.02991314204273721 +- eps)
    stats3._3.sum.value should be(0.016997754644979143 +- eps)
    stats3._3.sum_squares.value should be(0.8939015597257655 +- eps)
    stats3._3.variance.value should be(8.947960668689724E-4 +- eps)
  }

  it should "generate the all descriptive statistics of Frame-4" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame4().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame4)
    val d0 = rr.getOrElse(TimeSeriesFrame4())
    val stats4: (Values, Values, Values, Values) = batch(d0)
    //println(stats4)

    stats4._1.geo_mean.value should be(0.0 +- eps)
    stats4._1.kurtosis.value should be(-1.2 +- eps)
    stats4._1.max.value should be(49.95 +- eps)
    stats4._1.mean.value should be(24.975 +- eps)
    stats4._1.min.value should be(0.0 +- eps)
    stats4._1.n.value should be(1000.0 +- eps)
    stats4._1.quartile_1.value should be(12.4624999 +- eps)
    stats4._1.quartile_2.value should be(24.975 +- eps)
    stats4._1.quartile_3.value should be(37.4875 +- eps)
    stats4._1.population_variance.value should be(208.333125 +- eps)
    stats4._1.quadratic_mean.value should be(28.84586192159978 +- eps)
    stats4._1.skewness.value should be(-1.2116435509741383E-16 +- eps)
    stats4._1.standard_deviation.value should be(14.44097180478747 +- eps)
    stats4._1.sum.value should be(24975.0 +- eps)
    stats4._1.sum_squares.value should be(832083.7500000001 +- eps)
    stats4._1.variance.value should be(208.54166666666666 +- eps)

    stats4._2.geo_mean.value.isNaN shouldBe true
    stats4._2.kurtosis.value should be(-1.508531793268359 +- eps)
    stats4._2.max.value should be(0.9999952152 +- eps)
    stats4._2.mean.value should be(8.317208750000194E-4 +- eps)
    stats4._2.min.value should be(-0.9999902066 +- eps)
    stats4._2.n.value should be(1000.0 +- eps)
    stats4._2.quartile_1.value should be(-0.7102240882 +- eps)
    stats4._2.quartile_2.value should be(0.007741151200000001 +- eps)
    stats4._2.quartile_3.value should be(0.711083155375 +- eps)
    stats4._2.population_variance.value should be(0.502494605956662 +- eps)
    stats4._2.quadratic_mean.value should be(0.7088690271949222 +- eps)
    stats4._2.skewness.value should be(-0.003429275010240599 +- eps)
    stats4._2.standard_deviation.value should be(0.7092232395799098 +- eps)
    stats4._2.sum.value should be(0.8317208750000289 +- eps)
    stats4._2.sum_squares.value should be(502.49529771627533 +- eps)
    stats4._2.variance.value should be(0.5029976035602222 +- eps)

    stats4._3.geo_mean.value.isNaN shouldBe true
    stats4._3.kurtosis.value should be(-0.1925767043586446 +- eps)
    stats4._3.max.value should be(0.0883366126 +- eps)
    stats4._3.mean.value should be(1.6997754644980518E-5 +- eps)
    stats4._3.min.value should be(-0.0889319136 +- eps)
    stats4._3.n.value should be(1000.0 +- eps)
    stats4._3.quartile_1.value should be(-0.0204631927 +- eps)
    stats4._3.quartile_2.value should be(0.0003078098 +- eps)
    stats4._3.quartile_3.value should be(0.02088272235 +- eps)
    stats4._3.population_variance.value should be(8.939012708021035E-4 +- eps)
    stats4._3.quadratic_mean.value should be(0.029898186562495148 +- eps)
    stats4._3.skewness.value should be(-0.013457266568580248 +- eps)
    stats4._3.standard_deviation.value should be(0.02991314204273721 +- eps)
    stats4._3.sum.value should be(0.016997754644979143 +- eps)
    stats4._3.sum_squares.value should be(0.8939015597257655 +- eps)
    stats4._3.variance.value should be(8.947960668689724E-4 +- eps)

    stats4._4.geo_mean.value.isNaN shouldBe true
    stats4._4.kurtosis.value should be(-1.502571998082373 +- eps)
    stats4._4.max.value should be(1.0836877175 +- eps)
    stats4._4.mean.value should be(8.487186279000021E-4 +- eps)
    stats4._4.min.value should be(-1.0614232479 +- eps)
    stats4._4.n.value should be(1000.0 +- eps)
    stats4._4.quartile_1.value should be(-0.7039502091750001 +- eps)
    stats4._4.quartile_2.value should be(0.01516409095 +- eps)
    stats4._4.quartile_3.value should be(0.7068119806000001 +- eps)
    stats4._4.population_variance.value should be(0.5054316941191698 +- eps)
    stats4._4.quadratic_mean.value should be(0.7109377008166607 +- eps)
    stats4._4.skewness.value should be(-0.005937141573998348 +- eps)
    stats4._4.standard_deviation.value should be(0.7112929296365321 +- eps)
    stats4._4.sum.value should be(0.8487186279000241 +- eps)
    stats4._4.sum_squares.value should be(505.43241444247974 +- eps)
    stats4._4.variance.value should be(0.5059376317509207 +- eps)

  }

  "Processing the time-series frame as a stream" should "generate most descriptive statistics (percentiles are approximations)" in {
    import better.files.Dsl._
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame4().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame4)
    val d0 = rr.getOrElse(TimeSeriesFrame4())
    val stats4x = streaming(d0)
    //val stats4x = streamingStats(d0)
    val stats4: (Values, Values, Values, Values) = unpack(stats4x)
    //println(stats4)

    stats4._1.geo_mean.value should be(0.0 +- eps)
    stats4._1.kurtosis.value.isNaN shouldBe true
    stats4._1.max.value should be(49.95 +- eps)
    stats4._1.mean.value should be(24.975 +- eps)
    stats4._1.min.value should be(0.0 +- eps)
    stats4._1.n.value should be(1000.0 +- eps)
    stats4._1.quartile_1.value should be(12.4624999 +- eps)
    stats4._1.quartile_2.value should be(24.975 +- eps)
    stats4._1.quartile_3.value should be(37.4875 +- eps)
    stats4._1.population_variance.value should be(208.333125 +- eps)
    stats4._1.quadratic_mean.value should be(28.84586192159978 +- eps)
    stats4._1.skewness.value.isNaN shouldBe true
    stats4._1.standard_deviation.value should be(14.44097180478747 +- eps)
    stats4._1.sum.value should be(24975.0 +- eps)
    stats4._1.sum_squares.value should be(832083.7500000001 +- eps)
    stats4._1.variance.value should be(208.54166666666666 +- eps)

    stats4._2.geo_mean.value.isNaN shouldBe true
    stats4._2.kurtosis.value.isNaN shouldBe true
    stats4._2.max.value should be(0.9999952152 +- eps)
    stats4._2.mean.value should be(8.317208750000194E-4 +- eps)
    stats4._2.min.value should be(-0.9999902066 +- eps)
    stats4._2.n.value should be(1000.0 +- eps)
    stats4._2.quartile_1.value should be(-0.7102240882 +- eps)
    stats4._2.quartile_2.value should be(0.007741151200000001 +- eps)
    stats4._2.quartile_3.value should be(0.711083155375 +- eps)
    stats4._2.population_variance.value should be(0.502494605956662 +- eps)
    stats4._2.quadratic_mean.value should be(0.7088690271949222 +- eps)
    stats4._2.skewness.value.isNaN shouldBe true
    stats4._2.standard_deviation.value should be(0.7092232395799098 +- eps)
    stats4._2.sum.value should be(0.8317208750000289 +- eps)
    stats4._2.sum_squares.value should be(502.49529771627533 +- eps)
    stats4._2.variance.value should be(0.5029976035602222 +- eps)

    stats4._3.geo_mean.value.isNaN shouldBe true
    stats4._3.kurtosis.value.isNaN shouldBe true
    stats4._3.max.value should be(0.0883366126 +- eps)
    stats4._3.mean.value should be(1.6997754644980518E-5 +- eps)
    stats4._3.min.value should be(-0.0889319136 +- eps)
    stats4._3.n.value should be(1000.0 +- eps)
    stats4._3.quartile_1.value should be(-0.0204631927 +- eps)
    stats4._3.quartile_2.value should be(0.0003078098 +- eps)
    stats4._3.quartile_3.value should be(0.02088272235 +- eps)
    stats4._3.population_variance.value should be(8.939012708021035E-4 +- eps)
    stats4._3.quadratic_mean.value should be(0.029898186562495148 +- eps)
    stats4._3.skewness.value.isNaN shouldBe true
    stats4._3.standard_deviation.value should be(0.02991314204273721 +- eps)
    stats4._3.sum.value should be(0.016997754644979143 +- eps)
    stats4._3.sum_squares.value should be(0.8939015597257655 +- eps)
    stats4._3.variance.value should be(8.947960668689724E-4 +- eps)

    stats4._4.geo_mean.value.isNaN shouldBe true
    stats4._4.kurtosis.value.isNaN shouldBe true
    stats4._4.max.value should be(1.0836877175 +- eps)
    stats4._4.mean.value should be(8.487186279000021E-4 +- eps)
    stats4._4.min.value should be(-1.0614232479 +- eps)
    stats4._4.n.value should be(1000.0 +- eps)
    stats4._4.quartile_1.value should be(-0.7039502091750001 +- eps)
    stats4._4.quartile_2.value should be(0.01516409095 +- eps)
    stats4._4.quartile_3.value should be(0.7068119806000001 +- eps)
    stats4._4.population_variance.value should be(0.5054316941191698 +- eps)
    stats4._4.quadratic_mean.value should be(0.7109377008166607 +- eps)
    stats4._4.skewness.value.isNaN shouldBe true
    stats4._4.standard_deviation.value should be(0.7112929296365321 +- eps)
    stats4._4.sum.value should be(0.8487186279000241 +- eps)
    stats4._4.sum_squares.value should be(505.43241444247974 +- eps)
    stats4._4.variance.value should be(0.5059376317509207 +- eps)

  }

  it should "allow to check if sampling is consistent in Frame-1" in {
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame1().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame1)
    val d0 = rr.getOrElse(TimeSeriesFrame1())
    d0.isOrdered shouldBe true
    d0.hasSameSamplingRate shouldBe true

    val max = d0.delta.max.toNanos
    val min = d0.delta.min.toNanos
    val average = d0.delta.map(_.toNanos).sum / d0.delta.length
    max shouldBe min
    average shouldBe min
  }

  it should "allow to check if sampling is consistent in Frame-2" in {
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame2().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame2)
    val d0 = rr.getOrElse(TimeSeriesFrame2())
    d0.isOrdered shouldBe true
    d0.hasSameSamplingRate shouldBe true

    val max = d0.delta.max.toNanos
    val min = d0.delta.min.toNanos
    val average = d0.delta.map(_.toNanos).sum / d0.delta.length
    max shouldBe min
    average shouldBe min
  }

  it should "allow to check if sampling is consistent in Frame-3" in {
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame3().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame1)
    val d0 = rr.getOrElse(TimeSeriesFrame3())
    d0.isOrdered shouldBe true
    d0.hasSameSamplingRate shouldBe true

    val max = d0.delta.max.toNanos
    val min = d0.delta.min.toNanos
    val average = d0.delta.map(_.toNanos).sum / d0.delta.length
    max shouldBe min
    average shouldBe min
  }

  it should "allow to check if sampling is consistent in Frame-4" in {
    // We need to bring in shapeless "compile time reflection"
    import kantan.csv.generic._

    val data = cwd / ".." / "data/inegi/test1.csv"
    //val rr = loadData4[Double,Double,Double,Double](data)
    val rr = TimeSeriesFrame4().load(data)
    //println(rr)
    rr.isRight should be(true)

    //val d0 = rr.getOrElse(emptyTimeSeriesFrame4)
    val d0 = rr.getOrElse(TimeSeriesFrame4())
    d0.isOrdered shouldBe true
    d0.hasSameSamplingRate shouldBe true

    val max = d0.delta.max.toNanos
    val min = d0.delta.min.toNanos
    val average = d0.delta.map(_.toNanos).sum / d0.delta.length
    max shouldBe min
    average shouldBe min
  }

  "Simulation signals" should "allow addition" in {
    import pt.inescn.samplers.Function
    import pt.inescn.samplers.{Base => BaseOp}
    import pt.inescn.dsp.Fourier

    val f1 = 1 // Hz
    val f2 = 2 // Hz
    val f3 = 4 // Hz
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
    // samplesTo at 4*2 = 8Hz and samplesTo for 2.2 seconds
    val dur = 2.2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,dur)
    val ts1 = Function.TimeStamp("2017-06-16T11:35:54.100000Z", dd)
    val tss_1 = ts1(xx.length)

    tss_1.length shouldBe 17
    dd shouldBe 0.125 // seconds

    tss_1.head shouldBe Instant.parse("2017-06-16T11:35:54.100000Z")
    tss_1(1) shouldBe Instant.parse("2017-06-16T11:35:54.225Z")
    tss_1(2) shouldBe Instant.parse("2017-06-16T11:35:54.350Z")
    tss_1.last shouldBe Instant.parse("2017-06-16T11:35:56.100Z") // not quite there

    val sig1 = Function.Sin(f2, 0.5, Math.toRadians(-180))
    val sig2 = Function.Sin(f2, 0.5, 0)
    val sig5 = BaseOp.Sum(sig1, sig2)
    val sig6 = sig1 + sig2

    val s1 = sig1.toStream.take(tss_1.length)
    val s2 = sig2.toStream.take(tss_1.length)
    val s5 = sig5.toStream.take(tss_1.length)
    val result = s1.zip(s2).map( x => x._1 + x._2)

    s5 should contain theSameElementsInOrderAs result
    sig6(tss_1.length) should contain theSameElementsInOrderAs result
  }

  it should "allow subtraction" in {
    import pt.inescn.samplers.Function
    import pt.inescn.samplers.{Base => BaseOp}
    import pt.inescn.dsp.Fourier

    val f1 = 1 // Hz
    val f2 = 2 // Hz
    val f3 = 4 // Hz
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
    // samplesTo at 4*2 = 8Hz and samplesTo for 2.2 seconds
    val dur = 2.2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,dur)
    val ts1 = Function.TimeStamp("2017-06-16T11:35:54.100000Z", dd)
    val tss_1 = ts1(xx.length)

    tss_1.length shouldBe 17
    dd shouldBe 0.125 // seconds

    tss_1.head shouldBe Instant.parse("2017-06-16T11:35:54.100000Z")
    tss_1(1) shouldBe Instant.parse("2017-06-16T11:35:54.225Z")
    tss_1(2) shouldBe Instant.parse("2017-06-16T11:35:54.350Z")
    tss_1.last shouldBe Instant.parse("2017-06-16T11:35:56.100Z") // not quite there

    val sig1 = Function.Sin(f2, 1, 0)
    val sig2 = Function.Sin(f2, 1, 0)
    val sig5 = BaseOp.Subtract(sig1, sig2)
    val sig6 = sig1 - sig2

    val s1 = sig1(tss_1.length)
    val s2 = sig2(tss_1.length)
    val s5 = sig5(tss_1.length)
    val result = s1.zip(s2).map( x => x._1 - x._2)

    s5 should contain theSameElementsInOrderAs result
    sig6(tss_1.length) should contain theSameElementsInOrderAs result
  }

  it should "allow multiplication" in {
    import pt.inescn.samplers.Function
    import pt.inescn.samplers.{Base => BaseOp}
    import pt.inescn.dsp.Fourier

    val f1 = 1 // Hz
    val f2 = 2 // Hz
    val f3 = 4 // Hz
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
    // samplesTo at 4*2 = 8Hz and samplesTo for 2.2 seconds
    val dur = 2.2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,dur)
    val ts1 = Function.TimeStamp("2017-06-16T11:35:54.100000Z", dd)
    val tss_1 = ts1(xx.length)

    tss_1.length shouldBe 17
    dd shouldBe 0.125 // seconds

    tss_1.head shouldBe Instant.parse("2017-06-16T11:35:54.100000Z")
    tss_1(1) shouldBe Instant.parse("2017-06-16T11:35:54.225Z")
    tss_1(2) shouldBe Instant.parse("2017-06-16T11:35:54.350Z")
    tss_1.last shouldBe Instant.parse("2017-06-16T11:35:56.100Z") // not quite there

    val sig1 = Function.Sin(f1, 1, Math.toRadians(-45))
    val sig2 = Function.Sin(f2, 0.5, 0)
    val sig5 = BaseOp.Multiply(sig1, sig2)
    val sig6 = sig1 * sig2

    val s1 = sig1(tss_1.length)
    val s2 = sig2(tss_1.length)
    val s5 = sig5(tss_1.length)
    val result = s1.zip(s2).map( x => x._1 * x._2)

    s5 should contain theSameElementsInOrderAs result
    sig6(tss_1.length) should contain theSameElementsInOrderAs result
  }

  it should "allow division" in {
    import pt.inescn.samplers.Function
    import pt.inescn.samplers.{Base => BaseOp}
    import pt.inescn.dsp.Fourier

    val f1 = 1 // Hz
    val f2 = 2 // Hz
    val f3 = 4 // Hz
    val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
    // samplesTo at 4*2 = 8Hz and samplesTo for 2.2 seconds
    val dur = 2.2
    val (dd,xx) = Fourier.samplingSpecification(samplingFreq,dur)
    val ts1 = Function.TimeStamp("2017-06-16T11:35:54.100000Z", dd)
    val tss_1 = ts1(xx.length)

    tss_1.length shouldBe 17
    dd shouldBe 0.125 // seconds

    tss_1.head shouldBe Instant.parse("2017-06-16T11:35:54.100000Z")
    tss_1(1) shouldBe Instant.parse("2017-06-16T11:35:54.225Z")
    tss_1(2) shouldBe Instant.parse("2017-06-16T11:35:54.350Z")
    tss_1.last shouldBe Instant.parse("2017-06-16T11:35:56.100Z") // not quite there

    val sig1 = Function.Sin(f1, 1, Math.toRadians(-45))
    val sig2 = Function.Sin(f2, 0.5, 0)
    val sig5 = BaseOp.Divide(sig1, sig2)
    val sig6 = sig1 / sig2

    val s1 = sig1(tss_1.length)
    val s2 = sig2(tss_1.length)
    val s5 = sig5(tss_1.length)
    val result = s1.zip(s2).map( x => x._1 / x._2)

    s5 should contain theSameElementsInOrderAs result
    sig6(tss_1.length) should contain theSameElementsInOrderAs result
  }


  "Light SMV format file loader" should "load the binary data implicitly" in {

    import better.files.Dsl._
    val trainData = cwd / ".." / "data/libsvm/a1a"
    //val testData = cwd / ".." / "data/libsvm/a1a.t"
    //val tmp = File(JSystem.getProperty("java.io.tmpdir"))

    val tmp1 = Source.libSVMSparse(ValidateDepVar.binaryClassification, SparseFold.binaryClassFail)
    val a1ar1: Either[ADWError, SparseMatrixData] = tmp1.map(trainData)
    a1ar1.isRight shouldBe true
    val a1a: SparseMatrixData = a1ar1.right.get
    a1a.size shouldBe ((1605, 120))  // 119 + dependent variable
    a1a.numLabels shouldBe 2
    a1a.classSize(0) shouldBe 0
    a1a.classSize(1) shouldBe 395
    a1a.classSize(-1) shouldBe 1210  // 1210 + 395 = 1605
    // Data order has been reversed
    val a1a_r1: (Double, a1a.SparseFeature) = a1a(0)
    a1a_r1._1 shouldBe -1.0
    // Indexes start at 0 (not 1 like the file)
    a1a_r1._2.getOrElse(0,0) shouldBe 0
    a1a_r1._2(83) shouldBe 1
    a1a_r1._2(82) shouldBe 1
    a1a_r1._2(76) shouldBe 1
    a1a_r1._2(74) shouldBe 1
    a1a_r1._2(72) shouldBe 1
    a1a_r1._2(67) shouldBe 1
    a1a_r1._2(66) shouldBe 1
    a1a_r1._2(52) shouldBe 1
    a1a_r1._2(41) shouldBe 1
    a1a_r1._2(38) shouldBe 1
    a1a_r1._2(24) shouldBe 1
    a1a_r1._2(15) shouldBe 1
    a1a_r1._2(10) shouldBe 1
    a1a_r1._2(4) shouldBe 1
    val a1a_r2: (Double, a1a.SparseFeature) = a1a(a1a.size._1-1)
    a1a_r2._1 shouldBe -1.0
    a1a_r2._2.getOrElse(0,0) shouldBe 0
    a1a_r2._2(83) shouldBe 1
    a1a_r2._2(80) shouldBe 1
    a1a_r2._2(76) shouldBe 1
    a1a_r2._2(75) shouldBe 1
    a1a_r2._2(73) shouldBe 1
    a1a_r2._2(67) shouldBe 1
    a1a_r2._2(64) shouldBe 1
    a1a_r2._2(55) shouldBe 1
    a1a_r2._2(42) shouldBe 1
    a1a_r2._2(39) shouldBe 1
    a1a_r2._2(19) shouldBe 1
    a1a_r2._2(14) shouldBe 1
    a1a_r2._2(11) shouldBe 1
    a1a_r2._2(3) shouldBe 1
  }

  it  should "load the multi-class data implicitly" in {

    import better.files.Dsl._
    val trainData = cwd / ".." / "data/libsvm/svmguide2"
    //val testData = cwd / ".." / "data/libsvm/svmguide2.t"
    //val tmp = File(JSystem.getProperty("java.io.tmpdir"))

    val tmp1 = Source.libSVMSparse(ValidateDepVar.multiClassification,SparseFold.multiClassFail)
    val a1ar1: Either[ADWError, SparseMatrixData] = tmp1.map(trainData)
    a1ar1.isRight shouldBe true
    val a1a: SparseMatrixData = a1ar1.right.get
    a1a.size shouldBe ((391, 21)) // 20 + dependent column
    a1a.numLabels shouldBe 3
    a1a.classSize(0) shouldBe 0
    a1a.classSize(-1) shouldBe 0
    a1a.classSize(1) shouldBe 221
    a1a.classSize(2) shouldBe 117
    a1a.classSize(3) shouldBe 53 // sum = 391
    // Data order has been reversed
    val a1a_r1: (Double, a1a.SparseFeature) = a1a(0)
    a1a_r1._1 shouldBe 3.0
    // Indexes start at 0 (not 1 like the file)
    a1a_r1._2.getOrElse(0,0) shouldBe 0
    a1a_r1._2(1) should be (0.0478468899521531 +- eps)
    a1a_r1._2(2) should be (0.0741626794258373 +- eps)
    a1a_r1._2(3) should be (0.0382775119617225 +- eps)
    a1a_r1._2(4) should be (0.0287081339712919 +- eps)
    a1a_r1._2(5) should be (0.0550239234449761 +- eps)
    a1a_r1._2(6) should be (0.0263157894736842 +- eps)
    a1a_r1._2(7) should be (0.0574162679425837 +- eps)
    a1a_r1._2(8) should be (0.0406698564593301 +- eps)
    a1a_r1._2(9) should be (0.110047846889952 +- eps)
    a1a_r1._2(10) should be (0.0358851674641148 +- eps)
    a1a_r1._2(11) should be (0.0741626794258373 +- eps)
    a1a_r1._2(12) should be (0.0813397129186603 +- eps)
    a1a_r1._2(13) should be (0.00956937799043062 +- eps)
    a1a_r1._2(14) should be (0.0167464114832536 +- eps)
    a1a_r1._2(15) should be (0.0311004784688995 +- eps)
    a1a_r1._2(16) should be (0.0502392344497608 +- eps)
    a1a_r1._2(17) should be (0.0311004784688995 +- eps)
    a1a_r1._2(18) should be (0.062200956937799 +- eps)
    a1a_r1._2(19) should be (0.076555023923445 +- eps)
    a1a_r1._2(20) should be (0.0526315789473684 +- eps)

    val a1a_r2: (Double, a1a.SparseFeature) = a1a(a1a.size._1-1)
    a1a_r2._1 shouldBe 1.0
    a1a_r2._2.getOrElse(0,0) shouldBe 0
    a1a_r2._2(1) should be (0.0424107142857143 +- eps)
    a1a_r2._2(2) should be (0.0915178571428571 +- eps)
    a1a_r2._2(3) should be (0.0401785714285714 +- eps)
    a1a_r2._2(4) should be (0.015625 +- eps)
    a1a_r2._2(5) should be (0.015625 +- eps)
    a1a_r2._2(6) should be (0.0223214285714286 +- eps)
    a1a_r2._2(7) should be (0.0223214285714286 +- eps)
    a1a_r2._2(8) should be (0.0825892857142857 +- eps)
    a1a_r2._2(9) should be (0.120535714285714 +- eps)
    a1a_r2._2(10) should be (0.0736607142857143 +- eps)
    a1a_r2._2(11) should be (0.0535714285714286 +- eps)
    a1a_r2._2(12) should be (0.0535714285714286 +- eps)
    a1a_r2._2(13) should be (0.0178571428571429 +- eps)
    a1a_r2._2(14) should be (0.0357142857142857 +- eps)
    a1a_r2._2(15) should be (0.111607142857143 +- eps)
    a1a_r2._2(16) should be (0.0334821428571429 +- eps)
    a1a_r2._2(17) should be (0.0223214285714286 +- eps)
    a1a_r2._2(18) should be (0.0602678571428571 +- eps)
    a1a_r2._2(19) should be (0.0200892857142857 +- eps)
    a1a_r2._2(20) should be (0.0647321428571429 +- eps)

    /*
    val pipe1: (Boolean) => Either[ADWError, Double] = Trans1[Boolean,Int]().flatMap( Trans2[Int,Double]() )
    val result1: Either[ADWError, Double] = pipe1(false)
    */
  }

  // Experimental code

  "Kantan example" should "handle alternatives of a given type" in {
    // https://stackoverflow.com/questions/37418436/generic-way-of-reading-csv-of-class-hierarchy-in-scala
    sealed trait ATT
    case class Alternative1(i: Int, b: Boolean) extends ATT
    case class Alternative2(i: Int, of: Option[Float]) extends ATT

    import kantan.csv.ops._
    import kantan.csv.generic._

    // Giving a concrete type will cause failure
    """1,true
     2,3.14
     3,""".asCsvReader[ATT](rfc.withoutHeader) // .foreach(println _)

    """1,true
     2,3.14
     3,""".asCsvReader[ATT](rfc.withoutHeader) //.foreach(println _)


    //println("Op 1 --------------------- ")
    def func1[T](implicit dt: RowDecoder[T]): CsvReader[ReadResult[T]] = {
      val rr =
        """1,true
       2,3.14
       3,""".asCsvReader[T](rfc.withoutHeader)
      //rr.foreach(println _)
      rr
    }

    val ri1 = func1[ATT]
    ri1.nonEmpty shouldBe true
    //println(ri1)


    //println("Op 2 --------------------- ")
    def func2[T](implicit dt: RowDecoder[ATT]): CsvReader[ReadResult[ATT]] = {
      val rr =
        """1,true
       2,3.14
       3,""".asCsvReader[ATT](rfc.withoutHeader)
      //rr.foreach(println _)
      rr
    }

    val ri2: CsvReader[ReadResult[ATT]] = func2
    ri2.nonEmpty shouldBe true
    //println(ri2)
  }

  it should "allow for polymorphic typing" in {
    sealed trait ATT[T]
    case class Alternative1[T](i: T, b: Boolean) extends ATT[T]
    case class Alternative2[T](i: T, of: Option[Double]) extends ATT[T]

    import kantan.csv.ops._
    import kantan.csv.generic._

    // Giving a concrete type will cause failure
    """1,true
     2,3.14
     3,""".asCsvReader[Alternative1[Int]](rfc.withoutHeader) //.foreach(println _)

    // More abstract types (same polymorphic parametrization allows alterantive matching)
    """1,true
     2,3.14
     3,""".asCsvReader[ATT[Int]](rfc.withoutHeader) //.foreach(println _)


    //println("Op 1 --------------------- ")
    def func1[T](implicit dt: RowDecoder[T]): CsvReader[ReadResult[T]] = {
      val rr =
        """1,true
       2,3.14
       3,""".asCsvReader[T](rfc.withoutHeader)
      //rr.foreach(println _)
      rr
    }

    val ri1 = func1[ATT[Double]]
    ri1.nonEmpty shouldBe true
    //println(ri1)


    //println("Op 2 --------------------- ")
    def func2[T](implicit dt: RowDecoder[ATT[T]]): CsvReader[ReadResult[ATT[T]]] = {
      val rr =
        """1,true
       2,3.14
       3,""".asCsvReader[ATT[T]](rfc.withoutHeader)
      //rr.foreach(println _)
      rr
    }

    val ri2: CsvReader[ReadResult[ATT[Double]]] = func2[Double]
    ri2.nonEmpty shouldBe true
    //println(ri2)
  }

  it should "allow for converting to column major" in {
    sealed trait ATT[+T] {
      def value : T
    }
    case class Alternative1(i: Int, b: Boolean) extends ATT[(Int,Boolean)] {
      override def value: (Int, Boolean) = (i,b)
    }
    case class Alternative2(i: Double, of: Double) extends ATT[(Double,Double)] {
      override def value: (Double, Double) = (i,of)
    }
    case class Alternative3(i: Double, of: Option[Double]) extends ATT[(Double, Option[Double])] {
      override def value: (Double, Option[Double]) = (i, of)
    }

    sealed trait FTT[T] {
      def add(e: ATT[T]) : FTT[T]
      def reverse : FTT[T]
    }

    case class Alternative1F(i: List[Int] = List[Int](), b: List[Boolean] = List[Boolean]()) extends FTT[(Int,Boolean)] {
      override def add(e: ATT[(Int, Boolean)]): FTT[(Int, Boolean)] = {
        val v = e.value
        Alternative1F( v._1 :: i, v._2 :: b)
      }
      override def reverse: FTT[(Int, Boolean)] = Alternative1F(i.reverse, b.reverse)
    }

    case class Alternative2F(i: List[Double] = List[Double](), b: List[Option[Double]] = List[Option[Double]]()) extends FTT[(Double,Option[Double])] {
      override def add(e: ATT[(Double,Option[Double])]): FTT[(Double,Option[Double])] = {
        val v = e.value
        Alternative2F( v._1 :: i, v._2 :: b)
      }
      override def reverse: FTT[(Double,Option[Double])] = Alternative2F(i.reverse, b.reverse)
    }


    import kantan.csv.ops._
    import kantan.csv.generic._


    //println("ToColumns --------------------- ")
    def toColumns[T](f: FTT[T])(implicit dt: RowDecoder[ATT[T]]): Either[List[Throwable], FTT[T]] = {
      val rr: CsvReader[ReadResult[ATT[T]]] =
        """1,true
       2,3.14
       3,""".asCsvReader[ATT[T]](rfc.withoutHeader)
      //rr.foreach(println _) NB: can only consume once

      val z = (f, List[Throwable]())
      val tmp = rr.foldLeft(z) {
        case ((acc, el), Right(e)) => (acc.add(e), el)
        case ((acc, el), Left(e)) => (acc, e :: el)
      }
      //println(tmp._1)
      //println(tmp._2.mkString(",\n"))
      if (tmp._2.nonEmpty) Left(tmp._2.reverse) else Right(tmp._1.reverse)
    }

    val rtc1 = toColumns(Alternative1F())
    //println(rtc1)
    rtc1.isLeft shouldBe true
    //println(rtc1)
    val rtc2 = toColumns(Alternative2F())
    rtc2.isLeft shouldBe true
    //println(rtc2)
  }



}
