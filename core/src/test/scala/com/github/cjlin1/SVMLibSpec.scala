/** *****************************************************************************
  * Copyright (C) 2017 INESC-TEC
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  * *****************************************************************************/
package com.github.cjlin1


import pt.inescn.models.Base.AlgorithmType.BinaryClassification
import pt.inescn.models.Base.AlgorithmState.{ModelRecorded, Predicted}
import org.scalatest._
import java.lang.{System => JSystem}

import pt.inescn.etl.Base.ScalerState.{RangeRecorded, RangeUsed, Unscaled}

/**
  * Contains tests for the SVMlib wrapper.
  * Created by hmf on 14-07-2017.
  *
  * test:compile
  * test:console
  * test:consoleQuick
  * test:run
  * test:runMain
  *
  * sbt test
  * sbt "testOnly com.github.cjlin1.SVMLibSpec"
  *
  */
class SVMLibSpec extends FlatSpec with Matchers {

  val eps = 0.3
  val eps1 = 1e-6

  /**
    * We are testing the scaler whose equivalent command lines are:
    * java -classpath libsvm.jar svm_scale -l -1 -u 1 -s /tmp/SVMScaleRange_341a7b72-40d9-420f-8a9c-f67e47b65eaa /home/hmf/git/adw/data/libsvm/a1a > a1a.scaled
    * java -cp libsvm.jar svm_scale -l -1 -u 1 -s /tmp/SVMScaleRange_341a7b72-40d9-420f-8a9c-f67e47b65eaa /home/hmf/git/adw/data/libsvm/a1a > a1a.scaled
    * java -cp libsvm.jar svm_scale -l 0 -s /tmp/SVMScaleRange_341a7b72-40d9-420f-8a9c-f67e47b65eaa /home/hmf/git/adw/data/libsvm/a1a > a1a.scaled
    * java -cp libsvm.jar svm_scale -l 0 -y -1.0 1.0 -s /tmp/SVMScaleRange_341a7b72-40d9-420f-8a9c-f67e47b65eaa /home/hmf/git/adw/data/libsvm/a1a > a1a.scaled
    * java -cp libsvm.jar svm_scale -l 0 -y -1.0 1.0 -r /tmp/SVMScaleRange_341a7b72-40d9-420f-8a9c-f67e47b65eaa /home/hmf/git/adw/data/libsvm/a1a.t > a1a.t.scaled
    *
    */
  "SVMLib wrap scaling" should "produce the scaled data in files" in  {

    import better.files._
    import better.files.Dsl._

    val trainData = cwd / ".." / "data/libsvm/a1a"
    val testData = cwd / ".." / "data/libsvm/a1a.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))

    val s0: SVM.Scale[Unscaled] = SVM.Scale()
    s0.trainArgs should contain theSameElementsInOrderAs List("-s", "", "train")
    s0.testArgs should contain theSameElementsInOrderAs List("-r", "", "train")
    val r0: Either[String, SVM.Scale[RangeRecorded]] = s0.scale(trainData)
    /*
       WARNING: original #nonzeros 22249
         new      #nonzeros 181365
       Use -l 0 if many original feature values are zeros
     */
    r0.isLeft shouldBe true

    val s1 = s0.xLowerTo(0.0).yLowerTo(-1.0).yUpperTo(1.0)
    s1.trainArgs should contain theSameElementsInOrderAs List("-l","0.0","-y","-1.0","1.0","-s","","train")
    s1.testArgs should contain theSameElementsInOrderAs List("-l","0.0","-y","-1.0","1.0","-r","","train")
    val r1: Either[String, SVM.Scale[RangeRecorded]] = s1.scale(trainData)
    r1.isRight shouldBe true
    // Remove the temporary range file because it has a unique name
    // train file because CI uses a different path
    val data = (cwd / ".." / "data/libsvm/a1a").toString
    r1.getOrElse(s0).trainArgs.patch(6,Nil,1) should contain theSameElementsInOrderAs List("-l", "0.0", "-y", "-1.0", "1.0", "-s", data)
    r1.getOrElse(s0).testArgs.patch(6,Nil,1) should contain theSameElementsInOrderAs List("-l", "0.0", "-y", "-1.0", "1.0", "-r", data)
    r1.getOrElse(s0).trainDataFile shouldBe trainData.toString
    r1.getOrElse(s0).testDataFile should not be testData.toString

    val s2 = s0.xLowerTo(0.0).yLowerTo(-1.0).yUpperTo(1.0).outDirTo(tmp)
    s2.trainArgs should contain theSameElementsInOrderAs List("-l","0.0","-y","-1.0","1.0","-s","","train")
    s2.testArgs should contain theSameElementsInOrderAs List("-l","0.0","-y","-1.0","1.0","-r","","train")
    val r2: Either[String, SVM.Scale[RangeRecorded]] = s2.scale(trainData)
    r2.isRight shouldBe true
    // Remove the temporary range file because it has a unique name
    r2.getOrElse(s0).trainArgs.patch(6,Nil,1) should contain theSameElementsInOrderAs List("-l", "0.0", "-y", "-1.0", "1.0", "-s", data)
    r2.getOrElse(s0).testArgs.patch(6,Nil,1) should contain theSameElementsInOrderAs List("-l", "0.0", "-y", "-1.0", "1.0", "-r", data)
    r2.getOrElse(s0).trainDataFile shouldBe trainData.toString
    r2.getOrElse(s0).testDataFile should not be testData.toString

    val r3: Either[String, SVM.Scale[RangeUsed]] = r2.flatMap( _.reScale(testData) )
    r3.isRight shouldBe true
    // Remove the temporary range file because it has a unique name
    r3.getOrElse(s0).trainArgs.patch(6,Nil,1) should contain theSameElementsInOrderAs List("-l", "0.0", "-y", "-1.0", "1.0", "-s", data)
    r3.getOrElse(s0).testArgs.patch(6,Nil,1) should contain theSameElementsInOrderAs List("-l", "0.0", "-y", "-1.0", "1.0", "-r", data)
    r3.getOrElse(s0).trainDataFile shouldBe trainData.toString
    r3.getOrElse(s0).testDataFile shouldBe testData.toString
    r3.getOrElse(s0).xUpper.isNaN shouldBe true
    r3.getOrElse(s0).outDir shouldBe tmp.toString
  }

  "SVMLib wrap training" should "produce the correct command lines" in  {

    val lin1 = SVM.Linear()
    lin1.args should contain theSameElementsInOrderAs List("-t", "0")

    // Poly(degree : Int = defaultI, gamma : Double = defaultD, coef0 : Double = defaultD)
    val poly1 = SVM.Poly()
    poly1.args should contain theSameElementsInOrderAs List("-t", "1")
    val poly2 = SVM.Poly(3)
    poly2.args should contain theSameElementsInOrderAs List("-t", "1", "-d", "3")
    val poly3 = SVM.Poly(3, 0.001)
    poly3.args should contain theSameElementsInOrderAs List("-t", "1", "-d", "3", "-g", "0.001")
    val poly4 = SVM.Poly(3, 0.001, coef0 = 1.5)
    poly4.args should contain theSameElementsInOrderAs List("-t", "1", "-d", "3", "-g", "0.001", "-r", "1.5")
    val poly5 = SVM.Poly(gamma=0.0001, coef0 = 1.5, degree = 5)
    poly5.args should contain theSameElementsInOrderAs List("-t", "1", "-d", "5", "-g", "1.0E-4", "-r", "1.5")

    // RBF(gamma : Double)
    val rbf1 = SVM.RBF()
    rbf1.args should contain theSameElementsInOrderAs List("-t", "2")
    val rbf2 = SVM.RBF(gamma = 0.0001)
    rbf2.args should contain theSameElementsInOrderAs List("-t", "2", "-d", "1.0E-4")

    // Sigmoid(gamma : Double = defaultD, coef0 : Double = defaultD)
    val sig1 = SVM.Sigmoid()
    sig1.args should contain theSameElementsInOrderAs List("-t", "3")
    val sig2 = SVM.Sigmoid(coef0 = 2.0)
    sig2.args should contain theSameElementsInOrderAs List("-t", "3", "-d", "2.0")

    val pre1 = SVM.Precomputed()
    pre1.args should contain theSameElementsInOrderAs List("-t", "4")

    //CSVC(c: Double = defaultD, weights : Weights, general: General = General())
    val csvc1 = SVM.CSVC()
    csvc1.args should contain theSameElementsInOrderAs List("-s", "0")
    val csvc2 = SVM.CSVC(weights = List((1,1.0), (-2,5.0)))
    csvc2.args should contain theSameElementsInOrderAs List("-s", "0", "-w1", "1.0", "-w-2", "5.0")
    val csvc3 = SVM.CSVC(weights = List((1,1.0), (-2,5.0)), c=2)
    csvc3.args should contain theSameElementsInOrderAs List("-s", "0", "-c", "2.0", "-w1", "1.0", "-w-2", "5.0")

    // NuSVC(nu: Double = defaultD, general: General = General())
    val nusvc1 = SVM.NuSVC()
    nusvc1.args should contain theSameElementsInOrderAs List("-s", "1")
    val nusvc2 = SVM.NuSVC(nu = 0.6)
    nusvc2.args should contain theSameElementsInOrderAs List("-s", "1", "-n", "0.6")

    // oneClass(nu: Double = defaultD, general: General = General())
    val one1 = SVM.oneClass()
    one1.args should contain theSameElementsInOrderAs List("-s", "2")
    val one2 = SVM.oneClass(nu = 0.6)
    one2.args should contain theSameElementsInOrderAs List("-s", "2", "-n", "0.6")

    // epsilonSVR(c: Double = defaultD, epsilon: Double = defaultD, general: General = General())
    val eps1 = SVM.epsilonSVR()
    eps1.args should contain theSameElementsInOrderAs List("-s", "3")
    val eps2 = SVM.epsilonSVR(c = 1.2)
    eps2.args should contain theSameElementsInOrderAs List("-s", "3", "-c", "1.2")
    val eps3 = SVM.epsilonSVR(c = 1.2, epsilon = 0.01)
    eps3.args should contain theSameElementsInOrderAs List("-s", "3", "-c", "1.2", "-p", "0.01")

    // NuSVR(c: Double = defaultD, nu: Double = defaultD, general: General = General())
    val nu1 = SVM.NuSVR()
    nu1.args should contain theSameElementsInOrderAs List("-s", "4")
    val nu2 = SVM.NuSVR(c = 1.2)
    nu2.args should contain theSameElementsInOrderAs List("-s", "4", "-c", "1.2")
    val nu3 = SVM.NuSVR(c = 1.2, nu = 0.01)
    nu3.args should contain theSameElementsInOrderAs List("-s", "4", "-c", "1.2", "-n", "0.01")

    // Test compilation issue when the Predef.conforms (identity)
    // If we change the signature of the `check` to not have thr additional int parameter,
    // then the Predef.conforms is called. This is a hack to get around that. To confirm
    // which function is being called, just uncomment the println within the convert function
    //import SVM.conforms
    import SVM._

    def convert[T](value : T)(implicit check : IsDefault[T]) : List[String] = {
      //println(s"convert(${check.getClass.toString})")
      if (check(value)) List() else List(value.toString)
    }
    convert(false) shouldBe List[String]()


    //General(cacheSize: Int = defaultI, epsilon: Double = defaultD, shrinking: Boolean = defaultB,
    // probabilityEstimates: Boolean = defaultB, n: Int = defaultI, q: Boolean = defaultB)
    val gen1 = SVM.TrainParameters()
    gen1.args should contain theSameElementsInOrderAs List()
    val gen2 = SVM.TrainParameters(q=true)
    gen2.args should contain theSameElementsInOrderAs List("-q", "true")
    val gen3 = SVM.TrainParameters(q = true, v = 10)
    gen3.args should contain theSameElementsInOrderAs List("-v", "10", "-q", "true")
    val gen4 = SVM.TrainParameters(q = true, v = 10, probabilityEstimates = true)
    gen4.args should contain theSameElementsInOrderAs List("-b", "true", "-v", "10", "-q", "true")
    val gen5 = SVM.TrainParameters(q = true, v = 10, probabilityEstimates = true, shrinking = true)
    gen5.args should contain theSameElementsInOrderAs List("-h", "true", "-b", "true", "-v", "10", "-q", "true")
    val gen6 = SVM.TrainParameters(q = true, v = 10, probabilityEstimates = true, shrinking = true, epsilon=0.001)
    gen6.args should contain theSameElementsInOrderAs List("-e", "0.001","-h", "true", "-b", "true", "-v", "10", "-q", "true")
    val gen7 = SVM.TrainParameters(q = true, v = 10, probabilityEstimates = true, shrinking = true, epsilon=0.001, cacheSize = 120)
    gen7.args should contain theSameElementsInOrderAs List("-m", "120", "-e", "0.001","-h", "true", "-b", "true", "-v", "10", "-q", "true")
    val gen8 = SVM.TrainParameters(q = true, v = 10, probabilityEstimates = true, shrinking = true, epsilon=0.001, cacheSize = 120,
      algorithm = SVM.NuSVR(c = 1.2))
    gen8.args should contain theSameElementsInOrderAs List("-s", "4", "-c", "1.2", "-m", "120", "-e", "0.001", "-h", "true", "-b", "true", "-v", "10", "-q", "true")
    val gen9 = SVM.TrainParameters(q = true, v = 10, probabilityEstimates = true, shrinking = true, epsilon=0.001, cacheSize = 120,
      algorithm = SVM.NuSVR(c = 1.2), kernel = SVM.RBF(gamma = 0.0001))
    gen9.args should contain theSameElementsInOrderAs List("-s", "4", "-c", "1.2", "-t", "2", "-d", "1.0E-4", "-m", "120", "-e", "0.001", "-h", "true", "-b", "true", "-v", "10", "-q", "true")

  }

  it should "produce a model when training" in {
    import better.files._
    import better.files.Dsl._
    val trainData = cwd / ".." / "data/libsvm/svmguide1"
    //val testData = cwd / ".." / "data/libsvm/svmguide1.t"
    //val modelData = cwd / ".." / "data/libsvm/svmguide1.model"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    val modelData = tmp / "svmguide1.model"

    val gen1 = SVM.TrainParameters()
    gen1.args should contain theSameElementsInOrderAs List()
    val tr1 = SVM.Train()
    val model1 = tr1.train(gen1, trainData)
    model1.isRight shouldBe true
    model1.right.get.modelDataFile shouldBe modelData.toString
    modelData.exists shouldBe true
    val model = modelData.lines.toList
    val typeModel = model.head.split(" ").map(_.trim)
    typeModel(0) shouldBe "svm_type"
    typeModel(1) shouldBe "c_svc"

  }

  it should "produce a prediction when a (default) model is available (Astroparticle Physics)" in {

    import better.files.Dsl._

    val trainData = cwd / ".." / "data/libsvm/svmguide1"
    val testData = cwd / ".." / "data/libsvm/svmguide1.t"
    //val modelData = cwd / ".." / "data/libsvm/svmguide1.model"
    //val tmp = File(JSystem.getProperty("java.io.tmpdir"))
    //val tmp = File(System.getProperty("java.io.tmpdir")) / "a1"

    val gen1 = SVM.TrainParameters()
    val pred1 = SVM.PredictParameters()
    val tr1 = SVM.Train()
    val tr2: Either[String, SVM.Train[ModelRecorded, BinaryClassification]] = tr1.train(gen1, trainData)
    // Wont' compile, Ok only use the model if not already set by training
    // tr2.flatMap( tr => tr.predict(pred1, tr.modelDataFile, testData) )
    // OK, use trained model
    val tr3: Either[String, SVM.Train[Predicted, BinaryClassification]] = tr2.flatMap(tr => tr.predict(pred1, testData ) )
    tr3.isRight shouldBe true
    val met1 = tr3.right.get.getBoolMetrics
    met1.accuracy.value shouldBe (0.66925 +- eps1)
    // Won't compile. Ok, must predict first
    //val met0 = tr2.right.get.getMetrics

    //tr1.predict(pred1, testData) // wont' compile, Ok no model available yet
    val tr4: SVM.Train[ModelRecorded, BinaryClassification] = tr2.right.get
    // compiles, Ok - we can always use a pre-existing model
    //val tr5 = tr2.flatMap( tr => tr.predict(pred1, testData ) )
    // Does not compile, Ok, we need to know what type of training was used
    //val tr5 = tr1.predict(pred1, tr4.getModel, testData )
    val tr5: Either[String, SVM.Train[Predicted, BinaryClassification]] = tr2.flatMap(tr => tr.predict(pred1, tr4.getModel, testData ) )
    tr5.isRight shouldBe true
    val met2 = tr5.right.get.getBoolMetrics
    met2.accuracy.value shouldBe (0.66925 +- eps1)
  }

  it should "produce a prediction scaling first (Astroparticle Physics)" in {

    import better.files._
    import better.files.Dsl._

    val trainData = cwd / ".." / "data/libsvm/svmguide1"
    val testData = cwd / ".." / "data/libsvm/svmguide1.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))

    val s0: SVM.Scale[Unscaled] = SVM.Scale()
    val r0: Either[String, SVM.Scale[RangeRecorded]] = s0.xLowerTo(-1).xUpperTo(1).scale(trainData)
    r0.isRight shouldBe true
    val scaledTrain = r0.right.get.scaledData()
    scaledTrain shouldBe tmp / "svmguide1.scaled"
    //println(scaledTrain)
    // Won't compile. Ok, we have to rescale before we have the rescaled test data available
    //println(r0.right.get.reScaledData)

    val r1 = r0.flatMap( _.reScale(testData) )
    r0.isRight shouldBe true
    //val scaledTest = r1.right.get.scaledData
    val scaledTest = r1.right.get.reScaledData()
    scaledTest shouldBe tmp / "svmguide1.t.scaled"
    //println(scaledTest)

    val gen1 = SVM.TrainParameters()
    val pred1 = SVM.PredictParameters()
    val tr1 = SVM.Train()
    val tr2: Either[String, SVM.Train[ModelRecorded, BinaryClassification]] = tr1.train(gen1, scaledTrain)
    val tr3: Either[String, SVM.Train[Predicted, BinaryClassification]] = tr2.flatMap(tr => tr.predict(pred1, scaledTest ) )
    //println(tr3)
    tr3.isRight shouldBe true
    val met1 = tr3.right.get.getBoolMetrics
    met1.accuracy.value shouldBe (0.9615 +- eps1)

    val r2 = tr3.map( p => p.getClassificationAccuracy)
    r2.isRight shouldBe true
    val r21: (String, Double) = r2.right.get
    r21._1 shouldBe "Accuracy"
    r21._2 shouldBe (0.9615 +- eps1)

  }

  it should "5-fold cross validation (Bioinformatics)" in {

    import better.files._
    import better.files.Dsl._

    val trainData = cwd / ".." / "data/libsvm/svmguide2"
    //val testData = cwd / ".." / "data/libsvm/svmguide2.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))

    val s0: SVM.Scale[Unscaled] = SVM.Scale()
    val r0: Either[String, SVM.Scale[RangeRecorded]] = s0.xLowerTo(-1).xUpperTo(1).scale(trainData)
    r0.isRight shouldBe true
    val scaledTrain = r0.right.get.scaledData()
    scaledTrain shouldBe tmp / "svmguide2.scaled"
    //println(scaledTrain)
    // Won't compile. Ok, we have to rescale before we have the rescaled test data available
    //println(r0.right.get.reScaledData)

    val gen1 = SVM.TrainParameters(v = 5) // 5-fold cross validation

    val tr1 = SVM.Train()
    val tr2: Either[String, SVM.Train[ModelRecorded, BinaryClassification]] = tr1.train(gen1, trainData)
    tr2.isRight shouldBe true
    //println(tr2.map( _.outDir ))
    val r2 = tr2.map( p => p.getCrossValidationAccuracy )
    r2.isRight shouldBe true
    val r21: (String, Double) = r2.right.get
    r21._1 shouldBe "Cross Validation Accuracy"
    r21._2 shouldBe (0.565217 +- eps1)

    val tr3: Either[String, SVM.Train[ModelRecorded, BinaryClassification]] = tr1.train(gen1, scaledTrain)
    tr3.isRight shouldBe true
    val r3 = tr3.map( p => p.getCrossValidationAccuracy )
    //r3.isRight shouldBe true
    //println(r3)
    val r31: (String, Double) = r3.right.get
    r31._1 shouldBe "Cross Validation Accuracy"
    r31._2 shouldBe (0.80 +- 0.05)
  }


  it should "scaled and non-scaled prediction (Vehicle)" in {

    import better.files._
    import better.files.Dsl._

    val trainData = cwd / ".." / "data/libsvm/svmguide3"
    val testData = cwd / ".." / "data/libsvm/svmguide3.t"
    val tmp = File(JSystem.getProperty("java.io.tmpdir"))

    val s0: SVM.Scale[Unscaled] = SVM.Scale()
    val r0: Either[String, SVM.Scale[RangeRecorded]] = s0.xLowerTo(-1).xUpperTo(1).scale(trainData)
    r0.isRight shouldBe true
    val scaledTrain = r0.right.get.scaledData()
    scaledTrain shouldBe tmp / "svmguide3.scaled"
    //println(scaledTrain)
    // Won't compile. Ok, we have to rescale before we have the rescaled test data available
    //println(r0.right.get.reScaledData)

    val gen1 = SVM.TrainParameters()
    val pred1 = SVM.PredictParameters()

    val tr1 = SVM.Train()
    val tr2: Either[String, SVM.Train[ModelRecorded, BinaryClassification]] = tr1.train(gen1, trainData)
    tr2.isRight shouldBe true
    val tr3: Either[String, SVM.Train[Predicted, BinaryClassification]] = tr2.flatMap(tr => tr.predict(pred1, testData ) )
    tr3.isRight shouldBe true
    val r3 = tr3.map( p => p.getClassificationAccuracy)
    r3.isRight shouldBe true
    val r31: (String, Double) = r3.right.get
    r31._1 shouldBe "Accuracy"
    r31._2 shouldBe (0.0243902 +- eps1)

    val met3 = tr3.right.get.getBoolMetrics
    met3.accuracy.value shouldBe (0.0243902 +- eps1)

    val r1 = r0.flatMap( _.reScale(testData) )
    r0.isRight shouldBe true
    val scaledTest = r1.right.get.reScaledData()
    scaledTest shouldBe tmp / "svmguide3.t.scaled"

    val tr4: Either[String, SVM.Train[ModelRecorded, BinaryClassification]] = tr1.train(gen1, scaledTrain)
    tr4.isRight shouldBe true
    val tr5: Either[String, SVM.Train[Predicted, BinaryClassification]] = tr4.flatMap(tr => tr.predict(pred1, scaledTest ) )
    val r5 = tr5.map( p => p.getClassificationAccuracy )
    r5.isRight shouldBe true
    val r51: (String, Double) = r5.right.get
    r51._1 shouldBe "Accuracy"
    r51._2 shouldBe (0.121951 +- eps1)
  }

  // TODO: add regression tests

}
