package pt.inescn.macros


object MacroCore {
  import scala.reflect.api.Position

  case class FuncTypeInfo(arity:Int, input:List[String], output:String, func:String)

  case class SourcePosition(file:String, line:Int, col:Int)

  object SourcePosition {
    // pos.source.path.toString() provides the full path but then it will be machine dependent
    def apply(pos:Position): SourcePosition = new SourcePosition(pos.source.toString(), pos.line, pos.column)
  }

  case class FuncDeclInfo( callObject:String, callPos: SourcePosition,
                           className:String, classPos: SourcePosition,
                           methodName:String, methodPos: SourcePosition,
                           typeData: String)


  case class TaskInfo(literalMatch:String, funcName: String, params:List[String])

}
