package pt.inescn.macros

import scala.annotation.tailrec


// Terms and types with quasi-quotes - but not usable here?
//import scala.meta._

/*
  History
  --------
  https://www.scala-lang.org/blog/2017/10/09/scalamacros.html
      . https://www.reddit.com/r/scala/comments/818qar/quasiquotes_are_no_longer_being_improved/

  References
  -----------
  Def macros
  ----------
  . https://blog.knoldus.com/scala-macros-an-overview/
  . https://docs.scala-lang.org/overviews/macros/overview.html
  . https://github.com/scalamacros/sbt-example/blob/master/build.sbt
  . scala.reflect.api.Trees.scala
  . https://docs.scala-lang.org/overviews/quasiquotes/intro.html
  . https://docs.scala-lang.org/overviews/quasiquotes/syntax-summary.html
  . https://users.scala-lang.org/t/scala-macro-toolbox-for-basic-scala-source-code-parsing/1620

  Liftables
  ---------
  . https://gist.github.com/Blaisorblade/3208059
  . https://groups.google.com/forum/#!topic/scala-sips/tBBPE4VBCXs

  Reify and friends
  -----------------
  . https://gist.github.com/Blaisorblade/3208059

  Scalameta
  -----------
  https://github.com/scalameta/scalameta/blob/master/notes/quasiquotes.md
  https://github.com/scalameta/scalameta/blob/master/scalameta/trees/shared/src/main/scala/scala/meta/Trees.scala

  Dynamic Type Creation
  ----------------------
  . https://stackoverflow.com/questions/22850340/dynamically-creating-case-classes-with-macros
      . https://docs.scala-lang.org/overviews/macros/typeproviders.html
      . https://github.com/travisbrown/type-provider-examples
      . https://github.com/xeno-by/type-provider-examples

  Serlialization
  --------------
  https://github.com/mpollmeier/scalameta-serialiser

  TODO: get class initializer and parameters. Use that to instantiate the class.

  Tutorials
  ---------
  . SBTB 2015: Pathikrit Bhowmick, Get Productive with Scala Macros
    https://www.youtube.com/watch?v=qEQHTGwus7Y
  . Starting with Scala Macros: a short tutorial | Blog of Adam Warski
    http://www.warski.org/blog/2012/12/starting-with-scala-macros-a-short-tutorial/
  . GitHub - underscoreio/essential-macros: Examples of simple Scala Macros
    https://github.com/underscoreio/essential-macros
  . GitHub - carrot-garden/macro_essential-macros: Examples of simple Scala Macros
    https://github.com/carrot-garden/macro_essential-macros
  . Macros for the Rest of Us - YouTube
    https://www.youtube.com/watch?v=ZVYdiAudr-I
  . Scala Macros: What Are They, How Do They Work, and Who Uses Them? - YouTube
    https://www.youtube.com/watch?v=iZjedoeOL00


  Useful examples
  -----------------
  . https://stackoverflow.com/questions/23446951/find-actual-type-of-a-method-parameter-in-macro
  . https://stackoverflow.com/questions/38146810/lifting-string-variable-using-scala-quasiquotes
  . https://gist.github.com/squito/7156004
  . https://stackoverflow.com/questions/18735295/how-to-match-a-universetype-via-quasiquotes-or-deconstructors
  . https://groups.google.com/forum/#!topic/scala-user/3YF_98W9eSE
  . http://imranrashid.com/posts/learning-scala-macros/
  . http://www.warski.org/blog/2012/12/starting-with-scala-macros-a-short-tutorial/


  Libraries
  ---------
  . https://github.com/lihaoyi/sourcecode
  . https://github.com/dwickern/scala-nameof
  . Shapeless

 */

object TaskMacro {
  //import language.experimental.macros
  //import scala.reflect.macros.blackbox
  //import scala.reflect.macros.Context
  //import scala.language.experimental.macros
  //import scala.reflect.macros.whitebox.Context

  import scala.language.experimental.macros
  import scala.reflect.macros.blackbox

  import pt.inescn.macros.MacroCore._

  def getFuncInfo[I,O](func: I => O): FuncDeclInfo = macro getFuncInfoImpl[I,O]
  //def getFuncInfo[I,O](func: I => O): I => O = macro getFuncInfoImpl[I,O]
  //def getFuncInfo[I,O](func: I => O): String = macro getFuncInfoImpl[I,O]

  def printInfo[T](expr: T): Unit = macro printInfoImpl[T]


  /**
    * Prints out black-box content. Used for development and debugging.
    *
    * @param c black-box context
    * @param expr print information on this expression
    * @tparam T generic type of expression
    */
  def printInfoImpl[T:c.WeakTypeTag](c: blackbox.Context)(expr: c.Expr[T]): c.Expr[Unit]= {
    import c.universe._
    printInfo(c)(expr)
    reify(())
  }

  private def printInfo[T:c.WeakTypeTag](c: blackbox.Context)(expr: c.Expr[T]): Unit = {
    import c.universe._

    println(s"\n\nfunc.tree = ${expr.tree}")
    println(s"func.tree.type = ${expr.tree.tpe}")
    println(s"func.tree.class = ${expr.tree.getClass}")
    println(s"func.actualType = ${expr.actualType}")
    println(s"func.staticType = ${expr.staticType}")
    println(s"func.tree.isDef = ${expr.tree.isDef}")
    println(s"func.tree.isTerm = ${expr.tree.isTerm}")
    println(s"func.tree.isType = ${expr.tree.isType}")
    println(s"Raw: ${showRaw(expr.tree)}")
  }

  /**
    * Use as a starter function for development.
    *
    * @param c black-box context
    * @param expr print information on this expression
    * @tparam T generic type of expression
    */
  def testMatch[T](c: blackbox.Context)(expr: c.Expr[T]): Unit = {
    import c.universe._

    // Use scala.reflect.api.Trees.scala
    // The order of matching seems to be important,
    // For example both Ident(_) and Function() will
    // also match a Block(_)
    expr.tree match {
      case Ident(name) =>
        println(s"Ident($name)")
      case Function(vparams: List[ValDef], body: Tree) =>
        println(s"Function(${vparams.mkString(",")} => $body)")
      case q"{ ..$stats }" =>
        println(s"Block*($stats)")
      /*
    case Block(stats) =>
      println(s"Block($stats)")
      */
      case _ =>
        println(s"Unknown(${show(expr.tree)})")
    }
    // c.abort(c.enclosingPosition, "macro has reported an error")

    /*val cs = func.tree.children
    println(cs.mkString(";\n"))*/
  }


  /**
    * Use as a starter function for development.
    *
    * @param c black-box context
    * @param expr print information on this expression
    * @tparam T generic type of expression
    */
  def getFuncName[T:c.WeakTypeTag](c: blackbox.Context)(expr: c.Expr[T]): String = {
    import c.universe._

    expr.tree match {
      case q"{ ($paramIn) => $call($param)}" =>
        // passed in a named function
        println(s"$call($paramIn) => ???")
        call.toString
      case q"{ ($paramIn) => $body}" =>
        // passed in a named function
        println(s"($paramIn) => body")
        s"anon:${c.enclosingPosition}"
      case q"$obj.$name" =>
        // passed in a named value of a function
        println(s"Ident($obj~>$name)")
        obj.toString + "." + name.toString
      case q"$name" =>
        // passed in a named value of a function
        println(s"Ident($name)")
        name.toString
      /* Not reachable
      case q"{ ..$stats }" =>
        println(s"Block*($stats)")
        ""
      case _ =>
        c.abort(c.enclosingPosition, "TaskMacro error - getFuncName: ${show(expr.tree)}")
        */
    }
    /*val cs = func.tree.children
    println(cs.mkString(";\n"))*/
  }

  /**
    * Example of how one must "lift" strings to be used as macros
    *
    * @param c black-box context
    * @param info compile time informaion on function call
    */
  def makeDef(c: blackbox.Context)(info: FuncTypeInfo): c.Tree = {
    import c.universe._

    val funcS = info.func
    val paramsTypesS = info.input ++ List(info.output)
    val funcDef = s"$funcS[${paramsTypesS.mkString(",")}]"
    // Must lift string for quasi-quotes (otherwise marked as string type)
    val parsed = c.parse(funcDef)
    val funcSignature = tq"$parsed"
    //println(showRaw(funcSignature))
    funcSignature
  }

  @tailrec
  def allOwners(c: blackbox.Context)(acc: Seq[c.Symbol], s: c.Symbol): Seq[c.Symbol] = {
    import c.universe._

    if (s == `NoSymbol`) {
      acc
    } else {
      allOwners(c)(s +: acc, s.owner)
    }
  }




  /**
    *
    * NOTE: we cannot match of the type (for example tq"(..$param) => $body")
    * on an expression. We can only match quaisquotes using expressions.
    *
    * @param c black-box context
    * @param expr compiled expression of type `T`
    * @tparam T generic type that is being compiled
    * @return information of the function call
    */
  def getFuncDef[T:c.WeakTypeTag](c: blackbox.Context)(expr: c.Expr[T]): FuncTypeInfo = {
    import c.universe._

    //println(showRaw(expr.tree.tpe))
    //println(showRaw(expr.actualType))
    expr.tree.tpe match {
      case TypeRef(_,func1,params) =>
        val func = func1.asType
        val input: List[Symbol] = params.dropRight(1).map(_.typeSymbol)
        val output: Symbol = params.last.typeSymbol
        val funcS = func.fullName
        val inputS = input.map(_.fullName)
        val outputS = output.fullName
        FuncTypeInfo(input.size, inputS, outputS, funcS)
      case _ =>
        c.abort(c.enclosingPosition, s"TaskMacro error - getFuncDef: ${show(expr.tree)}")
    }
  }

  private def getFuncDecl[T:c.WeakTypeTag](c: blackbox.Context)(expr: c.Expr[T]): FuncDeclInfo = {
    import c.universe._

    val typeInfo = expr.actualType.toString
    expr.tree match {
      case q"{ ($paramIn) => $call($_)}" =>
        // passed in a named function
        call match {
          case Select(obj, funcName) =>
            val callName = obj.toString
            val callPos = SourcePosition(obj.pos)

            // Get type declaration of the object
            obj.symbol.info match {
              case ClassInfoType(_, scope, objUsed) =>
                // We have a class with methods
                val className = objUsed.fullName
                val classPos = SourcePosition(objUsed.pos)
                // Get the method we matched earlier
                val tmp = scope.filter{ e => e.isMethod && (e.name == funcName) }
                // May not be the correct one f we have methods with the same name
                val func = tmp.toSeq.head
                val methodName = func.fullName
                val methodPos = SourcePosition(func.pos)
                val info = FuncDeclInfo(callName, callPos, className, classPos, methodName, methodPos, typeInfo)
                //println(info)
                info

              case TypeRef(_,decl,_) =>
                // We have an object with methods
                val ClassInfoType(_,scope,_) = decl.info
                val className = decl.fullName
                val classPos = SourcePosition(decl.pos)
                // Get the method we matched earlier
                val tmp = scope.filter{ e => e.isMethod && (e.name == funcName) }
                // May not be the correct one f we have methods with the same name
                val func = tmp.toSeq.head
                val methodName = func.fullName
                val methodPos = SourcePosition(func.pos)
                val info = FuncDeclInfo(callName, callPos, className, classPos, methodName, methodPos, typeInfo)
                //println(info)
                info
            }

          case Ident(term) =>
            // passed in a named function
            // Same as q"$name"
            val callName = call.toString
            val callPos = SourcePosition(call.pos)

            // No call ing class/object so use full name
            val methodName = call.symbol.fullName
            val methodPos = SourcePosition(call.symbol.pos)

            // No calling class/object so use full name
            val funcOwner = call.symbol.owner
            val className = funcOwner.fullName
            val classPos = SourcePosition(funcOwner.pos)

            val info = FuncDeclInfo(callName, callPos, className, classPos, methodName, methodPos, typeInfo)
            //println(s"IdentTermOnly($term)")
            //println(info)
            info

          case _ =>
            c.abort(c.enclosingPosition, s"TaskMacro error - getFuncDecl: ${show(expr.tree)}")
        }

      case q"{ ($paramIn) => $body}" =>
        // passed in an anonymous function
        // q"$name"
        val call = expr.tree

        val callName = call.toString
        val callPos = SourcePosition(call.pos)

        // No call ing class/object so use full name
        val methodName = call.symbol.fullName
        val methodPos = SourcePosition(call.symbol.pos)

        // No calling class/object so use full name
        // https://contributors.scala-lang.org/t/enclosing-class-whats-available-from-the-universe/1220/8
        // https://github.com/nestorpersist/macro-examples/blob/master/macros/src/main/scala/com/persist/macros/BlackBox.scala
        // Get owners (variables, classes, objects, packages ...
        val owners = allOwners(c)(List(), call.symbol)
        // Get the class
        val classOwners = owners.filter( e => (e.isClass || e.isModule) && !e.isPackage )
        // Get the class nearest to the method definition
        val owner = classOwners.reverse.head
        val className = owner.fullName
        val classPos = SourcePosition(owner.pos)

        val info = FuncDeclInfo(callName, callPos, className, classPos, methodName, methodPos, typeInfo)
        //println(s"($paramIn) => $body")
        //s"anon:${c.enclosingPosition}"
        //println(info)
        info


      case q"$obj.$name" =>
        // passed in a named value of a function
        // Get type declaration of the object
        val TypeRef(_,decl,_) = obj.symbol.info
        // We assume it is a class/object with methods
        val ClassInfoType(_,scope,_) = decl.info
        // Get the method we matched earlier
        val tmp = scope.filter{ e => e.isMethod && (e.name == name) }
        // May not be the correct one f we have methods with the same name
        val func = tmp.toSeq.head

        val callName = obj.toString
        val callPos = SourcePosition(obj.pos)
        val className = decl.fullName
        val classPos = SourcePosition(decl.pos)
        val methodName = func.fullName
        val methodPos = SourcePosition(func.pos)
        val info = FuncDeclInfo(callName, callPos, className, classPos, methodName, methodPos, typeInfo)
        //println(s"Object.val($obj~>$name)")
        //println(info)
        info

      case q"$name" =>
        // passed in a named value of a function
        val callName = name.toString
        val callPos = SourcePosition(name.pos)

        // No calling class/object so use full name
        val methodName = name.symbol.fullName
        val methodPos = SourcePosition(name.symbol.pos)

        // Look for some enclosing context (function, value, object, ...)
        // Get owners (variables, classes, objects, packages ...
        val owners = allOwners(c)(List(), name.symbol)
        // Get the class
        val classOwners = owners.filter( e => (e.isClass || e.isModule) && !e.isPackage )
        // Get the class nearest to the method definition
        val owner = classOwners.reverse.head
        val className = owner.fullName
        val classPos = SourcePosition(owner.pos)

        val info = FuncDeclInfo(callName, callPos, className, classPos, methodName, methodPos, typeInfo)
        //println(s"IdentOnly($name)")
        //println(info)
        info

      /* unreachable
      case q"{ ..$stats }" =>
        println(s"Block*($stats)")
      case _ =>
        c.abort(c.enclosingPosition, "TaskMacro error - getFuncName: ${show(expr.tree)}")
        */
    }

  }


  /**
    * Helper function that creates a [[SourcePosition]]
    *
    * @param c black-box context
    * @param pos position of source code compilation
    * @return a [[SourcePosition]] that records the source code file,
    *         line and column of the compilation position
    */
  def sourcePositionAST(c: blackbox.Context)(pos:SourcePosition): c.universe.Tree = {
    import c.universe._
    q"SourcePosition(${pos.file}, ${pos.line}, ${pos.col})"
  }

  /**
    * Helper function that creates a [[FuncDeclInfo]] that holds
    * information on a function declaration. This is convoluted
    * methods is required because the case class is not liftable.
    *
    * @param c black-box context
    * @param info [[FuncDeclInfo]] tha was generated by scanning the
    *            parse tree.
    * @return
    */
  def FuncDeclInfoAST(c: blackbox.Context)(info:FuncDeclInfo): c.Expr[FuncDeclInfo] = {
    import c.universe._

    val callObj = q" ${info.callObject} "
    val callPos = sourcePositionAST(c)(info.callPos) // reify(NoPosition) //q" ${info.callPos} "  // reify( info.callPos )
    val callClss = q" ${info.className} "
    val clssPos = sourcePositionAST(c)(info.classPos)
    val callMethod = q" ${info.methodName} "
    val methodPos = sourcePositionAST(c)(info.methodPos)
    val callType = info.typeData

    val out = q""" FuncDeclInfo($callObj, $callPos, $callClss, $clssPos, $callMethod, $methodPos, $callType) """
    c.Expr[FuncDeclInfo](out)
  }


  def getFuncInfoImpl[I:c.WeakTypeTag, O:c.WeakTypeTag](c: blackbox.Context)(func: c.Expr[I => O]): c.Expr[FuncDeclInfo] = {
    //import c.universe._

    // For debugging purposes
    /*
    printInfo(c)(func)
    testMatch(c)(func)
    val funcId = getFuncName(c)(func)
    println(s"funcId = $funcId")
    val funcInfo = getFuncDef(c)(func)
    println(s"funcInfo = $funcInfo")
    val funcType = makeDef(c)(funcInfo)
    println(s"funcType = $funcType")
    */

    // Using liftables
    val info = getFuncDecl(c)(func)
    val out = FuncDeclInfoAST(c)(info)
    out

    // Notes:
    //

    // Ok
    //val out = Literal(Constant(info.toString))
    //c.Expr[String](out)

    // Using explicit typing (cannot lift?). No problem with free variable

    // Using a string of code
    // Not liftable
    // Can't unquote pt.inescn.macros.TaskMacro.FuncDeclInfo, consider providing an implicit instance of Liftable[pt.inescn.macros.TaskMacro.FuncDeclInfo]
    //val out = q""" $info """

    // Fails, free term problem, liftable issue?
    //val out = reify( info )
    //val out = reify( FuncDeclInfo("", NoPosition, "", NoPosition, "", NoPosition, "" ) )
    //out

    // free term problem, variable c of context?
    /*val out = reify {
      val tmp = FuncDeclInfo("", NoPosition, "", NoPosition, "", NoPosition, "")
      tmp
    }
    out*/

    // Fails
    //val out = Literal(Constant(info))
    //c.Expr[FuncDeclInfo](out)

    // Ok, but won't work because types don't print out in coding format
    //val code = s""" ${info.toString} """
    // Works partially, but same problem as with "import scala.reflect.internal.util.Position"
    /*val code =
      s""" FuncDeclInfo("${info.callObject}", NoPosition,
         |"${info.className}", NoPosition,
         |"${info.methodName}", NoPosition,
         |"${info.typeData}" ) """
    //println(code)
    val parsed = c.parse(code.stripMargin)
    // try returning parsed directly (c.Tree)
    c.Expr[FuncDeclInfo](parsed)*/

    // Ok
    //val parsed = c.parse(""" FuncDeclInfo("", NoPosition, "", NoPosition, "", NoPosition, "" ) """)
    // try returning parsed directly (c.Tree)
    //c.Expr[FuncDeclInfo](parsed)

    // Experiments returning c.Expr[String]
    //def getFuncInfoImpl[I:c.WeakTypeTag, O:c.WeakTypeTag](c: blackbox.Context)
    // (func: c.Expr[I => O]): c.Expr[String] = {
    // ...
    // }

    // Ok, using splice. Note that string in`t` is a liftable
    //val t = reify( "Boooo4" )
    //val out = reify(t.splice).tree
    //c.Expr[String](out)

    // Ok: return c.Expr[String], manual construction
    //val s = "Boooo3"
    //val out = Literal(Constant(s))
    //c.Expr[String](out)

    // Ok, liftable so can also use quasiquotes. No need for splicing to deal with free variable?
    //val s = "Boooo2"
    //val out = q""" $s """
    //c.Expr[String](out)

    // Ok, generates literal with quasiquotes
    //val out = q""" "Boooo1" """
    //c.Expr[String](out)

    // Fail: Why not liftable? Detected as a free term, but no splice is available
    //val s = "Boooo0"
    //val out = reify(s)
    //out

    // Using liftables
    // Ok: return c.Expr[String]
    //val out = reify("Booooo")
    //out

    // Ok: c.Tree
    //val parsed = c.parse(info.toString)
    //q"$parsed"
  }


  // Returns the tree of `a` after the type, printed as source code.
  def desugar(a: Any): String = macro desugarImpl

  def desugarImpl(c: blackbox.Context)(a: c.Expr[Any]): c.Expr[Nothing] = {
    import c.universe._

    val s = show(a.tree)
    c.Expr(
      Literal(Constant(s))
    )
  }


  // https://stackoverflow.com/questions/5050682/get-scala-variable-name-at-runtime
  // https://github.com/dwickern/scala-nameof
  // https://stackoverflow.com/questions/38548167/preserve-method-parameter-names-in-scala-macro
  // http://onoffswitch.net/extracting-scala-method-names-objects-macros/
  // . https://github.com/wolfe-pack/wolfe/wiki/Scala-AST-reference
  // https://blog.scalac.io/def-hello-macro-world.html

  def extract[T:c.WeakTypeTag](c: blackbox.Context)(t:c.Tree): TaskInfo = {
    import c.universe._

    t match {
      case Function(params, Select(qualifier, name)) =>
        // Anonymous function via val
        val paramsNames = params.map {
          case ValDef(Modifiers(_,_,_), TermName(parami), TypeTree(), EmptyTree) =>
            parami
          case _ =>
            "?"
        }
        TaskInfo(literalMatch = "Anonymous Val Function", funcName = Select(qualifier, name).toString, params = paramsNames)

      case Function(params, name@Apply(_,_)) =>
        // Anonymous function via def
        val paramsNames = params.map {
          case ValDef(Modifiers(_,_,_), TermName(parami), TypeTree(), EmptyTree) =>
            parami
          case _ =>
            "?"
        }
        TaskInfo(literalMatch = "Anonymous Def Function", funcName = name.toString, params = paramsNames)

      case Function(params, name@Block(_,_)) =>
        // Anonymous function via def
        val paramsNames = params.map {
          case ValDef(Modifiers(_,_,_), TermName(parami), TypeTree(), EmptyTree) =>
            parami
          case _ =>
            "?"
        }
        TaskInfo(literalMatch = "Block(Anonymous Def Function)", funcName = name.toString, params = paramsNames)

      case Block(_, Function(params, Apply(func, _))) =>
        // Standard function
        val paramsNames = params.map {
          case ValDef(Modifiers(_,_,_), TermName(parami), TypeTree(), EmptyTree) =>
            parami
          case _ =>
            "?"
        }
        TaskInfo(literalMatch = "Block(Function)", funcName = func.toString(), params = paramsNames)

      case Ident(term) =>
        // Cannot get function definition from val. Just get FunctionX
        /*
        println("named Ident")
        println(showRaw(term))
        println(showRaw(t.tpe))
        println(showRaw(t.symbol))
        println(s"method ${showRaw(t.symbol.info)}")
        println(t.tpe)
        println(t.tpe.dealias.members.foreach( println(_)))
        val t0 = t.tpe.dealias.members.filter(_.isMethod)
        println(showRaw(t0))
        val ttmp = t.tpe.dealias.members.filter{ t =>
          println(s"t = $t : ${showRaw(t)}")
          //println(s"t method = $t : ${showRaw(t.asMethod)}")
          //val xx = t == TermName("apply")
          println(showRaw(t.info))
          println(showRaw(t.asMethod))
          val xz = t.asMethod match { case Function(tt,_) => println(tt); case _ => println("Nothing")}
          val xx = t.asMethod match { case TermName("apply") => true ; case _ => false }
          println(s"xx = $xx")
          xx
        }
        println(showRaw(ttmp.head))
        t.tpe match {
          case TypeRef(_,decl,_) =>
            println(decl.info)
            ???
          case _ => ???
        }
        //println(showRaw(t.tpe.paramLists))
        */
        TaskInfo(literalMatch = "Ident(term)", funcName = term.toString, params = List())

      case _ =>
        c.abort(c.enclosingPosition, s"TaskMacro error - extract: ${showRaw(t)}")

    }

  }


  def names[T](a: T): TaskInfo = macro namesImpl[T]

  def namesImpl[T:c.WeakTypeTag](c: blackbox.Context)(a: c.Expr[T]): c.Expr[TaskInfo] = {
    import c.universe._

    val tmp = extract(c)(a.tree)
    val out = q""" TaskInfo(${tmp.literalMatch}, ${tmp.funcName}, ${tmp.params}) """
    c.Expr[TaskInfo](out)
  }

  import scala.language.implicitConversions
  implicit def nameFunc[T](a: T): (TaskInfo, T) = macro namedFuncImpl[T]

  // https://docs.scala-lang.org/overviews/macros/implicits.html
  def namedFuncImpl[T:c.WeakTypeTag](c: blackbox.Context)(a: c.Expr[T]): c.Expr[(TaskInfo,T)] = {
    import c.universe._

    val tmp = extract(c)(a.tree)
    val out = q""" (TaskInfo(${tmp.literalMatch}, ${tmp.funcName}, ${tmp.params}), $a )"""
    c.Expr[(TaskInfo,T)](out)
  }

}
