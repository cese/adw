package pt.inescn.macros


class FuncBlob() {
  def f(b:Int):Int = -b
  val h: Int => Double = (b:Int) => (2*b).toDouble
}

/**
  *  macroSub/test:run
  *  ~;clean;macroSub/test:run
  *
  *  @see https://www.scala-sbt.org/1.0/docs/Macro-Projects.html
  *       https://docs.scala-lang.org/overviews/macros/overview.html
  */
object Usage {

  // Named method of object
  def g(x:Double): Double = x*x

  import pt.inescn.macros.MacroCore.TaskInfo
  def usesFunc[I,O](f: (TaskInfo, I => O), i:I): O = {
    println(f._1)
    f._2(i)
  }

  def main(args: Array[String]): Unit = {

    // Named function
    def f(a:Int):Int = -a
    // Named function value
    val h: Int => Double = (a:Int) => (2*a).toDouble

    //def i(a:Int, b:Double):Double = a*b

    val blob = new FuncBlob()

    val r = TaskMacro.desugar(f _)
    println(s"r = $r")

    import pt.inescn.macros.MacroCore.FuncDeclInfo
    import pt.inescn.macros.MacroCore.SourcePosition

    //val test = FuncDeclInfo("", NoPosition, "", NoPosition, "", NoPosition, "" )
    //TaskMacro.printInfo(test)
    //TaskMacro.printInfo( FuncDeclInfo("", NoPosition, "", NoPosition, "", NoPosition, "" ) )

    val r0 = TaskMacro.getFuncInfo((i:Double) => i.toInt)
    println(s"r0 = $r0")
    val r1 = TaskMacro.getFuncInfo(f)
    println(s"r1 = $r1")
    val r2 = TaskMacro.getFuncInfo(h)
    println(s"r2 = $r2")
    val r3 = TaskMacro.getFuncInfo(g)
    println(s"r3 = $r3")
    val r4 = TaskMacro.getFuncInfo(blob.f)
    println(s"r4 = $r4")
    val r5 = TaskMacro.getFuncInfo(blob.h)
    println(s"r5 = $r5")

    println("????????????????")
    import pt.inescn.macros.MacroCore.TaskInfo
    //TaskMacro.printInfo(f _)
    /*println(TaskMacro.names(f _))
    println(TaskMacro.names(i _))
    println(TaskMacro.names(g _))
    println(TaskMacro.names(blob.f _))*/
    //println(TaskMacro.names((i:Double, j:Int) => i * j ))
    //println(TaskMacro.names(blob.h _))
    //println(TaskMacro.names(h))

    val r6 = usesFunc((TaskMacro.names(f _),f _), 1)
    println(s"r6 = $r6")

    import pt.inescn.macros.TaskMacro._
    val r7 = usesFunc(f _, 1)
    println(s"r7 = $r7")

  }
}