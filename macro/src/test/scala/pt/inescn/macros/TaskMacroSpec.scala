package pt.inescn.macros

import org.scalatest._

/**
  * macroSub/test:compile
  * macroSub/test:console
  * macroSub/test:consoleQuick
  * macroSub/test:run
  * macroSub/test:runMain
  *
  * sbt macroSub/test
  * sbt "macroSub/testOnly pt.inescn.macros.TaskMacroSpec"
  */
class TaskMacroSpec extends FlatSpec with Matchers with Inside {

  // Named method of object
  def g(x:Double): Double = x*x

  // A function that delegates to a named function
  import pt.inescn.macros.MacroCore.TaskInfo
  def usesFunc[I,O](f: (TaskInfo, I => O), i:I): (TaskInfo, O) = {
    (f._1, f._2(i))
  }

  "Position macros" should "get function positions" in {

    // Functions defined locally

    // Named function
    def f(a:Int):Int = -a

    // Named function value
    val h: Int => Double = (a:Int) => (2*a).toDouble

    // Function with multiple parameters
    //def i(a:Int, b:Double):Double = a*b

    // Functions from methods
    val blob = new FuncBlob()

    val r = TaskMacro.desugar(f _)
    r shouldBe "{\n  ((a: Int) => f(a))\n}"


    import pt.inescn.macros.MacroCore.FuncDeclInfo
    import pt.inescn.macros.MacroCore.SourcePosition

    //val test = FuncDeclInfo("", NoPosition, "", NoPosition, "", NoPosition, "" )
    //TaskMacro.printInfo(test)
    //TaskMacro.printInfo( FuncDeclInfo("", NoPosition, "", NoPosition, "", NoPosition, "" ) )


    val r0 = TaskMacro.getFuncInfo(
      (i:Double) => i.toInt
    )
    //println(s"r0 = $r0")
    r0 shouldBe FuncDeclInfo("((i: Double) => i.toInt)",SourcePosition("TaskMacroSpec.scala",55,18),"pt.inescn.macros.TaskMacroSpec",SourcePosition("TaskMacroSpec.scala",15,7),"pt.inescn.macros.TaskMacroSpec.$anonfun",SourcePosition("TaskMacroSpec.scala",55,18),"Double => Int")

    val r1 = TaskMacro.getFuncInfo(f)
    //println(s"r1 = $r1")
    r1 shouldBe  FuncDeclInfo("f",SourcePosition("TaskMacroSpec.scala",60,36),"pt.inescn.macros.TaskMacroSpec.<local TaskMacroSpec>",SourcePosition("TaskMacroSpec.scala",15,21),"pt.inescn.macros.TaskMacroSpec.f",SourcePosition("TaskMacroSpec.scala",31,9),"Int => Int")

    val r2 = TaskMacro.getFuncInfo(h)
    //println(s"r2 = $r2")
    r2 shouldBe FuncDeclInfo("h",SourcePosition("TaskMacroSpec.scala",64,36),"pt.inescn.macros.TaskMacroSpec",SourcePosition("TaskMacroSpec.scala",15,7),"pt.inescn.macros.TaskMacroSpec.h",SourcePosition("TaskMacroSpec.scala",34,9),"Int => Double")

    val r3 = TaskMacro.getFuncInfo(g)
    //println(s"r3 = $r3")
    r3 shouldBe FuncDeclInfo("TaskMacroSpec.this",SourcePosition("TaskMacroSpec.scala",68,36),"pt.inescn.macros.TaskMacroSpec",SourcePosition("TaskMacroSpec.scala",15,7),"pt.inescn.macros.TaskMacroSpec.g",SourcePosition("TaskMacroSpec.scala",18,7),"Double => Double")

    // No access to the source code position
    val r4 = TaskMacro.getFuncInfo(blob.f)
    //println(s"r4 = $r4")
    r4 shouldBe FuncDeclInfo("blob",SourcePosition("TaskMacroSpec.scala",73,36),"pt.inescn.macros.FuncBlob",SourcePosition("Usage.scala",4,7),"pt.inescn.macros.FuncBlob.f",SourcePosition("Usage.scala",5,7),"Int => Int")

    val r5 = TaskMacro.getFuncInfo(blob.h)
    //println(s"r5 = $r5")
    r5 shouldBe FuncDeclInfo("blob",SourcePosition("TaskMacroSpec.scala",77,36),"pt.inescn.macros.FuncBlob",SourcePosition("Usage.scala",4,7),"pt.inescn.macros.FuncBlob.h",SourcePosition("Usage.scala",6,7),"Int => Double")

    // TODO: does not compile. Use generic expression?
    /*
    val r6 = TaskMacro.getFuncInfo(i)
    //println(s"r6 = $r6")
    r6 shouldBe FuncDeclInfo("blob",SourcePosition("TaskMacroSpec.scala",77,36),"pt.inescn.macros.FuncBlob",SourcePosition("<no source file>",0,0),"pt.inescn.macros.FuncBlob.h",SourcePosition("<no source file>",0,0),"Int => Double")
    */
  }

  "Name macros" should "get function names and parameters" in {

    // Functions defined locally

    // Named function
    def f(a:Int):Int = -a

    // Named function value
    val h: Int => Double = (a:Int) => (2*a).toDouble

    // Function with multiple parameters
    def i(a:Int, b:Double):Double = a*b

    // Functions from methods
    val blob = new FuncBlob()

    import pt.inescn.macros.MacroCore.TaskInfo
    //TaskMacro.printInfo(f _)

    val i0 = TaskMacro.names(f _)
    //println(i0)
    i0 shouldBe TaskInfo("Block(Function)","f",List("a"))

    val i1 = TaskMacro.names(i _)
    //println(i1)
    i1 shouldBe TaskInfo("Block(Function)","i",List("a", "b"))

    val i2 = TaskMacro.names(g _)
    //println(i2)
    i2 shouldBe TaskInfo("Block(Function)","TaskMacroSpec.this.g",List("x"))

    val i3 = TaskMacro.names(blob.f _)
    //println(i3)
    i3 shouldBe TaskInfo("Block(Function)","blob.f",List("b"))

    // Does not work properly - no function name available
    val i4 = TaskMacro.names(
      (i:Double, j:Int) => i * j
    )
    //println(i4)
    i4 shouldBe TaskInfo("Anonymous Def Function","i.*(j)",List("i", "j"))

    // Does not work properly - no function name available and no parameter information
    val i5 = TaskMacro.names(blob.h _)
    //println(i5)
    i5 shouldBe TaskInfo("Anonymous Val Function","blob.h",List())

    val i6 = TaskMacro.names(h)
    //println(i6)
    i6 shouldBe TaskInfo("Ident(term)","h",List())

    val i7 = TaskMacro.names(i _)
    //println(i7)
    i7 shouldBe TaskInfo("Block(Function)","i",List("a", "b"))
  }

  it should "generate function information implicitly" in {
    // Functions defined locally

    // Named function
    def f(a:Int):Int = -a

    import pt.inescn.macros.TaskMacro._
    val r0 = usesFunc(f _, 1)
    //println(s"r0 = $r0")
    r0 shouldBe ((TaskInfo("Block(Function)","f",List("a")),-1))
  }

}
