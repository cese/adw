Anomaly Detection WorkBench
===========================

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)](https://en.wikipedia.org/wiki/MIT_License)
[![build status](https://gitlab.com/cese/adw/badges/master/build.svg)](https://gitlab.com/cese/adw/commits/master)
[![coverage report](https://gitlab.com/cese/adw/badges/master/coverage.svg)](https://gitlab.com/cese/adw/commits/master)

[![Scaladoc](http://javadoc-badge.appspot.com/com.github.cese.adw/adw_2.12.svg?label=scaladoc)](https://cese.gitlab.io/adw/api/)

![alt text](docs/src/main/resources/images/icon_color_small.png)

Documentation
=============

The projects documentation can be found [here]( https://cese.gitlab.io/adw/)

Acknowledgements
================

1. Predictive Maintenance
   EU Research Project
   MANTIS: Cyber Physical System based Proactive Collaborative Maintenance.
   2015-05-01 to 2018-07-31
   Funded by: ECSEL-01-2014 - ECSEL Key Applications and Essential Technologies (RIA); Grant agreement ID: 662189
   http://www.mantis-project.eu/
   https://cordis.europa.eu/project/rcn/198079/factsheet/en
1. Vibration analysis
   National Research Project   
   ADIRA INDUSTRY 4.0 
   2016-09-01 to 2019-08-31 
   Funded by: Fundo Europeu de Desenvolvimento Regional (FEDER) and by the Programa Operacional Competitividade e 
   Internacionalizao POCI-01-0247-FEDER-017922
   http://www.poci-compete2020.pt/noticias/detalhe/Proj17922_Adira
1. Predictive Maintenance 
   National Research Project
   PRODUTECH SIF
   2017-10-01 to 2020-09-30 
   Support from: POCI-01-0247-FEDER-024541
1. Robotics Anomaly Detection
   International Research Project (EU and Brazil)
   FASTEN
   2017-11-01 to 2020-10-31 
   Funded by: H2020-EU.2.1.1. - INDUSTRIAL LEADERSHIP - Leadership in enabling and industrial technologies - 
   Information and Communication Technologies (ICT); EUB-02-2017 - IoT Pilots ; Grant agreement ID: 777096
   https://cordis.europa.eu/project/rcn/212223/factsheet/en
   


License
=======
MIT License