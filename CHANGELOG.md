# ${project-name} Change Log

## Version 0.2.1

Release changes:

* Added documentation site using Abt Micro-site
* Set-up initial documentation structure
* Added initial documentation on `Task` and `Pipe`
* Documented installation of mermaidJS and KaTex JavaScripts and how to use them


