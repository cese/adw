---
layout: docs
title:  "Installation"
---

# Installation

Currently we do not have the platform available in a Maven repository. However it is possible to deploy the project 
using a **fat Jar**. To do this we use the 
[SBT assembly plugin]( https://github.com/sbt/sbt-assembly){:target="_blank"}. In order to generate a Jar at the SBT 
prompt execute the `assembly` command. This will generate, among others, a `core.jar` in the Scala's target directory 
(the file path is shown during assembly execution). We can also set verbose output on with the `debug` command. Here is 
an example when all Jar files are up to date:

```sbtshell
sbt:adw> assembly
[info] Strategy 'discard' was applied to 5 files (Run the task at debug level to see details)
[info] Assembly up to date: /home/user/IdeaProjects/adw/target/scala-2.12/adw-assembly-0.1.0-SNAPSHOT.jar
[warn] Multiple main classes detected.  Run 'show discoveredMainClasses' to see the list
[info] Strategy 'discard' was applied to 7 files (Run the task at debug level to see details)
[info] Assembly up to date: /home/user/IdeaProjects/adw/macro/target/scala-2.12/macros.jar
[info] Strategy 'discard' was applied to 151 files (Run the task at debug level to see details)
[info] Strategy 'first' was applied to 857 files (Run the task at debug level to see details)
[info] Assembly up to date: /home/user/IdeaProjects/adw/core/target/scala-2.12/core.jar
[info] Strategy 'discard' was applied to 116 files (Run the task at debug level to see details)
[info] Strategy 'first' was applied to 857 files (Run the task at debug level to see details)
[info] Assembly up to date: /home/user/IdeaProjects/adw/docs/target/scala-2.12/docs.jar
[success] Total time: 3 s, completed 4/dez/2018 10:51:15
```

At his point in time we have opted to deactivate the tests when generating the fat Jar (for testing purposes). If this 
were on and the tests failed, no fat Jar would be generated. Be sure to run the tests and confirm that all is working 
correctly before generating and using the Jar. 

# Using the fat Jar

In order to use the fat Jar we have to indicate the classpath top the JVM. Next we show an example of how we can execute 
a `main` method of a Scala object. First establish the base directory from were you will execute the application. In 
this example we use the platform's project root so that the data files can be found by the application. The example 
below shows the platform's root directory used in he next example (`pwd` - print name of working directory):

```bash
user@machine:~/IdeaProjects/adw$ pwd
/home/user/IdeaProjects/adw
```

To run a standard application invoke the JVM indicating the fat Jar and the main class and any parameters you need:

```bash
java -cp ./core/target/scala-2.12/core.jar inegi.ADIRAI40Experiments 1
```

The SBT assembly plugging should allow one to create the fat Jar with all the required code, including the test libraries 
and test code. The SBT command would be `test:assembly`. However we were not able to make this work. To run a 
[Scala test](http://www.scalatest.org/user_guide/using_the_runner){:target="_blank"} we therefore need to:
* Add the Scala test libraries to the JVM classpath;
* Add the the compiled test classes to the JVM classpath;
* Use a ScalaTest `Runner` to instantiate and run the tests.

The next line shows an example of running the `pt.inescn.detector.FCCAnomalyDetectorSpec` test in a **Linux** terminal: 

```bash
java -cp ./core/target/scala-2.12/core.jar:/home/user/.ivy2/cache/org.scalatest/scalatest_2.12/bundles/scalatest_2.12-3.0.5.jar:./core/target/scala-2.12/test-classes/ org.scalatest.tools.Runner -s pt.inescn.detector.FCCAnomalyDetectorSpec -o -e
```

In the command above the `-o` wnd `-e` set the output to the standard and error console output. If this were not set a 
GUI would list and report test execution results. The `scalatest_2.12-3.0.5.jar` is the ScalaTest library that 
contains the `org.scalatest.tools.Runner` class, which is responsible for running the test suites. The example below is 
the same as the one above but executed in a **Windows** console:

```cmd
java -cp ./core/target/scala-2.12/core.jar;C:/Users/user/.ivy2/cache/org.scalatest/scalatest_2.12/bundles/scalatest_2.12-3.0.5.jar;./core/target/scala-2.12/test-classes/ org.scalatest.tools.Runner -s pt.inescn.detector.FCCAnomalyDetectorSpec -o -e
``` 
