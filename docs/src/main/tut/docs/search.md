---
layout: docs
title:  "Introduction"
section: "intro"
---

# Parameter Search

This sections delves into the use of `Pipe`s that can be used to generate and search for the best ML hyper-parameters. 
We first look at how one can enumerate parameter values that can be used by the ML `Task`s. We then see how one can 
combine these parameters to generate sets of parameters by applying specialized operators. In this manner a user can 
set-up a parameter search space without the need to explicitly code the loops. Finally we show how these parameter 
search spaces can be consumed by `Task`s or used by `Pipe`'s to generate a pipeline search space. These form the 
building blocks to performed grid, random and mixed parameter searchers of ML hyper-parameters.  
 
A word of warning - many of the parameter enumerators are infinite. The documentation here prints out the values 
automatically, so care has been taken to avoid iterating over infinite sequences. In standard code, the streams are 
lazy so one need (should) not convert then to finite sequences unless explicitly necessary (explained later). Remember
that a stream connected to a sensor source is essentially an infinite data source. 
 
## Search operators

The operators that allow us to combine the streams (Iterators) are found in `pt.inescn.samplers.Ops`. The following 
packages under `pt.inescn.samplers` allow us to create finite and infinite streams:
* `pt.inescn.samplers.Enumeration`: generate standard streams (constants and ranges)
* `pt.inescn.samplers.Distribution`: generate real, integer proportional and mixed random data distributions
* `pt.inescn.samplers.Function`: generate data that are outputs of some standard functions (sine, chirp)
Note that all operators and streams are compatible with (equivalent to) Scala's Iterable. So the Scala's collection 
library can be used. 
 
### Combining Streams

We will no look at how one can generate and combine simple streams. To be able to do this we must first import 
the functions that do this witht he following code snippet: 

```tut:book
import pt.inescn.samplers.stream.Ops
import pt.inescn.samplers.stream.Ops._
```

The code the follows uses the Scala `Range` to create two finite sequences of integers. Many a times we want 
to test all combinations of a set of parameters. To do this we must combine the various streams to generate 
the cartesian product of all the parameters values. The code below shows how we use the `cartesia` function
to combine the streams to generate all possible combinations. 

```tut:book
    // Cartesian product
    // Its is commutative but resulting tuple order varies
    val pi1s = 1 to 2 by 1
    val pi2s = 1 to 3 by 1
    val pis1 = cartesian(pi1s, pi2s)
    // Should have 6 elements 2 x 3
    val p1s1l = pis1.toList
```

As can be seen, the resulting stream will have the expected 6 elements (2 x 3). Although we can use the function 
directly, it is easier to read the expression if we use the inline `##` operator so:

```tut:book
    // Cartesian product using operator
    // Same results as `cartesian`
    val pis1op = pi1s ## pi2s
```

This and any other operators we represent here can be applied repeatedly to generate sequences of parameters with 2 
or more values. Note also that, depending on the operator, combining two or more infinite streams will also produce  
an infinite stream. In the case of **cartesian operation, it cannot be applied to infinite sequences** otherwise it 
can no reset on a terminating stream so as o combine with different values of the other streams. A cartesian 
product will always terminate when all underlying streams terminate. Combining a special **empty** sequence will
automatically terminate iteration as is shown int hte code snippet below:

```tut:book
    // Combing an empty sampler with any other samples results in an empty sample
    val pi0s = Ops.empty[Double]
    val pis2 = cartesian(pi0s, pi2s)
    // Should be empty
    val p1s2l = pis2.toList
```

Even if we apply the empty sequence to several combinations, the resulting stream will still be empty: 

```tut:book
    // same as above but with operators
    val pis2op = pi0s ## pi1s ## pi2s
    // should be 0
    pis2op.size 
```

Streams are constructed using Scala `Tuple`s. This means that we can use different types for the different parameters
as is shown below:

```tut:book
    // We can combine samples of any type
    // We get arbitrarily nested tuples as a result
    val pi3s = 'a' to 'c' by 1
    val pis4 = cartesian(pis1, pi3s)
    val pis4l = pis4.toList
    // Should be 3 x 6 = 18
    pis4l.size
    
    // Same as above but with an operator
    val pis4op = pis1 ## pi3s    
```

Internally each operator combines the pair of operand elements using a `Tuple2`. This means that after applying the
operator more than twice we will end up with streams of combinations of tuples of tuples. More generally the order 
in which you apply the operators determines the structure of the resulting tuple. The following examples shows how
we can *shape* the tuples:

```tut:book
    // Same as above but with an operator
    val pis43op = (pi1s ## pi2s) ## (pis1 ## pi3s)   
```

The next operator we will look at is the `And` operator (`&&`). In this case the elements of each stream will be 
paired in order until the first (shortest) stream terminates. In the example below we see that one stream is shorter 
than another, so the resulting sequence length is as long as the shortest stream that was combined. 
 
```tut:book
    // And operator
    // Sampling in parallel (as long as the longest sampler)
    // [1,2] and [a,b,c] -> [(1,a),(2,b)]

    val pis4op = pi1s && pi3s
    // Expect contain (1,'a'),(2,'b')
    pis4op.toList

    // And operator is commutative resulting tuple order varies)

    val pis5op = pi3s && pi1s
    // should contain ('a',1),('b',2)
    pis5op.toList

```

The `And` can be safely applied to any combination of finite and infinite streams. In the case all combined streams 
are infinite, then the resulting stream will also be infinite. This operator is a good candidate for mixing finite
(possibly nominal) parameters with infinite numerical parameters. 

The next operator we will look at is the `Or` operator. In this case the elements of each stream will be paired
in order until all of the underlying streams have reached the end at least once.  

```tut:book
    // Or operator
    // Keeps combining the samplers in parallel as long as at
    // least one element in a sub-sampler has not been visited

    val pis6op = pi1s !! pi3s
    // should contain (1,'a'), (2,'b'), (1,'c'), (2,'a'), (1,'b'), (2,'c')
    pis6op.toList

    // Commutative (save for the tuple order)

    val pis7op = pi3s !! pi1s
    // should contain ('a',1), ('b',2), ('c',1), ('a',2), ('b',1), ('c',2)
    pis7op.toList 
```

In the example above we see that the shortest stream will have some of its elements repeated and combined with 
alternating elements of the longer stream. Unlike the `And`, the `Or` operator cannot be safely used with infinite
streams because such streams end will never occur and cycling through alternate values cannot happen. However if
all streams are infinite, both the `And` and `Or` operators will generate the same combined values.

The next operator is the pair operator `#&` that allows us to control how one stream's elements can be paired with 
multiple elements of another finite stream. 

```tut:book
    // Algorithm 1 has 2 parameters, one for kernels and another general one
    val param1_Alg1 = Iterable("k1", "k2", "k3")
    
    // parameter1 : But it each kernel has its own set of parameters
    val alg1_k1_p1 = Iterable("k1p1", "k1p2", "p3")
    val alg1_k2_p1 = Iterable("k2p1", "k2p2", "p3")
    val alg1_k3_p1 = Iterable("k3p1", "k3p2", "k3p3")

    // we now want to pair (left) the kernels with each of its parameters
    val sParam1_Alg1 = param1_Alg1 #& Iterable(alg1_k1_p1, alg1_k2_p1, alg1_k3_p1)
    // See how each kernel is assigned only the required/valid parameters
    /* should contain 
             ("k1","k1p1"),("k1","k1p2"),("k1","p3"),
             ("k2","k2p1"),("k2","k2p2"),("k2","p3"),
             ("k3","k3p1"),("k3","k3p2"),("k3","k3p3")
             */
    sParam1_Alg1.toList
```

This operator should only be applied to finite streams because one paring must terminate before the next is 
initiated. To illustrate the use of this operator lets assume we have a function that does SVM modeling. This
function supports various kernels (k1, k2, k3) and each kernel has its own set of parameters. For example kernel
k1 has specific kernel parameters k1p1, k1p2 and p3. Note that parameter p3 may also be use by some another (but 
not all other kernels). We therefore want to pair the parameter `k1` with parameters `k1p1`, `k1p2` and `p3`. We
also want to do the same for kernels `k2`and `k3` with their respective kernel specific parameters. 

In the example above we pair kernels `param1_Alg1` with a iterator `Iterable(alg1_k1_p1, alg1_k2_p1, alg1_k3_p1)`,
were each element is a stream that will e paired (in the same order) with the values of `param1_Alg1`.

The next operator allows us to `repeat` a set of finite streams continuously. The result stream is infinite. Note
that the repeated stream must be finite otherwise no repetition will occur and the operator will be ineffective.

```tut:book
    val s1 = 1 to 6
    val s2 = 'a' to 'c'

    // This is the sampling we want to repeat
    val c1 = s1 !! s2
    // should contain (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c')
    c1.toList 

    // We now repeat the samples above (ad-infinitum)
    // Lets sample c1 twice
    /* should contain 
             (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c'),
             (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c')
    */     
    val s3 = repeat(c1).take(12).toList
```

We also have a `repeatFor` operator that allows us to explicitly indicate how many times we want a stream is to be
repeated. The example below is equivalent to the one above but we explicitly indicate that we only repeat sampling
if the inner streams 2 times. We can now *extract* the full data because the sampling is limited. 
 
```tut:book
    // We now repeat the samples above twice
    // Lets sample c1 twice
    /* should contain 
             (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c'),
             (1,'a'),(2,'b'),(3,'c'),(4,'a'),(5,'b'),(6,'c')
    */ 
    val s4 = repeatFor(2)(c1).toList
```

Note however that the **inner streams may not be infinite** otherwise the repeat operator will not terminate. We can 
always limit sampling by using the `unit` operation shown below. 

Just as we can *extend* sampler streams using `repeat`, we can also terminate them earlier. The example below keeps 
generating the streams value's until a condition is met.  

```tut:book
    val c2 = s1.until(0) { (z:Int, i:Int) => (z, i > 5) } !! s2
```

We can also use a the `until` to select a fixed number of samples so:

```tut:book
    val c3 = s1.until(0) { (z:Int, i:Int) => (z+1, z < 12)}
```


### Enumerators

Enumerators essentially allow us to generate sequences of values of either float or integer values. The code below
shows how we can generate finite and infinite sequences of float values: 

```tut:book
    import pt.inescn.samplers.stream.Enumeration
    
    val r1 = Enumeration.range(0.0, 10, 1)
    // Should contain 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0
    r1.toList 

    val r2 = Enumeration.from(0.0, 1.0) take 11
    // Should contain 0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0
    r2.toList 

    val r3 = Enumeration.continually(1.0) take 11
    // Should contain 1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0,1
    r3.toList 

    val r4 = Enumeration.continually(1.0) map(i => i + 1.0) take 11
    // Should contain 2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2.0,2
    r4.toList 
```

Note that we can apply standard Scala operations on any of these streams (`Iterables`). In the example above the
map operation was used to apply a simple function to the streams values. The example below is the same as the one
above however in this case we generate sequences of integers. 

```tut:book
    val r5 = Enumeration.range(0, 10)
    // Should contain 0,1,2,3,4,5,6,7,8,9,10
    r5.toList

    val r6 = Enumeration.from(0) take 11
    // Should contain 0,1,2,3,4,5,6,7,8,9,10
    r6.toList 

    val r7 = Enumeration.continually(1) take 11
    // Should contain 1,1,1,1,1,1,1,1,1,1,1
    r7.toList 

    val r8 = Enumeration.continually(1) map(i => i + 1) take 11
    // Should contain 2,2,2,2,2,2,2,2,2,2,2
    r8.toList 
```

### Timestamps

In order to generate timestamps with a given sampling rate we need only indicate:
* The initial timestamp
* The delta time between each timestamp (inverse of the sampling frequency)
In the example below we use some helper function to determine the sampling delta of a 500 Hz signal. At the end
we sample this infinite sequence and check that that the timestamps are correct.

```tut:book
    import java.time.{Duration, Instant}
    import pt.inescn.dsp.Fourier

    val startInstant = "2017-06-16T11:35:54.100000Z"
    val start = Instant.parse(startInstant)
    
    val (dd,_) = Fourier.samplingSpecification(500,2)
    val delta = Fourier.convertFractionSecondsToDuration(dd)
    
    val len = 10
    val y = Enumeration.from(start, delta).take(len).toSeq
    
    // shouldBe 10
    y.length 
    
    // should be start
    y.head 
    
    // should be t1
    val t1 = Instant.parse("2017-06-16T11:35:54.102Z")
    y(1) 
    
    // Should be t9
    val t9 = Instant.parse("2017-06-16T11:35:54.118Z")
    y.last 

```

### Distributions 

A popular but inefficient way of selecting hyper-parameters is using grid search. An effective alternate is random 
search. In tis section we list the streams that allow us to sample several real number, integer and nominal 
distributions. We also show how these streams can be used to emulate noise or create multi-modal distributions. 

#### Real Number Distributions 

In order to generate an infinite sequence of random numbers we first need to instantiate a (pseudo-)random number 
generator. In the event you are using this to perform experiments (for example simulation), it is possible to set 
a constant seed so that the experiments are repeatable. We then instantiate the desired distribution and sample 
that. In the example below we sample a normal distribution with mean 0.0 and standard deviation 1.0.

```tut:book
    import pt.inescn.samplers.stream.Distribution
    import org.hipparchus.random.JDKRandomGenerator

    implicit val rnd: JDKRandomGenerator = new JDKRandomGenerator(9876)

    val y1: Seq[Double] = Distribution.normal(0,1.0).take(500).toSeq
    // should be 500
    y1.length 
```    
    
In the example that follows we list the Uniform, Gamma, Log-Normal, Exponential and Beta real valued distributions 
and their respective parameters.
    
```tut:book
    val a = 0
    val b = 1

    val y = Distribution.uniform(a, b).take(1000).toSeq
    // should be 1000
    y.length   
    
    val shape = 9.0
    val scale = 0.5
    val y = Distribution.gamma(shape, scale).take(10000).toSeq
    // should be 10000
    y.length     
    
    val shape = 0.25 // standard deviation
    val scale = 0.05 // mean parameter
    val y = Distribution.logNormal(shape, scale).take(1000).toSeq
    // should be 1000
    y.length 
    
    val pmean = 0.5
    val y = Distribution.exponential(pmean).take(1000).toSeq
    // should be 1000
    y.length 
    
    val alpha = 2.0
    val beta = 2.0
    val y = Distribution.beta(alpha, beta).take(1000).toSeq
    // should be 1000
    y.length 
```

#### Integer Distributions 

In order to generate infinite samples of random integer values we proceded in the same fashion as we did for the 
real valued distributions. In the example that follows we list the Zipf, Poisson and uniform integer values 
distributions and their respective parameters.


```tut:book
    val numberOfElements = 5 // K
    val exp = 4  // s
    val y = Distribution.zipf(numberOfElements, exp).take(1000).toSeq
    // should be 1000
    y.length 
    
    val smean = 20.0
    val y = Distribution.poisson(smean).take(1000).toSeq
    // should be 1000
    y.length 
    
    val a = 0
    val b = 1

    val n = 1000
    val y = Distribution.intUniform(a, b).take(n).map(_.toDouble).toSeq
    // should be n
    y.length 
    
    val a = 0
    val b = 1

    val n = 1000
    val y = Distribution.longUniform(a, b).take(n).map(_.toDouble).toSeq
    // shouldBe n
    y.length 
```

#### Discrete Distributions

In addition to numerical values we may also need to randomly generate nominal values or symbols. In this case we
need to express both what symbols that will be generated and the probability that each such symbol will be 
generated. This information is is stored in a `Map[Symbol, Proportion]` were `Symbol` can be any valid Scala 
object and `Proportion` is expressed as a floating value. The latter value represents a proportion that may be 
represented for example as a fraction 0..1 or even a percentage 0% to 100%. For easier checking the convention is
that these proportions some to a 100%. 

In the case below we generate a sequences of symbols were 10% of these symbols are value 1, 20% are symbol 2 and
70% are symbol 3 representing a total of 100% of the distribution. We then generate the infinite sequence via the
call to `proportional`. 

```tut:book
    val len = 10000
    val denom = len / 100.0
    val eps = 1.5 // %

    val mp = Map[Int,Double](1 -> 0.1, 2 -> 0.2, 3 -> 0.7)
    val y = Distribution.proportional(mp).take(len).toSeq

    val gen1_classes = y.groupBy( p => p ).map( p => (p._1, p._2.length) )
    // should be about 10%
    gen1_classes(1)
    // should be about 20% 
    gen1_classes(2)
    // should be about 70%
    gen1_classes(3)
```

In the example above we generate 10000 samples of this distribution. We then collect and count the number of 
samples of each simple. You can confirm that the proportions are as expected.   


#### Shaping Real-value Distributions

We may need to declare and sample some specialized distribution. One way to do this is to construct a `mixed`
distribution. As with the [Discrete Distributions](discrete-distributions) we need to declare the the proportion at 
which several underlying distributions are sampled. The example below shows how we can create a bi-modal 
distribution based on two normal distributions - one distribution (`x1`) has mean 0 and standard deviation 0.25 
while the second distribution (`x2`) has mean 1.5 and a standard deviation of 0.5. We now sample both underlying 
distributions with equal (50%) probability by declaring this via the `Map(x1 -> 0.5, x2 -> 0.5)`:

```tut:book
    // We can combine various distributions to construct our own tailor made PDFs
    // Lets say we want to produce a univariate bi-modal distribution: one node at
    // mean = 0, sd = 0.25 and another at mean = 1.5 and sd = 0.25
    val n_samples1 = 10
    val x1 = Distribution.normal(0.0, 0.25)
    val x2 = Distribution.normal(1.5, 0.5)
    val mp = Map(x1 -> 0.5, x2 -> 0.5)
    val x1x2 = Distribution.mixed( mp ).take(n_samples1).toSeq
```

### Functions

For some experiments we may need to generate artificial data and use that to test algorithms or perform some type
of sanity checking. We provide a few useful period functions that allows us to test some basic functionality such
as FFT or EMD decomposition. The code snippet below uses helper functions to generate samples from a 2Hz sine
wave with an offset of -45 degrees.   

```tut:book
    import pt.inescn.samplers.stream.Function

    // Period of 1 cycle = 0.5 sec
    val frequency = 2
    val amplitude = 1.0
    val phase = Math.toRadians(-45)

    // If one cycle is 0.5 sec, then in 1.0 sec we have 2 cycles
    // Lets sample at 10 times the signal frequency, increases sampling resolution only
    val (_,x) = Fourier.samplingSpecification(10*frequency,1.0)
    val sinF = Function.sin(frequency, amplitude, phase) _
    val y = x.map(sinF)
```

Note that we we could have used any sequential set of numeric values as the `x`input for the sine wave. The helper
function simply creates the appropriate `x` values. As seen below we also provide cosine, linear Chirp and Exponential 
Chirp functions. 

```tut:book
    val cosF = Function.cos(frequency, amplitude, phase) _
    val y = x.map(cosF)

    val deltaAmp = 1.2
    val deltaFreq = 0.9

    val chirpF = Function.linearChirp(frequency, amplitude, phase, deltaAmp, deltaFreq) _
    val y = x.map(chirpF)

    val deltaAmp = 1.2
    val deltaFreq = 1.5

    val (_,x) = Fourier.samplingSpecification(40*frequency,2.0)
    val chirpF = Function.expChirp(frequency, amplitude, phase, deltaAmp, deltaFreq) _
    val y = x.map(chirpF)
```

### Numeric Operations

When generating artificial data it is necessary we combine different *signals* by applying various mathematical 
operations. In the example below we generate two signals (`sig5` and `sig6`) by combining 3 sinusoidal signals
of varying frequencies and a random normal distribution signal representing noise. Currently we only support
the 4 basic mathematical operations of addition, subtraction, multiplication and division. Any other functions
may be applied by a `map` operation.

```tut:book
    val f1 = 1 /* Hz */ ; val a1 = 1  ;  val phase1 = Math.toRadians(-45)
    val f2 = 2 /* Hz */ ; val a2 = 1  ;  val phase2 = Math.toRadians(0)
    val f3 = 4 /* Hz */ ; val a3 = 0.6 ; val phase3 = Math.toRadians(20)
    //val samplingFreq = Fourier.nyquistNamplingFrequency(f1, f2, f3)
    // samplesTo at 500Hz and samplesTo for 2 seconds
    val (_,xx) = Fourier.samplingSpecification(500,2)

    val sig1 = xx.map( Function.sin(f1, a1, phase1) )
    val sig2 = xx.map( Function.sin(f2, a2, phase2) )
    val sig3 = xx.map( Function.cos(f3, a3, phase3) )
    val sig4 = Distribution.normal(0, 0.3)
    
    val sig5 = (sig1 - sig2 + sig3 + sig4).toSeq
    val sig6 = (sig1 * sig2 / sig3 + sig4).toSeq
```

## Grid Search

The goal of these samplers is to be able to generate (hyper) parameters that can be use by the ML algorithms so
that we can perform parameter tuning. Lets say for example we want to test several values for he learning rate 
of some algorithm. The code snippet below shows how we can generate these values using an exponential function. 

```tut:book
    // An example of an exponential learning rate parameter
    // 0.001, 0.01, 0.1, 1, 10, 100, .....
    val power = Function.power(10) _
    val learningRate = Enumeration.from(-3.0, 1.0).map( power )
    // should contain 0.001, 0.01, 0.1, 1, 10, 100
    learningRate.take(6).toList 
```

We have already seen that applying the combination operators to several streams will result in a structure of
nested tuples depending on the way these operators are used. However what we really need is a single n-Tuple
that contains all of the values in the order they were declared so that they can be used by any parameterized 
function independently of the way the streams were constructed. We must therefore flatten the resulting stream
into a single n-Tuple using the `flatten` function[^1]. The example below show how an arbitrary nested tuple
can be converted ti a single n-Tuple:

```tut:book
    // we now want to pair (left) the kernels with each of its parameters
    val sParam1_Alg1 = param1_Alg1 #& Iterable(alg1_k1_p1, alg1_k2_p1, alg1_k3_p1)

    val param2_Alg1 = Iterable("a1p2_1", "a1p2_2")
    // Now we can combine the paired parameters with any of the general parameters
    val s_Alg1 = sParam1_Alg1 ## param2_Alg1

    flatten(s_Alg1)
``` 

The example above effectively shows how we can set-up a grid search using the `##` operator and the `flatten`
function to declare and explore a search space of parameter values. 

## Random Search

As with the example above we can also declare and explore the parameter space by sampling it randomly. If we
want to declare a simple random search combining only infinite random distributions, we need only use the `&&`
operator. If we have one or ore finite (possibly nominal value) streams then we can use the `repeat` operator
to make them infinite and then use the `&&` as on all of the streams. The example below shows an alternate method
of how we can mix finite parameter values and infinite random values to form what is essentially a random parameter 
search. 

```tut:book
    // Lets set-up the search for the learning rate an exponential learning rate parameter
    // Lets use this 0.001, 0.01, 0.1, 1, 10, 100, .....
    val power = Function.power(10) _
    // should contain 0.001, 0.01, 0.1, 1, 10, 100
    val learningRateExp = Enumeration.from(-3.0, 1.0).map( power )

    // But we also want to explore random values for this parameter
    val x3 = Distribution.uniform(0.001, 1e3)

    // We also have a discrete and finite parameter that we want to combine
    // with both of the parameters above.
    val x4 = Iterable("k1", "k2")

    // NOTE: if at least one of the samplers are infinite, then the
    // composed sampler is also infinite. 
    val xs1 = (x4 !! (learningRateExp take 6) !! x3) take(12)
    val y1 = flatten( xs1 ).toList
```

We have use the (or) `!!` operator that will cycle each sampler until all finite streams both have had all 
their samples taken. As was previously stated because the or operator keeps cycling the values before 
terminating we have to ensure that finite streams are the final streams that are combined. Note also that
because we have at least one infinite stream, the result combined streams will also be an infinite stream. 
For this reason we need to `take` a fixed number of samples. In the case above because the variable are
printed out, we avoid issues with infinite streams by applying the `take` before the `flatten`, however this
is not required. We can always flatten infinite streams. 

## Mixing Search Strategies

In the examples above each sampler that is combined is either a finite or infinite stream. We can also append 
various search parameters and combine those if need be. Say we want to test some specific learning rates that 
were used in a previous experiment but also explore additional random values. We simply append to the finite
stream of pre-defined values an infinite (or finite) stream of values as is shown below. 

```tut:book
    
    val referenceLearningRate = Iterable(0.002, 0.5)
    // should contain 0.002,0.5,0.001,0.01,0.1,1.0,10.0,100.0
    val allLearningRates = (referenceLearningRate +++ learningRate).take(8).toList
```

In these case it is important **make sure that the non-terminating stream appears last** with no other 
non-terminating samplers. If this were not so, iterating through the values of the first infinite stream 
would not terminate and no elements of the following iterators would be reached. 

# Pipes with Parameters

The streaming operators were designed to work closely with the [Pipe](pipes.html) so that we can define a
search space of pipelines. Each pipeline can be used to evaluate the effects of using alternate tasks 
parameterized with different hyper-parameters. The result of the evaluation can then be used to perform
hyper-parameter tuning or sensitivity analysis. Before proceeding lets import the elements required 
to define the Pipes.

```tut:book
import pt.inescn.utils.ADWError
import pt.inescn.etl.Load.format
import pt.inescn.etl.stream.Load._
import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.Pipes._
import pt.inescn.macros.TaskMacro

// IDE does not detect need, keep it
import pt.inescn.macros.MacroCore.TaskInfo
import pt.inescn.macros.TaskMacro._
```

Besides the Pipes we will also use a `Frame` that represents the data stream that is processed by the ML algorithms 
for learning and prediction. It provides some useful functions and also ensures ensures laziness (Scala's map for 
example does not guarantee this). The Frames' internal `Iterable` and its respective `Iterator` are nonetheless 
available for use by the client. A detailed description is found [elsewhere](frames.html).

The example below shows a trivial pipe consisting of a single parametrize a task `funcs3` being executed with
a set of parameters defined using the stream operators we describe in tbis section.

```tut:book

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

    // This is an example of how we create a two parameter task

    // Lets set-up a single parameter
    val p1s = BigDecimal(1.0) to 10 by 1.0 map(_.toDouble)

    // This is the second parameter
    val p2s = BigDecimal(1.0) to 100 by 10.0 map(_.toDouble)
    
    // This is the second parameter
    val p3s = 'a' to 'j' by 1

    // Now we have to define the search space of the parameters
    val tmp = p1s && p2s && p3s
    
    // NOTE: now we have to flatten the search space so that implicit
    // task creation is possible. Note that the cse.Result type is
    // the same type as the tmp elements ((Double, Double), Char)
    val psFunc3 = flatten(tmp)

    // Create a task with two parameters using the defined search space
    val funcs3 = T(
            (p1:Double, p2:Double, p3:Char, i:Double) => {
        val tmp = p1 * p2 * i
        Right(tmp.toString + p3)
      }, psFunc3)

    // If we compile this we get a single task pipe for each parameter combination (search space)
    val ops3 = compile(funcs3)
    // We check that all compiled pipes have succeeded for each parameter
    // should be true
    ops3.forall{ case Right(_) => true ; case _ => false }
    // should be 10 
    ops3.size 
```

We firsts set-up a data source `Frame` tha will be used by the `funcs3` function. This function has three parameters 
(`p1`, `p2` and `p3`) and an input value `i`. We declare and define the parameter search space `tmp` by doing a 
simple sequential search. Note that we must flatten the search space in order to ensure that we have an n-Tuple 
available for use in a `Task`. Next we use the `T` operator to automatically create a `Task` that will apply all 
values of the search space as parameters to the function `funcs3`. We then compile the `Pipe` definition of a 
single task in order to obtain the pipeline search space. Because we only have combinations of parameters (shortest
parameter stream has 10 elements), the resulting number of pipelines is also 10. With some effort yu can also see
ome of the pipeline's parameters shown in the Task's parameter `Map`.

Unlike many auto-ML packages we define a pipeline search space by defining the parameter search space of each `Task`
and then combine those tasks into a `Pipe`. It is important to note that the final pipeline search space is defined
both by the parameters stream operators and the Pipe combination operator. The example below shows how we can 
combine the parameter streams and Pipe combinators to create a pipeline search space.

```tut:book
    import pt.inescn.search.stream.UtilTasks._

    // We now show how to use search spaces in a pipe
    val empty = Frame(("", List()))


    // Task 1: applies a function to the input of a Frame's column with 2 parameterized values
    val p11 = Enumeration.range(1.0, 3.0, 1.0)
    val p12 = Enumeration.range(1.0, 2.0, 1.0)
    val p13 = Iterable("x","y")
    val ptsk1 = flatten( p11 ## p12 ## p13)
    val tsk1: All[Frame,Frame] = T(
        (a: Double, b:Double, col:String, i:Frame) => {
          // convert the col to double and calculate the output
          val tmp:Frame = i((v:String) => a*v.toDouble + b, col)
          Right(tmp)
        },
    ptsk1)

    // Task 2: converts a Frame's column from Double to a string and prepends a parameterized string
    val p21 = Enumeration.continually("xCol:").take(6) +++ Enumeration.continually("yCol:").take(6)
    val p22 = Enumeration.continually("x").take(6) +++ Enumeration.continually("y").take(6)
    val ptsk2 = flatten( p21 && p22)
    val tsk2: All[Frame,Frame] = T(
        (a: String, col:String, i:Frame) => {
          // covert the column to a string and prepend the 'a'
          val tmp:Frame = i((v:Double) => a+v.toString, col)
          Right(tmp)
        }
      ,
    ptsk2)

    // now combine the tasks
    val s2 = load(testData) *: tsk1 ->: tsk2

    // Prepare the execution
    val c2 = compile(s2)
    // We check that all compiled pipes have succeeded for each parameter
    // should be true
    c2.forall{ case Right(_) => true ; case _ => false } 
    // shouldBe 12 = 2 * 3 * (x,y)
    c2.size 

    // Lets execute each of these pipes and extract the results
    val result4 = exec(c2)(empty)
    val results4: Iterable[Frame] = result4.map{ case (Executing(_), r) => r}
    // We execute the pipes and get a result for each
    val result4s = results4.toIndexedSeq
    // shouldBe 12 = 2 * 3 * (x,y)
    result4s.size 

    // Frames are ephemeral lazy streams, we can only access them once, so keep a copy
    // And use iterable to execute pending operations and get result
    val r4row1 = result4s(0).iterable.toList
    // We now check that we get the expected results
    /* should contain
        Row(Map("x" -> Val("xCol:2.0"), "y" -> Val("2"), "z" -> Val("3"))),
        Row(Map("x" -> Val("xCol:2.0"), "y" -> Val("2"), "z" -> Val("3"))),
        Row(Map("x" -> Val("xCol:2.0"), "y" -> Val("2"), "z" -> Val("3")))
    */
    r4row1 
    
```

As with the previous example we must define both the tasks and each tasks parameters search space. In the example
above we have defined tasks `tsk1` and `tsk2` with their respective parameters search spaces `ptsk1` and `ptsk2`. 
We then define a very simple Pipe that loads the data in a Frame executes then executes tasks `ptsk1` and `ptsk2`
in sequence, the output of the first task passing onto the second task. 

Both tasks `ptsk1` and `ptsk2` have a total parameters search space of 12 combinations of values each. In effect 
they represent 12 unique task instances each. Because in the Pipe definition we use the `->:` combination the 
resulting pipelines will execute each specific `ptsk1` instance followed with another specific `ptsk2` instance. 
We will therefore produce a total of 12 pipelines during compilation and execute that man pipelines later. We can 
confirm these values in the example above. Note however that because the results are automatically printed out here,
the results in `Frame`output of the pipelines cannot be viewed (`r4row1` is empty). However experimenting in the
Scala REPL should show the expected result.

If however we had combined the tasks with the `*:` operator then all combinations of unique tasks instances `ptsk1` 
and `ptsk2` would be used. This means the Pipe would generate 12 x 12 = 144 pipelines. Care should be taken when 
setting up the search space as it grows exponential.


[^1]: This function is a **macro** that also uses Scala's **priority inference** to identify the tuple structure and generate a function during compilation time that will flattens that specific structure.
