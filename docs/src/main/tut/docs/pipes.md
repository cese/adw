---
layout: docs
title:  "Introduction"
section: "intro"
---

# Pipes

A Pipe is a **directed acyclic graph**[^1] of tasks that describes the order in which [tasks](tasks.html) are executed. 
The graph is specified using a set of **operators** of a very simple (*informal*) **algebra**. Each path of such a graph 
is converted into a single pipeline of the tasks that are executed in linear sequence. As an example, the graph below 
depicts a pipe consisting of tasks `A`, `B`, `C` and `D`. 
<div class="mermaid">
graph LR;
  title[<u>Pipe Example 0</u>]
  A --> B;
  A --> C;
  B --> D;
  C --> D;
</div>
This graph has a total of two paths:
* A -> B -> D
* A -> C -> D

Each path enumerated above constitutes a **pipeline**. A Pipe's pipelines are generated during a *compilation* phase 
were the various constructs of the Pipe algebra are parsed and transformed into partially[^2] evaluated functions. 
Specifically each pipeline is converted into a single function whose input type is the same as the pipe's first function 
input type and the output type is the same as the pipeline's last function output type. More concretely if we have the 
following function types[^3]:
* A: `Function[I1,T1]`
* B: `Function[T1,T3]`
* C: `Function[T1,T3]`
* D: `Function[T3,O1]`

this means that the following partially evaluated functions will be generated with the `Function[I1,O1]` signature for 
**all pipelines**. It is important to point out that all pipelines will have the same input and output types in order 
to ensure type consistency. The pipe constructs use Scala's generic type so that any types may be passed on from task 
to task. However the Scala compiler will check that the types are correct ensuring proper Pipe construction (pipeline 
execution won't fail during runtime due to incorrect function parameter types). 

The *compilation* is executed in a lazy fashion. This means that each pipeline is generated one at a time. This process 
can proceed indefinitely if for example the Pipe encodes and infinite number of pipelines (explained later). The result of
the compilation is an `Iterable` [^4] containing the partially evaluated functions. A client can then collect and use 
each function as required (for example sampling the first `n` pipelines). 

## Pipe Algebra

In this section we describe the various operators that are available to construct the Pipe. We will use some functions
that are available in other packages to make these examples clear and concise. We assume the reader has already read the 
material on [tasks](tasks.html). We first proceed with a minimal set of imports.

```tut:book
import pt.inescn.etl.stream.Load._
import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.TestTasks._
import pt.inescn.search.stream.UtilTasks._
import pt.inescn.search.stream.Pipes._
import pt.inescn.utils.ADWError
```

The first import provides us with a `Frame` that holds a stream of data (this allows for lazy processing of data 
streams). The imports of `Tasks._`, `TestTasks._`and `UtilTasks._` provide us with example tasks to be used in the Pipe.
The `Pipe._` import provides us with the operators that enable Pipe construction via an *algebra* and access to 
several utility functions used for compiling and executing the pipelines. 

### Sequential Processing Operator `->:`

The sequential infix operator `->:` is the simplest operator of all. It defines the order in which two tasks are 
executed (from left to right). Lets say we want to implement the following pipe:

<div class="mermaid">
graph LR;
  title[<u>Pipe Example 1</u>]
  A["concat(0, x, y, z)"] --> B["concat(1, x)"];
  B["concat(1, x)"] --> C["concat(2, y)"];
  C["concat(2, y)"] --> D["concat(3, z)"];
</div>

The example below shows how to construct the Pipe consisting of 4 (of the same) tasks, how to compile it to generate 
the pipelines and finally how to execute the pipelines:

```tut:book
    val empty = Frame(("", List()))

    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )

    // Pipe
    val s1 = concat("0", "x", "y", "z") ->: concat("1", "x") ->: concat("2", "y") ->: concat("3", "z")

    // compile    
    val c1 = compile(s1)
    val ex1 = c1.toList
    ex1.size
    
    // execute
    val r1 = execDebug(c1)(Frame(testData: _*))
    val r1l = r1.toList
    r1l.size
```

The `concat` function is a simple task used for testing and demonstration purposes. It simply concatenates its first 
parameter to the `Frame`s columns named by the `concat`s last parameters (columns *x*, *y* and *z* in this case). So
`concat("0", "x", "y", "z")` will concatenate *"0"* to all of the the Frame's values in columns *x*, *y* and *z*. The 
pipe `concat("0", "x", "y", "z") ->: concat("1", "x") ->: concat("2", "y") ->: concat("3", "z")` will generate a new 
data frame so that all values of all columns have as its second last character the *"0"*. All values of column *"x"* 
have "1" as the last character. All values of column *"y"* have "2" as the last character. All values of column 
*"z"* have "3" as the last character. 

In order to execute this pipe we first need to `compile` it. The result is an iterator of either a compilation error or
a pipeline function. This compilation also includes the tracking of the compilation, which allows us to inspect the 
tasks that make up the pipelines. Compilation is lazy and therefore results in an Iterator that allows one to extract 
the pipelines as needed. We can nevertheless collect pipelines into a list for example as is done above with the 
conversion `c1.toList`.  In this example the pipe only produces a single pipeline - the size of the pipeline list is 
`1` as shown above. 

Once compiled we can execute the pipelines one at a time with the call `execDebug(c1)(Frame(testData: _*))`. As with 
compilation, lazy iterators are used to pull in the pipelines and execute them. Because these pipelines are independent, 
a user is free to execute them in parallel and in any order. One need only iterate through the pipelines functions and 
call them with the required input. The result is an iterator of either an execution error or the output of the pipeline, 
which in this case is a stream `Frame`. As with the compilation, the execution of the pipeline is tracked and recorded 
so that we can verify what was executed. 

### Enumerating tasks with `All`

Sometimes we want to test different versions of a task within the pipe's same step so we need to group those tasks as a 
set of nodes in the pipe. To do this we use the `All` construct that takes an iterator or sequence of tasks as its 
parameters. We can now generate several pipelines from a single pipe definition. Lets say we want to implement the 
following pipe:

<div class="mermaid">
graph LR;
  title[<u>Pipe Example 2</u>];
  
  subgraph All
  A
  B
  end
  
  subgraph All
  C
  D
  end
  
  A["concat(1, x)"] --> C["concat(3, z)"];
  B["concat(2, y)"] --> D["concat(4, z)"];
</div>

The example below shows how one can use the `All` construct and the `->:` operator to indicate the set of tasks that 
are execute in the same step of the pipe as described by the graph above (Pipe example 2). As with the example above we 
compile and execute the pipe. 

```tut:book

    val s3 = All(concat("1", "x"), concat("2", "y")) ->: All(concat("3", "z"), concat("4", "z"))

    // compile    
    val c3 = compile(s3)
    val c3l = c3.toList
    c3l.size
    
    // execute
    val r3 = execDebug(c3)(Frame(testData: _*))
    val r3l = r3.toList
    r3l.size
```

Notice now that the size of the list of compiled pipelines is the expected 2. We can also see that the first `Frame` has
the expected concatenation of the *1* and *3* in columns *x* and *y* respectively. It is important to point out that we 
see no output on the second pipeline because the **same** stream `Frame` we provided to both pipelines has 
already been depleted by the first pipeline. **It is important to remember that the `Frame` streams can only be consumed 
once**. Later on we will show you how to circumvent this issue by simply dynamically loading and instantiating the 
`Frame` for each pipeline when sourcing a file (or any other fixed data source). This however will not work for live 
data streams - one has to multiplex the data. 

All tasks of the first `All` construct will be paired with a task of the second `All` construct. This paring requires 
that the number of elements on the first step must be equal to the second one. If not, an error will be generated 
during run-time when the missing task is identified (detection is not perfect - if the right most `All` contains less
elements than the second one, an error will not be generated). This check cannot be performed during the compilation 
phase because the number of tasks may potentially be infinite. 
  
### Cross-product Operator `*:`

In order to combine the to sets of tasks (`concat("1", "x"), concat("2", "y")` and `concat("3", "z"), concat("4", "z")` 
the example above used the sequential `->:` operator. There are many cases were we want to combine the pipes' tasks in
order to test all possible combinations as is shown in the pipe example below:

<div class="mermaid">
graph LR;
  title[<u>Pipe Example 3</u>];
  
  L["load(data)"]
  A["concat(1, x)"]
  B["concat(2, y)"]
  C["concat(3, z)"]
  D["concat(4, z)"]
  
  subgraph Func
  L
  end
  
  subgraph All
  A
  B
  end
  
  subgraph All
  C
  D
  end
  
  L --> A
  L --> B
  A --> C
  A --> D
  B --> C
  B --> D
</div>

In this case the graph (Pipe *description*) consists of a total of 4 pipes (L &#x2A2F; {A,B} &#x2A2F; {C,D} ):

* L -> A -> C
* L -> A -> D
* L -> B -> C
* L -> B -> D

Unlike the example of the sequential operator the number of tasks within each `All`construct need not be the same. This
means that we can request sequences between a single task (L) and a set of tasks ({A,B}) or between sets of tasks of 
different sizes. As with the previous example we will use the `All` pipe construct to sequence the tasks but now use
the cross product operator `*:` combine these task groups. This operator also allows us to define and use a single 
*loading* function that is used by every pipeline, effectively allowing one to execute several experiments on the same 
data source. The example below show how this is done:

```tut:book
    val testData = List(
      "x" -> Seq("1", "1", "1"),
      "y" -> Seq("2", "2", "2"),
      "z" -> Seq("3", "3", "3")
    )
    
    // all *: all
    
    val s3 = load(testData) *: All(concat("1", "x"), concat("2", "y")) *: All(concat("3", "z"), concat("4", "z"))
    
    val c3 = compile(s3)
    val c3l = c3.toList
    c3l.size
    
    // execute
    val r3 = execDebug(c3)(empty)
    val r3l = r3.toList
    r3l.size
```

As expected we can see that both the number of compiled and executed pipelines is the expected 4. If you look at the 
compilation trace (variable `c3`) you will see that the first pipeline concatenates `1` to the Frame column `x` and 
`3` to the column `z`. The result (variable `r3`) of that first pipeline clearly shows that the concatenation tasks 
have been successfully executed. 

### Condition Run-time Checking with `Iff`

At times it is useful to control what combinations of tasks may or may not execute. We can check for the conditions
either at compile-time or run-time. At compile-time the input data is not available and therefore cannot be used to
control task execution. In this section we will look at how we can define a test to control the run-time execution 
of task. In this case, when each pipeline, generated by a Pipe description, is executed, a failed check will generate 
an error as output of that task in order to signal termination (non-execution of the task). Any subsequent tasks that 
make up the pipeline are not executed. Note that in such circumstances the initial tasks of the pipeline have 
effectively been wasted. 
 
Below is the definition of a task in the `Fiff` construct that checks if a task can be executed: 

```tut:book

  import pt.inescn.search.stream.OpCheck.Fiff

   /**
     * Check that we do not repeat the same function (name) applied
     * to the same columns (ColumnNames) irrespective of the parameters.
     */
  case object NoRepeats extends Fiff {
    override def apply[I,O](trk:Executing, i:I, tsk:Task[_,_]): Boolean = {
      // full chronological tracking
      val tmp: List[Task[_,_]] = trk.history
      // Get name and column ids
      val tmp1 = tmp.map(t => t.name -> t.params.filter{ case (_,ColumnNames(_)) => true ; case _ => false } )
      // Get rid of repeats
      val tmp2 = tmp1.toSet
      // Get the current task with column names
      val params = tsk.params.filter{ case (_, ColumnNames(_)) => true ; case _ => false }
      // Have we already execute such a call?
      val done = tmp2.contains((tsk.name,params))
      // If not, execute it
      !done
    }
  }

```

Every execution of pipeline tracks both the compilation and execution of the tasks. The `apply` above has the 
following parameters:
* The logs of the tasks executed in **(inverse) chronological order** until this point in pipeline
* The run-time data that will be passed onto the task thats to be be executed at this point
* The task that **will be executed** if the check function returns **true**

The function above takes the tracking (`history`) of tasks and then collects all of the `ColumnNames` passed on as
parameters to the already executed tasks. We then remove any duplicates of these column names by placing them into a 
`Set`. Next we identify all of the `ColumnNames` of the next task to execute. We then check if the same task using the 
same column name has been used. If so, return false (don't execute), otherwise return true (execute).

Once we have defined the function that does the check we need to set-up the Pipe description to indicate when the 
check will be performed on any given set of tasks. The code snipped below shows how this is done:

```tut:book
    val s3 = load(testData) *: All(concat("1x", "x"), concat("1y", "y")) *: Iff(NoRepeats, concat("2x", "x"), concat("2y", "y"))

    val c3l = compile(s3).toList
    // 4 sequential pipes - 2 x 2
    c3l.size

    // execute
    val r3l = execDebug(c3l.toIterable)(empty).toList
    // 4 partial function, ony 2 will execute to the end
    r3l.size
    
    // Get rid of errors/non-execution
    val r3r_traces = r3l.flatMap {
      case (Right(_), e) => List(e)
      case _ => List()
    }

```

We set-up a Pipe as we have done before. However now we use the `Iff` constructor to indicate that the `NoRepeat` 
check is applied to the tasks `concat("2x", "x")` and `concat("2y", "y")`. Note that the `Iff` construct can accept
any Pipe construct (will effectively process pipelines). This means that we can control the execution of complex 
pipelines consisting of several sequential tasks. In the case above we used a list of `Task`s directly, which are then 
automatically converted to a single `All` construct. It is important to note that any set of tasks in any Pipe step must 
have the same input and output types. **When the checked pipe consist of a complex pipeline, only the last tracked task 
is provided as input to the conditional execution checking function**.

### Condition Compile-time Checking with `When`

If we don't need the input data to make the decision of whether or not a task can execute, then we can make this 
decision during the compilation phase. As with the run-time example, the next code snippet shows he function used used
to check that the same task using the same parameters are use in the same pipeline:

```tut:book

  import pt.inescn.search.stream.OpCheck.Fwhen
  
  /**
    * Check that we do not repeat the same function (name) applied
    * to the same columns (ColumnNames) irrespective of the parameters.
    */
  case object NoRepeatsW extends Fwhen {
    override def apply(trk:Compiling): Boolean = {
      // full chronological tracking
      val tmp: List[Task[_,_]] = trk.history
      // Get name and column ids
      val tmp1 = tmp.map{ t =>
        val name = t.name
        val params = t.params.filter{ case (_, ColumnNames(_)) => true ; case _ => false }
        name -> params
      }
      // Get rid of repeats
      val tmp2 = tmp1.toSet
      // Use if no repeats
      tmp1.size == tmp2.size
    }
  }
```

Their is one very significant difference between the run-time and compile-time checks. During the runtime check, the 
task execution tracking (history) is collected as the pipeline executes, so when we perform the check we only have
a record of the past tasks. During compile-time check however when we perform the check we have information on 
all tasks compiled into the pipelines. This has two consequences: a) the conditional compilation check must consider all 
pipelines tasks and b) the conditional compilation check is assigned to a section of the Pipe and not to a Pipe's 
execution phase. 

The conditional compilation check above takes in a pipeline's compilation tracking. It the collects the all of the task 
names and their respective column names that will be executed in a given pipeline (from start to finish). We then remove 
all duplicates by generating a set of task names and their column names. We then check if the their are duplicates by 
comparing the size of the set with the size of the previously collected information. If we do find duplicates, then
this means that the pipeline will use the same task on the same column name more tha once. In this case we ignore this
pipeline and it will not be returned for execution. 

Once we have defined the function that does the check we need to set-up the Pipe description to indicate on what 
part of the Pipe this check will be performed. The code snipped below shows how this is done:

```tut:book
    val combinations = All(concat("1x", "x"), concat("1y", "y")) *: All(concat("2x", "x"), concat("2y", "y"))
    // make sure we do not repeatedly apply the same function to the same columns
    val s3 = load(testData) *: When(NoRepeatsW, combinations)

    val c3l = compile(s3).toList
    // 4 sequential pipes - 2 x 2, but we remove repeats so only 2
    c3l.size


    // execute
    val r3l = execDebug(c3.toIterable)(empty).toList
    // 4 partial function
    r3l.size

    val r3r_traces = r3l.flatMap {
      case (_, e) => List(e)
      case _ => List()
    }

```

We set-up a Pipe as we have done before. In this case we first define a section fo the final Pipe in a separate 
variable (`combinations`). We then construct the whole Pipe using this partial section of the Pipe, however now we 
use the `When` constructor to indicate that the `NoRepeatW` check is applied to all of the combination of tasks that
constitute the partial pipelines generated by the compilation of the `combinatons` Pipe. Note that the `When` 
construct can accept any Pipe. This means that we can control the compilation of complex pipelines consisting of 
several sequential tasks. In the case above we used the `All` construct and the `*:` operator to define a complex 
partial Pipe. It is important to note that any set of tasks in any Pipe step must have the same input and output 
types (Scala compilation will fail otherwise). 

### Aggregating Results of a Pipe

We have seen that a `Pipe` is compiled into a sequence of pipelines. Each pipelines consists of a list of `Task`s 
that execute sequentially. The output of one task is fed as input to the next task. We can take the resulting 
compilation, which is an iterator of pipelines, and execute anyone of these pipelines to obtain the final output. 
However, what we usually want to do is to execute all of these pipelines and collect all outputs and process these 
into a final result. The next section describe how we can create an **aggregator task** and use that, with the 
appropriate **aggregation operators**, to collect the output of each pipeline. The aggregator operators allow us
to control whether we want sequential or parallel execution of the pipelines. 

Before we describe the operators lets look at the definition of an aggregator task that will be used in the next 
examples. 

```tut:book
  object maxSum {


    type I = Val[Map[String, Double]]                 // Output of the pipes to aggregate: metric per column
    type Acc = Either[ADWError,(Executing,Val[_])]    // Either an error or maximum of a metric

    def maxSum(col: String, acc:Acc, i: Either[ADWError,I], trk: Executing): Acc = {
      (acc,i) match {
        case (Left(_), Right(vm)) =>
          // record latest as best
          val m = unpack[Map[String,Double]](vm)
          m.fold(err =>
            Right((trk,ErrVal(err))),
            { map: Map[String,Double] =>
              val v = map(col)
              Right((trk,Val(v)))
            })
        case (Right((_,os)), Right(vm)) =>
          // All is ok, select best score
          val m = unpack[Map[String,Double]](vm)
          val oldScore = unpack[Double](os)
          (oldScore, m) match  {
            case (Right(o), Right(map)) =>
              val v = map(col)
              if (v > o) Right((trk,Val(v))) else acc
            case (_, _) => acc
          }
        case (Right(_), Left(_)) =>
          // Ignore current failure, keep best so far
          acc
        case (Left(_), Left(e)) =>
          // Record latest failure (we could accumulate)
          Left(e)
      }
    }

    def apply(col: String): Agg[I,Acc] = {

      val tsk = new Aggregator[I,Acc] {
        override def name: String = "maxSum"
        override def params: Params = Map("col" -> col)
        override def zero: Acc = Left(ADWError("zero"))

        override def collect(acc: Acc, in: Either[ADWError,I], ta:Long, trk: Executing): Acc = {
          maxSum(col, acc, in, trk)
        }
      }

      Agg(tsk)
    }
  }

```

Here we create a task that assumes that we obtain a `Map[String, Double]` as output from each pipeline. This map 
holds the sum of several `Frame`'s columns, the column name being the key of the map. The aggregator's task is to 
identify and record what the maximum value for a given column of all the pipelines is. The aggregator will generate 
an `(Executing,Val[Double])` as its output. This value is referred to as the accumulator - it is passed on to the 
aggregation function whenever a pipeline generates a result. This value is then updated by the aggregator with the 
maximum and returned for comparison when the next pipeline terminates. 

Note that output of the aggregator will effectively be `Either[ADWError,(Executing,Val[_])]` because the input to
the aggregator (output of the pipeline) is an `Either[ADWError,Map[String, Double]]`. The `Either` is used to manage
errors. The aggregator must know what to do if a pipeline reports a failure. In this example we will simply ignore
these cases and keep checking for the maximum. 

The object `maxSum` is used to make it easier to define the aggregation task by:
* Declaring the expected output type of the pipelines: `type I = Val[Map[String, Double]]`
* Declaring the aggregator's accumulator: `type Acc = Either[ADWError,(Executing,Val[_])]`
* Defining the aggregation function: `maxSum(String, Acc, Either[ADWError,O], trk: Executing): Acc`
* Defining the constructor that creates an anonymous class and pack it into a Pipe `Func`: `apply(col: String): Agg[I,Acc]`

**A note on lazy evaluation**: in many of the `Pipe`s examples we use the `Frame` to read, process and store data in 
stream. Streams are potentially infinite so their evaluation is lazy - it is only when you access the stream's elements 
that the various operations on the stream are executed. In effect, when we declare a `Pipe` that uses `Frames`, 
pipeline execution simply constructs a sequence of `Frame` processing steps. When we execute each pipeline, we need a 
means of **activating the stream's computation before aggregation**. The last task of a pipeline must therefore kick off 
computation before the aggregation function is called. Aggregator are execute in a single synchronized thread, so if we 
activate a stream's processing there, then we effectively **kill parallel processing**. In the next Pipe example the 
function `sum("x")` already access all of the `Frame` rows and therefore executes all necessary computations. It is 
important to note however that the **aggregator must not activate pipeline computation** because it would not permit 
parallel execution (pipelines are executed in threads but the aggregator is execute in a single accumulator thread). 

Aggregator task creation has not been automated, so we need to define the task explicitly. The example above uses
an `apply` methods to construct an anonymous `Aggregator`. The apply also allows us to parametrize the tasks's 
execution. In this case the column name is the parameter. Any task should have the following defined to allow for good
task tracking and aggregation:
* The name of the task: `name`
* The parameters of the task: `params`

An aggregator task must define the following:
* The initial value of the accumulator: `zero`
* The accumulator function: `collect(acc: Acc, in: Either[ADWError,O], ta:Long, trk: Executing): Acc`

When we collect results from pipelines we are not only interested in the results of each pipeline but also on the 
pipelines themselves. For example, if we are evaluating performance, we may be interested in the pipeline's tasks
and their parameters. The aggregator function `collect` therefore provides the pipelines execution tracking. We
can use this to check what tasks were executed, the order in which they were executed and the parameters passed on 
to each task. The tracking information depends on the tasks `name` and `params` members. In the example above, we
simply return the racking information of the best performing pipeline. 

#### Sequentially Aggregating Results of a Pipe

The example below shows how we can construct a set of pipelines that are executed sequentially. We show how to set-up
the aggregation either explicitly using the `Aggregate` construct or implicitly via the `|:` operator.


```tut:book
    import scala.language.existentials

    val op = load(testData) *: toDouble("x", "y", "z") *: All(xDouble(1.0, "x"), xDouble(2.0, "x")) *: All(xDouble(3.0, "x"), xDouble(4.0, "x")) *: sum("x")
    val func = maxSum("x")

    // Example of adding verbose output to execution
    val p = Par(func, parallelism = 0, verbose = 1)(Par.scheduler)
    val s0 = Aggregate(op, p)
    
    // Debugging the operator |:
    val s1 = Aggregate(op, func)

    // compile new pipes
    val c1l = compile(s1).toList
    // should be 1
    c1l.size

    // execute the pipes (one result)
    val r1l = execDebug(c1l.toIterable)(empty).toList
    // should be 1
    r1l.size

    // get the result
    val r1r = r1l.head
    val Right((r1r_1, r1r_2)) = r1r._1.right.get
    // should be Val(24.0)
    r1r_2

    val s2 = op |: func

    // compile new pipes
    val c2l = compile(s2).toList
    // should be 1
    c2l.size    

    // execute the pipes (one result)
    val r2l = execDebug(c2l.toIterable)(empty).toList
    // should be 1
    r2l.size

    // get the result
    val r2r = r2l.head
    val Right((r2r_1, r2r_2)) = r2r._1.right.get
    // should be Val(24.0)
    r2r_2
```

In the example above we create a `Pipe` that results in a total of 4 pipelines. The last task `sum` takes a `Frame`
and sums each column storing the result in a `Map[String, Double]`. The last task effectively activates all of the
Frame's stream computations when it sums the columns. We then use the `s1 = Aggregate(op, func.t)` call to indicate 
that all of the `op`'s pipelines must be executed and the results accumulated by the `func` (contains the aggregator 
task). By default a `Par` is generated that indicates sequential execution. However we may want to change the 
`Par` default values. In this case we can instantiate and use the the `Par` (for example activating verbose standard 
console output) as is used in `s0 = Aggregate(op, p)`. We can the compile and execute this aggregation. Note that
we will only have one pipeline visible because only one (outermost) aggregating pipeline exists. 

Instead of explicitly using the `Aggregate` construct we can use the `|:` operator. The Pipe `s2` is equivalent to 
Pipe `s1`. In both cases compilation and execution produces the pipeline and aggregation result (maximum sum is 24).  


#### Aggregating Results of a Pipe in Parallel

The example below shows how we can construct a set of pipelines that are executed in parallel (threaded). We show 
how to set-up the aggregation either explicitly using the `Aggregate` construct or implicitly via the `||:` 
operator.

```tut:book

    import scala.language.existentials
    import scala.concurrent.duration.Duration

    val op = load(testData) *: toDouble("x", "y", "z") *: All(xDouble(1.0, "x"), xDouble(2.0, "x")) *: All(xDouble(3.0, "x"), xDouble(4.0, "x")) *: sum("x")
    val func = maxSum("x")

    val waitFor = Duration.Inf // 5000 millis
    val verbose = 0  // > 0 to print output
    val cores = Runtime.getRuntime.availableProcessors
    val scheduler = monix.execution.Scheduler.Implicits.global  // could be used implicitly

    val p1 = Par(func.t, cores, waitFor, verbose, scheduler)
    val s1 = Aggregate(op, p1)

    // compile new pipes
    val c1 = compile(s1)
    val c1l = c1.toList
    // aggregator takes the 4 result and returns 1
    c1l.size

    // execute the pipes (one result)
    val r1 = execDebug(c1)(empty)
    val r1l = r1.toList
    // should be 1
    r1l.size

    // get the result
    val r1r = r1l.head
    val Right((r1r_1, r1r_2)) = r1r._1.right.get
    // this is maximum metric value obtained
    // should be Val(24.0)
    r1r_2

    import monix.execution.Scheduler.Implicits.global  // used implicitly
    // Parallel execution: implicit scheduler
    val agg = Par(func, verbose = verbose, duration = waitFor, parallelism = cores)

    val s2 = op ||: agg

    // compile new pipes
    val c2: Iterable[Either[ADWError, Exec[Any, Either[ADWError, (Executing, Val[_])]]]] = compile(s2)
    val c2l = c2.toList
    // aggregator takes the 4 result and returns 1
    c2l.size

    // execute the pipes (one result)
    val r2: Iterable[(Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing)] =
      execDebug(c2)(empty)
    val r2l = r2.toList
    // should be 1
    r2l.size

    // get the result
    val r2r: (Either[ADWError, Either[ADWError, (Executing, Val[_])]], Executing) = r2l.head
    // this is maximum metric value obtained
    val Right((r2r_1, r2r_2)) = r2r._1.right.get
    // should be Val(24.0)
    r1r_2
```

We use the same pipe described in the previous example. The aggregation Pipe `s1` uses `Aggregate`. The first 2 
parameters indicate the Pipe which is to be executed and aggregated and the aggregation task respectively. However
the following addition parameters are used:

* The number of CPU cores to use for executing the aggregated pipelines (don't exceed twice the number of real CPU 
threads): `cores`
* The time the main thread will wait for the parallel execution of the aggregated pipelines: `'waitFor`
* Whether or not information on the execution of the aggregated pipelines is written to standard output: `verbose`
* The Java scheduler to be used when scheduling the OS's threads: `scheduler`

The example above sets these values explicitly. One can also access the companion `Par` object to get these 
default values. Once this is done, compilation and execution of `s1` proceeds as in the previous example generating 
the same results. In order to check for the parallel execution, set verbose parameter to a value greater than 0. 
The order of execution should not be deterministic. 

We can also set-up the same Pipe using the `||:` operator. In this case we must instantiate a `Par` with 
information on the aggregator task and the parameters for parallel execution (same as described in the explicit 
case). Note however that the scheduler need not be explicitly defined - it can be imported and used implicitly 
(see the Scala `import` just above the second `Aggregate`). We now combine the Pipe that generates the pipelines 
whose results must be aggregated with the aggregation task using the operator. The resulting Pipe `s2` compiles and 
executes the same pipelines with the same result.

### Evaluating the same pipeline several times using `Repeat`

We may need to execute the same pipeline several times. For example, if the pipeline represents a machine learning 
algorithm it may either be inherently stochastic or we may want to check it using the same data but ordered randomly
or injecting noise. In any of these cases we would like to evaluate the robustness of the algorithm by measuring for
example the expected accuracy and its deviation. To do this we can use a `Repea` construct. Lets assume we have 
the `Pipe` below that produces 4 pipelines. For each pipeline we collect the resulting `x` value of the last row of 
the stream `Frame` (think of this as a measure of some ML algorithm's accuracy). 

```tut:book
    val op: Op[Any, Row] = load(testData) *: toDouble("x", "y", "z") *: All(xDouble(1.0, "x"), xDouble(2.0, "x")) *: All(xDouble(3.0, "x"), xDouble(4.0, "x")) *: getLastRow("x")
```

The aim is to execute each of these pipelines 3 times. In order to aggregate and possibly summarize the results, we
first need to define an aggregator. The code below defines and instantiates an `Aggregator` that is wrapped into
an `Agg` Pipe operator. The `Agg`is used to parse and compose pipelines that use aggregation whether we are using
[standard aggregaton](#aggregating-results-of-a-pipe) or using the `Repeat` construct.

```tut:book
    def testRepeater(col: String): Agg[Row,Map[String,Double]] = {

      type Acc = Map[String,Double]

      val tsk = new Aggregator[Row,Acc] {
        override def name: String = "sumLastRowColumn"
        override def params: Params = Map("col" -> col)
        override def zero: Acc = Map()

        override def collect(acc: Acc, in: Either[ADWError,Row], ta:Long, trk: Executing): Acc = {
          val tmp = for {
            row <- in
            tmp <- unpackErr[Double](row(col))
            id = trk.toString
            sum = acc.get(id).fold(tmp)(v => v +  tmp)
            //_ = println(s"(id, sum) = ($id, $sum)")
          } yield acc.updated(id, sum)
          tmp.fold(_ => acc, m => m)
        }
      }

      Agg(tsk)
    }

    val rep = testRepeater("x")
```

The function above sums the output of the same pipeline and stores that in a `Map[String, Double]`. Note that the 
pipelines can be executed in any order so we use the `Executing` tracking information to identify the results of a 
specific pipeline. Once we have the aggregator ready for use we must generate a `Pipe` that specifies how many times
we want an inner pipe definition to be repeated. The code below shows how to use the `Repeat` construct to execute
the `op` pipe and pass on the results to the `rep` aggregator. Note that here we use the `Par` command to indicate 
that we want the pipes to be execute in ordered (as per the `Pipe` definition) fashion in a single thread.

```tut:book
    // Explicit set-up for sequential execution. parallelism <= 1
    val p1 = Par(rep, parallelism = 1, verbose = 0)(Par.scheduler)
    val s1 = Repeat(3, op, p1)

    // compile new pipes
    lazy val c1 = compile(s1)
    val c1l = c1.toList
    // repeat takes 1 and returns 1 that repeats 3 times
    c1l.size
    // should Right 
    c1.head 

    // execute the pipes (one result)
    lazy val r1 = execDebug(c1)(empty)
    val r1l = r1.toList
    // repeat takes 1 and returns 1 that repeats 3 times
    r1l.size 
    // should be Right(Map)
    r1l.head._1 
    
    // Map size should be 4
    r1l.head._1.right.get.size 
    
```

Once compiled we can execute the repeat pipe and collect 3 results: each result is the map containing the `x` value
of each of the 4 pipelines. Note that each of the 4 pipelines are different. By summing the values we can confirm 
that in fact 4 *copies* of each pipeline exists: the sum is 3 times the original `x`value. You can confirm this by 
verifying that the map has only 4 elements. Note that we can also set the parallel execution scheduler explicitly as 
is shown below:

```tut:book
    val s2 = Repeat(3, op, rep)
```

Usually we use the `Repeat` command to execute the pipelines in parallel. The code below executes the same
aggregator but now each `Pipe` set is executed 30 times. 

```tut:book
    // parallel execution
    val waitFor = Duration.Inf // 5000 millis
    val verbose = 0  // > 0 to print output
    val cores = Runtime.getRuntime.availableProcessors

    import monix.execution.Scheduler.Implicits.global  // used implicitly
    // Parallel execution
    // Implicit scheduler
    val reps = Par(rep, verbose = verbose, duration = waitFor, parallelism = cores)

    // Implicit set-up for parallel execution.
    // If you uncomment the print line in the collect method, you can see the execution is unordered
    val s3 = Repeat(30, op, reps)

    // compile new pipes
    val c3 = compile(s3)
    val c3l = c3.toList
    // repeat takes 1 and returns 1 
    c3l.size
    // should be Right 
    c3.head 

    // execute the pipes (one result)
    val r3 = execDebug(c3)(empty)
    val r3l = r3.toList
    // should be 1
    r3l.size 
    // should be Right
    r3l.head._1 
```

We can confirm that order of execution is not sequential if we uncomment the aggregator's `println` found in the 
`for` code block. The output will not be ordered as in the case of the previous example.  

Usually what we want to do is execute a `Repeat` followed by a standard aggregation. Say for example we have an ML
`Pipe` that have 4 versions of an algorithm (our `s3` from the previous example). We can record the results of 
each repeat - for example the expected accuracy and its standard deviation. The next step would be to select 
the best pipeline - find the pipeline that has the maximum accuracy and lowest standard deviation (the max sum 
of the `x` value of the `s3'example above).  

In order to do this we start off by defining a new aggregator the selects the best result of the `Repeat`'s output.
The code below shows how we record the result of the accumulated repeat - each accumulation of each repeated pipeline
is stored in a `Map[String,Double]`. The aggregator from `testAggregator` uses the `updateExpResults` function
to check if a previous result has already been recorded. If so it selects and records the best from that previous
record and the new result received. If not it simply records the first record as the current best result. 
 
```tut:book
    def updateExpResults(in: Map[String,Double], acc: Map[String,Double]): Map[String, Double] = {
      in.foldLeft(acc){ case (acum,(k,v)) =>
        val max = acum.get(k).fold( v )( d => Math.max(v,d) )
        acum.updated(k, max)
      }
    }

    def testAggregator(): Agg[Map[String,Double],Map[String,Double]] = {

      type Acc = Map[String,Double]

      val tsk = new Aggregator[Map[String,Double],Acc] {
        override def name: String = "maxLastRowColumn"
        override def params: Params = Map()
        override def zero: Acc = Map[String,Double]()

        override def collect(max: Acc, in: Either[ADWError,Map[String,Double]], ta:Long, trk: Executing): Acc = {
          val tmp = for {
            inMap <- in
            newMax = updateExpResults(inMap, max)
          } yield newMax
          tmp.right.get
        }
      }

      Agg(tsk)
    }
```

The code below shows how we chain a `Repeat` first followed by the standard `Aggregator`. We opted to show the 
use of an aggregator that executes the repeats in `s3` in parallel. It is advisable to use parallel execution if 
the the aggregated pipelines are time-consuming and require much CPU processing. 
 
```tut:book
    val aggx = Par(testAggregator(), verbose = verbose, duration = waitFor, parallelism = cores)
    val s4 = s3 ||: aggx

    // compile new pipes
    lazy val c4 = compile(s4)
    val c4l = c4.toList
    // agg takes single repeat(30), so 1
    c4l.size
    // should be Right 
    c4.head 

    // execute the pipes (collects all into one result)
    val r4 = execDebug(c4)(empty)
    val r4l = r4.toList
    // should be 1
    r4l.size 
    // should be Right
    r4l.head._1 

    // the original pipe generates 4 experiments, each was repeated 30 times
    // so we collect all 4 experimental results as summarized by the repeat
    // should be 4
    r4l.head._1.right.get.size 
```

In the example above all of the `s3` results are aggregated into a single output. One can see this from the size of
the resulting map. 

## Task Tracking

When a `Pipe` is compiled it generates several pipelines. Each pipeline is converted in a single partially evaluated
function that calls a sequence of other partially evaluated functions, each representing a `Task`. When each of these
partial functions are generated, either during compilation or executed when the pipeline is executed, the system 
generates records that tracks these tasks. The tracking information is stored in a `Tracking` object, whose 
simplified definition is shown below:

```tut:book
  trait Tracking {
    val history: List[Task[_,_]]

    def +(tsk: Task[_,_]): Tracking
    def drop: (Task[_,_], Tracking)
    def dropLast: (Task[_,_], List[Task[_,_]])
    def ++(trk: Tracking): Tracking
  }
```

Its only member is a list of `Task`s that were recorded during compilation or execution. The `history` member holds 
the compilation history in a recursive top-down order (same order as the `Pipe`'s declaration). The compilation 
tracking is stored in the specific `Compiling` class whose simplified definition is shown below:

```tut:book
  abstract class Compiling(history: List[Task[_,_]]) extends Tracking {}
```
The `history` member holds the execution history in inverse chronological order. The execution tracking is stored in 
the specific `Executing` class whose simplified definition is shown below:

```tut:book
  abstract class Executing(history: List[Task[_,_]]) extends Tracking {}
```

This tracking information can be used for debugging the compilation or execution of the `Pipe`s. It can also be used to 
perform [run-time control of task execution](#condition-run-time-checking-with-iff) or perform 
[conditional compilation](#condition-compile-time-checking-with-when) of the `Pipe`s. 

-----------------------------------------------------------------

[^1]: https://en.wikipedia.org/wiki/Directed_acyclic_graph
[^2]: http://unisonweb.org/2017-10-13/scala-world.html
[^3]: https://www.scala-lang.org/api/current/scala/Function1.html
[^4]: https://www.scala-lang.org/api/current/scala/collection/Iterable.html

