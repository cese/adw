---
layout: docs
title:  "Introduction"
---

# Introduction

ADW is an experimental platform used for:
 * Exploring and designing anomaly detection machine learning (ML) algorithms;
 * Testing the performance of the ML algorithms;
 * Selecting good hyper-parameter; 
 * Monitoring model performance;
 * Prototyping and deploying pipelines for real-case system evaluation

ADW also contains data sets and models of published work ([see publications](../publications.html){:target="_blank"}). 
If you use ADW in your work, cite one of the publications. 
 
This is a work in progress. If you find an errors or omissions please report it 
[here.](https://gitlab.com/cese/adw/issues){:target="_blank"}

## Project Layout

Before proceeding with the description of the workbench a few words on how the code and hence the packages are 
organized. The project itself consist of several sub-projects that include: 
* The **core** system; 
* A **macro** (meta-programming) sub-project used to automate code generation;
* The **docs** documentation (micro) site project. 
For a more detailed description of the SBT project layout please look at the information on 
[contributing](../contributing.html){:target="_blank"} to the project. Here we will focus solely on the core project. 

Each project follows the standard SBT directory structure. However, in addition to the SBT `project` directory that 
holds several n«build configuration files, ADW also contains a `data` directory with several large data sets. These data 
sets are used for testing, experiments and used to generate the published work results. Each project also includes a set 
of test suites using [ScalaTest](http://www.scalatest.org/){:target="_blank"}. 

The `core` project is split into:
* **Library source code**: the source is primarily written in Scala (`src/main/scala`), but we also have Java code
(`src/main/scala`, usually used to interface Java libraries), Matlab, Octave and R (respectively in `matlab`, `ocatve`
and `R` in the `src/main/` directory). The last three directories contain mostly code that is used to generate the gold 
standards for tests and reference implementations used for testing and generating results for published work. 
* **Basic test source code**: every Scala object that warrants it will have a corresponding test suite. All basic unit
tests are found in the `src/test` directory. The same non-test source code structure and package names are used in this 
test directory. The filenames correspond to the same filenames in the source directory but are appended with the letters
`Spec` (which stand for specification). For example the `pt.inescn.etl.stream.DataFiles` file in `src/main/scala` has 
its tests in the `pt.inescn.etl.stream.DataFilesSpec` file in the `src/test/scala` directory.
* **Long running ML test source code**: This directory also has Scala test suites using the same corresponding source 
directory names. However the test suite filenames do not have a corresponding source code filenames (without the `Spec`
ending of course). These tests are not executed automatically in the automated continuous integration (CI) process. 
These tests are reserved for long running process used for example to test ML algorithm performance. 

# Core Packages

The core library is divided into the following packages:

1. `dsp`: digital signal processing routines. This includes for example filtering, envelope detection and the Fourier 
and Hilbert transforms. Several java libraries provide the required underlying functionality. 
1. `etl`: contains the code for the extraction, transformation and loading of data. 
1. `eval`: contains the routines used to generate the performance metrics of the ML algorithms. This includes binary
classification and regression metrics.
1. `features`: contains code to generate features from data. This includes for example descriptive statistics and the 
Linear Frequency Cepstral Coefficients (LFCC).
1. `plot`: contains utility routines for plotting for example data from the Fourier and the Short Time Fourier 
transforms.
1. `samplers`: contain functions to create samples of values. These can be simple enumerations, random values of a 
specific distribution or standard functions. All these samples are modeled as lazy, possibly infinite, streams. In 
addition to this one has several operators that used to combine these streams. This includes cartesian combinations, 
appending streams and mapping streams with numerical functions. These streams can be used, for example, to synthesize 
test data or generate parameters for the ML algorithms.
1. `search`: contains the code that allows one to set-up ML pipelines so that they can be executed and their 
performance evaluated. These pipelines can also be setup to perform simple hyper-parameter searching and configured to
execute concurrently. 
1. 'util': utility classes and objects used through the core.

# Core Search

The search package contains the following main components:

* **Tasks**: are functions that consume input and generate output. Tasks are the basic elements of a ML pipeline. They 
have an identifier (name) and their execution is tracked and recorded with this identifier. Such tracking can be used 
for example to debug pipe executions or record the top performing ML models. Tasks can be used in two modes: generating 
and transforming or aggregating data. Tasks can be strung together to form a pipeline using a small set of well defined 
pipe operators. 
* **Pipes**: a pipe is a description used to generate (*compile* into) one or more **pipelines** using a simple Scala 
based domain specific language (DSL). This DSL consists of several operators that describe how one can combine tasks to 
form several pipelines. A pipeline consists of a linear sequence of tasks that are executed sequentially. The input of 
the pipeline is the input of the pipeline's first task. The output of one task is passed on to the input of the next 
task. The output of the final task is the output of the pipeline. The set of generated pipelines can be executed 
sequentially or concurrently. The results of each pipeline can be aggregated. The execution of each task in a pipeline 
is tracked and recorded. Pipes are *composable*. The DSL also includes operators that allow such *composability*. 
* **Operator Checks**: are specialized functions that can be used within the to pipe definition that allow for the 
conditional execution of tasks either statically (during compilation) or dynamically (during run-time).  

In the following sections we will describe and exemplify the use of the components enumerated above. These section also 
serve as a tutorial for those starting to use the platform.



