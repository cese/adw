---
layout: docs
title:  "PRODUTECH"
section: "res"
---

# PRODUTECH Technical Notes

This section contains developer and user notes for the PRODUTECH demonstrator (Predictive Maintenance Analytics). 
It contains information on setting up and using the ADW demonstrator code. This is **not** part of the ADW and
will be removed when the project terminates.   

## Dynamic Pipes Module

A Web-based front-end will provide a GUI that allows users to set-up and execute machine learning pipelines. The 
pipelines are created dynamically. The pipelines here are intended for deployment and execution purposes. ADW 
however is used for generating many pipelines for testing and hyper-parameter tuning only. we have therefore 
created:

* A set of dynamic type-encoding (Java)[^1] task wrappers were developed that allow the statically typed ADW 
(Scala) tasks to be used dynamically;
* A very simple Java API[^2] was developed that allows one to dynamically compose pipelines. This includes 
methods to check if the parameters and input/outputs between two tasks are compatible;
* An example pipeline is provided in Java code[^3] that can be used as a basis for further development.

Note that the methods parameter types are encoded manually so run-time errors may occur if these are incorrect. 
Also note that a developer is free to develop additional tasks. One need only extend the `STask` interface to 
include the execution code and type encoding. It is important to note that this is a Scala interface that was
designed to facilitate integration between the two languages. In principle it is also possible for Java code to 
extend this interface (trait), so Java developers may also implement their own tasks in Java. 

We provide an example of how to construct and use an ADW pipeline dynamically. We include data files and 
utilities to send data and the then to receive and process this data via a message broker. More details on this 
are provided in the next sections. As with the ADW Tasks, these examples also use the pipes to construct lazy 
streams that are made available via the [Frame](docs/frames.html) API. Tasks take a Frame as an input and return a
new Frame that processes the data. The Frame's streams (iterators) are lazy because the underlying operations on 
the Frame are only execute when the Frame's data is consumed by the pipeline's last task. 

Developers, nevertheless, are free to develop tasks that consume any type of input an produce any other type of 
output other than Frames. For example some ADW pipelines aggregate machine learning performance metrics that are 
used to compare and select the best model. The developer is responsible for casting the inputs and outputs to the 
correct type. 

All task parameters are passed in as Strings (some using the JSON format). Each Task must provide a function 
for each parameter that converts the string to the intended type. This is by design: we want to be able to see
and manipulate this information in the web-based GUI. Developers are free to use any format they see fit. Text 
formatting may also be employed to facilitate user interaction. 

The Java pipelines can only execute a single sequence of tasks (no graphs allowed). 

## Getting Started

### Getting the Files

In order to be able to compile and execute the example we need to copy the required files to a working directory
of your choice. These files include: 

* A library (Jar file) with the compile Java Virtual Machine (JVM) code;
* The data files used for simulating the sensor data;
* The message broker configuration file with information used to connect to the server;
* Java source code with the example pipeline.

We now provide instructions on how to get these files in the appropriate directory. We will use the base 
directory `~/Desktop/produtech/demo/demo_prep_critcal/` but any one will do. We assume the project have been 
cloned to the directory `~/IdeaProjects/adw`. Instructions are based on the Linux command line. For windows use 
the corresponding commands and for the Java paths keep using the forward slash, but use the semi-colon ";" as a 
path separator instead of the colon ":".

The full code, including dependent libraries are provided via a [fat Jar](../installation.html#using-the-fat-jar).
If you are using the source code you can clone, compile and generate the single file library via SBT[^4] using the 
following command:
 
```sbtshell
assembly
```

The file is the placed in the target directory. Copy the library to the base working directory so:

```
mkdir ~/Desktop/produtech/demo/demo_prep_critcal
user@machine:~/IdeaProjects/adw$ cp ./core/target/scala-2.12/core.jar ~/Desktop/produtech/demo/demo_prep_critcal/
```

We now copy the files that contain sensor data that will be used for testing. Two sets are copied - one that 
contains a single file for faster testing, and another that contains a full set of experiments that can be used
to test the ML pipeline. This directory contains a single file:

```
mkdir -p ~/Desktop/produtech/demo/demo_prep_critcal/data/inegi/ensaios_rolamentos_2
user@machine:~/IdeaProjects/adw$ cp data/inegi/ensaios_rolamentos_2/rol1_1000_cmdn_ens1.csv ~/Desktop/produtech/demo/demo_prep_critcal/data/inegi/ensaios_rolamentos_2
```

This directory contains all the files of an experiment[^5]:

```
mkdir -p ~/Desktop/produtech/demo/demo_prep_critcal/data/inegi/ensaios_rolamentos_3
user@machine:~/IdeaProjects/adw$ cp data/inegi/ensaios_rolamentos_3/*.csv ~/Desktop/produtech/demo/demo_prep_critcal/data/inegi/ensaios_rolamentos_3
```

The example source code consists of the Java source file and the message broker configuration files. You can find
then in the ADW source code under the PRODUTECH package. Here is a listing:

```
user@machine:~/IdeaProjects/adw$ ls -l core/src/main/java/produtech/
total 48
-rw-rw-r-- 1 user user 39630 mar 18 11:40 DynamicInvoke.java
-rw-rw-r-- 1 user user   127 mar 18 15:10 ReceiveConfig
-rw-rw-r-- 1 user user   131 mar 18 15:10 SendConfig
```

Copy these to the base directory. Note that the source ,ust be placed in the appropriate directory in order to 
compile and execute correctly.

```
mkdir ~/Desktop/produtech/demo/demo_prep_critcal/produtech
user@machine:~/IdeaProjects/adw$ cp ./core/src/main/java/produtech/* ~/Desktop/produtech/demo/demo_prep_critcal/produtech
```

If you change the directory be sure to change the package name also. In the end you should have the following 
set-up: 

```
user@machine:~/Desktop/produtech/demo/demo_prep_critcal$ ls -lh ~/Desktop/produtech/demo/demo_prep_critcal/**
-rw-r--r-- 1 user user 100M mar 18 17:03 /home/user/Desktop/produtech/demo/demo_prep_critcal/core.jar

/home/user/Desktop/produtech/demo/demo_prep_critcal/data:
total 4,0K
drwxr-xr-x 4 user user 4,0K mar 18 17:02 inegi

/home/user/Desktop/produtech/demo/demo_prep_critcal/produtech:
total 64K
-rw-r--r-- 1 user user 13K mar 18 16:57 MyDynamicInvoke.class
-rw-r--r-- 1 user user 39K mar 18 16:34 MyDynamicInvoke.java
-rw-r--r-- 1 user user 127 mar 18 16:27 ReceiveConfig
-rw-r--r-- 1 user user 131 mar 18 16:27 SendConfig
```

### Compiling the Examples

Before we can compile the code we need to change the class name so that is does not clash with the existing 
compiled class that is already present in the library (fat jar). So we first change the file name because 
Java requires that the class name and file name be the same: rename `DynamicInvoke.java` to 
`MyDynamicInvoke.java`.

Next we rename the original class: 

```java
public class DynamicInvoke {
```

to the new name:

```java
public class MyDynamicInvoke {
```

We now change the instantiation of the class:

```java
    public static void main(String[] args) {

        DynamicInvoke di = new DynamicInvoke();
```

to use the new class we are going to compile:

```java
    public static void main(String[] args) {

        MyDynamicInvoke di = new MyDynamicInvoke();
```

otherwise we would be using the library's example code and not the code we just edited. We must also indicate 
the location were the configuration files are found. We first change the original path of the send application 
from this:

```
    String sfilename = "core/src/main/java/produtech/SendConfig";
```

to this:

```
    String sfilename = "produtech/SendConfig";
```

We also change the original path of the receive application from this:

```
    String rfilename = "core/src/main/java/produtech/ReceiveConfig";
```

to this:

```
cd ~/Desktop/produtech/demo/demo_prep_critcal/
```

Those files have configurations for the use of a RabbitMQ sever and specific queue that can be used for testing 
and demonstration purposes. 

### Running the Example

We are now ready to compile the Java source code. Any IDE can be used for this purpose but we provide command 
line to do this in order for you to get started quickly: 

```
user@achine:~/Desktop/produtech/demo/demo_prep_critcal$ javac produtech/MyDynamicInvoke.java -cp core.jar 
```

If compilation went well then an equivalent filename with the extension `class` should have been generated. It
is the compiled class we will now execute. To do this we must indicate to the JVM were to look for the compiled
code. The `cp` flag points to both the fat jar ("core.jar") and the current directory (`.`) 

```
user@achine:~/Desktop/produtech/demo/demo_prep_critcal$ java -cp core.jar:. produtech.MyDynamicInvoke
```

The code includes basic examples of constructing pipelines and examples of constructing the demonstrator 
pipeline. When necessary, we first create and execute a *send* task that sends the simulation or test data to 
a queue in the RabbitMQ message broker. We have taken care to send and consume the exact number of records to
leave the queue empty. Note that if the queue does not have enough data, the pipeline will wait for that data. 

In order to view the queue and its messages use the RabbitMQ web-interface using a link of this form: 

http://111.222.333.444:15672/#/queues

For details on the host address and user credentials, look at the configuration files. **Make sure you only
manipulate your own queue and no one else's.**

### Helper Utilities

We also provide some utilities that allow you to send and receive data to and from the message broker's queue. 
This may be useful when testing your own code. If you have cloned the ADW source code and have SBT installed 
then you can send data so:

```sbtshell
root/runMain produtech.SendApp -verbose 1 -bufferSize 2 -machineID lol -sensorID sens1 -numMessages 10 -dir data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv
```

and receive data so:

```sbtshell
root/runMain produtech.ReceiveApp -verbose 1 -iter CallBack -waitMultiplier 2 -numMessages 2
```

If you cannot compile and execute the code via SBT, you need to set-up up the configuration files first. Here
is an example of the command lines you need in order to place the configuration files in the appropriate 
directory:

```
user@machine:~/Desktop/produtech/demo/demo_prep_critcal$ mkdir -p core/src/main/java/produtech/
user@user:~/Desktop/produtech/demo/demo_prep_critcal$ cp produtech/ReceiveConfig core/src/main/java/produtech/
user@user:~/Desktop/produtech/demo/demo_prep_critcal$ cp produtech/SendConfig core/src/main/java/produtech/
```

And now you can send the data so:

```
java -cp core.jar:. produtech.SendApp -verbose 1 -bufferSize 2 -machineID lol -sensorID sens1 -numMessages 10 -dir data/inegi/ensaios_rolamentos_3/rol1_rpm1000_hp0_b_mm0_exp1.csv
```

and receive data so:

```
java -cp core.jar:. produtech.ReceiveApp -verbose 1 -iter CallBack -waitMultiplier 2 -numMessages 2
```

For more information on the application's command-line parameters use the `-help` flag and look at the  
source code. 

-------------------------------------------------------

[^1]: [Java Dynamic Tasks](../api/produtech/STasks$.html)
[^2]: [Dynamic Java Pipelines API](../api/produtech/DynamicCompile$.html)
[^3]: [Dynamic Java Pipeline example](../api/produtech/DynamicInvoke.html)
[^4]: [Notes on using SBT](../docs/devnotes/introduction.html)
[^5]: This data was graciously donated by [INEG](http://www.inegi.pt/)