---
layout: docs
title:  "Introduction"
section: "intro"
---

# Frames

One of the common problems with ML algorithms is accessing data from various sources that are made available in 
different formats. Ideally we would have a single interface that allows us to ingest and process data of any type. In 
order to facilitate data processing and the experimental set-up of streaming data, we have defined a `Frame` 
interface that provides a set of common useful operations. We also provide a number of instances of this interface
that allow us to load data from various sources (source code, files, remote servers). Once instantiated, all of
the construction and use of the ML pipeline remains unchanged. In this section we will describe the `Frame` and 
related classes such as `Row` and how they are used. 

## Underlying Concepts of Frame Processing

Stream data is by definition infinite. In order to represent a sequence of operations on these streams, `Frame` 
uses **lazy evaluation** - we define operations to be applied to a stream of data but only execute these operations
when we effectively consume the data. `Frame` has therefore a set of methods that allow one to delay the application 
of functions to its data. Applying a function to a frame always returns a new lazy frame. We can compose these 
functions in sequential fashion transforming one frame into another. Theses sequences can have an arbitrary number of
operations. When we consume the data from the last frame, all operations will be executed in the order they were 
defined. Consuming data means for example iterating through the data and visualizing it.

Stream data is **ephemeral**, so once you have consumed data from a stream, it cannot be consumed again. Assume we 
have created a sequence of Frames `f1`, `f2` and `f3` were each frame applies an additional transformation to the 
previous frame. In this case if we consume data from `f1`, it will not be available to either `f2` or `f3`. Care
must be taken when we want to consume the same data repeatedly, for example during unit testing. In this case we have 
to instantiate a *data loader* `Frame` that can load data repeatedly (for example from a file).

Because data is ephemeral, `Frame` assumes data is consumed by a single client. This means that Frames are **not 
thread safe**. If you want to use multiple consumers you will have to use some external mechanism to do the 
multiplexing of data for you. For example consume data from a message broker.  

Data can only be consumed once, which means that all functions in a `Frame` either:
* Transform a sample (map, filter, etc.)
* Aggregate samples (for example sliding window)
* Ignore or select certain elements of a sample
We model each sample as a record which we will refer to it as a `Row`. All these operations are synchronous. So 
`Frame` only provide **synchronous processing**. This together with single thread processing allows us chain operations 
in a lazy fashion. These operations may represent data conversion, transformation and processing tasks. A complete 
streaming ML pipeline can be constructed using `Frame`s. A ML algorithm can consist of a set operations on a data 
stream and may include for example model learning and prediction.   
 
## Frames versus Pipes

We have seen that [Pipes](pipes.html) also allow us to construct pipelines of tasks that can be used for data 
processing. `Frame`s can do the same thing and so seem to be redundant. However, when we define a sequence of 
operations via `Frame`s, we effectively create a single linear pipeline of actions. Pipes on the other hand allow us 
to creates sets of pipelines of sequential tasks, each of which can then be executed in parallel. Usually we define 
Pipes that use Frames to construct and test several pipelines whose performance are to be tested. In effect, the 
Pipe's tasks constructs the Frame pipeline and then execute it. This is exemplified when we can use the Pipe 
constructs to perform parameter [searching](search.html#pipes-with-parameters). In addition to this, `Frame`s
apply anonymous functions are anonymous. However a Pipe's tasks are given names and their execution is tracked 
and recorded.

The following sections will describe how a `Val` wrapper is used to hold values of different types representing 
for example sensor data. How these data are *packed* into `Row`s so that that they can be processed as streams of
rows, And finally how we can use the `Frame` to read, process and save these `Row`

## Storing values of different types

Before using the `Frame` API, we need to load its definitions so:
 
```tut:book
import pt.inescn.etl.stream.Load._
```

When we read data we can read data in various formats representing several data types. In order to represent the 
various data in  a single data structure, we need wrap these into a `Val`. This interface allows us to not only store 
the value but also record its type. It is important to note that compile-time type information is thus lost. So we 
may, by requesting an invalid type to be read, cause run-time errors (that raise exception, more on this later). The 
example below shows how we can declare and read the values contained within a `Val`. 

```tut:book
    // should be a Val[String]
    val r0: Val[String] = "100"
    // should be a String
    r0.v 
    
    // should be Val[Array[Double]]
    val r2: Val[Array[Double]] = Array(300.0, 301, 302)
    // should be Array[Double]
    r2.v 

    // We can generate vector versions automatically
    // Note that we need to type the Val, but we need not add all typing information
    val r3: Val[_] = r0.arr(5)
    // should be Array[String]
    r3.v 

    // If don't add the Val explicilty, it will still be used implicitly
    val r4: Array[String] = r0.arr(5)
    // should be Array[String]
    r4.v     
```

In the example above, an implicit Scala conversion is performed when we declare the value type (in other words we do
no need to explicitly instantiate a Val). Also note that we need not always declare the specific type (the `_` in
the `Val[_]` indicates that any type will do), for example when we print out the value.
  
Beside an implicit conversion when instantiating values, we also provide implicit conversions that let us read a value.
In the code below the implicit conversion is automatically performed if we declare the return type to be an `Either`. 
If conversion is possible, we obtain a `Right(value)`. If however an exception kis raised, it is caught and converted
into a `Left(ADWError(message))`  were message holds a n error message. 

```tut:book
    val v1: Val[String] = "100.0"
    val v2: Val[Double] = 200.0
    val v3: Val[Int] = 300

    val s1: Either[String, String] = v1
    val s2: Either[String, Double] = v2
    val s3: Either[String, Int] = v3

    // should be 'right
    val r: Either[String, Double] = for {
      t <- s1
      t1 = t.toDouble
      t2 <- s2
      t3 <- s3
      ts = t1 + t2 + t3
    } yield ts
    // should be 600.0
    r.right.get 
```

The use of the `Either` Monad allows us to use the `flatMap` operation to read and process data. The code above shows
how the `flatMap` syntactic sugar of the `for` is used to read 3 values add them and return the total as a 
`Right(Val[Double])`. If any error occurs within the `for` then the `yield` will return a `Left(ADWError(_))`. This
provides us with a shorthand to succinctly express the read and write operations on `Val`s. 

We can also read and write values explicitly. In the example below we store a `Double` in `Val` an then read its value
using the `pack` and `unpack` functions. 

```tut:book
 val r1 = pack(0.0)
 val d1 = unpack[Double](0.0)
```

**Important Note:** all the above functions may result in a run-time error. Some of these may be in the form of an
exception and others may represented as a `Left(ADWError(_))`. However these run-time errors manifest themselves in
unexpected ways. In the example below we instantiate a `String` but request that its value be read incorrectly as an 
`Integer` or even as a `(Int,String)` tuple. If you rin this example, no run-time error occurs.

```tut:book
 val r1 = pack("invalid")
 val d2 = unpack[String](r1) 
 val d3 = unpack[Int](r1)
 val d4 = unpack[(Int,String)](r1)   
```

It is only when you access the information that this occurs. So in the example below an excption will be raised: 

```tut:book:fail
 val d5 = d3.right.get + 1 
```

## Representing Sample as `Row`

Each sample of a data stream may contain more than one value. This sample record is represented as a `Row`. These rows
are data dictionaries that index each value with a name (`String`). The example below shows how one can explicitly 
create several values of different types and store them in a `Row`: 

```tut:book
    // Initialize with full typing
    val r0: Val[String] = "100"
    val r1: Val[Double] = 200.0
    val r2: Val[_] = "300"
    val r4: Val[_] = "400"
    val r5: Val[_] = 400

    // Store and check that typing is still visible
    val row1 = Row("r0" -> r0, "r1" -> r1, "r2" -> r2)
    // should be 3
    row1.size 
    // should contain ("r0", "r1", "r2")
    row1.v.colNames
    // should contain Map("r0" -> java.lang.String, "r1" -> java.lang.Double, "r2" -> java.lang.String) 
    row1.colTypes 
```

Note that in the above example we need not declare all value types explicitly. A row has several methods that allow
us to query and manipulate its values. We can for example find out he `size` of the row (number of fields) and 
what each field's types are (`colTypes`). Note that runtime types may be unknown due to type erase. This occurs 
for example with Java's arrays. 

We can apply functions with a single parameter to a row field and replace that field with another value. In the 
example below we have an anonymous function, which is applied to the `Row` `row1`, and takes a `String` parameter `e` 
and converts it to a `Double`. The `row1` parameters `"r0"` and `"r2"` are the names of the fields to which this 
function is applied. 

```tut:book
    val row3 = row1((e: String) => e.toDouble, "r0", "r2")
    // should contain Map("r0" -> java.lang.Double, "r1" -> java.lang.Double, "r2" -> java.lang.Double) 
    row3.colTypes 
```
We can see that after its application, the fields that were originally `String`s are transformed `Double`s. Note that 
when the function is applied, the `unpack` and `pack` functions are implicitly used for the conversions. 

The next example shows the same functionality as the example above, however this time the result does not override 
the field value but a new one is created. To do this we use a tuple `"a" -> "b"` to indicate that the new field value
generated from the original `"a"` is now stored in a new field with the name `"b"`. 

```tut:book
    val row4 = row1((e: String) => e.toDouble, "r0" -> "r0d", "r2" -> "r2d")
    // should contain Map("r0" -> String, "r0d" -> java.lang.Double, "r1" -> java.lang.Double, "r2" -> String, "r2d" -> java.lang.Double) 
    row4.colTypes 
```

The next function converts the `Row`'s fields into an array. Note that by default if not column names are given, all
of the fields are used. Note however that in this case the order of the values is not guaranteed. If you need the 
values in a specific order, name the columns explicitly.  

```tut:book
    // should contain Array(300.0, 200.0, 100.0)
    val row3Arr = row3.arr((e: Double) => e)
    val row4Arr = row3.arr((e: Double) => e, "r0", "r1", "r2")
```

We can also apply functions with 2 parameters, each is assigned a value of a `Row` field. This means that we must
indicate bold field names used as arguments to the function. In this example we need not indicate were to place the
result. So a third field name must be provided. This means that such an apply function will need a triplet for
each apply: first existing field name for the first argument, second existing field name for the second argument and 
a third field name for the result value. The example below shows how to apply a two argument function to 2 sets of
fields. 

```tut:book
    val row5 = row3((a: Double, b: Double) => a + b, ("r0", "r1", "r0_1"), ("r0", "r2", "r0_2"))
```

Note that we need not create new fields - if the third field name already exists, then the original value will be 
overwritten. The order in which the apply function is used is not guaranteed so the user should not assume that 
when creating a new field, it will be available for the next set of arguments. 

The function below shows hwo we can transform two rows into a single one (again, not ordering of the fields is 
guaranteed):

```tut:book
    val row2 = Row("r4" -> r4, "r7" -> r5)
    val row6 = row2 ++ row4
```

We can also update a value of a row field directly without applying a single argument function. The example below 
shows how the `update` method can be used to replace field's value. 

```tut:book
    val nr0: Val[_] = "0"
    val row7 = row6.updated("r0", nr0)
    // should be nr0
    row7("r0") 
```

We can also add a field so:

```tut:book
    val row8 = row7 - "r0"
    // should be empty
    row8.get("r0") 
```

or remove a field so from a `Row`:

```tut:book
    val row9 = row8 + ("r0" -> r0)
    // should be Some(r0)
    row9.get("r0") 
```

Sometimes we don't need to retain and use all of the row's fields. We can `project` only a subset of the row's fields
and then use those in subsequent functions. The example below shows how we can select such a subset of fields:

```tut:book
    val row10 = row9.project("r0", "r1", "r2").apply((e: String) => e.toDouble, "r0", "r2")
```

One may also fold over a Row's fields in order to aggregate across a record. The example below shows how we can
sum all values in a single row: 

```tut:book
    val SUM = "sum"
    val acc = Row(SUM -> pack(0.0))
    // Should be 600
    val row11 = row10.foldLeft(acc) { case (accRow, (k, v)) =>
      val t = applyArgs((a1: Double, a2: Double) => a1 + a2, accRow(SUM), v)
      Row(SUM -> t)
    }
```

The aggregation assumes that he result will also be a row. So in the case above the accumulator that is initiated 
with a value of 0 is placed in a `Row` with the field name `"sum"`. The method `foldLeft` accepts a functions that
takes in he accumulator `accRow` and the key-value pair `(k,v)` representing each of the row's fields. Here we simply 
ignore the names of the fields in `k` and keep adding the values `v`. 

## Instantiating Test Frames

We will now look at the `Frame` API. Before we can use this API w need to instantiate the initial Frame hat sources
the data. For testing purposes we provide a simple Frame constructor that accepts a set of sequences (including lists) 
or iterators, each representing a stream's field. In the two examples below we have a source for three fields `"x"`, 
`"y"` and `"z"`. Once the frames have been instantiated we can use the API's methods.

```tut:book
    val ix = Iterable("1", "1", "1", "1", "1", "3", "3", "3", "3", "3", "5", "5", "5", "5", "5", "1", "1", "1", "1", "1").toIterator
    val iy = Iterable("11", "11", "11", "11", "11", "13", "13", "13", "13", "13", "15", "15", "15", "15", "15", "11", "11", "11", "11", "11").toIterator
    val iz = Iterable("21", "21", "21", "21", "21", "23", "23", "23", "23", "23", "25", "25", "25", "25", "25", "21", "21", "21", "21", "21").toIterator
    val colx1 = ("x", ix)
    val coly1 = ("y", iy)
    val colz1 = ("z", iz)
    
    val cols1 = Frame(colx1, coly1, colz1)
    // should be 3
    cols1.colSize 
    // should contain ("x", "y", "z")
    cols1.colIDs 
    // should contain ("x", "y", "z")
    cols1.v.colNames
    // should contain Map( "x" -> String, "y" -> String, "z" -> String) 
    cols1.colTypes 
```

In both these examples (above and below) once we have the `Frame` we can use several methods to query about the type
of the content. Specifically we can find out hwo many fields exists (`colSize`), what those fields' names are 
(`colIDs` or `colNames`) and the data type of each field (`colTypes`).

```tut:book
    val colx1 = ("x", Seq("1", "1", "1", "1", "1", "3", "3", "3", "3", "3", "5", "5", "5", "5", "5", "1", "1", "1", "1", "1"))
    val coly1 = ("y", Seq("11", "11", "11", "11", "11", "13", "13", "13", "13", "13", "15", "15", "15", "15", "15", "11", "11", "11", "11", "11"))
    val colz1 = ("z", Seq("21", "21", "21", "21", "21", "23", "23", "23", "23", "23", "25", "25", "25", "25", "25", "21", "21", "21", "21", "21"))
    
    val cols1 = Frame(colx1, coly1, colz1)
    // should be 3
    cols1.colSize 
    // should contain ("x", "y", "z")
    cols1.colIDs 
    // should contain ("x", "y", "z")
    cols1.v.colNames
    // should contain Map( "x" -> String, "y" -> String, "z" -> String) 
    cols1.colTypes 
```

## Basic Frame Operations

We now look at the API of the `Frame`. The first allows us to ignore a number of records from the stream using the
`drop` method. In the example below we eliminate the first 5 samples from the data stream. 

```tut:book
    val cols2 = Frame(colx1, coly1, colz1).drop(5)
    val read2: IndexedSeq[Row] = cols2.iterable.iterator.toIndexedSeq
    // should be 15
    read2.size 
```

Note that the `Frame`s process streams, which could in principle be infinite. However we created a Frame from a 
finite data source so it is possible to use `size` or `length` to count how many elements are in the underlying 
`Iterator`. However, care must be taken when we do this (for example extract only a fixed number of samples using 
`take`). Note that once we have used the underlying `Iterator` the stream may be empty (for example when using 
`size`). In this case for testing and demonstration purposes we convert the data to `IndexedSeq` to make a 
copy of the data. **However for infinite streams do not do this.**

**Important Note:** The `Frame` accumulates operations and must apply these before making the new transformed 
samples available. Samples (Rows) are only made available via Scala's `Iterator`. This iterator mus always be 
accessed via the `iterable` method to ensure that all pending operations are applied first. 

The example below shows how we can generate sliding windows of the data using the `window`method. The first 
parameter is the size of the window and the second parameter is the number the step size. 

```tut:book
    val conv0 = Frame(colx1, coly1, colz1).window(4, 2).iterable.iterator.toIndexedSeq
    // should be 9
    conv0.length 
```

The operations described in [Row](representing-sample-as-row) are also available via the `Frame` API. We will 
look at these and some additional methods specific to the Frame API. In the example below we add a time-stamp 
column `"t"` to he data. We then apply a single parameter function to the `Row`s that converts the `String` 
values to `Double` values. We then make a copy of the finite stream and show the second sample of the data.
Note that we have used a `lazy` declaration of the stream to prevent the consumption of the data (documentation
by default print out the contents of the `val`). **This should not be used in production code.**  

```tut:book
    val colt2 = ("t", List(
      "0.000000", "0.000195", "0.000391", "0.000586", "0.000781",
      "0.000977", "0.001172", "0.001367", "0.001563", "0.001758",
      "0.001953", "0.002148", "0.002344", "0.002539", "0.002734",
      "0.002930", "0.003125", "0.003320", "0.003516", "0.003711"))

    lazy val convd = Frame(colt2, colx1, coly1, colz1)
    lazy val conv0d = convd((x: String) => x.toDouble, "t", "x", "y", "z")
    val conv1d = conv0d.iterable.iterator.toIndexedSeq
    // All should be should be java.lang.Double
    conv1d(1)("t").v 
    conv1d(1)("x").v 
    conv1d(1)("y").v 
    conv1d(1)("z").v 
```

In the example below we have created a `Frame` with the same structure as above. The first column is a represent a 
relative time-stamp in fractions of seconds starting at 0. Here we take this column's values and convert them to a 
fixed date and time time-stamp starting from `stamp`. To do this we first convert the values from `String` to 
`Double` then to `Long` scaled to nanoseconds. The nanoseconds are then converted to a `Duration` that is then 
added to the start `stamp` producing the final desired result. Finally we apply a sliding window as we did in the 
previous example.

```tut:book
    import java.time.{Duration, LocalDateTime}
    import pt.inescn.etl.Load.format

    val colx2 = ("x", List(
      "-0.101918", "0.886976", "0.207878", "-0.968121", "-0.386563",
      "0.598651", "0.531784", "0.902926", "0.536692", "0.596197",
      "0.039791", "0.112179", "0.004210", "0.212172", "0.010958",
      "0.307872", "-0.408034", "1.088191", "0.628710", "-0.481035"))
      
    val coly2 = ("y", List(
      "1.346609", "0.931745", "-0.090765", "-1.355280", "-2.908090",
      "0.164716", "0.519811", "1.399932", "0.485239", "-1.158396",
      "-1.644161", "-0.383748", "-0.441173", "1.435676", "0.576064",
      "1.034289", "-1.108588", "-0.049748", "-0.744704", "1.091714"))
      
    val colz2 = ("z", List(
      "-2.201459", "-0.621733", "1.079872", "-0.161299", "-0.523405",
      "2.409951", "1.240612", "0.320921", "-1.868204", "-1.377741",
      "1.041012", "2.372268", "0.475184", "0.157826", "-0.932026",
      "-0.492199", "-1.072158", "2.406418", "1.043367", "0.217882"))
    
    val stamp = LocalDateTime.parse("2018-05-04 13:43:52.101", format)
      
    lazy val cols9 = Frame(colt2, colx2, coly2, colz2)
    lazy val conv5_0 = cols9((x: String) => x.toDouble, "t", "x", "y", "z")
    lazy val conv5_1 = conv5_0((x: Double) => (x * 1e9).toLong, "t")
    lazy val conv5_2 = conv5_1((x: Long) => Duration.ofNanos(x), "t")
    lazy val conv5_3 = conv5_2((x: Duration) => stamp.plus(x), "t")
    lazy val conv5_4 = conv5_3((x: LocalDateTime) => x.toString, "t")
    lazy val conv5 = conv5_4.window(4, 2)
    
    lazy val conv5d = conv5.iterable.iterator.toIndexedSeq
    /* should be 
      Row(
        Map(
          "t" -> Val(Vector("2018-05-04T13:43:52.101", "2018-05-04T13:43:52.101195", "2018-05-04T13:43:52.101391", "2018-05-04T13:43:52.101586")),
          "x" -> Val(Vector(-0.101918, 0.886976, 0.207878, -0.968121)),
          "y" -> Val(Vector(1.346609, 0.931745, -0.090765, -1.35528)),
          "z" -> Val(Vector(-2.201459, -0.621733, 1.079872, -0.161299))))
    */
    conv5d(0)
```

Note that in the example above we broke the conversion into various steps. We are however free convert the initial 
fraction of seconds relative time-stamp directly to the final absolute date and time tme-stamp in a single function. 
This would be a more direct and efficient way of proceeding. The example split these operation into separate 
functions in order to demonstrate how such operations can be accumulated in a pipeline fashion. You may also opt 
for this route when separate functions need to be reused. 

We may have a need to develop and reuse functions that assume the existence of certain fields. This means that these
functions will have *hard-coded* field names. Data sources, read for example from a file, may not have these 
expected field names. If many of such reusable function exist, it may be easier to alter the field names when
loading the data. The example blow shows how we can take any `Frame` and change the field names sing the
`renameCols` method. The `a -> b` operator creates a tuple which indicates that an existing field `a` is to be 
renamed `b`. 


```tut:book
    lazy val cols1 = Frame(colt2, colx2, coly2, colz2)
    val cols2 = cols1.renameCols("x" -> "xx", "y" -> "yy", "z" -> "zz")
```

Once renames, the new field names should be used as is exemplified below. Failure to use the new names will 
cause a run-time error.

```tut:book
    lazy val cols1 = Frame(colt2, colx2, coly2, colz2).renameCols("x" -> "xx", "y" -> "yy", "z" -> "zz")
    val cols3 = cols1.updated("zz", "Invalidated")
```


Here we create a single new variable of a new type that is the sum of 2 other fields (columns). We can then
map the existing Frame into a new one were only several of the orginal fields exist. In the example below 
we only keep two of the original fields.


```tut:book
    lazy val cols1 = Frame(colt2, colx2, coly2, colz2)
    lazy val cols2 = cols1.renameCols("x" -> "xx", "y" -> "yy", "z" -> "zz")
    lazy val cols3 = cols2((s: String) => s.toDouble, "xx", "yy", "zz")
    lazy val cols4 = cols3((a: Double, b: Double) => a + b, ("xx", "yy", "x+y"))
    val cols5 = cols4.project("yy", "x+y")
```


Here we create a single new variable of a new type that is the sum of 2 other fields (columns) as above. We can 
then map the existing Frame into a new one were several fields are *removed* from the Frame. 


```tut:book
    lazy val cols1 = Frame(colt2, colx2, coly2, colz2)
    lazy val cols2 = cols1.renameCols("x" -> "xx", "y" -> "yy", "z" -> "zz")
    lazy val cols3 = cols2((s: String) => s.toDouble, "xx", "yy", "zz")
    lazy val cols4 = cols3((a: Double, b: Double) => a + b, ("xx", "yy", "x+y"))
    val cols5 = cols4.ignore("xx", "zz")
```

Depending on the number of fields we want to keep, selecting or ignoring the field names may be more succinct.

We may also filter the samples of a `Frame`. In the example below `filterWith` takes as a parameter a function
that processes sample ( `Row`) and returns true to keep the record or false otherwise. Here we remove all samples 
whose `"x"` field has a value above 1.0. Note that `xv.fold` processes either a `Left` (always returns false) or
`Right` (checks if the value converted to a `Double` is larger than 1.0).  

```tut:book
    lazy val cols6g = Frame(colx1, coly1, colz1)
    lazy val cols6c = cols6g.filterWith { r =>
      val xv: Either[String, String] = r("x")
      xv.fold(_ => false, s => s.toDouble > 1.0)
    }
    
    val read6c = cols6c.iterable.iterator.toIndexedSeq
    
    // should contain  ("x", "y", "z")
    cols6c.v.colNames 
    
    // should be Map("x" -> java.lang.String, "y" -> java.lang.String, "z" -> java.lang.String)
    cols6c.colTypes
    
    // shouldBe Row(Map("x" -> Val("3"), "y" -> Val("13"), "z" -> Val("23"))) 
    read6c.head 
    
    // should be 10
    read6c.length 
```

The example below converts a sub-set of the `Frame`'s fields into an `Array`. Unlike the `Row`'s `arr`, the 
`Frame.arr` method also takes as parameter as function that can be used to convert each selected field into
the desired type. Note that in this example the types need not be converted so an identity function is used. 

```tut:book
    lazy val cols0 = Frame(colx1, coly1, colz1)
    lazy val cols1 = cols0.renameCols("x" -> "xx", "y" -> "yy", "z" -> "zz")
    lazy val cols2 = cols1((s: String) => s.toDouble, "xx", "yy")
    lazy val cols3 = cols2((a: Double, b: Double) => a + b, ("xx", "yy", "x+y"))
    lazy val cols5e = cols3.arr("v_x_y", (i: Double) => i, "xx", "yy", "x+y")
      
    val read5e = cols5e.iterable.iterator.toIndexedSeq
    
    // should contain ("xx", "zz", "yy", "x+y", "v_x_y")
    cols5e.v.colNames 
    // should be Map( "xx" -> java.lang.Double, "yy" -> java.lang.Double, "zz" -> java.lang.String, "x+y" -> java.lang.Double, "v_x_y" -> Array[Double])
    cols5e.colTypes 
    
    // should be Val(1.0)
    read5e.head("xx") 
    // should be Val("21")
    read5e.head("zz") 
    // should be Val(11.0)
    read5e.head("yy") 
    // should be Val(12.0)
    read5e.head("x+y") 
    
    val tt: Either[String, Array[Double]] = unpack[Array[Double]](read5e.head("v_x_y"))
    val ta = tt.right.get
    // should be Array(12.0, 11.0, 1.0)
    ta 
```

## Mapping and Aggregation Frame Operations

In this section we describe methods that explicitly perform mapping and aggregation. The first example shown below
uses the `map` to take three fields (`"x"`, `"y"` and `"z"`), converts those to `Double` and then adds the double
values of `"x"` and `"y"` to creates new fields with the names `"xx"`, `"yy""` and  `"x+y"`. Note that mapping may
be used to replace the `Row` sample altogether, add new fields to the already existing `Row` or simply replace
the values of the existing fields. To replace the original `Row`, simply return the new row. To add new fields
use the `Row` `++` operator as shown below. To replace existing fields use the `Row` `++` operator as below but 
now use the original field names whose values you want replaced. 

```tut:book
    lazy val cols6 = Frame(colx1, coly1, colz1)
    lazy val cols6a = cols6.map({ r: Row =>
      val nr = for {
        xx <- unpack[String](r("x"))
        yy <- unpack[String](r("y"))
        zz <- unpack[String](r("z"))
        xd = xx.toDouble
        yd = yy.toDouble
        zd = zz.toDouble
        x_y = xd + yd
        nrow = Row(Map[String, Val[_]]("xx" -> xd, "yy" -> yd, "zz" -> zd, "x+y" -> x_y))
      } yield nrow
      // If no error then add new columns (replace existing ones)
      nr.fold(err => r + ("x+y" -> Val[String](err)), (nrow: Row) => r ++ nrow)
    },
      "xx" -> 0.0, "yy" -> 0.0, "zz" -> 0.0, "x+y" -> 0.0
    )
    
    
    val read6a = cols6a.iterable.iterator.toIndexedSeq
    // should contain ("x", "xx", "y", "zz", "yy", "x+y", "z")
    cols6a.v.colNames 
    
    // should be Map("x"->java.lang.String, "xx"->java.lang.Double, 
    //               "y"->java.lang.String, "yy"->java.lang.Double,
    //               "z"->java.lang.String, "zz"->java.lang.Double,
    //            "x+y" ->java.lang.Double)
    cols6a.colTypes 
    
    // should be Row(Map(
    //         "x" -> Val("1"),  "xx" -> Val(1.0), 
    //         "y" -> Val("11"), "yy" -> Val(11.0), 
    //         "z" -> Val("21"), "zz" -> Val(21.0),
    //         "x+y" -> Val(12.0)))
    read6a.head 
```

It is important top note that thr `map` requires an additional parameter that indicates what the type of the new
fields are if any. This is provided as tuples of field name and its value. If no new fields are introduces, none 
need be used. In the example above we provide the names `"xx"`, `"yy""` and  `"x+y"` each with the value 0.0.

In the example below we show hwo a Row's field values may be changed. Because we overwrite existing columns with a 
different type we need to provide the Row parameter for the already existing fields `"x"`, `"y""` and  `"z"`. Here 
we replace converted values `"x"`, `"y"` and replace `"z"` with the sum of `"x" + "y"`.

```tut:book
    lazy val cols6f = Frame(colx1, coly1, colz1)
    lazy val cols6b = cols6f.map({ r: Row =>
      val nr = for {
        xx <- unpack[String](r("x"))
        yy <- unpack[String](r("y"))
        zz <- unpack[String](r("z"))
        xd = xx.toDouble
        yd = yy.toDouble
        //zd = zz.toDouble
        x_y = xd + yd
        nrow = Row(Map[String, Val[_]]("x" -> xd, "y" -> yd, "z" -> x_y))
      } yield nrow
      // If no error then add new columns (replace existing ones)
      nr.fold(err => r + ("x+y" -> Val[String](err)), (nrow: Row) => r ++ nrow)
    },
      "x" -> 0.0, "y" -> 0.0, "z" -> 0.0
    )
    
    val read6b = cols6b.iterable.iterator.toIndexedSeq
    
    // should contain ("x", "y", "z")
    cols6b.v.colNames 
    
    // should be Map("x" -> java.lang.Double, "y" -> java.lang.Double, "z" -> java.lang.Double)
    cols6b.colTypes 
    
    // should be Row(Map("x" -> Val(1.0), "y" -> Val(11.0), "z" -> Val(12.0)))
    read6b.head 
```

Aggregation of the `Row`'s values can be accomplished using the `foldLeft`. This method take as a parameter the 
initial accumulator value, and a function that accepts the accumulator and a `Row` field and returns the 
accumulator. The execution of the parametrized function effectively updates the accumulator. The next example sums 
over the columns of each row. The result is a `Frame` (stream) of accumulator values (sum of each samples 
fields per row). 

```tut:book
    val key = "sum"
    val acc = Row(key -> 0.0)
    lazy val cols1 = Frame(colx1, coly1, colz1)
    lazy val sum1 = cols1.foldLeft(acc){ case (acum,(k,e)) =>
      val el = unpack[String](e)
      el.fold( _ => acum, { vl =>
        val d = vl.toDouble
        val se:Either[_,Double] = acum(key)
        val ne = se.fold(_ => se, v => v + d)
        val tmp = acum.updated(key,ne)
        tmp
      })
    }
    
    val readSum1 = sum1.iterable.iterator.toIndexedSeq
    // should contain ("sum")
    sum1.v.colNames 
    // should be Map("sum" -> java.lang.Double)
    sum1.colTypes 
    // should be Row(Map("sum" -> Val(33.0)))
    readSum1.head 
    // should be 20
    readSum1.length 
```

We can also fold over the entire frame, but this is less useful for infinite streams. To do this we use Scala's
`Iterator` interface. 

```tut:book
    lazy val cols2 = Frame(colx1, coly1, colz1).apply((s:String) => s.toDouble, "x", "y", "z")
    val acum = Row("xsum" -> 0.0, "ysum" -> 0.0, "zsum" -> 0.0 )
    val sum = cols2.iterable.foldLeft(acum) { (acc,row) =>
      val xe = unpack[Double](row("x"))
      val ye = unpack[Double](row("y"))
      val ze = unpack[Double](row("z"))

      val xs = unpack[Double](acc("xsum")).right.get
      val ys = unpack[Double](acc("ysum")).right.get
      val zs = unpack[Double](acc("zsum")).right.get

      val sx = xe.fold(_ => xs, v => v + xs)
      val sy = ye.fold(_ => ys, v => v + ys)
      val sz = ze.fold(_ => zs, v => v + zs)
      acc.updated("xsum",sx)
        .updated("ysum",sy)
        .updated("zsum",sz)
    }
    
    // should be 50.0
    sum("xsum") 
```

**Note:** sometimes it may be necessary to maintain state while processing a stream. For example we may want to 
keep a running average. In such a case use the `Frame.map` in combination with a mutable value (for example 
average). When you process a `Row` obtain the intended original value, use that to update the mutable running 
average and insert that into the returned Row. The resulting stream will have a new non-mutable field that holds 
the running average.

## Combining Frames by Rows or Columns

We may also combine two or existing frames into a single frame. Frames may be combined by columns as is shown below. 
Here we have one frame with the fields `"x"` and `"y"` and another frame with the field `"z""`, We can extend the 
first frame with te second using the `||` operator. The result is a new `Frame` with the fields `"x"`, `"y"` and
`"z"`. All operation on the new frame will transparently operate on the combined frames.

```tut:book
    lazy val cols1 = Frame(colx1, coly1).apply((s:String) => s.toDouble,"x", "y")
    lazy val cols2 = Frame(colz1).apply((s:String) => s.toInt,"z")

    lazy val cols = cols1 || cols2

    val read = cols.iterable.iterator.toIndexedSeq
    // should contain ("x", "y", "z")
    cols.v.colNames 
    
    // shouldBe Map("x" -> java.lang.Double, "y" -> java.lang.Double, "z" -> java.lang.Integer)
    cols.colTypes 
    
    // should be Row(Map("x" -> Val(1.0), "y" -> Val(11.0), "z" -> Val(21)))
    read.head 
    
    // shouldbe 20
    read.length 
```

We can also combine frames by rows. In other words we can take 2 frames and append the second one to the first
one. In the example below we append one frame onto another using the `Frame`'s `++` operator. One can confirm 
that the append operation is correct.

```tut:book
    // First frame
    val colxa1 = ("x", Seq("1",  "2", "3", "4", "5"))
    val colya1 = ("y", Seq("11","12","13","14","15"))
    lazy val colsa1 = Frame(colxa1, colya1).apply((s:String) => s.toDouble,"x").apply((s:String) => s.toInt,"y")

    // Second frame
    val colxa2 = ("x", Seq(" 6"," 7", "8", "9","10"))
    val colya2 = ("y", Seq("16","17","18","19","20"))
    lazy val colsa2 = Frame(colxa2, colya2).apply((s:String) => s.toDouble,"x").apply((s:String) => s.toInt,"y")

    // 'Stack' one after the other
    lazy val cols = colsa1 ++ colsa2

    val read = cols.iterable.iterator.toIndexedSeq
    
    // should contain ("x", "y")
    cols.v.colNames 
    
    // should be Map( "x" -> java.lang.Double, "y" -> java.lang.Integer)
    cols.colTypes 
    
    // should be 10
    read.length
    // should contain 
    //  Row(Map("x" -> Val(1.0), "y" -> Val(11))),
    //  Row(Map("x" -> Val(2.0), "y" -> Val(12))),
    //  Row(Map("x" -> Val(3.0), "y" -> Val(13))),
    //  Row(Map("x" -> Val(4.0), "y" -> Val(14))),
    //  Row(Map("x" -> Val(5.0), "y" -> Val(15))),
    //  Row(Map("x" -> Val(6.0), "y" -> Val(16))),
    //  Row(Map("x" -> Val(7.0), "y" -> Val(17))),
    //  Row(Map("x" -> Val(8.0), "y" -> Val(18))),
    //  Row(Map("x" -> Val(9.0), "y" -> Val(19))),
    //  Row(Map("x" -> Val(10.0), "y" -> Val(20)))
    read 
```

**Note:** care must be taken when appending frames. Only the last appended frame may be infinite. In this case
the resulting Frame will also be infinite. If any other frame is infinite, no iteration will occur on any of the 
next appended frames. This operator is usually used during testing when several finite data sets have to be 
combined into a single stream of `Row`s. 

## Alternate Source Frame Instances

We may create Frames from various sources. These may include for example files and message brokers. Here we will 
review several methods that allow us to load and process CSV (Comma Separated Value) files. The first example, 
shown below, loads a single CSV file.

```tut:book
    import com.github.tototoshi.csv.DefaultCSVFormat
    
    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    import better.files._
    import better.files.Dsl._

    val dataFile: File = cwd / "data/inegi/ensaios_rolamentos_2/rol1_1000_cmdn_ens1.csv"
    lazy val data: Frame = Frame.csv(dataFile)
    
    /* should contain Array(
       Row(Map("Time(s)" -> Val("0.000000"), "X" -> Val("-0.101918"), "Y" -> Val("1.346609"),  "Z" -> Val("-2.201459"))),
       Row(Map("Time(s)" -> Val("0.000195"), "X" -> Val("0.886976"),  "Y" -> Val("0.931745"),  "Z" -> Val("-0.621733"))),
       Row(Map("Time(s)" -> Val("0.000391"), "X" -> Val("0.207878"),  "Y" -> Val("-0.090765"), "Z" -> Val("1.079872"))),
       Row(Map("Time(s)" -> Val("0.000586"), "X" -> Val("-0.968121"), "Y" -> Val("-1.355280"), "Z" -> Val("-0.161299"))),
       Row(Map("Time(s)" -> Val("0.000781"), "X" -> Val("-0.386563"), "Y" -> Val("-2.908090"), "Z" -> Val("-0.523405"))),
       Row(Map("Time(s)" -> Val("0.000977"), "X" -> Val("0.598651"),  "Y" -> Val("0.164716"),  "Z" -> Val("2.409951"))),
       Row(Map("Time(s)" -> Val("0.001172"), "X" -> Val("0.531784"),  "Y" -> Val("0.519811"),  "Z" -> Val("1.240612"))),
       Row(Map("Time(s)" -> Val("0.001367"), "X" -> Val("0.902926"),  "Y" -> Val("1.399932"),  "Z" -> Val("0.320921"))),
       Row(Map("Time(s)" -> Val("0.001563"), "X" -> Val("0.536692"),  "Y" -> Val("0.485239"),  "Z" -> Val("-1.868204"))),
       Row(Map("Time(s)" -> Val("0.001758"), "X" -> Val("0.596197"),  "Y" -> Val("-1.158396"), "Z" -> Val("-1.377741")))
      )
    */
    lazy val read = data.iterable.iterator.take(10).toIndexedSeq
    read.foreach(println) 
```

To load the CSV file we first configure the CSV reader using the `MyFormat`. We indicate to the library that the 
CSV file uses a comma as delimiter. We then import several utilities that allow us to name file names and paths 
(the better-files library). The `dataFile` value indicates that we want to load a single file (we provide the 
complete path to the file we want to load). Finally we use the `Frame`'s `csv` method to load the data. From here 
on we can use the Frame independently from its source.

The next example is the same as the one above however here we explicitly list several CSV files as the sources.
The Frame `csvs` methods is used to load all of the files. The resulting frame consisting of all of the data in 
the files concatenated into a single stream. The files are concatenated in the order they are declared in the `csvs`
parameter. Note that in this case we have parsed the time-stamp of the file into the appropriate `Java` type.

```tut:book
    val norm_exp1 = cwd / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_norm_mm0_exp1.csv"
    val ir_exp1 = cwd / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_ir_mm1_exp1.csv"
    val or_exp1 = cwd / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_or_mm1_exp1.csv"
    val b_exp1 = cwd / "data/inegi/ensaios_rolamentos_2/rol1_rpm1000_hp0_b_mm1_exp1.csv"
    
    lazy val data2: Frame = Frame.csvs(MyFormat, Seq(norm_exp1, ir_exp1, b_exp1, or_exp1))
    lazy val data2_1 = data2((x: String) => LocalDateTime.parse(x, format), "t")
    lazy val data2_2 = data2_1((x: String) => x.toDouble, "x", "y", "z")
    
    // should be 4
    data2_2.colSize 
    
    lazy val read2_2 = data2_2.iterable.iterator.map(m => m("t"))
        /*
          should contain List(
          Val(LocalDateTime.parse("2018-05-09T13:06:45")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000001")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000002")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000003")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000004")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000005")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000006")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000007")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000008")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000009")),
          Val(LocalDateTime.parse("2018-05-09T13:06:45.000010")),
          ...
        )
        */
read2_2.take(11).foreach(println)
```

When working with several files, the filenames themselves contain features names and values. When we load these 
files we need to augment the existing record with those features names and values. The example below shows how we 
can use **filename globbing** to refer to a set of files and parse those filenames to obtain the feature names and 
values. In this case we refer to all files in a given directory that have the `"csv"` extension and the filename 
contains the sub-string `"hp"`.  To parse the filenames we use the `DataFiles.parseFileNames` method. We then 
filter the result to remove any errors that may have occurred. The result is a triple holding the full pathname
of the file, a dictionary containing the feature name and value (all as `String`) and the a dictionary containing the 
feature name and unit (all as `String`).

```tut:book
    import pt.inescn.etl.stream.DataFiles
    import pt.inescn.app.Utils

    val dir = cwd / "data/inegi/ensaios_rolamentos_2"
    val matchNames = "*hp*.csv"
    val files = DataFiles.parseFileNames(dir, matchNames, Utils.parseFileName).toList
    val parserOk = files.filter(_.isRight).map(_.right.get)
    
    lazy val (filesp, features, units) = parserOk.unzip3
    
    /*
      should contain 
        Map("load" -> "0", "damage" -> "ir", "exp" -> "1", "damage_size" -> "1", "bearing" -> "1", "speed" -> "1000"),
        Map("load" -> "0", "damage" -> "b", "exp" -> "1", "damage_size" -> "1", "bearing" -> "1", "speed" -> "1000"),
        Map("load" -> "2", "damage" -> "norm", "exp" -> "4", "damage_size" -> "3", "bearing" -> "1", "speed" -> "1000"),
        Map("load" -> "0", "damage" -> "norm", "exp" -> "1", "damage_size" -> "0", "bearing" -> "1", "speed" -> "1000"),
        Map("load" -> "0", "damage" -> "or", "exp" -> "1", "damage_size" -> "1", "bearing" -> "1", "speed" -> "1000")
    */
    features 
    
    /* should contain
      Map("load" -> "hp", "damage" -> "ir", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm"),
      Map("load" -> "hp", "damage" -> "b", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm"),
      Map("load" -> "hp", "damage" -> "norm", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm"),
      Map("load" -> "hp", "damage" -> "norm", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm"),
      Map("load" -> "hp", "damage" -> "or", "exp" -> "exp", "damage_size" -> "mm", "bearing" -> "rol", "speed" -> "rpm")
    */
    units 
```

Parsing the filename is context dependent. As such in order to parse the filename we pass to the `parseFileNames` 
the function that does the actual parsing. In the case above we use as an example the `pt.inescn.app.Utils.parseFileName` 
method. Of course what we really want is to combine these parsed features with the corresponding files' records.
The example below shows how to add the filenames' parsed features to the corresponding data. Here we use a three 
methods to: a) match and parse the filenames into features (`DataFiles.fileNamesfeatures`), b) ignore any parsing
errors that may have occurred (we should really check for and signal these error, `DataFiles.ignoreParseErrors`) 
and c) load each file and combine its data with the corresponding filename feature (`Frame.extendCSVs`).

```tut:book
    val features3 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features4 = DataFiles.ignoreParseErrors(features3)
    lazy val data4 = Frame.extendCSVs(MyFormat, features4)
    lazy val data5 = data4("x", "z", "damage")
    /* should contain 
      Row(Map("x" -> Val("-20.000000"), "z" -> Val("40.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-21.000000"), "z" -> Val("41.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-22.000000"), "z" -> Val("42.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-23.000000"), "z" -> Val("43.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-24.000000"), "z" -> Val("44.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-25.000000"), "z" -> Val("45.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-26.000000"), "z" -> Val("46.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-27.000000"), "z" -> Val("47.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-28.000000"), "z" -> Val("48.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-29.000000"), "z" -> Val("49.000000"), "damage" -> Val("b"))),
      Row(Map("x" -> Val("-0.101918"), "z" -> Val("-2.201459"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.886976"), "z" -> Val("-0.621733"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.207878"), "z" -> Val("1.079872"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("-0.968121"), "z" -> Val("-0.161299"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("-0.386563"), "z" -> Val("-0.523405"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.598651"), "z" -> Val("2.409951"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.531784"), "z" -> Val("1.240612"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.902926"), "z" -> Val("0.320921"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.536692"), "z" -> Val("-1.868204"), "damage" -> Val("norm"))),
      Row(Map("x" -> Val("0.596197"), "z" -> Val("-1.377741"), "damage" -> Val("norm")))
    */
    val read5 = data5.iterable.iterator.toIndexedSeq
```

Once again we get a single finite stream that represents all of the data of the concatenated files in the order they 
are declared or used. Notice how the results now contain for example a new `"damage"` feature obtained from the 
filename.

When we load the files we may not be interested in loading all of the data into a single stream. For example, we 
may want to load part of the file in order to generate training, evaluation or test data-sets. The next example 
shows how `Frame.extendCSVs` is parameterize to load continuous sections of the data. In this case we load the 
first `numberOfSamples` records from the file. If no value is passed onto the last parameter all of the data is 
loaded.

```tut:book
    import pt.inescn.etl.stream.Load.Frame._
    
    val features3 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features4 = DataFiles.ignoreParseErrors(features3)
    
    //Number of samples taken from the file
    val numberOfSamples = 2048 
    lazy val goodLearn: Frame = Frame.extendCSVs(MyFormat, features4, SplitOptions.takeOnly(numberOfSamples))
    goodLearn.toString(10)
```

The parameter takes a specialized function that loads part of the data. Several of these functions are found in the
`SplitOptions` module and they include:

* `all` : identity function, no selection. All data is used.
* `takeOnly(n: Int)` : takes only the first `n` records
* `dropOnly(n: Int)` : takes only the last `n` records
* `takeThenDrop(takeN: Int, dropN:Int)` : takes only the first `takeN` records and of those drops the first `dropN` 
records.
* `dropThenTake(takeN: Int, dropN:Int)`: drops the first `dropN` records and of those takes only the first `takeN` 
                                         records.

Users are free to define and use their own selection functions. One need only define a function with the required
signature (see source code). 
 
Splitting data files into train and test data-sets may not be as simple as in the previous example. The `Frame`
API provides a means to select data records based on the fields values. The following example shows how. Before 
describing the steps to do this, lets provide some context in order to make the example easier to understand. 
Assume we have sensor data representing vibration from a number of ball-bearings. The data is collected for each 
bearing under several conditions. Each file with data from one such a bearing is placed into a file whose filename 
has the bearing's id (digits representing a numeric ID). The corresponding field name is (surprisingly enough) 
`"bearing"`. The bearing's data is collected with and without damage. The type of damage (or lack thereof) is also 
recorded in the filename - the field name is `"damage"`. If the value is `"norm"` then the bearing is not damaged, 
otherwise it is. 

The goal is to split the data into training and test data-sets. We want to train an on-line algorithm by providing
data that is generated from the bearings `"1"` and `"2"` only. Because we want to perform anomaly detection, we 
will only analyze signals from these bearings with no damage (`"norm"`). The test data-set will use bearing `"3"`
only. In this case however we want all available data, including the data when the bearing is damaged. 

The example below shows he steps required to select the data as described in the previous paragraph. We first use
file globbing (see variables `dir` and `matchNames`) to indicate the source files to be ingested for processing.
We then parse the filenames to get features from the filenames (`DataFiles.fileNamesfeatures`) and ignore any
parsing errors that may have occurred (`DataFiles.ignoreParseErrors`). In the example below we have 90 of these 
files. 

In order to select the test and train data files we first extract the features' values hat will be used to select 
the data records. This is done in the `classes`  function that takes in a `Row` and extracts the `"damage""` and 
`"bearing"` features. These are *packed* into a tuple to be used for filtering (we declared the `FileFilter` as
a shorthand for this filter type). Next we define the `testFilter` function used to select the train data - if 
the the data is from bearings `"1"` and `"2"` and they have no damage, return true to collect else return false. 
The `testFilter` function is used select the test data - if the data is from bearing `"3"` return true to collect 
it (irrespective of whether it is damaged or not).

We collected the filtered files using the `DataFiles.splitFiles` method just to demonstrate the results. By default 
the parameter `shuffle` will be true. However for testing purposes one can turn this off in order to facilitate 
debugging. In the example below we list the files that are select for each type of data-set. You can confirm the 
bearing IDs and  damage states are correct. 

```tut:book
    import pt.inescn.etl.stream.Load

    // Use files
    val dir = cwd / "data/inegi/ensaios_rolamentos_3"
    val matchNames = "*.csv"

    val features1 = DataFiles.fileNamesfeatures(dir, matchNames, Utils.parseFileName)
    val features2 = DataFiles.ignoreParseErrors(features1).toList
    // should be 90
    features2.size

    type FileFilter = (String,String)

    // What is used to tag the class
    def classes(r: Row) : FileFilter = {
      val clas = Load.unpack[String](r("damage")).right.get
      val bearing = Load.unpack[String](r("bearing")).right.get
      (clas, bearing)
    }
    
    // learning data has ony 1 class: no failures (total of expectedNumberOfFiles files)
    // avoid data leakage, learn only with bearings 'rol1' and 'rol2'
    def learnFilter(r: FileFilter) : Boolean = { (r._1 == "norm") && ((r._2 == "1") || (r._2 == "2")) }
    
    // test data has any class (total of 2*expectedNumberOfFiles files)
    // avoid data leakage, test only with bearings 'rol3'
    def testFilter(r: FileFilter) : Boolean = { r._2 == "3" }

    lazy val (train,test) = DataFiles.splitFiles(features2.toIterator, classes, learnFilter, testFilter, shuffle = false)
    val train_files = train.map(_._1.path.getFileName.toString)
    val test_files = test.map(_._1.path.getFileName.toString)
```

At this point we are ready to load the data. We simply use the `Frame.extendCSVs` to generate the training and test
`Frame`s. In the example we assume that an on-line algorithm will be tested so a single stream must be created
consisting first of the training data and then the test data. We concatenate the `Frame`s using the `++` operator. 
The result can now be processed using the `Frame` API. 

```tut:book
    // Need to sort to ensure results in multiple platforms are the same
    val trains = train.toList.sortBy(_._1.path.getFileName.toString).toIterator
    val tests = test.toList.sortBy(_._1.path.getFileName.toString).toIterator
    
    lazy val learnFrame: Frame = Frame.extendCSVs(MyFormat, trains, SplitOptions.all)
    lazy val testFrame: Frame = Frame.extendCSVs(MyFormat, tests, SplitOptions.all)
    lazy val fullData = learnFrame ++ testFrame
    fullData.iterable.iterator.take(10).foreach(println)
```

The example below shows how an alternate set of training and test data-sets can be generated. In this case the 
training data-set will contain the data of all bearings when no damage has occurred. The test data-set will contain 
all the data of all bearings irrespective of the damage state. These data-sets are  not valid training and test 
data-sets because the hare the same data. The resulting model could possible over-fit but this cannot be detected. 

We will use two parameters to select sub-sets of data samples separately into the train and test data-sets respectively.
The fourth parameter (`nLearnSamples = numberOfSamples`) instructs the `Frame.splitExtendCSVs` to place only the
first `numberOfSamples` into the training data-set. The fifth parameter (`nIgnoreTestSamples = numberOfSamples`) 
instructs the `Frame.splitExtendCSVs` to ignore the first `numberOfSamples` to be placed into the test data-set. The 
sixth parameter (`nTestSamples = 0`) instructs the `Frame.splitExtendCSVs` to collect a fixed number of records 
into the test data-set (after the initial records have been dropped). The 0 in this case signifies that all of the 
remaining records should be loaded. 

Note that we can always use sophisticated selection filters as in the previous examples in conjunction with the
`nLearnSamples`, `nIgnoreTestSamples` and `nTestSamples` parameters. 

```tut:book
    // What is used to tag the class
    def classes(r: Row) : Val[_] = { r("damage") }
    // learning data has ony 1 class: no failures (total of expectedNumberOfFiles files)
    def learnFilter(r: Val[_]) : Boolean = { r == Val("norm") }
    // test data has any class (total of 2*expectedNumberOfFiles files)
    def testFilter(r: Val[_]) : Boolean = { true }

    object MyFormat extends DefaultCSVFormat {
      override val delimiter = ',' // ';'
    }

    lazy val (trainData,testData) = DataFiles.splitFiles(features2.toIterator, classes, learnFilter, testFilter, shuffle = false)

    val expectedNumberOfTrainFiles = 45 // only good
    val expectedNumberOfTestFiles = 90 // good and bad
    val numberOfSamplesPerFile = 20480 // excluding the header

    // Lets read `numberOfSamples` from all the files for training
    val numberOfSamples = 2048 //Start of file: number of samples to learn

    // We generate training data so that each file uses only the first 'numberOfSamples'
    // The test files will ignore those first 'numberOfSamples' and use the rst of the data
    // for testing (test sample size = 0). Datafiles are shuffled by default
    lazy val (learn, test) = Frame.splitExtendCSVs(MyFormat, trainData, testData, numberOfSamples, numberOfSamples, 0, shuffle = false)

    // Lets just check some of the columns
    val learnIdx = learn("X", "Y", "speed", "damage").iterable.toIndexedSeq
    
    // Did we read the expected total number of records ?
    println(numberOfSamples * expectedNumberOfTrainFiles)
    learnIdx.size

    // Lets just check some of the columns
    val testIdx = test("X", "Y", "speed", "damage").iterable.toIndexedSeq
    println((numberOfSamplesPerFile - numberOfSamples) *  expectedNumberOfTestFiles)
    testIdx.size 
```

For large data-sets it may be too time-consuming to repeat the data-generation step often. It is possible to load 
data into a `Frame`, perform data pre-processing and then save the data to a file that can then be loaded back into 
a `Frame`. The example below shows how we can load and shape the data to our needs and then save it to the 
operating system's temporary directory. It is advisable to save temporary files to the OSs temporary directory so 
that they can be removed automatically.
 
```tut:book
    import java.lang.{System => JSystem}


    val (trainData,testData) = DataFiles.splitFiles(features2.toIterator, classes, learnFilter, testFilter, shuffle = false)
    lazy val (learn, test) = Frame.splitExtendCSVs(MyFormat, trainData, testData, numberOfSamples,numberOfSamples, 0, shuffle = false)
    lazy val fullData = learn ++ test
    
    val tmp = File(JSystem.getProperty("java.io.tmpdir")) / "fulldata.csv"
    Load.save(tmp, MyFormat, fullData)
```

We have given a relatively detailed description of the `Frame` API. For more information take look at the 
[API](../api/index.html). More specifically look at the [Load module](../api/pt/inescn/etl/stream/Load$.html) 
that has the `Row` and `Frame` API. The source is also available in Gitlab.





