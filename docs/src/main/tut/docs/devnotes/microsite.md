---
layout: docs
title:  "SBT Microsite"
section: "devnotes"
---

# Setting Up SBT Micro-site

GitLab allows us to host the project documentation on one of its sites (domain name `gitlab.io`). Their are several ways 
to [generate and publish](https://docs.gitlab.com/ee/user/project/pages/) static[^3] HTML pages:

1. Edit the static HTML pages and publish them using GitLab's CI
2. Use a a static site generator to generate the HTML pages and then publish those using GitLab's CI

We opted for the 2nd alternative because several options based on Markdown allow us to quickly set-up and edit the HTML 
pages with minimum training and HTML expertise. Any static site generator can be used so we chose 
[SBT Microsites](https://47deg.github.io/sbt-microsites/)[^2] because:
1. It is usually deployed in Scala projects[^4]
2. Uses [Jekyll](https://jekyllrb.com/) as does GitLab
3. We can easily configure and use several JavaScript plugins (for example to generate maths expressions)

Although we strive to stick close to GitLab's static page generator set-up, it is not 100% compatible. Concretely we 
will use [Kramdown](https://kramdown.gettalong.org/quickref.html) as our Markdown language even though GitLab has its 
own [GitLab Flavored Markdown (GFM)](https://docs.gitlab.com/ee/user/markdown.html)

The publishing process is as follows:
1. Create the directory and/or Markdown source files in the appropriate documentation directory of the documentation 
sub-project (more information [here](#configuring-sbt-microsite)); 
2. Edit the Markdown (Kramdown) source (extension `.md`);
3. Execute the SBT commands to generate the static HTML files and publish them to a local directory;
4. Go to the locally published directory and run the jekyll serve command there;
5. Open your browser at `http://localhost:4000/relative_base` were `relative_base` must be configured in the SBT 
microsite's configuration file (`relative_base` will be set to "adw" in conformance with GitLab.io);
6. Validate the output and return to step (1) and repeat as many times as necessary.

## Installing Ruby and Jekyll

In order to follow the steps above we have to set-up the HTML static generator Jekyll and the SBT micorsite plugin 
locally on our machine so that we can execute step (3) above (SBT 
[Microsite uses Jekyll](https://47deg.github.io/sbt-microsites/docs/index.html) under the hood). 
Unfortunately, this means we also have to install the static site generator Jekyll and the SBT micorsite plugin on 
GitLab's virtual machine that runs the CI process (referred to as *runners*). This is because the CI starts on a clean 
clone and then runs CI scripts used to publish the site. We do not keep the generated site in GitLab but only the 
source, so the static site generator, via the microsite sbt commands, must also be executed. 

Jekyll provides [installation instructions](https://jekyllrb.com/docs/installation/) for various operating systems. 
We will now describe a **Linux Ubuntu** installation. We cannot install Ruby via its 
[Debian package](https://jekyllrb.com/docs/installation/ubuntu/) because SBT microsite requires a 
[specific Ruby version](https://47deg.github.io/sbt-microsites/docs/index.html#prerequisites) [^5]. To solve this we 
install the [Ruby Version Manager (RVM)](https://rvm.io/rvm/install) and then use that to install the correct version. 
We first install the Ruby dependencies as follows in the command line:

```Bash
sudo apt-get update
sudo apt-get install curl g++ gcc autoconf automake bison libc6-dev libffi-dev libgdbm-dev libncurses5-dev \
  libsqlite3-dev libtool libyaml-dev make pkg-config sqlite3 zlib1g-dev libgmp-dev libreadline-dev \
  libssl-dev
```

Next we install *RVM* by first getting its PGP signature and then getting the BASH script installer so:
 
```Bash
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
curl -sSL https://get.rvm.io | bash -s stable
```
 
Before running the RVM command we need to set the environment variables as follows (alternatively logout and then 
login to your X session again):

```Bash
source ~/.rvm/scripts/rvm
```

And now we install the correct Jekyll version (check the version number in the BST microsite first, as of this writing 
its stated `3.2.1+`):

```Bash
rvm install 2.2.8
rvm use 2.2.8 --default
```

If you execute the above install and don't have root privileges a required replacement of a library (libssl-dev to 
libssl1.0-dev) will fail[^10]. If you use root privileges via `sudo` you will need to interactively confirm the change. 
Make sure tis change is does not corrupt your system. 

We can easily check the version running ruby:

```Bash
ruby -v
```

Next we must install the *SASS* and *Jekyll* using the following *Gem* commands:

```Bash
gem update --system
gem install sass
gem install jekyll -v 3.2.1
```

Note that we installed the version that is indicated in the Microsite installation instructions. 

During installation the `.profile` file is changed. 

```Bash
# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*
```

In the commands above we sourced the `.profile` in order to get access the *RVM* command. However for all new terminals 
to have access to the command you have to log out (not lock) of your X session so that the file is sourced (in Ubuntu 
only the bash startup files are sourced for terminal emulation. For example the `.bashrc` is sourced [^11]).


## Installing the Microsite Plugin

SBT Microsite is a SBT plugin, so we need only follow the 
[install](https://47deg.github.io/sbt-microsites/docs/index.html#set-it-up-in-your-project) instructions of SBT.
First add the following line to the `project/plugins.sbt` file within your project or sub-project where you want to 
use the microsites:

```SBT
addSbtPlugin("com.47deg"  % "sbt-microsites" % "0.7.24")
```

and then enable the plugin by adding the following line to the `build.sbt` file:

```SBT
enablePlugins(MicrositesPlugin)
```

## Configuring SBT Microsite

In order to use SBT Microsite we need to set-up a `docs` SBT sub-project. To do that we create the following directory 
at the root of the SBT project:

```Bash
docs/src/main
```

Under this we have the following directories:

1. `docs/src/main/tut`: this directory has the Markdown (Kramdown) sources. The source files here will first be 
processed by [tut](http://tpolecat.github.io/tut/), a tutorial generator for [Scala](https://www.scala-lang.org/). Tut
*"interprets Scala code in `tut` sheds, allowing you to write documentation that is typechecked and run as part of your 
build."* The resulting Kramdown source is then processed by Microsite to generate the final HTML pages. 
1. `docs/src/main/resources/microsite`: contains a copy of the Jekyll directory structure that allows us to configure 
Jekyll page generation. SBT Microsite processes some of these files and then copies all of these together with it own 
output to a Jekyll scratchpad that is then used by Jekyll to produce the final static HTML website. Note that the 
location of these directories can be [configured](https://47deg.github.io/sbt-microsites/docs/settings.html). We have 
used the following set of directories:
    1. The `docs/src/main/resources/microsite/img` directory contains images that we use to 
[customize](https://47deg.github.io/sbt-microsites/docs/customize.html) the microsite. 
    1. The `docs/src/main/resources/microsite/js` directory contains all of the JavaScript code used by the HTML pages. 
    This includes *plugins* for graph and math expression rendering (see the section on 
    [Microsite Plugins](#microsite-plugins), [KaTex](katex.html) and [MermaidJS](mermaid.html) for more information). 
    1. The `docs/src/main/resources/microsite/includes` directory contains files[^6] that can be 
    [included](https://jekyllrb.com/docs/includes/) using the [Liquid](https://jekyllrb.com/docs/liquid/) template 
    [language](https://shopify.github.io/liquid/). For example `{% raw %} {% include katex.md %} {% endraw  %}` is used 
    to *import* the Katex scripts and fonts into a Markdown file. 
    1. The `docs/src/main/resources/microsite/data` directory contains one of the most important configuration files, 
    which is **`menu.yml`**. In this file we define Microsite's 
    [menus](https://47deg.github.io/sbt-microsites/docs/layouts.html#how-to-setup-the-docs-menu)
    that are specified[^9] by indicating: the titles and URLs of the pages and the hierarchical structure of the menus 
    (`options` and `nested_options`). 
    
The microsite configuration is found in the `build.sbt` file's sub-project. Here we set-up the path to the `docs` 
sub-project, establish the project dependencies with other sub-projects, indicate the settings such as libraries and 
configuration variables, enable the necessary plugins and perform any tasks required prior to building. This is an 
example:

```Scala
lazy val docs = (project in file("docs"))
  .dependsOn(root)
  .settings(moduleName := "docs")
  .settings(commonSettings)
  .settings(micrositeSettings: _*)
  .settings(
    siteSubdirName := "api",
    addMappingsToSiteDir(mappings in packageDoc in Compile in root, siteSubdirName)
   )
  .enablePlugins(MicrositesPlugin)
  .enablePlugins(TutPlugin)
  .enablePlugins(SiteScaladocPlugin)
```

Note that the `addMappingsToSiteDir` task is used to copy the results of the `ScalaDoc` compilation into an `api`
directory. This directory is relative to the `docs` project target directory (`docs/target/site`), so the API 
documentation is automatically placed in the Jekyll site output and available for use at the site's root. In the 
`menu.yml` menu configuration file we can access the API with the following instruction: `url: api/index.html`

To [configure](https://47deg.github.io/sbt-microsites/docs/settings.html) the Microsite we have set-up a variable 
(`micrositeSettings`) with all of the SBT microsite configuration variables. Here is an example:

```Scala
lazy val micrositeSettings = Seq(
  micrositeName := "ADW",
  micrositeDescription := "Anomaly Detection Workbench Tutorial",
  micrositeBaseUrl := "adw",
  micrositeAuthor := "INESC TEC - CESE",
  micrositeDocumentationUrl := "docs",
  micrositeHomepage := "https://cese.gitlab.io/adw",
  micrositeGitHostingService := GitLab,
  micrositeGitHostingUrl := "https://gitlab.com/cese/adw",
  micrositeDataDirectory := (resourceDirectory in Compile).value / "microsite" / "data", // default
  micrositeStaticDirectory := (resourceDirectory in Compile).value / "microsite" / "static", // default
  micrositeExtraMdFiles := Map(
    file("CHANGELOG.md") -> ExtraMdFileConfig(
      "changelog.md",
      "page",
      Map("title" -> "Changelog", "section" -> "changelog", "position" -> "99")
    ),
    file("LICENSE") -> ExtraMdFileConfig(
      "license.md",
      "page",
      Map("title" -> "License", "section" -> "license", "position" -> "100")
    )
  ),
  // https://github.com/47deg/sbt-microsites/blob/master/src/main/scala/microsites/MicrositesPlugin.scala#L113
  includeFilter in makeSite := "*.html" | "*.css" | "*.png" | "*.jpg" | "*.gif" | "*.js" | "*.swf" | "*.md"
    | "*.ttf" | "*.woff" | "*.woff2",
  micrositeGitterChannel := false,
  micrositeShareOnSocial := false
)
```

The settings above have these important values:
* `micrositeBaseUrl`: this sets the base offset of the site that is used to generate all of the site's URL. We need 
this in order to comply with GitLab's hosting requirements (project name is always added to the GitLab's domain name). 
* `includeFilter`: we need to alter this in order to ensure that the appropriate files are copied to the final site. 
In particular we need add the extensions `*.ttf`, `*.woff` and `*.woff2` in order to copy several fonts used by 
JavaScript plugins.
* `micrositeDocumentationUrl` is the relative URL that is used as the link in the site's top right 
[documenation](..){:target="_blank"} link.
* `micrositeGitHostingService` and `micrositeGitHostingUrl` variables are used to name and link to our GitLab project 
source in *Gitlab.com*.
* `micrositeDescription` is the text that is shown in large fonts in the center of the Microsite's home page background 
(also referred to the `Jumbotron`). 
* `micrositeName` is used as the text on the top left corner of the the Microsite's home page background.

## Microsite CI Configuration

All Gitlab CI configuration is done in the `.gitlab-ci.yml` found in the ADW **root** (not the `docs` sub-project 
folder) because it can potentially be used on any (or all) os the sub-projects. See the 
[introductory](introduction.html) notes on SBT and CI for more information. 

GitLab does not allow for publishing pages like GitHub does so we coluld not use 
Microsite's [instructions](https://47deg.github.io/sbt-microsites/docs/publish-with-travis.html). In order to publish 
the pages we have to build the `doc` sub-project and then explicitly copy the site to the GitLab's hosting `public` 
directory using a job in GitLab's CI process. Note that we also need to use GitLab's `pages` stage and job name 
explicitly so that the pages are deployed to the `gitlab.io` domain correctly (copying to the `public` directory is 
not enough). So first we declare the `pages` stage so:

```YAML
stages:
  - build
  - test
  - doc
  - pages
```

And then we set-up the `pages` job to:
 * Execute the SBT Microsite build command (`sbt makeMicrosite`) to generate the static site;
 * Copy the resulting site to a `public` directory (must create this directory first);
 * Indicate to the GitLab CI were the local static site can be found `paths: public`

Here is the relevant snippet:

```YAML
pages:
  stage: pages
  script:
  - sbt makeMicrosite
  - mkdir public
  - cp -r docs/target/site/* public
  artifacts:
    paths:
    - public
  only:
    - master
```

We are only interested in generating the site for the master branch because we have only one project documentation site.
We therefore use `YAML`'s `only` directive to indicate that the pages' job should only be executed for the master 
branch (no time wasted on documentation when it is not going to be deployed). Ultimately we could tag stable versions 
and use that to tag and add version specific documentation. This is not currently done. 

In order to run the above commands we have to ensure that the following has been installed:
* Java
* [SBT](https://www.scala-sbt.org/)
* [Jekyll](https://jekyllrb.com/)

For the case of Java and SBT please see the [introductory](introduction.html) notes on the setup of CI. 

For Jekyll we adapted Microsite's [instructions](https://47deg.github.io/sbt-microsites/docs/index.html#continuous-integration---travis)
to the GitLab platform. More concretely we followed documentation [here](https://docs.gitlab.com/ee/user/project/pages/).
Accordingly the following was added to the `.gitlab-ci.yml` configuration file:

```YAML
before_script:
  - export PATH=${PATH}:./vendor/bundle
  - apt install git curl libssl-dev libreadline-dev zlib1g-dev autoconf bison build-essential libyaml-dev libreadline-dev libncurses5-dev libffi-dev libgdbm-dev -y
  - gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
  - curl -sSL https://get.rvm.io | bash -s stable
  - source /etc/profile.d/rvm.sh
  - rvm use 2.2.8 --install --fuzzy
  - gem update --system
  - gem install sass
  - ruby --version
  - gem install jekyll -v 3.2.1
```

Note that the above instructions are basically the same instructions we used for the 
[local Jekyll](#installing-ruby-and-jekyll) installation. These instructions run before any job is executed thereby 
ensuring that all the tools are installed and ready for building the sub-project. After this the 
[documentation build](#microsite-ci-configuration) CI commands can be executed. 

## Microsite Plugins

Jekyll has many useful plugins that can be [installed](https://jekyllrb.com/docs/plugins/) in 3 different ways. We have 
opted for a 4th solution that embeds JavaScript directly into the Markdown source in order to generate the desired HTML 
during viewing in the client side. By doing this we can:
* More closely follow the JS install and usage instructions of the plugins we will use;
* Avoid the need to use a 3rd party sites by making the JS scripts available in our site;
* Easily select and use the versions of the plugins.

To do this we place all of the libraries' JS minified code into the 
[`micrositeJsDirectory`](https://47deg.github.io/sbt-microsites/docs/settings.html) directory[^7] (not the 
`micrositePluginsDirectory` directory that is reserved for the Ruby plugin code). For examples of some of the JS 
plugins that have been installed look at [KaTex](katex.html) and [MermaidJS](mermaid.html).

## Building the Microsite

Instructions on how to generate the site are [here](https://47deg.github.io/sbt-microsites/docs/build-the-microsite.html).
We will review them in this section and provide the correct (expected) URLs to make setup and testing easier. in this 
section we assume that the preoject has been cloned in the the directory `/home/user/git/adw`. So first, at the root 
of the project we need to launch SBT and then invoke the docs build command.

This is how we launch SBT:

```BASH
user@machine:~$ cd git/adw
user@machine:~/git/adw$ sbt
[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/git/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/git/adw/)
[info] sbt server started at local:///home/user/.sbt/1.0/server/434504055fb0c15104d0/sock
sbt:adw> 
```

then build the site (this is not a clean build, so Scaladoc is not invoked. We also do iot show the full output in 
order to make this easier to read):

```BASH
sbt:adw> makeMicrosite
[info] Running tut.TutMain /home/user/git/adw/docs/src/main/tut /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll .*\.(md|markdown|txt|htm|html)
[tut] compiling: /home/user/git/adw/docs/src/main/tut/publications.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/contributing.md
...
[tut] compiling: /home/user/git/adw/docs/src/main/tut/index.md
Copying from /home/user/git/adw/CHANGELOG.md to /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md/ExtraMdFileConfig(changelog.md,page,Map(title -> Changelog, section -> changelog, position -> 99))
Copying from /home/user/git/adw/LICENSE to /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md/ExtraMdFileConfig(license.md,page,Map(title -> License, section -> license, position -> 100))
[info] Running tut.TutMain /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll .*\.(md|markdown|txt|htm|html)
[tut] compiling: /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md/changelog.md
[tut] compiling: /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md/license.md
[info] Configuration file: /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_config.yml
[info]             Source: /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll
[info]        Destination: /home/user/git/adw/docs/target/jekyll
[info]  Incremental build: disabled. Enable with --incremental
[info]       Generating... 
[info]                     done in 0.938 seconds.
[info]  Auto-regeneration: disabled. Use --watch to enable.
[success] Total time: 16 s, completed 13/nov/3019 14:12:04
sbt:adw> 
```

The Microsite command first runs `tut` ([see](http://tpolecat.github.io/tut//commands.html)) and then 
`makeSite`[^8] ([see](https://www.scala-sbt.org/sbt-site/)). We are free to run these commands directly. It is 
especially useful when writing Tut sheds because it allows us to quickly confirm that the Scala code is compiling 
correctly. Here is an example:

```Bash
sbt:adw> tut
[info] Running tut.TutMain /home/user/git/adw/docs/src/main/tut /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll .*\.(md|markdown|txt|htm|html)
[tut] compiling: /home/user/git/adw/docs/src/main/tut/publications.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/contributing.md
...
[tut] compiling: /home/user/git/adw/docs/src/main/tut/index.md
[success] Total time: 18 s, completed 13/nov/3019 14:18:24
```

From the commands above the output indicates were the docs site target is located. We can use that to serve and view 
the microsite in our browser. To do this:
* First navigate to the docs sub-projects target directory (`cd /home/user/git/adw/docs/target/site`)
* Launch the jekyll server (`jekyll serve`)
* Now browse the [site at http://localhost:4000/adw/](http://localhost:4000/adw/){:target="_blank"} (don't forget the last slash)

Note that the site's base URL is **adw** in accordance with our [microsite settings](#configuring-sbt-microsite). 
When editing the Markdown we can keep the Jekyll server running. It automatically detects and serves the new pages
whenever we run the `makeMicrosite` or `tut` commands. It allows us to interactively edit and check the results.

Sometimes it is necessary to make a fresh start. In this case we can remove the site so:

```SBT
sbt:adw> docs:clean
```

We can repeat the commands above to generate the site again.

SBT comes with a pre-built target `doc` that generates the API documentation via the `scaladoc` compiler. Note that the 
doc target exists for all sub-projects. So to generate the API documentation of the `core` sub-project we execute the 
following sbt command:

```SBT
sbt:adw> root / doc
```

To run all of the doc targets on all sub-projects just execute:

```SBT
sbt:adw> doc
```

If the `core` API documentation has been generated, generating the Microsite command will not trigger a Scaladoc 
compilation. If any changes have been made to the source code, the Microsite command will automatically trigger a 
Scaladoc compilation

As with the other SBT targets we can also clean the API documentation so:

```SBT
sbt:adw> doc:clean
```

Note that the Microsite commands automatically use the `docs` project, so the following commands need **not** 
be written out with the target made explicit:


```SBT
sbt:adw> docs:tut
sbt:adw> docs:makeSite
sbt:adw> docs:makeMicrosite
```

or 

```SBT
sbt:adw> docs / tut
sbt:adw> docs / makeSite
sbt:adw> docs / makeMicrosite

But can instead be written as:

```SBT
sbt:adw> tut
sbt:adw> makeSite
sbt:adw> makeMicrosite
```

Small note: Jekyl's preview command does not seem to work correctly:

```SBT
docs / previewSite
```


----------


[^1]: Short notes on [generating and publishing](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/){:target="_blank"} pages to be hosted on `GitLab.io`.

[^2]: SBT Microsites on [GitHub](https://github.com/47deg/sbt-microsites){:target="_blank"}.

[^3]: Static here means that the pages are not generated on the fly (for example using PHP). However pages may have dynamic content using, for example, JavaScript.

[^4]: A [list](https://github.com/47deg/sbt-microsites#sbt-microsites-in-the-wild){:target="_blank"} of some Scala projects using SBT Microsites.

[^5]: One could risk and install the latest version but we prefer the minimum version specified in the SBT Microsite installation documentation. 

[^6]: The includes can be parameterized by passing in variables, some of which are are [predefined](https://jekyllrb.com/docs/variables/){:target="_blank"} by Jekyll. 

[^7]: Default: `micrositeJsDirectory := (resourceDirectory in Compile).value / "site" / "scripts"`

[^8]: The GitHub [sbt-site](https://github.com/sbt/sbt-site){:target="_blank"}.

[^9]: Not compatible with [Jekyll menus](https://jekyllrb.com/tutorials/navigation/){:target="_blank"}.

[^10]: Issue reported [here](https://github.com/rbenv/ruby-build/issues/1215){:target="_blank"}.

[^11]: Information on BASH [startup files](https://www.gnu.org/software/bash/manual/html_node/Bash-Startup-Files.html){:target="_blank"}.
