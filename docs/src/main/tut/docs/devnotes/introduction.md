---
layout: docs
title:  "Introduction"
section: "devnotes"
---

# Introduction

In this section we include several notes that will help developers uses, test and contribute to the platform. This 
includes the use of the Scala Build Tool (SBT), setting up the CI configuration to automate building, testing and 
documentation generation and tipson using the IntelliJ IDE.


## SBT projects

The (interactive) [Scala Build Tool](https://www.scala-sbt.org/){:target="_blank"} is a JVM based build tool developed 
in Scala. It can be used on any JVM based language. However it is particularly suited for Scala development. Here we 
review some basic commands to get you started using this project (for example running tests and examples, contributing 
code or documentation). For a more in-depth look at this tool consult the online
[documentation](https://www.scala-sbt.org/documentation.html){:target="_blank"}

The SBT build process is configured via a `build.sbt` file in the project's root. A specialized DSL can be used to 
specify the project or sub-projects, dependencies among them, the library dependencies for each sub-project and 
compilation flags required for configuration and set-up. The platform is divided into several sub-projects that 
have dependencies among them. These sub-projects are defined in the `build.sbt` file using the `project` DSL 
construct. For example the `core` project is defined so:

```Scala
lazy val root = (project in file("core"))
```

The SBT DSL uses a *fluent*[^1] API to set up the sub-projects. The example below shows the set-up of the `core` 
sub-project:

```Scala
lazy val MLTest = config("ml") extend(Test)

lazy val root = (project in file("core"))
  //.dependsOn(macroSub)
  .configs(MLTest)
  .settings(
    commonSettings,
    libraryDependencies ++= core_libs,
    libraryDependencies += scalactic,
    libraryDependencies += scalatest,
    inConfig(MLTest)(Defaults.testSettings),
    libraryDependencies += scalatest % MLTest,
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-explaintypes",
      "-Ypatmat-exhaust-depth", "off"),
  )
```

The `file` construct indicates that the source code for the project is in the `core`directory. SBT expects a pre-defined 
directory structure. In the `core/src` directory we have the following directories:
* **main**: the main source code tha makes up the platform. Here you find both **Java** and **Scala** code. Any 
additional sources are only used as reference. For example *R*, *Matlab* and *Octave* code used for prototyping and 
testing are also found here, but they are neither compiled or tested;
* **test**: the unit tests used to test the core functionality is found here. The test suites are coded using the 
[ScalaTest](http://www.scalatest.org/){:target="_blank"} framework. This framework provides easy to understand DSL 
constructs and is well integrated into the SBT tool. Only Java and Scala code is tested;
* **ml**: this is also a unit test directory that uses [ScalaTest](http://www.scalatest.org/){:target="_blank"}, however 
it is reserved for tests that consume many CPU cycles. This includes code to test ML algorithms on large data sets, test 
parameter tuning techniques such as cross validation. Only Java and Scala code is tested.

In the example above all `core` source is found in the `core/src` directory. Because the `ml` test directory is not a 
standard SBT directory, it has to be explicilty defined using the following commands:

```Scala
lazy val MLTest = config("ml") extend(Test)

lazy val root = (project in file("core"))
  .configs(MLTest)
  .settings(
    inConfig(MLTest)(Defaults.testSettings),
   libraryDependencies += scalatest % MLTest
  )
```

Basically we create a `MLTest` target by extending the default SBT `Test` target. We then indicate in the `core` project 
that an additional target exists. This new target is given a name, which is *"MLTest"* (and **not** *"ml"*). We can then 
use this name to explicilty refer to the tests in the `core/src/ml` directory. 

For the development of code we need to indicate what libraries are required. These libraries are defined using the 
`libraryDependencies` construct. The example below shows how we define the dependency on these libraries. More 
concretely we can add the dependencies directly, via a variable holding a single library or a variable holding a 
list of these libraries. 

```Scala
val scalatest = "org.scalatest" %% "scalatest" % "3.0.5" % "test" 

lazy val root = (project in file("core"))
  .settings(
    commonSettings,
    libraryDependencies ++= core_libs,
    libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.5" % "test",
    libraryDependencies += scalatest,
    libraryDependencies += scalatest % MLTest,
  )
```

For example the *Scalactic* library is indicated directly using the SBT dependency string. *ScalaTest* on the other hand 
is defined via a variable `scalatest`. And the core list of dependencies is listed in `core_libs`. We use variables
when the library dependencies are reused in different sub-projects. Whenever you need to add a new library, search 
the [Maven repository](https://mvnrepository.com/){:target="_blank"} (or any other repository) for the required library. 
Copy and paste, from the SBT tab, the artifact identifier string to the `build.sbt` file accordingly. Whenever SBT 
(re)loads the `build.sbt` it will use [Apache Ivy](http://ant.apache.org/ivy/){:target="_blank"} to search for and 
download the libraries to your local disk. 

The library dependencies can also be associated to a target. In the example above the `% "test"` indicates to SBT that
these libraries are ony required by the test target. This means that when we generate a library for the `core` 
sub-project, the test libraries need not be included in the final *Jar* assembly. 

**Note:** Whenever you change the `build.sbt` file you need to reload it either by explicilty restarting SBT or 
issuing the `reload` command (see below for some basic information on SBT commands).

Whenever the output of a target is used by another target or sub-project, SBT automatically established the 
dependency. Any call for a target will call those dependent targets or sb-projects first. SBT does this by 
identifying for example that Java code in one sub-project is used by another Scala sub-project. However, we have
to establish this dependency explicitly when the generated artifacts are unknown to SBT. In the example below
we indicate tha the micro-site documentation, that is built by the `docs` sub-project depends on the build of 
the `root` (core) project: 

```Scala
lazy val docs = (project in file("docs"))
  .dependsOn(root)
  .settings(moduleName := "docs")
  .settings(commonSettings)
  .settings(micrositeSettings: _*)
  .settings(
    siteSubdirName := "api",
    addMappingsToSiteDir(mappings in packageDoc in Compile in root, siteSubdirName)
   )
  .enablePlugins(MicrositesPlugin)
  .enablePlugins(TutPlugin)
  .enablePlugins(SiteScaladocPlugin)
```

In this project we also include the Scaladoc API that is generated by the `doc`


## SBT commands: getting started

In order to get you started using the platform code we will review some basic SBT commands that will allow you to, 
compile and test the code. You can easily add your own source code and use the platform. SBT can be use used both
as a command or in interactive mode. We will review the use of SBT in interactive mode. First open a terminal at 
the root directory of the project and then launch the SBT server[^2] as follows:

```BASH
user@machine:~$ cd git/adw
user@machine:~/git/adw$ sbt
[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/git/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/git/adw/)
[info] sbt server started at local:/home/user/.sbt/1.0/server/434504055fb0c15104d0/sock
sbt:adw> 
```

When SBT is launched it first checks its [configuration](sbt-configuration) and if necessary downloads the required 
version of the SBT used by the project (in other words your locally installed version opf SBT need not be the same 
as the project SBT version). It then checks for the required plugins and if necessary downloads those. Finally it checks
the `build.sbt` file project settings and makes sure that the required libraries are downloaded. Ths may take a while 
depending on your network bandwidth. 

Once in interactive mode, any changes to the configurations or settings are **not** updated. You can restart the SBT 
server, but this is time-consuming. Alternatively reload the the configurations or settings by issuing a `reload` 
command so:

```BASH
sbt:adw> reload
[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/git/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/git/adw/)
```

We have several sub-projects and these can be listed using the `projects`:

```sbtshell
sbt:adw> projects
[info] In file:/home/user/git/adw/
[info] 	 * adw
[info] 	   docs
[info] 	   macroSub
[info] 	   root
```

The `*` indicates which project has been selected. in the example above no sub-projects has been selected because the
platform `adw` is selected. This means that any commands we issue will be applied to all of the sub-projects if they 
have a corresponding target. For example both `root`, which is the `core` sub-project, `macroSub` have the `compile`
and `test` targets. Issuing such a command will apply it to all sub-projects.

In order to restart afresh we can issue the `clean` command so:

```sbtshell
sbt:adw> clean
[success] Total time: 1 s, completed 19/nov/2030 15:57:42
```

This command removes all artifacts generated by the SBT targets. This means that a `compile` command will start 
afresh. Any subsequent compilations will only compile the source code that was changed and any source code that 
depends on these cages. This is because SBT uses an incremental compiler in order to reduce compilation time 
(Scala compilation can take long due to type inference). Here is an example of a fresh compilation[^3]:

```sbtshell
sbt:adw> compile
[info] Updating ...
[info] Updating root...
[info] Updating macroSub...
[info] Done updating.
[warn] There may be incompatibilities among your library dependencies.
[warn] Run 'evicted' to see detailed eviction warnings
[info] Compiling 2 Scala sources to /home/user/git/adw/macro/target/scala-2.12/classes ...
[info] Done updating.
[info] Done updating.
[warn] There may be incompatibilities among your library dependencies.
[warn] Run 'evicted' to see detailed eviction warnings
[warn] 6 warnings found
[info] Done compiling.
[info] Updating docs...
[info] Compiling 75 Scala sources and 17 Java sources to /home/user/git/adw/core/target/scala-2.12/classes ...
[info] Done updating.
[info] Done compiling.
[success] Total time: 58 s, completed 19/nov/2031 16:30:28
```

**Note:** that the tests are **not** automatically compiled. These are either implicitly compiled when we execute the 
`test` or `testQuick` commands (triggered by target dependency) or explicitly by using the following command:

```sbtshell
sbt:adw> test:compile
[info] Compiling 1 Scala source to /home/user/git/adw/macro/target/scala-2.12/test-classes ...
[info] Compiling 25 Scala sources and 5 Java sources to /home/user/git/adw/core/target/scala-2.12/test-classes ...
[info] Done compiling.
[info] Done compiling.
[success] Total time: 55 s, completed 19/nov/2038 16:49:21
```

The `test` qualifier used before the `compile` command tells SBT to apply the compile command to the test target only. 
Note that we do not stipulate the project so any project that has a test target will be compiled. We could also 
indicate the project specifically. Here we clean and recompile the `root` project for the `test` target only:
[^4]:

```sbtshell
[success] Total time: 2 s, completed 19/nov/2038 17:01:06
sbt:adw> root / test:clean
[success] Total time: 0 s, completed 19/nov/2038 17:01:16
sbt:adw> root / test:compile
[info] Updating root...
[info] Done updating.
[warn] There may be incompatibilities among your library dependencies.
[warn] Run 'evicted' to see detailed eviction warnings
[info] Compiling 75 Scala sources and 17 Java sources to /home/user/git/adw/core/target/scala-2.12/classes ...
[info] Done compiling.
[warn] 7 warnings found
[info] Done compiling.
[success] Total time: 76 s, completed 19/nov/2040 17:02:37
```

Of-course if we had issued the `compilation` command for the test target, with no indication of the project, the results 
should be the same. 

Scala or Java source code projects also have a `doc` command that uses 
[Scaladoc](https://docs.scala-lang.org/style/scaladoc.html){:target="_blank"} to generate the API based on the code 
comments. It uses specialized tags to aid in the technical 
[documentation](https://docs.scala-lang.org/overviews/scaladoc/for-library-authors.html){:target="_blank"} of source 
code. It is the same tool used to document the [Scala API](https://www.scala-lang.org/api/current/).  In order to 
generate the API documentation the `doc`command must be used, which results in:

```sbtshell
sbt:adw> doc
[info] Main Scala API documentation to /home/user/git/adw/macro/target/scala-2.12/api...
[info] Main Scala API documentation to /home/user/git/adw/core/target/scala-2.12/api...
model contains 10 documentable templates
model contains 819 documentable templates
[info] Main Scala API documentation successful.
[success] Total time: 37 s, completed 19/nov/2032 16:43:37
``` 

Once the project has been compiled we should execute the tests to check that everything is working as expected. As with
the commands above we can indicate the targets and/or sub-projects. We will review some additional options that make 
compilation and testing more interactive and therefore effective. Many of the platform's tests require data that is 
located at the projects root: `adw/data`. This directory is not associate with any sub-projects and does not follow
SBT's directory conventions. The platform's test also generate temporary data used in the tests. All of this data is 
placed in the **OS's temporary directory** and is therefore automatically removed on a reboot. We use 
[ScalaTest](http://www.scalatest.org/){:target="_blank"} to develop and run test suites. 

If we issue a `test` command with no target and/or sub-project, all test targets in any project are executed. All 
test suites in any project will always be executed. The example below shows how all of the test suites can be execute: 

```sbtshell
sbt:adw> test
[info] TasksSpec:
[info] Tasks
[info] - should allow compilation and execution of a single normalize function
[info] - should allow compilation and execution of a function that returns multiple parameters/results
[info] - should allow compilation and execution of a set of functions
[info] LoadStreamSpec:
[info] Row Vals
[info] - should be automatically created via implicits
[info] - should allow implicit conversions
[info] Rows
[info] - should should allow manipulation of Val
...
[info] HilbertSpec:
[info] Hilbert transform
[info] - should produce correct analytical signal
[info] DataFilesSpec:
[info] Load files
[info] - should partially load the start of data files
[info] - should split data into train and tests streams
[info] - should partially load the end of data files
[info] - should intersperse data files with test protocol with basic types for keys
[info] - should intersperse data files with test protocol with Va[_] types for keys
[info] Better-files
[info] - should handle the partitioning of files
[info] FCCAnomalyDetectorSpec:
[info] Anomaly detector
[info] - should work in a single pipe detector
[info] - should multiple pipe detector
[info] Run completed in 9 minutes, 41 seconds.
[info] Total number of tests run: 228
[info] Suites: completed 23, aborted 0
[info] Tests: succeeded 228, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
[success] Total time: 584 s, completed 19/nov/2035 17:30:16
```

Tests are usually executed in parallel (depending on the availability of multi-cores). At the end of executing all of 
the test suites in a project, ScalaTest reports on the number of tests executed and how many failed. Any failure will 
indicate the file and test name so we can easily identify and correct the problem. Tests that passed are shown in 
*green*{: style="color: green"}. Tests that fail are shown in *red*{: style="color: red"}. The results of a 
project's test are shown in *blue*{: style="color: blue"}. If a project has no test suites the results show a zero 
count for the number of tests suites and aborted test suites. The total number of tests that succeeded, failed, were 
canceled, ignored or are pending are also shown as 0. 

Note that we could also execute the same command above in non interactive session so:

```sbtshell
user@machine:~$ cd git/adw
user@machine:~/git/adw$ sbt test
[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/git/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/git/adw/)
[info] Run completed in 18 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] ParamSearchSpec:
[info] Simple grid search strategy
[info] - should generate typed values
...
[info] Better-files
[info] - should handle the partitioning of files
[info] FCCAnomalyDetectorSpec:
[info] Anomaly detector
[info] - should work in a single pipe detector
[info] - should multiple pipe detector
[info] Run completed in 9 minutes, 47 seconds.
[info] Total number of tests run: 228
[info] Suites: completed 23, aborted 0
[info] Tests: succeeded 228, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
```

The number of tests is large and some tests take several seconds. All added, running the full set of test suites is not 
practical. This is essentially reserved for continuous integration (CI) in GitLab. However, before committing and 
pushing your changes to the repository, **make sure all tests have passed**. Run the **full set** of test suites before
pushing your changes. 

During development we should however avoid running all of the tests. We can do this by executing only some of the tests.
We can explicitly select and execute a single test suite by indicating the Scala test class as shown below:

```sbtshell
sbt:adw> testOnly pt.inescn.samplers.stream.OpsSpec
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for Test / testOnly
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for macroSub / Test / testOnly
[info] Run completed in 12 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for docs / Test / testOnly
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators
...
[info] Combining samplers
[info] - should allow the creation of a discrete search space
[info] - should allow the creation of a random search space
[info] - should allow repeating sampling of combined samplers
[info] Run completed in 710 milliseconds.
[info] Total number of tests run: 27
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 27, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
[success] Total time: 3 s, completed 20/nov/2043 11:36:25
```

Note that in the example above SBT will scan all sub-projects if the sub-project is not indicated. We can also indicate 
the sub-project to scan as shown below (make sure the project name is set before the class name):

```sbtshell
sbt:adw> root / testOnly pt.inescn.samplers.stream.OpsSpec
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators
...
[info] Combining samplers
[info] - should allow the creation of a discrete search space
[info] - should allow the creation of a random search space
[info] - should allow repeating sampling of combined samplers
[info] Run completed in 590 milliseconds.
[info] Total number of tests run: 27
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 27, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
[success] Total time: 1 s, completed 20/nov/2044 11:44:55
```

The SBT commands also accepts *globbing* and this can be used to great effect with `test`. The next example shows how we
can select all tests under a given package. With this selection the only two test suites (`OpsSpec`and `SamplerSpec`) in
the packages below `pt.inescn.samplers` were executed.

```sbtshell
sbt:adw> root/ testOnly pt.inescn.samplers.*
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators
...
[info] Combining samplers
[info] - should allow the creation of a discrete search space
[info] - should allow the creation of a random search space
[info] - should allow repeating sampling of combined samplers
[info] SamplerSpec:
[info] Simple numeric ranges
[info] - should generate a list of samples
[info] - should generate a stream of samples
...
[info] Java discrete multi-class generator
[info] - should generate correct multi-class distribution
[info] Scala discrete multi-class generator
[info] - should generate correct multi-class distribution
[info] - should generate correct multi-class distribution dynamically
[info] AppendB operations of samplers
[info] - should generate a single discrete sample
[info] - should generate a single continuous sample
[info] Run completed in 699 milliseconds.
[info] Total number of tests run: 64
[info] Suites: completed 2, aborted 0
[info] Tests: succeeded 64, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
[success] Total time: 1 s, completed 20/nov/2045 13:16:23
```

Tests that are not pure unit tests (test only basic functionality) and take **significant time** must be placed in the 
**`MLTest` target** [^5] of the core project (or in any other sub-project that has such a target). One can use an 
interactive SBT session to execute these tests, however it may be best to execute this as a (background) SBT command 
so:

```sbtshell
mf@machine:~/git/adw$ sbt "root / MLTest / testOnly pt.inescn.model.ModelSpecLong"
[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/git/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/git/adw/)
```

Note that here we **must indicate the sub-project** name because the new `MLTest` is not a standard SBT target. If we do 
not SBT will not recognize the target and exit with an error. Here is the same command but in interactive mode:

```sbtshell
sbt:adw> root / MLTest / testOnly pt.inescn.model.ModelSpecLong
```

Another option is to use the `testQuick` command; it executes only those tests that had previously failed. Assume we 
executed all test suites and the `pt.inescn.samplers.stream.OpsSpec` test failed. Instead of selectively picking 
the tests that failed and executing these, simply execute the `testQuick` optionally indicating the target packages.
In the first example, `testQuick` is applied to all test suites in any project and picks up what failed previously:

```sbtshell
sbt:adw> testQuick
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for Test / testQuick
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for macroSub / Test / testQuick
[info] Run completed in 14 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for docs / Test / testQuick
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators *** FAILED ***
[info]   Ops((a,1), (b,2), (c,1), (a,2), (b,1), (c,2)) did not contain the same elements as List((a,2), (b,2), (c,1), (a,2), (b,1), (c,2)) (OpsSpec.scala:95)
[info] Enumeration
[info] - should allow generating ranges
[info] - should produce a series of timestamps
...
[info] Run completed in 702 milliseconds.
[info] Total number of tests run: 27
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 26, failed 1, canceled 0, ignored 0, pending 0
[info] *** 1 TEST FAILED ***
[error] Failed tests:
[error] 	pt.inescn.samplers.stream.OpsSpec
[error] (root / Test / testQuick) sbt.TestsFailedException: Tests unsuccessful
[error] Total time: 2 s, completed 20/nov/2046 14:14:03
```

In the next example we indicate the project and use globbing:

```sbtshell
sbt:adw> testQuick root / pt.inescn.samplers.*
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for Test / testQuick
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for macroSub / Test / testQuick
[info] Run completed in 10 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for docs / Test / testQuick
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators *** FAILED ***
[info]   Ops((a,1), (b,2), (c,1), (a,2), (b,1), (c,2)) did not contain the same elements as List((a,2), (b,2), (c,1), (a,2), (b,1), (c,2)) (OpsSpec.scala:95)
[info] Enumeration
[info] - should allow generating ranges
[info] - should produce a series of timestamps
...
[info] Run completed in 654 milliseconds.
[info] Total number of tests run: 27
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 26, failed 1, canceled 0, ignored 0, pending 0
[info] *** 1 TEST FAILED ***
[error] Failed tests:
[error] 	pt.inescn.samplers.stream.OpsSpec
[error] (root / Test / testQuick) sbt.TestsFailedException: Tests unsuccessful
[error] Total time: 2 s, completed 20/nov/2047 14:16:37
```
These commands can also be executed in non interactive mode. The following are two example. One with globbing:

```sbtshell
user@machine:~/git/adw$ sbt "testQuick pt.inescn.*"
[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/git/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/git/adw/)
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for Test / testQuick
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for macroSub / Test / testQuick
[info] Run completed in 19 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for docs / Test / testQuick
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators *** FAILED ***
[info]   Ops((a,1), (b,2), (c,1), (a,2), (b,1), (c,2)) did not contain the same elements as List((a,2), (b,2), (c,1), (a,2), (b,1), (c,2)) (OpsSpec.scala:95)
...
[info] Run completed in 1 second, 102 milliseconds.
[info] Total number of tests run: 27
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 26, failed 1, canceled 0, ignored 0, pending 0
[info] *** 1 TEST FAILED ***
[error] Failed tests:
[error] 	pt.inescn.samplers.stream.OpsSpec
[error] (root / Test / testQuick) sbt.TestsFailedException: Tests unsuccessful
[error] Total time: 3 s, completed 20/nov/2081 14:21:46
```

And the other without:

```sbtshell
user@machine:~/git/adw$ sbt "testQuick"
[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/git/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/git/adw/)
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for Test / testQuick
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for macroSub / Test / testQuick
[info] Run completed in 17 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for docs / Test / testQuick
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators *** FAILED ***
[info]   Ops((a,1), (b,2), (c,1), (a,2), (b,1), (c,2)) did not contain the same elements as List((a,2), (b,2), (c,1), (a,2), (b,1), (c,2)) (OpsSpec.scala:95)
...
[info] Run completed in 920 milliseconds.
[info] Total number of tests run: 27
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 26, failed 1, canceled 0, ignored 0, pending 0
[info] *** 1 TEST FAILED ***
[error] Failed tests:
[error] 	pt.inescn.samplers.stream.OpsSpec
[error] (root / Test / testQuick) sbt.TestsFailedException: Tests unsuccessful
[error] Total time: 3 s, completed 20/nov/2081 14:21:19
```

In order to make your workflow more efficient we suggest using SBT's 
[triggered execution](https://www.scala-sbt.org/1.x/docs/Triggered-Execution.html){:target="_blank"}. SBT commands may 
be set on *standby* so that when any dependent target changes, the command is automatically triggered and returns back 
to *standby* mode awaiting new changes. To trigger a command simply prepend it with the tilde `~`. For example if we use 
the `~compile` command and then change a source file, incremental compilation will be executed automatically. We can 
use this to automate our code, compile and test cycle. In the example below we see three iterations of this cycle:

```sbtshell
sbt:adw> ~testQuick 
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for Test / testQuick
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for macroSub / Test / testQuick
[info] Run completed in 16 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for docs / Test / testQuick
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators *** FAILED ***
[info]   Ops((a,1), (b,2), (c,1), (a,2), (b,1), (c,2)) did not contain the same elements as List((a,2), (b,2), (c,1), (a,2), (b,1), (c,2)) (OpsSpec.scala:95)
[info] Enumeration
[info] - should allow generating ranges
[info] - should produce a series of timestamps
...
[info] Combining samplers
[info] - should allow the creation of a discrete search space
[info] - should allow the creation of a random search space
[info] - should allow repeating sampling of combined samplers
[info] Run completed in 971 milliseconds.
[info] Total number of tests run: 27
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 26, failed 1, canceled 0, ignored 0, pending 0
[info] *** 1 TEST FAILED ***
[error] Failed tests:
[error] 	pt.inescn.samplers.stream.OpsSpec
[error] (root / Test / testQuick) sbt.TestsFailedException: Tests unsuccessful
[error] Total time: 3 s, completed 20/nov/2081 14:25:33
1. Waiting for source changes in project adw... (press enter to interrupt)
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for Test / testQuick
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for macroSub / Test / testQuick
[info] Compiling 1 Scala source to /home/user/git/adw/core/target/scala-2.12/test-classes ...
[info] Run completed in 17 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for docs / Test / testQuick
[info] Done compiling.
[info] OpsSpec:
[info] Combination operators
[info] - should generate sample iterators
[info] Enumeration
[info] - should allow generating ranges
[info] - should produce a series of timestamps
[info] Real distribution
...
[info] - should allow repeating sampling of combined samplers
[info] Run completed in 603 milliseconds.
[info] Total number of tests run: 27
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 27, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
[success] Total time: 13 s, completed 20/nov/2081 14:25:59
2. Waiting for source changes in project adw... (press enter to interrupt)
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for Test / testQuick
[info] Passed: Total 0, Failed 0, Errors 0, Passed 0
[info] No tests to run for macroSub / Test / testQuick
[info] Run completed in 28 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for root / Test / testQuick
[info] Run completed in 14 milliseconds.
[info] Total number of tests run: 0
[info] Suites: completed 0, aborted 0
[info] Tests: succeeded 0, failed 0, canceled 0, ignored 0, pending 0
[info] No tests were executed.
[info] No tests to run for docs / Test / testQuick
[success] Total time: 1 s, completed 20/nov/2081 14:26:11
3. Waiting for source changes in project adw... (press enter to interrupt)
...
```

In the first cycle the quick test executes and fails a test that had already previously failed. We then correct the 
source code and SBT detects the changes and runs the test again. At this point the test succeeds. It is **important 
to note** that if we make any additional changes, even if they they inadvertently break a test, **no additional 
tests will be executed**. So in the third iteration no tests are executed. In order to run tests either force the 
execution using the `test` selectively or change the test source code itself. What one does normally is to work on 
a test suite adding new tests as you get the previous tests to succeed. When you are done, move onto the next test suite
and so on. **Regularly run a the full to ensure no regression errors have not been introduced.** If not you run the risk 
of introducing incompatible changes that will be difficult to fix later. 

It is **good practice** to add to the API documentation header the appropriate **test suite command** so that anyone 
that wants to run this specific test need only copy and paste the command. Make sure to add this to any test suites you 
add to the source code. 

As was shown, the `test`, `testOnly` and `testQuick` can also be used with a class name as seen below. 

```sbtshell
testOnly pt.inescn.etl.stream.DataFilesSpec
```

```sbtshell
~testOnly pt.inescn.etl.stream.LoadStreamSpec
```

However, if we select a single test suite we can execute this class directly so:

```sbtshell
root / MLTest / testOnly pt.inescn.model.ModelSpecLong
```

without using a command. In the same fashion we can also issue a `run` command that will scan for and execute the only
main function that exists. If more than one class exists then we have to use the `runMain` command. Note that in this 
case *globbing* does not make sense. in the next example we show how we can execute a `main` and pass parameters to it. 
Here we must also indicate the sub-project, otherwise the class will not be found and executed.

```sbtshell
sbt:adw> root/runMain inegi.ADIRAI40Experiments 4
[warn] Multiple main classes detected.  Run 'show discoveredMainClasses' to see the list
[info] Packaging /home/user/git/adw/core/target/scala-2.12/adw_2.12-0.2.1.jar ...
[info] Done packaging.
[info] Running inegi.ADIRAI40Experiments 4
Executing experiment 4.
```

To exit a SBT session either execute the `exit` command (shown below) or press the keys `Ctrl-D`. 

```sbtshell
sbt:adw> exit
[info] shutting down server
```

We have set up a `docs` sub-project to publish the platform's web site via GitLabs CI hosted on *Gitlab.io* (detailed 
information can be found here [here](microsite.html)). We have made this sub-project depend on the core `root` 
sub-project so we only generate this documentation if compilation succeeds. We have also added the API `doc` task to 
this sub-project so that it is always executed when a command is run on it. To generate the full static web-site we need 
to execute the 
[`makeMicrosite`](https://47deg.github.io/sbt-microsites/docs/build-the-microsite.html#build-the-microsite){:target="_blank"}
command as shown below:

```sbtshell
sbt:adw> makeMicrosite
[info] Running tut.TutMain /home/user/git/adw/docs/src/main/tut /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll .*\.(md|markdown|txt|htm|html)
[tut] compiling: /home/user/git/adw/docs/src/main/tut/publications.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/contributing.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/resources.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/pipes.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/tasks.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/installation.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/search.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/katex.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/relativeurl.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/mermaid.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/microsite.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/introduction.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/index.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/introduction.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/resources/submenu1.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/resources/submenu2.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/index.md
Copying from /home/user/git/adw/CHANGELOG.md to /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md/ExtraMdFileConfig(changelog.md,page,Map(title -> Changelog, section -> changelog, position -> 99))
Copying from /home/user/git/adw/LICENSE to /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md/ExtraMdFileConfig(license.md,page,Map(title -> License, section -> license, position -> 100))
[info] Running tut.TutMain /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll .*\.(md|markdown|txt|htm|html)
[tut] compiling: /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md/changelog.md
[tut] compiling: /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_extra_md/license.md
[info] Main Scala API documentation to /home/user/git/adw/core/target/scala-2.12/api...
[info] Configuration file: /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll/_config.yml
[info]             Source: /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll
[info]        Destination: /home/user/git/adw/docs/target/jekyll
[info]  Incremental build: disabled. Enable with --incremental
[info]       Generating... 
[info]                     done in 2.02 seconds.
[info]  Auto-regeneration: disabled. Use --watch to enable.
model contains 819 documentable templates
[info] Main Scala API documentation successful.
[success] Total time: 44 s, completed 19/nov/2038 17:32:21
```

This command runs the [`tut`](http://tpolecat.github.io/tut/){:target="_blank"} command that processes the Scala code 
sheds, it then executes the `doc` command (if it is not up to date. In the example above the API docs are up to date) 
and finally processes and copies the Markdown files as per the Microsite configuration. The files are copied to the 
`target` site directory. The publishing of the static web-site now in the target directory is done via GitLab's CI. This 
means that we have to execute this process during CI and configure the CI script to 
[publish](microsite.html#microsite-ci-configuration){:target="_blank"} the results. The CI is configured to generate and 
publish the web-site only when the `master` branch is checked into the repository. 

The `tut` command can be execute directly as shown below:

```sbtshell
sbt:adw> tut
[info] Running tut.TutMain /home/user/git/adw/docs/src/main/tut /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll .*\.(md|markdown|txt|htm|html)
[tut] compiling: /home/user/git/adw/docs/src/main/tut/publications.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/contributing.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/resources.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/pipes.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/tasks.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/installation.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/search.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/katex.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/relativeurl.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/mermaid.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/microsite.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/devnotes/introduction.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/index.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/introduction.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/resources/submenu1.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/docs/resources/submenu2.md
[tut] compiling: /home/user/git/adw/docs/src/main/tut/index.md
[success] Total time: 15 s, completed 21/nov/2038 10:52:19
```

The `tut` command actually *compiles* the code in a Scala 
[REPL](https://docs.scala-lang.org/overviews/repl/overview.html){:target="_blank"} session (Read-Eval(uate)-Print-Loop). 
This process is time consuming and error prone because the code must be correct. In order to make things easier we 
suggest you first code the sheds in an IDE and only then use it in the Markdown files. Second, when checking that the 
Markdown sheds compile successfully, just execute the `tut` command and not the `makeMicrosite`. Note that sheds in a 
single Markdown file are compiled in sequence so one shed can build up ion the previous ones (for example `import` 
statements need not be repeated and variables carry through). 
 
Unlike the commands above these commands (as of this writing) do **not work with triggered execution (~).** The 
Microsite plugin also provides equivalent functionality with the `previewSite` and `previewAuto` commands however 
these did not seem to work well[^6]. 

For more details on the `docs` sub-project take a look at for example information on the 
[set-up of the microsite](microsite.html#setting-up-sbt-micro-site) and the use of the [KaTex](katex.html) and 
[MermaidJS](mermaid.html) plugins described in this section. 

### Debugging SBT

Sometimes we need a little insight into the values of the `build.sbt` files so that we can change configuration 
correctly. The first command we can use allows us to list the main and sub-projects that have been configured shown 
next:

```sbtshell
sbt:adw> projects
[info] In file:/home/user/git/adw/
[info] 	 * adw
[info] 	   docs
[info] 	   macroSub
[info] 	   root
sbt:adw> 
```

For any project or sub-project we can list the values of a given build target or configuration value. In the example 
below we list the configured target directories values (were the build artifacts are placed) of the project (lists 
for all sub-projects) and for a sub-project in particular. Note that we must stipulate the full path (no *globbing*). 

```sbtshell
sbt:adw> show adw / target
[info] macroSub / target
[info] 	/home/user/git/adw/macro/target
[info] docs / target
[info] 	/home/user/git/adw/docs/target
[info] root / target
[info] 	/home/user/git/adw/core/target
[info] target
[info] 	/home/user/git/adw/target
sbt:adw> show root / target
[info] /home/user/git/adw/core/target
```

Here we do the same the same as above and can see that the compilation artifacts are placed in the general target 
directory. 

```sbtshell
sbt:adw> show root / compile / target
[info] /home/user/git/adw/core/target
```


The `show` command lets us see that the values of the configuration variables are, whether they are SBT's standard 
defaults (such as `target`) or one we define in the `build.sbt` file. For example we defined the `siteSubdirName` 
variable used to indicate the offset directory were the site's API documentation will be copied to. We can see its
value so:

```sbtshell
sbt:adw> show siteSubdirName
[info] api
```

Many times a plugin will have a set of configuration variables tha we can change to our needs. We have created a few
SBT tasks that allow us to view those values. This tasks can also be `inspect`ed to give us additional information. 
In the next example we list the information on one of our *debug* tasks called `printTutConfig`:

```sbtshell
sbt:adw> inspect printTutConfig
[info] Task: Unit
[info] Description:
[info] 	Dump Tut Configuration Variables
[info] Provided by:
[info] 	ProjectRef(uri("file:/home/user/git/adw/"), "adw") / printTutConfig
[info] Defined at:
[info] 	/home/user/git/adw/build.sbt:556
[info] Dependencies:
[info] 	docs / tutPluginJars
[info] 	docs / tutNameFilter
[info] 	docs / tutTargetDirectory
[info] 	Tut / scalacOptions
[info] 	docs / tutSourceDirectory
[info] Delegates:
[info] 	printTutConfig
[info] 	ThisBuild / printTutConfig
[info] 	Global / printTutConfig
```

We can invoke this task either by executing it directly:

```sbtshell
sbt:adw> printTutConfig
printTutClasspath
tutSourceDirectory       = /home/user/git/adw/docs/src/main/tut
tutNameFilter            = .*\.(md|markdown|txt|htm|html)
tutTargetDirectory       = /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll
scalacOptions in Tut     = List(-target:jvm-1.8)
tutPluginJars            = Vector()
[success] Total time: 0 s, completed 21/nov/2058 13:06:47
```
or printing its output (which is `Unit`):

```sbtshell
sbt:adw> show printTutConfig
printTutClasspath
tutSourceDirectory       = /home/user/git/adw/docs/src/main/tut
tutNameFilter            = .*\.(md|markdown|txt|htm|html)
tutTargetDirectory       = /home/user/git/adw/docs/target/scala-2.12/resource_managed/main/jekyll
scalacOptions in Tut     = List(-target:jvm-1.8)
tutPluginJars            = Vector()
[info] ()
[success] Total time: 0 s, completed 21/nov/2058 13:06:43
```

Sometimes we also need to construct paths that depend on other paths. We have for example the `printKatexFontsClassPath` 
and `printAPIDocClasspath` tasks that show us were the *Katex* font files and the site *Scaladoc* API source and 
destination directories are located. Here are examples of their output:

```sbtshell
sbt:adw> printKatexFontsClassPath
printKatexFontsClassPath
target.in(docs).in(Compile).value.getAbsolutePath = /home/user/git/adw/docs/target
crossTarget.in(docs).in(Compile).value.getAbsolutePath = /home/user/git/adw/docs/target/scala-2.12
fonts.value = /home/user/git/adw/docs/src/main/resources/microsite/js/fonts
src =/home/user/git/adw/docs/src/main/resources/microsite/js/fonts
des =/home/user/git/adw/docs/target/site/js/fonts
[success] Total time: 1 s, completed 21/nov/2055 13:27:18
```

and

```sbtshell
sbt:adw> printAPIDocClasspath
printAPIDocClasspath
target.in(root).in(Compile).in(doc) = /home/user/git/adw/core/target/scala-2.12/api
scalacOptions.in(root).in(Compile) = List(-unchecked, -deprecation, -feature, -Xlint, -explaintypes, -Ypatmat-exhaust-depth, off)
[success] Total time: 0 s, completed 21/nov/2055 13:27:43
```

The SBT Microsite configurations are also available via the `printSiteConfig` task. Only part of the configuration is 
show. If you need more just add them to the task. Remember to reload the build file in SBT interactive mode if you 
change it. 

A very useful `inspect` command lets us inspect the SBT tasks and determined for example its dependencies and relations 
with other tasks. In the example below we show the information on the `run` command: 

```sbtshell
sbt:adw> inspect run
[info] Input task: Unit
[info] Description:
[info] 	Runs a main class, passing along arguments provided on the command line.
[info] Provided by:
[info] 	ProjectRef(uri("file:/home/user/git/adw/"), "adw") / Compile / run
[info] Defined at:
[info] 	(sbt.Defaults.configTasks) Defaults.scala:578
[info] Dependencies:
[info] 	Compile / bgJobService
[info] 	Compile / bgRun
[info] Delegates:
[info] 	Compile / run
[info] 	run
[info] 	ThisBuild / Compile / run
[info] 	ThisBuild / run
[info] 	Zero / Compile / run
[info] 	Global / run
[info] Related:
[info] 	Test / run
[info] 	macroSub / Test / run
[info] 	root / Compile / run
[info] 	docs / Tut / run
[info] 	docs / Test / run
[info] 	docs / Compile / run
[info] 	macroSub / Compile / run
[info] 	root / Ml / run
[info] 	root / Test / run
```

Notice how we can see that `run` depends on `compile`. We also that several other tasks and targets depend on `run`. 
More concretely the tests and `tut` sheds can only run if the source is can also be run. Not all of the fields may be 
available and depends on the task developers' input. Here is an example of the SBT Microsite command:

```sbtshell
sbt:adw> inspect makeMicrosite
[info] No entry for key.
[info] Description:
[info] 	Main Task to build a Microsite
[info] Delegates:
[info] 	makeMicrosite
[info] 	ThisBuild / makeMicrosite
[info] 	Global / makeMicrosite
[info] Related:
[info] 	docs / makeMicrosite
```

When SBT loads the configuration file it will will check the library dependencies and load them. During this phase 
multiple (possibly incompatible) versions of the library may be found via transitive dependencies. If you need to check 
the list again use the `evicted` command. It will list the conflicts detected and show which version of the library was 
selected. Here is a partial example:

```sbtshell
sbt:adw> evicted
[info] Updating macroSub...
[info] Updating root...
[info] Updating ...
[info] Updating docs...
[info] Done updating.
[info] Here are other dependency conflicts that were resolved:
[info] 	* com.lihaoyi:sourcecode_2.12:0.1.4 is selected over 0.1.3
[info] 	    +- com.lihaoyi:fansi_2.12:0.2.5                       (depends on 0.1.4)
[info] 	    +- com.lihaoyi:pprint_2.12:0.5.3                      (depends on 0.1.4)
[info] 	    +- com.lihaoyi:fastparse-utils_2.12:1.0.0             (depends on 0.1.4)
[info] 	    +- com.lihaoyi:fastparse_2.12:1.0.0                   (depends on 0.1.4)
[info] 	    +- org.scalameta:common_2.12:3.7.4                    (depends on 0.1.3)
[info] Done updating.
[info] Here are other dependency conflicts that were resolved:
[info] 	* com.github.fommil.netlib:core:1.1.2 is selected over 1.1
[info] 	    +- com.github.fommil.netlib:all:1.1.2                 (depends on 1.1.2)
[info] 	    +- org.scalanlp:breeze_2.12:0.13.1                    (depends on 1.1.2)
[info] 	    +- com.github.fommil.netlib:native_system-java:1.1    (depends on 1.1)
[info] 	    +- com.github.fommil.netlib:native_ref-java:1.1       (depends on 1.1)
[info] 	* org.apache.commons:commons-math3:3.6.1 is selected over 3.2
[info] 	    +- uk.me.berndporr:iirj:1.0                           (depends on 3.6.1)
[info] 	    +- pt.inescn:adw_2.12:0.2.1                           (depends on 3.2)
[info] 	    +- org.scalanlp:breeze_2.12:0.13.1                    (depends on 3.2)
...
```

SBT in interactive mode makes to easy for you to revisit and reuse commands by recording a **history of commands**. This
is loaded whenever you restart an SBT session. To find a command you last executed just *walk* up and down the history 
timeline using the *up* and *down* keys. Enter to select the command. Note that this command history is kept separately 
for the main project and for each sub-project. So if you use the SBT 'project' command a new history list is used. SBT 
in interactive also has **tab completion**. To find a command enter the initial characters of the command and press the 
tab key. The example below shows the sequence for the inputs `com`, `comp` and `compi`. The last input is automatically 
completed.

```sbtshell
sbt:adw> com
commands                      compatibilityWarningOptions   compile                       compile:                      compileAnalysisFilename       compileIncSetup               
compileIncremental            compilerCache                 compilers                     completions                   
sbt:adw> comp
compatibilityWarningOptions   compile                       compile:                      compileAnalysisFilename       compileIncSetup               compileIncremental            
compilerCache                 compilers                     completions                   
sbt:adw> compile
```

## Trying out the Samples

Through the documentation (site) we will use [Tut](http://tpolecat.github.io/tut/){:target="_blank"} to show code 
snippets. Tut will ensure that these snippets will compile and run correctly and can therefore server as examples. To 
run these examples we suggest you use an 
[SBT REPL](https://www.scala-sbt.org/1.x/docs/Command-Line-Reference.html#Configuration-level+tasks){:target="_blank"}
session. To make things easier the Tut sheds use the `:book` [modifier](http://tpolecat.github.io/tut//modifiers.html) 
so that one need only copy and paste the example. **Make sure you use the `:book` modifier when creating your own code 
snippets. To run an example first launch an SBT session, select the project with which you want to run the examples, 
reset the compilation flags and execute the `console` command as shown below:

```sbtshell
user@machine:~/git/adw$ sbt
[info] Loading settings for project global-plugins from idea.sbt ...
[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/git/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/git/adw/)
[info] sbt server started at local:///home/user/.sbt/1.0/server/434504055fb0c15104d0/sock
sbt:adw> projects
[info] In file:/home/user/git/adw/
[info] 	 * adw
[info] 	   docs
[info] 	   macroSub
[info] 	   root
sbt:adw> project root
[info] Set current project to adw (in build file:/home/user/git/adw/)
sbt:adw> set scalacOptions in (Compile, console) := Seq() 
[info] Defining Compile / console / scalacOptions
[info] The new value will be used by Compile / console, Compile / consoleQuick
[info] Reapplying settings...
[info] Set current project to adw (in build file:/home/user/git/adw/)
sbt:adw> console
[info] Starting scala interpreter...
Welcome to Scala 2.12.7 (Java HotSpot(TM) 64-Bit Server VM, Java 1.8.0_181).
Type in expressions for evaluation. Or try :help.

scala> 
```

We have reset the compilation flags so that the *linting* messages not confuse the user. However any flag that is used 
in the `build.sbt` file can also be used. Consult the build file and the `scalac` compiler to see what can be used by 
the `scalacOptions` variable. Here are some examples (note that `+=` and `++=` keep the project's existing settings):

Don't forget to set the `project adw` to activate the main project settings (exit from a sub-project).

```sbtshell
:settings -Xprint:typer
set scalacOptions in (Compile, console) := Seq("-Xprint:typer") 
set scalacOptions in (Compile, console) += "-Xprint:typer"
set scalacOptions in ThisBuild ++= Seq("-Xprint:namer", "-Xprint:typer")
```

You can now start using the REPL. In the example below we create two variables and print out the evaluation of the 
expression `x + y`. Notice that tab completion is also available:

```sbtshell
scala> val x = 1
x: Int = 1

scala> val y = 2
y: Int = 2

scala> print
print   printf   println

scala> print
   def print(x: Any): Unit

scala> println(x + y)
3

scala> 
```

Here is a copy & paste session from a example of the [core search package](../docs/index.html#core-search) (removed 
some blank lines to make it easier to read):

```scala
scala> import pt.inescn.search.stream.Pipes.Executing
import pt.inescn.search.stream.Pipes.Executing

scala> import pt.inescn.search.stream.Tasks._
import pt.inescn.search.stream.Tasks._

scala> // import pt.inescn.search.stream.Tasks._
scala> import pt.inescn.utils.ADWError
import pt.inescn.utils.ADWError

scala> // import pt.inescn.utils.ADWError
scala> import pt.inescn.search.stream.Pipes.Executing
import pt.inescn.search.stream.Pipes.Executing

scala> // import pt.inescn.search.stream.Pipes.Executing
scala> // The function
scala>     def concatFunc(ap:String, i:String):Either[ADWError, String] = Right(i + ap)
concatFunc: (ap: String, i: String)Either[pt.inescn.utils.ADWError,String]

scala> // concatFunc: (ap: String, i: String)Either[pt.inescn.utils.ADWError,String]
scala>     // Its parameter value

scala>     val ap = "_3"
ap: String = _3

scala> // ap: String = _3
scala>     // Create the task

scala>     val concat =  ("concat", concatFunc _, ap).T
concat: pt.inescn.search.stream.Pipes.Func[String,String,Unit] = Func(concat(List(_3)))

scala> // concat: pt.inescn.search.stream.Pipes.Func[String,String,Unit] = Func(concat(List(_3)))
```

Notice that we copy and pasted the full sample as is with the commented output of tut's `:book` modifier.  


## SBT Configuration

SBT itself also has configuration files that can be located either in the users home directory:

```
/home/user/.sbt/1.0/plugins
```

or the base projects's `project` directory:

```
/home/user/git/adw/project
```

**Any and all configurations must be placed in the project's SBT configuration folder**. The first configuration 
file is the `build.properties` file were we specify the SBT version:

```
sbt.version=1.2.6
```

The `plugins.sbt` file has SBT commands (using the same DSL and code as used in the `build.sbt` file) that allows 
us to set-up additional resolvers and load the necessary plugins. Plugins are Scala modules that are used by SBT
during any of the build phases. They also provide additional commands. Some plugins require that they be enabled.
In theses cases use the `enablePlugins` function in the `build.sbt` file to activate the plugin. You will also need 
to include the import to access the plugin tasks. For an example see `MicrositesPlugin` in the `build.sbt` file. 

Both these configurations ensure that that any one that builds this platform will do it under very similar conditions. 

## SBT Memory issues

Due to lack of memory the SBT commands may fail. For example when executing a test the JVM's (Java Virtual Machine) GC 
(garbage collector) may throw an Out of Memory Exception (OOM). If this occurs the first step is to make sure the JVM 
executes the Scala applications and tests without any OOM exceptions. If it does raise the OOM 
exception we need to increase the amount of memory the JVM may consume. Note that it is important that the OOM is 
not due to a **memory leak**. Once we have established the minimum amount of memory required by the JVM we the set the 
SBT's JVM memory configurations. At this point one should be able to execute the commands successfully within the SBT
shell. 

In order to proceed with the tests, first establish the base memory setting used by SBT executing the following command
line:

```bash
user@machine:~/IdeaProjects/adw$ sbt -v 
[process_args] java_version = '8'
# Executing command line:
java
-Xms1024m
-XX:ReservedCodeCacheSize=128m
-Xmx6G
-XX:MaxMetaspaceSize=2G
-XX:+CMSClassUnloadingEnabled
-jar
/usr/share/sbt/bin/sbt-launch.jar

[info] Loading global plugins from /home/user/.sbt/1.0/plugins
[info] Loading settings for project adw-build from plugins.sbt ...
[info] Loading project definition from /home/user/IdeaProjects/adw/project
[info] Loading settings for project adw from build.sbt ...
[info] Set current project to adw (in build file:/home/user/IdeaProjects/adw/)
[info] sbt server started at local:///home/user/.sbt/1.0/server/80ea990a12c62568d452/sock
sbt:adw> 
```

We now want to test how much memory is required by the JVM. The initial lines above provide us with the default memory 
used by SBT. We must therefore set the amount as the base memory and increase that in our tests. **Note:**  to execute 
the Scala application using the JVM we need to add all the compiled classes to the JVM's classpath. To do this it is 
best we first package the platform's code into a Jar library. More specifically we need to generate a 
[fat Jar](../installation.html#using-the-fat-jar){:target="_blank"} that also includes all of the required libraries used 
by the platform. Once done we can now execute a test directly using the JVM as shown in the next example:

```bash
java -Xmx1G -cp ./core.jar:/home/user/.ivy2/cache/org.scalatest/scalatest_2.12/bundles/scalatest_2.12-3.0.5.jar:./test-classes/ org.scalatest.tools.Runner -s pt.inescn.detector.FCCAnomalyDetectorSpec -o -e
```

In the command above the `-o` wnd `-e` set the output to the standard and error console output. If this were not set a 
GUI would list and report test execution results. The `scalatest_2.12-3.0.5.jar` is the ScalaTest library that contains 
the `org.scalatest.tools.Runner` class, which is responsible for running the test suites. Note that we also have to 
include the compiled test classes to the JVM's classpath (`assembly` does not do this out-of-the-box). The example below 
is the same as the one above but executed in a **Windows** console:


```bash
C:\Users\user\IdeaProjects\adw>java -Xmx1G -cp ./core/target/scala-2.12/core.jar;C:/Users/user/.ivy2/cache/org.scalatest/scalatest_2.12/bundles/scalatest_2.12-3.0.5.jar;./core/target/scala-2.12/test-classes/ org.scalatest.tools.Runner -s pt.inescn.detector.FCCAnomalyDetectorSpec -o -e
Run starting. Expected test count is: 2
FCCAnomalyDetectorSpec:
Anomaly detector
- should work in a single pipe detector
- should multiple pipe detector
Run completed in 6 minutes, 9 seconds.
Total number of tests run: 2
Suites: completed 1, aborted 0
Tests: succeeded 2, failed 0, canceled 0, ignored 0, pending 0
All tests passed.
Run completed in 6 minutes, 9 seconds.
Total number of tests run: 2
Suites: completed 1, aborted 0
Tests: succeeded 2, failed 0, canceled 0, ignored 0, pending 0
All tests passed.
```

Once we have established the minimum amount of memory that is required we can now set SBT to use that amount. The 
following  examples shows an example in **Windows** of how one can launch SBT with the required memory settings:

```bash
java -Xms1024m -XX:ReservedCodeCacheSize=128m -Xmx1G -XX:MaxMetaspaceSize=2G -XX:+CMSClassUnloadingEnabled -jar "C:/Program Files (x86)/sbt/bin/sbt-launch.jar"
```

Note that in principal we need only set the `-Xmx1G` flag. Here is a minimal working example:


```bash
java -Xmx1G -jar "C:/Program Files (x86)/sbt/bin/sbt-launch.jar"
```

In the SBT shell we can now execute `testOnly pt.inescn.detector.FCCAnomalyDetectorSpec` directly with no memory issues. 
Setting these flags and invoking SBT indirectly is cumbersome[^9]. We therefore opted to both configure SBT to 
[fork](https://www.scala-sbt.org/1.x/docs/Forking.html) the application and test executions and set the `javaOptions` of 
the forked execution directly in the `build.sbt` file. This also has the added benefit of avoiding other issues related 
to the SBT's class loader (multi-media files not found or problems with serialization). 

Several problems exist when setting a sub-project specific options. First in order to set the JVM flags within the 
`build.sbt` we have to configure SBT to fork a JVM for each command. This consumes more memory and CPU time (the JVM 
options are applied to each JVM and several of these may be executed in parallel). The second issue is when one executes 
a task indicating the sub-project, SBT changes the current working directory to the root of the sub-project. This means 
that the paths to the data files must be set accordingly.

**NOTE**: our aim is to fork the `test` and `run` tasks so that we can then set the JVM parameters. If we use: 

```sbtshell
fork in test := true
```

or the equivalent:

```sbtshell
test / fork := true
```

Then we need to set the JVM parameters so:

```sbtshell
test / javaOptions += "-Xmx1G"
```

If we set by project:

```sbtshell
root/Test/fork := true
```

then we need to set the parameters accordingly:

```sbtshell
root / Test / javaOptions += "-Xmx1G"
```

in the case we use the configuration `Test` and not the task `test`. As was pointed out before, using the sub-project 
in the set-up path will change the current working directory to the sub-project's root. 


## GitLab CI Set-up

All of [Gitlab CI/CD configuration](https://docs.gitlab.com/ee/ci/){:target="_blank"} is done in the `.gitlab-ci.yml` 
[YAML](https://docs.gitlab.com/ee/ci/yaml/){:target="_blank"} found in the ADW base. GitLab's continuous integration 
(CI) and continuous deployment (CD) infrastructure provides *runners* in the form of Linux virtual machines that execute 
a script. In order to execute the commands described above we have to ensure that the following has been installed:
* Java
* [SBT](https://www.scala-sbt.org/){:target="_blank"}
* [Jekyll](https://jekyllrb.com/){:target="_blank"} for use with the SBT Microsite 

For Java we can request use a virtual machine that has Java pre-installed. So in the `.gitlab-ci.yml` we have:

```yaml
image: java:8
```

In order to install the rest of the required tools add the relevant command lines in the `before_script:` section. We 
first make sure that the virtual machine's ([APT](https://en.wikipedia.org/wiki/APT_(Debian)){:target="_blank"}) package 
system is up to date by issuing an `update` and then installing the HTTP protocol that can be used by APT to download 
the packages. Here is the relevant snippet:

```yaml
before_script:
  - apt-get update -y
  - apt-get install apt-transport-https -y
  ...
```

We also need to install [Jekyll](https://jekyllrb.com/){:target="_blank"} that is use by the BST Microsite plugin. More 
information on the setup of this plugin can be found [here](microsite.html){:target="_blank"}. Note that we have opted 
to install the [Ruby Version Manager](https://rvm.io/){:target="_blank"}(RVM) in order to install the specific version 
of Ruby indicated by the SBT Microsite installation guide. Here is the YAML snippet:

```yaml
before_script:
  ...
  - export PATH=${PATH}:./vendor/bundle
  - apt install git curl libssl-dev libreadline-dev zlib1g-dev autoconf bison build-essential libyaml-dev libreadline-dev libncurses5-dev libffi-dev libgdbm-dev -y
  - gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
  - curl -sSL https://get.rvm.io | bash -s stable
  - source /etc/profile.d/rvm.sh
  - rvm use 2.2.8 --install --fuzzy
  - gem update --system
  - gem install sass
  - ruby --version
  - gem install jekyll -v 3.2.1
  ...
```

Finally we install an SBT server using the APT tool as shown below:

```yaml
before_script:
  ...
  - echo "deb http://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list
  - apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
  - apt-get update -y
  - apt-get install sbt -y
  - sbt sbtVersion

```

Note that we issue an `sbt sbtVersion` command to initialize the SBT installation. If we had set-up a global 
configuration, this set-up would also automatically download the required plugins. Because we only place our SBT 
configurations in the project's base directory, plugins are only downloaded and installed when the SBT commands are 
issue for the project and sub-projects (see [notes](#sbt-commands-getting-started) above). It is at this point the the
libraries are also downloaded. 

Once installed we are ready to execute our CI commands. These can be any command line instruction that can be issued in 
a Linux terminal. Usually the CI commands are split into **stages**. Each stage may have one or more **jobs** that can 
execute a sequence of instructions. All jobs that are defined in the same stage are executed in **parallel**. Stages are 
executed in sequence. In this way we can setup a (non-cyclic) graph of CI and CD scripts. If any job fails, the CI/CD 
graph terminates execution. The result of the CI/CD execution is shown in the Git projects 
[README.md](https://gitlab.com/cese/adw/blob/master/README.md){:target="_blank"} file. If it succeeded the
[build badge](https://gitlab.com/cese/adw/commits/master) will be *green*{: style="color: green"} otherwise it will be 
*red*{: style="color: red"}.

Note that the SCoverage plugin command can also be execute manually to check on the test coverage status. In the next 
example we see a session that generated a coverage report after `clean` and `test` was performed:

```sbtshell
sbt:adw> coverageReport
[info] Waiting for measurement data to sync...
[info] Waiting for measurement data to sync...
[info] Waiting for measurement data to sync...
[info] Waiting for measurement data to sync...
[info] Reading scoverage instrumentation [/home/user/git/adw/target/scala-2.12/scoverage-data/scoverage.coverage.xml]
[warn] No coverage data, skipping reports
[info] Reading scoverage instrumentation [/home/user/git/adw/macro/target/scala-2.12/scoverage-data/scoverage.coverage.xml]
[info] Reading scoverage measurements...
[info] Generating scoverage reports...
[info] Reading scoverage instrumentation [/home/user/git/adw/core/target/scala-2.12/scoverage-data/scoverage.coverage.xml]
[info] Written Cobertura report [/home/user/git/adw/macro/target/scala-2.12/coverage-report/cobertura.xml]
[info] Written XML coverage report [/home/user/git/adw/macro/target/scala-2.12/scoverage-report/scoverage.xml]
[info] Written HTML coverage report [/home/user/git/adw/macro/target/scala-2.12/scoverage-report/index.html]
[info] Statement coverage.: 37.56%
[info] Branch coverage....: 100.00%
[info] Coverage reports completed
[info] All done. Coverage was [37.56%]
[info] Reading scoverage instrumentation [/home/user/git/adw/docs/target/scala-2.12/scoverage-data/scoverage.coverage.xml]
[warn] No coverage data, skipping reports
[info] Reading scoverage measurements...
[info] Generating scoverage reports...
[info] Written Cobertura report [/home/user/git/adw/core/target/scala-2.12/coverage-report/cobertura.xml]
[info] Written XML coverage report [/home/user/git/adw/core/target/scala-2.12/scoverage-report/scoverage.xml]
[info] Written HTML coverage report [/home/user/git/adw/core/target/scala-2.12/scoverage-report/index.html]
[info] Statement coverage.: 49.47%
[info] Branch coverage....: 56.41%
[info] Coverage reports completed
[info] All done. Coverage was [49.47%]
[success] Total time: 6 s, completed 22/nov/2081 10:49:33
```


This framework defines the following stages:

```yaml
stages:
  - build
  - test
  - doc
  - pages
```

These stages are automatically execute when the [Git](https://git-scm.com/){:target="_blank"} project is committed and 
pushed to the repository. GitLab provides a dashboard for each Git project. This platform's Git CI/CD pipeline dashboard
is found [here](https://gitlab.com/cese/adw/pipelines){:target="_blank"}.

The `build` stage has one job that is used to compile the project (it compiles all SBT sub-projects). Here is the 
relevant code snippet:

```yaml
build job:
  stage: build
  script:
    - sbt compile
```

The `test` stage has a single job that executes all [ScalaTest](http://www.scalatest.org/){:target="_blank"} unit tests. 
In addition to this we also collects test [coverage](http://scoverage.org/){:target="_blank"}[^7] using an 
[SBT plugin](https://github.com/scoverage/sbt-scoverage){:target="_blank"}, which is configured in the 
`projects/plugin.sbt` file. Here is the code snippet that does the testing and test coverage analysis. 

```yaml
test job:
  stage: test
  script:
    - sbt clean coverage test coverageReport
```

The SBT command line above contains three separate instructions. The first is a `clean` to ensure we re-build with all 
of the latest source files before any tests are execute in order to ensure that the latest changes are tested. We then 
have a `coverage` instruction that sets coverage testing on. Then we execute the tests with `test`. Because we have 
coverage testing activated the tests are collected and coverage estimated. Finally the `coverageReport` generates the 
reports. We also use a [coverage badge](https://gitlab.com/cese/adw/commits/master){:target="_blank"} in the 
[README.md](https://gitlab.com/cese/adw/blob/master/README.md){:target="_blank"} file. If the test coverage is acceptable 
then the [coverage badge](https://gitlab.com/cese/adw/commits/master) will be *green*{: style="color: green"} otherwise 
if it is low, it will be *red*{: style="color: red"}.

The `doc` stage has one job that generates the projects API documentation using Scaladoc. It will later be incorporated 
into the static web-site that is generated and published via the `pages` job (see below):

```yaml
doc job:
  stage: doc
  script:
    - sbt doc
```

The last stage `pages` has one job that generates the static web pages using the SBT Microsite plugin (see relevant 
information [here](microsite.html)). This plugin is also configured in the `projects/plugin.sbt` file. The pages job
has a specific pre-configured [behaviour](https://docs.gitlab.com/ee//user/project/pages/){:target="_blank"} in 
GitLab's CI/CD infrastructure. Here is the relevant code:

```yaml
pages:
  stage: pages
  script:
  - sbt makeMicrosite
  - mkdir public
  - cp -r docs/target/site/* public
  artifacts:
    paths:
    - public
  only:
    - master
```

It is important to note that the **job name must also be named `pages`**. Unlike the previous jobs this one has three 
instructions. The first is the `sbt makeMicrosite` that generates the static web site. These files are placed by SBT in 
the `docs/target/site/*` target directory. The second instruction prepares a `public` directory into which the third 
instruction copies all of the static web site files. Once the `public` directory is ready we must instruct GitLab's CI 
to publish the pages. The `artifacts` section indicates that all of the contents in `public` must be published. The 
`only` section tells GitLab's CI do **execute the `stage` job only for the master branch**. This means that is you do 
fork a branch in order to contribute documentation you have to run the `sbt makeMicrosite` and verify that the result is 
properly rendered in a browser. 

We point out that we also use a Jekyll based static web page generator as does 
[GitLab pages](https://docs.gitlab.com/ee//user/project/pages/getting_started_part_four.html){:target="_blank"}. However
this is not a strict requirement. Any site with static web-pages can be generated and deployed.

## Using an IDE

As a convenience to the [eclipse](https://www.eclipse.org/) users the 
[sbt eclipse plugin](https://github.com/sbt/sbteclipse){:target="_blank"} is included in the project. Before proceeding 
make sure you have installed the SBT plugin first. To use this plugin first clone the project using the command line 
`git clone https://gitlab.com/cese/adw.git`[^8]. Then at the base directory execute `sbt eclipse`. You can then import 
the Scala project via the IDE menus. Note that whenever you change the `build.sbt` file or any of the SBT configuration 
files, you have to rerun the `sbt eclipse` command.

An alternate option is to use the [IntelliJ IDE](https://www.jetbrains.com/idea/){:target="_blank"}. It has support for 
SBT [out of the box](https://www.jetbrains.com/help/idea/sbt-support.html). Before proceeding you will need to install 
the [Scala Plugin](https://plugins.jetbrains.com/plugin/1347-scala){:target="_blank"}. It is also advisable to install 
the [Markdown plugin](https://plugins.jetbrains.com/plugin/7793-markdown-support). As above you can `git clone` the 
repository, launch SBt in the root and create an IntelliJ project file using the command `gen-idea` and then import the 
project (`File -> New -> Project from Existing Sources`). As with Eclipse, any changes to the SBT project configurations 
requires you run the SBT command and reload the project. On every import you have to set the project to recognize it as 
a Scala project. 

The easiest and most direct way is to import the Scala project from the repository via the IDE menu: 
`File -> New -> Project from Version Control -> Git`. Indicate the URl and clone the repo (provide your username and 
password tom login). The repo will be cloned. When the SBT build file is detected the IDE will request you set-up the 
JDK and Scala compilers. Once done an internal IDE SBT session will be launched. You can now run the SBT 
commands there. You are also free to run the SBT session in a console outside the IDE. IntelliJ will detect changes to 
the SBT configuration files. You can either manual import those changes or enable auto-import, which will do this 
for you automatically. 

----------------------------------

[^1]: Fluent Interface described [here](https://en.wikipedia.org/wiki/Fluent_interface){:target="_blank"}.
[^2]: You have to have SBT installed locally on your machine. Installation instruction can be found [here](https://www.scala-sbt.org/1.0/docs/Setup.html){:target="_blank"}.
[^3]: Compilation may result in warnings and errors, which are not shown here. 
[^4]: We did not expect compilation of the core non-test source code, and cannot explain why this happens. 
[^5]: Instructions on how to add a new [test target](https://www.scala-sbt.org/1.x/docs/Testing.html#Custom+test+configuration){:target="_blank"}.
[^6]: YMMV
[^7]: SCoverage GitHub [site]([SCoverage](https://github.com/scoverage)){:target="_blank"}.
[^8]: The project URL is found in the projects main GitLab page. 
[^9]: An alternate solution is `set SBT_OPTS = -Xmx1G` (windows) or `export SBT_OPTS = -Xmx1G` (Linux) and then execute SBT directly.