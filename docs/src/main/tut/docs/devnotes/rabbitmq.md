---
layout: docs
title:  "Rabbit MQ Details"
section: "devnotes"
---

# Introduction

Some tests have shown that it is necessary to be careful with the set-up of the
message broker. If this is not done, performance may be on par with the 
sampling an processing create required to deal with certain data.

We followed some of the suggestions made in these links:

* [Rabbitmq Best Practice - part1](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html)
* [Rabbitmq Best Practice - part1: prefetch section](https://www.cloudamqp.com/blog/2017-12-29-part1-rabbitmq-best-practice.html#prefetch)
* [Rabbitmq Best Practice - part2](https://www.cloudamqp.com/blog/2018-01-08-part2-rabbitmq-best-practice-for-high-performance.html)

## Issues with Slow consumption

### Non-Persistence

We observed that the sending of was able to store data at very high rates. 
However, consumers seemed to lag. After an analysis by the IoT colleagues,
it was suggested that the `BasicProperties` parameter was set to null but 
should be explicitly set to a specific value. Concretely the following 
method should be called with a `MessageProperties`or a value of our own 
choosing: 

```
channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes)
```
Need to make the messages non-persistent. 

```
channel.basicPublish("", QUEUE_NAME, MessageProperties.TEXT_PLAIN, message.getBytes)
```

This had a good effect of reducing the number of messages on queue. These
still grew steadily but a a lower rate. Unfortunately the number of 
unacknowledged messages increased and kep increasing at a low rate. We
used auto-acknowledge to reduce tis top 0.  

### Auto-acknowledge

We were using manual acknowledgement calling:

```
val deliveryTag = msg.getEnvelope.getDeliveryTag
channel.basicAck(deliveryTag, false)
```

This was removed and the auto-acknowledge flag was set to true:

```
msg = channel.basicGet(queueName, true)
```

The number of unacknowledged messages decreased to 0. However the number of
message sin the queue still rises at the same steady rate. 

**We have noticed that client are queue a large amount of messages.** To
test this we need only deactivate the main producer and purge all queued
messages. The main consumer will still process messages that are collected
at the client side. In norder to avoid overruning the clients we either 
need server side throttling, very efficient clients, use a pull 
architecture or drop messages. Note that *efficient clients* a solution 
because in any of the other cases data will still accumulate.    

### Throttling the Producer

RabbitMQ is a push architecture (as apposed to the pull of 
[Apache Kafka](https://kafka.apache.org/)). One way to throttle at the 
server is based on RabbitMQ's unacknowledged messages count referred to
as [prefetch](https://www.rabbitmq.com/confirms.html#channel-qos-prefetch).
By using this we should be able to can control the 
[throughput](https://www.rabbitmq.com/confirms.html#channel-qos-prefetch-throughput).
In order to do this we added the QoS command `channel.basicQos(10)` so
as exemplified [here](https://www.rabbitmq.com/consumer-prefetch.html):

```
// create a connection instance
val conn: Connection = factory.newConnection
// the connection interface can be used to open a channel
val channel: Channel = conn.createChannel
// Per consumer limit
channel.basicQos(10)
```

These QoS setting are set at a the [channel level](https://www.rabbitmq.com/confirms.html#channel-qos-prefetch-throughput)
but can also be set at the queue level. 

**This change did not alter anything**. In fact it seemed to make to problem 
slightly worse for the main consumer. This is expected because RabbitMQ only 
uses `prefecth`to stop sending when the number of outstanding acknowledgements
exceeds the limit set. We were to set these top 0 by using auto-acknowledge. 
 
We could try and use manual acknowledges to throttle the server but this 
is problematic. Should we accumulate acknowledges in the clients? Currently 
experiments seem to show that message accumulation is due to **consumer 
inefficiencies**. 

### TTL

Another option we did not explore because it may have unintended consequences.
See this [blog entry](https://www.rabbitmq.com/blog/2014/01/23/preventing-unbounded-buffers-with-rabbitmq/).

### Here are some additional references: 

* [RabbitMQ Javadoc API](https://www.rabbitmq.com/documentation.html){:target="_blank"}
  * [MessageProperties](https://rabbitmq.github.io/rabbitmq-java-client/api/current/com/rabbitmq/client/MessageProperties.html)
* [RabbitMQ Documentation](https://rabbitmq.github.io/rabbitmq-java-client/api/current/){:target="_blank"}
  * [Priority](https://www.rabbitmq.com/priority.html){:target="_blank"}
  * [Consumer Priority](https://www.rabbitmq.com/consumer-priority.html){:target="_blank"}
  * [Flow Control](https://www.rabbitmq.com/flow-control.html){:target="_blank"}
* [Kafka versus RabbitMQ](https://www.upsolver.com/blog/kafka-versus-rabbitmq-architecture-performance-use-case){:target="_blank"}

