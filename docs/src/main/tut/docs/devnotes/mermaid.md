---
layout: docs
title:  "MermaidJS"
section: "devnotes"
---

# MermaidJS Plugin


In order to draw the graphs in the documentation we are using [MermaidJS](https://mermaidjs.github.io/) whose code 
repository is in [GitHub](https://github.com/knsv/mermaid). This JS library is executed after the page is loaded. It 
takes a text description of a graph that is placed in a `<div>` with class `mermaid` tag and converts it into a SVG 
diagram using the `<svg>` tag. Here is an example of a left to tight flowchart:

```HTML
<div class="mermaid">
  graph LR;
    title[<u>Pipe Example 1</u>]
    A --> B;
    A --> C;
    B --> D;
    C --> D;
</div>
```

which is rendered as:

<div class="mermaid">
graph LR;
  title[<u>Pipe Example 1</u>]
  A --> B;
  A --> C;
  B --> D;
  C --> D;
</div>


We note that a [Jekyll gem](https://rubygems.org/gems/jekyll-mermaid/versions/1.0.0)[^1] exists for this but we have opted 
to include the compiled version of Mermaid in our documentation sub-project because:
1. We have no dependency on the network
1. We need not add another element to install in the CI pipeline
1. Version management and therefore bug creep is under control

Instruction on how to install and a link to download the compiled sources is 
[here](https://mermaidjs.github.io/usage.html). In order to install this library in the documentation project we had
to:

1. Download the latest version [here](https://unpkg.com/mermaid/)
1. Create the Jekyll **JavaScript includes** directory `docs/src/main/microsite/js`. 
1. Copy the minified version of the file `mermaid.full.min.js` to the Jekyll `js` directory

We also include an example of the `mermaid.min.css` style sheet that seems to be 
[necessary](https://github.com/knsv/mermaid/issues/273) for proper rendering. 

In order to use this library in the documentation markdown pages we have no need for additional setup. This is because 
the [sbt microsite](https://47deg.github.io/sbt-microsites/) automatically includes all JS libraries in the HTML page 
body. In this case it adds (press *Shift-Ctrl-I* in FireFox and look for mermaid):
```HTML
<script src="/adw/js/mermaid.full.min.js"></script>
```
However we have included the following set up in order to reference the style sheet and manage some MermaidJS configurations:
```HTML
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="../js/mermaid.min.css">
</head>
<script>mermaid.initialize(
   {
     startOnLoad:true,
     flowchart:{ useMaxWidth:true, }     
    });
</script>
```

During experimentation we found that a previous version was *inserting* large blank spaces between the top and bottom 
graph and the surrounding text. We reported the [issue](https://github.com/knsv/mermaid/issues/754) and found that 
the problem was that MermaidJS was setting the `height` attribute to 100% (see below). 

```HTML
<svg id="mermaidChart0" xmlns="http://www.w3.org/2000/svg" 
     height="100%" viewBox="0 0 335.71875 219.375" 
     style="max-width:335.71875px;">
```
The issue could be resolved by removing the height attribute, because it is affecting the SVG 
[scaling](https://css-tricks.com/scale-svg/). However this is not possible by configuring Kramdown or MermaiJS 
parsing to remove the *offending* attribute. But we can use the parent container to scale the SVG so that no space is 
seen. Here is the final HTML snippet:

```HTML
<div class="mermaid" style="height: 30%" data-processed="true">
  <svg id="mermaidChart0" xmlns="http://www.w3.org/2000/svg" 
     height="100%" viewBox="0 0 304 210.3333282470703" style="max-width:304px;">
     ...
</svg></div>
```

For Kramdown we need only add the `height` attribute to the `<div>` tag, here is an example:

```HTML
<div class="mermaid" style="height: 30%">
    graph LR;
      title[<u>Pipe Example 1</u>]
      A --> B;
      A --> C;
      B --> D;
      C --> D;
</div>
```
The latest versions of MermaidJS use the `width = "100%"` so this problem does not show up. However in the future we may 
need to use this technique to scale the SVG for proper rending. 



[^1]: See GitLab's notes on [Jekyll's graphing plugin](https://docs.gitlab.com/ee/user/markdown.html#mermaid). 


{% include katex.md %}
{% include mermaid.md %}