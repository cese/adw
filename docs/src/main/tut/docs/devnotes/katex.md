---
layout: docs
title:  "KaTex"
section: "devnotes"
---

# KaTex Plugin

We also need to show mathematical expressions in our HTML documentation. The usual tool used is 
[mathjax](https://www.mathjax.org/). However we have opted for use of [KaTex](https://katex.org/), which is also an 
[source project](https://github.com/Khan/KaTeX) and part of the [Khan Academy](https://www.khanacademy.org/). This is
because [Gitlab](https://docs.gitlab.com/ee/user/markdown.html#math) also uses this Latex like renderer in its 
documentation generator. Support for maths expressions is already available in Kramdown, however the math 
[blocks](https://kramdown.gettalong.org/syntax.html#math-blocks) are **not** the same as those used in GitLab. More
concretely all math expressions must be placed between the `$$` block boundaries **only**. If the math block is the only 
element in a line it will be rendered in display mode. If it is in a line it is shown inline. A line with only a math 
expression can also be rendered inline (see [documentation](https://kramdown.gettalong.org/syntax.html#math-blocks)). 

As with MermaidJS we will use the JS conversion scripts directly in the Markdown pages. We can find instruction on how 
do this [here](https://xuc.me/blog/katex-and-jekyll/) and [here](https://kramdown.gettalong.org/math_engine/mathjax.html)
[^1]. According to the first set of instructions we need to *activate* the markdown engine. The following is an example 
of how it is done:

```Markdown
    ---
    layout: docs
    title:  "Introduction"
    section: "intro"
    markdown: kramdown
    kramdown:
      math_engine: mathjax
    ---
```

However this does not seem to be necessary. Nevertheless, we do need to add the JS scrips and we can do this via the 
standard method of using the `<script>` tag. The following code should be added to the start of the markdown file:

```HTML
<!-- Load jQuery -->
<script src="//code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Load KaTeX -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.1.1/katex.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/KaTeX/0.1.1/katex.min.js"></script>
```

Once the converters have been loaded they must be invoked on the appropriate math blocks. When Kramdown encounters a 
**display mode** math block it is replaced by a [script tag](https://kramdown.gettalong.org/math_engine/mathjax.html):
```HTML
 <script type="math/tex; mode=display"> 
``` 
otherwise it is replaced by an **inline** script tag:
```HTML
 <script type="math/tex"> 
``` 
The text inside these scrip blocks needs to be processed and converted to HTML. We can add the following JS code to the 
end of the markdown file to do this:
```HTML
<script type="text/javascript">
    $("script[type='math/tex']").replaceWith(
      function(){
        var tex = $(this).text();
        return "<span class=\"inline-equation\">" + 
               katex.renderToString(tex) +
               "</span>";
    });
    
    $("script[type='math/tex; mode=display']").replaceWith(
      function(){
        var tex = $(this).text();
        return "<div class=\"equation\">" + 
               katex.renderToString("\\displaystyle "+tex) +
               "</div>";
    });
</script>
```
Of course it becomes cumbersome to add this code repeatedly every time you require LaTex math. We can use [Jekyll's 
include mechanism](https://jekyllrb.com/docs/includes/#including-files-relative-to-another-file) to do this for us. We
create the Jekyll includes directory `docs/src/main/resources/microsite/includes` and place the file `katex.md` in it.
This file contains the `<script>` tags to both load the libraries and the converter functions (see details above). We 
then need only *include* this file at the end of the markdown so:

```
{% raw  %} {% include katex.md %} {% endraw %}
```

As a result the following source below is converted by Katex:

```Markdown
Math expression in display mode:

$$E = mc^2$$

Here is an inline expression $$E = mc^2$$ here?
```

which results in the following output:

Math expression in display mode:

$$E = mc^2$$

Here is an inline expression $$E = mc^2$$ here?

We also made changes so that we use local libraries and do not load them from the Internet. Once again this allows us 
to:
1. Have no dependency on the network
1. Manage versions and therefore avoid bug creep

The latest JQuery can be found [here](https://jquery.com/download/). The Katex 
[installation](https://katex.org/docs/node.html) instruction also includes a link to the 
[minified](https://github.com/Khan/KaTeX/releases) Katex files. For the Katex conversion we only require the slim 
versions of the library (`jquery-x.y.z.slim.min.js`). However we downloaded all the files including fonts and 
contributions. All these files are copied to the Jekyll JS directory `docs/src/main/microsite/js`.

Unfortunately using the [included](https://jekyllrb.com/docs/includes/) file means that JS script loading URLs must be 
relative to the markdown file itself. So we need to copy all of the JavaScript files to the same `js` directory (no 
subdirectories can be used). However, when these are loaded they will look for the fonts in the `js/fonts` directory 
(this is defined in the `katex.css` file located in the `js`directory). But the sbt-microsite plugin only scans for 
and copies the files specified by the `includeFilter` now defined in the `build.sbt` file. The font files are not 
included in this filter by default. To allow for the copying of the fonts we add the font extensions `ttf`, `woff` and
`woff2` so:

```scala
  includeFilter in makeSite := "*.html" | "*.css" | "*.png" | "*.jpg" | "*.gif" | "*.js" | "*.swf" | "*.md"
    | "*.ttf" | "*.woff" | "*.woff2",
```

[^1]: Original information was obtained [here](https://github.com/gettalong/kramdown/issues/292). 
[^2]: Solution obtained from [Relative paths in Jekyll](https://ricostacruz.com/til/relative-paths-in-jekyll).

{% include katex.md %}
{% include mermaid.md %}