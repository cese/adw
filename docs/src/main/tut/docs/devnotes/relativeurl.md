---
layout: docs
title:  "relative URLs"
section: "devnotes"
---

# Setting up a Relative base for URL References

As we previously described in the [mermaidJS](mermaid.md) and [KaTex](katex) plugins, the included files reference the 
JS scripts (and other files) using URLs relative to a base, which are the Markdown source files themselves. This means 
the the `katex.md` and `mermaid.md` files for example cannot be included with a fixed URL in all Markdown files located 
at different directories. We need to determine the base relative to the position of the source Markdown file and use 
that to reference the correct URLs. A solution[^2] is to use 
[Liquid](https://jekyllrb.com/docs/liquid/){:target="_blank"} template language to determine the path relative to the 
site base so:

```liquid
{% raw %}
{% assign base = '' %}
{% assign depth = page.url | split: '/' | size | minus: 1 %}
{% if    depth <= 1 %}{% assign base = '.' %}
{% elsif depth == 2 %}{% assign base = '..' %}
{% elsif depth == 3 %}{% assign base = '../..' %}
{% elsif depth == 4 %}{% assign base = '../../..' %}{% endif %}
{% endraw %} 
```

A `base.md` with this context is created and placed in the site root. It is then included at the start of the Markdown 
sources (see for  example files `katex.md` and `mermaid.md`) and all URLs are then defined using the `base` variable. 
Here is an example snippet taken from the `mermaid.js` file:

```HTML
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="{{base}}/js/mermaid.min.css">
</head>
```

and a snippet from the `katex.md` file:

```HTML
<script type="text/javascript" src="{{base}}/js/katex.min.js"></script>
```

