---
layout: page
title: Contributing
section: contributing
position: 2
---


## Project structure

```
project
│   README.md
│   CHANGELOG.md
│   COPYRIGHT
│   LICENSE
│
└───folder1
│   │   file011.txt
│   │   file012.txt
│   │
│   └───subfolder1
│       │   file111.txt
│       │   file112.txt
│       │   ...
│   
└───folder2
    │   file021.txt
    │   file022.txt

```

## Development Technologies

TODO
```
clean
compile
test:compile

test
testOnly pt.inescn.etl.stream.DataFilesSpec
testQuick

~testOnly pt.inescn.etl.stream.LoadStreamSpe

root/runMain inegi.ADIRAI40Experiments 4

exit
^D
```

```
show siteSubdirName
show root / target
show root / compile / target
adw/core/target/scala-2.12/api

show printTutClasspath
show printSiteClasspath
show printSiteResourceClasspath
show printDocClasspath

inspect run
evicted
```


```
reload
update
docs:clean

root / doc
doc


tut
root/tut
makeSite
root / makeSite
makeMicrosite
root/makeMicrosite

previewSite
```

```
cd /git/adw/docs/target/site
jekyll serve

```

## Continuous Integration
https://docs.gitlab.com/ee/ci/yaml/README.html
https://docs.gitlab.com/ee/ci/yaml/

## Documentation
### Markdown references:
1. https://daringfireball.net/projects/markdown/
1. https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
1. https://docs.gitlab.com/ee/user/markdown.html

https://jekyllrb.com/docs/
https://linuxize.com/post/how-to-install-ruby-on-ubuntu-18-04/
https://docs.gitlab.com/ce/user/project/pages/index.html

GitLAb uses Kramdown
```markdown
[here](https://gitlab.com/cese/adw/issues){:target="_blank"}
```




