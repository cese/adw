---
layout: page
title: Publications
section: Publications
position: 3
---

# Publications

This a a list of published articles whose data sets and experiments can be found in the source code. 

## Data Stream Models

* Ricardo Sousa, Joel Antunes, Filipe Coutinho, Emanuel Silva, Joaquim Santos, Hugo Ferreira, "Robust Cepstral Based Features for Anomaly Detection in Ball Bearings", The International Journal of Advanced Manufacturing Technology, Springer, DOI: 10.1007/s00170-019-03597-2; [link](https://link.springer.com/article/10.1007%2Fs00170-019-03597-2).

## Batch Data Models


