<!-- https://ricostacruz.com/til/relative-paths-in-jekyll -->
{% include base.md %}

<!-- Load jQuery -->
<script type="text/javascript" src="{{base}}/js/jquery-3.3.1.slim.min.js"></script>
<!-- Load KaTeX -->
<link rel="stylesheet" href="{{base}}/js/katex.min.css">
<script type="text/javascript" src="{{base}}/js/katex.min.js"></script>

<script type="text/javascript">
    $("script[type='math/tex']").replaceWith(
      function(){
        var tex = $(this).text();
        return "<span class=\"inline-equation\">" + 
               katex.renderToString(tex) +
               "</span>";
    });
    
    $("script[type='math/tex; mode=display']").replaceWith(
      function(){
        var tex = $(this).text();
        return "<div class=\"equation\">" + 
               katex.renderToString("\\displaystyle "+tex) +
               "</div>";
    });
</script>
