<!-- https://ricostacruz.com/til/relative-paths-in-jekyll -->
{% include base.md %}

<head>
  <meta charset="utf-8">
  <link rel="stylesheet" href="{{base}}/js/mermaid.min.css">
</head>
<script>mermaid.initialize(
   {
     startOnLoad:true,
     flowchart:{ useMaxWidth:true, }     
    });
</script>
