

# CSV
# Formatting
The SCV file is expected to adhere to the following requirements:

* The separator should be a semi-colon (";")
* The character code should be UTF-8
* A header must be provided
* All characters (including the header) should be in lower case
* Header
  * Only ascii characters 
  * Use quotes 
  * No spaces
  * lower case
* Fields
  * The first field must be a time stamp
  * It should have the format yyyy-mm-dd HH:MM:SS.SSSSSS
  * The fraction number of seconds must have enough precision for the selected sampling rate 
  * This field should be labeled "t"
  * If a label is available it should be the second field
  * The label should have the header "label"
  * The accelerometer should be labeled "x", "y" and "z"
  
## CSV Naming Convention

### Ball-bearing data
The file will be used to encode the experiment set-up and label. The
file name has the following structure:

* **rolX** indicates the ID of the ball-bearing used. It identifies 
the physical component. 
* **rpmXXXX** indicates the rotation speed of bearings (in this case the number o revolutions per minute) 
* **hpXX** indicates the load applied to the bearings (in this case measured in horse power)
* **AAAAA** is the code labelling the data with the type of failure. The
current codes are:
  * _norm_: No anomaly (failure)
  * _ir_: damage in the inner race
  * _or_: damage in the outer race
  * _b_: damage in the ball-bearing
* **micronXXXX** : is the size of the damage (in this case measured in microns)
* **expXXX** : is the identifier of the experiment 