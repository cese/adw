
#include <math.h>


#define FORESTL     581012	/* Number of samples  */
#define FORESTD     13		/* Size of the inputs */
#define FORESTGAMMA 1.0		/* Gamma for the rbf  */


// The contents of file "forest-short-581012x13.raw"

short forestdata[FORESTL][FORESTD];


// Returns the class +1 or -1 of example i

double 
forest_class(int i)
{
  return (double) forestdata[i][FORESTD-1];
}



// This computes the dot-product of 10 shorts using MMX.  
// These macros work with gcc.

static int 
asm_dot_product_10_short(const short* vec0, const short* vec1)
{
# define MMXemms \
    __asm__ volatile("emms" : : : "memory" )
# define MMXrr(op,src,dst) \
    __asm__ volatile( #op " %%" #src ",%%" #dst : : : "memory")
# define MMXir(op,imm,dst) \
    __asm__ volatile( #op " %0,%%" #dst : : "i" (imm) : "memory")
# define MMXar(op,addr,dst) \
    __asm__ volatile( #op " %0,%%" #dst : : "m" (*(int*)(addr)) : "memory")
# define MMXra(op,src,addr) \
    __asm__ volatile( #op " %%" #src ",%0" : : "m" (*(int*)(addr)) : "memory")
  int res = 0;
  MMXar( movq,       vec0,     mm0);
  MMXar( pmaddwd,    vec1,     mm0);
  MMXar( movq,       vec0+4,   mm1);
  MMXar( pmaddwd,    vec1+4,   mm1);
  MMXar( movq,       vec0+8,   mm2);
  MMXrr( paddd,      mm1,      mm0);
  MMXar( pmaddwd,    vec1+8,   mm2);
  MMXir( psllq,      32,       mm2);
  MMXrr( paddd,      mm2,      mm0);
  MMXrr( movq,       mm0,      mm6);
  MMXir( psrlq,      32,       mm0);
  MMXrr( paddd,      mm0,      mm6);
  MMXra( movd,       mm6,     &res);
  MMXemms;
  return res;
}

// This is a quick exp(-|x|) 
// Its result is within 1e-5 from the double exponential

double
qexpmx(double x)
{
#define A0   ((double)1.0)
#define A1   ((double)0.125)
#define A2   ((double)0.0078125)
#define A3   ((double)0.00032552083)
#define A4   ((double)1.0172526e-5) 
  x = fabs(x);
  if (x < 13.0) 
    {
      double y;
      y = A0+x*(A1+x*(A2+x*(A3+x*A4)));
      y *= y;
      y *= y;
      y *= y;
      y = 1/y;
      return y;
    }
  return 0;
}

// This computes a linear kernel product

double 
forest_dot_product(int i, int j)
{
  const short *p1 = forestdata[i];
  const short *p2 = forestdata[j];
  int s = asm_dot_product_10_short(p1,p2);
  double x;
  if (p1[10] == p2[10])
    s += 2000000;
  if (p1[11] == p2[11])
    s += 2000000;
  return (double) s / 1000000.0;
  
}


// This computes K(i,j)

static int forestnormokay = 0;
static double forestnorm[FORESTL];


double 
forest_kernel(int i, int j)
{
  double x;
  if (! forestnormokay)
    {
      int i;
      for (i=0; i<FORESTL; i++)
	forestnorm[i] = forest_dot_product(i,i);
      forestnormokay = 1;
    }
  if (i == j)
    return 1;
  x = forest_dot_product(i,j);
  return qexpmx(FORESTGAMMA * (forestnorm[i] - 2*x + forestnorm[j]));
}




