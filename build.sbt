import sbt.Keys.libraryDependencies

def getJDKVersion: Int = {
  var version = System.getProperty("java.version")
  if (version.startsWith("1.")) version = version.substring(2, 3)
  else {
    val dot = version.indexOf(".")
    if (dot != -1) version = version.substring(0, dot)
  }
  version.toInt
}

// Eclipse plugin
EclipseKeys.useProjectId := true
EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.ManagedClasses
EclipseKeys.executionEnvironment := Some(EclipseExecutionEnvironment.JavaSE18)
EclipseKeys.withSource := true
EclipseKeys.withJavadoc := true
EclipseKeys.withBundledScalaContainers := false
EclipseKeys.eclipseOutput := Some(".target")

val projScalaVersion = "2.12.10"
//val projScalaVersion = "2.13.0" // TODO: try again later - missing kantan.csv, jason4s, monix, breeze, smile
// https://stackoverflow.com/questions/40622878/how-do-i-tell-sbt-to-use-a-nightly-build-of-scala-2-12-or-2-13
//resolvers += "scala-integration" at "https://scala-ci.typesafe.com/artifactory/scala-integration/"
//val projScalaVersion = "2.12.9-bin-87d43d0"

scalacOptions += "-target:jvm-1.8"
// https://github.com/scoverage/sbt-scoverage/issues/239
//javacOptions ++= Seq("-encoding", "UTF-8", "-source", "1.8", "-target", "1.8", "-Xlint")
javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

scalaVersion in ThisBuild := projScalaVersion

// For ScalaTest: add in ~/.sbt/0.13/global.sbt
// export http_proxy="http://proxy.inescn.pt:3128"
resolvers += "Artima Maven Repository" at "https://repo.artima.com/releases"

//resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases"
resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
//resolvers += "Sonatype Releases" at "http://oss.sonatype.org/content/repositories/releases"
//resolvers += "mvnrepository" at "http://mvnrepository.com/artifact"
//resolvers += "Maven Central" at "http://repo1.maven.org/maven2"

//classpathTypes += "maven-plugin"

//kill process with cmd+c - testar
/*
https://github.com/scala/scala/pull/6479
SBT's CTRL-C handler is not on by default, you need to add
the following to build.sbt.
cancelable in Global := true

https://www.scala-sbt.org/0.13/sxr/sbt/Keys.scala.html
val cancelable = SettingKey[Boolean]("cancelable", "Enables (true) or disables (false) the ability to interrupt task execution with CTRL+C.", BMinusSetting)
 */
//cancelable in Global := true

// https://47deg.github.io/sbt-microsites/docs/
// Micro sites tut: http://tpolecat.github.io/tut/
// See also https://scalameta.org/mdoc/
import microsites._

// Viewing results
// 1. go to target/site.
// 2. start Jekyll with 'jekyll serve'.
// 3. http://localhost:4000/adw/
// publishMicrosite
// We do not publish, instead we use Gitlab's CI to generate the site
// Local site is placed in: git/adw/docs/target/site

val meta = """META.INF(.)*""".r

val commonSettings = Seq(
  scalaVersion := projScalaVersion,

  organization := "pt.inescn",
  //name := "adw",
  version := "0.2.1",
  description := "ADW: Anomaly Detection Workbench",
  organizationName := "INESC TEC",
  organizationHomepage := Some(url("https://www.inesctec.pt/en/centres/cese")), // used by sbt micro-site 1st
  homepage := Some(url("https://www.inesctec.pt")), // used by sbt micro-site 2nd

  //test in assembly := {},
  /*assemblyMergeStrategy in assembly := {
    case meta(_) => MergeStrategy.discard
    case _ => MergeStrategy.first
  }*/

)


// Extra-test
lazy val MLTest = config("ml") extend(Test)

// https://github.com/scalafx/scalafx-hello-world/blob/master/build.sbt
// Determine OS version of JavaFX binaries
lazy val osName = System.getProperty("os.name") match {
  case n if n.startsWith("Linux") => "linux"
  case n if n.startsWith("Mac") => "mac"
  case n if n.startsWith("Windows") => "win"
  case _ => throw new Exception("Unknown platform!")
}
lazy val jdkVersion = getJDKVersion

// Add JavaFX dependencies
lazy val javaFXModules = Seq("base", "controls", "fxml", "graphics", "media", "swing", "web")
/*lazy val javaFX = if (jdkVersion < 9)
                     Seq()
                  else javaFXModules.map( m =>
                            //  "org.openjfx" % s"javafx-$m" % "12.0.2" classifier osName
                                "org.openjfx" % s"javafx-$m" % "13" classifier osName
                        )*/
lazy val javaFX = javaFXModules.map( m =>
  // "org.openjfx" % s"javafx-$m" % "12.0.2" classifier osName
  "org.openjfx" % s"javafx-$m" % "13" classifier osName
  )

val scalatest_version = "3.0.8"
val scalactic = "org.scalactic" %% "scalactic" % scalatest_version % Test
val scalatest = "org.scalatest" %% "scalatest" % scalatest_version // % Test already specified

// Unit testing
val junit = "junit" % "junit" % "4.12" % Test


// Meta-programming

val scalaReflect = Def.setting { "org.scala-lang" % "scala-reflect" % scalaVersion.value }
val reflect = "org.scala-lang" % "scala-reflect" % projScalaVersion
//val scalameta_version = "3.7.4"
val scalameta_version = "4.1.12"
val scalameta = Seq(
  "org.scalameta" %% "scalameta" % scalameta_version,
  // TODO "org.scalameta" %% "contrib" % scalameta_version,
  reflect)

val shapeless = Seq("com.chuusai" %% "shapeless" % "2.3.3")

// I/O

// Scala I/O
val better_files_version = "3.8.0"
val better_files = Seq("com.github.pathikrit" %% "better-files" % better_files_version)

val rabbitmq = Seq("com.rabbitmq" % "amqp-client" % "5.7.3")

// CSV
// Core library, included automatically if any other module is imported.
//lazy val kantan_version = "0.1.19"
lazy val kantan_version = "0.5.1" // TODO: No 2.13 support
val kantan = Seq(
  "com.nrinaudo" %% "kantan.csv" % kantan_version,
  // Java 8 date and time instances.
  "com.nrinaudo" %% "kantan.csv-java8" % kantan_version,
  // Automatic type class instances derivation.
  "com.nrinaudo" %% "kantan.csv-generic" % kantan_version,
  // jackson-csv engine.
  "com.nrinaudo" %% "kantan.csv-jackson" % kantan_version)

// CSV
val totoshi_version = "1.3.6"
val totoshi = Seq("com.github.tototoshi" %% "scala-csv" % totoshi_version )  // TODO: use only one CSV lib

// JASON
val circeVersion = "0.12.0-M3"
val circe = Seq(
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "io.circe" %% "circe-shapes" % circeVersion)

// Date and Time
// http://www.time4j.net/tutorial/appendix.html
val threeten = Seq("org.threeten" % "threeten-extra" % "1.0")

// https://github.com/scala/bug/issues/11635
val json4s_version = "3.5.1" // "3.5.1" // "3.6.6" "3.6.7"
val json4s = Seq(
  "org.json4s" %% "json4s-native" % json4s_version,
  "org.json4s" %% "json4s-jackson" % json4s_version,
  "org.json4s" %% "json4s-ext" % json4s_version)

// Concurrent/parallel processing

//val monix = Seq("io.monix" %% "monix" % "2.3.0")
//val monix = Seq("io.monix" %% "monix" % "3.0.0-RC1")
// TODO: https://github.com/monix/monix/issues/862, check back on July for 2.13
val monix_version = "3.0.0-RC2"
val monix = Seq("io.monix" %% "monix" % monix_version)

// Plotting
val jfree_version = "1.0.19"
//val jfree_version = "1.5.0" // TODO: depends on scalanlp/Breeze
val jfreesvg_version = "3.4"
val jfree = Seq("org.jfree" % "jfreechart" % jfree_version,
                "org.jfree" % "jfreesvg" % jfreesvg_version)              // TODO: check version numbers


// https://github.com/alexarchambault/plotly-scala
// https://dzone.com/articles/beautiful-java-visualization-with-tablesaws-plotly
// https://github.com/jtablesaw/tablesaw
// https://jtablesaw.github.io/tablesaw/
//
// https://github.com/mpkocher/plotlypad
// https://plot.ly/scala/getting-started/

//Save in pdf files
// https://mvnrepository.com/artifact/com.orsonpdf/orsonpdf
val orsonpdf_version = "1.9"
val orsonpdf = Seq ("com.orsonpdf" % "orsonpdf" % orsonpdf_version )


// Math

// TODO: remove and use only commons?
val hipparchus_version = "1.5"
//libraryDependencies += "org.hipparchus" % "hipparchus-aggregator" % hipparchus_version
val hipparchus = Seq(
  "org.hipparchus" % "hipparchus-stat" % hipparchus_version,
  "org.hipparchus" % "hipparchus-fft" % hipparchus_version,
  "org.hipparchus" % "hipparchus-geometry" % hipparchus_version,
  "org.hipparchus" % "hipparchus-optim" % hipparchus_version,
  "org.hipparchus" % "hipparchus-fitting" % hipparchus_version,
  "org.hipparchus" % "hipparchus-clustering" % hipparchus_version,
  "org.hipparchus" % "hipparchus-ode" % hipparchus_version,
  "org.hipparchus" % "hipparchus-samples" % hipparchus_version)

// Apache Common: Math3
val commons = Seq("org.apache.commons" % "commons-math3" % "3.6.1" withSources() withJavadoc())

//val breeze_version = "0.13.1"
val breeze_version = "1.0-RC2" // TODO: no 2.13 support
//val breeze_version = "0.13.2" // TODO: no 2.13 support
// No 2.13 support, see https://github.com/scalanlp/breeze/issues/738
val breeze = Seq(
  "org.scalanlp" %% "breeze" % breeze_version,
  "org.scalanlp" %% "breeze-natives" % breeze_version, // BLAS, etc.
  "org.scalanlp" %% "breeze-viz" % breeze_version ) // It depends on LGPL code


// DSP

// DSP functions
val iirj_version = "1.0"
val iirj = Seq("uk.me.berndporr" % "iirj" % iirj_version)
// https://github.com/JorenSix/TarsosDSP
// http://www.source-code.biz/dsp/java/
// https://sourceforge.net/projects/dsplaboratory/
// http://vadim2000.chat.ru/soft.htm#Signalgo
// http://pauljbotelho.com/software.php?software=jein
// http://www.dickbaldwin.com/tocdsp.htm

// ML
//lazy val smileVersion = "1.5.2"
lazy val smileVersion = "1.5.3" // TODO: No 2.13 support
val smile = Seq(
  "com.github.haifengl" % "smile-core" % smileVersion,
  "com.github.haifengl" % "smile-nlp" % smileVersion,
  "com.github.haifengl" % "smile-netlib" % smileVersion,
  "com.github.haifengl" % "smile-plot" % smileVersion,
  "com.github.haifengl" %% "smile-scala" % smileVersion)

lazy val sl4jVersion = "1.7.26"
val sl4j = Seq("org.slf4j" % "slf4j-simple" % sl4jVersion)


// Used to measure memory consumption
// https://stackoverflow.com/questions/41182727/measure-memory-usage-of-code-unit
// . https://github.com/rklaehn/jamm-demo
// . https://github.com/jbellis/jamm
// Checking the parameters:
//   inspect root / javaOptions
//   show root / javaOptions
//   show compile:dependencyClasspath
//   show root/test:dependencyClasspath
// Check all is ok:
//    show root / Test / javaOptions
// NOTE: test / javaOptions does not work but Test / javaOptions is ok
// Use of Test also allows us to keep the runtime memory settings
// Add to the project
//   javaOptions += "-javaagent:/home/hmf/.ivy2/cache/com.github.jbellis/jamm/jars/jamm-0.3.3.jar",
//   Test / javaOptions += jammy.value,
// Adds JVM parameter
def makeAgentOptions(classpath:Classpath) : String = {
  val jammJar = classpath.map(_.data).filter(_.toString.contains("jamm")).head
  s"-javaagent:$jammJar"
}

// For debugging only
val jam_version = "0.3.3"
val jam = Seq("com.github.jbellis" % "jamm" % jam_version % "test")

// Adds agent JVM parameter
lazy val jammy = (dependencyClasspath in Test).map(makeAgentOptions)


val core_libs =
  scalameta ++ shapeless ++
  better_files ++ circe ++ totoshi ++ kantan ++ json4s ++ threeten ++
  monix ++
  jfree ++ orsonpdf ++
  hipparchus ++ commons ++ breeze ++
  smile ++
  iirj ++ rabbitmq ++ 
    sl4j
  /*jam*/

// https://stackoverflow.com/questions/27824438/how-to-include-test-classes-and-test-dependencies-in-sbt-asssembly
lazy val coreAssemblySettings = Seq(
  test in assembly := {},
  //mainClass in assembly := Some("pt.inescn.Main"),
  assemblyJarName in assembly := "test-core.jar",
  assemblyMergeStrategy in assembly := {
    case meta(_) => MergeStrategy.discard
    case _ => MergeStrategy.first
  }
)

lazy val root = (project in file("core"))
  .dependsOn(macroSub)
  .configs(MLTest)
  .settings(inConfig(Test)(coreAssemblySettings): _*)
  .settings(
    name := "adw",
    commonSettings,
    // Scala I/O
    libraryDependencies ++= core_libs,
    libraryDependencies += scalactic,
    libraryDependencies += scalatest,
    libraryDependencies += junit,
    libraryDependencies ++= javaFX,
    //libraryDependencies += spring,
    // Adding a separate ML test directory (long running processes)
    inConfig(MLTest)(Defaults.testSettings),
    libraryDependencies += scalatest % MLTest,
    // other settings here
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-explaintypes",
      "-Ypatmat-exhaust-depth", "off"), //, "-Ymacro-debug-verbose" , "-Ymacro-debug-lite"
    //Test / javaOptions += jammy.value,
    //target in Compile in doc := crossTarget.value / "api" // default
    //target in Compile in doc := "docs" / "api"  // same as below
    //target in Compile in doc := baseDirectory.value / "docs" / "api"
    test in assembly := {},
    assemblyJarName in assembly := "core.jar",
    assemblyMergeStrategy in assembly := {
      case meta(_) => MergeStrategy.discard
      case _ => MergeStrategy.first
    }
  )

lazy val micrositeSettings = Seq(
  micrositeName := "ADW",
  micrositeDescription := "Anomaly Detection Workbench Tutorial",
  micrositeBaseUrl := "adw",
  micrositeAuthor := "INESC TEC - CESE",
  micrositeDocumentationUrl := "docs",
  micrositeHomepage := "https://cese.gitlab.io/adw",
  micrositeGitHostingService := GitLab,
  micrositeGitHostingUrl := "https://gitlab.com/cese/adw",
  micrositeDataDirectory := (resourceDirectory in Compile).value / "microsite" / "data", // default
  micrositeStaticDirectory := (resourceDirectory in Compile).value / "microsite" / "static", // default
  micrositeExtraMdFiles := Map(
    file("CHANGELOG.md") -> ExtraMdFileConfig(
      "changelog.md",
      "page",
      Map("title" -> "Changelog", "section" -> "changelog", "position" -> "99")
    ),
    file("LICENSE") -> ExtraMdFileConfig(
      "license.md",
      "page",
      Map("title" -> "License", "section" -> "license", "position" -> "100")
    )
  ),
  // https://github.com/47deg/sbt-microsites/blob/master/src/main/scala/microsites/MicrositesPlugin.scala#L113
  includeFilter in makeSite := "*.html" | "*.css" | "*.png" | "*.jpg" | "*.gif" | "*.js" | "*.swf" | "*.md"
    | "*.ttf" | "*.woff" | "*.woff2",
  micrositeGitterChannel := false,
  micrositeShareOnSocial := false
)

lazy val docsMappingsAPIDir = settingKey[String]("Name of subdirectory in site target directory for api docs")

// 1) https://www.scala-sbt.org/sbt-site/api-documentation.html
// 2) https://www.scala-sbt.org/sbt-site/getting-started.html
// 3) https://github.com/scanamo/scanamo/blob/master/build.sbt // unidoc example
lazy val docs = (project in file("docs"))
  .dependsOn(root)
  .settings(moduleName := "docs")
  .settings(commonSettings)
  .settings(micrositeSettings: _*)
  .settings(
    name := "adw",
    siteSubdirName := "api",
    // Copy the files generated by the task packageDoc to the site's api sub-directory
    //addMappingsToSiteDir(mappings in (Compile, packageSrc), siteSubdirName),
    addMappingsToSiteDir(mappings in packageDoc in Compile in root, siteSubdirName),
    test in assembly := {},
    assemblyJarName in assembly := "docs.jar",
    assemblyMergeStrategy in assembly := {
      case meta(_) => MergeStrategy.discard
      case _ => MergeStrategy.first
    }

  )
  .enablePlugins(MicrositesPlugin)
  .enablePlugins(TutPlugin)
  .enablePlugins(SiteScaladocPlugin)


import sbt.IO

// Hack (9-11-2018)
// NOTE: when we try to access and use the sbt microsite settings we get an error
// Error: error: Illegal dynamic reference: scopedKey
// For a workaround see: https://github.com/sbt/sbt/issues/1993
// We created a fonts task just t get the appropriate SettingKey[sbt.File]

lazy val fonts = SettingKey[File]("fonts")

fonts := (docs / micrositeJsDirectory).value / "fonts"

lazy val copyKatexFonts = taskKey[Unit]("Copy Katex fonts")

// This task copies the KaTex font files to the microsite's target font directory
// We changed the includeFilter in the "doc" subproject to include the KaTex font files
// so we need not use this command
copyKatexFonts := {
  val dest = target.in(docs).in(Compile).value / "site" / "js" / "fonts"
  /*
  // Error, see hack above
  val scopedKey: SettingKey[sbt.File] = docs / micrositeJsDirectory
  val f = scopedKey.value.getAbsolutePath
  */
  val src = fonts.value
  IO.copyDirectory(src, dest, overwrite = true)
  None
}

// Make sure we can reference external libraries
autoAPIMappings := true


// MLTest

lazy val macroSub = (project in file("macro"))
  .settings(
    // Do not use the same name otherwise SBT wil complaint that a project will depend on itself
    name := "adwMacro",
    commonSettings,
    libraryDependencies += scalaReflect.value,
    // other settings here
    libraryDependencies ++= scalameta,
    libraryDependencies += scalatest,
    scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-explaintypes",
      "-Ypatmat-exhaust-depth", "off", "-Xlog-free-terms" ),
    test in assembly := {},
    assemblyJarName in assembly := "macros.jar",
    assemblyMergeStrategy in assembly := {
      case meta(_) => MergeStrategy.discard
      case _ => MergeStrategy.first
    }
  )




// TODO: add scalastyle checks also
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-explaintypes")
// last one used
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-explaintypes", "-Ypatmat-exhaust-depth", "off")

//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint:deprecation")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-Xfatal-warnings")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xfatal-warnings")
//scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature", "-Xlint", "-uniqid", "-explaintypes")

// http://www.scala-sbt.org/0.13/docs/Howto-Scaladoc.html
//scalacOptions in (Compile,doc) := Seq("-groups", "-implicits")
// https://stackoverflow.com/questions/8884866/how-to-exclude-java-source-files-in-doc-task
// sources in (Compile, doc) ~= (_ filter (_.getName endsWith ".scala"))
// https://issues.scala-lang.org/browse/SI-5533
// scaladoc -skip-packages <pack1>:<pack2>:...:<packN>
//scalacOptions in (Compile, doc) := List("-skip-packages",  "com.github.cjlin1")

//
//SBT offers 3 ways to pass JVM parameters:
//
//    Set environment variable SBT_OPTS
//    Set environment variable JAVA_OPTS
//    Pass parameters by command line option `-J-X`
//
// export JAVA_OPTS="-Xmx512m" sbt run
// JAVA_OPTS= -Dhttp.proxyHost=http://proxy2.inescn.pt -Dhttp.proxyPort=3128
// Use -J-X options to sbt for individual options, e.g. -J-Xmx2048 -J-XX:MaxPermSize=512
// This must be true for the line below to work
//fork in run := true
//
// java -XX:+PrintFlagsFinal
// java -XX:+PrintCommandLineFlags
// jps -lvm
// jinfo -flags <vmid>
// jinfo -sysprops <vmid>
//
//javaOptions in run += "-ea"
run / javaOptions += "-Xmx1G"
root / Test / javaOptions += "-Xmx1G"
//root / Test / javaOptions ++= Seq("-Xmx9G", "-Xms9G")
// Compilation error: stack overflow. Increase stack
// Don't forget to add the option to add the fork for the compile stage
javaOptions in compile += "-Xss512M"

//  Using Java Sound API in a Scala SBT-managed project
// When trying to use the Java Sound API to load valid WAV files under SBT, this failed with
// javax.sound.sampled.UnsupportedAudioFileException: file is not a supported file type
// SBT changes the default system class loader, which in trn causes problems when loading the
// Java Sound API classes. Their are 2 ways around this:
// 1. Change the SBT build to for a process so that the default class loader is used
//    See: https://stackoverflow.com/questions/18676712/java-sound-devices-found-when-run-in-intellij-but-not-in-sbt
// 2. Change the class loaders dynamically when using the Java Sound API
//    See: https://stackoverflow.com/questions/24145198/why-does-audiosystem-getmixerinfo-return-different-results-under-sbt-vs-scala
//    https://stackoverflow.com/questions/31727385/sbt-scala-audiosystem
// We have opted for solution (1)

fork in compile := true
fork in run := true
//fork in root/run := true // Does not work !!
run / connectInput := true
// This is not working, scalatest is messing with the classloader
//fork in test := true
root/Test/fork := true
root/run/fork := true
//test / connectInput := true

// http://central.sonatype.org/pages/ossrh-guide.html
// http://central.sonatype.org/pages/sbt.html
// https://github.com/xerial/sbt-sonatype
// http://www.scala-sbt.org/release/docs/Using-Sonatype.html
// http://outworkers.com/blog/post/scala-tips-publishing-to-sonatype-and-maven-central
// https://github.com/shekhargulati/52-technologies-in-2016/blob/master/02-sbt/README.md

// TODO: not used, remove?
// Unit testing
//libraryDependencies += "junit" % "junit" % "4.4"

// TODO: not used, remove?
// Logging
// http://alvinalexander.com/scala/how-to-use-java-style-logging-slf4j-scala
// https://github.com/typesafehub/scala-logging
//libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.23"
////libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.1"
//libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"


// TODO: not used, remove?
/* Linear Algebra
// http://statr.me/2015/09/an-overview-of-linear-algebra-libraries-in-scala-java/
// http://ejml.org/wiki/index.php?title=Main_Page
// http://commons.apache.org/proper/commons-math/index.html
libraryDependencies += "net.sourceforge.f2j" % "arpack_combined_all" % "0.1"
libraryDependencies += "com.github.fommil.netlib" % "all" % "1.1.2" pomOnly()
libraryDependencies += "com.googlecode.matrix-toolkits-java" % "mtj" % "1.0.4"
*/


/* TODO: not used, remove?
lazy val saddle_version = "1.3.5-SNAPSHOT"
libraryDependencies += "org.scala-saddle" %% "saddle-core" % saddle_version
//libraryDependencies += "org.scala-saddle" %% "saddle-hdf5" % saddle_version
*/

// TODO: not used, remove?
// ML "1.2.2"
//lazy val smileVersion = "1.5.2"
//libraryDependencies += "com.github.haifengl" % "smile-core" % smileVersion
//libraryDependencies += "com.github.haifengl" %% "smile-scala" % smileVersion
////libraryDependencies += "com.github.lwhite1" % "tablesaw" % "0.7.6.4"
////libraryDependencies += "com.github.lwhite1" % "tablesaw" % "0.7.6.9"
//libraryDependencies += "com.github.lwhite1" % "tablesaw" % "0.7.7.1"


/* TODO: not used, remove?
libraryDependencies += "org.knowm.xchart" % "xchart" % "3.2.2"
*/


// ML Libraries

/*
// http://bytedeco.org/
// TODO
libraryDependencies += "org.bytedeco" % "javacpp" % "1.2.4"
//libraryDependencies += "org.bytedeco" % "javacpp" % "1.3.1"

libraryDependencies += "org.nd4j"     % "canova-api" % "0.0.0.17"
libraryDependencies += "org.nd4j"     % "canova-nd4j-image" % "0.0.0.17"


//lazy val dl4jVersion = "0.7.2"
lazy val dl4jVersion = "0.8.0"

//lazy val nd4jVersion = "0.7.2"
lazy val nd4jVersion = "0.8.0"


// fails libraryDependencies += "org.nd4j" % "nd4j-native-platform" % "0.6.0" classifier "" classifier "linux-x86_64"
//libraryDependencies += "org.nd4j"   % "nd4j-native-platform" % nd4jVersion
//libraryDependencies += "org.nd4j" % "nd4j-cuda-7.5-platform" % nd4jVersion
libraryDependencies += "org.nd4j" % "nd4j-cuda-8.0-platform" % nd4jVersion

// TODO
// https://github.com/deeplearning4j/nd4s
// we want https://github.com/deeplearning4j/nd4s/tree/nd4s-0.6.0
// https://github.com/deeplearning4j/nd4s/issues/82
// git clone https://github.com/deeplearning4j/nd4s.git
// cd nd4s/
// git tag -l
// git checkout tags/nd4s-0.6.0
// or git checkout tags/<tag_name> -b <branch_name>
// sbt
// set scalaVersion := "2.12.0"
// test:console
// (1 to 9).asNDArray(3,3)
//libraryDependencies += "org.nd4j" % "nd4s_2.12.0-M3" % "0.4-rc3.8"
// https://github.com/deeplearning4j/nd4s/issues/96
//libraryDependencies += "org.nd4j" %% "nd4s" % "0.7.0"
//libraryDependencies += "org.nd4j" %% "nd4s" % "0.6.0"
// https://github.com/deeplearning4j/nd4s/issues/96#issuecomment-260345356
// https://mvnrepository.com/artifact/org.nd4j/nd4s_2.11
//libraryDependencies += "org.nd4j" %% "nd4s" % nd4jVersion


libraryDependencies += "org.datavec" % "datavec-api" % "0.7.2"
libraryDependencies += "org.datavec" % "datavec-data-image" % "0.7.2"
libraryDependencies += "org.datavec" % "datavec-data-codec" % "0.7.2"
libraryDependencies += "org.datavec" % "datavec-data-audio" % "0.7.2"
//libraryDependencies += "org.datavec" % "datavec-nd4j-common" % "0.7.2" already loaded

libraryDependencies += "org.deeplearning4j" % "deeplearning4j-core" % "0.7.2"
*/

/* TODO: not used, remove?
//lazy val jtransforms_version = "3.1"
//libraryDependencies += "com.github.wendykierp" % "JTransforms" % jtransforms_version
*/

/* TODO: not used, remove?
lazy val elki_version = "0.7.1"
libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki" % elki_version
libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki-batik-visualization" % elki_version
libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki-project" % elki_version
libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki-libsvm" % elki_version
//libraryDependencies += "de.lmu.ifi.dbs.elki" % "elki-bundle" % elki_version
*/

/* TODO: not used, remove?
lazy val twelvemonkeys_version = "3.3.2"
// Breeze Viz uses JFreeChart which requires these extensions to java's ImageIO
// Due to issue in the dependency graph we had to explicitly add imageio-core and common-lang
// See https://github.com/haraldk/TwelveMonkeys/issues/167
libraryDependencies += "com.twelvemonkeys.imageio" % "imageio-core" % twelvemonkeys_version
libraryDependencies += "com.twelvemonkeys.common" % "common-lang" % twelvemonkeys_version
libraryDependencies += "com.twelvemonkeys.imageio" % "imageio-jpeg" % twelvemonkeys_version
libraryDependencies += "com.twelvemonkeys.imageio" % "imageio-tiff" % twelvemonkeys_version
*/

// https://github.com/vegas-viz/Vegas
// https://mvnrepository.com/artifact/org.vegas-viz/vegas_2.11
//libraryDependencies += "org.vegas-viz" %% "vegas" % "0.3.9"
// 2.12 not available


// TODO
// https://scala-blitz.github.io/
// https://bitbucket.org/oscarlib/oscar/wiki/Home
// https://github.com/bruneli/scalaopt
// https://github.com/scalanlp/breeze
// https://github.com/vagmcs/Optimus
// https://www.hipparchus.org/
// https://www.orekit.org/
// http://www.scalafx.org/
// https://github.com/vigoo/scalafxml
// https://github.com/scalafx/scalafx-ensemble
// Symja Library - Java Symbolic Math System
// https://bitbucket.org/axelclk/symja_android_library/overview
// https://github.com/yuemingl
// https://github.com/yuemingl/SymJava
// https://github.com/merantix/picasso (DNNs)
// http://www.ybrikman.com/writing/2014/05/05/you-are-what-you-document/
// https://github.com/szeiger/ornate
// https://github.com/tpolecat/tut
// https://github.com/laughedelic/literator
// https://github.com/wookietreiber/sbt-scaliterate
// https://stackoverflow.com/questions/16934488/how-to-link-classes-from-jdk-into-scaladoc-generated-doc


/***************************************
 *
 * Debug Routines
 *
 ***************************************/


// Tut
// Tasks
// - tut
// - tutQuick
// - tutOnly
// Changing configuration variables locally
// https://stackoverflow.com/questions/40766783/how-to-get-the-sub-project-path-in-sbt-multi-project-build
//tutSourceDirectory := sourceDirectory.in(root).in(Compile).value / "tut"
//tutSourceDirectory := baseDirectory.in(root).in(Compile).value / "docs" / "tut"
//tutTargetDirectory := crossTarget.in(root).value / "site" / "tut"

// https://stackoverflow.com/questions/21698205/how-to-display-classpath-used-for-run-task
// show printTutConfig
lazy val printTutConfig = taskKey[Unit]("Dump Tut Configuration Variables")

// inspect printTutConfig
printTutConfig := {
  println("printTutClasspath")
  println(s"tutSourceDirectory       = ${tutSourceDirectory.in(docs).value}")
  println(s"tutNameFilter            = ${tutNameFilter.in(docs).value}")
  println(s"tutTargetDirectory       = ${tutTargetDirectory.in(docs).value}")
  println(s"scalacOptions in Tut     = ${scalacOptions.in(Tut).value}")
  println(s"tutPluginJars            = ${tutPluginJars.in(docs).value}")
  None
}


// SBT MicroSite
// Tasks:
// - makeSite
// - previewSite
// - previewAuto
// Changing configuration variables locally
//siteSourceDirectory := target.value / "generated-stuff"
//siteSourceDirectory in makeSite := baseDirectory.in(root).in(Compile).value / "docs" / "site"
//siteSourceDirectory in makeSite := baseDirectory.in(root).in(Compile).value / "site"
//siteSourceDirectory := target.value / "generated-stuff"
//siteSourceDirectory := baseDirectory.in(root).in(Compile).value / "docs" / "site"
//siteSourceDirectory in MLTest := sourceDirectory.in(root).value / "paradox-site-1"
//siteSourceDirectory in Compile := sourceDirectory.in(root).value / "paradox-site-2"
//siteSourceDirectory in sourceManaged := sourceManaged.in(root).value / "paradox-site-3"
//siteSourceDirectory in crossTarget := crossTarget.in(root).value / "paradox-site-3"

// inspect printSiteConfig
lazy val printSiteConfig = taskKey[Unit]("Dump Micro-site configuration (not all)")

// show printSiteConfig
printSiteConfig := {
  println("printSiteResourceClasspath")
  println(s"micrositeImgDirectory              = ${micrositeImgDirectory.in(docs).value}")
  println(s"micrositeCssDirectory              = ${micrositeCssDirectory.in(docs).value}")
  println(s"micrositeJsDirectory               = ${micrositeJsDirectory.in(docs).value}")
  println(s"micrositeExternalLayoutsDirectory  = ${micrositeExternalLayoutsDirectory.in(docs).value}")
  println(s"micrositeExternalIncludesDirectory = ${micrositeExternalIncludesDirectory.in(docs).value}")
  println(s"micrositeDataDirectory             = ${micrositeDataDirectory.in(docs).value}")
  println(s"micrositeStaticDirectory           = ${micrositeStaticDirectory.in(docs).value}")
  println(s"micrositePluginsDirectory          = ${micrositePluginsDirectory.in(docs).value}")
  println(s"micrositeExternalLayoutsDirectory  = ${micrositeExternalLayoutsDirectory.in(docs).value}")
  println(s"micrositeExternalIncludesDirectory = ${micrositeExternalIncludesDirectory.in(docs).value}")
  None
}


lazy val printKatexFontsClassPath = taskKey[Unit]("Copy Katex fonts classpath")

printKatexFontsClassPath := {
  println("printKatexFontsClassPath")
  println(s"target.in(docs).in(Compile).value.getAbsolutePath = ${target.in(docs).in(Compile).value.getAbsolutePath}")
  println(s"crossTarget.in(docs).in(Compile).value.getAbsolutePath = ${crossTarget.in(docs).in(Compile).value.getAbsolutePath}")
  val dest = target.in(docs).in(Compile).value / "site" / "js" / "fonts"
  /*
  // Error, see hack above
  val scopedKey: SettingKey[sbt.File] = docs / micrositeJsDirectory
  val f = scopedKey.value.getAbsolutePath
  */
  println(s"fonts.value = ${fonts.value}")
  val src = fonts.value
  println(s"src =${src.getAbsolutePath}")
  println(s"des =${dest.getAbsolutePath}")
  None
}


// show printDocClasspath
lazy val printAPIDocClasspath = taskKey[Unit]("Dump SPI doc classpath")

printAPIDocClasspath := {
  println("printAPIDocClasspath")
  println(s"target.in(root).in(Compile).in(doc) = ${target.in(root).in(Compile).in(doc).value.getAbsolutePath}")
  println(s"scalacOptions.in(root).in(Compile) = ${scalacOptions.in(root).in(Compile).value}")
  None
}

