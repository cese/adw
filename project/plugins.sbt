
resolvers += "Artima Maven Repository" at "https://repo.artima.com/releases"

// For DL4J
addSbtPlugin("org.bytedeco" % "sbt-javacpp" % "1.13")

// For ScalaTest
// TODO: how do we add to "~/.sbt/0.13/global.sbt" the following resolver?
// TODO: resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
// http://stackoverflow.com/questions/42226004/unknown-artifact-sbtplugin-super-safe-compiler-with-scala-2-12/42444449#42444449
// Only works with 2.12.2
//addSbtPlugin("com.artima.supersafe" % "sbtplugin" % "1.1.2")

// For CI coverage testing
// coverageEnabled := true
// sbt clean coverage test
// sbt coverageReport
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.1")

// Avoid using the global sbt config file
addSbtPlugin("com.typesafe.sbteclipse" % "sbteclipse-plugin" % "5.2.4")

// http://www.scalastyle.org/
// http://www.scalastyle.org/sbt.html


// Micro sites
// sudo apt-get install jekyll
addSbtPlugin("com.47deg"  % "sbt-microsites" % "0.7.27")

// Create an assembly to run the application
// https://github.com/sbt/sbt-assembly
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.8")
